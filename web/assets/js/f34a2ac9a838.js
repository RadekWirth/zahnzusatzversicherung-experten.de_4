/*
 * jQuery mmenu v5.6.4
 * @requires jQuery 1.7.0 or later
 *
 * mmenu.frebsite.nl
 *	
 * Copyright (c) Fred Heusschen
 * www.frebsite.nl
 *
 * License: CC-BY-NC-4.0
 * http://creativecommons.org/licenses/by-nc/4.0/
 */
!function(e){function t(){e[n].glbl||(r={$wndw:e(window),$docu:e(document),$html:e("html"),$body:e("body")},i={},a={},o={},e.each([i,a,o],function(e,t){t.add=function(e){e=e.split(" ");for(var n=0,s=e.length;s>n;n++)t[e[n]]=t.mm(e[n])}}),i.mm=function(e){return"mm-"+e},i.add("wrapper menu panels panel nopanel current highest opened subopened navbar hasnavbar title btn prev next listview nolistview inset vertical selected divider spacer hidden fullsubopen"),i.umm=function(e){return"mm-"==e.slice(0,3)&&(e=e.slice(3)),e},a.mm=function(e){return"mm-"+e},a.add("parent sub"),o.mm=function(e){return e+".mm"},o.add("transitionend webkitTransitionEnd click scroll keydown mousedown mouseup touchstart touchmove touchend orientationchange"),e[n]._c=i,e[n]._d=a,e[n]._e=o,e[n].glbl=r)}var n="mmenu",s="5.6.4";if(!(e[n]&&e[n].version>s)){e[n]=function(e,t,n){this.$menu=e,this._api=["bind","init","update","setSelected","getInstance","openPanel","closePanel","closeAllPanels"],this.opts=t,this.conf=n,this.vars={},this.cbck={},"function"==typeof this.___deprecated&&this.___deprecated(),this._initMenu(),this._initAnchors();var s=this.$pnls.children();return this._initAddons(),this.init(s),"function"==typeof this.___debug&&this.___debug(),this},e[n].version=s,e[n].addons={},e[n].uniqueId=0,e[n].defaults={extensions:[],navbar:{add:!0,title:"Menu",titleLink:"panel"},onClick:{setSelected:!0},slidingSubmenus:!0},e[n].configuration={classNames:{divider:"Divider",inset:"Inset",panel:"Panel",selected:"Selected",spacer:"Spacer",vertical:"Vertical"},clone:!1,openingInterval:25,panelNodetype:"ul, ol, div",transitionDuration:400},e[n].prototype={init:function(e){e=e.not("."+i.nopanel),e=this._initPanels(e),this.trigger("init",e),this.trigger("update")},update:function(){this.trigger("update")},setSelected:function(e){this.$menu.find("."+i.listview).children().removeClass(i.selected),e.addClass(i.selected),this.trigger("setSelected",e)},openPanel:function(t){var s=t.parent(),a=this;if(s.hasClass(i.vertical)){var o=s.parents("."+i.subopened);if(o.length)return void this.openPanel(o.first());s.addClass(i.opened),this.trigger("openPanel",t),this.trigger("openingPanel",t),this.trigger("openedPanel",t)}else{if(t.hasClass(i.current))return;var r=this.$pnls.children("."+i.panel),l=r.filter("."+i.current);r.removeClass(i.highest).removeClass(i.current).not(t).not(l).not("."+i.vertical).addClass(i.hidden),e[n].support.csstransitions||l.addClass(i.hidden),t.hasClass(i.opened)?t.nextAll("."+i.opened).addClass(i.highest).removeClass(i.opened).removeClass(i.subopened):(t.addClass(i.highest),l.addClass(i.subopened)),t.removeClass(i.hidden).addClass(i.current),a.trigger("openPanel",t),setTimeout(function(){t.removeClass(i.subopened).addClass(i.opened),a.trigger("openingPanel",t),a.__transitionend(t,function(){a.trigger("openedPanel",t)},a.conf.transitionDuration)},this.conf.openingInterval)}},closePanel:function(e){var t=e.parent();t.hasClass(i.vertical)&&(t.removeClass(i.opened),this.trigger("closePanel",e),this.trigger("closingPanel",e),this.trigger("closedPanel",e))},closeAllPanels:function(){this.$menu.find("."+i.listview).children().removeClass(i.selected).filter("."+i.vertical).removeClass(i.opened);var e=this.$pnls.children("."+i.panel),t=e.first();this.$pnls.children("."+i.panel).not(t).removeClass(i.subopened).removeClass(i.opened).removeClass(i.current).removeClass(i.highest).addClass(i.hidden),this.openPanel(t)},togglePanel:function(e){var t=e.parent();t.hasClass(i.vertical)&&this[t.hasClass(i.opened)?"closePanel":"openPanel"](e)},getInstance:function(){return this},bind:function(e,t){this.cbck[e]=this.cbck[e]||[],this.cbck[e].push(t)},trigger:function(){var e=this,t=Array.prototype.slice.call(arguments),n=t.shift();if(this.cbck[n])for(var s=0,i=this.cbck[n].length;i>s;s++)this.cbck[n][s].apply(e,t)},_initMenu:function(){this.$menu.attr("id",this.$menu.attr("id")||this.__getUniqueId()),this.conf.clone&&(this.$menu=this.$menu.clone(!0),this.$menu.add(this.$menu.find("[id]")).filter("[id]").each(function(){e(this).attr("id",i.mm(e(this).attr("id")))})),this.$menu.contents().each(function(){3==e(this)[0].nodeType&&e(this).remove()}),this.$pnls=e('<div class="'+i.panels+'" />').append(this.$menu.children(this.conf.panelNodetype)).prependTo(this.$menu),this.$menu.parent().addClass(i.wrapper);var t=[i.menu];this.opts.slidingSubmenus||t.push(i.vertical),this.opts.extensions=this.opts.extensions.length?"mm-"+this.opts.extensions.join(" mm-"):"",this.opts.extensions&&t.push(this.opts.extensions),this.$menu.addClass(t.join(" "))},_initPanels:function(t){var n=this,s=this.__findAddBack(t,"ul, ol");this.__refactorClass(s,this.conf.classNames.inset,"inset").addClass(i.nolistview+" "+i.nopanel),s.not("."+i.nolistview).addClass(i.listview);var o=this.__findAddBack(t,"."+i.listview).children();this.__refactorClass(o,this.conf.classNames.selected,"selected"),this.__refactorClass(o,this.conf.classNames.divider,"divider"),this.__refactorClass(o,this.conf.classNames.spacer,"spacer"),this.__refactorClass(this.__findAddBack(t,"."+this.conf.classNames.panel),this.conf.classNames.panel,"panel");var r=e(),l=t.add(t.find("."+i.panel)).add(this.__findAddBack(t,"."+i.listview).children().children(this.conf.panelNodetype)).not("."+i.nopanel);this.__refactorClass(l,this.conf.classNames.vertical,"vertical"),this.opts.slidingSubmenus||l.addClass(i.vertical),l.each(function(){var t=e(this),s=t;t.is("ul, ol")?(t.wrap('<div class="'+i.panel+'" />'),s=t.parent()):s.addClass(i.panel);var a=t.attr("id");t.removeAttr("id"),s.attr("id",a||n.__getUniqueId()),t.hasClass(i.vertical)&&(t.removeClass(n.conf.classNames.vertical),s.add(s.parent()).addClass(i.vertical)),r=r.add(s)});var d=e("."+i.panel,this.$menu);r.each(function(t){var s,o,r=e(this),l=r.parent(),d=l.children("a, span").first();if(l.is("."+i.panels)||(l.data(a.sub,r),r.data(a.parent,l)),l.children("."+i.next).length||l.parent().is("."+i.listview)&&(s=r.attr("id"),o=e('<a class="'+i.next+'" href="#'+s+'" data-target="#'+s+'" />').insertBefore(d),d.is("span")&&o.addClass(i.fullsubopen)),!r.children("."+i.navbar).length&&!l.hasClass(i.vertical)){l.parent().is("."+i.listview)?l=l.closest("."+i.panel):(d=l.closest("."+i.panel).find('a[href="#'+r.attr("id")+'"]').first(),l=d.closest("."+i.panel));var c=!1,h=e('<div class="'+i.navbar+'" />');if(l.length){switch(s=l.attr("id"),n.opts.navbar.titleLink){case"anchor":c=d.attr("href");break;case"panel":case"parent":c="#"+s;break;default:c=!1}h.append('<a class="'+i.btn+" "+i.prev+'" href="#'+s+'" data-target="#'+s+'" />').append(e('<a class="'+i.title+'"'+(c?' href="'+c+'"':"")+" />").text(d.text())).prependTo(r),n.opts.navbar.add&&r.addClass(i.hasnavbar)}else n.opts.navbar.title&&(h.append('<a class="'+i.title+'">'+n.opts.navbar.title+"</a>").prependTo(r),n.opts.navbar.add&&r.addClass(i.hasnavbar))}});var c=this.__findAddBack(t,"."+i.listview).children("."+i.selected).removeClass(i.selected).last().addClass(i.selected);c.add(c.parentsUntil("."+i.menu,"li")).filter("."+i.vertical).addClass(i.opened).end().each(function(){e(this).parentsUntil("."+i.menu,"."+i.panel).not("."+i.vertical).first().addClass(i.opened).parentsUntil("."+i.menu,"."+i.panel).not("."+i.vertical).first().addClass(i.opened).addClass(i.subopened)}),c.children("."+i.panel).not("."+i.vertical).addClass(i.opened).parentsUntil("."+i.menu,"."+i.panel).not("."+i.vertical).first().addClass(i.opened).addClass(i.subopened);var h=d.filter("."+i.opened);return h.length||(h=r.first()),h.addClass(i.opened).last().addClass(i.current),r.not("."+i.vertical).not(h.last()).addClass(i.hidden).end().filter(function(){return!e(this).parent().hasClass(i.panels)}).appendTo(this.$pnls),r},_initAnchors:function(){var t=this;r.$body.on(o.click+"-oncanvas","a[href]",function(s){var a=e(this),o=!1,r=t.$menu.find(a).length;for(var l in e[n].addons)if(e[n].addons[l].clickAnchor.call(t,a,r)){o=!0;break}var d=a.attr("href");if(!o&&r&&d.length>1&&"#"==d.slice(0,1))try{var c=e(d,t.$menu);c.is("."+i.panel)&&(o=!0,t[a.parent().hasClass(i.vertical)?"togglePanel":"openPanel"](c))}catch(h){}if(o&&s.preventDefault(),!o&&r&&a.is("."+i.listview+" > li > a")&&!a.is('[rel="external"]')&&!a.is('[target="_blank"]')){t.__valueOrFn(t.opts.onClick.setSelected,a)&&t.setSelected(e(s.target).parent());var u=t.__valueOrFn(t.opts.onClick.preventDefault,a,"#"==d.slice(0,1));u&&s.preventDefault(),t.__valueOrFn(t.opts.onClick.close,a,u)&&t.close()}})},_initAddons:function(){var t;for(t in e[n].addons)e[n].addons[t].add.call(this),e[n].addons[t].add=function(){};for(t in e[n].addons)e[n].addons[t].setup.call(this)},_getOriginalMenuId:function(){var e=this.$menu.attr("id");return e&&e.length&&this.conf.clone&&(e=i.umm(e)),e},__api:function(){var t=this,n={};return e.each(this._api,function(e){var s=this;n[s]=function(){var e=t[s].apply(t,arguments);return"undefined"==typeof e?n:e}}),n},__valueOrFn:function(e,t,n){return"function"==typeof e?e.call(t[0]):"undefined"==typeof e&&"undefined"!=typeof n?n:e},__refactorClass:function(e,t,n){return e.filter("."+t).removeClass(t).addClass(i[n])},__findAddBack:function(e,t){return e.find(t).add(e.filter(t))},__filterListItems:function(e){return e.not("."+i.divider).not("."+i.hidden)},__transitionend:function(e,t,n){var s=!1,i=function(){s||t.call(e[0]),s=!0};e.one(o.transitionend,i),e.one(o.webkitTransitionEnd,i),setTimeout(i,1.1*n)},__getUniqueId:function(){return i.mm(e[n].uniqueId++)}},e.fn[n]=function(s,i){return t(),s=e.extend(!0,{},e[n].defaults,s),i=e.extend(!0,{},e[n].configuration,i),this.each(function(){var t=e(this);if(!t.data(n)){var a=new e[n](t,s,i);a.$menu.data(n,a.__api())}})},e[n].support={touch:"ontouchstart"in window||navigator.msMaxTouchPoints||!1,csstransitions:function(){if("undefined"!=typeof Modernizr&&"undefined"!=typeof Modernizr.csstransitions)return Modernizr.csstransitions;var e=document.body||document.documentElement,t=e.style,n="transition";if("string"==typeof t[n])return!0;var s=["Moz","webkit","Webkit","Khtml","O","ms"];n=n.charAt(0).toUpperCase()+n.substr(1);for(var i=0;i<s.length;i++)if("string"==typeof t[s[i]+n])return!0;return!1}()};var i,a,o,r}}(jQuery),/*	
 * jQuery mmenu offCanvas addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){var t="mmenu",n="offCanvas";e[t].addons[n]={setup:function(){if(this.opts[n]){var i=this.opts[n],a=this.conf[n];o=e[t].glbl,this._api=e.merge(this._api,["open","close","setPage"]),("top"==i.position||"bottom"==i.position)&&(i.zposition="front"),"string"!=typeof a.pageSelector&&(a.pageSelector="> "+a.pageNodetype),o.$allMenus=(o.$allMenus||e()).add(this.$menu),this.vars.opened=!1;var r=[s.offcanvas];"left"!=i.position&&r.push(s.mm(i.position)),"back"!=i.zposition&&r.push(s.mm(i.zposition)),this.$menu.addClass(r.join(" ")).parent().removeClass(s.wrapper),this.setPage(o.$page),this._initBlocker(),this["_initWindow_"+n](),this.$menu[a.menuInjectMethod+"To"](a.menuWrapperSelector);var l=window.location.hash;if(l){var d=this._getOriginalMenuId();d&&d==l.slice(1)&&this.open()}}},add:function(){s=e[t]._c,i=e[t]._d,a=e[t]._e,s.add("offcanvas slideout blocking modal background opening blocker page"),i.add("style"),a.add("resize")},clickAnchor:function(e,t){if(!this.opts[n])return!1;var s=this._getOriginalMenuId();if(s&&e.is('[href="#'+s+'"]'))return this.open(),!0;if(o.$page)return s=o.$page.first().attr("id"),s&&e.is('[href="#'+s+'"]')?(this.close(),!0):!1}},e[t].defaults[n]={position:"left",zposition:"back",blockUI:!0,moveBackground:!0},e[t].configuration[n]={pageNodetype:"div",pageSelector:null,noPageSelector:[],wrapPageIfNeeded:!0,menuWrapperSelector:"body",menuInjectMethod:"prepend"},e[t].prototype.open=function(){if(!this.vars.opened){var e=this;this._openSetup(),setTimeout(function(){e._openFinish()},this.conf.openingInterval),this.trigger("open")}},e[t].prototype._openSetup=function(){var t=this,r=this.opts[n];this.closeAllOthers(),o.$page.each(function(){e(this).data(i.style,e(this).attr("style")||"")}),o.$wndw.trigger(a.resize+"-"+n,[!0]);var l=[s.opened];r.blockUI&&l.push(s.blocking),"modal"==r.blockUI&&l.push(s.modal),r.moveBackground&&l.push(s.background),"left"!=r.position&&l.push(s.mm(this.opts[n].position)),"back"!=r.zposition&&l.push(s.mm(this.opts[n].zposition)),this.opts.extensions&&l.push(this.opts.extensions),o.$html.addClass(l.join(" ")),setTimeout(function(){t.vars.opened=!0},this.conf.openingInterval),this.$menu.addClass(s.current+" "+s.opened)},e[t].prototype._openFinish=function(){var e=this;this.__transitionend(o.$page.first(),function(){e.trigger("opened")},this.conf.transitionDuration),o.$html.addClass(s.opening),this.trigger("opening")},e[t].prototype.close=function(){if(this.vars.opened){var t=this;this.__transitionend(o.$page.first(),function(){t.$menu.removeClass(s.current).removeClass(s.opened),o.$html.removeClass(s.opened).removeClass(s.blocking).removeClass(s.modal).removeClass(s.background).removeClass(s.mm(t.opts[n].position)).removeClass(s.mm(t.opts[n].zposition)),t.opts.extensions&&o.$html.removeClass(t.opts.extensions),o.$page.each(function(){e(this).attr("style",e(this).data(i.style))}),t.vars.opened=!1,t.trigger("closed")},this.conf.transitionDuration),o.$html.removeClass(s.opening),this.trigger("close"),this.trigger("closing")}},e[t].prototype.closeAllOthers=function(){o.$allMenus.not(this.$menu).each(function(){var n=e(this).data(t);n&&n.close&&n.close()})},e[t].prototype.setPage=function(t){var i=this,a=this.conf[n];t&&t.length||(t=o.$body.find(a.pageSelector),a.noPageSelector.length&&(t=t.not(a.noPageSelector.join(", "))),t.length>1&&a.wrapPageIfNeeded&&(t=t.wrapAll("<"+this.conf[n].pageNodetype+" />").parent())),t.each(function(){e(this).attr("id",e(this).attr("id")||i.__getUniqueId())}),t.addClass(s.page+" "+s.slideout),o.$page=t,this.trigger("setPage",t)},e[t].prototype["_initWindow_"+n]=function(){o.$wndw.off(a.keydown+"-"+n).on(a.keydown+"-"+n,function(e){return o.$html.hasClass(s.opened)&&9==e.keyCode?(e.preventDefault(),!1):void 0});var e=0;o.$wndw.off(a.resize+"-"+n).on(a.resize+"-"+n,function(t,n){if(1==o.$page.length&&(n||o.$html.hasClass(s.opened))){var i=o.$wndw.height();(n||i!=e)&&(e=i,o.$page.css("minHeight",i))}})},e[t].prototype._initBlocker=function(){var t=this;this.opts[n].blockUI&&(o.$blck||(o.$blck=e('<div id="'+s.blocker+'" class="'+s.slideout+'" />')),o.$blck.appendTo(o.$body).off(a.touchstart+"-"+n+" "+a.touchmove+"-"+n).on(a.touchstart+"-"+n+" "+a.touchmove+"-"+n,function(e){e.preventDefault(),e.stopPropagation(),o.$blck.trigger(a.mousedown+"-"+n)}).off(a.mousedown+"-"+n).on(a.mousedown+"-"+n,function(e){e.preventDefault(),o.$html.hasClass(s.modal)||(t.closeAllOthers(),t.close())}))};var s,i,a,o}(jQuery),/*	
 * jQuery mmenu scrollBugFix addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){var t="mmenu",n="scrollBugFix";e[t].addons[n]={setup:function(){var i=this,r=this.opts[n];this.conf[n];if(o=e[t].glbl,e[t].support.touch&&this.opts.offCanvas&&this.opts.offCanvas.blockUI&&("boolean"==typeof r&&(r={fix:r}),"object"!=typeof r&&(r={}),r=this.opts[n]=e.extend(!0,{},e[t].defaults[n],r),r.fix)){var l=this.$menu.attr("id"),d=!1;this.bind("opening",function(){this.$pnls.children("."+s.current).scrollTop(0)}),o.$docu.on(a.touchmove,function(e){i.vars.opened&&e.preventDefault()}),o.$body.on(a.touchstart,"#"+l+"> ."+s.panels+"> ."+s.current,function(e){i.vars.opened&&(d||(d=!0,0===e.currentTarget.scrollTop?e.currentTarget.scrollTop=1:e.currentTarget.scrollHeight===e.currentTarget.scrollTop+e.currentTarget.offsetHeight&&(e.currentTarget.scrollTop-=1),d=!1))}).on(a.touchmove,"#"+l+"> ."+s.panels+"> ."+s.current,function(t){i.vars.opened&&e(this)[0].scrollHeight>e(this).innerHeight()&&t.stopPropagation()}),o.$wndw.on(a.orientationchange,function(){i.$pnls.children("."+s.current).scrollTop(0).css({"-webkit-overflow-scrolling":"auto"}).css({"-webkit-overflow-scrolling":"touch"})})}},add:function(){s=e[t]._c,i=e[t]._d,a=e[t]._e},clickAnchor:function(e,t){}},e[t].defaults[n]={fix:!0};var s,i,a,o}(jQuery),/*	
 * jQuery mmenu autoHeight addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){var t="mmenu",n="autoHeight";e[t].addons[n]={setup:function(){if(this.opts.offCanvas){var i=this.opts[n];this.conf[n];if(o=e[t].glbl,"boolean"==typeof i&&i&&(i={height:"auto"}),"string"==typeof i&&(i={height:i}),"object"!=typeof i&&(i={}),i=this.opts[n]=e.extend(!0,{},e[t].defaults[n],i),"auto"==i.height||"highest"==i.height){this.$menu.addClass(s.autoheight);var a=function(t){if(this.vars.opened){var n=parseInt(this.$pnls.css("top"),10)||0,a=parseInt(this.$pnls.css("bottom"),10)||0,o=0;this.$menu.addClass(s.measureheight),"auto"==i.height?(t=t||this.$pnls.children("."+s.current),t.is("."+s.vertical)&&(t=t.parents("."+s.panel).not("."+s.vertical).first()),o=t.outerHeight()):"highest"==i.height&&this.$pnls.children().each(function(){var t=e(this);t.is("."+s.vertical)&&(t=t.parents("."+s.panel).not("."+s.vertical).first()),o=Math.max(o,t.outerHeight())}),this.$menu.height(o+n+a).removeClass(s.measureheight)}};this.bind("opening",a),"highest"==i.height&&this.bind("init",a),"auto"==i.height&&(this.bind("update",a),this.bind("openPanel",a),this.bind("closePanel",a))}}},add:function(){s=e[t]._c,i=e[t]._d,a=e[t]._e,s.add("autoheight measureheight"),a.add("resize")},clickAnchor:function(e,t){}},e[t].defaults[n]={height:"default"};var s,i,a,o}(jQuery),/*	
 * jQuery mmenu backButton addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){var t="mmenu",n="backButton";e[t].addons[n]={setup:function(){if(this.opts.offCanvas){var i=this,a=this.opts[n];this.conf[n];if(o=e[t].glbl,"boolean"==typeof a&&(a={close:a}),"object"!=typeof a&&(a={}),a=e.extend(!0,{},e[t].defaults[n],a),a.close){var r="#"+i.$menu.attr("id");this.bind("opened",function(e){location.hash!=r&&history.pushState(null,document.title,r)}),e(window).on("popstate",function(e){o.$html.hasClass(s.opened)?(e.stopPropagation(),i.close()):location.hash==r&&(e.stopPropagation(),i.open())})}}},add:function(){return window.history&&window.history.pushState?(s=e[t]._c,i=e[t]._d,void(a=e[t]._e)):void(e[t].addons[n].setup=function(){})},clickAnchor:function(e,t){}},e[t].defaults[n]={close:!1};var s,i,a,o}(jQuery),/*	
 * jQuery mmenu columns addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){var t="mmenu",n="columns";e[t].addons[n]={setup:function(){var i=this.opts[n];this.conf[n];if(o=e[t].glbl,"boolean"==typeof i&&(i={add:i}),"number"==typeof i&&(i={add:!0,visible:i}),"object"!=typeof i&&(i={}),"number"==typeof i.visible&&(i.visible={min:i.visible,max:i.visible}),i=this.opts[n]=e.extend(!0,{},e[t].defaults[n],i),i.add){i.visible.min=Math.max(1,Math.min(6,i.visible.min)),i.visible.max=Math.max(i.visible.min,Math.min(6,i.visible.max)),this.$menu.addClass(s.columns);for(var a=this.opts.offCanvas?this.$menu.add(o.$html):this.$menu,r=[],l=0;l<=i.visible.max;l++)r.push(s.columns+"-"+l);r=r.join(" ");var d=function(e){u.call(this,this.$pnls.children("."+s.current)),i.hideNavbars&&e.removeClass(s.hasnavbar)},c=function(){var e=this.$pnls.children("."+s.panel).filter("."+s.opened).length;e=Math.min(i.visible.max,Math.max(i.visible.min,e)),a.removeClass(r).addClass(s.columns+"-"+e)},h=function(){this.opts.offCanvas&&o.$html.removeClass(r)},u=function(t){this.$pnls.children("."+s.panel).removeClass(r).filter("."+s.subopened).removeClass(s.hidden).add(t).slice(-i.visible.max).each(function(t){e(this).addClass(s.columns+"-"+t)})};this.bind("open",c),this.bind("close",h),this.bind("init",d),this.bind("openPanel",u),this.bind("openingPanel",c),this.bind("openedPanel",c),this.opts.offCanvas||c.call(this)}},add:function(){s=e[t]._c,i=e[t]._d,a=e[t]._e,s.add("columns")},clickAnchor:function(t,i){if(!this.opts[n].add)return!1;if(i){var a=t.attr("href");if(a.length>1&&"#"==a.slice(0,1))try{var o=e(a,this.$menu);if(o.is("."+s.panel))for(var r=parseInt(t.closest("."+s.panel).attr("class").split(s.columns+"-")[1].split(" ")[0],10)+1;r!==!1;){var l=this.$pnls.children("."+s.columns+"-"+r);if(!l.length){r=!1;break}r++,l.removeClass(s.subopened).removeClass(s.opened).removeClass(s.current).removeClass(s.highest).addClass(s.hidden)}}catch(d){}}}},e[t].defaults[n]={add:!1,visible:{min:1,max:3},hideNavbars:!1};var s,i,a,o}(jQuery),/*	
 * jQuery mmenu counters addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){var t="mmenu",n="counters";e[t].addons[n]={setup:function(){var a=this,r=this.opts[n];this.conf[n];o=e[t].glbl,"boolean"==typeof r&&(r={add:r,update:r}),"object"!=typeof r&&(r={}),r=this.opts[n]=e.extend(!0,{},e[t].defaults[n],r),this.bind("init",function(t){this.__refactorClass(e("em",t),this.conf.classNames[n].counter,"counter")}),r.add&&this.bind("init",function(t){var n;switch(r.addTo){case"panels":n=t;break;default:n=t.filter(r.addTo)}n.each(function(){var t=e(this).data(i.parent);t&&(t.children("em."+s.counter).length||t.prepend(e('<em class="'+s.counter+'" />')))})}),r.update&&this.bind("update",function(){this.$pnls.children("."+s.panel).each(function(){var t=e(this),n=t.data(i.parent);if(n){var o=n.children("em."+s.counter);o.length&&(t=t.children("."+s.listview),t.length&&o.html(a.__filterListItems(t.children()).length))}})})},add:function(){s=e[t]._c,i=e[t]._d,a=e[t]._e,s.add("counter search noresultsmsg")},clickAnchor:function(e,t){}},e[t].defaults[n]={add:!1,addTo:"panels",update:!1},e[t].configuration.classNames[n]={counter:"Counter"};var s,i,a,o}(jQuery),/*	
 * jQuery mmenu dividers addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){var t="mmenu",n="dividers";e[t].addons[n]={setup:function(){var i=this,r=this.opts[n];this.conf[n];if(o=e[t].glbl,"boolean"==typeof r&&(r={add:r,fixed:r}),"object"!=typeof r&&(r={}),r=this.opts[n]=e.extend(!0,{},e[t].defaults[n],r),this.bind("init",function(t){this.__refactorClass(e("li",this.$menu),this.conf.classNames[n].collapsed,"collapsed")}),r.add&&this.bind("init",function(t){var n;switch(r.addTo){case"panels":n=t;break;default:n=t.filter(r.addTo)}e("."+s.divider,n).remove(),n.find("."+s.listview).not("."+s.vertical).each(function(){var t="";i.__filterListItems(e(this).children()).each(function(){var n=e.trim(e(this).children("a, span").text()).slice(0,1).toLowerCase();n!=t&&n.length&&(t=n,e('<li class="'+s.divider+'">'+n+"</li>").insertBefore(this))})})}),r.collapse&&this.bind("init",function(t){e("."+s.divider,t).each(function(){var t=e(this),n=t.nextUntil("."+s.divider,"."+s.collapsed);n.length&&(t.children("."+s.subopen).length||(t.wrapInner("<span />"),t.prepend('<a href="#" class="'+s.subopen+" "+s.fullsubopen+'" />')))})}),r.fixed){var l=function(t){t=t||this.$pnls.children("."+s.current);var n=t.find("."+s.divider).not("."+s.hidden);if(n.length){this.$menu.addClass(s.hasdividers);var i=t.scrollTop()||0,a="";t.is(":visible")&&t.find("."+s.divider).not("."+s.hidden).each(function(){e(this).position().top+i<i+1&&(a=e(this).text())}),this.$fixeddivider.text(a)}else this.$menu.removeClass(s.hasdividers)};this.$fixeddivider=e('<ul class="'+s.listview+" "+s.fixeddivider+'"><li class="'+s.divider+'"></li></ul>').prependTo(this.$pnls).children(),this.bind("openPanel",l),this.bind("update",l),this.bind("init",function(t){t.off(a.scroll+"-dividers "+a.touchmove+"-dividers").on(a.scroll+"-dividers "+a.touchmove+"-dividers",function(t){l.call(i,e(this))})})}},add:function(){s=e[t]._c,i=e[t]._d,a=e[t]._e,s.add("collapsed uncollapsed fixeddivider hasdividers"),a.add("scroll")},clickAnchor:function(e,t){if(this.opts[n].collapse&&t){var i=e.parent();if(i.is("."+s.divider)){var a=i.nextUntil("."+s.divider,"."+s.collapsed);return i.toggleClass(s.opened),a[i.hasClass(s.opened)?"addClass":"removeClass"](s.uncollapsed),!0}}return!1}},e[t].defaults[n]={add:!1,addTo:"panels",fixed:!1,collapse:!1},e[t].configuration.classNames[n]={collapsed:"Collapsed"};var s,i,a,o}(jQuery),/*	
 * jQuery mmenu dragOpen addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){function t(e,t,n){return t>e&&(e=t),e>n&&(e=n),e}var n="mmenu",s="dragOpen";e[n].addons[s]={setup:function(){if(this.opts.offCanvas){var a=this,o=this.opts[s],l=this.conf[s];if(r=e[n].glbl,"boolean"==typeof o&&(o={open:o}),"object"!=typeof o&&(o={}),o=this.opts[s]=e.extend(!0,{},e[n].defaults[s],o),o.open){var d,c,h,u,p,f={},v=0,m=!1,g=!1,b=0,C=0;switch(this.opts.offCanvas.position){case"left":case"right":f.events="panleft panright",f.typeLower="x",f.typeUpper="X",g="width";break;case"top":case"bottom":f.events="panup pandown",f.typeLower="y",f.typeUpper="Y",g="height"}switch(this.opts.offCanvas.position){case"right":case"bottom":f.negative=!0,u=function(e){e>=r.$wndw[g]()-o.maxStartPos&&(v=1)};break;default:f.negative=!1,u=function(e){e<=o.maxStartPos&&(v=1)}}switch(this.opts.offCanvas.position){case"left":f.open_dir="right",f.close_dir="left";break;case"right":f.open_dir="left",f.close_dir="right";break;case"top":f.open_dir="down",f.close_dir="up";break;case"bottom":f.open_dir="up",f.close_dir="down"}switch(this.opts.offCanvas.zposition){case"front":p=function(){return this.$menu};break;default:p=function(){return e("."+i.slideout)}}var _=this.__valueOrFn(o.pageNode,this.$menu,r.$page);"string"==typeof _&&(_=e(_));var $=new Hammer(_[0],o.vendors.hammer);$.on("panstart",function(e){u(e.center[f.typeLower]),r.$slideOutNodes=p(),m=f.open_dir}).on(f.events+" panend",function(e){v>0&&e.preventDefault()}).on(f.events,function(e){if(d=e["delta"+f.typeUpper],f.negative&&(d=-d),d!=b&&(m=d>=b?f.open_dir:f.close_dir),b=d,b>o.threshold&&1==v){if(r.$html.hasClass(i.opened))return;v=2,a._openSetup(),a.trigger("opening"),r.$html.addClass(i.dragging),C=t(r.$wndw[g]()*l[g].perc,l[g].min,l[g].max)}2==v&&(c=t(b,10,C)-("front"==a.opts.offCanvas.zposition?C:0),f.negative&&(c=-c),h="translate"+f.typeUpper+"("+c+"px )",r.$slideOutNodes.css({"-webkit-transform":"-webkit-"+h,transform:h}))}).on("panend",function(e){2==v&&(r.$html.removeClass(i.dragging),r.$slideOutNodes.css("transform",""),a[m==f.open_dir?"_openFinish":"close"]()),v=0})}}},add:function(){return"function"!=typeof Hammer||Hammer.VERSION<2?void(e[n].addons[s].setup=function(){}):(i=e[n]._c,a=e[n]._d,o=e[n]._e,void i.add("dragging"))},clickAnchor:function(e,t){}},e[n].defaults[s]={open:!1,maxStartPos:100,threshold:50,vendors:{hammer:{}}},e[n].configuration[s]={width:{perc:.8,min:140,max:440},height:{perc:.8,min:140,max:880}};var i,a,o,r}(jQuery),/*	
 * jQuery mmenu dropdown addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){var t="mmenu",n="dropdown";e[t].addons[n]={setup:function(){if(this.opts.offCanvas){var r=this,l=this.opts[n],d=this.conf[n];if(o=e[t].glbl,"boolean"==typeof l&&l&&(l={drop:l}),"object"!=typeof l&&(l={}),"string"==typeof l.position&&(l.position={of:l.position}),l=this.opts[n]=e.extend(!0,{},e[t].defaults[n],l),l.drop){if("string"!=typeof l.position.of){var c=this.$menu.attr("id");c&&c.length&&(this.conf.clone&&(c=s.umm(c)),l.position.of='[href="#'+c+'"]')}if("string"==typeof l.position.of){var h=e(l.position.of);if(h.length){this.$menu.addClass(s.dropdown),l.tip&&this.$menu.addClass(s.tip),l.event=l.event.split(" "),1==l.event.length&&(l.event[1]=l.event[0]),"hover"==l.event[0]&&h.on(a.mouseenter+"-dropdown",function(){r.open()}),"hover"==l.event[1]&&this.$menu.on(a.mouseleave+"-dropdown",function(){r.close()}),this.bind("opening",function(){this.$menu.data(i.style,this.$menu.attr("style")||""),o.$html.addClass(s.dropdown)}),this.bind("closed",function(){this.$menu.attr("style",this.$menu.data(i.style)),o.$html.removeClass(s.dropdown)});var u=function(i,a){var r=a[0],c=a[1],u="x"==i?"scrollLeft":"scrollTop",p="x"==i?"outerWidth":"outerHeight",f="x"==i?"left":"top",v="x"==i?"right":"bottom",m="x"==i?"width":"height",g="x"==i?"maxWidth":"maxHeight",b=null,C=o.$wndw[u](),_=h.offset()[f]-=C,$=_+h[p](),y=o.$wndw[m](),x=d.offset.button[i]+d.offset.viewport[i];if(l.position[i])switch(l.position[i]){case"left":case"bottom":b="after";break;case"right":case"top":b="before"}null===b&&(b=y/2>_+($-_)/2?"after":"before");var w,k;return"after"==b?(w="x"==i?_:$,k=y-(w+x),r[f]=w+d.offset.button[i],r[v]="auto",c.push(s["x"==i?"tipleft":"tiptop"])):(w="x"==i?$:_,k=w-x,r[v]="calc( 100% - "+(w-d.offset.button[i])+"px )",r[f]="auto",c.push(s["x"==i?"tipright":"tipbottom"])),r[g]=Math.min(e[t].configuration[n][m].max,k),[r,c]},p=function(e){if(this.vars.opened){this.$menu.attr("style",this.$menu.data(i.style));var t=[{},[]];t=u.call(this,"y",t),t=u.call(this,"x",t),this.$menu.css(t[0]),l.tip&&this.$menu.removeClass(s.tipleft+" "+s.tipright+" "+s.tiptop+" "+s.tipbottom).addClass(t[1].join(" "))}};this.bind("opening",p),o.$wndw.on(a.resize+"-dropdown",function(e){p.call(r)}),this.opts.offCanvas.blockUI||o.$wndw.on(a.scroll+"-dropdown",function(e){p.call(r)})}}}}},add:function(){s=e[t]._c,i=e[t]._d,a=e[t]._e,s.add("dropdown tip tipleft tipright tiptop tipbottom"),a.add("mouseenter mouseleave resize scroll")},clickAnchor:function(e,t){}},e[t].defaults[n]={drop:!1,event:"click",position:{},tip:!0},e[t].configuration[n]={offset:{button:{x:-10,y:10},viewport:{x:20,y:20}},height:{max:880},width:{max:440}};var s,i,a,o}(jQuery),/*	
 * jQuery mmenu fixedElements addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){var t="mmenu",n="fixedElements";e[t].addons[n]={setup:function(){if(this.opts.offCanvas){var s=this.opts[n];this.conf[n];o=e[t].glbl,s=this.opts[n]=e.extend(!0,{},e[t].defaults[n],s);var i=function(e){var t=this.conf.classNames[n].fixed;this.__refactorClass(e.find("."+t),t,"slideout").appendTo(o.$body)};i.call(this,o.$page),this.bind("setPage",i)}},add:function(){s=e[t]._c,i=e[t]._d,a=e[t]._e,s.add("fixed")},clickAnchor:function(e,t){}},e[t].configuration.classNames[n]={fixed:"Fixed"};var s,i,a,o}(jQuery),/*	
 * jQuery mmenu iconPanels addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){var t="mmenu",n="iconPanels";e[t].addons[n]={setup:function(){var i=this,a=this.opts[n];this.conf[n];if(o=e[t].glbl,"boolean"==typeof a&&(a={add:a}),"number"==typeof a&&(a={add:!0,visible:a}),"object"!=typeof a&&(a={}),a=this.opts[n]=e.extend(!0,{},e[t].defaults[n],a),a.visible++,a.add){this.$menu.addClass(s.iconpanel);for(var r=[],l=0;l<=a.visible;l++)r.push(s.iconpanel+"-"+l);r=r.join(" ");var d=function(t){t.hasClass(s.vertical)||i.$pnls.children("."+s.panel).removeClass(r).filter("."+s.subopened).removeClass(s.hidden).add(t).not("."+s.vertical).slice(-a.visible).each(function(t){e(this).addClass(s.iconpanel+"-"+t)})};this.bind("openPanel",d),this.bind("init",function(t){d.call(i,i.$pnls.children("."+s.current)),a.hideNavbars&&t.removeClass(s.hasnavbar),t.not("."+s.vertical).each(function(){e(this).children("."+s.subblocker).length||e(this).prepend('<a href="#'+e(this).closest("."+s.panel).attr("id")+'" class="'+s.subblocker+'" />')})})}},add:function(){s=e[t]._c,i=e[t]._d,a=e[t]._e,s.add("iconpanel subblocker")},clickAnchor:function(e,t){}},e[t].defaults[n]={add:!1,visible:3,hideNavbars:!1};var s,i,a,o}(jQuery),/*	
 * jQuery mmenu navbar addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){var t="mmenu",n="navbars";e[t].addons[n]={setup:function(){var i=this,a=this.opts[n],r=this.conf[n];if(o=e[t].glbl,"undefined"!=typeof a){a instanceof Array||(a=[a]);var l={};e.each(a,function(o){var d=a[o];"boolean"==typeof d&&d&&(d={}),"object"!=typeof d&&(d={}),"undefined"==typeof d.content&&(d.content=["prev","title"]),d.content instanceof Array||(d.content=[d.content]),d=e.extend(!0,{},i.opts.navbar,d);var c=d.position,h=d.height;"number"!=typeof h&&(h=1),h=Math.min(4,Math.max(1,h)),"bottom"!=c&&(c="top"),l[c]||(l[c]=0),l[c]++;var u=e("<div />").addClass(s.navbar+" "+s.navbar+"-"+c+" "+s.navbar+"-"+c+"-"+l[c]+" "+s.navbar+"-size-"+h);l[c]+=h-1;for(var p=0,f=0,v=d.content.length;v>f;f++){var m=e[t].addons[n][d.content[f]]||!1;m?p+=m.call(i,u,d,r):(m=d.content[f],m instanceof e||(m=e(d.content[f])),u.append(m))}p+=Math.ceil(u.children().not("."+s.btn).not("."+s.title+"-prev").length/h),p>1&&u.addClass(s.navbar+"-content-"+p),u.children("."+s.btn).length&&u.addClass(s.hasbtns),u.prependTo(i.$menu)});for(var d in l)i.$menu.addClass(s.hasnavbar+"-"+d+"-"+l[d])}},add:function(){s=e[t]._c,i=e[t]._d,a=e[t]._e,s.add("close hasbtns")},clickAnchor:function(e,t){}},e[t].configuration[n]={breadcrumbSeparator:"/"},e[t].configuration.classNames[n]={};var s,i,a,o}(jQuery),/*	
 * jQuery mmenu navbar addon breadcrumbs content
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){var t="mmenu",n="navbars",s="breadcrumbs";e[t].addons[n][s]=function(n,s,i){var a=e[t]._c,o=e[t]._d;a.add("breadcrumbs separator");var r=e('<span class="'+a.breadcrumbs+'" />').appendTo(n);this.bind("init",function(t){t.removeClass(a.hasnavbar).each(function(){for(var t=[],n=e(this),s=e('<span class="'+a.breadcrumbs+'"></span>'),r=e(this).children().first(),l=!0;r&&r.length;){r.is("."+a.panel)||(r=r.closest("."+a.panel));var d=r.children("."+a.navbar).children("."+a.title).text();t.unshift(l?"<span>"+d+"</span>":'<a href="#'+r.attr("id")+'">'+d+"</a>"),l=!1,r=r.data(o.parent)}s.append(t.join('<span class="'+a.separator+'">'+i.breadcrumbSeparator+"</span>")).appendTo(n.children("."+a.navbar))})});var l=function(){r.html(this.$pnls.children("."+a.current).children("."+a.navbar).children("."+a.breadcrumbs).html())};return this.bind("openPanel",l),this.bind("init",l),0}}(jQuery),/*	
 * jQuery mmenu navbar addon close content
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){var t="mmenu",n="navbars",s="close";e[t].addons[n][s]=function(n,s){var i=e[t]._c,a=e[t].glbl,o=e('<a class="'+i.close+" "+i.btn+'" href="#" />').appendTo(n),r=function(e){o.attr("href","#"+e.attr("id"))};return r.call(this,a.$page),this.bind("setPage",r),-1}}(jQuery),/*	
 * jQuery mmenu navbar addon next content
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){var t="mmenu",n="navbars",s="next";e[t].addons[n][s]=function(s,i){var a,o,r=e[t]._c,l=e('<a class="'+r.next+" "+r.btn+'" href="#" />').appendTo(s),d=function(e){e=e||this.$pnls.children("."+r.current);var t=e.find("."+this.conf.classNames[n].panelNext);a=t.attr("href"),o=t.html(),l[a?"attr":"removeAttr"]("href",a),l[a||o?"removeClass":"addClass"](r.hidden),l.html(o)};return this.bind("openPanel",d),this.bind("init",function(){d.call(this)}),-1},e[t].configuration.classNames[n].panelNext="Next"}(jQuery),/*	
 * jQuery mmenu navbar addon prev content
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){var t="mmenu",n="navbars",s="prev";e[t].addons[n][s]=function(s,i){var a=e[t]._c,o=e('<a class="'+a.prev+" "+a.btn+'" href="#" />').appendTo(s);this.bind("init",function(e){e.removeClass(a.hasnavbar).children("."+a.navbar).addClass(a.hidden)});var r,l,d=function(e){if(e=e||this.$pnls.children("."+a.current),!e.hasClass(a.vertical)){var t=e.find("."+this.conf.classNames[n].panelPrev);t.length||(t=e.children("."+a.navbar).children("."+a.prev)),r=t.attr("href"),l=t.html(),o[r?"attr":"removeAttr"]("href",r),o[r||l?"removeClass":"addClass"](a.hidden),o.html(l)}};return this.bind("openPanel",d),this.bind("init",function(){d.call(this)}),-1},e[t].configuration.classNames[n].panelPrev="Prev"}(jQuery),/*	
 * jQuery mmenu navbar addon searchfield content
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){var t="mmenu",n="navbars",s="searchfield";e[t].addons[n][s]=function(n,s){var i=e[t]._c,a=e('<div class="'+i.search+'" />').appendTo(n);return"object"!=typeof this.opts.searchfield&&(this.opts.searchfield={}),this.opts.searchfield.add=!0,this.opts.searchfield.addTo=a,0}}(jQuery),/*	
 * jQuery mmenu navbar addon title content
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){var t="mmenu",n="navbars",s="title";e[t].addons[n][s]=function(s,i){var a,o,r=e[t]._c,l=e('<a class="'+r.title+'" />').appendTo(s),d=function(e){if(e=e||this.$pnls.children("."+r.current),!e.hasClass(r.vertical)){var t=e.find("."+this.conf.classNames[n].panelTitle);t.length||(t=e.children("."+r.navbar).children("."+r.title)),a=t.attr("href"),o=t.html()||i.title,l[a?"attr":"removeAttr"]("href",a),l[a||o?"removeClass":"addClass"](r.hidden),l.html(o)}};return this.bind("openPanel",d),this.bind("init",function(e){d.call(this)}),0},e[t].configuration.classNames[n].panelTitle="Title"}(jQuery),/*	
 * jQuery mmenu screenReader addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){function t(e,t,n){e.prop("aria-"+t,n)[n?"attr":"removeAttr"]("aria-"+t,"true")}function n(e){return'<span class="'+a.sronly+'">'+e+"</span>"}var s="mmenu",i="screenReader";e[s].addons[i]={setup:function(){var o=this.opts[i],r=this.conf[i];if(l=e[s].glbl,"boolean"==typeof o&&(o={aria:o,text:o}),"object"!=typeof o&&(o={}),o=this.opts[i]=e.extend(!0,{},e[s].defaults[i],o),o.aria){if(this.opts.offCanvas){var d=function(){t(this.$menu,"hidden",!1)},c=function(){t(this.$menu,"hidden",!0)};this.bind("open",d),this.bind("close",c),c.call(this)}var h=function(){t(this.$menu.find("."+a.hidden),"hidden",!0),t(this.$menu.find('[aria-hidden="true"]').not("."+a.hidden),"hidden",!1)},u=function(e){t(this.$pnls.children("."+a.panel).not(e).not("."+a.hidden),"hidden",!0),t(e,"hidden",!1)};this.bind("update",h),this.bind("openPanel",h),this.bind("openPanel",u);var p=function(e){t(e.find("."+a.prev+", ."+a.next),"haspopup",!0)};this.bind("init",p),p.call(this,this.$menu.children("."+a.navbar))}if(o.text){var f=function(t){t.children("."+a.navbar).children("."+a.prev).html(n(r.text.closeSubmenu)).end().children("."+a.next).html(n(r.text.openSubmenu)).end().children("."+a.close).html(n(r.text.closeMenu)),t.is("."+a.panel)&&t.find("."+a.listview).find("."+a.next).each(function(){e(this).html(n(r.text[e(this).parent().is("."+a.vertical)?"toggleSubmenu":"openSubmenu"]))})};this.bind("init",f),f.call(this,this.$menu)}},add:function(){a=e[s]._c,o=e[s]._d,r=e[s]._e,a.add("sronly")},clickAnchor:function(e,t){}},e[s].defaults[i]={aria:!1,text:!1},e[s].configuration[i]={text:{closeMenu:"Close menu",closeSubmenu:"Close submenu",openSubmenu:"Open submenu",toggleSubmenu:"Toggle submenu"}};var a,o,r,l}(jQuery),/*	
 * jQuery mmenu searchfield addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){function t(e){switch(e){case 9:case 16:case 17:case 18:case 37:case 38:case 39:case 40:return!0}return!1}var n="mmenu",s="searchfield";e[n].addons[s]={setup:function(){var l=this,d=this.opts[s],c=this.conf[s];r=e[n].glbl,"boolean"==typeof d&&(d={add:d}),"object"!=typeof d&&(d={}),"boolean"==typeof d.resultsPanel&&(d.resultsPanel={add:d.resultsPanel}),d=this.opts[s]=e.extend(!0,{},e[n].defaults[s],d),c=this.conf[s]=e.extend(!0,{},e[n].configuration[s],c),this.bind("close",function(){this.$menu.find("."+i.search).find("input").blur()}),this.bind("init",function(n){if(d.add){var r;switch(d.addTo){case"panels":r=n;break;default:r=this.$menu.find(d.addTo)}if(r.each(function(){var t=e(this);if(!t.is("."+i.panel)||!t.is("."+i.vertical)){if(!t.children("."+i.search).length){var n=l.__valueOrFn(c.clear,t),s=l.__valueOrFn(c.form,t),a=l.__valueOrFn(c.input,t),r=l.__valueOrFn(c.submit,t),h=e("<"+(s?"form":"div")+' class="'+i.search+'" />'),u=e('<input placeholder="'+d.placeholder+'" type="text" autocomplete="off" />');h.append(u);var p;if(a)for(p in a)u.attr(p,a[p]);if(n&&e('<a class="'+i.btn+" "+i.clear+'" href="#" />').appendTo(h).on(o.click+"-searchfield",function(e){e.preventDefault(),u.val("").trigger(o.keyup+"-searchfield")}),s){for(p in s)h.attr(p,s[p]);r&&!n&&e('<a class="'+i.btn+" "+i.next+'" href="#" />').appendTo(h).on(o.click+"-searchfield",function(e){e.preventDefault(),h.submit()})}t.hasClass(i.search)?t.replaceWith(h):t.prepend(h).addClass(i.hassearch)}if(d.noResults){var f=t.closest("."+i.panel).length;if(f||(t=l.$pnls.children("."+i.panel).first()),!t.children("."+i.noresultsmsg).length){var v=t.children("."+i.listview).first();e('<div class="'+i.noresultsmsg+" "+i.hidden+'" />').append(d.noResults)[v.length?"insertAfter":"prependTo"](v.length?v:t)}}}}),d.search){if(d.resultsPanel.add){d.showSubPanels=!1;var h=this.$pnls.children("."+i.resultspanel);h.length||(h=e('<div class="'+i.panel+" "+i.resultspanel+" "+i.hidden+'" />').appendTo(this.$pnls).append('<div class="'+i.navbar+" "+i.hidden+'"><a class="'+i.title+'">'+d.resultsPanel.title+"</a></div>").append('<ul class="'+i.listview+'" />').append(this.$pnls.find("."+i.noresultsmsg).first().clone()),this.init(h))}this.$menu.find("."+i.search).each(function(){var n,r,c=e(this),u=c.closest("."+i.panel).length;u?(n=c.closest("."+i.panel),r=n):(n=e("."+i.panel,l.$menu),r=l.$menu),d.resultsPanel.add&&(n=n.not(h));var p=c.children("input"),f=l.__findAddBack(n,"."+i.listview).children("li"),v=f.filter("."+i.divider),m=l.__filterListItems(f),g="a",b=g+", span",C="",_=function(){var t=p.val().toLowerCase();if(t!=C){if(C=t,d.resultsPanel.add&&h.children("."+i.listview).empty(),n.scrollTop(0),m.add(v).addClass(i.hidden).find("."+i.fullsubopensearch).removeClass(i.fullsubopen+" "+i.fullsubopensearch),m.each(function(){var t=e(this),n=g;(d.showTextItems||d.showSubPanels&&t.find("."+i.next))&&(n=b);var s=t.data(a.searchtext)||t.children(n).text();s.toLowerCase().indexOf(C)>-1&&t.add(t.prevAll("."+i.divider).first()).removeClass(i.hidden)}),d.showSubPanels&&n.each(function(t){var n=e(this);l.__filterListItems(n.find("."+i.listview).children()).each(function(){var t=e(this),n=t.data(a.sub);t.removeClass(i.nosubresults),n&&n.find("."+i.listview).children().removeClass(i.hidden)})}),d.resultsPanel.add)if(""===C)this.closeAllPanels(),this.openPanel(this.$pnls.children("."+i.subopened).last());else{var s=e();n.each(function(){var t=l.__filterListItems(e(this).find("."+i.listview).children()).not("."+i.hidden).clone(!0);t.length&&(d.resultsPanel.dividers&&(s=s.add('<li class="'+i.divider+'">'+e(this).children("."+i.navbar).text()+"</li>")),s=s.add(t))}),s.find("."+i.next).remove(),h.children("."+i.listview).append(s),this.openPanel(h)}else e(n.get().reverse()).each(function(t){var n=e(this),s=n.data(a.parent);s&&(l.__filterListItems(n.find("."+i.listview).children()).length?(s.hasClass(i.hidden)&&s.children("."+i.next).not("."+i.fullsubopen).addClass(i.fullsubopen).addClass(i.fullsubopensearch),s.removeClass(i.hidden).removeClass(i.nosubresults).prevAll("."+i.divider).first().removeClass(i.hidden)):u||(n.hasClass(i.opened)&&setTimeout(function(){l.openPanel(s.closest("."+i.panel))},(t+1)*(1.5*l.conf.openingInterval)),s.addClass(i.nosubresults)))});r.find("."+i.noresultsmsg)[m.not("."+i.hidden).length?"addClass":"removeClass"](i.hidden),this.update()}};p.off(o.keyup+"-"+s+" "+o.change+"-"+s).on(o.keyup+"-"+s,function(e){t(e.keyCode)||_.call(l)}).on(o.change+"-"+s,function(e){_.call(l)});var $=c.children("."+i.btn);$.length&&p.on(o.keyup+"-"+s,function(e){$[p.val().length?"removeClass":"addClass"](i.hidden)}),p.trigger(o.keyup+"-"+s)})}}})},add:function(){i=e[n]._c,a=e[n]._d,o=e[n]._e,i.add("clear search hassearch resultspanel noresultsmsg noresults nosubresults fullsubopensearch"),a.add("searchtext"),o.add("change keyup")},clickAnchor:function(e,t){}},e[n].defaults[s]={add:!1,addTo:"panels",placeholder:"Search",noResults:"No results found.",resultsPanel:{add:!1,dividers:!0,title:"Search results"},search:!0,showTextItems:!1,showSubPanels:!0},e[n].configuration[s]={clear:!1,form:!1,input:!1,submit:!1};var i,a,o,r}(jQuery),/*	
 * jQuery mmenu sectionIndexer addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){var t="mmenu",n="sectionIndexer";e[t].addons[n]={setup:function(){var i=this,r=this.opts[n];this.conf[n];o=e[t].glbl,"boolean"==typeof r&&(r={add:r}),"object"!=typeof r&&(r={}),r=this.opts[n]=e.extend(!0,{},e[t].defaults[n],r),this.bind("init",function(t){if(r.add){var n;switch(r.addTo){case"panels":n=t;break;default:n=e(r.addTo,this.$menu).filter("."+s.panel)}n.find("."+s.divider).closest("."+s.panel).addClass(s.hasindexer)}if(!this.$indexer&&this.$pnls.children("."+s.hasindexer).length){this.$indexer=e('<div class="'+s.indexer+'" />').prependTo(this.$pnls).append('<a href="#a">a</a><a href="#b">b</a><a href="#c">c</a><a href="#d">d</a><a href="#e">e</a><a href="#f">f</a><a href="#g">g</a><a href="#h">h</a><a href="#i">i</a><a href="#j">j</a><a href="#k">k</a><a href="#l">l</a><a href="#m">m</a><a href="#n">n</a><a href="#o">o</a><a href="#p">p</a><a href="#q">q</a><a href="#r">r</a><a href="#s">s</a><a href="#t">t</a><a href="#u">u</a><a href="#v">v</a><a href="#w">w</a><a href="#x">x</a><a href="#y">y</a><a href="#z">z</a>'),this.$indexer.children().on(a.mouseover+"-sectionindexer "+s.touchstart+"-sectionindexer",function(t){var n=e(this).attr("href").slice(1),a=i.$pnls.children("."+s.current),o=a.find("."+s.listview),r=!1,l=a.scrollTop();a.scrollTop(0),o.children("."+s.divider).not("."+s.hidden).each(function(){r===!1&&n==e(this).text().slice(0,1).toLowerCase()&&(r=e(this).position().top)}),a.scrollTop(r!==!1?r:l)});var o=function(e){i.$menu[(e.hasClass(s.hasindexer)?"add":"remove")+"Class"](s.hasindexer)};this.bind("openPanel",o),o.call(this,this.$pnls.children("."+s.current))}})},add:function(){s=e[t]._c,i=e[t]._d,a=e[t]._e,s.add("indexer hasindexer"),a.add("mouseover touchstart")},clickAnchor:function(e,t){return e.parent().is("."+s.indexer)?!0:void 0}},e[t].defaults[n]={add:!1,addTo:"panels"};var s,i,a,o}(jQuery),/*	
 * jQuery mmenu setSelected addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){var t="mmenu",n="setSelected";e[t].addons[n]={setup:function(){var a=this.opts[n];this.conf[n];if(o=e[t].glbl,"boolean"==typeof a&&(a={hover:a,parent:a}),"object"!=typeof a&&(a={}),a=this.opts[n]=e.extend(!0,{},e[t].defaults[n],a),a.current||this.bind("init",function(e){e.find("."+s.listview).children("."+s.selected).removeClass(s.selected)}),a.hover&&this.$menu.addClass(s.hoverselected),a.parent){this.$menu.addClass(s.parentselected);var r=function(e){this.$pnls.find("."+s.listview).find("."+s.next).removeClass(s.selected);for(var t=e.data(i.parent);t&&t.length;)t=t.children("."+s.next).addClass(s.selected).closest("."+s.panel).data(i.parent)};this.bind("openedPanel",r),this.bind("init",function(e){r.call(this,this.$pnls.children("."+s.current))})}},add:function(){s=e[t]._c,i=e[t]._d,a=e[t]._e,s.add("hoverselected parentselected")},clickAnchor:function(e,t){}},e[t].defaults[n]={current:!0,hover:!1,parent:!1};var s,i,a,o}(jQuery),/*	
 * jQuery mmenu toggles addon
 * mmenu.frebsite.nl
 *
 * Copyright (c) Fred Heusschen
 */
function(e){var t="mmenu",n="toggles";e[t].addons[n]={setup:function(){var i=this;this.opts[n],this.conf[n];o=e[t].glbl,this.bind("init",function(t){this.__refactorClass(e("input",t),this.conf.classNames[n].toggle,"toggle"),this.__refactorClass(e("input",t),this.conf.classNames[n].check,"check"),e("input."+s.toggle+", input."+s.check,t).each(function(){var t=e(this),n=t.closest("li"),a=t.hasClass(s.toggle)?"toggle":"check",o=t.attr("id")||i.__getUniqueId();n.children('label[for="'+o+'"]').length||(t.attr("id",o),n.prepend(t),e('<label for="'+o+'" class="'+s[a]+'"></label>').insertBefore(n.children("a, span").last()))})})},add:function(){s=e[t]._c,i=e[t]._d,a=e[t]._e,s.add("toggle check")},clickAnchor:function(e,t){}},e[t].configuration.classNames[n]={toggle:"Toggle",check:"Check"};var s,i,a,o}(jQuery);
/*___________________________________________________________________________________________________________________________________________________
 _ jquery.mb.components                                                                                                                             _
 _                                                                                                                                                  _
 _ file: jquery.mb.YTPlayer.js                                                                                                                      _
 _ last modified: 16/05/15 21.29                                                                                                                    _
 _                                                                                                                                                  _
 _ Open Lab s.r.l., Florence - Italy                                                                                                                _
 _                                                                                                                                                  _
 _ email: matteo@open-lab.com                                                                                                                       _
 _ site: http://pupunzi.com                                                                                                                         _
 _       http://open-lab.com                                                                                                                        _
 _ blog: http://pupunzi.open-lab.com                                                                                                                _
 _ Q&A:  http://jquery.pupunzi.com                                                                                                                  _
 _                                                                                                                                                  _
 _ Licences: MIT, GPL                                                                                                                               _
 _    http://www.opensource.org/licenses/mit-license.php                                                                                            _
 _    http://www.gnu.org/licenses/gpl.html                                                                                                          _
 _                                                                                                                                                  _
 _ Copyright (c) 2001-2015. Matteo Bicocchi (Pupunzi);                                                                                              _
 ___________________________________________________________________________________________________________________________________________________*/
var ytp = ytp || {};

function onYouTubeIframeAPIReady() {
		if( ytp.YTAPIReady ) return;
		ytp.YTAPIReady = true;
		jQuery( document ).trigger( "YTAPIReady" );
}
( function( jQuery, ytp ) {
		var getYTPVideoID = function( url ) {
				var videoID, playlistID;
				if( url.indexOf( "youtu.be" ) > 0 ) {
						videoID = url.substr( url.lastIndexOf( "/" ) + 1, url.length );
						playlistID = videoID.indexOf( "?list=" ) > 0 ? videoID.substr( videoID.lastIndexOf( "=" ), videoID.length ) : null;
						videoID = playlistID ? videoID.substr( 0, videoID.lastIndexOf( "?" ) ) : videoID;
				} else if( url.indexOf( "http" ) > -1 ) {
						videoID = url.match( /[\\?&]v=([^&#]*)/ )[ 1 ];
						playlistID = url.indexOf( "list=" ) > 0 ? url.match( /[\\?&]list=([^&#]*)/ )[ 1 ] : null;
				} else {
						videoID = url.length > 15 ? null : url;
						playlistID = videoID ? null : url;
				}
				return {
						videoID: videoID,
						playlistID: playlistID
				};
		};
		/* todo: setPlaybackRate() playlist */
		jQuery.mbYTPlayer = {
				name: "jquery.mb.YTPlayer",
				version: "2.9.3",
				author: "Matteo Bicocchi",
				apiKey: "",
				defaults: {
						containment: "body",
						ratio: "auto", // "auto", "16/9", "4/3"
						videoURL: null,
						playlistURL: null,
						startAt: 0,
						stopAt: 0,
						autoPlay: true,
						vol: 50, // 1 to 100
						addRaster: false,
						opacity: 1,
						quality: "default", //or “small”, “medium”, “large”, “hd720”, “hd1080”, “highres”
						mute: false,
						loop: true,
						showControls: true,
						showAnnotations: false,
						showYTLogo: true,
						stopMovieOnBlur: true,
						realfullscreen: true,
						gaTrack: true,
						optimizeDisplay: true,
						onReady: function( player ) {}
				},
				/* @fontface icons */
				controls: {
						play: "P",
						pause: "p",
						mute: "M",
						unmute: "A",
						onlyYT: "O",
						showSite: "R",
						ytLogo: "Y"
				},
				locationProtocol: "https:",
				/**
				 *
				 * @param options
				 * @returns {*}
				 */
				buildPlayer: function( options ) {
						return this.each( function() {
								var YTPlayer = this;
								var $YTPlayer = jQuery( YTPlayer );
								YTPlayer.loop = 0;
								YTPlayer.opt = {};
								YTPlayer.state = {};
								YTPlayer.filtersEnabled = true;
								YTPlayer.filters = {
										grayscale: {
												value: 0,
												unit: "%"
										},
										hue_rotate: {
												value: 0,
												unit: "deg"
										},
										invert: {
												value: 0,
												unit: "%"
										},
										opacity: {
												value: 0,
												unit: "%"
										},
										saturate: {
												value: 0,
												unit: "%"
										},
										sepia: {
												value: 0,
												unit: "%"
										},
										brightness: {
												value: 0,
												unit: "%"
										},
										contrast: {
												value: 0,
												unit: "%"
										},
										blur: {
												value: 0,
												unit: "px"
										}
								};
								$YTPlayer.addClass( "mb_YTPlayer" );
								var property = $YTPlayer.data( "property" ) && typeof $YTPlayer.data( "property" ) == "string" ? eval( '(' + $YTPlayer.data( "property" ) + ')' ) : $YTPlayer.data( "property" );
								if( typeof property != "undefined" && typeof property.vol != "undefined" ) property.vol = property.vol === 0 ? property.vol = 1 : property.vol;
								jQuery.extend( YTPlayer.opt, jQuery.mbYTPlayer.defaults, options, property );
								if( !YTPlayer.hasChanged ) {
										YTPlayer.defaultOpt = {};
										jQuery.extend( YTPlayer.defaultOpt, jQuery.mbYTPlayer.defaults, options, property );
								}
								YTPlayer.isRetina = ( window.retina || window.devicePixelRatio > 1 );
								var isIframe = function() {
										var isIfr = false;
										try {
												if( self.location.href != top.location.href ) isIfr = true;
										} catch( e ) {
												isIfr = true;
										}
										return isIfr;
								};
								YTPlayer.canGoFullScreen = !( jQuery.browser.msie || jQuery.browser.opera || isIframe() );
								if( !YTPlayer.canGoFullScreen ) YTPlayer.opt.realfullscreen = false;
								if( !$YTPlayer.attr( "id" ) ) $YTPlayer.attr( "id", "video_" + new Date().getTime() );
								var playerID = "mbYTP_" + YTPlayer.id;
								YTPlayer.isAlone = false;
								YTPlayer.hasFocus = true;
								var videoID = this.opt.videoURL ? getYTPVideoID( this.opt.videoURL ).videoID : $YTPlayer.attr( "href" ) ? getYTPVideoID( $YTPlayer.attr( "href" ) ).videoID : false;
								var playlistID = this.opt.videoURL ? getYTPVideoID( this.opt.videoURL ).playlistID : $YTPlayer.attr( "href" ) ? getYTPVideoID( $YTPlayer.attr( "href" ) ).playlistID : false;
								YTPlayer.videoID = videoID;
								YTPlayer.playlistID = playlistID;
								YTPlayer.opt.showAnnotations = ( YTPlayer.opt.showAnnotations ) ? '0' : '3';
								var playerVars = {
										'autoplay': 0,
										'modestbranding': 1,
										'controls': 0,
										'showinfo': 0,
										'rel': 0,
										'enablejsapi': 1,
										'version': 3,
										'playerapiid': playerID,
										'origin': '*',
										'allowfullscreen': true,
										'wmode': 'transparent',
										'iv_load_policy': YTPlayer.opt.showAnnotations
								};
								if( document.createElement( 'video' ).canPlayType ) jQuery.extend( playerVars, {
										'html5': 1
								} );
								if( jQuery.browser.msie && jQuery.browser.version < 9 ) this.opt.opacity = 1;
								var playerBox = jQuery( "<div/>" ).attr( "id", playerID ).addClass( "playerBox" );
								var overlay = jQuery( "<div/>" ).css( {
										position: "absolute",
										top: 0,
										left: 0,
										width: "100%",
										height: "100%"
								} ).addClass( "YTPOverlay" );
								YTPlayer.isSelf = YTPlayer.opt.containment == "self";
								YTPlayer.defaultOpt.containment = YTPlayer.opt.containment = YTPlayer.opt.containment == "self" ? jQuery( this ) : jQuery( YTPlayer.opt.containment );
								YTPlayer.isBackground = YTPlayer.opt.containment.get( 0 ).tagName.toLowerCase() == "body";
								if( YTPlayer.isBackground && ytp.backgroundIsInited ) return;
								var isPlayer = YTPlayer.opt.containment.is( jQuery( this ) );
								YTPlayer.canPlayOnMobile = isPlayer && jQuery( this ).children().length === 0;
								if( !isPlayer ) {
										$YTPlayer.hide();
								} else {
										YTPlayer.isPlayer = true;
								}
								if( jQuery.browser.mobile && !YTPlayer.canPlayOnMobile ) {
										$YTPlayer.remove();
										return;
								}
								var wrapper = jQuery( "<div/>" ).addClass( "mbYTP_wrapper" ).attr( "id", "wrapper_" + playerID );
								wrapper.css( {
										position: "absolute",
										zIndex: 0,
										minWidth: "100%",
										minHeight: "100%",
										left: 0,
										top: 0,
										overflow: "hidden",
										opacity: 0
								} );
								playerBox.css( {
										position: "absolute",
										zIndex: 0,
										width: "100%",
										height: "100%",
										top: 0,
										left: 0,
										overflow: "hidden"
								} );
								wrapper.append( playerBox );
								YTPlayer.opt.containment.children().not( "script, style" ).each( function() {
										if( jQuery( this ).css( "position" ) == "static" ) jQuery( this ).css( "position", "relative" );
								} );
								if( YTPlayer.isBackground ) {
										jQuery( "body" ).css( {
												boxSizing: "border-box"
										} );
										wrapper.css( {
												position: "fixed",
												top: 0,
												left: 0,
												zIndex: 0
										} );
										$YTPlayer.hide();
								} else if( YTPlayer.opt.containment.css( "position" ) == "static" ) YTPlayer.opt.containment.css( {
										position: "relative"
								} );
								YTPlayer.opt.containment.prepend( wrapper );
								YTPlayer.wrapper = wrapper;
								playerBox.css( {
										opacity: 1
								} );
								if( !jQuery.browser.mobile ) {
										playerBox.after( overlay );
										YTPlayer.overlay = overlay;
								}
								if( !YTPlayer.isBackground ) {
										overlay.on( "mouseenter", function() {
												if( YTPlayer.controlBar ) YTPlayer.controlBar.addClass( "visible" );
										} ).on( "mouseleave", function() {
												if( YTPlayer.controlBar ) YTPlayer.controlBar.removeClass( "visible" );
										} )
								}
								if( !ytp.YTAPIReady ) {
										jQuery( "#YTAPI" ).remove();
										var tag = jQuery( "<script></script>" ).attr( {
												"src": jQuery.mbYTPlayer.locationProtocol + "//www.youtube.com/iframe_api?v=" + jQuery.mbYTPlayer.version,
												"id": "YTAPI"
										} );
										jQuery( "head" ).prepend( tag );
								} else {
										setTimeout( function() {
												jQuery( document ).trigger( "YTAPIReady" );
										}, 100 )
								}
								jQuery( document ).on( "YTAPIReady", function() {
										if( ( YTPlayer.isBackground && ytp.backgroundIsInited ) || YTPlayer.isInit ) return;
										if( YTPlayer.isBackground ) {
												ytp.backgroundIsInited = true;
										}
										YTPlayer.opt.autoPlay = typeof YTPlayer.opt.autoPlay == "undefined" ? ( YTPlayer.isBackground ? true : false ) : YTPlayer.opt.autoPlay;
										YTPlayer.opt.vol = YTPlayer.opt.vol ? YTPlayer.opt.vol : 100;
										jQuery.mbYTPlayer.getDataFromAPI( YTPlayer );
										jQuery( YTPlayer ).on( "YTPChanged", function() {
												if( YTPlayer.isInit ) return;
												YTPlayer.isInit = true;
												//if is mobile && isPlayer fallback to the default YT player
												if( jQuery.browser.mobile && YTPlayer.canPlayOnMobile ) {
														// Try to adjust the player dimention
														if( YTPlayer.opt.containment.outerWidth() > jQuery( window ).width() ) {
																YTPlayer.opt.containment.css( {
																		maxWidth: "100%"
																} );
																var h = YTPlayer.opt.containment.outerWidth() * .6;
																YTPlayer.opt.containment.css( {
																		maxHeight: h
																} );
														}
														new YT.Player( playerID, {
																videoId: YTPlayer.videoID.toString(),
																height: '100%',
																width: '100%',
																events: {
																		'onReady': function( event ) {
																				YTPlayer.player = event.target;
																				playerBox.css( {
																						opacity: 1
																				} );
																				YTPlayer.wrapper.css( {
																						opacity: 1
																				} );
																		}
																}
														} );
														return;
												}
												new YT.Player( playerID, {
														videoId: YTPlayer.videoID.toString(),
														playerVars: playerVars,
														events: {
																'onReady': function( event ) {
																		YTPlayer.player = event.target;
																		if( YTPlayer.isReady ) return;
																		YTPlayer.isReady = YTPlayer.isPlayer && !YTPlayer.opt.autoPlay ? false : true;
																		YTPlayer.playerEl = YTPlayer.player.getIframe();
																		$YTPlayer.optimizeDisplay();
																		YTPlayer.videoID = videoID;
																		jQuery( window ).on( "resize.YTP", function() {
																				$YTPlayer.optimizeDisplay();
																		} );
																		jQuery.mbYTPlayer.checkForState( YTPlayer );
																		// Trigger state events
																		var YTPEvent = jQuery.Event( "YTPUnstarted" );
																		YTPEvent.time = YTPlayer.player.time;
																		if( YTPlayer.canTrigger ) jQuery( YTPlayer ).trigger( YTPEvent );
																},
																/**
																 *
																 * @param event
																 *
																 * -1 (unstarted)
																 * 0 (ended)
																 * 1 (playing)
																 * 2 (paused)
																 * 3 (buffering)
																 * 5 (video cued).
																 *
																 *
																 */
																'onStateChange': function( event ) {
																		if( typeof event.target.getPlayerState != "function" ) return;
																		var state = event.target.getPlayerState();
																		if( YTPlayer.state == state ) return;
																		YTPlayer.state = state;
																		var eventType;
																		switch( state ) {
																				case -1: //------------------------------------------------ unstarted
																						eventType = "YTPUnstarted";
																						break;
																				case 0: //------------------------------------------------ ended
																						eventType = "YTPEnd";
																						break;
																				case 1: //------------------------------------------------ play
																						eventType = "YTPStart";
																						if( YTPlayer.controlBar ) YTPlayer.controlBar.find( ".mb_YTPPlaypause" ).html( jQuery.mbYTPlayer.controls.pause );
																						if( typeof _gaq != "undefined" && eval( YTPlayer.opt.gaTrack ) ) _gaq.push( [ '_trackEvent', 'YTPlayer', 'Play', ( YTPlayer.hasData ? YTPlayer.videoData.title : YTPlayer.videoID.toString() ) ] );
																						if( typeof ga != "undefined" && eval( YTPlayer.opt.gaTrack ) ) ga( 'send', 'event', 'YTPlayer', 'play', ( YTPlayer.hasData ? YTPlayer.videoData.title : YTPlayer.videoID.toString() ) );
																						break;
																				case 2: //------------------------------------------------ pause
																						eventType = "YTPPause";
																						if( YTPlayer.controlBar ) YTPlayer.controlBar.find( ".mb_YTPPlaypause" ).html( jQuery.mbYTPlayer.controls.play );
																						break;
																				case 3: //------------------------------------------------ buffer
																						YTPlayer.player.setPlaybackQuality( YTPlayer.opt.quality );
																						eventType = "YTPBuffering";
																						if( YTPlayer.controlBar ) YTPlayer.controlBar.find( ".mb_YTPPlaypause" ).html( jQuery.mbYTPlayer.controls.play );
																						break;
																				case 5: //------------------------------------------------ cued
																						eventType = "YTPCued";
																						break;
																				default:
																						break;
																		}
																		// Trigger state events
																		var YTPEvent = jQuery.Event( eventType );
																		YTPEvent.time = YTPlayer.player.time;
																		if( YTPlayer.canTrigger ) jQuery( YTPlayer ).trigger( YTPEvent );
																},
																/**
																 *
																 * @param e
																 */
																'onPlaybackQualityChange': function( e ) {
																		var quality = e.target.getPlaybackQuality();
																		var YTPQualityChange = jQuery.Event( "YTPQualityChange" );
																		YTPQualityChange.quality = quality;
																		jQuery( YTPlayer ).trigger( YTPQualityChange );
																},
																/**
																 *
																 * @param err
																 */
																'onError': function( err ) {
																		if( err.data == 150 ) {
																				console.log( "Embedding this video is restricted by Youtube." );
																				if( YTPlayer.isPlayList ) jQuery( YTPlayer ).playNext();
																		}
																		if( err.data == 2 && YTPlayer.isPlayList ) jQuery( YTPlayer ).playNext();
																		if( typeof YTPlayer.opt.onError == "function" ) YTPlayer.opt.onError( $YTPlayer, err );
																}
														}
												} );
										} );
								} )
						} );
				},
				/**
				 *
				 * @param YTPlayer
				 */
				getDataFromAPI: function( YTPlayer ) {
						YTPlayer.videoData = jQuery.mbStorage.get( "YYTPlayer_data_" + YTPlayer.videoID );
						if( YTPlayer.videoData ) {
								setTimeout( function() {
										YTPlayer.opt.ratio = YTPlayer.opt.ratio == "auto" ? "16/9" : YTPlayer.opt.ratio;
										YTPlayer.dataReceived = true;
										jQuery( YTPlayer ).trigger( "YTPChanged" );
										var YTPData = jQuery.Event( "YTPData" );
										YTPData.prop = {};
										for( var x in YTPlayer.videoData ) YTPData.prop[ x ] = YTPlayer.videoData[ x ];
										jQuery( YTPlayer ).trigger( YTPData );
								}, 500 );
								YTPlayer.hasData = true;
						} else if( jQuery.mbYTPlayer.apiKey ) {
								// Get video info from API3 (needs api key)
								// snippet,player,contentDetails,statistics,status
								jQuery.getJSON( "https://www.googleapis.com/youtube/v3/videos?id=" + YTPlayer.videoID + "&key=" + jQuery.mbYTPlayer.apiKey + "&part=snippet", function( data ) {
										YTPlayer.dataReceived = true;
										jQuery( YTPlayer ).trigger( "YTPChanged" );

										function parseYTPlayer_data( data ) {
												YTPlayer.videoData = {};
												YTPlayer.videoData.id = YTPlayer.videoID;
												YTPlayer.videoData.channelTitle = data.channelTitle;
												YTPlayer.videoData.title = data.title;
												YTPlayer.videoData.description = data.description.length < 400 ? data.description : data.description.substring( 0, 400 ) + " ...";
												YTPlayer.videoData.aspectratio = YTPlayer.opt.ratio == "auto" ? "16/9" : YTPlayer.opt.ratio;
												YTPlayer.opt.ratio = YTPlayer.videoData.aspectratio;
												YTPlayer.videoData.thumb_max = data.thumbnails.maxres ? data.thumbnails.maxres.url : null;
												YTPlayer.videoData.thumb_high = data.thumbnails.high ? data.thumbnails.high.url : null;
												YTPlayer.videoData.thumb_medium = data.thumbnails.medium ? data.thumbnails.medium.url : null;
												jQuery.mbStorage.set( "YYTPlayer_data_" + YTPlayer.videoID, YTPlayer.videoData );
										}
										parseYTPlayer_data( data.items[ 0 ].snippet );
										YTPlayer.hasData = true;
										var YTPData = jQuery.Event( "YTPData" );
										YTPData.prop = {};
										for( var x in YTPlayer.videoData ) YTPData.prop[ x ] = YTPlayer.videoData[ x ];
										jQuery( YTPlayer ).trigger( YTPData );
								} );
						} else {
								setTimeout( function() {
										jQuery( YTPlayer ).trigger( "YTPChanged" );
								}, 50 );
								if( YTPlayer.isPlayer && !YTPlayer.opt.autoPlay ) {
										var bgndURL = "https://i.ytimg.com/vi/" + YTPlayer.videoID + "/hqdefault.jpg";
										YTPlayer.opt.containment.css( {
												background: "rgba(0,0,0,0.5) url(" + bgndURL + ") center center",
												backgroundSize: "cover"
										} );
										YTPlayer.opt.backgroundUrl = bgndURL;
								}
								YTPlayer.videoData = null;
								YTPlayer.opt.ratio = YTPlayer.opt.ratio == "auto" ? "16/9" : YTPlayer.opt.ratio;
						}
						jQuery( YTPlayer ).off( "YTPData.YTPlayer" ).on( "YTPData.YTPlayer", function() {
								if( !YTPlayer.hasData ) {
										YTPlayer.hasData = true;
										if( YTPlayer.isPlayer && !YTPlayer.opt.autoPlay ) {
												var bgndURL = YTPlayer.videoData.thumb_max || YTPlayer.videoData.thumb_high || YTPlayer.videoData.thumb_medium;
												YTPlayer.opt.containment.css( {
														background: "rgba(0,0,0,0.5) url(" + bgndURL + ") center center",
														backgroundSize: "cover"
												} );
												YTPlayer.opt.backgroundUrl = bgndURL;
										}
								}
						} );
						if( YTPlayer.isPlayer && !YTPlayer.opt.autoPlay ) {
								YTPlayer.loading = jQuery( "<div/>" ).addClass( "loading" ).html( "Loading" ).hide();
								jQuery( YTPlayer ).append( YTPlayer.loading );
								YTPlayer.loading.fadeIn();
						}
				},
				/**
				 *
				 */
				removeStoredData: function() {
						jQuery.mbStorage.remove();
				},
				/**
				 *
				 * @returns {*|YTPlayer.videoData}
				 */
				getVideoData: function() {
						var YTPlayer = this.get( 0 );
						return YTPlayer.videoData;
				},
				/**
				 *
				 * @returns {*|YTPlayer.videoID|boolean}
				 */
				getVideoID: function() {
						var YTPlayer = this.get( 0 );
						return YTPlayer.videoID || false;
				},
				/**
				 *
				 * @param quality
				 */
				setVideoQuality: function( quality ) {
						var YTPlayer = this.get( 0 );
						if( !jQuery.browser.chrome ) YTPlayer.player.setPlaybackQuality( quality );
				},
				/**
				 * @param videos
				 * @param shuffle
				 * @param callback
				 * @returns {jQuery.mbYTPlayer}
				 */
				playlist: function( videos, shuffle, callback ) {
						var $YTPlayer = this;
						var YTPlayer = $YTPlayer.get( 0 );
						YTPlayer.isPlayList = true;
						if( shuffle ) videos = jQuery.shuffle( videos );
						if( !YTPlayer.videoID ) {
								YTPlayer.videos = videos;
								YTPlayer.videoCounter = 0;
								YTPlayer.videoLength = videos.length;
								jQuery( YTPlayer ).data( "property", videos[ 0 ] );
								jQuery( YTPlayer ).mb_YTPlayer();
						}
						if( typeof callback == "function" ) jQuery( YTPlayer ).on( "YTPChanged", function() {
								callback( YTPlayer );
						} );
						jQuery( YTPlayer ).on( "YTPEnd", function() {
								jQuery( YTPlayer ).playNext();
						} );
						return $YTPlayer;
				},
				/**
				 *
				 * @returns {jQuery.mbYTPlayer}
				 */
				playNext: function() {
						var YTPlayer = this.get( 0 );
						YTPlayer.videoCounter++;
						if( YTPlayer.videoCounter >= YTPlayer.videoLength ) YTPlayer.videoCounter = 0;
						jQuery( YTPlayer ).changeMovie( YTPlayer.videos[ YTPlayer.videoCounter ] );
						return this;
				},
				/**
				 *
				 * @returns {jQuery.mbYTPlayer}
				 */
				playPrev: function() {
						var YTPlayer = this.get( 0 );
						YTPlayer.videoCounter--;
						if( YTPlayer.videoCounter < 0 ) YTPlayer.videoCounter = YTPlayer.videoLength - 1;
						jQuery( YTPlayer ).changeMovie( YTPlayer.videos[ YTPlayer.videoCounter ] );
						return this;
				},
				/**
				 *
				 * @param opt
				 */
				changeMovie: function( opt ) {
						var YTPlayer = this.get( 0 );
						YTPlayer.opt.startAt = 0;
						YTPlayer.opt.stopAt = 0;
						YTPlayer.opt.mute = true;
						YTPlayer.hasData = false;
						YTPlayer.hasChanged = true;
						if( opt ) jQuery.extend( YTPlayer.opt, YTPlayer.defaultOpt, opt );
						YTPlayer.videoID = getYTPVideoID( YTPlayer.opt.videoURL ).videoID;
						jQuery( YTPlayer.playerEl ).CSSAnimate( {
								opacity: 0
						}, 200, function() {
								jQuery( YTPlayer ).YTPGetPlayer().cueVideoByUrl( encodeURI( jQuery.mbYTPlayer.locationProtocol + "//www.youtube.com/v/" + YTPlayer.videoID ), 1, YTPlayer.opt.quality );
								jQuery.mbYTPlayer.checkForState( YTPlayer );
								jQuery( YTPlayer ).optimizeDisplay();
								jQuery.mbYTPlayer.getDataFromAPI( YTPlayer );
								return this;
						} );
				},
				/**
				 *
				 * @returns {player}
				 */
				getPlayer: function() {
						return jQuery( this ).get( 0 ).player;
				},
				playerDestroy: function() {
						var YTPlayer = this.get( 0 );
						ytp.YTAPIReady = false;
						ytp.backgroundIsInited = false;
						YTPlayer.isInit = false;
						YTPlayer.videoID = null;
						var playerBox = YTPlayer.wrapper;
						playerBox.remove();
						jQuery( "#controlBar_" + YTPlayer.id ).remove();
						clearInterval( YTPlayer.checkForStartAt );
						clearInterval( YTPlayer.getState );
						return this;
				},
				/**
				 *
				 * @param real
				 * @returns {jQuery.mbYTPlayer}
				 */
				fullscreen: function( real ) {
						var YTPlayer = this.get( 0 );
						if( typeof real == "undefined" ) real = YTPlayer.opt.realfullscreen;
						real = eval( real );
						var controls = jQuery( "#controlBar_" + YTPlayer.id );
						var fullScreenBtn = controls.find( ".mb_OnlyYT" );
						var videoWrapper = YTPlayer.isSelf ? YTPlayer.opt.containment : YTPlayer.wrapper;
						//var videoWrapper = YTPlayer.wrapper;
						if( real ) {
								var fullscreenchange = jQuery.browser.mozilla ? "mozfullscreenchange" : jQuery.browser.webkit ? "webkitfullscreenchange" : "fullscreenchange";
								jQuery( document ).off( fullscreenchange ).on( fullscreenchange, function() {
										var isFullScreen = RunPrefixMethod( document, "IsFullScreen" ) || RunPrefixMethod( document, "FullScreen" );
										if( !isFullScreen ) {
												YTPlayer.isAlone = false;
												fullScreenBtn.html( jQuery.mbYTPlayer.controls.onlyYT );
												jQuery( YTPlayer ).YTPSetVideoQuality( YTPlayer.opt.quality );
												videoWrapper.removeClass( "fullscreen" );
												videoWrapper.CSSAnimate( {
														opacity: YTPlayer.opt.opacity
												}, 500 );
												videoWrapper.css( {
														zIndex: 0
												} );
												if( YTPlayer.isBackground ) {
														jQuery( "body" ).after( controls );
												} else {
														YTPlayer.wrapper.before( controls );
												}
												jQuery( window ).resize();
												jQuery( YTPlayer ).trigger( "YTPFullScreenEnd" );
										} else {
												jQuery( YTPlayer ).YTPSetVideoQuality( "default" );
												jQuery( YTPlayer ).trigger( "YTPFullScreenStart" );
										}
								} );
						}
						if( !YTPlayer.isAlone ) {
								function hideMouse() {
										YTPlayer.overlay.css( {
												cursor: "none"
										} );
								}
								jQuery( document ).on( "mousemove.YTPlayer", function( e ) {
										YTPlayer.overlay.css( {
												cursor: "auto"
										} );
										clearTimeout( YTPlayer.hideCursor );
										if( !jQuery( e.target ).parents().is( ".mb_YTPBar" ) ) YTPlayer.hideCursor = setTimeout( hideMouse, 3000 );
								} );
								hideMouse();
								if( real ) {
										videoWrapper.css( {
												opacity: 0
										} );
										videoWrapper.addClass( "fullscreen" );
										launchFullscreen( videoWrapper.get( 0 ) );
										setTimeout( function() {
												videoWrapper.CSSAnimate( {
														opacity: 1
												}, 1000 );
												YTPlayer.wrapper.append( controls );
												jQuery( YTPlayer ).optimizeDisplay();
												YTPlayer.player.seekTo( YTPlayer.player.getCurrentTime() + .1, true );
										}, 500 )
								} else videoWrapper.css( {
										zIndex: 10000
								} ).CSSAnimate( {
										opacity: 1
								}, 1000 );
								fullScreenBtn.html( jQuery.mbYTPlayer.controls.showSite );
								YTPlayer.isAlone = true;
						} else {
								jQuery( document ).off( "mousemove.YTPlayer" );
								YTPlayer.overlay.css( {
										cursor: "auto"
								} );
								if( real ) {
										cancelFullscreen();
								} else {
										videoWrapper.CSSAnimate( {
												opacity: YTPlayer.opt.opacity
										}, 500 );
										videoWrapper.css( {
												zIndex: 0
										} );
								}
								fullScreenBtn.html( jQuery.mbYTPlayer.controls.onlyYT );
								YTPlayer.isAlone = false;
						}

						function RunPrefixMethod( obj, method ) {
								var pfx = [ "webkit", "moz", "ms", "o", "" ];
								var p = 0,
										m, t;
								while( p < pfx.length && !obj[ m ] ) {
										m = method;
										if( pfx[ p ] == "" ) {
												m = m.substr( 0, 1 ).toLowerCase() + m.substr( 1 );
										}
										m = pfx[ p ] + m;
										t = typeof obj[ m ];
										if( t != "undefined" ) {
												pfx = [ pfx[ p ] ];
												return( t == "function" ? obj[ m ]() : obj[ m ] );
										}
										p++;
								}
						}

						function launchFullscreen( element ) {
								RunPrefixMethod( element, "RequestFullScreen" );
						}

						function cancelFullscreen() {
								if( RunPrefixMethod( document, "FullScreen" ) || RunPrefixMethod( document, "IsFullScreen" ) ) {
										RunPrefixMethod( document, "CancelFullScreen" );
								}
						}
						return this;
				},
				/**
				 *
				 * @returns {jQuery.mbYTPlayer}
				 */
				toggleLoops: function() {
						var YTPlayer = this.get( 0 );
						var data = YTPlayer.opt;
						if( data.loop == 1 ) {
								data.loop = 0;
						} else {
								if( data.startAt ) {
										YTPlayer.player.seekTo( data.startAt );
								} else {
										YTPlayer.player.playVideo();
								}
								data.loop = 1;
						}
						return this;
				},
				/**
				 *
				 * @returns {jQuery.mbYTPlayer}
				 */
				play: function() {
						var YTPlayer = this.get( 0 );
						if( !YTPlayer.isReady ) return;
						var controls = jQuery( "#controlBar_" + YTPlayer.id );
						var playBtn = controls.find( ".mb_YTPPlaypause" );
						playBtn.html( jQuery.mbYTPlayer.controls.pause );
						YTPlayer.player.playVideo();
						YTPlayer.wrapper.CSSAnimate( {
								opacity: YTPlayer.isAlone ? 1 : YTPlayer.opt.opacity
						}, 2000 );
						jQuery( YTPlayer.playerEl ).CSSAnimate( {
								opacity: 1
						}, 1000 );
						jQuery( YTPlayer ).css( "background-image", "none" );
						return this;
				},
				/**
				 *
				 * @param callback
				 * @returns {jQuery.mbYTPlayer}
				 */
				togglePlay: function( callback ) {
						var YTPlayer = this.get( 0 );
						if( YTPlayer.state == 1 ) this.YTPPause();
						else this.YTPPlay();
						if( typeof callback == "function" ) {
								callback( YTPlayer.state );
						}
						return this;
				},
				/**
				 *
				 * @returns {jQuery.mbYTPlayer}
				 */
				stop: function() {
						var YTPlayer = this.get( 0 );
						var controls = jQuery( "#controlBar_" + YTPlayer.id );
						var playBtn = controls.find( ".mb_YTPPlaypause" );
						playBtn.html( jQuery.mbYTPlayer.controls.play );
						YTPlayer.player.stopVideo();
						return this;
				},
				/**
				 *
				 * @returns {jQuery.mbYTPlayer}
				 */
				pause: function() {
						var YTPlayer = this.get( 0 );
						var controls = jQuery( "#controlBar_" + YTPlayer.id );
						var playBtn = controls.find( ".mb_YTPPlaypause" );
						playBtn.html( jQuery.mbYTPlayer.controls.play );
						YTPlayer.player.pauseVideo();
						return this;
				},
				/**
				 *
				 * @param val
				 * @returns {jQuery.mbYTPlayer}
				 */
				seekTo: function( val ) {
						var YTPlayer = this.get( 0 );
						YTPlayer.player.seekTo( val, true );
						return this;
				},
				/**
				 *
				 * @param val
				 * @returns {jQuery.mbYTPlayer}
				 */
				setVolume: function( val ) {
						var YTPlayer = this.get( 0 );
						if( !val && !YTPlayer.opt.vol && YTPlayer.player.getVolume() == 0 ) jQuery( YTPlayer ).YTPUnmute();
						else if( ( !val && YTPlayer.player.getVolume() > 0 ) || ( val && YTPlayer.opt.vol == val ) ) {
								if( !YTPlayer.isMute ) jQuery( YTPlayer ).YTPMute();
								else jQuery( YTPlayer ).YTPUnmute();
						} else {
								YTPlayer.opt.vol = val;
								YTPlayer.player.setVolume( YTPlayer.opt.vol );
								if( YTPlayer.volumeBar && YTPlayer.volumeBar.length ) YTPlayer.volumeBar.updateSliderVal( val )
						}
						return this;
				},
				/**
				 *
				 * @returns {jQuery.mbYTPlayer}
				 */
				mute: function() {
						var YTPlayer = this.get( 0 );
						if( YTPlayer.isMute ) return;
						YTPlayer.player.mute();
						YTPlayer.isMute = true;
						YTPlayer.player.setVolume( 0 );
						if( YTPlayer.volumeBar && YTPlayer.volumeBar.length && YTPlayer.volumeBar.width() > 10 ) {
								YTPlayer.volumeBar.updateSliderVal( 0 );
						}
						var controls = jQuery( "#controlBar_" + YTPlayer.id );
						var muteBtn = controls.find( ".mb_YTPMuteUnmute" );
						muteBtn.html( jQuery.mbYTPlayer.controls.unmute );
						jQuery( YTPlayer ).addClass( "isMuted" );
						if( YTPlayer.volumeBar && YTPlayer.volumeBar.length ) YTPlayer.volumeBar.addClass( "muted" );
						var YTPEvent = jQuery.Event( "YTPMuted" );
						YTPEvent.time = YTPlayer.player.time;
						if( YTPlayer.canTrigger ) jQuery( YTPlayer ).trigger( YTPEvent );
						return this;
				},
				/**
				 *
				 * @returns {jQuery.mbYTPlayer}
				 */
				unmute: function() {
						var YTPlayer = this.get( 0 );
						if( !YTPlayer.isMute ) return;
						YTPlayer.player.unMute();
						YTPlayer.isMute = false;
						YTPlayer.player.setVolume( YTPlayer.opt.vol );
						if( YTPlayer.volumeBar && YTPlayer.volumeBar.length ) YTPlayer.volumeBar.updateSliderVal( YTPlayer.opt.vol > 10 ? YTPlayer.opt.vol : 10 );
						var controls = jQuery( "#controlBar_" + YTPlayer.id );
						var muteBtn = controls.find( ".mb_YTPMuteUnmute" );
						muteBtn.html( jQuery.mbYTPlayer.controls.mute );
						jQuery( YTPlayer ).removeClass( "isMuted" );
						if( YTPlayer.volumeBar && YTPlayer.volumeBar.length ) YTPlayer.volumeBar.removeClass( "muted" );
						var YTPEvent = jQuery.Event( "YTPUnmuted" );
						YTPEvent.time = YTPlayer.player.time;
						if( YTPlayer.canTrigger ) jQuery( YTPlayer ).trigger( YTPEvent );
						return this;
				},
				/**
				 *
				 * @param filter
				 * @param value
				 * @returns {jQuery.mbYTPlayer}
				 */
				applyFilter: function( filter, value ) {
						var YTPlayer = this.get( 0 );
						YTPlayer.filters[ filter ].value = value;
						if( YTPlayer.filtersEnabled ) this.YTPEnableFilters();
						return this;
				},
				/**
				 *
				 * @param filters
				 * @returns {jQuery.mbYTPlayer}
				 */
				applyFilters: function( filters ) {
						var YTPlayer = this.get( 0 );
						this.on( "YTPReady", function() {
								for( var key in filters ) {
										YTPlayer.filters[ key ].value = filters[ key ];
										jQuery( YTPlayer ).YTPApplyFilter( key, filters[ key ] );
								}
								jQuery( YTPlayer ).trigger( "YTPFiltersApplied" );
						} );
						return this;
				},
				/**
				 *
				 * @param filter
				 * @param value
				 * @returns {*}
				 */
				toggleFilter: function( filter, value ) {
						return this.each( function() {
								var YTPlayer = this;
								if( !YTPlayer.filters[ filter ].value ) YTPlayer.filters[ filter ].value = value;
								else YTPlayer.filters[ filter ].value = 0;
								if( YTPlayer.filtersEnabled ) jQuery( this ).YTPEnableFilters();
						} )
						return this;
				},
				/**
				 *
				 * @param callback
				 * @returns {*}
				 */
				toggleFilters: function( callback ) {
						return this.each( function() {
								var YTPlayer = this;
								if( YTPlayer.filtersEnabled ) {
										jQuery( YTPlayer ).trigger( "YTPDisableFilters" );
										jQuery( YTPlayer ).YTPDisableFilters();
								} else {
										jQuery( YTPlayer ).YTPEnableFilters();
										jQuery( YTPlayer ).trigger( "YTPEnableFilters" );
								}
								if( typeof callback == "function" ) callback( YTPlayer.filtersEnabled );
						} )
				},
				/**
				 *
				 * @returns {*}
				 */
				disableFilters: function() {
						return this.each( function() {
								var YTPlayer = this;
								var iframe = jQuery( YTPlayer.playerEl );
								iframe.css( "-webkit-filter", "" );
								iframe.css( "filter", "" );
								YTPlayer.filtersEnabled = false;
						} )
				},
				/**
				 *
				 * @returns {*}
				 */
				enableFilters: function() {
						return this.each( function() {
								var YTPlayer = this;
								var iframe = jQuery( YTPlayer.playerEl );
								var filterStyle = "";
								for( var key in YTPlayer.filters ) {
										if( YTPlayer.filters[ key ].value ) filterStyle += key.replace( "_", "-" ) + "(" + YTPlayer.filters[ key ].value + YTPlayer.filters[ key ].unit + ") ";
								}
								iframe.css( "-webkit-filter", filterStyle );
								iframe.css( "filter", filterStyle );
								YTPlayer.filtersEnabled = true;
						} )
						return this;
				},
				/**
				 *
				 * @param filter
				 * @param callback
				 * @returns {*}
				 */
				removeFilter: function( filter, callback ) {
						return this.each( function() {
								if( typeof filter == "function" ) {
										callback = filter;
										filter = null;
								}
								var YTPlayer = this;
								if( !filter )
										for( var key in YTPlayer.filters ) {
												jQuery( this ).YTPApplyFilter( key, 0 );
												if( typeof callback == "function" ) callback( key );
										} else {
												jQuery( this ).YTPApplyFilter( filter, 0 );
												if( typeof callback == "function" ) callback( filter );
										}
						} );
						return this;
				},
				/**
				 *
				 * @returns {{totalTime: number, currentTime: number}}
				 */
				manageProgress: function() {
						var YTPlayer = this.get( 0 );
						var controls = jQuery( "#controlBar_" + YTPlayer.id );
						var progressBar = controls.find( ".mb_YTPProgress" );
						var loadedBar = controls.find( ".mb_YTPLoaded" );
						var timeBar = controls.find( ".mb_YTPseekbar" );
						var totW = progressBar.outerWidth();
						var currentTime = Math.floor( YTPlayer.player.getCurrentTime() );
						var totalTime = Math.floor( YTPlayer.player.getDuration() );
						var timeW = ( currentTime * totW ) / totalTime;
						var startLeft = 0;
						var loadedW = YTPlayer.player.getVideoLoadedFraction() * 100;
						loadedBar.css( {
								left: startLeft,
								width: loadedW + "%"
						} );
						timeBar.css( {
								left: 0,
								width: timeW
						} );
						return {
								totalTime: totalTime,
								currentTime: currentTime
						};
				},
				/**
				 *
				 * @param YTPlayer
				 */
				buildControls: function( YTPlayer ) {
						var data = YTPlayer.opt;
						// @data.printUrl: is deprecated; use data.showYTLogo
						data.showYTLogo = data.showYTLogo || data.printUrl;
						if( jQuery( "#controlBar_" + YTPlayer.id ).length ) return;
						YTPlayer.controlBar = jQuery( "<span/>" ).attr( "id", "controlBar_" + YTPlayer.id ).addClass( "mb_YTPBar" ).css( {
								whiteSpace: "noWrap",
								position: YTPlayer.isBackground ? "fixed" : "absolute",
								zIndex: YTPlayer.isBackground ? 10000 : 1000
						} ).hide();
						var buttonBar = jQuery( "<div/>" ).addClass( "buttonBar" );
						/* play/pause button*/
						var playpause = jQuery( "<span>" + jQuery.mbYTPlayer.controls.play + "</span>" ).addClass( "mb_YTPPlaypause ytpicon" ).click( function() {
								if( YTPlayer.player.getPlayerState() == 1 ) jQuery( YTPlayer ).YTPPause();
								else jQuery( YTPlayer ).YTPPlay();
						} );
						/* mute/unmute button*/
						var MuteUnmute = jQuery( "<span>" + jQuery.mbYTPlayer.controls.mute + "</span>" ).addClass( "mb_YTPMuteUnmute ytpicon" ).click( function() {
								if( YTPlayer.player.getVolume() == 0 ) {
										jQuery( YTPlayer ).YTPUnmute();
								} else {
										jQuery( YTPlayer ).YTPMute();
								}
						} );
						/* volume bar*/
						var volumeBar = jQuery( "<div/>" ).addClass( "mb_YTPVolumeBar" ).css( {
								display: "inline-block"
						} );
						YTPlayer.volumeBar = volumeBar;
						/* time elapsed */
						var idx = jQuery( "<span/>" ).addClass( "mb_YTPTime" );
						var vURL = data.videoURL ? data.videoURL : "";
						if( vURL.indexOf( "http" ) < 0 ) vURL = jQuery.mbYTPlayer.locationProtocol + "//www.youtube.com/watch?v=" + data.videoURL;
						var movieUrl = jQuery( "<span/>" ).html( jQuery.mbYTPlayer.controls.ytLogo ).addClass( "mb_YTPUrl ytpicon" ).attr( "title", "view on YouTube" ).on( "click", function() {
								window.open( vURL, "viewOnYT" )
						} );
						var onlyVideo = jQuery( "<span/>" ).html( jQuery.mbYTPlayer.controls.onlyYT ).addClass( "mb_OnlyYT ytpicon" ).on( "click", function() {
								jQuery( YTPlayer ).YTPFullscreen( data.realfullscreen );
						} );
						var progressBar = jQuery( "<div/>" ).addClass( "mb_YTPProgress" ).css( "position", "absolute" ).click( function( e ) {
								timeBar.css( {
										width: ( e.clientX - timeBar.offset().left )
								} );
								YTPlayer.timeW = e.clientX - timeBar.offset().left;
								YTPlayer.controlBar.find( ".mb_YTPLoaded" ).css( {
										width: 0
								} );
								var totalTime = Math.floor( YTPlayer.player.getDuration() );
								YTPlayer.goto = ( timeBar.outerWidth() * totalTime ) / progressBar.outerWidth();
								YTPlayer.player.seekTo( parseFloat( YTPlayer.goto ), true );
								YTPlayer.controlBar.find( ".mb_YTPLoaded" ).css( {
										width: 0
								} );
						} );
						var loadedBar = jQuery( "<div/>" ).addClass( "mb_YTPLoaded" ).css( "position", "absolute" );
						var timeBar = jQuery( "<div/>" ).addClass( "mb_YTPseekbar" ).css( "position", "absolute" );
						progressBar.append( loadedBar ).append( timeBar );
						buttonBar.append( playpause ).append( MuteUnmute ).append( volumeBar ).append( idx );
						if( data.showYTLogo ) {
								buttonBar.append( movieUrl );
						}
						if( YTPlayer.isBackground || ( eval( YTPlayer.opt.realfullscreen ) && !YTPlayer.isBackground ) ) buttonBar.append( onlyVideo );
						YTPlayer.controlBar.append( buttonBar ).append( progressBar );
						if( !YTPlayer.isBackground ) {
								YTPlayer.controlBar.addClass( "inlinePlayer" );
								YTPlayer.wrapper.before( YTPlayer.controlBar );
						} else {
								jQuery( "body" ).after( YTPlayer.controlBar );
						}
						volumeBar.simpleSlider( {
								initialval: YTPlayer.opt.vol,
								scale: 100,
								orientation: "h",
								callback: function( el ) {
										if( el.value == 0 ) {
												jQuery( YTPlayer ).YTPMute();
										} else {
												jQuery( YTPlayer ).YTPUnmute();
										}
										YTPlayer.player.setVolume( el.value );
										if( !YTPlayer.isMute ) YTPlayer.opt.vol = el.value;
								}
						} );
				},
				/**
				 *
				 *
				 * */
				checkForState: function( YTPlayer ) {
						var interval = YTPlayer.opt.showControls ? 100 : 1000;
						clearInterval( YTPlayer.getState );
						//Checking if player has been removed from scene
						if( !jQuery.contains( document, YTPlayer ) ) {
								jQuery( YTPlayer ).YTPPlayerDestroy();
								clearInterval( YTPlayer.getState );
								clearInterval( YTPlayer.checkForStartAt );
								return;
						}
						jQuery.mbYTPlayer.checkForStart( YTPlayer );
						YTPlayer.getState = setInterval( function() {
								var prog = jQuery( YTPlayer ).YTPManageProgress();
								var $YTPlayer = jQuery( YTPlayer );
								var data = YTPlayer.opt;
								var startAt = YTPlayer.opt.startAt ? YTPlayer.opt.startAt : 0;
								var stopAt = YTPlayer.opt.stopAt > YTPlayer.opt.startAt ? YTPlayer.opt.stopAt : 0;
								stopAt = stopAt < YTPlayer.player.getDuration() ? stopAt : 0;
								if( YTPlayer.player.time != prog.currentTime ) {
										var YTPEvent = jQuery.Event( "YTPTime" );
										YTPEvent.time = YTPlayer.player.time;
										jQuery( YTPlayer ).trigger( YTPEvent );
								}
								YTPlayer.player.time = prog.currentTime;
								if( YTPlayer.player.getVolume() == 0 ) $YTPlayer.addClass( "isMuted" );
								else $YTPlayer.removeClass( "isMuted" );
								if( YTPlayer.opt.showControls )
										if( prog.totalTime ) {
												YTPlayer.controlBar.find( ".mb_YTPTime" ).html( jQuery.mbYTPlayer.formatTime( prog.currentTime ) + " / " + jQuery.mbYTPlayer.formatTime( prog.totalTime ) );
										} else {
												YTPlayer.controlBar.find( ".mb_YTPTime" ).html( "-- : -- / -- : --" );
										}
								if( eval( YTPlayer.opt.stopMovieOnBlur ) )
										if( !document.hasFocus() ) {
												if( YTPlayer.state == 1 ) {
														YTPlayer.hasFocus = false;
														$YTPlayer.YTPPause();
												}
										} else if( document.hasFocus() && !YTPlayer.hasFocus && !( YTPlayer.state == -1 || YTPlayer.state == 0 ) ) {
										YTPlayer.hasFocus = true;
										$YTPlayer.YTPPlay();
								}
								if( YTPlayer.controlBar && YTPlayer.controlBar.outerWidth() <= 400 && !YTPlayer.isCompact ) {
										YTPlayer.controlBar.addClass( "compact" );
										YTPlayer.isCompact = true;
										if( !YTPlayer.isMute && YTPlayer.volumeBar ) YTPlayer.volumeBar.updateSliderVal( YTPlayer.opt.vol );
								} else if( YTPlayer.controlBar && YTPlayer.controlBar.outerWidth() > 400 && YTPlayer.isCompact ) {
										YTPlayer.controlBar.removeClass( "compact" );
										YTPlayer.isCompact = false;
										if( !YTPlayer.isMute && YTPlayer.volumeBar ) YTPlayer.volumeBar.updateSliderVal( YTPlayer.opt.vol );
								}
								if( YTPlayer.player.getPlayerState() == 1 && ( parseFloat( YTPlayer.player.getDuration() - 3 ) < YTPlayer.player.getCurrentTime() || ( stopAt > 0 && parseFloat( YTPlayer.player.getCurrentTime() ) > stopAt ) ) ) {
										if( YTPlayer.isEnded ) return;
										YTPlayer.isEnded = true;
										setTimeout( function() {
												YTPlayer.isEnded = false
										}, 2000 );
										if( YTPlayer.isPlayList ) {
												clearInterval( YTPlayer.getState );
												var YTPEnd = jQuery.Event( "YTPEnd" );
												YTPEnd.time = YTPlayer.player.time;
												jQuery( YTPlayer ).trigger( YTPEnd );
												return;
										} else if( !data.loop ) {
												YTPlayer.player.pauseVideo();
												YTPlayer.wrapper.CSSAnimate( {
														opacity: 0
												}, 1000, function() {
														var YTPEnd = jQuery.Event( "YTPEnd" );
														YTPEnd.time = YTPlayer.player.time;
														jQuery( YTPlayer ).trigger( YTPEnd );
														YTPlayer.player.seekTo( startAt, true );
														if( !YTPlayer.isBackground ) {
																YTPlayer.opt.containment.css( {
																		background: "rgba(0,0,0,0.5) url(" + YTPlayer.opt.backgroundUrl + ") center center",
																		backgroundSize: "cover"
																} );
														}
												} );
										} else YTPlayer.player.seekTo( startAt, true );
								}
						}, interval );
				},
				/**
				 *
				 * */
				checkForStart: function( YTPlayer ) {
						var $YTPlayer = jQuery( YTPlayer );
						//Checking if player has been removed from scene
						if( !jQuery.contains( document, YTPlayer ) ) {
								jQuery( YTPlayer ).YTPPlayerDestroy();
								return
						}
						if( jQuery.browser.chrome ) YTPlayer.opt.quality = "default";
						YTPlayer.player.pauseVideo();
						jQuery( YTPlayer ).muteYTPVolume();
						jQuery( "#controlBar_" + YTPlayer.id ).remove();
						if( YTPlayer.opt.showControls ) jQuery.mbYTPlayer.buildControls( YTPlayer );
						if( YTPlayer.opt.addRaster ) {
								var classN = YTPlayer.opt.addRaster == "dot" ? "raster-dot" : "raster";
								YTPlayer.overlay.addClass( YTPlayer.isRetina ? classN + " retina" : classN );
						} else {
								YTPlayer.overlay.removeClass( function( index, classNames ) {
										// change the list into an array
										var current_classes = classNames.split( " " ),
												// array of classes which are to be removed
												classes_to_remove = [];
										jQuery.each( current_classes, function( index, class_name ) {
												// if the classname begins with bg add it to the classes_to_remove array
												if( /raster.*/.test( class_name ) ) {
														classes_to_remove.push( class_name );
												}
										} );
										classes_to_remove.push( "retina" );
										// turn the array back into a string
										return classes_to_remove.join( " " );
								} )
						}
						YTPlayer.checkForStartAt = setInterval( function() {
								jQuery( YTPlayer ).YTPMute();
								var startAt = YTPlayer.opt.startAt ? YTPlayer.opt.startAt : 1;
								var canPlayVideo = ( YTPlayer.player.getVideoLoadedFraction() > startAt / YTPlayer.player.getDuration() );
								if( YTPlayer.player.getDuration() > 0 && YTPlayer.player.getCurrentTime() >= startAt && canPlayVideo ) {
										clearInterval( YTPlayer.checkForStartAt );
										YTPlayer.isReady = true;
										if( typeof YTPlayer.opt.onReady == "function" ) YTPlayer.opt.onReady( YTPlayer );
										var YTPready = jQuery.Event( "YTPReady" );
										jQuery( YTPlayer ).trigger( YTPready );
										YTPlayer.player.pauseVideo();
										if( !YTPlayer.opt.mute ) jQuery( YTPlayer ).YTPUnmute();
										YTPlayer.canTrigger = true;
										if( YTPlayer.opt.autoPlay ) {
												$YTPlayer.YTPPlay();
												$YTPlayer.css( "background-image", "none" );
												jQuery( YTPlayer.playerEl ).CSSAnimate( {
														opacity: 1
												}, 1000 );
												YTPlayer.wrapper.CSSAnimate( {
														opacity: YTPlayer.isAlone ? 1 : YTPlayer.opt.opacity
												}, 1000 );
										} else {
												YTPlayer.player.pauseVideo();
												if( !YTPlayer.isPlayer ) {
														jQuery( YTPlayer.playerEl ).CSSAnimate( {
																opacity: 1
														}, 1000 );
														YTPlayer.wrapper.CSSAnimate( {
																opacity: YTPlayer.isAlone ? 1 : YTPlayer.opt.opacity
														}, 1000 );
												}
										}
										if( YTPlayer.isPlayer && !YTPlayer.opt.autoPlay ) {
												YTPlayer.loading.html( "Ready" );
												setTimeout( function() {
														YTPlayer.loading.fadeOut();
												}, 100 )
										}
										if( YTPlayer.controlBar ) YTPlayer.controlBar.slideDown( 1000 );
								} else {
										//YTPlayer.player.playVideo();
										if( startAt >= 0 ) YTPlayer.player.seekTo( startAt, true );
								}
						}, 1000 );
				},
				/**
				 *
				 * @param s
				 * @returns {string}
				 */
				formatTime: function( s ) {
						var min = Math.floor( s / 60 );
						var sec = Math.floor( s - ( 60 * min ) );
						return( min <= 9 ? "0" + min : min ) + " : " + ( sec <= 9 ? "0" + sec : sec );
				}
		};
		/**
		 *
		 * @returns {boolean}
		 */
		jQuery.fn.toggleVolume = function() {
				var YTPlayer = this.get( 0 );
				if( !YTPlayer ) return;
				if( YTPlayer.player.isMuted() ) {
						jQuery( YTPlayer ).YTPUnmute();
						return true;
				} else {
						jQuery( YTPlayer ).YTPMute();
						return false;
				}
		};
		/**
		 *
		 */
		jQuery.fn.optimizeDisplay = function() {
				var YTPlayer = this.get( 0 );
				var data = YTPlayer.opt;
				var playerBox = jQuery( YTPlayer.playerEl );
				var win = {};
				var el = YTPlayer.wrapper;
				win.width = el.outerWidth();
				win.height = el.outerHeight();
				var margin = 24;
				var overprint = 100;
				var vid = {};
				if( data.optimizeDisplay ) {
						vid.width = win.width + ( ( win.width * margin ) / 100 );
						vid.height = data.ratio == "16/9" ? Math.ceil( ( 9 * win.width ) / 16 ) : Math.ceil( ( 3 * win.width ) / 4 );
						vid.marginTop = -( ( vid.height - win.height ) / 2 );
						vid.marginLeft = -( ( win.width * ( margin / 2 ) ) / 100 );
						if( vid.height < win.height ) {
								vid.height = win.height + ( ( win.height * margin ) / 100 );
								vid.width = data.ratio == "16/9" ? Math.floor( ( 16 * win.height ) / 9 ) : Math.floor( ( 4 * win.height ) / 3 );
								vid.marginTop = -( ( win.height * ( margin / 2 ) ) / 100 );
								vid.marginLeft = -( ( vid.width - win.width ) / 2 );
						}
						vid.width += overprint;
						vid.height += overprint;
						vid.marginTop -= overprint / 2;
						vid.marginLeft -= overprint / 2;
				} else {
						vid.width = "100%";
						vid.height = "100%";
						vid.marginTop = 0;
						vid.marginLeft = 0;
				}
				playerBox.css( {
						width: vid.width,
						height: vid.height,
						marginTop: vid.marginTop,
						marginLeft: vid.marginLeft
				} );
		};
		/**
		 *
		 * @param arr
		 * @returns {Array|string|Blob|*}
		 *
		 */
		jQuery.shuffle = function( arr ) {
				var newArray = arr.slice();
				var len = newArray.length;
				var i = len;
				while( i-- ) {
						var p = parseInt( Math.random() * len );
						var t = newArray[ i ];
						newArray[ i ] = newArray[ p ];
						newArray[ p ] = t;
				}
				return newArray;
		};
		/* Exposed public method */
		jQuery.fn.YTPlayer = jQuery.mbYTPlayer.buildPlayer;
		jQuery.fn.YTPlaylist = jQuery.mbYTPlayer.playlist;
		jQuery.fn.YTPPlayNext = jQuery.mbYTPlayer.playNext;
		jQuery.fn.YTPPlayPrev = jQuery.mbYTPlayer.playPrev;
		jQuery.fn.YTPChangeMovie = jQuery.mbYTPlayer.changeMovie;
		jQuery.fn.YTPGetVideoID = jQuery.mbYTPlayer.getVideoID;
		jQuery.fn.YTPGetPlayer = jQuery.mbYTPlayer.getPlayer;
		jQuery.fn.YTPPlayerDestroy = jQuery.mbYTPlayer.playerDestroy;
		jQuery.fn.YTPFullscreen = jQuery.mbYTPlayer.fullscreen;
		jQuery.fn.YTPPlay = jQuery.mbYTPlayer.play;
		jQuery.fn.YTPTogglePlay = jQuery.mbYTPlayer.togglePlay;
		jQuery.fn.YTPToggleLoops = jQuery.mbYTPlayer.toggleLoops;
		jQuery.fn.YTPStop = jQuery.mbYTPlayer.stop;
		jQuery.fn.YTPPause = jQuery.mbYTPlayer.pause;
		jQuery.fn.YTPSeekTo = jQuery.mbYTPlayer.seekTo;
		jQuery.fn.YTPMute = jQuery.mbYTPlayer.mute;
		jQuery.fn.YTPUnmute = jQuery.mbYTPlayer.unmute;
		jQuery.fn.YTPToggleVolume = jQuery.mbYTPlayer.toggleVolume;
		jQuery.fn.YTPSetVolume = jQuery.mbYTPlayer.setVolume;
		jQuery.fn.YTPSetVideoQuality = jQuery.mbYTPlayer.setVideoQuality;
		jQuery.fn.YTPManageProgress = jQuery.mbYTPlayer.manageProgress;
		jQuery.fn.YTPGetVideoData = jQuery.mbYTPlayer.getVideoData;
		jQuery.fn.YTPApplyFilter = jQuery.mbYTPlayer.applyFilter;
		jQuery.fn.YTPApplyFilters = jQuery.mbYTPlayer.applyFilters;
		jQuery.fn.YTPToggleFilter = jQuery.mbYTPlayer.toggleFilter;
		jQuery.fn.YTPToggleFilters = jQuery.mbYTPlayer.toggleFilters;
		jQuery.fn.YTPRemoveFilter = jQuery.mbYTPlayer.removeFilter;
		jQuery.fn.YTPDisableFilters = jQuery.mbYTPlayer.disableFilters;
		jQuery.fn.YTPEnableFilters = jQuery.mbYTPlayer.enableFilters;
		/**
		 *
		 * @deprecated
		 *
		 **/
		jQuery.fn.mb_YTPlayer = jQuery.mbYTPlayer.buildPlayer;
		jQuery.fn.playNext = jQuery.mbYTPlayer.playNext;
		jQuery.fn.playPrev = jQuery.mbYTPlayer.playPrev;
		jQuery.fn.changeMovie = jQuery.mbYTPlayer.changeMovie;
		jQuery.fn.getVideoID = jQuery.mbYTPlayer.getVideoID;
		jQuery.fn.getPlayer = jQuery.mbYTPlayer.getPlayer;
		jQuery.fn.playerDestroy = jQuery.mbYTPlayer.playerDestroy;
		jQuery.fn.fullscreen = jQuery.mbYTPlayer.fullscreen;
		jQuery.fn.buildYTPControls = jQuery.mbYTPlayer.buildControls;
		jQuery.fn.playYTP = jQuery.mbYTPlayer.play;
		jQuery.fn.toggleLoops = jQuery.mbYTPlayer.toggleLoops;
		jQuery.fn.stopYTP = jQuery.mbYTPlayer.stop;
		jQuery.fn.pauseYTP = jQuery.mbYTPlayer.pause;
		jQuery.fn.seekToYTP = jQuery.mbYTPlayer.seekTo;
		jQuery.fn.muteYTPVolume = jQuery.mbYTPlayer.mute;
		jQuery.fn.unmuteYTPVolume = jQuery.mbYTPlayer.unmute;
		jQuery.fn.setYTPVolume = jQuery.mbYTPlayer.setVolume;
		jQuery.fn.setVideoQuality = jQuery.mbYTPlayer.setVideoQuality;
		jQuery.fn.manageYTPProgress = jQuery.mbYTPlayer.manageProgress;
		jQuery.fn.YTPGetDataFromFeed = jQuery.mbYTPlayer.getVideoData;
} )( jQuery, ytp );
;
/*
 * ******************************************************************************
 *  jquery.mb.components
 *  file: jquery.mb.CSSAnimate.min.js
 *
 *  Copyright (c) 2001-2014. Matteo Bicocchi (Pupunzi);
 *  Open lab srl, Firenze - Italy
 *  email: matteo@open-lab.com
 *  site: 	http://pupunzi.com
 *  blog:	http://pupunzi.open-lab.com
 * 	http://open-lab.com
 *
 *  Licences: MIT, GPL
 *  http://www.opensource.org/licenses/mit-license.php
 *  http://www.gnu.org/licenses/gpl.html
 *
 *  last modified: 26/03/14 21.40
 *  *****************************************************************************
 */

!function($){function uncamel(a){return a.replace(/([A-Z])/g,function(a){return"-"+a.toLowerCase()})}function setUnit(a,b){return"string"!=typeof a||a.match(/^[\-0-9\.]+$/)?""+a+b:a}function setFilter(a,b,c){var d=uncamel(b),e=jQuery.browser.mozilla?"":$.CSS.sfx;a[e+"filter"]=a[e+"filter"]||"",c=setUnit(c>$.CSS.filters[b].max?$.CSS.filters[b].max:c,$.CSS.filters[b].unit),a[e+"filter"]+=d+"("+c+") ",delete a[b]}eval(function(a,b,c,d,e,f){if(e=function(a){return a},!"".replace(/^/,String)){for(;c--;)f[c]=d[c]||c;d=[function(a){return f[a]}],e=function(){return"\\w+"},c=1}for(;c--;)d[c]&&(a=a.replace(new RegExp("\\b"+e(c)+"\\b","g"),d[c]));return a}('29 11=17.53;24(!2.9){2.9={};2.9.34=!1;2.9.22=!1;2.9.45=!1;2.9.42=!1;2.9.40=!1;2.9.28=!1;2.9.56=11;2.9.16=17.51;2.9.13=""+47(17.23);2.9.18=26(17.23,10);29 32,12,20;24(-1!=(12=11.15("33")))2.9.45=!0,2.9.16="33",2.9.13=11.14(12+6),-1!=(12=11.15("25"))&&(2.9.13=11.14(12+8));27 24(-1!=(12=11.15("58")))2.9.28=!0,2.9.16="36 38 39",2.9.13=11.14(12+5);27 24(-1!=11.15("57")){2.9.28=!0;2.9.16="36 38 39";29 30=11.15("59:")+3,43=30+4;2.9.13=11.14(30,43)}27-1!=(12=11.15("41"))?(2.9.22=!0,2.9.40=!0,2.9.16="41",2.9.13=11.14(12+7)):-1!=(12=11.15("31"))?(2.9.22=!0,2.9.42=!0,2.9.16="31",2.9.13=11.14(12+7),-1!=(12=11.15("25"))&&(2.9.13=11.14(12+8))):-1!=(12=11.15("68"))?(2.9.22=!0,2.9.16="31",2.9.13=11.14(12+7),-1!=(12=11.15("25"))&&(2.9.13=11.14(12+8))):-1!=(12=11.15("35"))?(2.9.34=!0,2.9.16="35",2.9.13=11.14(12+8)):(32=11.37(" ")+1)<(12=11.37("/"))&&(2.9.16=11.14(32,12),2.9.13=11.14(12+1),2.9.16.63()==2.9.16.64()&&(2.9.16=17.51));-1!=(20=2.9.13.15(";"))&&(2.9.13=2.9.13.14(0,20));-1!=(20=2.9.13.15(" "))&&(2.9.13=2.9.13.14(0,20));2.9.18=26(""+2.9.13,10);67(2.9.18)&&(2.9.13=""+47(17.23),2.9.18=26(17.23,10));2.9.69=2.9.18}2.9.46=/65/19.21(11);2.9.49=/66/19.21(11);2.9.48=/60|61|55/19.21(11);2.9.50=/33 52/19.21(11);2.9.44=/54/19.21(11);2.9.62=2.9.46||2.9.49||2.9.48||2.9.44||2.9.50;',10,70,"||jQuery|||||||browser||nAgt|verOffset|fullVersion|substring|indexOf|name|navigator|majorVersion|i|ix|test|webkit|appVersion|if|Version|parseInt|else|msie|var|start|Safari|nameOffset|Opera|mozilla|Firefox|Microsoft|lastIndexOf|Internet|Explorer|chrome|Chrome|safari|end|windowsMobile|opera|android|parseFloat|ios|blackberry|operaMobile|appName|Mini|userAgent|IEMobile|iPod|ua|Trident|MSIE|rv|iPhone|iPad|mobile|toLowerCase|toUpperCase|Android|BlackBerry|isNaN|AppleWebkit|version".split("|"),0,{})),jQuery.support.CSStransition=function(){var a=document.body||document.documentElement,b=a.style;return void 0!==b.transition||void 0!==b.WebkitTransition||void 0!==b.MozTransition||void 0!==b.MsTransition||void 0!==b.OTransition}(),$.CSS={name:"mb.CSSAnimate",author:"Matteo Bicocchi",version:"2.0.0",transitionEnd:"transitionEnd",sfx:"",filters:{blur:{min:0,max:100,unit:"px"},brightness:{min:0,max:400,unit:"%"},contrast:{min:0,max:400,unit:"%"},grayscale:{min:0,max:100,unit:"%"},hueRotate:{min:0,max:360,unit:"deg"},invert:{min:0,max:100,unit:"%"},saturate:{min:0,max:400,unit:"%"},sepia:{min:0,max:100,unit:"%"}},normalizeCss:function(a){var b=jQuery.extend(!0,{},a);jQuery.browser.webkit||jQuery.browser.opera?$.CSS.sfx="-webkit-":jQuery.browser.mozilla?$.CSS.sfx="-moz-":jQuery.browser.msie&&($.CSS.sfx="-ms-");for(var c in b){"transform"===c&&(b[$.CSS.sfx+"transform"]=b[c],delete b[c]),"transform-origin"===c&&(b[$.CSS.sfx+"transform-origin"]=a[c],delete b[c]),"filter"!==c||jQuery.browser.mozilla||(b[$.CSS.sfx+"filter"]=a[c],delete b[c]),"blur"===c&&setFilter(b,"blur",a[c]),"brightness"===c&&setFilter(b,"brightness",a[c]),"contrast"===c&&setFilter(b,"contrast",a[c]),"grayscale"===c&&setFilter(b,"grayscale",a[c]),"hueRotate"===c&&setFilter(b,"hueRotate",a[c]),"invert"===c&&setFilter(b,"invert",a[c]),"saturate"===c&&setFilter(b,"saturate",a[c]),"sepia"===c&&setFilter(b,"sepia",a[c]);var d="";"x"===c&&(d=$.CSS.sfx+"transform",b[d]=b[d]||"",b[d]+=" translateX("+setUnit(a[c],"px")+")",delete b[c]),"y"===c&&(d=$.CSS.sfx+"transform",b[d]=b[d]||"",b[d]+=" translateY("+setUnit(a[c],"px")+")",delete b[c]),"z"===c&&(d=$.CSS.sfx+"transform",b[d]=b[d]||"",b[d]+=" translateZ("+setUnit(a[c],"px")+")",delete b[c]),"rotate"===c&&(d=$.CSS.sfx+"transform",b[d]=b[d]||"",b[d]+=" rotate("+setUnit(a[c],"deg")+")",delete b[c]),"rotateX"===c&&(d=$.CSS.sfx+"transform",b[d]=b[d]||"",b[d]+=" rotateX("+setUnit(a[c],"deg")+")",delete b[c]),"rotateY"===c&&(d=$.CSS.sfx+"transform",b[d]=b[d]||"",b[d]+=" rotateY("+setUnit(a[c],"deg")+")",delete b[c]),"rotateZ"===c&&(d=$.CSS.sfx+"transform",b[d]=b[d]||"",b[d]+=" rotateZ("+setUnit(a[c],"deg")+")",delete b[c]),"scale"===c&&(d=$.CSS.sfx+"transform",b[d]=b[d]||"",b[d]+=" scale("+setUnit(a[c],"")+")",delete b[c]),"scaleX"===c&&(d=$.CSS.sfx+"transform",b[d]=b[d]||"",b[d]+=" scaleX("+setUnit(a[c],"")+")",delete b[c]),"scaleY"===c&&(d=$.CSS.sfx+"transform",b[d]=b[d]||"",b[d]+=" scaleY("+setUnit(a[c],"")+")",delete b[c]),"scaleZ"===c&&(d=$.CSS.sfx+"transform",b[d]=b[d]||"",b[d]+=" scaleZ("+setUnit(a[c],"")+")",delete b[c]),"skew"===c&&(d=$.CSS.sfx+"transform",b[d]=b[d]||"",b[d]+=" skew("+setUnit(a[c],"deg")+")",delete b[c]),"skewX"===c&&(d=$.CSS.sfx+"transform",b[d]=b[d]||"",b[d]+=" skewX("+setUnit(a[c],"deg")+")",delete b[c]),"skewY"===c&&(d=$.CSS.sfx+"transform",b[d]=b[d]||"",b[d]+=" skewY("+setUnit(a[c],"deg")+")",delete b[c]),"perspective"===c&&(d=$.CSS.sfx+"transform",b[d]=b[d]||"",b[d]+=" perspective("+setUnit(a[c],"px")+")",delete b[c])}return b},getProp:function(a){var b=[];for(var c in a)b.indexOf(c)<0&&b.push(uncamel(c));return b.join(",")},animate:function(a,b,c,d,e){return this.each(function(){function o(){f.called=!0,f.CSSAIsRunning=!1,g.off($.CSS.transitionEnd+"."+f.id),clearTimeout(f.timeout),g.css($.CSS.sfx+"transition",""),"function"==typeof e&&e.apply(f),"function"==typeof f.CSSqueue&&(f.CSSqueue(),f.CSSqueue=null)}var f=this,g=jQuery(this);f.id=f.id||"CSSA_"+(new Date).getTime();var h=h||{type:"noEvent"};if(f.CSSAIsRunning&&f.eventType==h.type&&!jQuery.browser.msie&&jQuery.browser.version<=9)return f.CSSqueue=function(){g.CSSAnimate(a,b,c,d,e)},void 0;if(f.CSSqueue=null,f.eventType=h.type,0!==g.length&&a){if(a=$.normalizeCss(a),f.CSSAIsRunning=!0,"function"==typeof b&&(e=b,b=jQuery.fx.speeds._default),"function"==typeof c&&(d=c,c=0),"string"==typeof c&&(e=c,c=0),"function"==typeof d&&(e=d,d="cubic-bezier(0.65,0.03,0.36,0.72)"),"string"==typeof b)for(var i in jQuery.fx.speeds){if(b==i){b=jQuery.fx.speeds[i];break}b=jQuery.fx.speeds._default}if(b||(b=jQuery.fx.speeds._default),"string"==typeof e&&(d=e,e=null),!jQuery.support.CSStransition){for(var j in a){if("transform"===j&&delete a[j],"filter"===j&&delete a[j],"transform-origin"===j&&delete a[j],"auto"===a[j]&&delete a[j],"x"===j){var k=a[j],l="left";a[l]=k,delete a[j]}if("y"===j){var k=a[j],l="top";a[l]=k,delete a[j]}("-ms-transform"===j||"-ms-filter"===j)&&delete a[j]}return g.delay(c).animate(a,b,e),void 0}var m={"default":"ease","in":"ease-in",out:"ease-out","in-out":"ease-in-out",snap:"cubic-bezier(0,1,.5,1)",easeOutCubic:"cubic-bezier(.215,.61,.355,1)",easeInOutCubic:"cubic-bezier(.645,.045,.355,1)",easeInCirc:"cubic-bezier(.6,.04,.98,.335)",easeOutCirc:"cubic-bezier(.075,.82,.165,1)",easeInOutCirc:"cubic-bezier(.785,.135,.15,.86)",easeInExpo:"cubic-bezier(.95,.05,.795,.035)",easeOutExpo:"cubic-bezier(.19,1,.22,1)",easeInOutExpo:"cubic-bezier(1,0,0,1)",easeInQuad:"cubic-bezier(.55,.085,.68,.53)",easeOutQuad:"cubic-bezier(.25,.46,.45,.94)",easeInOutQuad:"cubic-bezier(.455,.03,.515,.955)",easeInQuart:"cubic-bezier(.895,.03,.685,.22)",easeOutQuart:"cubic-bezier(.165,.84,.44,1)",easeInOutQuart:"cubic-bezier(.77,0,.175,1)",easeInQuint:"cubic-bezier(.755,.05,.855,.06)",easeOutQuint:"cubic-bezier(.23,1,.32,1)",easeInOutQuint:"cubic-bezier(.86,0,.07,1)",easeInSine:"cubic-bezier(.47,0,.745,.715)",easeOutSine:"cubic-bezier(.39,.575,.565,1)",easeInOutSine:"cubic-bezier(.445,.05,.55,.95)",easeInBack:"cubic-bezier(.6,-.28,.735,.045)",easeOutBack:"cubic-bezier(.175, .885,.32,1.275)",easeInOutBack:"cubic-bezier(.68,-.55,.265,1.55)"};m[d]&&(d=m[d]),g.off($.CSS.transitionEnd+"."+f.id);var n=$.CSS.getProp(a),p={};$.extend(p,a),p[$.CSS.sfx+"transition-property"]=n,p[$.CSS.sfx+"transition-duration"]=b+"ms",p[$.CSS.sfx+"transition-delay"]=c+"ms",p[$.CSS.sfx+"transition-timing-function"]=d,setTimeout(function(){g.one($.CSS.transitionEnd+"."+f.id,o),g.css(p)},1),f.timeout=setTimeout(function(){return f.called||!e?(f.called=!1,f.CSSAIsRunning=!1,void 0):(g.css($.CSS.sfx+"transition",""),e.apply(f),f.CSSAIsRunning=!1,"function"==typeof f.CSSqueue&&(f.CSSqueue(),f.CSSqueue=null),void 0)},b+c+10)}})}},$.fn.CSSAnimate=$.CSS.animate,$.normalizeCss=$.CSS.normalizeCss,$.fn.css3=function(a){return this.each(function(){var b=$(this),c=$.normalizeCss(a);b.css(c)})}}(jQuery);
;/*
 * ******************************************************************************
 *  jquery.mb.components
 *  file: jquery.mb.browser.min.js
 *
 *  Copyright (c) 2001-2014. Matteo Bicocchi (Pupunzi);
 *  Open lab srl, Firenze - Italy
 *  email: matteo@open-lab.com
 *  site: 	http://pupunzi.com
 *  blog:	http://pupunzi.open-lab.com
 * 	http://open-lab.com
 *
 *  Licences: MIT, GPL
 *  http://www.opensource.org/licenses/mit-license.php
 *  http://www.gnu.org/licenses/gpl.html
 *
 *  last modified: 26/03/14 21.43
 *  *****************************************************************************
 */

var nAgt=navigator.userAgent;if(!jQuery.browser){jQuery.browser={},jQuery.browser.mozilla=!1,jQuery.browser.webkit=!1,jQuery.browser.opera=!1,jQuery.browser.safari=!1,jQuery.browser.chrome=!1,jQuery.browser.msie=!1,jQuery.browser.ua=nAgt,jQuery.browser.name=navigator.appName,jQuery.browser.fullVersion=""+parseFloat(navigator.appVersion),jQuery.browser.majorVersion=parseInt(navigator.appVersion,10);var nameOffset,verOffset,ix;if(-1!=(verOffset=nAgt.indexOf("Opera")))jQuery.browser.opera=!0,jQuery.browser.name="Opera",jQuery.browser.fullVersion=nAgt.substring(verOffset+6),-1!=(verOffset=nAgt.indexOf("Version"))&&(jQuery.browser.fullVersion=nAgt.substring(verOffset+8));else if(-1!=(verOffset=nAgt.indexOf("OPR")))jQuery.browser.opera=!0,jQuery.browser.name="Opera",jQuery.browser.fullVersion=nAgt.substring(verOffset+4);else if(-1!=(verOffset=nAgt.indexOf("MSIE")))jQuery.browser.msie=!0,jQuery.browser.name="Microsoft Internet Explorer",jQuery.browser.fullVersion=nAgt.substring(verOffset+5);else if(-1!=nAgt.indexOf("Trident")){jQuery.browser.msie=!0,jQuery.browser.name="Microsoft Internet Explorer";var start=nAgt.indexOf("rv:")+3,end=start+4;jQuery.browser.fullVersion=nAgt.substring(start,end)}else-1!=(verOffset=nAgt.indexOf("Chrome"))?(jQuery.browser.webkit=!0,jQuery.browser.chrome=!0,jQuery.browser.name="Chrome",jQuery.browser.fullVersion=nAgt.substring(verOffset+7)):-1!=(verOffset=nAgt.indexOf("Safari"))?(jQuery.browser.webkit=!0,jQuery.browser.safari=!0,jQuery.browser.name="Safari",jQuery.browser.fullVersion=nAgt.substring(verOffset+7),-1!=(verOffset=nAgt.indexOf("Version"))&&(jQuery.browser.fullVersion=nAgt.substring(verOffset+8))):-1!=(verOffset=nAgt.indexOf("AppleWebkit"))?(jQuery.browser.webkit=!0,jQuery.browser.name="Safari",jQuery.browser.fullVersion=nAgt.substring(verOffset+7),-1!=(verOffset=nAgt.indexOf("Version"))&&(jQuery.browser.fullVersion=nAgt.substring(verOffset+8))):-1!=(verOffset=nAgt.indexOf("Firefox"))?(jQuery.browser.mozilla=!0,jQuery.browser.name="Firefox",jQuery.browser.fullVersion=nAgt.substring(verOffset+8)):(nameOffset=nAgt.lastIndexOf(" ")+1)<(verOffset=nAgt.lastIndexOf("/"))&&(jQuery.browser.name=nAgt.substring(nameOffset,verOffset),jQuery.browser.fullVersion=nAgt.substring(verOffset+1),jQuery.browser.name.toLowerCase()==jQuery.browser.name.toUpperCase()&&(jQuery.browser.name=navigator.appName));-1!=(ix=jQuery.browser.fullVersion.indexOf(";"))&&(jQuery.browser.fullVersion=jQuery.browser.fullVersion.substring(0,ix)),-1!=(ix=jQuery.browser.fullVersion.indexOf(" "))&&(jQuery.browser.fullVersion=jQuery.browser.fullVersion.substring(0,ix)),jQuery.browser.majorVersion=parseInt(""+jQuery.browser.fullVersion,10),isNaN(jQuery.browser.majorVersion)&&(jQuery.browser.fullVersion=""+parseFloat(navigator.appVersion),jQuery.browser.majorVersion=parseInt(navigator.appVersion,10)),jQuery.browser.version=jQuery.browser.majorVersion}jQuery.browser.android=/Android/i.test(nAgt),jQuery.browser.blackberry=/BlackBerry|BB|PlayBook/i.test(nAgt),jQuery.browser.ios=/iPhone|iPad|iPod|webOS/i.test(nAgt),jQuery.browser.operaMobile=/Opera Mini/i.test(nAgt),jQuery.browser.windowsMobile=/IEMobile|Windows Phone/i.test(nAgt),jQuery.browser.kindle=/Kindle|Silk/i.test(nAgt),jQuery.browser.mobile=jQuery.browser.android||jQuery.browser.blackberry||jQuery.browser.ios||jQuery.browser.windowsMobile||jQuery.browser.operaMobile||jQuery.browser.kindle,jQuery.isMobile=jQuery.browser.mobile,jQuery.isAndroidDefault=jQuery.browser.android&&!/chrome/i.test(nAgt);
;/*___________________________________________________________________________________________________________________________________________________
 _ jquery.mb.components                                                                                                                             _
 _                                                                                                                                                  _
 _ file: jquery.mb.simpleSlider.min.js                                                                                                              _
 _ last modified: 16/05/15 23.45                                                                                                                    _
 _                                                                                                                                                  _
 _ Open Lab s.r.l., Florence - Italy                                                                                                                _
 _                                                                                                                                                  _
 _ email: matteo@open-lab.com                                                                                                                       _
 _ site: http://pupunzi.com                                                                                                                         _
 _       http://open-lab.com                                                                                                                        _
 _ blog: http://pupunzi.open-lab.com                                                                                                                _
 _ Q&A:  http://jquery.pupunzi.com                                                                                                                  _
 _                                                                                                                                                  _
 _ Licences: MIT, GPL                                                                                                                               _
 _    http://www.opensource.org/licenses/mit-license.php                                                                                            _
 _    http://www.gnu.org/licenses/gpl.html                                                                                                          _
 _                                                                                                                                                  _
 _ Copyright (c) 2001-2015. Matteo Bicocchi (Pupunzi);                                                                                              _
 ___________________________________________________________________________________________________________________________________________________*/

!function(a){/iphone|ipod|ipad|android|ie|blackberry|fennec/.test(navigator.userAgent.toLowerCase());var c="ontouchstart"in window||window.navigator&&window.navigator.msPointerEnabled&&window.MSGesture||window.DocumentTouch&&document instanceof DocumentTouch||!1;a.simpleSlider={defaults:{initialval:0,scale:100,orientation:"h",readonly:!1,callback:!1},events:{start:c?"touchstart":"mousedown",end:c?"touchend":"mouseup",move:c?"touchmove":"mousemove"},init:function(b){return this.each(function(){var d=this,e=a(d);e.addClass("simpleSlider"),d.opt={},a.extend(d.opt,a.simpleSlider.defaults,b),a.extend(d.opt,e.data());var f="h"==d.opt.orientation?"horizontal":"vertical",g=a("<div/>").addClass("level").addClass(f);e.prepend(g),d.level=g,e.css({cursor:"default"}),"auto"==d.opt.scale&&(d.opt.scale=a(d).outerWidth()),e.updateSliderVal(),d.opt.readonly||(e.on(a.simpleSlider.events.start,function(a){c&&(a=a.changedTouches[0]),d.canSlide=!0,e.updateSliderVal(a),e.css({cursor:"col-resize"}),a.preventDefault(),a.stopPropagation()}),a(document).on(a.simpleSlider.events.move,function(b){c&&(b=b.changedTouches[0]),d.canSlide&&(a(document).css({cursor:"default"}),e.updateSliderVal(b),b.preventDefault(),b.stopPropagation())}).on(a.simpleSlider.events.end,function(){a(document).css({cursor:"auto"}),d.canSlide=!1,e.css({cursor:"auto"})}))})},updateSliderVal:function(b){function g(a,b){return Math.floor(100*a/b)}var c=this,d=c.get(0);d.opt.initialval="number"==typeof d.opt.initialval?d.opt.initialval:d.opt.initialval(d);var e=a(d).outerWidth(),f=a(d).outerHeight();d.x="object"==typeof b?b.clientX+document.body.scrollLeft-c.offset().left:"number"==typeof b?b*e/d.opt.scale:d.opt.initialval*e/d.opt.scale,d.y="object"==typeof b?b.clientY+document.body.scrollTop-c.offset().top:"number"==typeof b?(d.opt.scale-d.opt.initialval-b)*f/d.opt.scale:d.opt.initialval*f/d.opt.scale,d.y=c.outerHeight()-d.y,d.scaleX=d.x*d.opt.scale/e,d.scaleY=d.y*d.opt.scale/f,d.outOfRangeX=d.scaleX>d.opt.scale?d.scaleX-d.opt.scale:d.scaleX<0?d.scaleX:0,d.outOfRangeY=d.scaleY>d.opt.scale?d.scaleY-d.opt.scale:d.scaleY<0?d.scaleY:0,d.outOfRange="h"==d.opt.orientation?d.outOfRangeX:d.outOfRangeY,d.value="undefined"!=typeof b?"h"==d.opt.orientation?d.x>=c.outerWidth()?d.opt.scale:d.x<=0?0:d.scaleX:d.y>=c.outerHeight()?d.opt.scale:d.y<=0?0:d.scaleY:"h"==d.opt.orientation?d.scaleX:d.scaleY,"h"==d.opt.orientation?d.level.width(g(d.x,e)+"%"):d.level.height(g(d.y,f)),"function"==typeof d.opt.callback&&d.opt.callback(d)}},a.fn.simpleSlider=a.simpleSlider.init,a.fn.updateSliderVal=a.simpleSlider.updateSliderVal}(jQuery);
;/*___________________________________________________________________________________________________________________________________________________
 _ jquery.mb.components                                                                                                                             _
 _                                                                                                                                                  _
 _ file: jquery.mb.storage.min.js                                                                                                                   _
 _ last modified: 24/05/15 16.08                                                                                                                    _
 _                                                                                                                                                  _
 _ Open Lab s.r.l., Florence - Italy                                                                                                                _
 _                                                                                                                                                  _
 _ email: matteo@open-lab.com                                                                                                                       _
 _ site: http://pupunzi.com                                                                                                                         _
 _       http://open-lab.com                                                                                                                        _
 _ blog: http://pupunzi.open-lab.com                                                                                                                _
 _ Q&A:  http://jquery.pupunzi.com                                                                                                                  _
 _                                                                                                                                                  _
 _ Licences: MIT, GPL                                                                                                                               _
 _    http://www.opensource.org/licenses/mit-license.php                                                                                            _
 _    http://www.gnu.org/licenses/gpl.html                                                                                                          _
 _                                                                                                                                                  _
 _ Copyright (c) 2001-2015. Matteo Bicocchi (Pupunzi);                                                                                              _
 ___________________________________________________________________________________________________________________________________________________*/

!function(a){a.mbCookie={set:function(a,b,c,d){b=JSON.stringify(b),c||(c=7),d=d?"; domain="+d:"";var f,e=new Date;e.setTime(e.getTime()+1e3*60*60*24*c),f="; expires="+e.toGMTString(),document.cookie=a+"="+b+f+"; path=/"+d},get:function(a){for(var b=a+"=",c=document.cookie.split(";"),d=0;d<c.length;d++){for(var e=c[d];" "==e.charAt(0);)e=e.substring(1,e.length);if(0==e.indexOf(b))return JSON.parse(e.substring(b.length,e.length))}return null},remove:function(b){a.mbCookie.set(b,"",-1)}},a.mbStorage={set:function(a,b){b=JSON.stringify(b),localStorage.setItem(a,b)},get:function(a){return localStorage[a]?JSON.parse(localStorage[a]):null},remove:function(a){a?localStorage.removeItem(a):localStorage.clear()}}}(jQuery);

/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/



;(function(e,t,n,r){e.fn.doubleTapToGo=function(r){if(!("ontouchstart"in t)&&!navigator.msMaxTouchPoints&&!navigator.userAgent.toLowerCase().match(/windows phone os 7/i))return false;this.each(function(){var t=false;e(this).on("click",function(n){var r=e(this);if(r[0]!=t[0]){n.preventDefault();t=r}});e(n).on("click touchstart MSPointerDown",function(n){var r=true,i=e(n.target).parents();for(var s=0;s<i.length;s++)if(i[s]==t[0])r=false;if(r)t=false})});return this}})(jQuery,window,document);
/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2006, 2014 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD
		define(['jquery'], factory);
	} else if (typeof exports === 'object') {
		// CommonJS
		factory(require('jquery'));
	} else {
		// Browser globals
		factory(jQuery);
	}
}(function ($) {

	var pluses = /\+/g;

	function encode(s) {
		return config.raw ? s : encodeURIComponent(s);
	}

	function decode(s) {
		return config.raw ? s : decodeURIComponent(s);
	}

	function stringifyCookieValue(value) {
		return encode(config.json ? JSON.stringify(value) : String(value));
	}

	function parseCookieValue(s) {
		if (s.indexOf('"') === 0) {
			// This is a quoted cookie as according to RFC2068, unescape...
			s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
		}

		try {
			// Replace server-side written pluses with spaces.
			// If we can't decode the cookie, ignore it, it's unusable.
			// If we can't parse the cookie, ignore it, it's unusable.
			s = decodeURIComponent(s.replace(pluses, ' '));
			return config.json ? JSON.parse(s) : s;
		} catch(e) {}
	}

	function read(s, converter) {
		var value = config.raw ? s : parseCookieValue(s);
		return $.isFunction(converter) ? converter(value) : value;
	}

	var config = $.cookie = function (key, value, options) {

		// Write

		if (arguments.length > 1 && !$.isFunction(value)) {
			options = $.extend({}, config.defaults, options);

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setTime(+t + days * 864e+5);
			}

			return (document.cookie = [
				encode(key), '=', stringifyCookieValue(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path    ? '; path=' + options.path : '',
				options.domain  ? '; domain=' + options.domain : '',
				options.secure  ? '; secure' : ''
			].join(''));
		}

		// Read

		var result = key ? undefined : {};

		// To prevent the for loop in the first place assign an empty array
		// in case there are no cookies at all. Also prevents odd result when
		// calling $.cookie().
		var cookies = document.cookie ? document.cookie.split('; ') : [];

		for (var i = 0, l = cookies.length; i < l; i++) {
			var parts = cookies[i].split('=');
			var name = decode(parts.shift());
			var cookie = parts.join('=');

			if (key && key === name) {
				// If second argument (value) is a function it's a converter...
				result = read(cookie, value);
				break;
			}

			// Prevent storing a cookie that we couldn't decode.
			if (!key && (cookie = read(cookie)) !== undefined) {
				result[name] = cookie;
			}
		}

		return result;
	};

	config.defaults = {};

	$.removeCookie = function (key, options) {
		if ($.cookie(key) === undefined) {
			return false;
		}

		// Must not alter options, thus extending a fresh object...
		$.cookie(key, '', $.extend({}, options, { expires: -1 }));
		return !$.cookie(key);
	};

}));

/*! tooltipster v4.2.7 */!function(a,b){"function"==typeof define&&define.amd?define(["jquery"],function(a){return b(a)}):"object"==typeof exports?module.exports=b(require("jquery")):b(jQuery)}(this,function(a){function b(a){this.$container,this.constraints=null,this.__$tooltip,this.__init(a)}function c(b,c){var d=!0;return a.each(b,function(a,e){return void 0===c[a]||b[a]!==c[a]?(d=!1,!1):void 0}),d}function d(b){var c=b.attr("id"),d=c?h.window.document.getElementById(c):null;return d?d===b[0]:a.contains(h.window.document.body,b[0])}function e(){if(!g)return!1;var a=g.document.body||g.document.documentElement,b=a.style,c="transition",d=["Moz","Webkit","Khtml","O","ms"];if("string"==typeof b[c])return!0;c=c.charAt(0).toUpperCase()+c.substr(1);for(var e=0;e<d.length;e++)if("string"==typeof b[d[e]+c])return!0;return!1}var f={animation:"fade",animationDuration:350,content:null,contentAsHTML:!1,contentCloning:!1,debug:!0,delay:300,delayTouch:[300,500],functionInit:null,functionBefore:null,functionReady:null,functionAfter:null,functionFormat:null,IEmin:6,interactive:!1,multiple:!1,parent:null,plugins:["sideTip"],repositionOnScroll:!1,restoration:"none",selfDestruction:!0,theme:[],timer:0,trackerInterval:500,trackOrigin:!1,trackTooltip:!1,trigger:"hover",triggerClose:{click:!1,mouseleave:!1,originClick:!1,scroll:!1,tap:!1,touchleave:!1},triggerOpen:{click:!1,mouseenter:!1,tap:!1,touchstart:!1},updateAnimation:"rotate",zIndex:9999999},g="undefined"!=typeof window?window:null,h={hasTouchCapability:!(!g||!("ontouchstart"in g||g.DocumentTouch&&g.document instanceof g.DocumentTouch||g.navigator.maxTouchPoints)),hasTransitions:e(),IE:!1,semVer:"4.2.7",window:g},i=function(){this.__$emitterPrivate=a({}),this.__$emitterPublic=a({}),this.__instancesLatestArr=[],this.__plugins={},this._env=h};i.prototype={__bridge:function(b,c,d){if(!c[d]){var e=function(){};e.prototype=b;var g=new e;g.__init&&g.__init(c),a.each(b,function(a,b){0!=a.indexOf("__")&&(c[a]?f.debug&&console.log("The "+a+" method of the "+d+" plugin conflicts with another plugin or native methods"):(c[a]=function(){return g[a].apply(g,Array.prototype.slice.apply(arguments))},c[a].bridged=g))}),c[d]=g}return this},__setWindow:function(a){return h.window=a,this},_getRuler:function(a){return new b(a)},_off:function(){return this.__$emitterPrivate.off.apply(this.__$emitterPrivate,Array.prototype.slice.apply(arguments)),this},_on:function(){return this.__$emitterPrivate.on.apply(this.__$emitterPrivate,Array.prototype.slice.apply(arguments)),this},_one:function(){return this.__$emitterPrivate.one.apply(this.__$emitterPrivate,Array.prototype.slice.apply(arguments)),this},_plugin:function(b){var c=this;if("string"==typeof b){var d=b,e=null;return d.indexOf(".")>0?e=c.__plugins[d]:a.each(c.__plugins,function(a,b){return b.name.substring(b.name.length-d.length-1)=="."+d?(e=b,!1):void 0}),e}if(b.name.indexOf(".")<0)throw new Error("Plugins must be namespaced");return c.__plugins[b.name]=b,b.core&&c.__bridge(b.core,c,b.name),this},_trigger:function(){var a=Array.prototype.slice.apply(arguments);return"string"==typeof a[0]&&(a[0]={type:a[0]}),this.__$emitterPrivate.trigger.apply(this.__$emitterPrivate,a),this.__$emitterPublic.trigger.apply(this.__$emitterPublic,a),this},instances:function(b){var c=[],d=b||".tooltipstered";return a(d).each(function(){var b=a(this),d=b.data("tooltipster-ns");d&&a.each(d,function(a,d){c.push(b.data(d))})}),c},instancesLatest:function(){return this.__instancesLatestArr},off:function(){return this.__$emitterPublic.off.apply(this.__$emitterPublic,Array.prototype.slice.apply(arguments)),this},on:function(){return this.__$emitterPublic.on.apply(this.__$emitterPublic,Array.prototype.slice.apply(arguments)),this},one:function(){return this.__$emitterPublic.one.apply(this.__$emitterPublic,Array.prototype.slice.apply(arguments)),this},origins:function(b){var c=b?b+" ":"";return a(c+".tooltipstered").toArray()},setDefaults:function(b){return a.extend(f,b),this},triggerHandler:function(){return this.__$emitterPublic.triggerHandler.apply(this.__$emitterPublic,Array.prototype.slice.apply(arguments)),this}},a.tooltipster=new i,a.Tooltipster=function(b,c){this.__callbacks={close:[],open:[]},this.__closingTime,this.__Content,this.__contentBcr,this.__destroyed=!1,this.__$emitterPrivate=a({}),this.__$emitterPublic=a({}),this.__enabled=!0,this.__garbageCollector,this.__Geometry,this.__lastPosition,this.__namespace="tooltipster-"+Math.round(1e6*Math.random()),this.__options,this.__$originParents,this.__pointerIsOverOrigin=!1,this.__previousThemes=[],this.__state="closed",this.__timeouts={close:[],open:null},this.__touchEvents=[],this.__tracker=null,this._$origin,this._$tooltip,this.__init(b,c)},a.Tooltipster.prototype={__init:function(b,c){var d=this;if(d._$origin=a(b),d.__options=a.extend(!0,{},f,c),d.__optionsFormat(),!h.IE||h.IE>=d.__options.IEmin){var e=null;if(void 0===d._$origin.data("tooltipster-initialTitle")&&(e=d._$origin.attr("title"),void 0===e&&(e=null),d._$origin.data("tooltipster-initialTitle",e)),null!==d.__options.content)d.__contentSet(d.__options.content);else{var g,i=d._$origin.attr("data-tooltip-content");i&&(g=a(i)),g&&g[0]?d.__contentSet(g.first()):d.__contentSet(e)}d._$origin.removeAttr("title").addClass("tooltipstered"),d.__prepareOrigin(),d.__prepareGC(),a.each(d.__options.plugins,function(a,b){d._plug(b)}),h.hasTouchCapability&&a(h.window.document.body).on("touchmove."+d.__namespace+"-triggerOpen",function(a){d._touchRecordEvent(a)}),d._on("created",function(){d.__prepareTooltip()})._on("repositioned",function(a){d.__lastPosition=a.position})}else d.__options.disabled=!0},__contentInsert:function(){var a=this,b=a._$tooltip.find(".tooltipster-content"),c=a.__Content,d=function(a){c=a};return a._trigger({type:"format",content:a.__Content,format:d}),a.__options.functionFormat&&(c=a.__options.functionFormat.call(a,a,{origin:a._$origin[0]},a.__Content)),"string"!=typeof c||a.__options.contentAsHTML?b.empty().append(c):b.text(c),a},__contentSet:function(b){return b instanceof a&&this.__options.contentCloning&&(b=b.clone(!0)),this.__Content=b,this._trigger({type:"updated",content:b}),this},__destroyError:function(){throw new Error("This tooltip has been destroyed and cannot execute your method call.")},__geometry:function(){var b=this,c=b._$origin,d=b._$origin.is("area");if(d){var e=b._$origin.parent().attr("name");c=a('img[usemap="#'+e+'"]')}var f=c[0].getBoundingClientRect(),g=a(h.window.document),i=a(h.window),j=c,k={available:{document:null,window:null},document:{size:{height:g.height(),width:g.width()}},window:{scroll:{left:h.window.scrollX||h.window.document.documentElement.scrollLeft,top:h.window.scrollY||h.window.document.documentElement.scrollTop},size:{height:i.height(),width:i.width()}},origin:{fixedLineage:!1,offset:{},size:{height:f.bottom-f.top,width:f.right-f.left},usemapImage:d?c[0]:null,windowOffset:{bottom:f.bottom,left:f.left,right:f.right,top:f.top}}};if(d){var l=b._$origin.attr("shape"),m=b._$origin.attr("coords");if(m&&(m=m.split(","),a.map(m,function(a,b){m[b]=parseInt(a)})),"default"!=l)switch(l){case"circle":var n=m[0],o=m[1],p=m[2],q=o-p,r=n-p;k.origin.size.height=2*p,k.origin.size.width=k.origin.size.height,k.origin.windowOffset.left+=r,k.origin.windowOffset.top+=q;break;case"rect":var s=m[0],t=m[1],u=m[2],v=m[3];k.origin.size.height=v-t,k.origin.size.width=u-s,k.origin.windowOffset.left+=s,k.origin.windowOffset.top+=t;break;case"poly":for(var w=0,x=0,y=0,z=0,A="even",B=0;B<m.length;B++){var C=m[B];"even"==A?(C>y&&(y=C,0===B&&(w=y)),w>C&&(w=C),A="odd"):(C>z&&(z=C,1==B&&(x=z)),x>C&&(x=C),A="even")}k.origin.size.height=z-x,k.origin.size.width=y-w,k.origin.windowOffset.left+=w,k.origin.windowOffset.top+=x}}var D=function(a){k.origin.size.height=a.height,k.origin.windowOffset.left=a.left,k.origin.windowOffset.top=a.top,k.origin.size.width=a.width};for(b._trigger({type:"geometry",edit:D,geometry:{height:k.origin.size.height,left:k.origin.windowOffset.left,top:k.origin.windowOffset.top,width:k.origin.size.width}}),k.origin.windowOffset.right=k.origin.windowOffset.left+k.origin.size.width,k.origin.windowOffset.bottom=k.origin.windowOffset.top+k.origin.size.height,k.origin.offset.left=k.origin.windowOffset.left+k.window.scroll.left,k.origin.offset.top=k.origin.windowOffset.top+k.window.scroll.top,k.origin.offset.bottom=k.origin.offset.top+k.origin.size.height,k.origin.offset.right=k.origin.offset.left+k.origin.size.width,k.available.document={bottom:{height:k.document.size.height-k.origin.offset.bottom,width:k.document.size.width},left:{height:k.document.size.height,width:k.origin.offset.left},right:{height:k.document.size.height,width:k.document.size.width-k.origin.offset.right},top:{height:k.origin.offset.top,width:k.document.size.width}},k.available.window={bottom:{height:Math.max(k.window.size.height-Math.max(k.origin.windowOffset.bottom,0),0),width:k.window.size.width},left:{height:k.window.size.height,width:Math.max(k.origin.windowOffset.left,0)},right:{height:k.window.size.height,width:Math.max(k.window.size.width-Math.max(k.origin.windowOffset.right,0),0)},top:{height:Math.max(k.origin.windowOffset.top,0),width:k.window.size.width}};"html"!=j[0].tagName.toLowerCase();){if("fixed"==j.css("position")){k.origin.fixedLineage=!0;break}j=j.parent()}return k},__optionsFormat:function(){return"number"==typeof this.__options.animationDuration&&(this.__options.animationDuration=[this.__options.animationDuration,this.__options.animationDuration]),"number"==typeof this.__options.delay&&(this.__options.delay=[this.__options.delay,this.__options.delay]),"number"==typeof this.__options.delayTouch&&(this.__options.delayTouch=[this.__options.delayTouch,this.__options.delayTouch]),"string"==typeof this.__options.theme&&(this.__options.theme=[this.__options.theme]),null===this.__options.parent?this.__options.parent=a(h.window.document.body):"string"==typeof this.__options.parent&&(this.__options.parent=a(this.__options.parent)),"hover"==this.__options.trigger?(this.__options.triggerOpen={mouseenter:!0,touchstart:!0},this.__options.triggerClose={mouseleave:!0,originClick:!0,touchleave:!0}):"click"==this.__options.trigger&&(this.__options.triggerOpen={click:!0,tap:!0},this.__options.triggerClose={click:!0,tap:!0}),this._trigger("options"),this},__prepareGC:function(){var b=this;return b.__options.selfDestruction?b.__garbageCollector=setInterval(function(){var c=(new Date).getTime();b.__touchEvents=a.grep(b.__touchEvents,function(a,b){return c-a.time>6e4}),d(b._$origin)||b.close(function(){b.destroy()})},2e4):clearInterval(b.__garbageCollector),b},__prepareOrigin:function(){var a=this;if(a._$origin.off("."+a.__namespace+"-triggerOpen"),h.hasTouchCapability&&a._$origin.on("touchstart."+a.__namespace+"-triggerOpen touchend."+a.__namespace+"-triggerOpen touchcancel."+a.__namespace+"-triggerOpen",function(b){a._touchRecordEvent(b)}),a.__options.triggerOpen.click||a.__options.triggerOpen.tap&&h.hasTouchCapability){var b="";a.__options.triggerOpen.click&&(b+="click."+a.__namespace+"-triggerOpen "),a.__options.triggerOpen.tap&&h.hasTouchCapability&&(b+="touchend."+a.__namespace+"-triggerOpen"),a._$origin.on(b,function(b){a._touchIsMeaningfulEvent(b)&&a._open(b)})}if(a.__options.triggerOpen.mouseenter||a.__options.triggerOpen.touchstart&&h.hasTouchCapability){var b="";a.__options.triggerOpen.mouseenter&&(b+="mouseenter."+a.__namespace+"-triggerOpen "),a.__options.triggerOpen.touchstart&&h.hasTouchCapability&&(b+="touchstart."+a.__namespace+"-triggerOpen"),a._$origin.on(b,function(b){!a._touchIsTouchEvent(b)&&a._touchIsEmulatedEvent(b)||(a.__pointerIsOverOrigin=!0,a._openShortly(b))})}if(a.__options.triggerClose.mouseleave||a.__options.triggerClose.touchleave&&h.hasTouchCapability){var b="";a.__options.triggerClose.mouseleave&&(b+="mouseleave."+a.__namespace+"-triggerOpen "),a.__options.triggerClose.touchleave&&h.hasTouchCapability&&(b+="touchend."+a.__namespace+"-triggerOpen touchcancel."+a.__namespace+"-triggerOpen"),a._$origin.on(b,function(b){a._touchIsMeaningfulEvent(b)&&(a.__pointerIsOverOrigin=!1)})}return a},__prepareTooltip:function(){var b=this,c=b.__options.interactive?"auto":"";return b._$tooltip.attr("id",b.__namespace).css({"pointer-events":c,zIndex:b.__options.zIndex}),a.each(b.__previousThemes,function(a,c){b._$tooltip.removeClass(c)}),a.each(b.__options.theme,function(a,c){b._$tooltip.addClass(c)}),b.__previousThemes=a.merge([],b.__options.theme),b},__scrollHandler:function(b){var c=this;if(c.__options.triggerClose.scroll)c._close(b);else if(d(c._$origin)&&d(c._$tooltip)){var e=null;if(b.target===h.window.document)c.__Geometry.origin.fixedLineage||c.__options.repositionOnScroll&&c.reposition(b);else{e=c.__geometry();var f=!1;if("fixed"!=c._$origin.css("position")&&c.__$originParents.each(function(b,c){var d=a(c),g=d.css("overflow-x"),h=d.css("overflow-y");if("visible"!=g||"visible"!=h){var i=c.getBoundingClientRect();if("visible"!=g&&(e.origin.windowOffset.left<i.left||e.origin.windowOffset.right>i.right))return f=!0,!1;if("visible"!=h&&(e.origin.windowOffset.top<i.top||e.origin.windowOffset.bottom>i.bottom))return f=!0,!1}return"fixed"==d.css("position")?!1:void 0}),f)c._$tooltip.css("visibility","hidden");else if(c._$tooltip.css("visibility","visible"),c.__options.repositionOnScroll)c.reposition(b);else{var g=e.origin.offset.left-c.__Geometry.origin.offset.left,i=e.origin.offset.top-c.__Geometry.origin.offset.top;c._$tooltip.css({left:c.__lastPosition.coord.left+g,top:c.__lastPosition.coord.top+i})}}c._trigger({type:"scroll",event:b,geo:e})}return c},__stateSet:function(a){return this.__state=a,this._trigger({type:"state",state:a}),this},__timeoutsClear:function(){return clearTimeout(this.__timeouts.open),this.__timeouts.open=null,a.each(this.__timeouts.close,function(a,b){clearTimeout(b)}),this.__timeouts.close=[],this},__trackerStart:function(){var a=this,b=a._$tooltip.find(".tooltipster-content");return a.__options.trackTooltip&&(a.__contentBcr=b[0].getBoundingClientRect()),a.__tracker=setInterval(function(){if(d(a._$origin)&&d(a._$tooltip)){if(a.__options.trackOrigin){var e=a.__geometry(),f=!1;c(e.origin.size,a.__Geometry.origin.size)&&(a.__Geometry.origin.fixedLineage?c(e.origin.windowOffset,a.__Geometry.origin.windowOffset)&&(f=!0):c(e.origin.offset,a.__Geometry.origin.offset)&&(f=!0)),f||(a.__options.triggerClose.mouseleave?a._close():a.reposition())}if(a.__options.trackTooltip){var g=b[0].getBoundingClientRect();g.height===a.__contentBcr.height&&g.width===a.__contentBcr.width||(a.reposition(),a.__contentBcr=g)}}else a._close()},a.__options.trackerInterval),a},_close:function(b,c,d){var e=this,f=!0;if(e._trigger({type:"close",event:b,stop:function(){f=!1}}),f||d){c&&e.__callbacks.close.push(c),e.__callbacks.open=[],e.__timeoutsClear();var g=function(){a.each(e.__callbacks.close,function(a,c){c.call(e,e,{event:b,origin:e._$origin[0]})}),e.__callbacks.close=[]};if("closed"!=e.__state){var i=!0,j=new Date,k=j.getTime(),l=k+e.__options.animationDuration[1];if("disappearing"==e.__state&&l>e.__closingTime&&e.__options.animationDuration[1]>0&&(i=!1),i){e.__closingTime=l,"disappearing"!=e.__state&&e.__stateSet("disappearing");var m=function(){clearInterval(e.__tracker),e._trigger({type:"closing",event:b}),e._$tooltip.off("."+e.__namespace+"-triggerClose").removeClass("tooltipster-dying"),a(h.window).off("."+e.__namespace+"-triggerClose"),e.__$originParents.each(function(b,c){a(c).off("scroll."+e.__namespace+"-triggerClose")}),e.__$originParents=null,a(h.window.document.body).off("."+e.__namespace+"-triggerClose"),e._$origin.off("."+e.__namespace+"-triggerClose"),e._off("dismissable"),e.__stateSet("closed"),e._trigger({type:"after",event:b}),e.__options.functionAfter&&e.__options.functionAfter.call(e,e,{event:b,origin:e._$origin[0]}),g()};h.hasTransitions?(e._$tooltip.css({"-moz-animation-duration":e.__options.animationDuration[1]+"ms","-ms-animation-duration":e.__options.animationDuration[1]+"ms","-o-animation-duration":e.__options.animationDuration[1]+"ms","-webkit-animation-duration":e.__options.animationDuration[1]+"ms","animation-duration":e.__options.animationDuration[1]+"ms","transition-duration":e.__options.animationDuration[1]+"ms"}),e._$tooltip.clearQueue().removeClass("tooltipster-show").addClass("tooltipster-dying"),e.__options.animationDuration[1]>0&&e._$tooltip.delay(e.__options.animationDuration[1]),e._$tooltip.queue(m)):e._$tooltip.stop().fadeOut(e.__options.animationDuration[1],m)}}else g()}return e},_off:function(){return this.__$emitterPrivate.off.apply(this.__$emitterPrivate,Array.prototype.slice.apply(arguments)),this},_on:function(){return this.__$emitterPrivate.on.apply(this.__$emitterPrivate,Array.prototype.slice.apply(arguments)),this},_one:function(){return this.__$emitterPrivate.one.apply(this.__$emitterPrivate,Array.prototype.slice.apply(arguments)),this},_open:function(b,c){var e=this;if(!e.__destroying&&d(e._$origin)&&e.__enabled){var f=!0;if("closed"==e.__state&&(e._trigger({type:"before",event:b,stop:function(){f=!1}}),f&&e.__options.functionBefore&&(f=e.__options.functionBefore.call(e,e,{event:b,origin:e._$origin[0]}))),f!==!1&&null!==e.__Content){c&&e.__callbacks.open.push(c),e.__callbacks.close=[],e.__timeoutsClear();var g,i=function(){"stable"!=e.__state&&e.__stateSet("stable"),a.each(e.__callbacks.open,function(a,b){b.call(e,e,{origin:e._$origin[0],tooltip:e._$tooltip[0]})}),e.__callbacks.open=[]};if("closed"!==e.__state)g=0,"disappearing"===e.__state?(e.__stateSet("appearing"),h.hasTransitions?(e._$tooltip.clearQueue().removeClass("tooltipster-dying").addClass("tooltipster-show"),e.__options.animationDuration[0]>0&&e._$tooltip.delay(e.__options.animationDuration[0]),e._$tooltip.queue(i)):e._$tooltip.stop().fadeIn(i)):"stable"==e.__state&&i();else{if(e.__stateSet("appearing"),g=e.__options.animationDuration[0],e.__contentInsert(),e.reposition(b,!0),h.hasTransitions?(e._$tooltip.addClass("tooltipster-"+e.__options.animation).addClass("tooltipster-initial").css({"-moz-animation-duration":e.__options.animationDuration[0]+"ms","-ms-animation-duration":e.__options.animationDuration[0]+"ms","-o-animation-duration":e.__options.animationDuration[0]+"ms","-webkit-animation-duration":e.__options.animationDuration[0]+"ms","animation-duration":e.__options.animationDuration[0]+"ms","transition-duration":e.__options.animationDuration[0]+"ms"}),setTimeout(function(){"closed"!=e.__state&&(e._$tooltip.addClass("tooltipster-show").removeClass("tooltipster-initial"),e.__options.animationDuration[0]>0&&e._$tooltip.delay(e.__options.animationDuration[0]),e._$tooltip.queue(i))},0)):e._$tooltip.css("display","none").fadeIn(e.__options.animationDuration[0],i),e.__trackerStart(),a(h.window).on("resize."+e.__namespace+"-triggerClose",function(b){var c=a(document.activeElement);(c.is("input")||c.is("textarea"))&&a.contains(e._$tooltip[0],c[0])||e.reposition(b)}).on("scroll."+e.__namespace+"-triggerClose",function(a){e.__scrollHandler(a)}),e.__$originParents=e._$origin.parents(),e.__$originParents.each(function(b,c){a(c).on("scroll."+e.__namespace+"-triggerClose",function(a){e.__scrollHandler(a)})}),e.__options.triggerClose.mouseleave||e.__options.triggerClose.touchleave&&h.hasTouchCapability){e._on("dismissable",function(a){a.dismissable?a.delay?(m=setTimeout(function(){e._close(a.event)},a.delay),e.__timeouts.close.push(m)):e._close(a):clearTimeout(m)});var j=e._$origin,k="",l="",m=null;e.__options.interactive&&(j=j.add(e._$tooltip)),e.__options.triggerClose.mouseleave&&(k+="mouseenter."+e.__namespace+"-triggerClose ",l+="mouseleave."+e.__namespace+"-triggerClose "),e.__options.triggerClose.touchleave&&h.hasTouchCapability&&(k+="touchstart."+e.__namespace+"-triggerClose",l+="touchend."+e.__namespace+"-triggerClose touchcancel."+e.__namespace+"-triggerClose"),j.on(l,function(a){if(e._touchIsTouchEvent(a)||!e._touchIsEmulatedEvent(a)){var b="mouseleave"==a.type?e.__options.delay:e.__options.delayTouch;e._trigger({delay:b[1],dismissable:!0,event:a,type:"dismissable"})}}).on(k,function(a){!e._touchIsTouchEvent(a)&&e._touchIsEmulatedEvent(a)||e._trigger({dismissable:!1,event:a,type:"dismissable"})})}e.__options.triggerClose.originClick&&e._$origin.on("click."+e.__namespace+"-triggerClose",function(a){e._touchIsTouchEvent(a)||e._touchIsEmulatedEvent(a)||e._close(a)}),(e.__options.triggerClose.click||e.__options.triggerClose.tap&&h.hasTouchCapability)&&setTimeout(function(){if("closed"!=e.__state){var b="",c=a(h.window.document.body);e.__options.triggerClose.click&&(b+="click."+e.__namespace+"-triggerClose "),e.__options.triggerClose.tap&&h.hasTouchCapability&&(b+="touchend."+e.__namespace+"-triggerClose"),c.on(b,function(b){e._touchIsMeaningfulEvent(b)&&(e._touchRecordEvent(b),e.__options.interactive&&a.contains(e._$tooltip[0],b.target)||e._close(b))}),e.__options.triggerClose.tap&&h.hasTouchCapability&&c.on("touchstart."+e.__namespace+"-triggerClose",function(a){e._touchRecordEvent(a)})}},0),e._trigger("ready"),e.__options.functionReady&&e.__options.functionReady.call(e,e,{origin:e._$origin[0],tooltip:e._$tooltip[0]})}if(e.__options.timer>0){var m=setTimeout(function(){e._close()},e.__options.timer+g);e.__timeouts.close.push(m)}}}return e},_openShortly:function(a){var b=this,c=!0;if("stable"!=b.__state&&"appearing"!=b.__state&&!b.__timeouts.open&&(b._trigger({type:"start",event:a,stop:function(){c=!1}}),c)){var d=0==a.type.indexOf("touch")?b.__options.delayTouch:b.__options.delay;d[0]?b.__timeouts.open=setTimeout(function(){b.__timeouts.open=null,b.__pointerIsOverOrigin&&b._touchIsMeaningfulEvent(a)?(b._trigger("startend"),b._open(a)):b._trigger("startcancel")},d[0]):(b._trigger("startend"),b._open(a))}return b},_optionsExtract:function(b,c){var d=this,e=a.extend(!0,{},c),f=d.__options[b];return f||(f={},a.each(c,function(a,b){var c=d.__options[a];void 0!==c&&(f[a]=c)})),a.each(e,function(b,c){void 0!==f[b]&&("object"!=typeof c||c instanceof Array||null==c||"object"!=typeof f[b]||f[b]instanceof Array||null==f[b]?e[b]=f[b]:a.extend(e[b],f[b]))}),e},_plug:function(b){var c=a.tooltipster._plugin(b);if(!c)throw new Error('The "'+b+'" plugin is not defined');return c.instance&&a.tooltipster.__bridge(c.instance,this,c.name),this},_touchIsEmulatedEvent:function(a){for(var b=!1,c=(new Date).getTime(),d=this.__touchEvents.length-1;d>=0;d--){var e=this.__touchEvents[d];if(!(c-e.time<500))break;e.target===a.target&&(b=!0)}return b},_touchIsMeaningfulEvent:function(a){return this._touchIsTouchEvent(a)&&!this._touchSwiped(a.target)||!this._touchIsTouchEvent(a)&&!this._touchIsEmulatedEvent(a)},_touchIsTouchEvent:function(a){return 0==a.type.indexOf("touch")},_touchRecordEvent:function(a){return this._touchIsTouchEvent(a)&&(a.time=(new Date).getTime(),this.__touchEvents.push(a)),this},_touchSwiped:function(a){for(var b=!1,c=this.__touchEvents.length-1;c>=0;c--){var d=this.__touchEvents[c];if("touchmove"==d.type){b=!0;break}if("touchstart"==d.type&&a===d.target)break}return b},_trigger:function(){var b=Array.prototype.slice.apply(arguments);return"string"==typeof b[0]&&(b[0]={type:b[0]}),b[0].instance=this,b[0].origin=this._$origin?this._$origin[0]:null,b[0].tooltip=this._$tooltip?this._$tooltip[0]:null,this.__$emitterPrivate.trigger.apply(this.__$emitterPrivate,b),a.tooltipster._trigger.apply(a.tooltipster,b),this.__$emitterPublic.trigger.apply(this.__$emitterPublic,b),this},_unplug:function(b){var c=this;if(c[b]){var d=a.tooltipster._plugin(b);d.instance&&a.each(d.instance,function(a,d){c[a]&&c[a].bridged===c[b]&&delete c[a]}),c[b].__destroy&&c[b].__destroy(),delete c[b]}return c},close:function(a){return this.__destroyed?this.__destroyError():this._close(null,a),this},content:function(a){var b=this;if(void 0===a)return b.__Content;if(b.__destroyed)b.__destroyError();else if(b.__contentSet(a),null!==b.__Content){if("closed"!==b.__state&&(b.__contentInsert(),b.reposition(),b.__options.updateAnimation))if(h.hasTransitions){var c=b.__options.updateAnimation;b._$tooltip.addClass("tooltipster-update-"+c),setTimeout(function(){"closed"!=b.__state&&b._$tooltip.removeClass("tooltipster-update-"+c)},1e3)}else b._$tooltip.fadeTo(200,.5,function(){"closed"!=b.__state&&b._$tooltip.fadeTo(200,1)})}else b._close();return b},destroy:function(){var b=this;if(b.__destroyed)b.__destroyError();else{"closed"!=b.__state?b.option("animationDuration",0)._close(null,null,!0):b.__timeoutsClear(),b._trigger("destroy"),b.__destroyed=!0,b._$origin.removeData(b.__namespace).off("."+b.__namespace+"-triggerOpen"),a(h.window.document.body).off("."+b.__namespace+"-triggerOpen");var c=b._$origin.data("tooltipster-ns");if(c)if(1===c.length){var d=null;"previous"==b.__options.restoration?d=b._$origin.data("tooltipster-initialTitle"):"current"==b.__options.restoration&&(d="string"==typeof b.__Content?b.__Content:a("<div></div>").append(b.__Content).html()),d&&b._$origin.attr("title",d),b._$origin.removeClass("tooltipstered"),b._$origin.removeData("tooltipster-ns").removeData("tooltipster-initialTitle")}else c=a.grep(c,function(a,c){return a!==b.__namespace}),b._$origin.data("tooltipster-ns",c);b._trigger("destroyed"),b._off(),b.off(),b.__Content=null,b.__$emitterPrivate=null,b.__$emitterPublic=null,b.__options.parent=null,b._$origin=null,b._$tooltip=null,a.tooltipster.__instancesLatestArr=a.grep(a.tooltipster.__instancesLatestArr,function(a,c){return b!==a}),clearInterval(b.__garbageCollector)}return b},disable:function(){return this.__destroyed?(this.__destroyError(),this):(this._close(),this.__enabled=!1,this)},elementOrigin:function(){return this.__destroyed?void this.__destroyError():this._$origin[0]},elementTooltip:function(){return this._$tooltip?this._$tooltip[0]:null},enable:function(){return this.__enabled=!0,this},hide:function(a){return this.close(a)},instance:function(){return this},off:function(){return this.__destroyed||this.__$emitterPublic.off.apply(this.__$emitterPublic,Array.prototype.slice.apply(arguments)),this},on:function(){return this.__destroyed?this.__destroyError():this.__$emitterPublic.on.apply(this.__$emitterPublic,Array.prototype.slice.apply(arguments)),this},one:function(){return this.__destroyed?this.__destroyError():this.__$emitterPublic.one.apply(this.__$emitterPublic,Array.prototype.slice.apply(arguments)),this},open:function(a){return this.__destroyed?this.__destroyError():this._open(null,a),this},option:function(b,c){return void 0===c?this.__options[b]:(this.__destroyed?this.__destroyError():(this.__options[b]=c,this.__optionsFormat(),a.inArray(b,["trigger","triggerClose","triggerOpen"])>=0&&this.__prepareOrigin(),"selfDestruction"===b&&this.__prepareGC()),this)},reposition:function(a,b){var c=this;return c.__destroyed?c.__destroyError():"closed"!=c.__state&&d(c._$origin)&&(b||d(c._$tooltip))&&(b||c._$tooltip.detach(),c.__Geometry=c.__geometry(),c._trigger({type:"reposition",event:a,helper:{geo:c.__Geometry}})),c},show:function(a){return this.open(a)},status:function(){return{destroyed:this.__destroyed,enabled:this.__enabled,open:"closed"!==this.__state,state:this.__state}},triggerHandler:function(){return this.__destroyed?this.__destroyError():this.__$emitterPublic.triggerHandler.apply(this.__$emitterPublic,Array.prototype.slice.apply(arguments)),this}},a.fn.tooltipster=function(){var b=Array.prototype.slice.apply(arguments),c="You are using a single HTML element as content for several tooltips. You probably want to set the contentCloning option to TRUE.";if(0===this.length)return this;if("string"==typeof b[0]){var d="#*$~&";return this.each(function(){var e=a(this).data("tooltipster-ns"),f=e?a(this).data(e[0]):null;if(!f)throw new Error("You called Tooltipster's \""+b[0]+'" method on an uninitialized element');if("function"!=typeof f[b[0]])throw new Error('Unknown method "'+b[0]+'"');this.length>1&&"content"==b[0]&&(b[1]instanceof a||"object"==typeof b[1]&&null!=b[1]&&b[1].tagName)&&!f.__options.contentCloning&&f.__options.debug&&console.log(c);var g=f[b[0]](b[1],b[2]);return g!==f||"instance"===b[0]?(d=g,!1):void 0}),"#*$~&"!==d?d:this}a.tooltipster.__instancesLatestArr=[];var e=b[0]&&void 0!==b[0].multiple,g=e&&b[0].multiple||!e&&f.multiple,h=b[0]&&void 0!==b[0].content,i=h&&b[0].content||!h&&f.content,j=b[0]&&void 0!==b[0].contentCloning,k=j&&b[0].contentCloning||!j&&f.contentCloning,l=b[0]&&void 0!==b[0].debug,m=l&&b[0].debug||!l&&f.debug;return this.length>1&&(i instanceof a||"object"==typeof i&&null!=i&&i.tagName)&&!k&&m&&console.log(c),this.each(function(){var c=!1,d=a(this),e=d.data("tooltipster-ns"),f=null;e?g?c=!0:m&&(console.log("Tooltipster: one or more tooltips are already attached to the element below. Ignoring."),console.log(this)):c=!0,c&&(f=new a.Tooltipster(this,b[0]),e||(e=[]),e.push(f.__namespace),d.data("tooltipster-ns",e),d.data(f.__namespace,f),f.__options.functionInit&&f.__options.functionInit.call(f,f,{origin:this}),f._trigger("init")),a.tooltipster.__instancesLatestArr.push(f)}),this},b.prototype={__init:function(b){this.__$tooltip=b,this.__$tooltip.css({left:0,overflow:"hidden",position:"absolute",top:0}).find(".tooltipster-content").css("overflow","auto"),this.$container=a('<div class="tooltipster-ruler"></div>').append(this.__$tooltip).appendTo(h.window.document.body)},__forceRedraw:function(){var a=this.__$tooltip.parent();this.__$tooltip.detach(),this.__$tooltip.appendTo(a)},constrain:function(a,b){return this.constraints={width:a,height:b},this.__$tooltip.css({display:"block",height:"",overflow:"auto",width:a}),this},destroy:function(){this.__$tooltip.detach().find(".tooltipster-content").css({display:"",overflow:""}),this.$container.remove()},free:function(){return this.constraints=null,this.__$tooltip.css({display:"",height:"",overflow:"visible",width:""}),this},measure:function(){this.__forceRedraw();var a=this.__$tooltip[0].getBoundingClientRect(),b={size:{height:a.height||a.bottom-a.top,width:a.width||a.right-a.left}};if(this.constraints){var c=this.__$tooltip.find(".tooltipster-content"),d=this.__$tooltip.outerHeight(),e=c[0].getBoundingClientRect(),f={height:d<=this.constraints.height,width:a.width<=this.constraints.width&&e.width>=c[0].scrollWidth-1};b.fits=f.height&&f.width}return h.IE&&h.IE<=11&&b.size.width!==h.window.document.documentElement.clientWidth&&(b.size.width=Math.ceil(b.size.width)+1),b}};var j=navigator.userAgent.toLowerCase();-1!=j.indexOf("msie")?h.IE=parseInt(j.split("msie")[1]):-1!==j.toLowerCase().indexOf("trident")&&-1!==j.indexOf(" rv:11")?h.IE=11:-1!=j.toLowerCase().indexOf("edge/")&&(h.IE=parseInt(j.toLowerCase().split("edge/")[1]));var k="tooltipster.sideTip";return a.tooltipster._plugin({name:k,instance:{__defaults:function(){return{arrow:!0,distance:6,functionPosition:null,maxWidth:null,minIntersection:16,minWidth:0,position:null,side:"top",viewportAware:!0}},__init:function(a){var b=this;b.__instance=a,b.__namespace="tooltipster-sideTip-"+Math.round(1e6*Math.random()),b.__previousState="closed",b.__options,b.__optionsFormat(),b.__instance._on("state."+b.__namespace,function(a){"closed"==a.state?b.__close():"appearing"==a.state&&"closed"==b.__previousState&&b.__create(),b.__previousState=a.state}),b.__instance._on("options."+b.__namespace,function(){b.__optionsFormat()}),b.__instance._on("reposition."+b.__namespace,function(a){b.__reposition(a.event,a.helper)})},__close:function(){this.__instance.content()instanceof a&&this.__instance.content().detach(),this.__instance._$tooltip.remove(),this.__instance._$tooltip=null},__create:function(){var b=a('<div class="tooltipster-base tooltipster-sidetip"><div class="tooltipster-box"><div class="tooltipster-content"></div></div><div class="tooltipster-arrow"><div class="tooltipster-arrow-uncropped"><div class="tooltipster-arrow-border"></div><div class="tooltipster-arrow-background"></div></div></div></div>');this.__options.arrow||b.find(".tooltipster-box").css("margin",0).end().find(".tooltipster-arrow").hide(),this.__options.minWidth&&b.css("min-width",this.__options.minWidth+"px"),this.__options.maxWidth&&b.css("max-width",this.__options.maxWidth+"px"),
this.__instance._$tooltip=b,this.__instance._trigger("created")},__destroy:function(){this.__instance._off("."+self.__namespace)},__optionsFormat:function(){var b=this;if(b.__options=b.__instance._optionsExtract(k,b.__defaults()),b.__options.position&&(b.__options.side=b.__options.position),"object"!=typeof b.__options.distance&&(b.__options.distance=[b.__options.distance]),b.__options.distance.length<4&&(void 0===b.__options.distance[1]&&(b.__options.distance[1]=b.__options.distance[0]),void 0===b.__options.distance[2]&&(b.__options.distance[2]=b.__options.distance[0]),void 0===b.__options.distance[3]&&(b.__options.distance[3]=b.__options.distance[1]),b.__options.distance={top:b.__options.distance[0],right:b.__options.distance[1],bottom:b.__options.distance[2],left:b.__options.distance[3]}),"string"==typeof b.__options.side){var c={top:"bottom",right:"left",bottom:"top",left:"right"};b.__options.side=[b.__options.side,c[b.__options.side]],"left"==b.__options.side[0]||"right"==b.__options.side[0]?b.__options.side.push("top","bottom"):b.__options.side.push("right","left")}6===a.tooltipster._env.IE&&b.__options.arrow!==!0&&(b.__options.arrow=!1)},__reposition:function(b,c){var d,e=this,f=e.__targetFind(c),g=[];e.__instance._$tooltip.detach();var h=e.__instance._$tooltip.clone(),i=a.tooltipster._getRuler(h),j=!1,k=e.__instance.option("animation");switch(k&&h.removeClass("tooltipster-"+k),a.each(["window","document"],function(d,k){var l=null;if(e.__instance._trigger({container:k,helper:c,satisfied:j,takeTest:function(a){l=a},results:g,type:"positionTest"}),1==l||0!=l&&0==j&&("window"!=k||e.__options.viewportAware))for(var d=0;d<e.__options.side.length;d++){var m={horizontal:0,vertical:0},n=e.__options.side[d];"top"==n||"bottom"==n?m.vertical=e.__options.distance[n]:m.horizontal=e.__options.distance[n],e.__sideChange(h,n),a.each(["natural","constrained"],function(a,d){if(l=null,e.__instance._trigger({container:k,event:b,helper:c,mode:d,results:g,satisfied:j,side:n,takeTest:function(a){l=a},type:"positionTest"}),1==l||0!=l&&0==j){var h={container:k,distance:m,fits:null,mode:d,outerSize:null,side:n,size:null,target:f[n],whole:null},o="natural"==d?i.free():i.constrain(c.geo.available[k][n].width-m.horizontal,c.geo.available[k][n].height-m.vertical),p=o.measure();if(h.size=p.size,h.outerSize={height:p.size.height+m.vertical,width:p.size.width+m.horizontal},"natural"==d?c.geo.available[k][n].width>=h.outerSize.width&&c.geo.available[k][n].height>=h.outerSize.height?h.fits=!0:h.fits=!1:h.fits=p.fits,"window"==k&&(h.fits?"top"==n||"bottom"==n?h.whole=c.geo.origin.windowOffset.right>=e.__options.minIntersection&&c.geo.window.size.width-c.geo.origin.windowOffset.left>=e.__options.minIntersection:h.whole=c.geo.origin.windowOffset.bottom>=e.__options.minIntersection&&c.geo.window.size.height-c.geo.origin.windowOffset.top>=e.__options.minIntersection:h.whole=!1),g.push(h),h.whole)j=!0;else if("natural"==h.mode&&(h.fits||h.size.width<=c.geo.available[k][n].width))return!1}})}}),e.__instance._trigger({edit:function(a){g=a},event:b,helper:c,results:g,type:"positionTested"}),g.sort(function(a,b){if(a.whole&&!b.whole)return-1;if(!a.whole&&b.whole)return 1;if(a.whole&&b.whole){var c=e.__options.side.indexOf(a.side),d=e.__options.side.indexOf(b.side);return d>c?-1:c>d?1:"natural"==a.mode?-1:1}if(a.fits&&!b.fits)return-1;if(!a.fits&&b.fits)return 1;if(a.fits&&b.fits){var c=e.__options.side.indexOf(a.side),d=e.__options.side.indexOf(b.side);return d>c?-1:c>d?1:"natural"==a.mode?-1:1}return"document"==a.container&&"bottom"==a.side&&"natural"==a.mode?-1:1}),d=g[0],d.coord={},d.side){case"left":case"right":d.coord.top=Math.floor(d.target-d.size.height/2);break;case"bottom":case"top":d.coord.left=Math.floor(d.target-d.size.width/2)}switch(d.side){case"left":d.coord.left=c.geo.origin.windowOffset.left-d.outerSize.width;break;case"right":d.coord.left=c.geo.origin.windowOffset.right+d.distance.horizontal;break;case"top":d.coord.top=c.geo.origin.windowOffset.top-d.outerSize.height;break;case"bottom":d.coord.top=c.geo.origin.windowOffset.bottom+d.distance.vertical}"window"==d.container?"top"==d.side||"bottom"==d.side?d.coord.left<0?c.geo.origin.windowOffset.right-this.__options.minIntersection>=0?d.coord.left=0:d.coord.left=c.geo.origin.windowOffset.right-this.__options.minIntersection-1:d.coord.left>c.geo.window.size.width-d.size.width&&(c.geo.origin.windowOffset.left+this.__options.minIntersection<=c.geo.window.size.width?d.coord.left=c.geo.window.size.width-d.size.width:d.coord.left=c.geo.origin.windowOffset.left+this.__options.minIntersection+1-d.size.width):d.coord.top<0?c.geo.origin.windowOffset.bottom-this.__options.minIntersection>=0?d.coord.top=0:d.coord.top=c.geo.origin.windowOffset.bottom-this.__options.minIntersection-1:d.coord.top>c.geo.window.size.height-d.size.height&&(c.geo.origin.windowOffset.top+this.__options.minIntersection<=c.geo.window.size.height?d.coord.top=c.geo.window.size.height-d.size.height:d.coord.top=c.geo.origin.windowOffset.top+this.__options.minIntersection+1-d.size.height):(d.coord.left>c.geo.window.size.width-d.size.width&&(d.coord.left=c.geo.window.size.width-d.size.width),d.coord.left<0&&(d.coord.left=0)),e.__sideChange(h,d.side),c.tooltipClone=h[0],c.tooltipParent=e.__instance.option("parent").parent[0],c.mode=d.mode,c.whole=d.whole,c.origin=e.__instance._$origin[0],c.tooltip=e.__instance._$tooltip[0],delete d.container,delete d.fits,delete d.mode,delete d.outerSize,delete d.whole,d.distance=d.distance.horizontal||d.distance.vertical;var l=a.extend(!0,{},d);if(e.__instance._trigger({edit:function(a){d=a},event:b,helper:c,position:l,type:"position"}),e.__options.functionPosition){var m=e.__options.functionPosition.call(e,e.__instance,c,l);m&&(d=m)}i.destroy();var n,o;"top"==d.side||"bottom"==d.side?(n={prop:"left",val:d.target-d.coord.left},o=d.size.width-this.__options.minIntersection):(n={prop:"top",val:d.target-d.coord.top},o=d.size.height-this.__options.minIntersection),n.val<this.__options.minIntersection?n.val=this.__options.minIntersection:n.val>o&&(n.val=o);var p;p=c.geo.origin.fixedLineage?c.geo.origin.windowOffset:{left:c.geo.origin.windowOffset.left+c.geo.window.scroll.left,top:c.geo.origin.windowOffset.top+c.geo.window.scroll.top},d.coord={left:p.left+(d.coord.left-c.geo.origin.windowOffset.left),top:p.top+(d.coord.top-c.geo.origin.windowOffset.top)},e.__sideChange(e.__instance._$tooltip,d.side),c.geo.origin.fixedLineage?e.__instance._$tooltip.css("position","fixed"):e.__instance._$tooltip.css("position",""),e.__instance._$tooltip.css({left:d.coord.left,top:d.coord.top,height:d.size.height,width:d.size.width}).find(".tooltipster-arrow").css({left:"",top:""}).css(n.prop,n.val),e.__instance._$tooltip.appendTo(e.__instance.option("parent")),e.__instance._trigger({type:"repositioned",event:b,position:d})},__sideChange:function(a,b){a.removeClass("tooltipster-bottom").removeClass("tooltipster-left").removeClass("tooltipster-right").removeClass("tooltipster-top").addClass("tooltipster-"+b)},__targetFind:function(a){var b={},c=this.__instance._$origin[0].getClientRects();if(c.length>1){var d=this.__instance._$origin.css("opacity");1==d&&(this.__instance._$origin.css("opacity",.99),c=this.__instance._$origin[0].getClientRects(),this.__instance._$origin.css("opacity",1))}if(c.length<2)b.top=Math.floor(a.geo.origin.windowOffset.left+a.geo.origin.size.width/2),b.bottom=b.top,b.left=Math.floor(a.geo.origin.windowOffset.top+a.geo.origin.size.height/2),b.right=b.left;else{var e=c[0];b.top=Math.floor(e.left+(e.right-e.left)/2),e=c.length>2?c[Math.ceil(c.length/2)-1]:c[0],b.right=Math.floor(e.top+(e.bottom-e.top)/2),e=c[c.length-1],b.bottom=Math.floor(e.left+(e.right-e.left)/2),e=c.length>2?c[Math.ceil((c.length+1)/2)-1]:c[c.length-1],b.left=Math.floor(e.top+(e.bottom-e.top)/2)}return b}}}),a});
/* =============================================================================
 * Set viewport meta tag
 * ========================================================================== */
jQuery(document).ready(function()
{
	if( jQuery('body').hasClass('mobile') )
	{
		jQuery('head').append('<meta name="viewport" content="width=device-width,initial-scale=1.0">');
	}
});


/* =============================================================================
 * IOS workaround for wrong position bug under IOS11,12
 * ========================================================================== */
jQuery(document).ready(function()
{
	if( jQuery('body').hasClass('ios') )
	{
		// current chrome, safari 11, safari 12
		if( jQuery('body').hasClass('ch70') || jQuery('body').hasClass('sf11') || jQuery('body').hasClass('sf12')  )
		{
			jQuery("html, body").animate({scrollTop: 0});
		}
	}
});


/* =============================================================================
 * Universal optin loader
 * ========================================================================== */
function Eclipse_optin(strElementType)
{
	if(strElementType == undefined || strElementType == '')
	{
		return;
	}
	// user settings not applied yet
	if(localStorage.getItem('user_privacy_settings') == undefined || localStorage.getItem('user_privacy_settings') == '' || localStorage.getItem('user_privacy_settings') <= 0)
	{
		return
	}

	// find all scripts having a data-src attribute
	var targets = jQuery(strElementType+'[data-src]');

	if(targets.length > 0)
	{
		jQuery.each(targets,function(i,e)
		{
			var privacy = jQuery(e).data('privacy');
			if(privacy == undefined)
			{
				privacy = 0;
			}

			var attr = 'src';
			if(strElementType == 'link')
			{
				attr = 'href';
			}
			else if(strElementType == 'object')
			{
				attr = 'data';
			}

			if(localStorage.getItem('user_privacy_settings') >= privacy)
			{
				jQuery(e).attr(attr,jQuery(e).data('src') );
			}
		});
	}
}

jQuery(document).ready(function()
{
	Eclipse_optin('script');
	Eclipse_optin('link');
	Eclipse_optin('iframe');
	Eclipse_optin('object');
	Eclipse_optin('img');
});

// listen to Eclipse.user_privacy Event
jQuery(document).on('Eclipse.user_privacy',function(event,params)
{
	Eclipse_optin('script');
	Eclipse_optin('link');
	Eclipse_optin('iframe');
	Eclipse_optin('object');
	Eclipse_optin('img');
});


/* =============================================================================
 * Eclipse_setPrivacy(intLevel)
 * @param integer	Level of privacy
 * ========================================================================== */
function Eclipse_setPrivacy(intLevel)
{
	// set local storage
	localStorage.setItem('user_privacy_settings',intLevel);
	// set cookie for php
	jQuery.cookie('user_privacy_settings',intLevel,{expires:30,path:'/'});
	// fire event
	jQuery(document).trigger('Eclipse.user_privacy',{'level':intLevel});
}


/* =============================================================================
 * Eclipse_clearPrivacy(blnReload)
 * @param boolean	Reload page true/false
 * ========================================================================== */
function Eclipse_clearPrivacy(blnReload)
{
	if(blnReload == undefined || blnReload == null)
	{
		blnReload = true;
	}

	// clear local storage
	localStorage.removeItem('user_privacy_settings');
	// empty cookie
	jQuery.removeCookie('user_privacy_settings',{path:'/'});
	// fire event
	jQuery(document).trigger('Eclipse.clear_privacy_settings',{});

	// reload page
	if(blnReload)
	{
		location.reload();
	}
}


/* =============================================================================
 * mainmenu menuheader (deactivate link)
 * ========================================================================== */
jQuery(document).ready(function(){
	jQuery('.mainmenu .menuheader').removeAttr("href");
});

/* =============================================================================
 * mainmenu avoid_click
 * ========================================================================== */
jQuery(document).ready(function(){
	jQuery('.mainmenu a.avoid_click, .smartmenu-content a.avoid_click, #mobnav a.avoid_click').attr("href","javascript:void(0);");
	jQuery('.mainmenu a.avoid-click, .smartmenu-content a.avoid-click, #mobnav a.avoid-click').attr("href","javascript:void(0);");
});

/* =============================================================================
 * ce_parallax scripts/parallax/
 * ========================================================================== */
/*if(!jQuery('body').hasClass('ios')){
	jQuery(window).stellar({
		horizontalScrolling: false,
		responsive:true,
	})
}*/

/* =============================================================================
 * ce_table responsive
 * ========================================================================== */
function respTables() {
	jQuery('.ce_table').each(function() {
	 	var tableWidth = jQuery(this).find('table').width();
	 	var ce_tableWidth = jQuery(this).width();
	 	if (tableWidth > ce_tableWidth) {
		 	jQuery(this).addClass('overflow');
	 	} else {
		 	jQuery(this).removeClass('overflow');
	 	}
	});
};

jQuery(document).ready(function(){
	respTables();
});

jQuery(window).on("resize", function(){
	respTables();
});

/* =============================================================================
 * css3 animation (css/animate.css)
 * ========================================================================== */
var el = jQuery("body");
var animationClasses = ["fadeIn", "zoomIn", "fadeInDown","fadeInDownBig","fadeInLeft","fadeInLeftBig","fadeInRight","fadeInRightBig","fadeInUp","fadeInUpBig","rotateIn","zoomIn","slideInDown","slideInLeft","slideInRight","slideInUp","bounceIn","bounceInDown","bounceInLeft","bounceInRight","bounceInUp"];
jQuery.each(animationClasses, function(key, value){
	el.find("." + value).each(function(){
		jQuery(this).removeClass(value).attr("data-animate", value);

	});
});

jQuery(document).ready(function() {
	var animate_start = function(element) {
		element.find('.animate').not('.nowaypoint').each(function() {
			var item = jQuery(this);
		    var animation = item.data("animate");
		    if(jQuery('body').hasClass('ios') || jQuery('body').hasClass('android')) {
		    	item.removeClass('animate');
		    	return true;

		    } else {

	            item.waypoint(function(direction) {
	    			item.removeClass('animate').addClass('animated '+animation).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
	    				item.removeClass(animation+' animated');
	    			});
	            },{offset: '90%',triggerOnce: true});
	        }
		});
	};

	setTimeout(function() {
		jQuery('.mod_article').each(function() {
	        animate_start( jQuery(this) );
	    });
	},500);

});

/* =============================================================================
 * ce_youtube_background (scripts/jquery.mb.YTPlayer/)
 * ========================================================================== */
jQuery(document).ready(function() {
	if(!jQuery('body').hasClass('mobile')) {
		jQuery(".player").mb_YTPlayer();
	}
});

/* =============================================================================
 * scrollToTop
 * ========================================================================== */
jQuery(document).ready(function(){
	jQuery('a.totop').click(function(e){
    	e.preventDefault();
    	jQuery("html, body").animate({scrollTop: jQuery('#contentwrapper').offset().top - 100}, 500, 'linear');
	});
});

/* =============================================================================
 * sticky header
 * ========================================================================== */
jQuery(document).ready(function()
{
	var header = jQuery('#header');
	if(header)
	{
		var childs = header.clone();

		var inner = '';
		childs.wrap(function()
		{
		  	inner += jQuery( this ).html();
		});
		inner += '';

		var stickyheader = document.createElement('div');
		jQuery(stickyheader).attr('id','stickyheader').addClass('stickyheader').html(inner).insertAfter('.body_bottom');
		jQuery('#stickyheader .inside').wrap('<div class="header cloned"></div>');
		jQuery(stickyheader).find('a').removeAttr('tabindex');
	}
});

function headerFixed() {
	var topHeight = jQuery("#top-wrapper").outerHeight();
	if(topHeight == jQuery(window).innerHeight())
	{
		topHeight = jQuery(window).innerHeight() / 3;
	}

	if (jQuery(this).scrollTop() > topHeight) {
		jQuery("body").addClass("fixed-header");

	} else {
		jQuery("body").removeClass("fixed-header");
    }
};

jQuery(document).ready(function(){
	headerFixed();
});

jQuery(window).scroll(function() {
	headerFixed();
});

/* =============================================================================
 * generate smartmenu on onepage_page
 * ========================================================================== */
jQuery(document).ready(function() {

	/* delete menu of smartmenu */
	var smartmenu = jQuery('body.onepage_page .body_bottom .smartmenu-content .smartmenu-table');
	smartmenu.empty();

	/* clone mainmenu to smartmenu */
	var mobSmartmenu = jQuery("body.onepage_page .header.original nav.mainmenu ul.level_1").clone().removeClass("mainmenu");
	smartmenu.append(mobSmartmenu);
});

/* =============================================================================
 * smartmenu
 * ========================================================================== */
jQuery(document).ready(function(){

	jQuery('.smartmenu-trigger').click(function(e){
		jQuery('.smartmenu-content').addClass('open');
		jQuery('.smartmenu').addClass('open');
		jQuery('body').addClass('no_scroll');
	});

	jQuery('.smartmenu-content .smartmenu-close').click(function(e){
		jQuery('.smartmenu-content').removeClass('open');
		jQuery('.smartmenu').removeClass('open');
		jQuery('body').removeClass('no_scroll');
	});

	jQuery('.smartmenu-content a:not(.submenu)').click(function(e){
		// is not new window
		if(jQuery(e.target).attr('target') != '_blank')
		{
			jQuery('.smartmenu-content').removeClass('open');
			jQuery('body').removeClass('no_scroll');
		}
	});

	// open trail content on page load
	jQuery('.smartmenu-content').find('.trail').addClass('open');
	jQuery('.smartmenu-content').find('.trail > ul').show();
});

jQuery(document).ready(function(){

	jQuery('.smartmenu-content .subitems_trigger').on('click', function(){
		var element = jQuery(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp();
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown();
			element.siblings('li').children('ul').slideUp();
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp();
		}
	});

});

/* =============================================================================
 * mmenu // jquery.mmenu.min.all.js
 * ========================================================================== */
jQuery(document).ready(function() {

	var mobnav = jQuery("body.onepage_page .header nav.mainmenu").clone().attr("id","mobnav").removeClass("mainmenu");
	jQuery('.body_bottom').append(mobnav);

	mobnav = jQuery("#mobnav");

	// inject additional layout section [mmenu_top]
	var content_mmenu_top = '';
	if(jQuery('#mmenu_top'))
	{
		content_mmenu_top = jQuery('#mmenu_top').html();
		mobnav.addClass('has_section mmenu_top');
		jQuery('body > #mmenu_top').remove();
	}

	// inject additional layout section [mmenu_bottom]
	var content_mmenu_bottom = '';
	if(jQuery('#mmenu_bottom'))
	{
		content_mmenu_bottom = jQuery('#mmenu_bottom').html();
		mobnav.addClass('has_section mmenu_bottom');
		jQuery('body > #mmenu_bottom').remove();
	}

	// init mmenu
	mobnav.mmenu(
	{
		"navbars":
		[
			{"position": "top", "content": [content_mmenu_top]},
			{"position": "bottom","content": [content_mmenu_bottom]}
		],
		setSelected: {parent: true, current: true},
		onClick: {close: false, preventDefault: false}
	});

	// open mmenu
	var api = mobnav.data('mmenu');
	jQuery('#nav-open-btn, #stickyheader #nav-open-btn').click(function(e)
	{
		e.preventDefault();
		api.open();
	});

	// is onepage
	if(mobnav.hasClass('onepagenav')){
		jQuery("#mobnav a:not(.backlink)").bind('click',function(e)
		{
	   		if(jQuery('body').hasClass('is_regular_page'))
	   		{
		   		window.location = jQuery(this).attr('href');
		   		return true;
	   		}
	   		else
	   		{
	   			api.close();
	   		}
	 	});
	}

	// use internal forward pages as placeholders
	mobnav.find('li.forward').click(function(e)
	{
		// check if button has a nested level
		var next = jQuery(this).find('.mm-next');
		if (next == undefined || next == null)
		{
			return true;
		}
		// prevent regular click event
		e.preventDefault();
		// open panel
		api.openPanel( jQuery( next.data('target') ) );
	});


});

/* =============================================================================
 * megamenu
 * ========================================================================== */
jQuery(document).ready(function(){
	jQuery('.mainmenu li.megamenu .level_2').wrap('<div class="megamenu-wrapper"></div>');
	jQuery('li.megamenu.remove-link a.a-level_2').removeAttr("href");
});

function megamenuWidth() {
	var elWidth = jQuery(".header .inside").width();
	jQuery(".header .mainmenu ul .megamenu-wrapper").css('width', elWidth);
};

jQuery(window).on("resize", function(){
	megamenuWidth();
});

jQuery(document).ready(function(){
	megamenuWidth();
});

/* =============================================================================
 * doubleTapToGo scripts/doubletaptogo/
 * ========================================================================== */
jQuery(document).ready(function(){
	if(jQuery('body').hasClass('android') || jQuery('body').hasClass('win') || jQuery('body').hasClass('ios')){
		jQuery('nav.mainmenu li.submenu:not(.level_2 .megamenu)').doubleTapToGo();
		jQuery('.smartmenu li.submenu').doubleTapToGo();
		jQuery('.top_metanavi li.submenu').doubleTapToGo();
		jQuery('.header_metanavi li.submenu').doubleTapToGo();
		jQuery('.mod_langswitcher li').doubleTapToGo();

		jQuery.each(jQuery('.mod_langswitcher li a'),function(i,elem)
		{
			jQuery(elem).click(function(e) { e.preventDefault(); window.location.href = jQuery(this).attr('href'); });
		});
	}
});

/* =============================================================================
 * Add no-scroll class to body when lightbox opens
 * ========================================================================== */
jQuery(document).bind('cbox_open',function() {
	jQuery('body').addClass('no_scroll');
});

jQuery(document).bind('cbox_closed',function() {
	jQuery('body').removeClass('no_scroll');
});

/* =============================================================================
 * remove class mainmenu in offcanvas menu
 * ========================================================================== */
jQuery(document).ready(function(){
	jQuery('.mm-menu').removeClass('mainmenu');
});

/* =============================================================================
 * offcanvas-top
 * ========================================================================== */
jQuery(document).ready(function(){
	jQuery('.offcanvas-trigger').click(function(){
		jQuery('#offcanvas-top').toggleClass('offcanvas-top-open');
		jQuery('.offcanvas-trigger').toggleClass('offcanvas-top-open');
	});
});


/* =============================================================================
 * one-page-nav
 * ========================================================================== */

/**
 * Add an item counter class to the body on one_page sites
 */
jQuery(document).ready(function()
{
	if(jQuery('body').hasClass('onepage_page'))
	{
		// count articles in #slider
		jQuery('body').addClass( 'onepage_items_' + jQuery('#slider .inside > .mod_article').length );
	}
});

/**
 * Initialize smooth scrolling to one page anchor links
 */
jQuery(document).ready(function() {

	if(jQuery('body').hasClass('is_regular_page'))
	{
		jQuery('.smartmenu-content a').addClass('backlink');
		jQuery('.onepage_page a').addClass('backlink');
	}

	var stickyheader = jQuery(".stickyheader");
	var header = jQuery(".header.original");
	var lastActive = null;
	var links = jQuery(".onepage_page .mainmenu a:not(.backlink), .onepage_page .onepagenav a:not(.backlink), .page_navigation a:not(.backlink), .smartmenu-content a:not(.backlink)");
	var horizontalScroll = jQuery('body').hasClass('horizontal_scrolling');
	// not on mobile devices because content is 100%
	if(horizontalScroll && jQuery('body').hasClass('mobile'))
	{
		horizontalScroll = false;
	}

	var duration = 300;
	var timeout = 200;

	// jump to hashtag anchor in url
	var strHash = window.location.hash.toString();
	if(strHash.length > 0)
	{
		var target = jQuery(strHash);
		jQuery(strHash+' a').addClass('active');

		lastActive = jQuery(".onepagenav a:not(.backlink)[href*=\\"+strHash+"]");
		lastActive.addClass('active');

		var offsetX = 0;
		//var posX = target.offset().left - offsetX;

		var offsetY = stickyheader.height();
		//var posY = target.offset().top - offsetY;

		jQuery("html, body").animate({scrollTop: posY, scrollLeft:posX},
		{
			start		: function()
			{
				jQuery.cookie('onepage_animate',1);
			},
			complete	: function()
			{
				setTimeout(function()
				{
					jQuery.cookie('onepage_animate',0);
				}, timeout);
			}
		}, 0);
	}

	// set the last anchor to active on page load and scroll to it
	if(jQuery.cookie('onepage_active') !== null && jQuery.cookie('onepage_active') != undefined && strHash.length < 1)
	{
		jQuery.each(links, function(index, elem) {
			var hash = this.href.split("#");
			if(!hash[1])
			{
				return;
			}
			var anchor = hash[1].toString();
			if(anchor == jQuery.cookie('onepage_active'))
			{
				jQuery(elem).addClass('active');
				lastActive = jQuery(elem);
			}
		});

		// find target in page
		var target = jQuery("#"+jQuery.cookie('onepage_active'));
		if(target.length > 0)
		{
			var offsetX = 0;
			var posX = target.offset().left - offsetX;

			var offsetY = stickyheader.height();
			var posY = target.offset().top - offsetY;

			jQuery("html,body").stop().animate({scrollTop: posY, scrollLeft: posX}, {
				duration	: 0,
				start		: function()
				{
					// on start: flag that onepage started its animation
					jQuery.cookie('onepage_animate',1);
				},
				complete	: function()
				{
					setTimeout(function()
					{
						jQuery.cookie('onepage_animate',0);
						jQuery.cookie('onepage_position',null);
					}, timeout);
				}
			});
		}
	}

	// click event listener
	links.click(function(event)
	{
		var hash = this.href.split("#");
		if(!hash[1])
		{
			return true;
		}
		var anchor = hash[1].toString();
		var target = jQuery("#"+anchor);
		if(target.length < 1)
		{
			return false;
		}

		// store the current active anchor as cookie for further use
		jQuery.cookie('onepage_active',anchor);

		var offsetX = 0;
		var posX = target.offset().left - offsetX;

		var offsetY = stickyheader.height();
		var posY = target.offset().top - offsetY;

		jQuery("html,body").stop().animate({scrollTop: posY, scrollLeft: posX},
		{
			duration	: duration,
			start		: function()
			{
				jQuery.cookie('onepage_animate',1);
			},
			step		: function()
			{
				// on start: flag that onepage started its animation
				jQuery.cookie('onepage_animate',1);
			},
			complete	: function()
			{
				// on complete: remove the flag
				setTimeout(function()
				{
					jQuery.cookie('onepage_animate',0);
				}, timeout);

				// store last position
				jQuery.cookie('onepage_position',{'x':posX,'y':posY,'anchor':anchor});
			}
		});

		// toggle active class
		if(lastActive != jQuery(this))
		{
			links.removeClass('active');

			jQuery(".onepagenav a[href*="+anchor+"]").addClass('active');
			jQuery(".mainmenu a[href*="+anchor+"]").addClass('active');

			jQuery(this).addClass('active');
		}

		event.preventDefault();
		return false;
	});
});


/**
 * Set navi active on scroll
 */
jQuery(document).ready(function() {
	var links = jQuery(".onepage_page .mainmenu a:not(.backlink), .onepage_page .onepagenav a:not(.backlink), .page_navigation a");
	var stickyheader = jQuery(".stickyheader");
	var header = jQuery(".header.original");
	var lastActive = jQuery(".onepage_page .mainmenu a.active, .onepage_page .onepagenav a.active");
	var horizontalScroll = jQuery('body').hasClass('horizontal_scrolling');
	// not on mobile devices because content is 100%
	if(horizontalScroll && jQuery('body').hasClass('mobile'))
	{
		horizontalScroll = false;
	}

	var lastScrollX = 0;
	var lastScrollY = 0;

	jQuery(window).scroll(function()
	{
		// escape when animation is running
		if(jQuery.cookie('onepage_animate') > 0)
		{
			return;
		}

		// remove active class from all links
		links.removeClass('active');

		var scrollX = jQuery(window).scrollLeft();
		var scrollY = jQuery(window).scrollTop();
		var offsetX = 10;
		var offsetY = stickyheader.height();

		jQuery.each(links, function(index, elem)
		{
			var hash = elem.href.split("#");
			if(!hash[1])
			{
				return;
			}
			var anchor = hash[1].toString();
			var target = jQuery("#"+anchor);
			if(target.length < 1)
			{
				return;
			}

			// vertical scrolling
			var posY = target.offset().top - offsetY;
			var sizeY = posY + target.height();
			if (posY <= scrollY && sizeY >= scrollY && !horizontalScroll)
			{
	        	jQuery(elem).addClass("active");
	        }

	        // horizontal scolling
	        var posX = target.offset().left - offsetX;
			var sizeX = posX + target.width();
			if (posX <= scrollX && sizeX >= scrollX && horizontalScroll)
			{
	        	jQuery(elem).addClass("active");
	        }
	    });
	});

});

/* =============================================================================
 * imagebox workaround if content height > fix height
 * ========================================================================== */
function imageboxHeight() {
	jQuery(".ce_text_imagebox").each(function() {
		var fixHeight = jQuery(this).height();
		var contentHeight = jQuery(this).find(".inside").outerHeight();
		if (contentHeight > fixHeight) {
			jQuery(this).addClass("oversize");
		} else {
			jQuery(this).removeClass("oversize");
		}
	});
};

jQuery(document).ready(function(){
	imageboxHeight();
});

jQuery(window).on("resize", function(){
	imageboxHeight();
});

/* =============================================================================
 * ce_text_image_bar workaround if content height > fix height
 * ========================================================================== */
function imagebarHeight() {
	jQuery(".ce_text_image_bar").each(function() {
		var fixHeight = jQuery(this).height();
		var contentHeight = jQuery(this).find(".text-table").outerHeight();
		if (contentHeight > fixHeight) {
			jQuery(this).addClass("oversize");
		} else {
			jQuery(this).removeClass("oversize");
		}
	});
};

jQuery(document).ready(function(){
	imagebarHeight();
});

jQuery(window).on("resize", function(){
	imagebarHeight();
});

/* =============================================================================
 * search top trigger
 * ========================================================================== */
jQuery(document).ready(function(){
	jQuery(".ce_search_label").click(function(){
    	jQuery(".body_bottom .mod_search").addClass("show-search");
	});
	jQuery(".body_bottom .close-window").click(function(){
    	jQuery(".body_bottom .mod_search").removeClass("show-search");
	});
});

/* =============================================================================
 * scroll to anchors
 * ========================================================================== */
jQuery(document).ready(function()
{
    jQuery('#main a[href*=\\#], #left a[href*=\\#], #right a[href*=\\#], #slider a[href*=\\#], #footer a[href*=\\#], #bottom a[href*=\\#]').click(function(e)
	{
		// return with default behavior
		if( jQuery(this).hasClass('external-anchor') || jQuery(this).hasClass('not-anchor') )
		{
			return true;
		}

    	var target = jQuery('#'+jQuery(this).attr("href").split('#')[1]);
		if(target == undefined || target == null)
        {
            return true;
        }
        else if(target.length < 1)
        {
	        return true;
        }

        e.preventDefault();

        var stickheaderHeight = 0;
		if(jQuery('#stickyheader'))
		{
			stickheaderHeight = jQuery('#stickyheader').height();
        }

        jQuery("html, body").animate({scrollTop: target.offset().top - stickheaderHeight}, 500);
        return false;
    });
});

