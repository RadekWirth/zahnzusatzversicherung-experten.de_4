
<style>
	li { margin : 20px 0;}
</style>


<h1>Interfaces Dokumentation</h1>

<h1>GET</h1>

<ul>
<li><strong>/getInsuranceList</strong>
	<br />Liefert alle Versicherungen inkl Adressen aus der Datenbank
</li>

<li><strong>/getInsuranceList/{insurance-id}</strong>
	<br /><i>*insurance-id id</i>
	<br />Liefert eine bestimmte Versicherung inkl Adresse aus der Datenbank
</li>

<li><strong>/tariffList alias /getTariffList</strong>
	<br />Liefert alle Tarife mit tarifbezogenen Daten aus der Datenbank
</li>

<li><strong>/tariffList/{tafiff-id} alias /getTariffList/{tariff-id}</strong>
	<br /><i>*tariff-id idt</i>
	<br />Bestimmter Tarif mit tarifbezogenen Daten aus der Datenbank
	<br />+ Teile von Guidelines
</li>

<li><strong>/tariffListWithRating alias /getTariffListWithRating</strong>
	<br />Alle Tarife mit einfachen Daten aus der Datenbank
	<br />+ Rating 
</li>

<li><strong>/tariffListWithRating/{tafiff-id} alias /getTariffListWithRating/{tariff-id}</strong>
	<br /><i>*tariff-id idt</i>
	<br />Bestimmter Tarif mit einfachen Daten aus der Datenbank
	<br />+ Rating
	<br />+ Teile von Guidelines
</li>

<li><strong>/getTariffGuidelines</strong>
	<br />Guidelines für alle Tarife - notwendig?
</li>

<li><strong>/getTariffGuidelines/{tariff-id}</strong>
	<br /><i>*tariff-id idt</i>
	<br />Guidelines für einen bestimmten Tarif
</li>

<li><strong>/tariffBenefitLimitation/{tariff-id} alias /getTariffBenefitLimitation/{tariff-id}</strong>
	<br /><i>*tariff-id idt</i>
	<br />Leistungsbeschränkungen für einen bestimmten Tarif
</li>

<li><strong>/tariffComparison/{tariff-id}</strong>
	<br /><i>*tariff-id idt</i>
	<br />Daten für Tarifvergleiche / 4er Vergleiche
</li>

<li><strong>/tariffRefundExamples/{tariff-id}</strong>
	<br /><i>*tariff-id idt</i>
	<br />Daten für Erstattungsbeispiele
</li>

<li><strong>/getBenefitsData/{tariff-id}</strong>
	<br /><i>*tariff-id idt</i>
	<br />Daten für die Leistungsbeschreibung
</li>

<li><strong>/getLongBenefits/{tariff-id}</strong>
	<br /><i>*tariff-id idt</i>
	<br />Daten für die Ausgabe der langen Leistungsbeschreibung für Staffel 0
</li>

<li><strong>/getLongBenefits/{tariff-id}/{series}</strong>
	<br /><i>*tariff-id idt</i>
	<br /><i>*series    staffel</i>
	<br />Daten für die Ausgabe der langen Leistungsbeschreibung für eine Staffel (0-2)
</li>

<li><strong>/getShortBenefits/{tariff-id}</strong>
	<br /><i>*tariff-id idt</i>
	<br />Daten für die Ausgabe der kurzen Leistungsbeschreibung für Staffel 0
</li>

<li><strong>/getShortBenefits/{tariff-id}/{series}</strong>
	<br /><i>*tariff-id idt</i>
	<br /><i>*series    staffel</i>
	<br />Daten für die Ausgabe der kurzen Leistungsbeschreibung für eine Staffel (0-2)
</li>

<li><strong>/getRating/{tariff-id}</strong>
	<br /><i>*tariff-id idt</i>
	<br />Daten für Ratings (Erwachsene, Kinder, Externe Ratings)
</li>

<li><strong>/getTooltips</strong>
	<br />Tooltips der Texte für die kurze Leistungsbeschreibung


<li><strong>/getCalculator</strong>

<p>
Daten für den Rechner nach Admin-Portal
<p>
Folgende Parameter werden hier angenommen
<br />*type 0 = Erw.
<br />*birthdate geburtsdatum 1977-11-20
<br />*wishedBegin versicherungsbegin 1585692000
<br />*missing-teeth fehlende zähne 2
<br />*replaced-teeth-removable herausnehmbarer ZE 3
<br />*dental-treatment laufende zahnärztliche Behandlung 1
<br />*replaced-teeth-fix festsitzender ZE 5
<br />*replaced-teeth-older-10-years ZE älter als 10 Jahre 9
<br />*periodontitis Parodontose- bzw. Parodontitisbehandlung 1
<br />*periodontitis-healed Parodontose vollständig ausgeheilt 1
<br />*bite-splint Aufbissschiene 1



<li><strong>/getCalculator/{parameter}</strong>

<p>
Daten für den Rechner nach Admin-Portal
<p>
Folgende Parameter können übergeben werden (type = 0 :: Erwachsene)
<br />*type 0 = Erw., 1 = Kinder
<br />*birthdate geburtsdatum (format: jjjj-mm-tt)
<br />*wishedBegin versicherungsbegin (format: unixtime)
<br />*missing-teeth fehlende zähne (0 - 10)
<br />*replaced-teeth-removable herausnehmbarer ZE (0 - 20)
<br />*dental-treatment laufende zahnärztliche Behandlung (0 = nein, 1 = ja)
<br />*replaced-teeth-fix festsitzender ZE (0 - 20)
<br />*replaced-teeth-older-10-years ZE älter als 10 Jahre (2 = 0 - 2, 5 = 3 - 5, 6 = 6, 7 = 7, 8 = 8, 9 = 9 oder mehr)
<br />*periodontitis Parodontose- bzw. Parodontitisbehandlung (0 = nein, 1 = ja)
<br />*periodontitis-healed Parodontose vollständig ausgeheilt (0 = nein, 1 = ja)
<br />*bite-splint Aufbissschiene (0 = nein, 1 = ja)

<p>
Folgende Parameter können übergeben werden (type = 1 :: Kinder)
<br />*type 0 = Erw., 1 = Kinder
<br />*birthdate geburtsdatum (format: jjjj-mm-tt)
<br />*wishedBegin versicherungsbegin (format: unixtime)
<br />*dental-treatment laufende zahnärztliche Behandlung (0 = nein, 1 = ja)
<br />*orthodontics-treatment laufende KFO Behandlung (0 = nein, 1 = ja)



<h1>POST</h1>

<li><strong>/addNewContract</strong>



<h1>aktuell nicht genutzte Interfaces</h1>

<li><strong>/tariffMenu</strong>

<li><strong>/insuranceOrder/{birthdate}/{insuranceType}</strong>

<li><strong>/dbInsuranceOrder/{birthdate}/{insuranceType}</strong>

<li><strong>/calculator/{birthdate}/{insuranceType}</strong>


Rechner
*birthdate ddmmjjjj
*insuranceType 1/0

<li><strong>/calculator/{birthdate}/{wishedBegin}/{insuranceType}</strong>


Rechner
*birthdate ddmmjjjj
*wishedBegin unixtime
*insuranceType 1/0

<li><strong>/calculator/{birthdate}/{wishedBegin}/{insuranceType}/{missingTeeth}/{prosthesis}/{implantants}/{olderTenYrs}/{inCare}/{parodontitis}/{parodontitisCured}/{biteSplint}</strong>


Rechner
*birthdate ddmmjjjj
*wishedBegin unixtime
*insuranceType 1/0
