<?php
#error_reporting(E_ALL);
ini_set('display_errors','On');

//Germany
#setlocale(LC_ALL, 'de_DE');

# FIX CORS SETUP HEADER
$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "https://www.zahnzusatzversicherung-experten.de" || $http_origin == "https://sb.zahnzusatzversicherung-experten.de")
{  
    header("Access-Control-Allow-Origin: $http_origin");
}

header('Content-Type: application/json');

	
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();


spl_autoload_register(function ($class) 
{
	if(file_exists('../crm/classes/wzm/'.$class.'.class.php')) {
		require_once '../crm/classes/wzm/'.$class.'.class.php';
	}

	if(file_exists('../crm/classes/crm/'.$class.'.class.php')) {
		require_once '../crm/classes/crm/'.$class.'.class.php';
	}

	if(file_exists('../crm/web-modules/project/'.$class.'.class.php')) {
		require_once '../crm/web-modules/project/'.$class.'.class.php';
	}

	if(file_exists('../crm/web-modules/'.$class.'/'.$class.'.class.php')) {
		require_once '../crm/web-modules/'.$class.'/'.$class.'.class.php';
	}

	if($class == 'databaseResultSet') {
		require_once '../crm/web-modules/database/'.$class.'.class.php';
	}
	if(strstr($class, 'Helper')) {
		require_once '../crm/web-modules/helperClasses/'.$class.'.class.php';
	}
	if($class=='L' || $class=='S') {
		require_once '../crm/web-modules/core/'.$class.'.class.php';
	}
});

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();
#$app = new \Slim\Slim(array('templates.path' => './templates');
$app->contentType('application/json; charset=utf-8');

/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, `Slim::patch`, and `Slim::delete`
 * is an anonymous function.
 */

// GET route
$app->get(
    '',
    function () {
        $template = <<<EOT
<!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8"/>
            <title>Slim Framework for PHP 5</title>
            <style>
                html,body,div,span,object,iframe,
                h1,h2,h3,h4,h5,h6,p,blockquote,pre,
                abbr,address,cite,code,
                del,dfn,em,img,ins,kbd,q,samp,
                small,strong,sub,sup,var,
                b,i,
                dl,dt,dd,ol,ul,li,
                fieldset,form,label,legend,
                table,caption,tbody,tfoot,thead,tr,th,td,
                article,aside,canvas,details,figcaption,figure,
                footer,header,hgroup,menu,nav,section,summary,
                time,mark,audio,video{margin:0;padding:0;border:0;outline:0;font-size:100%;vertical-align:baseline;background:transparent;}
                body{line-height:1;}
                article,aside,details,figcaption,figure,
                footer,header,hgroup,menu,nav,section{display:block;}
                nav ul{list-style:none;}
                blockquote,q{quotes:none;}
                blockquote:before,blockquote:after,
                q:before,q:after{content:'';content:none;}
                a{margin:0;padding:0;font-size:100%;vertical-align:baseline;background:transparent;}
                ins{background-color:#ff9;color:#000;text-decoration:none;}
                mark{background-color:#ff9;color:#000;font-style:italic;font-weight:bold;}
                del{text-decoration:line-through;}
                abbr[title],dfn[title]{border-bottom:1px dotted;cursor:help;}
                table{border-collapse:collapse;border-spacing:0;}
                hr{display:block;height:1px;border:0;border-top:1px solid #cccccc;margin:1em 0;padding:0;}
                input,select{vertical-align:middle;}
                html{ background: #EDEDED; height: 100%; }
                body{background:#FFF;margin:0 auto;min-height:100%;padding:0 30px;width:440px;color:#666;font:14px/23px Arial,Verdana,sans-serif;}
                h1,h2,h3,p,ul,ol,form,section{margin:0 0 20px 0;}
                h1{color:#333;font-size:20px;}
                h2,h3{color:#333;font-size:14px;}
                h3{margin:0;font-size:12px;font-weight:bold;}
                ul,ol{list-style-position:inside;color:#999;}
                ul{list-style-type:square;}
                code,kbd{background:#EEE;border:1px solid #DDD;border:1px solid #DDD;border-radius:4px;-moz-border-radius:4px;-webkit-border-radius:4px;padding:0 4px;color:#666;font-size:12px;}
                pre{background:#EEE;border:1px solid #DDD;border-radius:4px;-moz-border-radius:4px;-webkit-border-radius:4px;padding:5px 10px;color:#666;font-size:12px;}
                pre code{background:transparent;border:none;padding:0;}
                a{color:#70a23e;}
                header{padding: 30px 0;text-align:center;}
            </style>
        </head>
        <body>
            <header>
                <a href="http://www.slimframework.com"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHIAAAA6CAYAAABs1g18AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABRhJREFUeNrsXY+VsjAMR98twAo6Ao4gI+gIOIKOgCPICDoCjCAjXFdgha+5C3dcv/QfFB5i8h5PD21Bfk3yS9L2VpGnlGW5kS9wJMTHNRxpmjYRy6SycgRvL18OeMQOTYQ8HvIoJKiiz43hgHkq1zvK/h6e/TyJQXeV/VyWBOSHA4C5RvtMAiCc4ZB9FPjgRI8+YuKcrySO515a1hoAY3nc4G2AH52BZsn+MjaAEwIJICKAIR889HljMCcyrR0QE4v/q/BVBQva7Q1tAczG18+x+PvIswHEAslLbfGrMZKiXEOMAMy6LwlisQCJLPFMfKdBtli5dIihRyH7A627Iaiq5sJ1ThP9xoIgSdWSNVIHYmrTQgOgRyRNqm/M5PnrFFopr3F6B41cd8whRUSufUBU5EL4U93AYRnIWimCIiSI1wAaAZpJ9bPnxx8eyI3Gt4QybwWa6T/BvbQECUMQFkhd3jSkPFgrxwcynuBaNT/u6eJIlbGOBWSNIUDFEIwPZFAtBfYrfeIOSRSXuUYCsprCXwUIZWYnmEhJFMIocMDWjn206c2EsGLCJd42aWSyBNMnHxLEq7niMrY2qyDbQUbqrrTbwUPtxN1ZZCitQV4ZSd6DyoxhmRD6OFjuRUS/KdLGRHYowJZaqYgjt9Lchmi3QYA/cXBsHK6VfWNR5jgA1DLhwfFe4HqfODBpINEECCLO47LT/+HSvSd/OCOgQ8qE0DbHQUBqpC4BkKMPYPkFY4iAJXhGAYr1qmaqQDbECCg5A2NMchzR567aA4xcRKclI405Bmt46vYD7/Gcjqfk6GP/kh1wovIDSHDfiAs/8bOCQ4cf4qMt7eH5Cucr3S0aWGFfjdLHD8EhCFvXQlSqRrY5UV2O9cfZtk77jUFMXeqzCEZqSK4ICkSin2tE12/3rbVcE41OBjBjBPSdJ1N5lfYQpIuhr8axnyIy5KvXmkYnw8VbcwtTNj7fDNCmT2kPQXA+bxpEXkB21HlnSQq0gD67jnfh5KavVJa/XQYEFSaagWwbgjNA+ywstLpEWTKgc5gwVpsyO1bTII+tA6B7BPS+0PiznuM9gPKsPVXbFdADMtwbJxSmkXWfRh6AZhyyzBjIHoDmnCGaMZAKjd5hyNJYCBGDOVcg28AXQ5atAVDO3c4dSALQnYblfa3M4kc/cyA7gMIUBQCTyl4kugIpy8yA7ACqK8Uwk30lIFGOEV3rPDAELwQkr/9YjkaCPDQhCcsrAYlF1v8W8jAEYeQDY7qn6tNGWudfq+YUEr6uq6FZzBpJMUfWFDatLHMCciw2mRC+k81qCCA1DzK4aUVfrJpxnloZWCPVnOgYy8L3GvKjE96HpweQoy7iwVQclVutLOEKJxA8gaRCjSzgNI2zhh3bQhzBCQQPIHGaHaUd96GJbZz3Smmjy16u6j3FuKyNxcBarxqWWfYFE0tVVO1Rl3t1Mb05V00MQCJ71YHpNaMcsjWAfkQvPPkaNC7LqTG7JAhGXTKYf+VDeXAX9IvURoAwtTFHvyYIxtnd5tPkywrPafcwbeSuGVwFau3b76NO7SHQrvqhfFE8kM0Wvpv8gVYiYBlxL+fW/34bgP6bIC7JR7YPDubcHCPzIp4+cum7U6NlhZgK7lua3KGLeFwE2m+HblDYWSHG2SAfINuwBBfxbJEIuWZbBH4fAExD7cvaGVyXyH0dhiAYc92z3ZDfUVv+jgb8HrHy7WVO/8BFcy9vuTz+nwADAGnOR39Yg/QkAAAAAElFTkSuQmCC" alt="Slim"/></a>
            </header>
            <h1>Welcome to Slim!</h1>
            <p>
                Congratulations! Your Slim application is running. If this is
                your first time using Slim, start with this <a href="http://docs.slimframework.com/#Hello-World" target="_blank">"Hello World" Tutorial</a>.
            </p>
            <section>
                <h2>Get Started</h2>
                <ol>
                    <li>The application code is in <code>index.php</code></li>
                    <li>Read the <a href="http://docs.slimframework.com/" target="_blank">online documentation</a></li>
                    <li>Follow <a href="http://www.twitter.com/slimphp" target="_blank">@slimphp</a> on Twitter</li>
                </ol>
            </section>
            <section>
                <h2>Slim Framework Community</h2>

                <h3>Support Forum and Knowledge Base</h3>
                <p>
                    Visit the <a href="http://help.slimframework.com" target="_blank">Slim support forum and knowledge base</a>
                    to read announcements, chat with fellow Slim users, ask questions, help others, or show off your cool
                    Slim Framework apps.
                </p>

                <h3>Twitter</h3>
                <p>
                    Follow <a href="http://www.twitter.com/slimphp" target="_blank">@slimphp</a> on Twitter to receive the very latest news
                    and updates about the framework.
                </p>
            </section>
            <section style="padding-bottom: 20px">
                <h2>Slim Framework Extras</h2>
                <p>
                    Custom View classes for Smarty, Twig, Mustache, and other template
                    frameworks are available online in a separate repository.
                </p>
                <p><a href="https://github.com/codeguy/Slim-Extras" target="_blank">Browse the Extras Repository</a></p>
            </section>
        </body>
    </html>
EOT;
        echo $template;
    }
);



function encode_items(&$item, $key)
{
    #$item = htmlentities( (string) $item, ENT_QUOTES, 'utf-8', FALSE);
    $item = iconv("Windows-1252", "UTF-8", $item);


    #$item = mb_convert_encoding($item,'Windows-1252','UTF-8');
}


/* INTRO OVER - HERE COMES ZZV */


$app->get('/', function () {
	header("HTTP/1.0 418 I'm A Teapot");
});

/* Alle Versicherungen */
$app->get('/getInsuranceList(/:insurance)', function ($insurance=null) {
	$c = new contract();
	$tl = $c->getInsuranceList($insurance);

	if(!$tl) 
	{
		header("HTTP/1.0 500 Internal Server Error");
		die('no data');
	}

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	array_walk_recursive($tl, 'encode_items');
	header('Content-Type: application/json; charset=utf-8');

	print json_encode($tl);

});


/* Alle Tarife */
$app->get('/tariffList', function () {
	$c = new contract();
	$tl = $c->getTariffList();

	if(!$tl) 
	{
		header("HTTP/1.0 500 Internal Server Error");
		die('no data');
	}

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	array_walk_recursive($tl, 'encode_items');
	header('Content-Type: application/json; charset=utf-8');

	print json_encode($tl);

});

$app->get('/getTariffList', function() use ($app) {
	$app->redirect('/rest/tariffList', 301);
});


/* Tarife nach idt */
$app->get('/tariffList/:insurance', function ($insurance) {
	$c = new contract();
	if(!is_numeric($insurance)) 
		$insurance = urlencode($insurance);

	$tl = $c->getTariffList($insurance);
	$tg = $c->getTariffGuidelines($insurance);
	
	if($tg)
		$tl[0]['missingTeethInsuranceableLimit'] = $tg[0]['missingTeethInsuranceableLimit'];

	if(!$tl) 
	{
		header("HTTP/1.0 500 Internal Server Error");
		die('no data');
	}

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	array_walk_recursive($tl, 'encode_items');
	header('Content-Type: application/json; charset=utf-8');

	print json_encode($tl);
});


$app->get('/getTariffList/:insurance', function($insurance) use ($app) {
	$app->redirect('/rest/tariffList/'.$insurance, 301);
});


/* Alle Tarife */
$app->get('/tariffListWithRating', function () {
	$c = new contract();

	$tl = $c->getTariffList();

	$wishedBegin = mktime(0, 0, 0, date("m")+1, 1, date("Y"));

	foreach($tl as $id => $val)
	{
		if(isset($val['insurance_id']))
			$in = $c->getTariffInsurance($val['insurance_id']);

		$tr = $c->getTariffRating($val['idt']);
		$tg = $c->getTariffGuidelines($val['idt']);
		$tf = $c->getTariffAVBFile($val['idt']);
		$bbt = $c->getBonusBaseTable($val['idt'], $wishedBegin);

		$t[$val['idt']] = $val;
		$t[$val['idt']]['insurance'] = $in;
		$t[$val['idt']]['rating'] = $tr; 
		$t[$val['idt']]['files'] = $tf;  
		$t[$val['idt']]['bonusBaseTable'] = $bbt;
 

		if($tg)
			$t[$val['idt']]['missingTeethInsuranceableLimit'] = $tg[0]['missingTeethInsuranceableLimit'];

		$class = array('K-KFO' => $val['isChildTariffWithKfoPayment'], 'ZE+ZB+ZR' => $val['showIfDenturesTreatmentProphylaxis'], 'ZE+ZB' => $val['showIfDenturesTreatment'], 'ZE+ZR' => $val['showIfDenturesProphylaxis'], 'ZB+ZR' => $val['showIfTreatmentProphylaxis'], 'ZE' => $val['showIfDentures'], 'ZB' => $val['showIfTreatment'], 'ZR' => $val['showIfProphylaxis'] );
		$t[$val['idt']]['class'] = $class;	
	}


	if(!$t) 
	{
		header("HTTP/1.0 500 Internal Server Error");
		die('no data');
	}

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($t); exit;
	}

	array_walk_recursive($t, 'encode_items');
	header('Content-Type: application/json; charset=utf-8');

	print json_encode($t);
});


$app->get('/getTariffListWithRating', function() use ($app) {
	$app->redirect('/rest/tariffListWithRating', 301);
});


/* Tarife nach idt */
$app->get('/tariffListWithRating/:insurance', function ($insurance) {
	$c = new contract();
	if(!is_numeric($insurance)) 
		$insurance = urlencode($insurance);

	$tl = $c->getTariffList($insurance);

	$wishedBegin = mktime(0, 0, 0, date("m")+1, 1, date("Y"));

	if($tl)
       {
	foreach($tl as $id => $val)
	{
		if(isset($val['insurance_id']))
			$in = $c->getTariffInsurance($val['insurance_id']);

		$tr = $c->getTariffRating($val['idt']);
		$tg = $c->getTariffGuidelines($val['idt']);
		$tf = $c->getTariffAVBFile($val['idt']);
		$bbt = $c->getBonusBaseTable($val['idt'], $wishedBegin);

		$t[$val['idt']] = $val;
		$t[$val['idt']]['insurance'] = $in;
		$t[$val['idt']]['rating'] = $tr; 
		$t[$val['idt']]['files'] = $tf;  
		$t[$val['idt']]['bonusBaseTable'] = $bbt;

		if($tg)
			$t[$val['idt']]['missingTeethInsuranceableLimit'] = $tg[0]['missingTeethInsuranceableLimit'];

		$class = array('K-KFO' => $val['isChildTariffWithKfoPayment'], 'ZE+ZB+ZR' => $val['showIfDenturesTreatmentProphylaxis'], 'ZE+ZB' => $val['showIfDenturesTreatment'], 'ZE+ZR' => $val['showIfDenturesProphylaxis'], 'ZB+ZR' => $val['showIfTreatmentProphylaxis'], 'ZE' => $val['showIfDentures'], 'ZB' => $val['showIfTreatment'], 'ZR' => $val['showIfProphylaxis'] );
		$t[$val['idt']]['class'] = $class;
	}
	} else echo "Tarif nicht aktiv oder keine Config?!?";

	if(!$t) 
	{
		header("HTTP/1.0 500 Internal Server Error");
		die(' no data');
	}

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($t); exit;
	}

	array_walk_recursive($t, 'encode_items');
	header('Content-Type: application/json; charset=utf-8');

#print_r($t);
	print json_encode($t);
});


$app->get('/getTariffListWithRating/:insurance', function($insurance) use ($app) {
	$app->redirect('/rest/tariffListWithRating/'.$insurance, 301);
});



/* Alle Tarife */
$app->get('/getTariffGuidelines', function () {
	$c = new contract();
	$tl = $c->getTariffGuidelines();

	if(!$tl) 
	{
		header("HTTP/1.0 500 Internal Server Error");
		die('no data');
	}

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	array_walk_recursive($tl, 'encode_items');
	header('Content-Type: application/json; charset=utf-8');

	print json_encode($tl);

});

/* Tarife nach idt */
$app->get('/getTariffGuidelines/:insurance', function ($insurance) {
	$c = new contract();
	if(!is_numeric($insurance)) 
		$insurance = urlencode($insurance);

	$tl = $c->getTariffGuidelines($insurance);

	if(!$tl) 
	{
		header("HTTP/1.0 500 Internal Server Error");
		die('no data');
	}

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	array_walk_recursive($tl, 'encode_items');
	header('Content-Type: application/json; charset=utf-8');

	print json_encode($tl);
});


/* Tarife LB nach idt */
$app->get('/tariffBenefitLimitation/:insurance', function ($insurance) {
	$c = new contract();

	if(!is_numeric($insurance)) 
		$insurance = urlencode($insurance);

	$tl = $c->getTariffBenefitLimitation($insurance);

	if(!$tl) 
	{
		header("HTTP/1.0 500 Internal Server Error");
		die('no data');
	}

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	array_walk_recursive($tl, 'encode_items');
	header('Content-Type: application/json; charset=utf-8');

	print json_encode($tl);
});


/* Tarife Menu nach Versicherung */
$app->get('/tariffMenu', function () {
	$c = new contract();
	$tl = $c->getTariffMenu();

	if(!$tl) 
	{
		header("HTTP/1.0 500 Internal Server Error");
		die('no data');
	}

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	array_walk_recursive($tl, 'encode_items');
	header('Content-Type: application/json; charset=utf-8');

	print json_encode($tl, 256);
});


$app->get('/insuranceOrder/:birthdate/:insurancetype', function ($birthdate, $insuranceType) {
	$c = new contract();
	if(strlen($birthdate)<>8) die('Birthdate not in adequate format - ddmmyyyy');
	if(strlen($insuranceType)<>1 && ($insuranceType > 1 || $insuranceType <0)) die('Insurance Type not in adequate format - (0 - 1)');

	switch($insuranceType)
	{
		case 0:
			$insuranceType = 'gesetzlich';			
			break;
		case 1:
			$insuranceType = 'heil';
			break;
	}

	$birthdate = substr($birthdate, -4).'-'.substr($birthdate, 3, 2).'-'.substr($birthdate, 2);
	$age = stringHelper::calcAge($birthdate);

	$io = $c->getInsuranceOrder($age, $insuranceType);
	print json_encode($io);
});


$app->get('/dbInsuranceOrder/:birthdate/:insurancetype', function ($birthdate, $insurancetype) {
	$c = new contract();
	if(strlen($birthdate)<>8) die('Birthdate not in adequate format - ddmmyyyy');
	if(strlen($insurancetype)<>1 && ($insurancetype > 1 || $insurancetype <0)) die('Insurance Type not in adequate format - (0 - 1)');

	switch($insuranceType)
	{
		case 0:
			$insuranceType = 'gesetzlich';			
			break;
		case 1:
			$insuranceType = 'heil';
			break;
	}

	$birthdate = substr($birthdate, -4).'-'.substr($birthdate, 3, 2).'-'.substr($birthdate, 2);
	$age = stringHelper::calcAge($birthdate);

	$io = $c->getDbInsuranceOrder($age, $insurancetype);
	print json_encode($io);
});


$app->get('/calculator/:birthdate/:insurancetype', function ($birthdate, $insurancetype) {

	if(strlen($birthdate)<>8) die('Birthdate not in adequate format - ddmmyyyy');
	if(strlen($insurancetype)<>1 && ($insurancetype > 1 || $insurancetype <0)) die('Insurance Type not in adequate format - (0 - 1)');
       
	$c = new contract();
	
	$data['formData'][1]['birthdate'] = substr($birthdate, -4).'-'.substr($birthdate, 3, 2).'-'.substr($birthdate, 2);
	$data['formData'][2]['wishedBegin'] = time();

	// $il = InsuranceList.
	$il = $c->getInsurances('adult', $data);

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($il); exit;
	}

	array_walk_recursive($il, 'encode_items');
	header('Content-Type: application/json; charset=utf-8');

	print json_encode($il);
});


$app->get('/calculator/:birthdate/:wishedBegin/:insurancetype', function ($birthdate, $wishedBegin, $insurancetype) {

	if(strlen($birthdate)<>8) die('Birthdate not in adequate format - ddmmyyyy');
	if(strlen($insurancetype)<>1 && ($insurancetype > 2 || $insurancetype <0)) die('Insurance Type not in adequate format - (0-2)');
       
	// Offen:
	// Prüfen, ob Kind oder Erw., 
	// Übertragen des Versicherungstypes.
	
	$c = new contract();
	
	$data['formData'][1]['birthdate'] = substr($birthdate, -4).'-'.substr($birthdate, 3, 2).'-'.substr($birthdate, 2);
	//$data['formData'][2]['wishedBegin'] = time();
	$data['formData'][2]['wishedBegin'] = $wishedBegin;

	// $il = InsuranceList.
	$il = $c->getInsurances('adult', $data);

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($il); exit;
	}

	array_walk_recursive($il, 'encode_items');
	header('Content-Type: application/json; charset=utf-8');

print json_encode($il);
});


$app->get('/calculator/:birthdate/:wishedBegin/:insurancetype/:missingTeeth/:prosthesis/:implantants/:olderTenYrs/:inCare/:parodontitis/:parodontitisCured/:biteSplint', function ($birthdate, $wishedBegin, $insuranceType, $missingTeeth, $prostesis, $implantants, $olderTenYears, $inCare, $periodontitis, $periodontitisCured, $biteSplint) {

	// Fragen für den Rechner Erwachsene

	// Prüfungen
	if(strlen($birthdate)<>8) die('Birthdate not in adequate format - ddmmyyyy');
	if(strlen($wishedBegin)<>10) die('wishedBegin not in adequate format - time() - length 10!');
	if(strlen($insurancetype)<>1 && ($insurancetype > 2 || $insurancetype <0)) die('Insurance Type not in adequate format - (0-2)');
	if($missingTeeth < 0 || $missingTeeth > 9) die('Missing Teeth must be between 0 and 9!');
	if($prostesis < 0 && $prostesis > 28) die('Prostesis shall be 0 - 28');
	if($implantans < 0 && $implantans > 28) die('Implantans shall be 0 - 28');
	if($olderTenYears < 1 && $olderTenYears > 9) die('Teeth older 10 Years possible values 2, 4, 5 - 9');
	if($inCare != '0' && $inCare != '1') die('InCare valid values 0 = no / 1 = yes ');
	if($periodontitis != '0' && $periodontitis != '1') die('periodontitis valid values 0 = no / 1 = yes ');
	if($periodontitisCured != '0' && $periodontitisCured != '1') die('periodontitisCured valid values 0 = no / 1 = yes ');
	if($biteSplint != '0' && $biteSplint != '1') die('biteSplint valid values 0 = no / 1 = yes ');	

/*
	$birthdate
	$wishedBegin
	$insuranceType
	$missingTeeth
	$prostesis
	$implantants
	$olderTenYears
	$inCare
	$periodontitis
	$periodontitisCured
	$biteSplint       
*/

	// Offen:
	// Prüfen, ob Kind oder Erw.? Andere Fragen bei Kind!
	// Übertragen des Versicherungstypes.
	


	$c = new contract();
	
	$data['formData'][1]['birthdate'] = substr($birthdate, -4).'-'.substr($birthdate, 3, 2).'-'.substr($birthdate, 2);
	//$data['formData'][2]['wishedBegin'] = time();
	$data['formData'][2]['wishedBegin'] = $wishedBegin;
	$data['formData'][2]['tooth1'] = $missingTeeth;
	$data['formData'][2]['tooth3'] = $prostesis;
	$data['formData'][2]['tooth4'] = $implantants;
	$data['formData'][2]['tooth5'] = $olderTenYears;
	$data['formData'][2]['incare'] = $inCare==0?'no':'yes';
	$data['formData'][2]['tooth5'] = $olderTenYears;
	$data['formData'][2]['periodontitis'] = $periodontitis==0?'no':'yes';
	$data['formData'][2]['periodontitisCured'] = $periodontitisCured==0?'no':'yes';
	$data['formData'][2]['biteSplint'] = $biteSplint==0?'no':'yes';

	// $il = InsuranceList.
	$il = $c->getInsurances('adult', $data);

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($il); exit;
	}

	array_walk_recursive($il, 'encode_items');
	header('Content-Type: application/json; charset=utf-8');

print json_encode($il);
});


/* Neue Routes / Aliase */

/* 4er Vergleiche */
$app->get('/tariffComparison/:insurance', function ($insurance) {

	$args = ['idt' => $insurance];

	$m = new modules($args);

	#if(!is_numeric($insurance)) 
	#	$insurance = urlencode($insurance);

	$tl = $m->getComparisonData();
#if($insurance == 75)
#	print_r($tl);

	$tl['bonusTable'] = $m->getBonusBase();
	$tl['files'] = $m->getTariffAVBFile($insurance);


	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);
});

/* Erstattungsbeispiele */
$app->get('/tariffRefundExamples/:insurance', function ($insurance) {

	$args = ['idt' => $insurance];

	$m = new modules($args);
	#if(!is_numeric($insurance)) 
	#	$insurance = urlencode($insurance);

	$tl = $m->getRefundExamples();

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);
});

$app->get('/tariffRefundExamplesDebug/:insurance', function ($insurance) {

	$args = ['idt' => $insurance];

	$m = new modules($args);

	$tl = $m->getRefundExamples(true);

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);
});


$app->get('/getBenefitsData/:insurance', function ($insurance) {

	$args = ['no-data' => '1'];

	$m = new modules($args);
	if(!is_numeric($insurance)) 
		$insurance = urlencode($insurance);

	$tl = $m->getBenefitsData($insurance);

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);
});

$app->get('/getRating/:insurance', function ($insurance) {

	$args = ['idt' => $insurance];

	$m = new modules($args);
	#if(!is_numeric($insurance)) 
	#	$insurance = urlencode($insurance);

	$tl = $m->getRating();

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);
});

$app->get('/getLongBenefits/:insurance(/:series)', function ($insurance, $series=0) {
	$b = new benefits($series);
	if(!is_numeric($insurance)) 
		$insurance = urlencode($insurance);

	$tl = $b->getLongBenefits($insurance);

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);
});

$app->get('/getShortBenefits/:insurance(/:series)', function ($insurance, $series=0) {
	if($series < 0 || $series > 2) $series = 0;
	$b = new benefits($series);
	if(!is_numeric($insurance)) 
		$insurance = urlencode($insurance);
#error_reporting(E_ALL);
	$tl = $b->getShortBenefits($insurance);

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		error_reporting(E_ALL);
		print_r($tl, true); exit;
	}

	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);
});


$app->get('/getInsuranceYearsTableNum/:insurance', function ($insurance) {
	$b = new benefits();
	if(!is_numeric($insurance)) 
		$insurance = urlencode($insurance);

	$tl = $b->getInsuranceYearsTableNum($insurance);

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);
});

$app->get('/getTooltips', function () {
	$c = new contract();
	$tl = $c->getTooltips();

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);
});



$app->get('/getCalculator', function () {

	$args = ['birthdate' => '1977-11-20', 
			'wishedBegin' => '1585692000',
			'missing-teeth' => 2, 
			'replaced-teeth-removable' => 3, 
			'dental-treatment' => 1, 
			'replaced-teeth-fix' => 5, 
			'replaced-teeth-older-10-years' => 7, 
			'periodontitis' => 0,
			'periodontitis-healed' => 0,
			'bite-splint' => 1];

	$m = new modules($args);

	$m->doCalc();

	$tl = $m->getCalculation();

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	array_walk_recursive($tl, 'encode_items');
	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);
});


$app->get('/getCalculator/:args+', 
	function ($args)
{

if(count($args) > 0)
{
	switch($args[0])
	{	
		case 0:
		# Erw.
		if(count($args) == 11) 
		{
			if(isset($args[1]) && strlen($args[1]) == 10)
				$birthdate = $args[1];
			if(isset($args[2]) && strlen($args[2]) == 10)
				$wishedBegin = $args[2];
			if(isset($args[3]) && $args[3] >= 0 && $args[3] < 11)
				$missingTeeth = $args[3];
			if(isset($args[4]) && $args[4] >= 0 && $args[4] < 29)
				$replacedTeethRemovable = $args[4];
			if(isset($args[5]) && $args[5] >= 0 && $args[5] < 2)
				$dentalTreatment= $args[5];
			if(isset($args[6]) && $args[6] >= 0 && $args[6] < 29)
				$replacedTeethFix = $args[6];
			if(isset($args[7]) && in_array($args[7], [2,5,6,7,8,9]))
				$replacedTeethOlder10Years= $args[7];
			if(isset($args[8]) && $args[8] >= 0 && $args[8] < 2)
				$periodontitis= $args[8];
			if(isset($args[9]) && $args[9] >= 0 && $args[9] < 2)
				$periodontitisHealed= $args[9];
			if(isset($args[10]) && $args[10] >= 0 && $args[10] < 2)
				$biteSplint= $args[10];
		} else 
			die('Anzahl der übergebenen Felder nicht richtig!');

		$args = ['type' => $args[0], 'birthdate' => $birthdate, 
			'wishedBegin' => $wishedBegin,
			'missing-teeth' => $missingTeeth, 
			'replaced-teeth-removable' => $replacedTeethRemovable, 
			'dental-treatment' => $dentalTreatment, 
			'replaced-teeth-fix' => $replacedTeethFix, 
			'replaced-teeth-older-10-years' => $replacedTeethOlder10Years, 
			'periodontitis' => $periodontitis,
			'periodontitis-healed' => $periodontitisHealed,
			'bite-splint' => $biteSplint];

			break;

		case 1:
		# Kinder
		if(count($args) == 5)
		{
			if(isset($args[1]) && strlen($args[1]) == 10)
				$birthdate = $args[1];
			if(isset($args[2]) && strlen($args[2]) == 10)
				$wishedBegin = $args[2];
			if(isset($args[3]) && $args[3] >= 0 && $args[3] < 2)
				$dentalTreatment= $args[3];
			if(isset($args[4]) && $args[4] >= 0 && $args[4] < 2)
				$orthodonticsTreatment= $args[4];

		$args = ['type' => $args[0], 'birthdate' => $birthdate, 
			'wishedBegin' => $wishedBegin,
			'dental-treatment' => $dentalTreatment, 
			'orthodontics-treatment' => $orthodonticsTreatment];

		} else 
			die('Anzahl der übergebenen Felder nicht richtig!');

			break;
	}

} else die('Anzahl der übergebenen Felder nicht richtig!');



	$m = new modules($args);

	$m->doCalc();

	$tl = $m->getCalculation();

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	array_walk_recursive($tl, 'encode_items');
	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);


});




/* Add Contract */
$app->post('/addNewContract', function() {

	$r = $_POST; 
	$c = new contract();


	$file = 'save.data'.date("dmY");
	file_put_contents($file, serialize($r).'~', FILE_APPEND | LOCK_EX); 


	if(isset($r['person_gender']) && ($r['person_gender'] == 'Herr' || $r['person_gender'] == 2))
		$r['person_gender'] = 'm';
	if(isset($r['person_gender']) && ($r['person_gender'] == 'Frau' || $r['person_gender'] == 1))
		$r['person_gender'] = 'f';

	if(isset($r['insure_person_gender']) && ($r['insure_person_gender'] == 'Herr' || $r['insure_person_gender'] == 2))
		$r['insure_person_gender'] = 'm';
	if(isset($r['insure_person_gender']) && ($r['insure_person_gender'] == 'Frau' || $r['insure_person_gender'] == 1))
		$r['insure_person_gender'] = 'f';

	if(isset($r['person_other_nationality']) && $r['person_nationality'] == 'other')
	{
		$r['person_nationality'] = $r['person_other_nationality'];
		unset($r['person_other_nationality']);
	} else unset($r['person_other_nationality']);


	if(isset($r['insure_person_other_nationality']) && $r['insure_person_nationality'] == 'other')
	{
		$r['insure_person_nationality'] = $r['insure_person_other_nationality'];
		unset($r['insure_person_other_nationality']);
	} else unset($r['insure_person_other_nationality']);


	if(isset($r['insure_person_firstname']) && isset($r['insure_person_lastname']) && $r['insure_person_firstname'] == '' && $r['insure_person_lastname'] == '')
	{
		foreach($r as $key => $val)
		{
			if(substr($key, 0, 7) == 'insure_')
				unset($r[$key]);
		}
	}

	if(isset($r['person_birthdate']))
	{
		$r['person_birthdate'] = stringHelper::convertDate($r['person_birthdate'], 'sql');
	}
	if(isset($r['insure_person_birthdate']))
	{
		$r['insure_person_birthdate'] = stringHelper::convertDate($r['insure_person_birthdate'], 'sql');
	}

	if(isset($r['wished_begin']))
	{
		$r['wished_begin'] = stringHelper::convertDate($r['wished_begin'], 'sql');
	}

	if(isset($r['display_price']))
	{
		$r['display_price'] = utf8_decode($r['display_price']);
		$r['display_price'] = str_replace(',','.',str_replace('?','',$r['display_price']));
	}


	$allowedFields = array('idt', 'person_gender', 'person_firstname', 'person_lastname', 'person_birthdate', 'person_nationality', 'person_job', 'person_gkv', 
				'insure_person_gender', 'insure_person_firstname', 'insure_person_lastname', 'insure_person_birthdate', 'insure_person_nationality', 'insure_person_job', 'insure_person_gkv',
				'address_street', 'address_postcode', 'address_city', 'contact_phone', 'contact_email', 'contact_msg', 'wished_begin', 'series', 'baseprice', 'surplus', 'display_price', 'browserAgent');

	foreach($r as $k => $v)
	{
		if(in_array($k, $allowedFields))
			$ret[$k] = $v;
	}


	if(isset($r['parameters']))
	{
		if(isset($r['parameters']['series']))
			$ret['series'] = $r['parameters']['series'];

		if(isset($r['parameters']['insurancetype']))
			$ret['insurancetype'] = $r['parameters']['insurancetype'];

		if(isset($r['parameters']['child']))
			$ret['child'] = $r['parameters']['child'];

		if(isset($r['parameters']['dentalTreatment']))
			$ret['dentalTreatment'] = $r['parameters']['dentalTreatment'];

		if(isset($r['parameters']['orthodonticsTreatment']))
			$ret['orthodonticsTreatment'] = $r['parameters']['orthodonticsTreatment'];

		if(isset($r['parameters']['missingTeeth']))
			$ret['missingTeeth'] = $r['parameters']['missingTeeth'];

		if(isset($r['parameters']['prosthesis']))
			$ret['prosthesis'] = $r['parameters']['prosthesis'];

		if(isset($r['parameters']['implantants']))
			$ret['implantants'] = $r['parameters']['implantants'];

		if(isset($r['parameters']['olderTenYrs']))
			$ret['olderTenYrs'] = $r['parameters']['olderTenYrs'];

		if(isset($r['parameters']['inCare']))
			$ret['inCare'] = $r['parameters']['inCare'];

		if(isset($r['parameters']['parodontitis']))
			$ret['periodontitis'] = $r['parameters']['parodontitis'];

		if(isset($r['parameters']['parodontitisCured']))
			$ret['periodontitisCured'] = $r['parameters']['parodontitisCured'];

		if(isset($r['parameters']['biteSplint']))
			$ret['biteSplint'] = $r['parameters']['biteSplint'];

		if(isset($r['parameters']['page']))
			$ret['sourcePath'] = $r['parameters']['page'];

	}

	$ret['created_at'] = date('Y-m-d H:i:s');
	$ret['source'] = 'zzv-experten.de';	
	$ret['to_transfer'] = 1;
	$ret['transferred'] = 0;

	
	$id = $c->addContractToMiddleware($ret);

	header('Content-Type: application/json; charset=utf-8');

	print $id;
});




/* Add Contract */
$app->post('/addNewContractTest', function() {
	$r = $_POST; 
	$c = new contract();

	unset($r['id']);

	if(isset($r['person_gender']) && ($r['person_gender'] == 'Herr' || $r['person_gender'] == 2))
		$r['person_gender'] = 'm';
	if(isset($r['person_gender']) && ($r['person_gender'] == 'Frau' || $r['person_gender'] == 1))
		$r['person_gender'] = 'f';

	if(isset($r['insure_person_gender']) && ($r['insure_person_gender'] == 'Herr' || $r['insure_person_gender'] == 2))
		$r['insure_person_gender'] = 'm';
	if(isset($r['insure_person_gender']) && ($r['insure_person_gender'] == 'Frau' || $r['insure_person_gender'] == 1))
		$r['insure_person_gender'] = 'f';

	if(isset($r['person_other_nationality']) && $r['person_nationality'] == 'other')
	{
		$r['person_nationality'] = $r['person_other_nationality'];
		unset($r['person_other_nationality']);
	} else unset($r['person_other_nationality']);


	if(isset($r['insure_person_other_nationality']) && $r['insure_person_nationality'] == 'other')
	{
		$r['insure_person_nationality'] = $r['insure_person_other_nationality'];
		unset($r['insure_person_other_nationality']);
	} else unset($r['insure_person_other_nationality']);


	if(isset($r['insure_person_firstname']) && isset($r['insure_person_lastname']) && $r['insure_person_firstname'] == '' && $r['insure_person_lastname'] == '')
	{
		foreach($r as $key => $val)
		{
			if(substr($key, 0, 7) == 'insure_')
				unset($r[$key]);
		}
	}

	if(isset($r['person_birthdate']))
	{
		$r['person_birthdate'] = stringHelper::convertDate($r['person_birthdate'], 'sql');
	}
	if(isset($r['insure_person_birthdate']))
	{
		$r['insure_person_birthdate'] = stringHelper::convertDate($r['insure_person_birthdate'], 'sql');
	}

	if(isset($r['wished_begin']))
	{
		$r['wished_begin'] = stringHelper::convertDate($r['wished_begin'], 'sql');
	}

	if(isset($r['display_price']))
	{
		$r['display_price'] = utf8_decode($r['display_price']);
		$r['display_price'] = str_replace(',','.',str_replace('?','',$r['display_price']));
	}


	$allowedFields = array('idt', 'person_gender', 'person_firstname', 'person_lastname', 'person_birthdate', 'person_nationality', 'person_job', 'person_gkv', 
				'insure_person_gender', 'insure_person_firstname', 'insure_person_lastname', 'insure_person_birthdate', 'insure_person_nationality', 'insure_person_job', 'insure_person_gkv',
				'address_street', 'address_postcode', 'address_city', 'contact_phone', 'contact_email', 'contact_msg', 'wished_begin', 'series', 'baseprice', 'surplus', 'display_price', 'browserAgent');

	foreach($r as $k => $v)
	{
		if(in_array($k, $allowedFields))
			$ret[$k] = $v;
	}

// person gender 1 = f
// person gender 2 = m
// geschlecht d - nicht abgedeckt
// gebdat insure_person_birthdate fehlt
// email kommt mit Fehler in Name (utf8!)

	if(isset($r['parameters']))
	{
		if(isset($r['parameters']['series']))
			$r['series'] = $r['parameters']['series'];

		if(isset($r['parameters']['insurancetype']))
			$r['insurancetype'] = $r['parameters']['insurancetype'];

		if(isset($r['parameters']['dentalTreatment']))
			$r['dentalTreatment'] = $r['parameters']['dentalTreatment'];

		if(isset($r['parameters']['orthodonticsTreatment']))
			$r['orthodonticsTreatment'] = $r['parameters']['orthodonticsTreatment'];

		if(isset($r['parameters']['missingTeeth']))
			$r['missingTeeth'] = $r['parameters']['missingTeeth'];

		if(isset($r['parameters']['prosthesis']))
			$r['prosthesis'] = $r['parameters']['prosthesis'];

		if(isset($r['parameters']['implantants']))
			$r['implantants'] = $r['parameters']['implantants'];

		if(isset($r['parameters']['olderTenYrs']))
			$r['olderTenYrs'] = $r['parameters']['olderTenYrs'];

		if(isset($r['parameters']['inCare']))
			$r['inCare'] = $r['parameters']['inCare'];

		if(isset($r['parameters']['parodontitis']))
			$r['parodontitis'] = $r['parameters']['parodontitis'];

		if(isset($r['parameters']['parodontitisCured']))
			$r['parodontitisCured'] = $r['parameters']['parodontitisCured'];

		if(isset($r['parameters']['biteSplint']))
			$r['biteSplint'] = $r['parameters']['biteSplint'];
	}


	if(isset($r['parameters']))
		unset($r['parameters']);
	if(isset($r['browserName']))
		unset($r['browserName']);
	if(isset($r['browserVersion']))
		unset($r['browserVersion']);
	if(isset($r['browserPlatform']))
		unset($r['browserPlatform']);

	$r['created_at'] = date('Y-m-d H:i:s');
	$r['source'] = 'zzv-experten.de';	
	$r['to_transfer'] = 1;
	$r['transferred'] = 0;

	
	$id = $c->addContractToMiddleware($r);

	header('Content-Type: application/json; charset=utf-8');

	print $id;
});




/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
$app->run();