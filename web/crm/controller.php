<?php

header('Content-Type: text/html; charset=UTF-8');

/* start session */
session_start();


error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors','On');

/*
 *	parameter
 *
 *	@cm
 *	@event
**/


$classModule = $_REQUEST['cm'];
$event = $_REQUEST['event'];


// check
if(empty($classModule) || empty($event)) {
	echo "parameter empty, nothing to do";
	exit;
}



function __autoload($className) {
	if(file_exists('classes/crm/'.$className.'.class.php')) {
		require_once 'classes/crm/'.$className.'.class.php';
		return;
	}

	if(file_exists('classes/wzm/'.$className.'.class.php')) {
		require_once 'classes/wzm/'.$className.'.class.php';
		return;
	}

	if($className == 'tFPDF') {
		require_once('libs/tfpdf131/tfpdf.php');
 		return; 
	} 

	if($className == 'FPDI') {
		require_once('libs/FPDI-162/fpdi_bridge.php');
		require_once('libs/FPDI-162/fpdf_tpl.php');
		require_once('libs/FPDI-162/pdf_context.php');
		require_once('libs/FPDI-162/pdf_parser.php');
		require_once('libs/FPDI-162/fpdi_pdf_parser.php');
		require_once('libs/FPDI-162/fpdi.php');
 		return; 
	}
	if($className == 'PDF') {
		require_once('libs/PDF/pdf.php');
 		return; 
	}

	if($className == 'AlphaPDF') {
		require_once('libs/alpha/alphapdf.php');
 		return;
	}
	if($className == 'PHPMailer') {
		require_once('libs/PHPMailer_5.5.23/class.phpmailer.php');
		return;
	}

	$folderName = $className;
	$folderName = str_replace('Controller', '', $folderName);
	if($strPos = strpos($className, 'View')) {
		$folderName = substr($className, 0, $strPos);
	}

	if($className == 'databaseResultSet') {
		$folderName = 'database';
	}
	elseif(strstr($className, 'Helper')) {
		$folderName = 'helperClasses';
	}
	elseif((substr($className, 0, 4)=='core') ||  $className=='view'
		|| $className=='L' || $className=='S') {
		$folderName = 'core';
	}

	require_once 'web-modules/'.$folderName.'/'.$className.'.class.php';

}




// load controller
$controllerClass = $classModule.'Controller';
eval("\$controller = new $controllerClass();");

if(!empty($controller)) {
	$ret = $controller->handleEvent($event);
	if(empty($ret['err'])) {
		// check for forwarding
		if(isset($ret['forward'])) {
			$forwardCM = (isset($ret['forward']['classModule']))?
				$ret['forward']['classModule']:$classModule;
			$url = urlHelper::makeCoreURL($forwardCM,
				$ret['forward']['event'], 
				isset($ret['forward']['parameter'])?$ret['forward']['parameter']:"",
				isset($ret['forward']['ank'])?$ret['forward']['ank']:"");
			header('Location: '.$url);
			exit;
		}
		// otherwise we should have a view
		elseif(isset($ret['view'])) {

			$view = $ret['view'];
			$viewErr = $view->getErr();
			if(!empty($viewErr)) {
				echo errorHandler::genMessage($viewErr);
			}
			else {
				if(($parent=get_parent_class($view))=='view' ||
				    (get_parent_class($parent))=='view') {
					echo $view->view();
				}else {
					echo "Wow, that was a very good run!";
				}
			}
		}
		// even otherwise we should have some code to view
		elseif(isset($ret['code'])) {
			echo $ret['code'];
		}
		// else we have a problem
		else {
			// disabled for sending headers for downloads correctly
			// echo "I found no job to do :(";
		}
	}
	else {
		if(strstr($ret['errmsg'], 'no valid formId')!==FALSE ||
			$ret['errmsg'] == 'form source infos missing') {
			header('Location: '.urlHelper::makeCoreURL('crmIntern', 'home'));
			exit;
		}
		$view = errorHandler::getView($ret);
		echo $view->view();
	}
}else {
	echo "controller empty";
}
?>