<?php

class importFromCsv {

private $ids = array();

public function start() {
	$handle = fopen ("import/praemientabelle-2502-2009.csv","r");
	$i=0; $start=false;
	while ( ($data = fgetcsv ($handle, 1000, ";")) !== FALSE ) {
		/*
		if(is_int($data[1]) && $data[1]==0) {
			$start=true;
		}
		*/

		if($i==2) $this->grepIds($data);
		if($i==4) $start=true;

		if($start) {
			echo "<pre>".print_r($data, true)."</pre>";
			$this->importRow($data);
		}
		$i++;
	}
	fclose ($handle);
}

public function importRow($data) {
	$wzm = new wzm();
	$c = count($data);
	for($i=3; $i<$c; $i++) {
		$row = array(
			'idt' => $this->ids[$i],
			'age' => $data[1],
			'gender' => 'male',
			'bonus' => str_replace(',', '.', $data[$i])
		);
		$wzm->addBonusBase($row);
		$i++;
		$row = array(
			'idt' => $this->ids[$i],
			'age' => $data[1],
			'gender' => 'female',
			'bonus' => str_replace(',', '.', $data[$i])
		);
		$wzm->addBonusBase($row);
	}
}


private function grepIds($data) {
	$col = 3;
	while(!empty($data[$col])) {
		$id = substr($data[$col], 0, strpos($data[$col], '-'));
		$this->ids[$col] = $id;
		$this->ids[$col+1] = $id;
		$col+=2;
	}
// 	print_r($this->ids);
}

// text transformations
private function t($text) {
// 	$ret = html_entity_decode($text, ENT_QUOTES, "ISO8859-15");
	$ret = str_replace("'", "\'", $text);
	return $ret;
}

}

?>