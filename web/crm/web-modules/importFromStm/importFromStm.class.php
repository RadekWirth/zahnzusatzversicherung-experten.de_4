<?php

class importFromStm {


public function start() {
	$mjf = new myjobfuture();
	$mjf->clearTables();
 	$this->importApplicants();
	$this->importCompanies();
}


private function getSourceDb() {
	return new database("import", 'localhost', 'v164064',
		'v164064', '3kj47u3x');
}


// -- NOTES START ---------------
private function importNotes($uid, $bewerber) {
	$dbS = $this->getSourceDb();
	$mjf = new myjobfuture();
	$sourceRs = $dbS->query("SELECT * FROM stm2_telnotiz WHERE kennziffer=".$bewerber['Kennziffer']);
	while($rowS = $sourceRs->fetch()) {
		$mjf->addNote($uid, $this->t($rowS['text']), $rowS['datum']);
	}
}
// -- NOTES START ---------------



// -- COMPANY START ---------------
private function importCompanies() {
	$dbS = $this->getSourceDb();
	$mjf = new myjobfuture();
	$company = new company();
	$user = new user();

	$sourceRs = $dbS->query("SELECT * FROM stm_firma");
	while($rowS = $sourceRs->fetch()) {
		$dataUser = array(
		'type' => 'company',
		'name' => 'f'.$rowS['Kennziffer'],
		'password' => $rowS['Passwort'],
		'email' => $rowS['Email']
		);

		$uid = $user->masterAdd($dataUser);

		$dataCompany = array(
		'uid' => $uid,
		'name' => $this->t($rowS['Firma']),
		'faculty' => $this->t($rowS['Fachbereich']),
		'contactForename' => $this->t($rowS['Vorname']),
		'contactSurname' => $this->t($rowS['Name']),
		'contactDepartment' => $this->t($rowS['Abteilung']),
		'staff' => $this->t(($rowS['MitarbeiterAnz']==0)?0:$rowS['MitarbeiterAnz']),
		'profile' => $this->t($rowS['Profil'])
		);

		$cid = $company->add($dataCompany);

		$dataAddress = array(
		'uid' => $uid,
		'street' => $this->t(substr($rowS['Strasse'], 0, 100)),
		'postcode' => $this->t($rowS['PLZ']),
		'city' => $this->t(substr($rowS['Ort'], 0, 100)),
		'country' => 'DEU'
		);

		$mjf->addAddress($dataAddress);

		$dataContact = array(
		'uid' => $uid,
		'phoneBusiness' => $this->t($rowS['Telefon']),
		'fax' => $this->t($rowS['Telefax']),
		'emailBusiness' => '',
		'homepage' => $this->t($rowS['Homepage'])
		);

		$mjf->addContact($dataContact);

		$this->importCompanyJobOffers($cid, $rowS);
	}
}


private function importCompanyJobOffers($cid, $firma) {
	$dbS = $this->getSourceDb();
	$mjf = new myjobfuture();
	$company = new company();
	$user = new user();

	$sourceRs = $dbS->query("SELECT * FROM stm_stellenangebot WHERE Firma_Kennziffer=".$firma['Kennziffer']);
	while($rowS = $sourceRs->fetch()) {
		$dataOffer = array(
		'cid' => $cid,
		'active' => $rowS['Aktiviert'],
		'contactEmail' => $this->t($rowS['EmailBewerb']),
		'jobTypeId' => ($rowS['PositionNr']==0)?0:$rowS['PositionNr']+4,
		'position' => $this->t($rowS['Titel']),
		'description' => $this->t($rowS['Beschreibung']),
		'location' => $this->t($rowS['Region']),
		'creationDate' => $this->t($rowS['Datum']),
		'finishDate' => $this->t($rowS['Ablaufdatum'])
		);

		$uid = $company->addJobOffer($dataOffer);
	}
}
// -- COMPANY END -----------------

// -- APP START -------------------
private function importApplicants() {
	$dbS = $this->getSourceDb();
	$mjf = new myjobfuture();
	$user = new user();

	$sourceRs = $dbS->query("SELECT * FROM stm_bewerber");
	while($rowS = $sourceRs->fetch()) {

		// handle freelancers
		if($rowS['Kennziffer'] == 1103 ||
			$rowS['Kennziffer'] == 1183 ||
			$rowS['Kennziffer'] == 1173 ||
			$rowS['Kennziffer'] == 1157) {

			$this->importFreelancer($rowS);
			continue;
		}

		$dataUser = array(
		'type' => 'applicant',
		'name' => 'b'.$rowS['Kennziffer'],
		'password' => $rowS['Passwort'],
		'email' => $rowS['Email']
		);

		$uid = $user->masterAdd($dataUser);
		$mjf->addFeedback($uid, 'findUs', $rowS['WieGefunden']);

		$dataPerson = array(
		'uid' => $uid,
		'forename' => $this->t($rowS['Vorname']),
		'surname' => $this->t($rowS['Name']),
		'nationaility' => $this->convertPersonNationality($rowS['Staatsangehoerigkeit']),
		'sex' => $rowS['GeschlechtNr'],
		'birthdate' => $rowS['Geburtstag'],
		'birthplace' => $this->t(substr($rowS['Geburtsort'], 0, 100)),
		'familyStatusId' => ((empty($rowS['FamilienstandNr']))?0:$rowS['FamilienstandNr']+53),
		'children' => $rowS['KinderAnz']
		);

		$mjf->addPerson($dataPerson);

		$dataAddress = array(
		'uid' => $uid,
		'street' => $this->t(substr($rowS['Strasse'], 0, 100)),
		'postcode' => $this->t($rowS['PLZ']),
		'city' => $this->t(substr($rowS['Ort'], 0, 100)),
		'country' => $this->convertAddressCountry($rowS['Land'])
		);

		$mjf->addAddress($dataAddress);

		$dataContact = array(
		'uid' => $uid,
		'phone' => $this->t($rowS['TelefonPriv']),
		'phoneBusiness' => $this->t($rowS['TelefonGesch']),
		'mobile' => $this->t($rowS['Mobil']),
		'fax' => '',
		'emailBusiness' => '',
		'homepage' => $this->t($rowS['Homepage'])
		);

		$mjf->addContact($dataContact);

		$this->importJobSearch($uid, $rowS);
		$this->importNotes($uid, $rowS);
	}
}

private function importJobSearch($uid, $bewerber) {
	$dbS = $this->getSourceDb();
	$mjf = new myjobfuture();
	$applicant = new applicant();
	$user = new user();

	$sourceJs = $dbS->query("SELECT * FROM stm_stellengesuch WHERE Bewerber_Kennziffer=".$bewerber['Kennziffer']);
	$rowJ = $sourceJs->fetch();

	$dataJobSearch = array(
	'uid' => $uid,
	'active' => $rowJ['Aktiviert'],
	'career' => $this->t($bewerber['Werdegang']),
	'explainChange' => $this->t($bewerber['VeraenderungBegr']),
	'commercialEmployee' => $this->t($bewerber['GewerbAN']),
	'wantedBranchId' => ($rowJ['BerufsfeldNr']==0)?0:$rowJ['BerufsfeldNr']+4,
	'wantedPosition' => $this->t($rowJ['Titel']),
	'wantedYearlySalary' => $this->t($rowJ['Brutto']),
	'wantedWorkModelId' => ($rowJ['ArbeitsmodellNr']==0)?0:$rowJ['ArbeitsmodellNr']+45,
	'mobileDrivingId' => ($rowJ['BerufspendlerNr']==0)?0:$rowJ['BerufspendlerNr']+49,
	'mobileDrive' => $this->t(($rowJ['BerufspendlerNr']==1)?0:1),
	'mobileDriveText' => $this->t($rowJ['ReisebereitschaftFrei']),
	'mobileMove' => $this->t(($rowJ['UmzugNr']==1)?0:1),
	'mobileMoveText' => $this->t($rowJ['UmzugFrei']),
	'creationDate' => $rowJ['Datum']
	);

	$jsid = $applicant->addJobSearch($dataJobSearch);

	$lang = $this->getEducationLanguages($bewerber['Kennziffer']);

	$dataEducation = array(
	'jsid' => $jsid,
	'educationId' => $bewerber['SchulausbildungNr'],
	'apprenticeship1' => $this->t($bewerber['Berufsausbildung']),
	'apprenticeship2' => $this->t($bewerber['Berufsausbildung2']),
	'jobExperience' => (($bewerber['BerufserfahrungNr']==1 || $bewerber['BerufserfahrungNr']==0)?0:($bewerber['BerufserfahrungNr']==2)?3:7),
	'studyId' => ($bewerber['StudiumNr']==1 || $bewerber['StudiumNr']==0)?0:$bewerber['StudiumNr']+3,
	'studyBranchId' => ($bewerber['StudienrichtungNr']<=1)?0:$bewerber['StudienrichtungNr']+6,
	'studyName' => $this->t($bewerber['Studiengang']),
	'studyPlace' => $this->t($bewerber['Hochschule']),
	'studyCertificateId' => ($bewerber['AbschlussNr']==0 || $bewerber['AbschlussNr']==1)?0:$bewerber['AbschlussNr']+14,
	'studyEndDate' => $bewerber['AbschlussDatum'],
	'militaryId' => ($bewerber['WehrdienstNr']==0 || $bewerber['WehrdienstNr']==4)?0:$bewerber['WehrdienstNr']+24,
	'trainings' => $this->t($bewerber['Weiterbildung']),
	'languageId1' => $lang[0]['lang'],
	'languageSkillId1' => $lang[0]['skill'],
	'languageId2' => $lang[1]['lang'],
	'languageSkillId2' => $lang[1]['skill'],
	'languageId3' => $lang[2]['lang'],
	'languageSkillId3' => $lang[2]['skill'],
	'itKnowledge' => $this->t($bewerber['ITKenntnisse']),
	'freetext' => $this->t($bewerber['FreitextAusb'])
	);

	$eid = $applicant->addEducation($dataEducation);

	$this->importJobs($jsid, $bewerber['Kennziffer']);
}


private function importJobs($jsid, $kennziffer) {
	$dbS = $this->getSourceDb();
	$applicant = new applicant();
	$rs = $dbS->query("SELECT * FROM stm_position WHERE Bewerber_Kennziffer=".$kennziffer);
	$i=0;
	while($row = $rs->fetch()) {
		if($i>1) break;
		$type='current';
		if($i>0) $type='last';

		$dataJob = array(
		'jsid' => $jsid,
		'type' => $type,
		'definition' => $this->t($row['PosDefinition']),
		'description' => $this->t($row['PosBeschreibung']),
		'startDate' => $this->t($row['ZeitraumVon']),
		'endDate' => $row['ZeitraumBis'],
		'company' => $this->t($row['Arbeitgeber']),
		'jobTypeAsText' => $this->t($row['Branche']),
		'cancelationPeriod' => $this->t($row['Kuendigungsfrist']),
		'leader' => ($row['FuehrungsverantwNr']==1 || $row['FuehrungsverantwNr']==0)?0:1,
		'shelterWorkers' => $this->t($row['UnterstMitarb']),
		'budgetCharge' => $row['Budgetverantw'],
		'yearlySalary' => $this->t($row['Gehalt']),
		'workDescription' => $this->t($row['AufgBeschreibung']),
		'freetext' => $this->t($row['FreitextPos'])
		);
		$i++;
		$applicant->addJob($dataJob);
	}
}

private function getEducationLanguages($kennziffer) {
	$dbS = $this->getSourceDb();

	$lang = array(
		0 => array('lang' => 0, 'skill' => 0),
		1 => array('lang' => 0, 'skill' => 0),
		2 => array('lang' => 0, 'skill' => 0));
	$i=0;
	$sourceJs = $dbS->query("SELECT * FROM stm_bewerber_has_stm_sprache WHERE Bewerber_Kennziffer=".$kennziffer);
	while($row = $sourceJs->fetch()) {
		if($row['SpracheNr']==1) $lang[$i]['lang'] = 0;
		else $lang[$i]['lang'] = $row['SpracheNr']+27;

		if($row['KenntnisstandNr']==1) $lang[$i]['skill'] = 0;
		else if($row['KenntnisstandNr']==2 || $row['KenntnisstandNr']==3) $lang[$i]['skill'] = 41;
		else if($row['KenntnisstandNr']==4) $lang[$i]['skill'] = 42;
		else if($row['KenntnisstandNr']==5) $lang[$i]['skill'] = 43;
		else if($row['KenntnisstandNr']==6) $lang[$i]['skill'] = 45;

		$i++;
	}

	return $lang;
}
// -- APP End -------------------


// -- FREE START -------------------
private function importFreelancer($rowS) {
	$dbS = $this->getSourceDb();
	$mjf = new myjobfuture();
	$user = new user();
	$freelancer = new freelancer();

	$dataUser = array(
	'type' => 'freelancer',
	'name' => 's'.$rowS['Kennziffer'],
	'password' => $rowS['Passwort'],
	'email' => $rowS['Email']
	);

	$uid = $user->masterAdd($dataUser);
	$mjf->addFeedback($uid, 'findUs', $rowS['WieGefunden']);

	$dataAddress = array(
	'uid' => $uid,
	'street' => $this->t(substr($rowS['Strasse'], 0, 100)),
	'postcode' => $this->t($rowS['PLZ']),
	'city' => $this->t(substr($rowS['Ort'], 0, 100)),
	'country' => $this->convertAddressCountry($rowS['Land'])
	);

	$mjf->addAddress($dataAddress);

	$dataContact = array(
	'uid' => $uid,
	'phone' => $this->t($rowS['TelefonPriv']),
	'phoneBusiness' => $this->t($rowS['TelefonGesch']),
	'mobile' => $this->t($rowS['Mobil']),
	'fax' => '',
	'emailBusiness' => '',
	'homepage' => $this->t($rowS['Homepage'])
	);

	$mjf->addContact($dataContact);

	$dataFree = array(
	'uid' => $uid,
	'salutation' => $rowS['AnredeNr'],
	'forename' => $this->t($rowS['Vorname']),
	'surname' => $this->t($rowS['Name']),
	'profession' => $this->t($rowS['Berufsausbildung']),
	'studyCertificateId' => ($rowS['AbschlussNr']==0 || $rowS['AbschlussNr']==1)?0:$rowS['AbschlussNr']+14,
	);

	$fid = $freelancer->add($dataFree);
	$this->importNotes($uid, $rowS);

	// add profile
	$rs = $dbS->query("SELECT * FROM stm_stgselbstaendig WHERE Bewerber_Kennziffer=".$rowS['Kennziffer']);
	$row = $rs->fetch();
	$dataProfile = array(
	'fid' => $fid,
	'active' => $row['Aktiviert'],
	'worktype' => $this->t($row['Taetigkeitsart']),
	'description' => $this->t($row['TaetigkeitFrei']),
	'branches' => $this->t($row['Branchen']),
	'workReferences' => $this->t($row['Referenzen']),
	'workComment' => $this->t($row['Anmerkungen']),
	'creationDate' => $row['Datum'],
	'finishDate' => $row['Ablaufdatum']
	);

	$fpid = $freelancer->addProfile($dataProfile);
// 	if(empty($fpid)) {
// 	$this->error("profile add,  fid: ".$fid.", knzf: ".$rowS['Kennziffer']);
// 	}
}
// -- FREE END -------------------

//-------------------------------------------------
private function convertPersonNationality($value) {
	if($value=='deutsch') return 'DEU';
	if($value=='rum&auml;nisch') return 'ROU';
	if($value=='&ouml;sterreich') return 'AUT';
	if($value=='italienisch') return 'ITA';
	if($value=='griech./orthodox') return 'GRC';
	if($value=='dt') return 'DEU';
	if($value=='D') return 'DEU';
	if($value=='Deutscher') return 'DEU';
	if($value=='CH') return 'CHE';
	return '';
}


private function convertAddressCountry($value) {
	if($value=='Deutschland') return 'DEU';
	if($value=='Baden-W&uuml;rttemberg') return 'DEU';
	if($value=='BRD') return 'DEU';
	if($value=='Deutschland, Baden-W&uuml;rttemberg') return 'DEU';
	if($value=='Baden W&uuml;rttemberg') return 'DEU';
	if($value=='D') return 'DEU';
	if($value=='germany') return 'DEU';
	if($value=='BW') return 'DEU';
	if($value=='Deutschland / Germany') return 'DEU';
	if($value=='Th&uuml;ringen') return 'DEU';
	if($value=='BaW&uuml;') return 'DEU';
	if($value=='Bayern') return 'DEU';
	if($value=='Deutschland-Bayern') return 'DEU';
	if($value=='baden-w&uuml;rtemberg') return 'DEU';
	if($value=='BW') return 'DEU';
	if($value=='Schweiz') return 'CHE';
	return '';
}

// text transformations
private function t($text) {
	$ret = html_entity_decode($text, ENT_QUOTES, "ISO8859-15");
	$ret = str_replace("'", "\'", $ret);
	return $ret;
}

private function error($msg) {
	echo "<br />".$msg;
}


} // end class

?>