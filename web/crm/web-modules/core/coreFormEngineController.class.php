<?php

class coreFormEngineController {

public function handleEvent($event) {

switch($event) {

	case 'receiveData':
		$ret = $this->handleReceivedData($_POST, $_REQUEST['formId']);
		break;

	default:
		return array('err' => 1, 'errmsg' => 'event could not be handled',
			'errclass' => get_class($this));

}

return $ret;
}

private function handleReceivedData($receivedData, $formId) {
// save post data to have the possibility to recycle it on an error
$_SESSION['formEngine']['postDump'] = $receivedData;

// check other needed session vars
if(empty($_SESSION['formEngine']['classModule']) ||
	empty($_SESSION['formEngine']['event'])) {

	return array('err' => 3, 'errmsg' => 'form source infos missing',
		'errclass' => get_class($this));
}

// check formId
if($_SESSION['formEngine']['formId']!=$formId) {
	return array('err' => 2, 'errmsg' => 'no valid formId ('.
		$_SESSION['formEngine']['formId'].' != '.$formId.')',
		'errclass' => $_SESSION['formEngine']['classModule'],
		'errevent' => $_SESSION['formEngine']['event']);
}

/*
	here we can perform some security checks
	with the post data $receivedData
	skipped for this release
*/

// create forward statement for return
$forward = array('classModule' => $_SESSION['formEngine']['classModule'],
	'event' => $_SESSION['formEngine']['event'],
	'parameter' => $_SESSION['formEngine']['parameter']);


// destroy old form session
unset($_SESSION['formEngine']);

// save data
$_SESSION['formEngine']['receivedData'] = $receivedData;

return array('err' => 0, 'forward' => $forward);
}


} // end class

?>