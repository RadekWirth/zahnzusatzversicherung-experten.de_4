<?php

/*
	Language Class

	for easier use this class name is very short,
	because translation is very often used.

*/


class L {

// -----------------------------------------------------------------------------
public function __construct() {
}

// -----------------------------------------------------------------------------
/*
	this method returns the translation for parameter $translate
	it uses the default target language, set by user
*/
public static function _($phraseId, $phraseIdEnd=null, $noEntities=false) {
	global $db_main;

	// set standard language
	$lang = 'de';

	// overwrite standard target language if a individual is set
	$langRet = L::getLanguageInterface();
	if($langRet['err']==0) {
	$lang = $langRet['lang'];
	}

	$db = project::getDb();

	if(is_numeric($phraseId)) {
		if(!empty($phraseIdEnd)) {
			$rs = $db->queryPDO("SELECT phrase FROM language ".
				"WHERE id >= '".$phraseId."' AND id <= '".$phraseIdEnd."' ".
				"AND lang='".$lang."'");
		}
		else {
			$rs = $db->queryPDO("SELECT phrase FROM language ".
				"WHERE id='".$phraseId."' AND lang='".$lang."'");
		}
	}
	else {
		$rs = $db->queryPDO("SELECT phrase FROM language ".
 			"WHERE textid='".$phraseId."' AND lang='".$lang."'");
	}

	if($phraseIdEnd && $db->getNumRows() > 1) {
		$ret = array();
		while($row = $db->fetchPDO($rs)) {
			if($noEntities) {
				$ret[] = $row['phrase'];
			}
			else {
				$ret[] = str_replace('\n', '<br />',
					nl2br($row['phrase']));
			}
		}
	}
	else {
		$row = $db->fetchPDO($rs);
		if($noEntities) {
			$ret = $row['phrase'];
		}
		else {
			$ret = str_replace('\n', '<br />', nl2br($row['phrase']));
		}
	}
	return $ret;
}
// -----------------------------------------------------------------------------
public static function errMsg($errClass, $errMsg) {
	global $db_main;

	$db = project::getDb();

	$rs = $db->queryPDO("SELECT phrase FROM language ".
		"WHERE class='".$errClass."' AND errmsg='".$errMsg."' AND lang='de'");

	$row = $db->fetchPDO($rs);
	if(empty($row)) {
		$row['phrase'] = 'Error in '.$errClass.': '.$errMsg;
	}
	return $row['phrase'];
}
// -----------------------------------------------------------------------------
public static function setLanguageInterface($lang) {
	$_SESSION['language']['interface'] = $lang;
}

public static function getLanguageInterface() {
	if(!empty($_SESSION['language']['interface'])) {
	return array('err' => 0,
		'lang' => $_SESSION['language']['interface']);
	}else {
	return array('err' => 1);
	}
}
// -----------------------------------------------------------------------------

} // end class

?>
