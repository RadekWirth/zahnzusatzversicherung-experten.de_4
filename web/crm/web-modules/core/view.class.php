<?php

abstract class view {

private $code;
private $err;
private $replCodes;
protected $pageId;
protected $dataProcessed;

public function __construct($init=null, $template=null) {

	// defaults
	$this->pageId = 1;

	// load std template
	if(empty($template)) {
		$template = 'templates/index.tpl.html';
	}
	$this->code = implode("",file($template));

	$this->replace('OptionDayStart', $this->getOptions('day', $_GET['vonDateD']));
	$this->replace('OptionDayEnd', $this->getOptions('day', $_GET['bisDateD']));
	$this->replace('OptionMonthStart', $this->getOptions('month', $_GET['vonDateM']));
	$this->replace('OptionMonthEnd', $this->getOptions('month', $_GET['bisDateM']));
	$this->replace('OptionYearStart', $this->getOptions('year', $_GET['vonDateY']));
	$this->replace('OptionYearEnd', $this->getOptions('year', $_GET['bisDateY']));
	$this->replace('OptionABC', $this->getOptions('abc'));
	$this->replace('UuId', user::getUid());

	if($init) {
		if(isset($init['successMsg'])) {
		$this->setSuccessMsg($init['successMsg']);
		}
	}

	$this->dataProcessed = false;
}

protected function replace($key, $value) {
	$this->replCodes[$key] = $value;
	$this->code = str_replace('${'.$key.'}', $value, $this->code);
}

public function generateLinkLine($filterDate = null, $count, $itemsPerPage=50) {
	if($count>$itemsPerPage)
		{ 

			
			$httpRoot = "/controller.php";

			foreach($_GET as $item => $val)
			{
				if($item != "page")
				{
					if(!$addurl) $addurl = "?";
	
					if($addurl && $addurl != "?")
						$addurl .= "&";

					if($item == "telefon" && $val == "on")
						$val = "1";

					$addurl .= $item."=".$val;
				}
			}

			$linkpages = $count / $itemsPerPage;	

			$linkpageshtml .= "Seite ";
			for($i = 0; $i <= $linkpages; $i++)
			{
				$linkpageshtml .= "<a href='$httpRoot$addurl&page=$i'>".($i+1)." </a>";
			}
			$linkpageshtml .= " anzeigen";
		return $linkpageshtml;

	}
}

public function getReplaceCode($key) {
	return $this->replCodes[$key];
}

public function view() {
	if(!$this->dataProcessed) {
		$this->processData();
	}
	$this->replaceParts();
	return $this->code;
}

private function setSuccessMsg($msg) {
	$msg = str_replace('\n', '<br /><br />', $msg);
	$msg = '<p>'.$msg.'</p>';
	$this->replace('content', $msg);
}

public function getOptions ($type, $no=null) {
	$options ="";
	switch($type)
	{
		case 'day':
			for ($i=1; $i<=31; $i++)
			{
				$k = $i < 10 ? "0".$i : $i;

				if(is_numeric($no) && $no == $i) { $selected = ' selected'; } else { $selected = ''; }
				$options .= "<option value='".$k."' ".$selected.">$i</option>";
			}
		break;
		case 'month':
			for ($i=1; $i<=12; $i++)
			{
				$k = $i < 10 ? "0".$i : $i;

				if(is_numeric($no) && $no == $i) { $selected = ' selected'; } else { $selected = ''; }
				$options .= "<option value='".$k."' ".$selected.">$i</option>";
			}
		break;

		case 'year':
			$year = date('Y', time());

			for ($i=$year; $i>=2009; $i--)
			{
			
				if(is_numeric($no) && $no == $i) { $selected = ' selected'; } else { $selected = ''; }
				$options .= "<option value='".$i."' ".$selected.">".$i."</option>";
			}
		break;

		case 'abc':
			$abc = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
			
			$options .=  "<li><a href='controller.php?cm=crmIntern&amp;event=listShowAllPersons&amp;letter=1'>0-9</a></li>";
			foreach($abc as $letter)
			{
				$options .= " ";
				$options .=  "<li><a href='controller.php?cm=crmIntern&amp;event=listShowAllPersons&amp;letter=".$letter."'>".strtoupper($letter)."</a></li>";
			}
		break;
	}
	return $options;
}

public function getErr() {
	return $this->err;
}

public function setErr($err) {
	$this->err = $err;
}

public function setPageId($id) {
	$this->pageId = $id;
}

protected function replaceParts() {

	$this->replace('headmeta', project::getMetaCode());

	// replace forgotten parts to empty string
	$this->code = preg_replace('/\$\{(.*)\}/sU', '', $this->code);
}


// -----------------------------------------------------------------------------
public function setDataArray($array) {
	$this->dataArray = $array;
}

public function getDataArray() {
	return $this->dataArray;
}

public function addToDataArray($array) {
	if(!is_array($this->dataArray)) {
		$this->dataArray = array();
	}
	$this->dataArray = array_merge($array, $this->dataArray);
}

public function getErrorHtml($errors) {
	$ret = '';

	
	if(is_array($errors))
	{
	foreach($errors as $error) {
			$ret .= '<div class="failure"><p>'.
				L::errMsg($error['errclass'], $error['errmsg']).
				'</p></div>';
		}
	}

	return $ret;
}
// -----------------------------------------------------------------------------
public function html($text, $fillWithNbsp=false) {
	if(empty($text) && $fillWithNbsp) return '&nbsp;';
	return nl2br($text, ENT_QUOTES);
}
// -----------------------------------------------------------------------------
// FW ---------------------------------------------------------------------------
public function zebra($z=0) {
	$mod=($z%2==0)?'odd':'even';return $mod;
}

} // end class

?>
