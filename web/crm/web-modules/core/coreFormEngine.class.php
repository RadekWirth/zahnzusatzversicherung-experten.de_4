<?php

/*
	Version 1.1, some fixes
*/

class coreFormEngine {

private $formTagStack;

// -----------------------------------------------------------------------------
/*
	@param: fields

	array(
		array(
			'legend' => 'html fieldset legend',
			'fields' => array(
				array(
				'label' => 'html label tag',
				'name' => 'html name attribute',
				'type' => 'html type attribute',
				'value' => 'hmtl value attribute opt',
				'checked' => 'html checked attribute opt',
				'options' => 'html select options req for select tag',
				** more exotic input attributes **
				)
				** add more fields via arrays **
			)
		)
		** add more fieldsets via arrays **
	)
*/
public function requestForm($fieldsets, $classModule, $event,
	$parameter=array(), $sessionHandler=true) {

	// session form
	if($sessionHandler) {
		// destroy old form session
		unset($_SESSION['formEngine']);
		$formId = stringHelper::genRandomString(6, '111');
		$_SESSION['formEngine'] = array('formId' => $formId,
			'classModule' => $classModule, 'event' => $event,
			'parameter' => $parameter);
		return $this->generateForm($fieldsets, $formId);
	}
	// form without session
	else {
		return $this->generateForm($fieldsets, null, urlHelper::makeCoreUrl(
			$classModule, $event, $parameter));
	}
}

// -----------------------------------------------------------------------------
/*
	has just to be called from
	requestForm()
*/
private function generateForm($fieldsets, $formId, $target=null) {

if(!count($fieldsets) || !count($fieldsets[0]) ||
	!count($fieldsets[0]['fields'])) {

	return array('err' => 1, 'errmsg' => 'no fields given to genereate form',
		'errclass' => get_class($this));
}

$cssId = true;

/* WHY THAT?
if(!empty($target)) {
	$cssId = false;
}
*/

$setTpl = '
<fieldset$p>
<legend>$l</legend>
$i
</fieldset>
';

$this->formTagStack = '';
$formCode='';
$setCounter=0;
$finalSubmit='';
foreach($fieldsets as $fieldset) {
	$setCounter++;
	$fieldCounter=0;
	$setCode='';
	$group=null;
	$numbering = false;

	if(isset($fieldset['group'])) {
		$group = $fieldset['group'];
	}

	if(isset($fieldset['params']) &&
		strpos($fieldset['params'], 'numbering')!==FALSE) {
		$numbering = true;
	}

	if(is_array($fieldset['fields']))
		$fieldCount = count($fieldset['fields']);
	else $fieldCount = 0;

	if(empty($fieldCount)) {
		continue;
	}

	$number=0;
	foreach($fieldset['fields'] as $field) {
		$fieldNumber = null;
		$fieldCounter++;

		if($field['type'] == 'blank') {
			continue;
		}

		if($numbering && $field['type']!='submit' && $field['type']!='image') {
			$number++;
			if($fieldCount > 1) {
			$fieldNumber = $number;
			}
		}

		if(empty($field['name'])) {
		return array('err' => 2, 'errmsg' => 'empty field name at '.
			'fieldset '.$setCounter.', field '.$fieldCounter,
			'errclass' => get_class($this));
		}

		$field['type'] = empty($field['type'])?'text':$field['type'];

		$field['label'] = empty($field['label'])?'** Label Missing **':
			$field['label'];

		$field['value'] = empty($field['value'])?'':$field['value'];

		// get html code and check for valid field return code
		$fieldCode = $this->getHtmlFormFieldCode($field, $group, $cssId,
			$fieldNumber);

		if(empty($fieldCode) &&
			!strstr($field['params'], 'appendToNextEntry')) {

			return array('err' => 3, 'errmsg' => 'form field code empty at '.
				'fieldset '.$setCounter.', field '.$fieldCounter,
				'errclass' => get_class($this));
		}elseif($field['name']=='finalSubmit') {
			$finalSubmit = "\n".$fieldCode;
		} else {
			$setCode .= $fieldCode;
		}
	}

	// add fieldset to return code
	if(isset($fieldset['params']) && strstr($fieldset['params'], 'disableGroups')) {
		$formCode .= $setCode;
	}
	else {
		$parameters='';
		if(!empty($fieldset['id'])) {
			$parameters.=' id="'.$fieldset['id'].'"';
		}
		if(strpos(isset($fieldset['params']) && $fieldset['params'], 'hideSet')!==FALSE) {
			$parameters .= ' style="display:none"';
		}
		$temp = str_replace('$l', $fieldset['legend'], $setTpl);
		$temp = str_replace('$p', $parameters, $temp);
		$formCode .= str_replace('$i', $setCode, $temp);
		$temp = '';
	}
}

// set target
$formTarget = '';
if(!empty($formId)) {
	$formTarget = urlHelper::makeCoreUrl('coreFormEngine', 'receiveData',
		array('formId' => $formId));
}else {
	// set target passed by parameter
	$formTarget = $target;
}

$formTarget = str_replace('&', '&amp;', $formTarget);

$retCode = "\n".'
<form action="'.$formTarget.'" method="post" enctype="multipart/form-data" onsubmit="return submitForm()">
'.$formCode
 .$finalSubmit.'
</form>
';

return $retCode;
}

// -----------------------------------------------------------------------------
/*
	this function has just to be called from
	generateForm()
*/
private function getHtmlFormFieldCode($field, $group=null, $cssId=true,
	$number=null) {

if(!count($field)) return '';

// collect attributes
$attributes = '';
if($cssId && $field['type']!='radio' && $field['type']!='checkbox'
	&& $field['type']!='date') {
	$attributes .= 'id="ID'.$group.ucwords($field['name']).'" ';
}
$selected = null;
$params = null;
$name = null;
$formNameTag=null;

if($field['type']=='text' && empty($field['class'])) {
	$field['class']='text';
}

foreach($field as $key => $value) {
	if($key=='label' || $key=='description') {
		continue;
	}
	elseif($key=='name') {
		if(!empty($group)) {
			$value = $group.'['.$field['name'].']';
		}
		else {
			$value = $field['name'];
		}
		$formNameTag = $value;
		$attributes .= 'name="'.$value.'" ';
	}
	elseif($key=='params') {
		$params = $value;
		continue;
	}
	elseif($key=='size') {
		switch($value) {
		case 'mini':  $size = '15%'; break;
		case 'small':  $size = '30%'; break;
		case 'medium':  $size = '50%'; break;
		case 'medium-big':  $size = '70%'; break;
		case 'big':  $size = '95%'; break;
		default:  $size = '30%'; break;
		}
 		$attributes .= 'style="width:'.$size.'" ';
	}
	elseif($key=='orientation') continue;
	elseif($key=='helpText') continue;
	elseif($key=='helpLabel') continue;
	elseif($key=='entryId') continue;
	elseif($key=='class') continue;
	elseif($key=='defaultCaptions') continue;
 	elseif($field['type']=='date' && $key=='value') continue;
 	elseif($field['type']=='date' && $key=='selected') continue;
	elseif($field['type']=='jshandler') continue;
	elseif($field['type']=='code') continue;
	elseif($field['type']=='date' && $key=='value') continue;
 	elseif($field['type']=='textarea' && $key=='value') continue;
	elseif($field['type']=='textarea' && $key=='type') continue;
	elseif($field['type']=='select' && $key=='options') continue;
	elseif($field['type']=='select' && $key=='type') continue;
	elseif($field['type']=='select' && $key=='value') continue;
	elseif($field['type']=='select' && $key=='selected')
		$field['value'] = $selected = $value;
	elseif($field['type']=='checkbox' && $key=='selected')
		$field['value'] = $selected = $value;
	elseif($field['type']=='checkbox' && $key=='options') continue;
	elseif($field['type']=='radio' && $key=='options') continue;
	elseif($field['type']=='radio' && $key=='selected')
		$field['value'] = $selected = $value;
	elseif($field['type']=='radio' && $key=='value') continue;
	elseif($field['type']=='checkbox' && $key=='value') continue;
	elseif($field['type']=='radio'	&& $key=='name'
		|| $field['type']=='checkbox'	&& $key=='name') {
		//remove attribute "ID...", notice the "=", no ".="
		$attributes = $key.'="'.$value.'" ';
		continue;
	}
	else {
	// default endline
	$attributes .= $key.'="'.$value.'" ';
	}
}

// set default size if emtpy
/*if(!isset($field['size']) &&
        ($field['type']=='text' || $field['type']=='textarea')) {
         $attributes .= 'style="width:50%" ';
}*/

// check fields where input is needed
$cssClasses = isset($field['class'])?$field['class']:"";
if(strpos($params, 'needed')!==FALSE && empty($field['value'])) {
	if(!empty($cssClasses)) {
		$cssClasses .= ' ';
	}
	$cssClasses .= 'fillMe';
}
if(!empty($cssClasses)) {
	$attributes .= 'class="'.$cssClasses.'" ';
}

if(strpos($params, 'needed')!==FALSE && $field['type']=='date') {
	if(empty($field['selected']['D']) || $field['selected']['D']=='00')
		$fillDateD = ' fillMe';
	if(empty($field['selected']['M']) || $field['selected']['M']=='00')
		$fillDateM = ' fillMe';
	if(empty($field['selected']['Y']) || $field['selected']['Y']=='0001')
		$fillDateY = ' fillMe';
}

// assemble form tag
$formTag='';
if($field['type'] == 'jshandler') {
	$jshDefaultText='';
	if(empty($field['value'])) {
		$jshLink  = $jshDefaultText = L::_(25);
	}
	if(isset($field['onclick'])) {
		$jshLink = '<a id="ID'.$field['name'].'LinkText" onclick="'.
			$field['onclick'].'" style="cursor:pointer">'.
			$jshDefaultText.'</a>';
	}

	$formTag = '<input style="display:none" type="hidden" '.$attributes.' />'.
		'<p id="ID'.$field['name'].'Text">'.$jshLink.'</p>';
}
elseif($field['type'] == 'textarea') {
	$formTag = '<textarea rows="10" cols="40" '.$attributes.'>'.
	$field['value'].'</textarea>';
}
elseif($field['type'] == 'select') {
	$formTag = '<select '.$attributes.'>';
	if(is_array($field['options']) && count($field['options'])) {
		foreach($field['options'] as $key => $value) {
			if(is_array($value)) {
				$formTag .= "\n".'<optgroup label="'.$value['groupLabel'].'">';
				foreach($value['items'] as $key2 => $value2) {
					$selectedTag='';
					if($selected===$key2 || $selected===$value2) {
						$selectedTag = 'selected="selected"';
					}
					$formTag .= '<option '.$selectedTag.' value="'.$key2.'">'.
						$value2.'</option>';
				}
				$formTag .= '</optgroup>';
			}
			else {
				if($field['name'] == 'tooth1')
				{
						#print('<pre>1. '.$selected); print(' # 2. '.$key); print(' # 3. '.$value.'</pre>');
				}
				$selectedTag='';
				if($selected==$key || $selected==$value) {
					$selectedTag = 'selected="selected"';
				}
				$formTag .= '<option '.$selectedTag.' value="'.$key.'">'.
					$value.'</option>';
				}
		}
	}
	$formTag .= '</select>';
}
elseif($field['type'] == 'radio') {
	$class='radioItem';
	if(!empty($field['class'])) {
		$class = $field['class'];
	}
	if(count($field['options'])) {
		$i=0;
		foreach($field['options'] as $key => $value) {
		$i++;
		if($selected==$key || $selected==$value) {
			$formTag .= "\n".'<span class="'.$class.'"><input id="ID'.
				$field['name'].$i.'"'.$attributes.
				' checked="checked" value="'.$key.'" />'.$value.'</span>';
		}
		else {
			$formTag .= "\n".'<span class="'.$class.'"><input id="ID'.
				$field['name'].$i.'"'.$attributes.
				' value="'.$key.'" />'.$value.'</span>';
		}
		}
	}
}
elseif($field['type'] == 'checkbox') {
	$class='checkboxItem';
	if(!empty($field['class'])) {
		$class = $field['class'];
	}
	if(count($field['options'])) {
		$i=0;
		foreach($field['options'] as $key => $value) {
		$i++;
		if($selected==$key) {
			$formTag .= "\n".'<span class="'.$class.'"><input id="ID'.
				$field['name'].$i.'"'.$attributes.
				' checked="checked" value="'.$key.'" />'.$value.'</span>';
		}
		else {
			$formTag .= "\n".'<span class="'.$class.'"><input id="ID'.
				$field['name'].$i.'"'.$attributes.
				' value="'.$key.'" />'.$value.'</span>';
		}
		}
	}
}
elseif($field['type'] == 'date') {
	$defaultCaptions['D'] = '--';
	$defaultCaptions['M'] = '--';
	$defaultCaptions['Y'] = '--';
	if(!empty($field['defaultCaptions']['D'])) {
		$defaultCaptions['D'] = $field['defaultCaptions']['D'];
	}
	if(!empty($field['defaultCaptions']['M'])) {
		$defaultCaptions['M'] = $field['defaultCaptions']['M'];
	}
	if(!empty($field['defaultCaptions']['Y'])) {
		$defaultCaptions['Y'] = $field['defaultCaptions']['Y'];
	}
	$optionD = '<option value="0">'.$defaultCaptions['D'].'</option>';
	$optionM = '<option value="0">'.$defaultCaptions['M'].'</option>';
	$optionY = '<option value="0">'.$defaultCaptions['Y'].'</option>';
	$selD=$selDD=$selM=$selMM=$selY=$selYY= null;
	$hideDay=false;
	$hideMonth=false;
	$hideYear=false;
	if(strpos($field['params'], 'hideDay')!==FALSE ||
		strpos($field['options'], 'hideDay')!==FALSE) {
	$hideDay = true;
	}
	if(strpos($field['params'], 'hideMonth')!==FALSE) {
	$hideMonth = true;
	}
	if(strpos($field['params'], 'hideYear')!==FALSE) {
	$hideYear = true;
	}

	if(is_array($field['selected'])) {
		$selD = $field['selected']['D'];
		$selM = $field['selected']['M'];
		$selY = $field['selected']['Y'];
	}
	else {
		$selD = stringHelper::getDfromDate($field['selected']);
		$selM = stringHelper::getMfromDate($field['selected']);
		$selY = stringHelper::getYfromDate($field['selected']);
	}

	$year = date("Y");
	for($i=1;$i<99; $i++) {
	$selDD=$selMM=$selYY=$zero='';
	if($i<10) $zero = '0';
	if($selD==$i) $selDD = ' selected="selected"';
	if($selM==$i) $selMM = ' selected="selected"';
	if($selY==($year-$i+1)) $selYY = ' selected="selected"';
	if($i<32) $optionD .= '<option value="'.$zero.$i.'"'.$selDD.'>'.$zero.$i.'</option>';
	if($i<13) $optionM .= '<option value="'.$zero.$i.'"'.$selMM.'>'.$zero.$i.'</option>';
	$optionY .= '<option value="'.($year-$i+1).'"'.$selYY.'>'.($year-$i+1).'</option>';
	}

	// check for values
	$value = $field['value'];
	if(!empty($value) && is_array($value)) {
		// days
		if(is_array($value['d'])) {
			$optionD='';
			foreach($value['d'] as $key => $val) {
				$zero=($i<10 && is_int($val))?'0':'';
				$sel=($selD==$key || $selD==$val)?' selected="selected"':'';
				$optionD .= '<option'.$sel.'>'.$zero.$val.'</option>';
			}
		}
		if(is_array($value['m'])) {
			$optionM='';
			foreach($value['m'] as $key => $val) {
				$zero=($i<10 && is_int($val))?'0':'';
				$sel=($selM==$key || $selM==$val)?' selected="selected"':'';
				$optionM .= '<option'.$sel.'>'.$zero.$val.'</option>';
			}
		}
		if(is_array($value['y'])) {
			$optionY='';
			foreach($value['y'] as $key => $val) {
				$zero=($i<10 && is_int($val))?'0':'';
				$sel=($selY==$key || $selY==$val)?' selected="selected"':'';
				$optionY .= '<option'.$sel.'>'.$zero.$val.'</option>';
			}
		}
	}


	// check for range
	/*
	$range = $field['range'];
	if(!empty($range) && is_array($range)) {
		// days
		if(!is_array($range['d'])) {
		}
	}
	*/

	$formTag  = '<span class="date">'."\n";

	// set parts for name tag when group is set
	$dtn1 = $dtn2 = '';
	if(!empty($group)) {
		$dtn1 = $group.'[';
		$dtn2 = ']';
	}

	if(!$hideDay) {
	$formTag .=
	'<select id="ID'.$group.ucwords($field['name']).'D" name="'.$dtn1.$field['name'].'D'.
		$dtn2.'" class="dateDay'.
		$fillDateD.'" '.$attributes.'>'.$optionD.'</select>.'."\n";
	}
	if(!$hideMonth) {
	$formTag .=
	'<select id="ID'.$group.ucwords($field['name']).'M" name="'.$dtn1.$field['name'].'M'.
		$dtn2.'" class="dateMonth'.
		$fillDateM.'" '.$attributes.'>'.$optionM.'</select>.'."\n";
	}
	if(!$hideYear) {
	$formTag .=
	'<select id="ID'.$group.ucwords($field['name']).'Y" name="'.$dtn1.$field['name'].'Y'.
		$dtn2.'" class="dateYear'.
		$fillDateY.'" '.$attributes.'>'.$optionY.'</select>'."\n";
	}

	$formTag .= '</span>';
}
elseif($field['type'] == 'code') {
	$formTag = $field['code'];
}
else {
	$formTag = '<input '.$attributes.'/>';
}

// check for help string
$helpCode = null;
if(!empty($field['helpText'])) {
	$helpTextId = $field['name'].'HelpText';
	$helpLabel = (!empty($field['helpLabel'])?$field['helpLabel']:'Hilfe');
	$helpCode = '<div class="help">'.
	'<a class="helpLabel" onclick="javascript:showHide(\''.$helpTextId.'\')"'.
	'>'.$helpLabel.'</a><p id="'.$helpTextId.
	'" class="helpText" style="display:none;">'.
	$field['helpText'].'</p></div>';
}
else if(!empty($field['helpLabel'])) {
	$helpCode = '<div class="help"><p>'.$field['helpLabel'].'</p></div>';
}

// assemble form entry
$entryClass='entry';
if(isset($field['orientation']) && $field['orientation']=='vertical') {
	$entryClass='entry-vertical';
}
elseif(isset($field['orientation']) && $field['orientation']=='horizontal') {
	$entryClass='entry-horizontal';
}
$entryIdCode = '';
if(empty($field['entryId'])) {
	if(!empty($group)) {
		$entryIdCode = ' id="eID'.$group.ucwords($field['name']).'"';
	}else {
		$entryIdCode = ' id="eID'.$field['name'].'"';
	}
}else {
	$entryIdCode = ' id="'.$field['entryId'].'"';
}
$hideEntryCode = '';
if(strpos($params, 'hideEntry')!==FALSE) {
	$hideEntryCode = ' style="display:none"';
}

$numberCode = '';
if($number!=null) {
	$numberCode = '<span class="number">'.$number.'.</span> ';
}


$formEntry = '
<div class="'.$entryClass.'"'.$entryIdCode.$hideEntryCode.'>
	<div class="label">$l</div>
	<div class="input">$i</div>
	'.$helpCode.'
	<div class="entryEnd"></div>
</div>
';

// check for label
if(!isset($field['params']) || strpos($field['params'], 'hideLabel')===FALSE) {
	$label = '<label for="ID'.$field['name'].'">'.$numberCode.
		$field['label'].'</label>';
}

// check for description
if(!empty($field['description'])) {
 	$label .= '<p class="description">'.$field['description'].'</p>';
}

// check if entry will be added to next entry
if(!empty($field['params']) && strstr($field['params'], 'appendToNextEntry')) {
	$this->formTagStack .= $formTag;
}
else {
	// check if formTagStack is filled to append to this entry
	if(!empty($this->formTagStack)) {
	$formTag .= $this->formTagStack;
	$this->formTagStack = '';
	}
	$retCode='';
	if($field['type']=='checkbox') {
		$retCode = str_replace('$l', '', $formEntry);
		$retCode = str_replace('$i', $formTag.' '.(!empty($label)?$label:''), $retCode);
	}else {
		$retCode = str_replace('$l', (!empty($label)?$label:''), $formEntry);
		$retCode = str_replace('$i', $formTag, $retCode);
	}
}

return $retCode;
}
// -----------------------------------------------------------------------------
public static function getReceivedData() {
	$ret = null;
	// get data delivered by formEngine
	if(!empty($_SESSION['formEngine']['receivedData']))
		$ret = $_SESSION['formEngine']['receivedData'];
	// destroy formEngine Session to avoid hanging in one state
	unset($_SESSION['formEngine']);
	return $ret;
}
// -----------------------------------------------------------------------------



} // end class

?>