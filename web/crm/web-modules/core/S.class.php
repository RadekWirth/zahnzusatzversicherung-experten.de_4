<?php

/* Session Class */

class S {

// -----------------------------------------------------------------------------
public static function get($module, $what=null) {
	if(empty($what)) {
	if(isset($module) && isset($_SESSION[$module]))
		return $_SESSION[$module];
	}else {
	if(isset($module) && isset($_SESSION[$module][$what]))
	return $_SESSION[$module][$what];
	}
}
// -----------------------------------------------------------------------------
public static function del($module, $what=null) {
	if(empty($what)) {
	unset($_SESSION[$module]);
	}else {
	unset($_SESSION[$module][$what]);
	}
}
// -----------------------------------------------------------------------------
public static function set($module, $what, $value) {
	#$_SESSION[$module] = array();
	$_SESSION[$module][$what] = $value;
}

// -----------------------------------------------------------------------------
public static function setTemp($what, $value) {
	$_SESSION['temp'][$what] = $value;
}
// -----------------------------------------------------------------------------
public static function getTemp($what) {
	return $_SESSION['temp'][$what];
}
// -----------------------------------------------------------------------------
public static function unsetTemp($what) {
	if(isset($_SESSION['temp'][$what])) {
	unset($_SESSION['temp'][$what]);
	}
}

// -----------------------------------------------------------------------------

} // end class

?>