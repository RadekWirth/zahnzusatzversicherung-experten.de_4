<?php

class crmLoginView extends view {

protected $dataArray;

public function __construct($init=null) {
	parent::__construct($init);
}


public function geth1($text) {
	return "\n".'<h1>'.$text.'</h1>';
}

public function geth2($text) {
	return "\n".'<h2>'.$text.'</h2>';
}

public function getp($text) {
	return "\n".'<p>'.$text.'</p>';
}

public function geta($link, $text) {
	return "\n".'<a href="'.$link.'">'.$text.'</a>';
}

protected function getHeader() {
	return $this->geth1(L::_(190));
}




} // end class

?>