<?php

class crmLoginViewLogin
	extends crmLoginView
{

function __construct($dataArray=null) {
	parent::__construct();
	$this->dataArray = $dataArray;
}

function processData() {
// get data for prefilling fields
//$data = (empty($this->dataArray['data']))?array():$this->dataArray['data'];
$data = $this->dataArray['data'];

$replCode = $this->geth1(L::_(1));

if(isset($this->dataArray['errors']))
	$replCode .= $this->getErrorHtml($this->dataArray['errors']);


// create field sets
$fieldsets =
array(
	array(
		'legend' => L::_(2),
		'fields' => array(
			array(
			'label' => L::_(3),
			'name' => 'project',
			'type' => 'text',
			'size' => 'medium-big',
			'maxlength' => 25,
			'value' => isset($data['project'])?$data['project']:""
			),
			array(
			'label' => L::_(4),
			'name' => 'name',
			'type' => 'text',
			'size' => 'medium-big',
			'maxlength' => 25,
			'value' => isset($data['name'])?$data['name']:""
			),
			array(
			'label' => L::_(7),
			'name' => 'password',
			'autocomplete' => 'current password',
			'type' => 'password',
			'size' => 'medium-big',
			'maxlength' => 50
			),
			array(
			'value' => L::_(1),
			'name' => 'submit',
			'type' => 'submit',
			'class' => 'submit3',
			'params' => 'hideLabel',
			'size' => 'medium'
			)
		)
	)

);

$formEngine = new coreFormEngine();
$replCode .= $formEngine->requestForm($fieldsets, 'crmLogin',
	'login');

// finish
$this->replace('content', $replCode);

}

} // end class

?>