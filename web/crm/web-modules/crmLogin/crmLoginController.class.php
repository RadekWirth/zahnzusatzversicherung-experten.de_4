<?php

class crmLoginController {

// -----------------------------------------------------------------------------
public function __construct() {

}
// -----------------------------------------------------------------------------
public function handleEvent($event) {

	switch($event) {

		case 'login':
			 $ret = $this->login(); break;

		default:
			return array('err' => 3, 'errmsg' => 'event could not be handled',
				'errclass' => 'shopController', 'errevent' => $event);
	}

	return $ret;
}
// -----------------------------------------------------------------------------
private function login() {
	$loginlog = new loginlog();

	// check ip for spam
	if($loginlog->isIpBlocked(getenv('REMOTE_ADDR'))) {
		return array('view' => new crmLoginViewBlocked());
		exit;
	}

	// get data delivered by formEngine
	$login = coreFormEngine::getReceivedData();

	if(!empty($login)) {
		$user = new user();
		$name = $login['name'];

		$checkName = $user->checkName($login['name']);

		if($checkName['err'] == 0) {
			if(!$user->login($login['project'], $login['name'],
				$login['password'])) {
					$checkName['err'] = 1;
			}
		}

		if($checkName['err'] != 0) {
			// login unsuccessful
			// log IP
			$loginlog->logSuspiciousIp(getenv('REMOTE_ADDR'));

			$view = new crmLoginViewLogin(
			array(
				'errors' =>	array(array('errmsg' => 'login failed',
					'errclass' => 'login')),
				'data' => $login
			));
		}
		else {
			// login successful
			return array('forward' => array('classModule' => 'crmIntern',
				'event' => 'home'));
			exit;
		}

	}
	else {
		$view = new crmLoginViewLogin(array('data' => $login));
	}


	return array('view' => $view);
}

// -----------------------------------------------------------------------------
} // end class

?>