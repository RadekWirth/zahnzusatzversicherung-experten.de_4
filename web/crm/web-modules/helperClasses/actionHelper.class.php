<?php

class actionHelper {

// -----------------------------------------------------------------------------
public static function startDownloadDialog($file, $name='') {
	// send file to browser
	if(empty($name)) {
		$name = $file;
	}
	header('Content-Type: application/pdf');
	header('Content-Length: '.filesize($file));
	header('Content-Disposition: attachment; filename="'.$name.'"');
	header('Cache-Control: private, max-age=0, must-revalidate');
	header('Pragma: public');

	$fp = fopen($file,"r") ;
	while(!feof($fp)) {
		$buff = fread($fp,4096);
		print $buff;
	}
}


public static function getAge($yyyymmdd, $ageByYear=false) {
    if(strlen($yyyymmdd)==10) {
       list($year,$month,$day) = explode("-",$yyyymmdd);

	if($ageByYear==true) return date("Y")-$year;


	$year_diff  = date("Y") - $year;
	$month_diff = date("m") - $month;
	$day_diff   = date("d") - $day;
	if (($month_diff < 0) || ($month_diff == 0 && $day_diff < 0)) {
       	$year_diff--;
	}

	return $year_diff;
     }
}

// -----------------------------------------------------------------------------
} // end class

?>