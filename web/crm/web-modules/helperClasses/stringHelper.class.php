<?php

class stringHelper {

public static function cleanStringLatin($name) {
	$ret='';
	$len = strlen($name);
	$name = strtolower($name);
	for($i=0; $i<$len; $i++) {
		$c = $name[$i];
//print $i.". ".$c." - ".ord($c)."<br />";
// 13 10 in Folge ist <br />

		if(
			(ord($c) >= 97 && ord($c) <= 122)  ||	// az
			(ord($c) >= 48 && ord($c) <= 57) 	||	// 09
			(ord($c) == 45) || (ord($c) == 46) ||	// -.
			(ord($c) == 95) || (ord($c) == 64)	||	// _@
			(ord($c) == 228) || (ord($c) == 32) ||	// ä[Space]
			(ord($c) == 246) || (ord($c) == 252) ||	// öü
			#(ord($c) == 10) || (ord($c) == 13) ||	// CR LF

			(ord($c) == 196) || (ord($c) == 214) ||	// ÄÖ
			(ord($c) == 220) || (ord($c) == 223)	// Üß
		) {
		$ret .= $c;
		}
	}
	return $ret;

}

public static function cleanString($name) {
	$ret='';
	$len = strlen($name);
	$name = strtolower($name);
	for($i=0; $i<$len; $i++) {
		$c = $name[$i];
		if(
			(ord($c) >= 97 && ord($c) <= 122) ||	// az
			(ord($c) >= 48 && ord($c) <= 57) ||		// 09
			(ord($c) == 45) || (ord($c) == 46) ||	// -.
			(ord($c) == 95 || (ord($c) == 64))		// _@
		) {
		$ret .= $c;
		}
	}
	return $ret;
}
// -----------------------------------------------------------------------------
public static function veryCleanString($name) {
	$ret='';
	$len = strlen($name);
	$name = strtolower($name);
	for($i=0; $i<$len; $i++) {
		$c = $name[$i];
		if(
			(ord($c) >= 97 && ord($c) <= 122) ||	// az
			(ord($c) >= 48 && ord($c) <= 57) 		// 09
		) {
		$ret .= $c;
		}
	}
	return $ret;
}
// -----------------------------------------------------------------------------
public static function isVeryCleanString($name) {
	$ret='';
	$len = strlen($name);
	$name = strtolower($name);
	for($i=0; $i<$len; $i++) {
		$c = $name[$i];
		if(
			(ord($c) >= 97 && ord($c) <= 122) ||	// az
			(ord($c) >= 48 && ord($c) <= 57) 		// 09
		) {	}
		else {
			return false;
		}
	}
	return true;
}
// -----------------------------------------------------------------------------
// copied AND modified from intern.class.php:
/*
 * generate a random string, eg password
 *
 * $length. lengths of string
 * $format: 100=a-z, 010=A-Z, 001=0-9
 * $excludeChars: bool for passwd with special expludings (eg 0, O, o, 1, l)
 * $counts: set the max occurences for char classes allowed by $format
 *		???=def. no recognition, ?2?=max occurence for A-Z is 2, 999=max value
 *
**/
public static function genRandomString($length, $format='111',
	$excludeChars=false, $readable=false, $counts='???') {

	$ret='';
	$arrCounts = $arrIds = array();
	$excludes = 'Oo01lIJQ';
	$readables = 'euoia';

	if($format[0]=='1') {
		$arrIds[]=0;
		$arrCounts[]=($counts[0]!='?' && $counts[0]>0)?$counts[0]:-1;
	}
	if($format[1]=='1') {
		$arrIds[]=1;
		$arrCounts[]=($counts[1]!='?' && $counts[1]>0)?$counts[1]:-1;
	}
	if($format[2]=='1') {
		$arrIds[]=2;
		$arrCounts[]=($counts[2]!='?' && $counts[2]>0)?$counts[2]:-1;
	}
	$min = 0;
	$max = count($arrIds)-1;

	$i=0;
	while($i < $length) {
		$c='';
		if(count($arrIds)==1) {
		$toSwitch = $arrIds[0];
		}else {
		$arrPos = mt_rand($min, $max);
		$toSwitch = $arrIds[$arrPos];
		}

		switch($toSwitch) {
		// 0, 1xx, a-z
		case 0: {
			if($readable && $i>1) {
				if(!strstr($readables, $ret[$i-1]) &&
					!strstr($readables, $ret[$i-2])) {
				$c = $readables[mt_rand(0, strlen($readables)-1)];
				break;
				}
				else if(!strstr($readables, $ret[$i-1]) ||
					!strstr($readables, $ret[$i-2])) {

					if(mt_rand(0, 4)>0) {
					$c = $readables[mt_rand(0, strlen($readables)-1)];
					break;
					}
				}
			}

			$c = chr(mt_rand(97, 122));
			break;
		}
		// 1, x1x, A-Z
		case 1: $c = chr(mt_rand(65, 90)); break;
		// 2, xx1, 0-9
		case 2: $c = "".mt_rand(0, 9); break;
		}

		if(!$excludeChars || ($excludeChars && !strstr($excludes, $c))) {
			$ret .= $c;
			$i++;

			// check occurence-marker
			if($arrCounts[$arrPos] > -1) {
				// count occurence
				$arrCounts[$arrPos]--;
				// remove char class id if max occurence count is reached
				if($arrCounts[$arrPos] == 0) {
					unset($arrIds[$arrPos]);
					unset($arrCounts[$arrPos]);
					$max--;
					// reset array keys to continous numbers
					$arrIds = array_merge(array(),$arrIds);
					$arrCounts = array_merge(array(),$arrCounts);
					// end while loop if no char class is available
					if(count($arrIds)==0) break;
				}
			}
		}
	}
	return $ret;
}
// -----------------------------------------------------------------------------
public static function toFloat($string) {
	$ret = str_replace(',','.', $string);
	return (float)$ret;
}

public static function makeGermanFloat($string, $digits=2)
{
	$number = str_replace(',', '.', $string);	
	return number_format($number, $digits, ',', ' ');
}
// -----------------------------------------------------------------------------
public static function convertDate($date, $format) {
	if(strpos($date, '.'))
	{
		#de format
		$d = explode('.', $date);

		if($format == 'sql')
			return $d[2].'-'.$d[1].'-'.$d[0];

		if($format == 'en')
			return $d[1].'/'.$d[0].'/'.$d[2];

		if($format == 'unix')
			return mktime(0, 0, 0, $d[1], $d[0], $d[2]);
	}

	elseif(strpos($date, '/'))
	{
		#en format
		$d = explode('/', $date);

		if($format == 'sql')
			return $d[2].'-'.$d[0].'-'.$d[1];

		if($format == 'de')
			return $d[1].'.'.$d[0].'.'.$d[2];

		if($format == 'unix')
			return mktime(0, 0, 0, $d[0], $d[1], $d[2]);
	}

	elseif(strpos($date, '-'))
	{
		#sql format
		$d = explode('-', $date);

		if($format == 'de')
			return $d[2].'.'.$d[1].'.'.$d[0];

		if($format == 'en')
			return $d[1].'/'.$d[2].'/'.$d[0];

		if($format == 'unix')
			return mktime(0, 0, 0, $d[1], $d[2], $d[0]);
	}	

	elseif(strlen($date) == 10)
	{
		#unixtime
		if($format == 'sql')
			return date('Y-m-d', $date);

		if($format == 'en')
			return date('m/d/Y', $date);

		if($format == 'de')
			return date('d.m.Y', $date);
	}
	else {
		#no date format
	}

	
}



public static function makeGermanDate($date) {
	return stringHelper::getDfromDate($date). '.' .
		stringHelper::getMfromDate($date). '.' .
		stringHelper::getYfromDate($date);
}

public static function makeShortDateFromSignUp($date) {
	return substr($date, 8, 2). '.' .
		substr($date, 5, 2). '. ' .
		substr($date, 11, 2). ':' .
		substr($date, 14, 2);
}

public static function makeDateFromSignUp($date) {
	return substr($date, 8, 2). '.'.
			substr($date, 5, 2). '.'.
			substr($date, 0, 4);
}

public static function makeDate($y, $m=0, $d=0) {
	if($d<10 && strlen($d)==1) $d = '0'.$d;
	if($m<10 && strlen($m)==1) $m = '0'.$m;
	return $y.'-'.$m.'-'.$d;
}

public static function getDfromDate($date) {
	if(strlen($date)==10)
	{	return substr($date, -2); }
	else 
	{	return substr($date, 8, 2); }
}

public static function getMfromDate($date) {
	return substr($date, 5, 2);
}

public static function getYfromDate($date) {
	return substr($date, 0, 4);
}

public static function dateToArray($date) {
	return array(
		'D' => stringHelper::getDfromDate($date),
		'M' => stringHelper::getMfromDate($date),
		'Y' => stringHelper::getYfromDate($date));
}

public static function arrayDateToString(&$array, $index) {
	$d=$m='00'; $y='0001';
	if(!isset($array[$index.'Y']) && !isset($array[$index.'M'])
		  && !isset($array[$index.'D'])) {
		return;
	}
	$array[$index] =
		((empty($array[$index.'Y']))?$y:$array[$index.'Y'])
		.'-'.((empty($array[$index.'M']))?$m:$array[$index.'M'])
		.'-'.((empty($array[$index.'D']))?$d:$array[$index.'D']);
	unset($array[$index.'Y']);
	unset($array[$index.'M']);
	unset($array[$index.'D']);
}

public static function findString($string, $needle)
{
	// returns bool
	$pos = strpos($string, $needle);
	return $pos;
}


public static function parseNcalcDate($date, $calcDays=0, $calcMonths=0, $calcYears=0, $convert=false)
{
	/* handles strings, parses as date and calculates
		@return date or time
	*/

	// recognize if we have a readable date
	if(stringHelper::findString($date, '-') == true)
	{
		// readable
		list($year, $month, $day) = explode("-", $date);
		if($convert) {
			$newDate = mktime(0, 0, 0, $month + $calcMonths, $day + $calcDays, $year + $calcYears);			
		} else 
			$newDate = date('Y-m-d', mktime(0, 0, 0, $month + $calcMonths, $day + $calcDays, $year + $calcYears));
	} else {
		// we already have time format!
		if($convert) {
			$newDate = date('Y-m-d', mktime(0, 0, 0, date('m') + $calcMonths, date('d') + $calcDays, date('Y') + $calcYears));
		} else 
			$newDate = mktime(0, 0, 0, date('m') + $calcMonths, date('d') + $calcDays, date('Y') + $calcYears);			
	}
	return $newDate;
}



public static function calcDate($date, $months) {
	$mDate = stringHelper::getMfromDate($date);
	$m = ($mDate + $months) % 12;
	if($m <= 0) {
		$m += 12;
	}

	$y = stringHelper::getYfromDate($date);
	$yCount = ($months - ($months % 12)) / 12;
 	$monthPos = ($months < 0)?$months*(-1):$months;
	$y = $y + $yCount;

	// eventually correct
	if($months < 0) {
		$y -= (($monthPos % 12) >= $mDate)?1:0;
	}else {
		$y += (($monthPos % 12) > (12-$mDate))?1:0;
	}

	$d = stringHelper::getDfromDate($date);
	return stringHelper::makeDate($y, $m, $d);
}

// $birthdate = YYYY-MM-DD
public static function calcAge($birthdate, $targetdate=null, $byYear=false) {
	list($year,$month,$day) = explode("-",$birthdate);

	// per default use the actual date
	$ref = array("Y" => date("Y"), "m" => date("m"), "d" => date("d"));

	// if targetdate, we use the targetdate if later than birthdate -> switch
	if(isset($targetdate)) {
		if($targetdate > mktime(0,0,0, $month, $day, $year))
		{
			$ref = array('Y' => date("Y", $targetdate) , 'm' => date("m", $targetdate) , 'd' => date("d", $targetdate));
		}
	} 
	
	$year_diff  = $ref["Y"] - $year;
	$month_diff = $ref["m"] - $month;
	$day_diff   = $ref["d"] - $day;
	
	// ByYear?
	if($byYear)
		return $year_diff;

	
	if($month_diff < 0) {
		$year_diff--;
	}
	elseif($day_diff < 0 && $month_diff == 0) {
		$year_diff--;
	}
	return $year_diff;
}

// $birthdate = YYYY-MM-DD
public static function calcAgeByYear($birthdate, $targetdate=null) {
	return stringHelper::calcAge($birthdate, $targetdate, true);
}

// -----------------------------------------------------------------------------
public static function arrayToSql($array) {
	$ret = '';
	$mucho=false;
	if(count($array))
	foreach($array as $key => $value) {
		if($mucho) $ret .= ', ';
		$ret .= $key."='".addslashes($value)."' ";
		$mucho=true;
	}
	return $ret;
}

/*
** depracated
** please use 'makeSqlWhereMany'
*/

public static function makeSqlWhereOneToMany($one, $values, $glue='OR') {
	$ret = '';
	$mucho=false;
	if(count($values))
	foreach($values as $value) {
		if($mucho) $ret .= $glue.' ';
		$ret .= $one."='".$value."' ";
		$mucho=true;
	}
	return $ret;
}


/*
** New function for 'makeSqlWhereOneToMany'
**
*/

public static function makeSqlWhereMany($one, $values) {
	$ret = '';
	$mucho=false;
	if(count($values))
	foreach($values as $value) {
		if($mucho) $ret .= ', ';
		$ret .= "'".$value."'";
		$mucho=true;
	}
	return $one." IN (".$ret.")";
}


public static function makeSqlWhere($cols, $values, $rule='OR', $like=false) {
	$ret = '';
	$i=0;
	$pattern = "#c='#v'";
	if($like) {
		$pattern = "#c LIKE '%#v%'";
	}

	if(!is_array($cols)) {
		$cols = array($cols);
	}

	foreach($cols as $col) {
		$pat = str_replace('#c', $col, $pattern);
		foreach($values as $value) {
			if($i>0) $ret .= ' '.$rule.' ';
			$ret .= str_replace('#v', $value, $pat);
			$i++;
		}
	}
	return $ret;
}

public static function makeSqlWhereByArray($array, $rule='OR', $like=false,
	$colPrefix='', $skipEntryIfEmpty=false) {

	$ret = '';
	$i=0;
	$pattern = "#c='#v'";
	if($like) {
		$pattern = "#c LIKE '%#v%'";
	}

	if(!is_array($array)) {
		return;
	}

	foreach($array as $col => $value) {
		if(empty($value)) {
			continue;
		}
		if($i>0) $ret .= ' '.$rule.' ';
		$pat = str_replace('#c', $colPrefix.$col, $pattern);
		$ret .= str_replace('#v', $value, $pat);
		$i++;
	}
	return $ret;
}// -----------------------------------------------------------------------------
public static function makeTextList($caption, $labels, $values) {
	$c = count($labels);
	$ret = "\n".'#===== '.$caption.' =====#';

	if($labels==null && !is_array($values)) {
		$ret .= "\n\n".$values."\n\n";
	}
	else {
		for($i=0; $i<$c; $i++) {
			$ret .= "\n\n".$labels[$i].': '.$values[$i];
		}
		$ret .= "\n\n";
	}
	return $ret;
}
// -----------------------------------------------------------------------------
public static function makeTextListByTextIds($caption, $data) {
	if(!count($data)) {
		return;
	}
	$ret = "\n".'#===== '.$caption.' =====#';

	foreach($data as $key => $value) {
		$ret .= "\n\n".L::_($key).': '.$value;
	}
	$ret .= "\n\n";
	return $ret;
}
// -----------------------------------------------------------------------------
public static function analyzeString($string) {
	$len = strlen($string);
	$numeric = $lower = $upper = $misc = 0;
	for($i=0; $i<$len; $i++) {
		$c = $string[$i];
		if(ord($c) >= 65 && ord($c) <= 90) $upper++;
		elseif(ord($c) >= 97 && ord($c) <= 122) $lower++;
		elseif(ord($c) >= 48 && ord($c) <= 57) $numeric++;
		else $misc++;
	}
	return array('numeric' => $numeric, 'upper' => $upper,
		'lower' => $lower, 'misc' => $misc);
}
// -----------------------------------------------------------------------------
public static function arrayUcFirst($array) {
	// make first char uppercase for each element
	if(!empty($array))
	foreach($array as &$x) {
		$x = ucfirst($x);
	}
	return $array;
}
// -----------------------------------------------------------------------------
public static function resolveTags($text) {
	// lists
	preg_match_all('/\(list(.*)?\)(.*)\(\/list\1\)/sU', $text, $matches);
	for($i=0; $matches[2][$i]; $i++) {
			$list = '';
			$items = explode("\n", $matches[2][$i]);
			if(count($items)) {
			foreach($items as $item) {
					$item = trim($item);
					if(empty($item)) continue;
					$list .= "\n".'<li>'.$item.'</li>';
			}
			if($matches[1][$i] == 'o') {
					$list = "\n".'<ol>'.$list.'</ol>';
			}else {
					$list = "\n".'<ul>'.$list.'</ul>';
			}
			$list = '</p>'.$list."\n".'<p>';
			$text = str_replace($matches[0][$i], $list, $text);
			}
	}
	return $text;
}
// -----------------------------------------------------------------------------
public static function germanAnswer($text) {
	if(is_string($text)) {
		if($text == 'no' || $text == 'yes') {
			$text = $text=='no'?'Nein':'Ja';
		} else if($text == 'adult' || $text == 'kids') {
			$text = $text=='adult'?'Erwachsener':'Kinder';
		}
	} else if(is_array($text)) {
		foreach($text as $key => $value) {
			if($value == 'no' || $value == 'yes') {
				$text[$key] = $value=='no'?'Nein':'Ja';
			}
		}
	} else return;
	
	return $text;
}

// -----------------------------------------------------------------------------
public static function strToHex($string)
{
    $hex='';
    for ($i=0; $i < strlen($string); $i++)
    {
        $hex .= dechex(ord($string[$i]));
    }
    return $hex;
}

// -------------------------------------------------------------------------------


public static function arrayToZoho($array) {
	if (is_array($array))
	{
		$string = "{";
		foreach($array as $n => $value)
		{
			if(strlen($string) >2)
				$string .= ',';
			$string .= '"'.$n.'":"'.stringHelper::str_decode_utf8($value).'"';
		}
		$string .= "}";
		
	}
return $string;
}




// -----------------------------------
// -------- encode to UTF-8 ----------
// -----------------------------------

public static function str_encode_utf8($string) {
  if (mb_detect_encoding($string, 'UTF-8', true) != 'UTF-8') {
    $string = utf8_encode($string);
  }
return $string;
}

// -----------------------------------

// -----------------------------------
// -------- decode from UTF-8 --------
// -----------------------------------

public static function str_decode_utf8($string) {
  if(is_string($string)) {
	if(mb_detect_encoding($string, 'UTF-8', true) == 'UTF-8') {
		$string = mb_convert_encoding($string, "Windows-1252", mb_detect_encoding(stringHelper::germanAnswer($string), "UTF-8, ISO-8859-1, ISO-8859-15", true)); 
	}
  } else if(is_array($string)) {
	foreach($string as $key => $value) {
		$string[$key] = stringHelper::str_decode_utf8($value);
  }}
return $string;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
} // end class


?>
