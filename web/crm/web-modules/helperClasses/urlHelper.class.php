<?php

class urlHelper {

public static function makeCoreURL($classModule, $event, $parameter=array(),
	$ank=null) {
	if(!empty($ank)) {
		$ank = '#'.$ank;
	}
	$paramString = urlHelper::arrayToUrlParameter($parameter);
	return "controller.php?cm=".$classModule."&event=".$event.$paramString.$ank;
}

public static function makeCoreURLAbsolut($classModule, $event, $parameter=array()) {
	$paramString = urlHelper::arrayToUrlParameter($parameter);
	return project::getProjectUrl().
		"controller.php?cm=".$classModule."&event=".$event.$paramString;
}

public static function arrayToUrlParameter($array) {
	$paramString = '';

	if(is_array($array)) {
	
	//if(count($array)) {
	foreach($array as $key => $value) {
	$paramString .= '&'.$key.'='.$value;
	}
	}
	return $paramString;
}

// generates a follow link, eg "return to main page"
public static function makeFollowLink($classModule, $event, $linkText,
	$parameter=array(), $ank=null) {
	$paramString = urlHelper::arrayToUrlParameter($parameter);
	$url = urlHelper::makeCoreURL($classModule, $event);
	$ret .= "\n".'<a href="'.$url.$paramString.$ank.'">'.
		$linkText.'</a></p>';
	return $ret;
}


// generates a link without p tag
public static function makeLink($classModule, $event, $linkText,
	$parameter=array(), $ank=null, $blank=false) {
	$attr = '';
	if($blank) {
		$attr .= 'target="blank" ';
	}
	$paramString = urlHelper::arrayToUrlParameter($parameter);
	$url = urlHelper::makeCoreURL($classModule, $event);
	return '<a '.$attr.'href="'.$url.$paramString.$ank.'">'.$linkText.'</a>';
}

// generates a link without p tag
public static function makeExternLink($target, $linkText, $blank=false) {
	$attr = '';
	if($blank) {
		$attr .= 'target="blank" ';
	}
	return '<a '.$attr.'href="'.$target.'">'.$linkText.'</a>';
}

} // end class

?>