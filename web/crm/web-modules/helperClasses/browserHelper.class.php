<?

class browserHelper { 
    private static function getVersion($userAgent, $match)
    {
	$version = "";
	if (preg_match('/.+(?:'.$match.')[\/: ]([\d.]+)/', $userAgent, $matches)) {
            	$version = $matches[1]; 
             }
	return $version; 

    }

    public static function detect($userAgent=null) { 
       if(!isset($userAgent)) 
		$userAgent = strtolower($_SERVER['HTTP_USER_AGENT']); 

        // Identify the browser. Check Opera and Safari first in case of spoof. Let Google Chrome be identified as Safari. 
        if (preg_match('/edg/', $userAgent)) { 
            $name = 'edge';
            $version = browserHelper::getVersion($userAgent, 'dg'); 
        }
        if (preg_match('/edge/', $userAgent)) { 
            $name = 'edge';
            $version = browserHelper::getVersion($userAgent, 'ge'); 
        }
        elseif (preg_match('/msie|Msie/', $userAgent)) { 
            $name = 'msie'; 
	     $version = browserHelper::getVersion($userAgent, 'ie');
        } 
        if (preg_match('/trident/', $userAgent)) { 
            $name = 'msie';
            $version = browserHelper::getVersion($userAgent, 'nt'); 
        }
        elseif (preg_match('/chrome|Chrome/', $userAgent)) { 
            $name = 'chrome'; 
            $version = browserHelper::getVersion($userAgent, 'me');
        } 
        elseif (preg_match('/safari|Safari/', $userAgent)) { 
            $name = 'safari'; 
	     $version = browserHelper::getVersion($userAgent, 'ri');
        } 
        elseif (preg_match('/opera|Opera/', $userAgent)) { 
            $name = 'opera'; 
	     $version = browserHelper::getVersion($userAgent, 'ra');
        } 
        elseif (preg_match('/firefox|Firefox/', $userAgent) && !preg_match('/compatible/', $userAgent)) { 
            $name = 'firefox'; 
            $version = browserHelper::getVersion($userAgent, 'ox');
        }
        elseif (preg_match('/webkit|Webkit/', $userAgent)) { 
            $name = 'webkit'; 
	     $version = browserHelper::getVersion($userAgent, 'it');
        } 
 
        else { 
            $name = 'unrecognized';
 	     $version = 'unknown';
        } 


        // Running on what platform? 
        if (preg_match('/linux|Linux/', $userAgent)) { 
            $platform = 'linux'; 
        } 
        elseif (preg_match('/macintosh|Macintosh|mac os x|Mac OS X/', $userAgent)) { 
            $platform = 'mac'; 
        } 
        elseif (preg_match('/windows|Windows|win32|Win32|win64|Win64/', $userAgent)) { 
            $platform = 'windows'; 
        } 
        else { 
            $platform = 'unrecognized'; 
        } 

	 // device?
	 $device = 'desktop';
	 $device_detail = 'unknown';
	 if (preg_match('/mobile|Mobile|sm-t5|lenovo tab 2|yoga tablet 2/', $userAgent)) { 
            $device = 'mobile'; 
	 
	     if (preg_match('/ipad|iPad/', $userAgent)) {
		$device_detail = 'iPad';
	     }	 		
	     if (preg_match('/iphone|iPhone/', $userAgent)) {
		$device_detail = 'iPhone';
	     }	 
	     if (preg_match('/linux|Linux/', $userAgent)) {

		if (preg_match('/nokia|Nokia/', $userAgent)) {
			$device_detail = 'Nokia';
		}
		if (preg_match('/h8266|H8266|f5321|F5321/', $userAgent)) {
			$device_detail = 'Sony';
		}
		if (preg_match('/motorola|Motorola/', $userAgent)) {
			$device_detail = 'Motorola';
		}
		if (preg_match('/pixel|Pixel/', $userAgent)) {
			$device_detail = 'Pixel';
		}
		if (preg_match('/pot|POT/', $userAgent)) {
			$device_detail = 'POT';
		}
		if (preg_match('/redmi|Redmi/', $userAgent)) {
			$device_detail = 'Redmi';
		}
		if (preg_match('/Mi A2 Lite/', $userAgent)) {
			$device_detail = 'Xiaomi';
		}
		if (preg_match('/samsung|sm|Samsung|SM|SAMSUNG/', $userAgent)) {
			$device_detail = 'Samsung';
		}
		if (preg_match('/sne-lx|SNE-LX|was-lx|WAS-LX|mar-l|MAR-L|mar-lx1a|MAX-LX1A|ele-l29|ELE-L29|ane-lx1|ANE-LX1|hma-l29|HMA-L29|vog-l29|VOG-L29|dub-lx1|DUB-LX1|fig-lx1|FIG-LX1|clt-l29|CLT-L29|jmm-l22|JMM-L22|stk-lx1|STK-LX1/', $userAgent)) {
			$device_detail = 'Huawei';
		}
		if (preg_match('/lenovo|Lenovo/', $userAgent)) {
			$device_detail = 'Lenovo';
		}
		if (preg_match('/yoga|Yoga/', $userAgent)) {
			$device_detail = 'Yoga';
		}
		if (preg_match('/htc|Htc|HTC/', $userAgent)) {
			$device_detail = 'HTC';
		}

	     }	 		
        } 



        return array( 
            'name'      	   => $name, 
            'version'   	   => $version, 
            'platform' 	   => $platform, 
	     'device'   	   => $device,
	     'device_detail'    => $device_detail,
            'userAgent'	   => $userAgent 
        ); 
    } 
} 
?>