<?php

class emailHelper {

public static function checkBlockedDomains($receipient)
{
	$domain = emailHelper::getDomain($receipient);
	if($domain == 'spamgourmet.com')
		return false;

	return true;
}

public static function checkBlockedEmails($receipient)
{
	if($receipient == 'musteremail77@gmx.de')
		return false;

	return true;
}


public static function getDomain($address) {
	if(!emailHelper::validEmail($address)) return false;

	$emailArray = explode('@', $address);
	if(count($emailArray)!=2) return false;

	$hostArray = explode('.', $emailArray[1]);
	$hostCount = count($hostArray);
	if($hostCount<2) return false;

	$domain = $hostArray[$hostCount-2]. '.' .$hostArray[$hostCount-1];
	return $domain;
}

public static function sendMail($sender, $recipient, $subject, $message, $charset = 'iso-8859-1')
{
// in use : crmIntern::sendMailContractArrived

	if(!emailHelper::checkBlockedDomains($recipient))
		return false;

	// für HTML-E-Mails muss der 'Content-type'-Header gesetzt werden
	$header  = 'MIME-Version: 1.0' . "\r\n";
	$header .= 'Content-type: text/html; charset='. $charset . "\r\n";

	// zusätzliche Header
	//$header .= 'To: '.$recipient. "\r\n";
	$header .= 'From: '.$sender. "\r\n";

	mail($recipient, $subject, $message, $header);
}


public static function sendHTMLMail($recipient, $subject, $message) {
	$from = 'info@zahnzusatzversicherung-experten.de';
	$name = 'System';

	if(!empty($name)) {
		$fromString = '"'.$name.'" <'.$from.'>';
	}
	$headers = "From: $fromString\n" .
			"Reply-To: $from\n".
			"MIME-Version: 1.0\n".
			"Content-Type: text/plain;\n\t charset=\"iso-8859-1\"\n".
			"Content-Transfer-Encoding: 8bit\n".
			"X-Sender: ".$from."\n".
			"X-Mailer: PHP\n".
			"X-Priority: 3\n";
	$parameter = '-f '.$from;
	return mail($recipient, $subject, $message, $headers, $parameter);
}



public static function validEmail($email) {
	$email = strtolower($email);
	if(strlen($email)>255) return false;

	// check chars
	if($email != stringHelper::cleanString($email)) {
	return false;
	}

	// First, we check that there's one @ symbol, and that the lengths are right
	if (!preg_match("/^[^@]{1,64}@[^@]{1,255}$/", $email)) {
		// Email invalid because wrong number of characters in one section,
		// or wrong number of @ symbols.
		return false;
	}

	// Split it into sections to make life easier
	$email_array = explode("@", $email);
	$local_array = explode(".", $email_array[0]);
	for ($i = 0; $i < sizeof($local_array); $i++) {
		$pattern = "@^(([A-Za-z0-9!#$%&#038;'*+\/=?^_`{|}~-][A-Za-z0-9!#$%&#038;'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))@";
		if (!preg_match($pattern, $local_array[$i])) {
		return false;
		}
	}


	// Check if domain is IP. If not, it should be valid domain name
	if (!preg_match("@^\[?[0-9\.]+\]?$@", $email_array[1])) {
		$domain_array = explode(".", $email_array[1]);
		if(sizeof($domain_array) < 2) {
			return false; // Not enough parts to domain
		}
		for ($i = 0; $i < sizeof($domain_array); $i++) {
			if(!preg_match('@^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$@', $domain_array[$i])) {
				return false;
			}
		}
	}
return true;
}

public static function getSslPage($url, $file_name) {

	$path = project::getProjectRoot();

	if(empty($path)===true) {
		errorHandler::throwErrorDb('Variable $path not set!', $idco);
		$path = PATH_TO_LIVE;
	}
	if(substr($path, -1)!='/')
		$path .= '/';

    $ch = curl_init();
    #curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    #curl_setopt($ch, CURLOPT_SSL_VERIFYSTATUS, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 50);

    curl_setopt($ch, CURLOPT_URL, str_replace(' ', '%20', $url));
    curl_setopt($ch, CURLOPT_AUTOREFERER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);

    curl_close($ch);

    #print_r($result); // prints the contents of the collected file before writing..


    // the following lines write the contents to a file in the same directory (provided permissions etc)
    $fh = fopen($path.'temp/'.$file_name, 'wb+');
    fwrite($fh, $result);
    fclose($fh);

}

// -----------------------------------------------------------------------------
public static function sendMailWithFiles($receipient, $subject, $message, $file, $idt, $from=null)
{
	if(!emailHelper::checkBlockedDomains($receipient))
		return false;

	$t = time();

	if(!$idt)
		$idt = S::get('wmOnlineSignup', 'idt');
	
	$path = project::getProjectRoot();

	if(empty($path)===true) {
		errorHandler::throwErrorDb('Variable $path not set!', $idco);
		$path = PATH_TO_LIVE;
	}


	if(is_array($file))
	{
		$file_list[$idt] = $file;
	} else
	{
		$file_list = project::gtf();
		if(empty($file_list[$idt]))
			errorHandler::throwErrorDb('no additional files found for idt??', $idt);
	}
	

	$from = project::getProjectEmail(); 
	$fromString = project::getProjectName();

	$mail = new PHPMailer(true);

	try {
 		$mail->CharSet  =  "utf-8";
        	$mail->Priority = 3;
        	$mail->Encoding = "8bit";
        	//$mail->CharSet = "iso-8859-15";
		$mail->Encoding = "quoted-printable";
		$mail->AddReplyTo($from, $fromString);
		$mail->AddAddress($receipient);
		
		$mail->SetFrom($from, $fromString);
		
		$mail->Subject = $subject;
		$mail->AltBody = 'Bitte verwenden Sie einen eMail-Client, der HTML gestützte Bereiche anzeigen kann!'; // optional - MsgHTML will create an alternate automatically

		$mail->MsgHTML($message);

		if(isset($file) && !is_array($file) && file_exists($file))
		{ 
			$mail->AddAttachment($file);
			errorHandler::throwErrorDb('added file to idt '.$idt, $file);
		} else
		{
			errorHandler::throwErrorDb('no file added. not present or not given. file '.$idt, $file);
		}


		for($i=0;$i<count($file_list[$idt]);$i++)
		{
			if(file_exists($path.$file_list[$idt][$i]))
			{
				$mail->AddAttachment($path.$file_list[$idt][$i]);
				errorHandler::throwErrorDb('added file to idt '.$idt, $path.$file_list[$idt][$i]);
			} elseif(file_exists($file_list[$idt][$i])) {
				$mail->AddAttachment($file_list[$idt][$i]);
				errorHandler::throwErrorDb('added file to idt '.$idt, $file_list[$idt][$i]);
			} elseif(strpos($file_list[$idt][$i], "rest")) {
				errorHandler::throwErrorDb('file from rest - idt '.$idt, $file_list[$idt][$i]);

				$file_name = substr ( $file_list[$idt][$i] , strripos($file_list[$idt][$i] , "/") +1 );

				self::getSslPage( $file_list[$idt][$i], $file_name );
	
				$mail->addAttachment($path.'/temp/'.$file_name);
				errorHandler::throwErrorDb('added file to idt '.$idt, $file_list[$idt][$i]);
			} else {
				errorHandler::throwErrorDb('file not found for idt '.$idt, $file_list[$idt][$i]);
			}
		}

		// TemporÃ¤re Attachments
		// central bis 15.06.2010
		$d = mktime(0,0,0,6,15,2010);

		if($idt == 4 && $t <= $d) {
			$mail->AddAttachment('tl_files/files/Central - Beitragsanpassung zum 1.7.2010.pdf');
		}

		// Anforderung vom 24.05.2010
		if ($receipient != "musteremail77@gmx.de")
		{	
			$mail->send();
			return array(true, 'Nachricht wurde verschickt!');
		}
		else {
			return array(false, 'Nachricht wurde nicht verschickt!');			
		}

	} catch (phpmailerException $e) {
  		return $e->errorMessage(); //Pretty error messages from PHPMailer
		}

}

// -----------------------------------------------------------------------------
// Neue Mailfunktion (30.11.2014)
// -----------------------------------------------------------------------------

public static function sendFileMail($sender, $recipient, $subject, $message, $file, $idt, $contract=true, $charset='utf-8', $recipientBCC=null, $path=null)
{

	if(!emailHelper::checkBlockedDomains($recipient) || !emailHelper::checkBlockedEmails($recipient))
		return false;

	// falls $sender nicht gesetzt
	$from = $sender;
	if(!isset($sender))
		$from = project::getProjectEmail(); 
	$fromString = project::getProjectName();

	$mail = new PHPMailer(true);

	try {
       	$mail->Priority = 3;
       	$mail->Encoding = "7bit";
       	$mail->CharSet = $charset;
		$mail->Encoding = "quoted-printable";
		$mail->AddReplyTo($from, $fromString);

		$mail->AddAddress($recipient);
		if($recipientBCC)
			$mail->AddBCC($recipientBCC);
		
		$mail->SetFrom($from, $fromString);
		
		$mail->Subject = $subject;
		$mail->AltBody = 'Bitte verwenden Sie einen eMail-Client, der HTML Bereiche anzeigen kann!'; // optional - MsgHTML will create an alternate automatically

		$mail->MsgHTML($message);

		if(isset($file)) $mail->AddAttachment($file);

		// hole Dateien zur Versicherung, wenn $contract == true
		if($contract)
		{	
			$file_list = project::gtf();
			
			for($i=0;$i<count($file_list[$idt]);$i++)
			{
				if($path)
				{
					$mail->AddAttachment($path.$file_list[$idt][$i]);					
				} else $mail->AddAttachment($file_list[$idt][$i]);
			}
		}
		$mail->send();

	} catch (phpmailerException $e) {
  		echo $e->errorMessage(); //Pretty error messages from PHPMailer
		}

}

// -----------------------------------------------------------------------------

} // end class

?>
