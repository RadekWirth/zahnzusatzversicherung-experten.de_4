<?
/*
** IO Helper Class
**
**
**
*/

class ioHelper
{
private $path;
private $separator;
private $filename;
private $fileHandle;

	public function __construct() {
		$this->filename = 'leistungen.csv';
		$this->separator = ';';
		$this->path = $_SERVER['DOCUMENT_ROOT'].'/crm/';
	}

// ---------------------------------------------------------------
	private function setFile($file) {
		$this->filename = $file;
	}

// ---------------------------------------------------------------
	public function getData() {
		if(!isset($this->fileHandle)) $this->getFileHandle();

		/*
		** First line of the file 
		**
		** All entries with a number (idt) will be imported
		** The first two entries (columns) are formating and 
		** text data.
		**
		** status {active, inactive, new}
		*/
		$firstLine = $this->readLine();
	// fix
		$firstLine = $firstLine.'##';
	// end fix

		$ary = explode($this->separator, $firstLine);
				
		foreach($ary as $line=>$idt) {
			$pos = strpos($idt, '##');
			if(!empty($idt) && $pos===false) {
				$lines[$line]['idt'] = $idt;
			} else { // no data, nothing more to do 
				}
		}

		// skip 2nd line - names of insurance not needed
		$this->readLine();

		while(!feof($this->fileHandle)) {
			$rest = $this->readLine();
			$ary = explode($this->separator, $rest);
		
			
			// end import of data if document ends	
			if($ary[0]=='') break; 

			$data[] = $ary;
		}

		// Set up Array
		for($i=0;$i<count($data);$i++) {
			foreach($data[$i] as $c=>$d) {
				if(isset($lines[$c])) {
					$lines[$c][$i] = $d;
				}
			}
		}

		// nearly done - let's save data into database!
		$wzm = new wzm();

		/*
		** if there should be any status = new data
		** set it to inactive first
		*/
		$wzm->updateStatus(array('status'=>'inactive'), 'new');


		// lines 0 and 1 are desc - let's start with them
		$sqlary = '';
		foreach($lines[0] as $id=>$data) {
			if($id>0) {
				$sqlary = array('ordr'=>$id, 'type'=>htmlentities($data), 'idt'=>0, 'text'=>htmlentities($lines[1][$id]), 'status'=>'new');
				$retId = $wzm->insertData('details', $sqlary);
				if(!isset($retId)) { print "error in sql"; break; }
			}
			
		}


		
		for($i=2;$i<count($lines);$i++) {
			$data = $lines[$i];
			$data['type'] = $data[0]=='kurz'?'short':'long';
			$data[0] = '';

			for($p=1;$p<count($data)-2;$p++) {
				$cleantext = htmlentities($data[$p]);
				if(substr($cleantext, 0, 6) == '&quot;') $cleantext = substr($cleantext, 6);
				if(substr($cleantext, -6) == '&quot;') $cleantext = substr($cleantext, 0, strlen($cleantext)-6);

				$sqlary = array('ordr'=>$p, 'type'=>htmlentities($data['type']), 'idt'=>intval($data['idt']), 'text'=>$cleantext, 'status'=>'new');
//print "<pre>".$cleantext."</pre>";
				$retId = $wzm->insertData('details', $sqlary);
				if(!isset($retId)) { print "error in sql"; break; }
			}
		}
				

		// everything went fine - update the active data
		$wzm->updateStatus(array('status'=>'inactive'), 'active');
		$wzm->updateStatus(array('status'=>'active'), 'new');
}

// ---------------------------------------------------------------
	private function getFileHandle () {
		$this->fileHandle = fopen($this->path.$this->filename, r);
	}

// ---------------------------------------------------------------
	private function readLine() {
		return fgets($this->fileHandle);
	}

// ---------------------------------------------------------------
}