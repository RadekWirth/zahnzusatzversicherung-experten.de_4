<?php

class viewHelper {

// -----------------------------------------------------------------------------
// generates a easy table for viewing purposes
public static function genEasyTableWithTextids($caption, $data,
	$hlevel=2, $colClass="", $skipEntryIfEmpty=false) {

	if(!empty($caption)) {
		$cCaption = "\n".'<h'.$hlevel.'>'.$caption.'</h'.$hlevel.'>';
	}

	$tRow = "\n".'
	<div class="row">
		<div class="col1'.$colClass.'">$l</div>
		<div class="col2'.$colClass.'">$r</div>
		<div class="rowEnd"></div>
	</div>';

	$cBody = '';

	$empties = 0;
	if(!empty($data)) {
	foreach($data as $key => $value) {
		if($skipEntryIfEmpty && empty($value)) {
			$empties++;
			continue;
		}
		$left = L::_($key);
		$temp = str_replace('$l', $left, $tRow);
		$temp = str_replace('$r', $value, $temp);
		$cBody .= $temp;
	}}

	if(empty($data)) {
		return "\n<!-- table is empty, i hid it. The viewHelper -->\n";
	}
	else {
		return "\n\n".'<div class="table">'.$cCaption.$cBody.'</div>';
	}
}
// -----------------------------------------------------------------------------
// generates a easy table for viewing purposes
public static function genEasyTable($caption, $dataLeft, $dataRight,
	$hlevel=2, $colClass="") {

	if(!empty($caption)) {
		$cCaption = "\n".'<h'.$hlevel.'>'.$caption.'</h'.$hlevel.'>';
	}

	$tRow = "\n".'
	<div class="row">
		<div class="col1'.$colClass.'">$l</div>
		<div class="col2'.$colClass.'">$r</div>
		<div class="rowEnd"></div>
	</div>';

	$cBody = '';

	$c = count($dataLeft);
	for($i=0; $i<$c; $i++) {
		$temp = str_replace('$l', $dataLeft[$i], $tRow);
		$temp = str_replace('$r', $dataRight[$i], $temp);
		$cBody .= $temp;
	}

	return "\n\n".'<div class="table">'.$cCaption.$cBody.'</div>';
}
// -----------------------------------------------------------------------------
// generates a table to view with the entries in array
public static function generateTable($caption, $entries, $actions, $actionCM,
	$hlevel=2, $colClass="") {

	if(!empty($caption)) {
		$cCaption = "\n".'<h'.$hlevel.'>'.$caption.'</h'.$hlevel.'>';
	}
	$tRow = "\n".'
	<div class="row">
		<div class="col1'.$colClass.'">$e</div>
		<div class="col2'.$colClass.'">$a</div>
		<div class="rowEnd"></div>
	</div>';

	$cBody = '';

	foreach($entries as $enKey => $enValue) {
		$cActions = '';
		foreach($actions as $acKey => $acValue) {
			$url = urlHelper::makeCoreURL($actionCM, $acKey);
			if(empty($acValue)) {
				$acValue = $enKey;
			}
			$cActions .= '<a href="'.$url.'&entry='.$enKey.'">'.
				$acValue.'</a> ';
		}

		$temp = str_replace('$e', $enValue, $tRow);
		$temp = str_replace('$a', $cActions, $temp);
		$cBody .= $temp;
	}

	return "\n\n".'<div class="table">'.$cCaption.$cBody.'</div>';
}
// -----------------------------------------------------------------------------

// generates a table to view with the entries in array
// this function has extended action management
/* parameter $actions; is $action['ids'][$enKey] set and true,
	this action will be apended, with the value in $action['ids'][$enKey]

	$actions = array(
		'ids' => array($entryId => $actionId),
		'cm' => ..., (class-module to use)
		'event => ..., (event to use)
		'caption' => ... (caption string)
	);
*/
public static function generateTableExtended($caption, $entries, $actions,
	$hlevel=2, $colClass="", $parameter=array()) {

if(!empty($caption)) {
	$cCaption = "\n".'<h'.$hlevel.'>'.$caption.'</h'.$hlevel.'>';
}
$tRow = "\n".'
<div class="row">
	<div class="col1'.$colClass.'">$e</div>
	<div class="col2'.$colClass.'">$a</div>
	<div class="rowEnd"></div>
</div>';

$cBody = '';
$paramString = '';

if(count($parameter)) {
	$paramString = urlHelper::arrayToUrlParameter($parameter);
}

foreach($entries as $enKey => $enValue) {
	$cActions = '';
	foreach($actions as $action) {
		if($action['ids'] == -1 || !empty($action['ids'][$enKey])) {
			$actionId = ($action['ids'] == -1)? $enKey : $action['ids'][$enKey];
			$url = urlHelper::makeCoreURL($action['cm'], $action['event']);
			$cActions .= '<a href="'.$url.'&amp;entryId='.$enKey.
			'&amp;actionId='.$actionId.$action['ank'].$paramString.'">'.
			$action['caption'].'</a> ';
		}
	}

	$temp = str_replace('$e', $enValue, $tRow);
	$temp = str_replace('$a', $cActions, $temp);
	$cBody .= $temp;
}

return "\n\n".'<div class="table">'.$cCaption.$cBody.'</div>';
}
// -----------------------------------------------------------------------------

public static function generateTableExtendedPlus($caption, $entries, $insurance, $address, $actions,
	$hlevel=2, $colClass="", $parameter=array()) {

if(!empty($caption)) {
	$cCaption = "\n".'<h'.$hlevel.'>'.$caption.'</h'.$hlevel.'>';
}
$tRow = "\n".'
<div class="row">
		<div class="col1'.$colClass.'">$e</div>
		<div class="col2'.$colClass.'">$a</div>
		<div class="col3'.$colClass.'">$i</div>
	<div class="rowEnd"></div>
		<div class="col4'.$colClass.'">$ad</div>
	<div class="rowEnd"></div>
</div>';

$cBody = '';
$paramString = '';

if(count($parameter)) {
	$paramString = urlHelper::arrayToUrlParameter($parameter);
}

foreach($entries as $enKey => $enValue) {
	$cActions = '';
	foreach($actions as $action) {
		if($action['ids'] == -1 || !empty($action['ids'][$enKey])) {
			$actionId = ($action['ids'] == -1)? $enKey : $action['ids'][$enKey];
			$url = urlHelper::makeCoreURL($action['cm'], $action['event']);
			$cActions .= '<a href="'.$url.'&amp;entryId='.$enKey.
			'&amp;actionId='
			.$actionId
			.(isset($action['ank']) ? $action['ank'] : "")
			.$paramString.'">'.
			$action['caption'].'</a> ';
		}
	$addressData = $address[$enKey]['street'].', '.$address[$enKey]['postcode'].' '.$address[$enKey]['city'];
	$insuranceData = '<span class="gkv">'.$insurance[$enKey].'</span>';
	}

	$temp = str_replace('$e', $enValue, $tRow);
	$temp = str_replace('$ad', $addressData, $temp);
	$temp = str_replace('$a', $cActions, $temp);
	$temp = str_replace('$i', $insuranceData, $temp);
	$cBody .= $temp;
}

return "\n\n".'<div class="table">'.$cCaption.$cBody.'</div>';
}
// -----------------------------------------------------------------------------
// interface
public static function makeFollowLink($classModule, $event, $linkText) {
	return urlHelper::makeFollowLink($classModule, $event, $linkText);
}
// -----------------------------------------------------------------------------
/* replace email-adresses in text to images */
public static function emailsToHtmlImages($text) {
	global $cpath, $spath;
	$ext = '.png';
	$hashImagePath = 'pictures/hashes/';
	$hashImagePathCms = 'pictures/hashes/';
	$commonPath = '../_common/';

	if(!file_exists($hashImagePathCms)) {
	mkdir($hashImagePathCms, 0755);
	}

	// load font
	$font = imageloadfont($commonPath.'fonts/8x13iso.gdf');
	$fontWidth = imagefontwidth($font);
	$fontHeight = imagefontheight($font);

	// search email-adresses
	preg_match_all('/\b[a-zA-Z0-9._%+-]+@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,4}\b/',
		$text, $matches);

	// evaluate search results
	$matches = $matches[0];
	$matches = array_unique($matches);
	if(count($matches)) {
		foreach($matches as $match) {
		$hashFilename = md5($match).$ext;
		// create image if not yet happened
		if(!file_exists($hashImagePathCms.$hashFilename)) {
			$im = imagecreate(strlen($match) * $fontWidth, $fontHeight);
			$bgColor = imagecolorallocate($im, 255, 255, 255);
			$fgColor = imagecolorallocate($im,   0,   0, 0);
			imagestring($im, $font, 0, 0, $match, $fgColor);

			imagepng($im, $hashImagePathCms.$hashFilename);
		}

		$correoLink = viewHelper::correoDecrypt('mailto:'.$match);
		$target = '<a class="hashLink" href="javascript:correoLink(\''.
			$correoLink.'\')">'.
			'<img src="'.$hashImagePath.$hashFilename.
			'" align="middle" class="hashimage" /></a>';
		$text = str_replace($match, $target, $text);
		}
	}
	return $text;
}

public static function correoDecrypt($string) {
	$len = strlen($string);
	for($i=0; $i<$len; $i++) {
		$c = ord($string[$i])-5;
		if($c == 92) $c = 35;
		$string[$i] = chr($c);
	}
	return $string;
}

public static function getAllianzTooth($int) {
	$ary = array('2' => '0 - 2', '5' => '3 - 5', '6' => '6', '7' => '7', '8' => '8', '9' => '9 und mehr');	

	return $ary[$int];
}
// -----------------------------------------------------------------------------
} // end class

?>