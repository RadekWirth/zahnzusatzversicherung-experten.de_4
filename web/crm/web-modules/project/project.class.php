<?php

class project {

private static function getDbData() {
	return array(
		/* 'server' => 'dedi490.your-server.de', */
		'server' => 'localhost',
		'db' => 'zzv_2_11_17',
		'user' => 'phbfbd_5',
		'password' => 'C9yLreQPmgPDTq74'
	);
}

public static function getDb() {
	$dbData = self::getDbData();
	$db = new database('crm', $dbData['server'], $dbData['db'],
		$dbData['user'], $dbData['password']);
	return $db;
}

public static function getReminderDays() {
	return 7;
}

public static function getProjectRoot() {
	return $_SERVER['DOCUMENT_ROOT'];
}
public static function getProjectFolder() {
	return "https://".$_SERVER['HTTP_HOST'].'/';
}

public static function getProjectUrl() {
	return 'https://www.zahnzusatzversicherung-experten.de';
}


public static function getProjectEmail() {
	return 'angebot@zahnzusatzversicherung-experten.de';
}

public static function getProjectName() {
	return 'Zahnzusatzversicherung-Experten';
}

public static function getTarifSwitch() {
	// hier wird festgelegt, ab welchem Datum das Alter angepasst werden muss.
	$dat["M"] = 12;
	$dat["D"] = 11;

	return $dat;
}


public static function getMetaCode() {
return '
<title>ZZV-Experten CRM</title>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta name="robots" content="INDEX,FOLLOW" />
<link href="css/style.css" type="text/css" media="screen,projection" rel="stylesheet" />
';
}


// get tariff array
public static function gta() {
	$wzm = new wzm();
	$activeTariffs = $wzm->getActiveTariffs();

	foreach($activeTariffs as $no => $row) 
	{
		$ids[$row['tariffShortName']] = $row['idt'];
	}
	return $ids;
}

// get tariff array full name
public static function gtaf() {
	$wzm = new wzm();
	$activeTariffs = $wzm->getActiveTariffs();

	foreach($activeTariffs as $no => $row) 
	{
		$ids[$row['name']] = $row['idt'];
	}
	return $ids;
}

// get InsuranceCompany
public static function gtic($idt) {
	$wzm = new wzm();
	$insurance = $wzm->getInsuranceByTariffId($idt);

	return array($idt => $insurance['ident']);
}



public static function gqa($idt) {
// Fragen, die bei den Landing Pages abgefragt werden.

	/* keine Fragen mehr bei LPs */
	$ids = array();
	return $ids[$idt];
}

// get tariff files
public static function gtf() {
	$path = project::getProjectRoot();
	if(substr($path, -1) != '/')
		$path .= '/';

	$path = $path.'tl_files/contract/';
	$files[1] = array($path.'CSS - ZE-Premium + ZGP - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'CSS - ZE-Premium + ZGP - Offizielle Verbraucherinformationen & Tarifbedingungen.pdf', $path.'CSS - Werbeprospekt.pdf', $path.'CSS - Hinweise zur Angabe von fehlenden Zaehnen.pdf');
	$files[2] = array($path.'Barmenia - ZGU+ - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'Barmenia - Offizielle Verbraucherinformationen & Tarifbedingungen.pdf');
	$files[3] = array($path.'ARAG - Offizielle Verbraucherinformationen & Tarifbedingungen.pdf', $path.'ARAG - Z90 Bonus - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[5] = array($path.'Universa - uni-Dent Privat - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'Universa - uni-Dent Privat - Verbraucherinfos & AVB.pdf');
	$files[6] = array($path.'Signal-Iduna - Werbeprospekt und offizielle Tarifbedingungen.pdf', $path.'Signal-Iduna - Komfort-Plus - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[7] = array($path.'Signal-Iduna - Werbeprospekt und offizielle Tarifbedingungen.pdf', $path.'Signal-Iduna - Komfort-Zahn - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[8] = array($path.'Nuernberger - Werbeprospekt.pdf', $path.'Nuernberger - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'Nuernberger - Offizielle Tarifbedingungen und Verbraucherinformationen.pdf');
	$files[11] = array($path.'Hanse Merkur - AVB & Verbraucherinfos.pdf', $path.'Hanse Merkur - EZ+EZT - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'Hanse Merkur - Werbeprospekt.pdf');
	$files[12] = array($path.'Hanse Merkur - AVB & Verbraucherinfos.pdf', $path.'Hanse Merkur - EZ+EZT+EZP - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'Hanse Merkur - Werbeprospekt.pdf');
	$files[13] = array($path.'ERGO-Direkt Premium - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'ERGO Direkt - Premium (PPZ) - Werbeprospekt.pdf');
	$files[14] = array($path.'ERGO-Direkt Premium - Offizielle Verbraucherinfos & Bedingungen.pdf', $path.'ERGO-Direkt Premium - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[15] = array($path.'Janitos - Offizielle Verbraucherinformationen und Tarifbedingungen.pdf', $path.'Janitos - Werbeprospekt.pdf', $path.'Janitos - JA Dental Plus - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[16] = array($path.'Allianz 740 - Offizielle Tarifbedingungen und Verbraucherinformationen.pdf', $path.'Allianz 740 - Werbeprospekt.pdf', $path.'Allianz 740 - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[17] = array($path.'ARAG Z90 Bonus - Offizielle Verbraucherinformationen und Bedingungen.pdf', $path.'ARAG Z90 Bonus - Werbeprospekt.pdf', $path.'ARAG Z90 Bonus - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[18] = array($path.'R+V - ELAN Zahn - AVB & Verbraucherinfos.pdf', $path.'R+V - Werbeprospekt.pdf', $path.'R+V - Z1-U - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[19] = array($path.'R+V - ELAN Zahn - AVB & Verbraucherinfos.pdf', $path.'R+V - Werbeprospekt.pdf', $path.'R+V - Z2-U - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[20] = array($path.'R+V - ELAN Zahn - AVB & Verbraucherinfos.pdf', $path.'R+V - Werbeprospekt.pdf', $path.'R+V - Z1-U + ZV - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[21] = array($path.'R+V - ELAN Zahn - AVB & Verbraucherinfos.pdf', $path.'R+V - Werbeprospekt.pdf', $path.'R+V - Z2-U + ZV - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[23] = array($path.'AXA - Dent-Premium-Komfort-U - Offizielle Verbraucherinfos & Tarifbedingungen.pdf', $path.'AXA - Dent Premium-U - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'AXA - Zahnvorsorge - Werbeprospekt.pdf');
	$files[24] = array($path.'AXA - Dent-Premium-Komfort-U - Offizielle Verbraucherinfos & Tarifbedingungen.pdf', $path.'AXA - Dent Komfort-U - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'AXA - Zahnvorsorge - Werbeprospekt.pdf');
	$files[25] = array($path.'Wuerttembergische - Zahn - AVB & Verbraucherinfos.pdf', $path.'Wuerttembergische V1 - Leistungsprotokoll.pdf', $path.'Wuerttembergische - V1 - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[26] = array($path.'Wuerttembergische - Zahn - AVB & Verbraucherinfos.pdf', $path.'Wuerttembergische V2 - Leistungsprotokoll.pdf', $path.'Wuerttembergische - V2 - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[27] = array($path.'Wuerttembergische - Zahn - AVB & Verbraucherinfos.pdf', $path.'Wuerttembergische V3 - Leistungsprotokoll.pdf', $path.'Wuerttembergische - V3 - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[28] = array($path.'Wuerttembergische - Zahn - AVB & Verbraucherinfos.pdf', $path.'Wuerttembergische Z1 - Leistungsprotokoll.pdf', $path.'Wuerttembergische - Z1 - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[29] = array($path.'Wuerttembergische - Zahn - AVB & Verbraucherinfos.pdf', $path.'Wuerttembergische Z2 - Leistungsprotokoll.pdf', $path.'Wuerttembergische - Z2 - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[30] = array($path.'Wuerttembergische - Zahn - AVB & Verbraucherinfos.pdf', $path.'Wuerttembergische Z3 - Leistungsprotokoll.pdf', $path.'Wuerttembergische - Z3 - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[31] = array($path.'ERGO Direkt - Zahnersatz Sofort (ZEZ) - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'ERGO-Direkt.ZEZ.ZE-Sofort.AVB.2013.pdf');
	$files[32] = array($path.'Signal-Iduna - Kompakt-Serie - AVB & Verbraucherinfos.pdf', $path.'Signal-Iduna - Kompakt-Start - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[33] = array($path.'Signal-Iduna - Kompakt-Serie - AVB & Verbraucherinfos.pdf', $path.'Signal-Iduna - Kompakt-Plus - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[34] = array($path.'Die Bayerische - Offizielle Verbraucherinformationen & Tarifbedingungen.pdf', $path.'Die Bayerische - VIP dental prestige - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'Die Bayerische - Werbeprospekt.pdf');
	$files[35] = array($path.'Die Bayerische - Offizielle Verbraucherinformationen & Tarifbedingungen.pdf', $path.'Die Bayerische - VIP dental plus - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'Die Bayerische - Werbeprospekt.pdf');
	$files[36] = array($path.'DKV - KombiMed Zahn - AVB & Verbraucherinfos.pdf', $path.'DKV - KombiMed Zahn - Werbeprospekt.pdf', $path.'DKV - KDT85 - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[37] = array($path.'DKV - KombiMed Zahn - AVB & Verbraucherinfos.pdf', $path.'DKV - KombiMed Zahn - Werbeprospekt.pdf', $path.'DKV - KDT85 + KDBE - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[38] = array($path.'CSS - IDEAL - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'CSS - IDEAL - AVB & Verbraucherinfos.pdf', $path.'CSS - IDEAL - Werbeprospekt.pdf');
	$files[42] = array($path.'DFV - Zahnschutz Exklusiv - AVB & Verbraucherinfos.pdf', $path.'DFV - Zahnschutz Exklusiv - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'DFV - Zahnschutz - Broschuere.pdf');
	$files[43] = array($path.'Muenchener Verein - DZV - AVB & Verbraucherinfos.pdf', $path.'Muenchener Verein - 571+572+573+574 - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'Muenchener Verein - DZV - Werbeprospekt.pdf');
	$files[45] = array($path.'INTER - Z90 - Z80 - Z70 - ZPro - AVB & Verbraucherinfos.pdf', $path.'INTER - Z90 - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[46] = array($path.'INTER - Z90 - Z80 - Z70 - ZPro - AVB & Verbraucherinfos.pdf', $path.'INTER - Z90+ZPro - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[48] = array($path.'Nuernberger - ZEP80+ZV - AVB & Verbraucherinfos.pdf', $path.'Nuernberger - ZEP80+ZV - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[50] = array($path.'Muenchener Verein - DZV - AVB & Verbraucherinfos.pdf', $path.'Muenchener Verein - 571 - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'Muenchener Verein - DZV - Werbeprospekt.pdf');
	$files[51] = array($path.'Muenchener Verein - DZV - AVB & Verbraucherinfos.pdf', $path.'Muenchener Verein - 570 - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'Muenchener Verein - DZV - Werbeprospekt.pdf');
	$files[75] = array($path.'Muenchener Verein - DZV - AVB & Verbraucherinfos.pdf', $path.'Muenchener Verein - 570+572+573+574 - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'Muenchener Verein - DZV - Werbeprospekt.pdf');
	$files[53] = array($path.'Signal-Iduna - Zahn - AVB & Verbraucherinfos.pdf',$path.'Signal-Iduna - ZahnTOP - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[57] = array($path.'Continentale - CEZP-U - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'Continentale - CEZP-U - AVB & Verbraucherinfos.pdf', $path.'Continentale - CEZP-U - Werbeprospekt.pdf');
	$files[60] = array($path.'Die Bayerische - VIP dental Plus - AVB & Verbraucherinfos.pdf', $path.'Die Bayerische - VIP dental Plus - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[61] = array($path.'CSS - ZE-Premium + ZGP - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'CSS - ZE-Premium + ZGP - Offizielle Verbraucherinformationen & Tarifbedingungen.pdf', $path.'CSS - Werbeprospekt.pdf', $path.'CSS - Hinweise zur Angabe von fehlenden Zaehnen.pdf');
	$files[83] = array($path.'INTER - Z90 - Z80 - Z70 - ZPro - AVB & Verbraucherinfos.pdf', $path.'INTER - Z80+ZPro - Ausfuehrliche Leistungsbeschreibung');
	$files[84] = array($path.'INTER - Z90 - Z80 - Z70 - ZPro - AVB & Verbraucherinfos.pdf', $path.'INTER - Z80 - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[85] = array($path.'INTER - Z90 - Z80 - Z70 - ZPro - AVB & Verbraucherinfos.pdf', $path.'INTER - Z70+ZPro - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[86] = array($path.'INTER - Z90 - Z80 - Z70 - ZPro - AVB & Verbraucherinfos.pdf', $path.'INTER - Z70 - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[205] = array($path.'UKV - ZahnPrivat - AVB & Verbraucherinfos.pdf', $path.'UKV - ZahnPrivat - Werbeprospekt.pdf', $path.'UKV - Zahnprivat Premium - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[206] = array($path.'UKV - ZahnPrivat - AVB & Verbraucherinfos.pdf', $path.'UKV - ZahnPrivat - Werbeprospekt.pdf', $path.'UKV - Zahnprivat Optimal - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[207] = array($path.'UKV - ZahnPrivat - AVB & Verbraucherinfos.pdf', $path.'UKV - ZahnPrivat - Werbeprospekt.pdf', $path.'UKV - Zahnprivat Kompakt - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[213] = array($path.'DKV - KombiMed Zahn - AVB & Verbraucherinfos.pdf', $path.'DKV - KombiMed Zahn - Werbeprospekt.pdf', $path.'DKV - KDTP100 - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[215] = array($path.'DKV - KombiMed Zahn - AVB & Verbraucherinfos.pdf', $path.'DKV - KombiMed Zahn - Werbeprospekt.pdf', $path.'DKV - KDTP100 + KDBE - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[219] = array($path.'Allianz - Dental-Plus - offizielle AVB & Verbraucherinfos.pdf', $path.'Allianz - Dental-Best & Plus - Werbeprospekt.pdf', $path.'Allianz - Dental-Plus - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[220] = array($path.'Allianz - Dental-Best - offizielle AVB & Verbraucherinfos.pdf', $path.'Allianz - Dental-Best & Plus - Werbeprospekt.pdf', $path.'Allianz - Dental-Best - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[227] = array($path.'Hallesche - Dent - AVB & Verbraucherinfos.pdf', $path.'Hallesche - Dent - Werbeprospekt.pdf', $path.'Hallesche - Giga.Dent - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[228] = array($path.'Hallesche - Dent - AVB & Verbraucherinfos.pdf', $path.'Hallesche - Dent - Werbeprospekt.pdf', $path.'Hallesche - Mega.Dent - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[229] = array($path.'Hallesche - Dent - AVB & Verbraucherinfos.pdf', $path.'Hallesche - Dent - Werbeprospekt.pdf');
	$files[238] = array($path.'Die Bayerische - ZAHN - AVB & Verbraucherinfos.pdf', $path.'Die Bayerische - ZAHN - Werbeprospekt.pdf', $path.'Die Bayerische - ZAHN Prestige - Ausfuehrliche Leistungsbeschreibung.pdf');	
	$files[239] = array($path.'Die Bayerische - ZAHN - AVB & Verbraucherinfos.pdf', $path.'Die Bayerische - ZAHN - Werbeprospekt.pdf', $path.'Die Bayerische - ZAHN Komfort - Ausfuehrliche Leistungsbeschreibung.pdf');	
	$files[240] = array($path.'Die Bayerische - ZAHN - AVB & Verbraucherinfos.pdf', $path.'Die Bayerische - ZAHN - Werbeprospekt.pdf', $path.'Die Bayerische - ZAHN Smart - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[255] = array($path.'Hanse Merkur - AVB & Verbraucherinfos.pdf', $path.'Hanse Merkur - EZL - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[256] = array($path.'Hanse Merkur - AVB & Verbraucherinfos.pdf', $path.'Hanse Merkur - EZK - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[258] = array($path.'Gothaer - MediZ Duo - AVB & Verbraucherinfos.pdf', $path.'Gothaer - MediZ Duo - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[260] = array($path.'Nuernberger - Z100-Z90-Z80 - AVB & Verbraucherinfos.pdf', $path.'Nuernberger - Z100 - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[262] = array($path.'Nuernberger - Z100-Z90-Z80 - AVB & Verbraucherinfos.pdf', $path.'Nuernberger - Z80 - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[264] = array($path.'Nuernberger - Z100-Z90-Z80 - AVB & Verbraucherinfos.pdf', $path.'Nuernberger - Z90 - Ausfuehrliche Leistungsbeschreibung.pdf');
	$files[273] = array($path.'Barmenia - MehrZahn100 - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'Barmenia - Mehr Zahn - AVB & Verbraucherinfos.pdf');
	$files[274] = array($path.'Barmenia - MehrZahn90 - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'Barmenia - Mehr Zahn - AVB & Verbraucherinfos.pdf');
	$files[275] = array($path.'Barmenia - MehrZahn80 - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'Barmenia - Mehr Zahn - AVB & Verbraucherinfos.pdf');
	$files[276] = array($path.'Barmenia - MehrZahn100 + ZV - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'Barmenia - Mehr Zahn - AVB & Verbraucherinfos.pdf');
	$files[277] = array($path.'Barmenia - MehrZahn90 + ZV - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'Barmenia - Mehr Zahn - AVB & Verbraucherinfos.pdf');
	$files[278] = array($path.'Barmenia - MehrZahn80 + ZV - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'Barmenia - Mehr Zahn - AVB & Verbraucherinfos.pdf');
	$files[284] = array($path.'SDK - Zahn 100 - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'SDK - Zahn - AVB & Verbraucherinfos.pdf');
	$files[285] = array($path.'SDK - Zahn 90 - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'SDK - Zahn - AVB & Verbraucherinfos.pdf');
	$files[286] = array($path.'SDK - Zahn 70 - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'SDK - Zahn - AVB & Verbraucherinfos.pdf');
	$files[287] = array($path.'Muenchener Verein - ZahnGesund 100 - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'Muenchener Verein - ZahnGesund - AVB & Verbraucherinfos.pdf');
	$files[288] = array($path.'Muenchener Verein - ZahnGesund 85+ - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'Muenchener Verein - ZahnGesund - AVB & Verbraucherinfos.pdf');
	$files[289] = array($path.'Muenchener Verein - ZahnGesund 75+ - Ausfuehrliche Leistungsbeschreibung.pdf', $path.'Muenchener Verein - ZahnGesund - AVB & Verbraucherinfos.pdf');


	$path = project::getProjectRoot();
	if(substr($path, -1) != '/')
		$path .= '/';

	$path .= 'tl_files/files/';

	for ($i=1; $i <= 289; $i++) 
	{
		if (isset($files[$i]) && is_array($files[$i])) {
			$files[$i] = array_merge($files[$i], array($path.'vmexperten.Erstinformation.pdf'));
		} else
		{
			$files[$i] = array($path.'vmexperten.Erstinformation.pdf');
		}
		
	}

	return $files;
}

// Financetestpath
public static function getFinancetestLogoPath() {
	return 'tl_files/bilder/Finanztest-Logos/';
}

// Insurance Logo URIs
public static function getFinancetestLogoFilename($idt) {
	$filename = array(
		/*2 => 'barmenia_zgu_finanztest_siegel_rgb',*/
		11 => 'hm_ez-ezt_finanztest_siegel_rgb',
		12 => 'hm_ez-ezt-ezp_finanztest_siegel_rgb',
		13 => 'ergo_zab-zae-zbb-zbe_finanztest_siegel_rgb',
		20 => 'rv_z1_zv_finanztest_siegel_rgb',
		23 => 'axa_prem_finanztest_siegel_rgb',
		34 => 'die_bayerische_vip_dental_prestige_finanztest_siegel_rgb',
		37 => 'dkv_kdt85_kdbe_finanztest_siegel_rgb',
		#38 => 'css_zahngesundheit-ideal_finanztest_siegel_rgb',
		42 => 'dfv_zahnschutz-exklusiv_finanztest_siegel_rgb',
		43 => 'mv_571-572-573-574_finanztest_siegel_rgb',
		46 => 'inter_z90-zpro_finanztest_siegel_rgb'
	);
	if(!empty($filename[$idt]))
		return $filename[$idt];
}

// Financettest-Rating
public static function getFinancetestRatingAsDecimal($idt, $year = 2014) {
	$rating = array(
		'2014' => array(
			5 => '1,4',
			15 => '1,3',
			17 => '1,4',
			18 => '1,1',
			19 => '2,1',
			20 => '1,1',
			21 => '2,1',
			24 => '1,8',
			34 => '1,3',
			36 => '1,4',
			37 => '1,4',
			38 => '1,4',
			39 => '1,1',
			40 => '1,1',
			44 => '1,0',
			45 => '1,4',
			46 => '1,4'
		)
	);
	return $rating[$year][$idt] ? $rating[$year][$idt] : null;
}

// Get Financetest-Logo Wide URI
public function getFinancetestLogoWideURI($idt, $fileprefix = '.png') {
	return $this->getFinancetestLogoPath() . $this->getFinancetestLogoFilename($idt) . '_wide' . $fileprefix;
}

// Get Financetest-Logo Small URI
public function getFinancetestLogoSmallURI($idt, $fileprefix = '.png') {
	return $this->getFinancetestLogoPath() . $this->getFinancetestLogoFilename($idt) . '_small' . $fileprefix;
}

// get tariff ids
public static function gti($name) {
	$ids = project::gta();
	if(!isset($ids[$name])) return 0;
	return $ids[$name];
}

// get tariff count
public static function gtc() {
	$ids = project::gta();
	return count($ids);
}


// get tariff shortname
public static function gtn($idt) {
		$ids = project::gta();
	foreach($ids as $n => $id) {
		if($id==$idt) return $n;
	}
	return null;
}

// get max idt no
public static function gtmi() {
	$ids = project::gta();

	return max($ids);
}


// get tariff fullname
public static function gtfn($idt) {
		$ids = project::gtaf();
	foreach($ids as $n => $id) {
		if($id==$idt) return $n;
	}
	return null;
}

// get signature
public static function gts($format='text') {
	switch ($format)
	{
		case 'text':
		return '

-------------------------------------------------------------------------------------------

Anschrift und Kontakt:
Versicherungsmakler Experten GmbH
Feursstr. 56 / RGB
82140 Olching

Geschäftsführer: Maximilian Waizmann, Versicherungsfachmann BWV
Eingetragen im Handelsregister des Amtsgericht München unter HRB 196198

Telefon  08142 - 651 39 28
Fax   08142 - 651 39 29
E-Mail:  info@zahnzusatzversicherung-experten.de
Internet:  www.zahnzusatzversicherung-experten.de

Vermittlerstatus
Die Versicherungsmakler Experten GmbH ist Versicherungsmakler gem. Â§34d GewO BRD

IHK-Registrierung
Eingetragen im Vermittlerregister der IHK-München unter der Nummer D-YIRL-NDZ7C-33

Unternehmensbeteiligungen
Das Unternehmen besitzt keine direkten oder indirekten Beteiligungen oder Stimmrechte am Kapital eines Versicherungsunternehmens. Es besitzt auch kein Versicherungsunternehmen direkte oder indirekte Stimmrechte oder Beteiligungen am Kapital dieses Unternehmens.

Vermögensschadenhaftpflichtversicherung
Eine den Bestimmungen der EU-Vermittlerrichtlinie entsprechende Berufshaftpflichtversicherung / Vermögensschadenhaftpflichtversicherung liegt vor (über ERGO).

Steuernummer
143/190/00969

Umsatzsteuer-ID
DE281397704

Zuständige Aufsichtsbehörde
Industrie- und Handelskammer für München und Oberbayern
Balanstr. 55-59
81541 München
Tel:  089 5116 - 0
Fax:  089 5116 - 1306
Mail:  ihkmail@muenchen.ihk.de
web:  www.muenchen.ihk.de

Anschrift der Schlichtungsstellen:
Bundesanstalt für Finanzdienstleistungsaufsicht (BAFin)
Graurheindorfer Straße 108
53117 Bonn
Tel:  0228 4108 - 0
Fax:  0228 4108 - 1550
Mail: poststelle@bafin.de
web:  www.bafin.de

Versicherungsombudsmann e.V.
Postfach 08 06 22
10006 Berlin
Tel:  030 206058 - 0
Fax:  030 206058 - 58
Mail:  info@versicherungsombudsmann.de
web:  www.versicherungsombudsmann.de

Ombudsmann für die private Kranken- und Pflegeversicherung
Postfach 06 02 22
10052 Berlin
Tel:  01802 55 04 44 (6ct pro Anruf aus dem Dt. Festnetz)
web:  www.pkv-ombudsmann.de

';
		break;
	case 'html':
	return '<p>
-------------------------------------------------------------------------------------------
</p>

<p>
Anschrift und Kontakt:<br />
Versicherungsmakler Experten GmbH<br />
Feursstr. 56 / RGB<br />
82140 Olching
</p>
<p>
Geschäftsführer: Maximilian Waizmann, Versicherungsfachmann BWV<br />
Eingetragen im Handelsregister des Amtsgericht München unter HRB 196198
</p>
<p>
Telefon  08142 - 651 39 28<br />
Fax   08142 - 651 39 29<br />
E-Mail:  info@zahnzusatzversicherung-experten.de<br />
Internet:  www.zahnzusatzversicherung-experten.de
</p>
<p>
Vermittlerstatus<br />
Die Versicherungsmakler Experten GmbH ist Versicherungsmakler gem. §34d GewO BRD
</p>
<p>
IHK-Registrierung<br />
Eingetragen im Vermittlerregister der IHK-München unter der Nummer D-YIRL-NDZ7C-33
</p>
<p>
Unternehmensbeteiligungen<br />
Das Unternehmen besitzt keine direkten oder indirekten Beteiligungen oder Stimmrechte am Kapital eines Versicherungsunternehmens. Es besitzt auch kein Versicherungsunternehmen direkte oder indirekte Stimmrechte oder Beteiligungen am Kapital dieses Unternehmens.
</p>
<p>
Vermögensschadenhaftpflichtversicherung<br />
Eine den Bestimmungen der EU-Vermittlerrichtlinie entsprechende Berufshaftpflichtversicherung / Vermögensschadenhaftpflichtversicherung liegt vor (über ERGO).<br />
</p>
<p>
Steuernummer<br />
143/190/00969
</p>
<p>
Umsatzsteuer-ID<br />
DE281397704
</p>
<p>
Zuständige Aufsichtsbehörde<br />
Industrie- und Handelskammer für München und Oberbayern<br />
Balanstr. 55-59<br />
81541 München<br />
Tel:  089 5116 - 0<br />
Fax:  089 5116 - 1306<br />
Mail:  ihkmail@muenchen.ihk.de<br />
web:  www.muenchen.ihk.de
</p>
<p>
Anschrift der Schlichtungsstellen: <br />
Bundesanstalt für Finanzdienstleistungsaufsicht (BAFin)<br />
Graurheindorfer Straße 108<br />
53117 Bonn<br />
Tel:  0228 4108 - 0<br />
Fax:  0228 4108 - 1550<br />
Mail: poststelle@bafin.de<br />
web:  www.bafin.de
</p>
<p>
Versicherungsombudsmann e.V.<br />
Postfach 08 06 22<br />
10006 Berlin<br />
Tel:  030 206058 - 0<br />
Fax:  030 206058 - 58<br />
Mail:  info@versicherungsombudsmann.de<br />
web:  www.versicherungsombudsmann.de
</p>
<p>
Ombudsmann für die private Kranken- und Pflegeversicherung<br />
Postfach 06 02 22<br />
10052 Berlin<br />
Tel:  01802 55 04 44 (6ct pro Anruf aus dem Dt. Festnetz)<br />
Fax: 030 20458931<br />
web: www.pkv-ombudsmann.de
</p>
';
	break;
	case 'antrag':
	return 'Die Versicherungsmakler Experten GmbH ist Versicherungsmakler gem. §34d GewO BRD. Das Unternehmen ist eingetragen im Handelsregister des AG München unter HRB 196198. Die Umsatzsteuer-ID lautet DE281397704. Das Unternehmen besitzt keine direkten oder indirekten Beteiligungen oder Stimmrechte am Kapital eines Versicherungsunternehmens. Es besitzt auch kein Versicherungsunternehmen direkte oder indirekte Stimmrechte oder Beteiligungen am Kapital dieses Unternehmens. Eingetragen im Vermittlerregister der IHK unter der Nummer D-YIRL-NDZ7C-33 (www.vermittlerregister.info, DIHK e.V. Breite Straße 29, 10178 Berlin, Tel: 030-20308-0, Fax: 030-20308-1000). Eine den Bestimmungen der EU Vermittlerrichtlinie entsprechende Berufshaftpflichtversicherung / Vermögensschadenhaftpflichtversicherung liegt vor (über ERGO). Zuständige Aufsichtsbehörde Industrie- und Handelskammer für München und Oberbayern, Balanstr. 55-59, 81541 München, Tel: 089 5116-0, Fax: 089 5116-1306, E-mail: ihkmail@muenchen.ihk.de, web: www.muenchen.ihk.de. Anschrift der Schlichtungsstellen: Bundesanstalt für Finanzdienstleistungsaufsicht (BAFin), Graurheindorfer Straße 108, 53117 Bonn, Tel: 0228 / 4108 - 0, Fax: 0228 / 4108 - 1550, web: www.bafin.de; Versicherungsombudsmann e.V., Postfach 08 06 22, 10006 Berlin, Tel: 030/206058-0, Fax: 030/206058-98, web: www.versicherungsombudsmann.de, Ombudsmann für die private Kranken- und Pflegeversicherung, Postfach 06 02 22, 10052 Berlin , Tel: 0800-2550444, www.pkv-ombudsmann.de';
	break;
	}

}
} // end class

?>
