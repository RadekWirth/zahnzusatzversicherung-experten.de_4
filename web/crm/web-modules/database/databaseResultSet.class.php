<?php


class databaseResultSet {

private $result;


function __construct($result) {
	$this->result = $result;
}

function numRows() {
	return ($this->result)?mysql_num_rows($this->result):0;

}

function fetch() {
	if($this->result) {
		return mysql_fetch_assoc($this->result);
	}
	else {
		return null;
	}
}


} // end class

?>