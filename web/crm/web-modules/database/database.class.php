<?php

class database {

private $db_server;
private $db_name;
private $db_user;
private $db_pw;

private $resource_link;
private $result;
private $query;
private $parent;
private $log_queries;
private $log_queries_PDO;
private $logStarttime;

private $lastInsertedId;
private $affectedRows;


public function __construct($parent="unknown", $dbserver=null, $dbname=null,
	$dbuser=null, $dbpw=null) {

	if(!empty($dbserver) && !empty($dbname) && !empty($dbuser) &&
	!empty($dbpw)) {
	$this->db_server= $dbserver;
	$this->db_name	= $dbname;
	$this->db_user	= $dbuser;
	$this->db_pw 	= $dbpw;
	}
	else {
	global $db;
	$this->db_server= $db["server"];
	$this->db_name	= $db["name"];
	$this->db_user	= $db["user"];
	$this->db_pw 	= $db["pw"];
	}

	$this->parent = $parent;
	$this->resource_link = null;

	$this->log_queries = false;
	$this->log_queries_PDO = false;
}

public function loadAdminConnection() {
	global $db_main;
	$this->db_server = $db_main["server"];
	$this->db_name   = $db_main["name"];
	$this->db_user   = $db_main["user"];
	$this->db_pw     = $db_main["pw"];
}

public function queryPDO($query) {
	$this->logStarttime();
	$this->query = $query;

	if(!$this->resource_link)
		$this->connectPDO();


	$stmt = $this->resource_link->prepare($query);
	
	$stmt->execute();

	if($this->log_queries_PDO) {
		$this->log($query, 'query');
	}

	if(strstr($query, 'INSERT')) {
		$this->lastInsertedId = $this->resource_link->lastInsertId(); 
	}

	if(strstr($query, 'SELECT')) {
		return $stmt;
	} else {
		 // else return affected Rows
		$this->affectedRows = $stmt->rowCount();
	}
}

public function fetchPDO($stmt) {
	return $stmt->fetch(PDO::FETCH_ASSOC);
}

private function connectPDO() {
	$dsn = 'mysql:dbname='.$this->db_name.';host='.$this->db_server.';charset=utf8';

	$user = $this->db_user;
	$password = $this->db_pw;

	$conn = NULL;
	$this->logStarttime = microtime(true);
	try{
		$conn = new PDO($dsn, $user, $password);

		#$conn->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES utf8");
		#$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
		#$conn->setAttribute(PDO::ATTR_PERSISTENT, true);
		#$conn->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
	} catch (PDOException $e) {
		echo 'Connection failed: '.$e->getMessage();
	}

	$this->resource_link = $conn;


	#if(php_sapi_name() == 'cgi-fcgi'){
	#      $query = $this->resource_link->prepare("set session wait_timeout=10000,interactive_timeout=10000,net_read_timeout=10000");
	#      $query->execute();
	#}
}

public function getNumRows($stmt) {
	return $stmt->rowCount();
}

public function getAffectedRows() {
	return $this->affectedRows;
}

public function getLastInsertedId() {
	return $this->lastInsertedId;
}

public function getQuery() {
	return $this->query;
}

private function logStarttime() {
	$this->logStarttime = microtime(true);
}

public function log($query, $method) {
	global $tbl;
	
	if(!$query)
		return false;

	$endtime = microtime(true);

	if(!$this->resource_link)
		$this->connectPDO();

	$query = str_replace("'", "\'", $query);
	$myq = "INSERT INTO log_dbqueries SET ".
		"query    = '".$query."', ".
		"class    = '".$this->parent."', ".
		"method   = '".$method."', ".
		"duration = '".($endtime - $this->logStarttime)."', ".
		"user_id  = '".$_SESSION['user']['login']['uid']."', ".
		"ip       = '".$_SERVER['REMOTE_ADDR']."', ".
		"datetime = '".date("Y-m-d H:i:s")."'";

	if($this->resource_link)
	{
		$stmt = $this->resource_link->prepare($myq);
		$stmt->execute();
	}

	// close connection
	$this->resource_link = null;
	$this->logStarttime = 0.0;
}

/* *************************** */
/* ********* obsolete ******** */
/* *************************** */

private function connect() {
	// check if already connected
	if($this->resource_link) return;

	$this->resource_link =
		mysql_connect($this->db_server, $this->db_user, $this->db_pw);

	if(!$this->resource_link) {
		return array('err' => 1, 'errmsg' => 'database connect failure',
			'errclass' => get_class($this));
	}
	elseif(!mysql_select_db($this->db_name, $this->resource_link)) {
		echo "Db Select failed: ".$this->db_name;
		return array('err' => 2, 'errmsg' => 'database select failure',
			'errclass' => get_class($this));
	}
}

public function query($query) {
	$this->logStarttime();
	$this->query = $query;
	$this->connect();
	$this->result = mysql_query($query, $this->resource_link);

	if(!$this->result) {
		$text = 'Invalid query: '.mysql_error();
		$text .= '<br />SQL: '.$query;
		die($text);
	}

	$this->log($query, 'queryOLD');

	if(0 && strstr($query, 'INSERT')) {
		if(!$this->insertId()) {
		echo "\n<p>Insert Failed: ".$this->query."</p>";
		}
	}
	return new databaseResultSet($this->result);
}

public function insertId() {
	$this->logStarttime();
	$ret = mysql_insert_id($this->resource_link);
	if($this->log_queries) $this->log($this->query, 'insert_id');
	return $ret;
}

public function affectedRows() {
	$this->logStarttime();
	$ret = mysql_affected_rows($this->resource_link);
	if($this->log_queries) $this->log($this->query, 'affected_rows');
	return $ret;
}

}//end class
?>