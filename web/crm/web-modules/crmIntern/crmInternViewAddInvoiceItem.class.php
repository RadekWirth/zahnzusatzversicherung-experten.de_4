<?php

class crmInternViewAddInvoiceItem
	extends myInternView
{

function __construct($dataArray=null) {
	parent::__construct();
	$this->dataArray = $dataArray;
}

function processData() {
// get data for prefilling fields
$data = (empty($this->dataArray['data']))?array():$this->dataArray['data'];


$type = $this->dataArray['type'];
$id = $this->dataArray['id'];

$iid = null;
if($iid = $this->dataArray['iid']) {
}

$replCode = '';
$possibilites = '';

$replCode .= $this->geth1(L::_(124));

// create field sets
$fieldsets =
array(
	array(
		'legend' => L::_(119),
		'group' => 'invoice',
		'fields' => array(
			array(
			'label' => L::_(105),
			'name' => 'caption',
			'type' => 'text',
			'size' => 'big',
			'maxlength' => 255,
			'value' => $data['caption'],
			),
			array(
			'label' => L::_(106),
			'name' => 'description',
			'type' => 'textarea',
			'size' => 'big',
			'value' => $data['description'],
			),

			array(
			'label' => L::_(122),
			'name' => 'price',
			'type' => 'text',
			'size' => 'medium',
			'maxlength' => 255,
			'value' => $data['price'],
			),
			array(
			'label' => L::_(123),
			'name' => 'tax',
			'type' => 'text',
			'size' => 'small',
			'maxlength' => 255,
			'value' => $data['tax'],
			),
		)
	),
	array(
		'legend' => L::_(128),
		'group' => 'period',
		'fields' => array(
			array(
			'label' => L::_(125),
 			'name' => 'period',
			'type' => 'text',
			'size' => 'small',
			'maxlength' => 5,
			'value' => $data['period']['period'],
			),
			array(
			'label' => L::_(120),
			'name' => 'billingFrom',
			'type' => 'date',
			'value' => $data['period']['billingFrom'],
			),
			array(
			'label' => L::_(121),
			'name' => 'billingTill',
			'type' => 'date',
			'value' => $data['period']['billingTill'],
			)
		)
	),
	array(
		'params' => 'disableGroups',
		'fields' => array(
			array(
			'value' => L::_(43),
			'name' => 'submit',
			'type' => 'submit',
			'class' => 'submit3',
			'params' => 'hideLabel'
			)
		)
	)
);

$formEngine = new coreFormEngine();
$replCode .= $formEngine->requestForm($fieldsets, 'crmIntern',
	'addAssignment', array('type' => $type, 'id' => $id));

// finish
$this->replace('content', $replCode);

}

} // end class

?>