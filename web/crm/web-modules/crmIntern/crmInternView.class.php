<?php

class crmInternView extends view {

protected $dataArray;

public function __construct($init=null, $template=null) {
	// load std template
	if(empty($template)) {
	$template = 'templates/indexIntern.tpl.html';
	}
	parent::__construct($init, $template);
}


public function geth1($text) {
	return "\n".'<h1>'.$text.'</h1>';
}

public function getalerth1($text) {
	return '<h1 class="alert">'.$text.'</h1>';
}

public function geth2($text) {
	return "\n".'<h2>'.$text.'</h2>';
}

public function getalerth2($text) {
	return '<h2 class="alert">'.$text.'</h2>';
}

public function geth3($text) {
	return "\n".'<h3>'.$text.'</h3>';
}

public function getalerth3($text) {
	return '<h3 class="alert">'.$text.'</h3>';
}

public function getp($text) {
	return "\n".'<p>'.$text.'</p>';
}



public function geta($link, $text) {
	return "\n".'<a href="'.$link.'">'.$text.'</a>';
}

public function html($string, $fillWithNbsp=false) {
	return parent::html($string, $fillWithNbsp);
}


// -----------------------------------------------------------------------------
// assemble status
public function getStatus($statusLabelName, $currentStatus, $entityId, $pid,
	$statusDates) {

	$statusList = '';
	$label = new label();
	$db = project::getDb();
	$rs = $label->getLabels($statusLabelName, 'lid', $currentStatus);
	$statusOptions = array(0 => L::_(25));
	if(!empty($rs))
	while($row = $db->fetchPDO($rs)) {

		$statusOptions[$row['lid']] = $row['name'];

		if(!empty($statusDates[$row['lid']]) &&
			$statusDates[$row['lid']]!='0000-00-00') {

			$statusOptions[$row['lid']] .= ' ('.
				stringHelper::makeGermanDate($statusDates[$row['lid']]).')';

			$statusList .= '<li>'.stringHelper::makeGermanDate(
				$statusDates[$row['lid']]).' | '.$row['name'];
		}
	}

	$fieldsets =
	array(
		array(
			'params' => 'disableGroups',
			'fields' => array(
				array(
				'name' => 'status',
				'type' => 'select',
				'size' => 'medium',
				'options' => $statusOptions,
				'selected' => $currentStatus,
				'params' => 'hideLabel'
				),

				array(
				'name' => 'date',
				'type' => 'input',
				'size' => 'small',
				'class' => 'submit1',
				'value' => date("d.m.Y"),
				'params' => 'hideLabel'
				),

				array(
				'name' => 'mail',
				'type' => 'radio',
				'selected' => 'no-mail',
				'options' => array('mail' => 'mit Mail', 'no-mail' => 'ohne Mail'),
				'class' => 'radioShortItem',
				'orientation' => 'vertical',
				'description' => 'Mail verschicken...',
				'params' => 'hideLabel'
				),

				array(
				'value' => L::_(43),
				'name' => 'submit',
				'type' => 'submit',
				'class' => 'submit3',
				'params' => 'hideLabel',
				'size' => 'medium'
				)
			)
		)
	);

	$replCode = $this->geth2(L::_(353));

	// add date list
	if(!empty($statusList)) {
		$replCode .= '<ul>'.$statusList.'</ul>';
	}

	$formEngine = new coreFormEngine();
	$replCode .= $formEngine->requestForm($fieldsets, 'crmIntern',
		'setStatus', array('statusLabelName' => $statusLabelName,
		'entityId' => $entityId, 'pid' => $pid), false);
	$replCode = '<div class="notes">'.$replCode.'</div>';

	return $replCode;
}

// -----------------------------------------------------------------------------
public function getImportantNotesMessage($idCol, $id) {
	$crm = new crm();
	$db = project::getDb();

	$notesRs = $crm->getImportantNotesCount($idCol, $id);

	if($notesRs->rowCount()) {
		while($row = $db->fetchPDO($notesRs)) {
			$importantNotesCount = $row['cnt'];
		}
	}
	if($importantNotesCount && $importantNotesCount>=0) {
		if($importantNotesCount == 1) {
			$notesCode = $this->getalerth2($importantNotesCount. ' wichtige Nachricht! Bitte beachten!');	
		} elseif($importantNotesCount > 1) {
			$notesCode = $this->getalerth2($importantNotesCount. ' wichtige Nachrichten! Bitte beachten!');
		}

	} else $notesCode = '';
	return $notesCode;
}

// -----------------------------------------------------------------------------
public function getNotes($idCol, $id, $Lcaption=61) {
	// look for notes
	$crm = new crm();
	$user = new user();
	$db = project::getDb();
	$notesRs = $crm->getNotes($idCol, $id);
	$notesCode = $this->geth2(L::_($Lcaption));
	if($notesRs->rowCount()) {
		while($row = $db->fetchPDO($notesRs)) {
			$cssClass = 'noteEntry';
			if(!empty($row['importance'])) {
				$cssClass = 'noteEntryImportant';
			}
			$notesCode .= '<div class="'.$cssClass.'">'
			.'<a href="'.urlHelper::makeCoreURL('crmIntern', 'delNote',
			array('nid' => $row['nid'], 'type' => $idCol,
			'id' => $row[$idCol])).
			'"><img src="img/del.gif" /></a>'
			.' &nbsp;'.$row['creationDate'].' - '.$user->getNameByUid($row['uid'])
			.nl2br($this->getp($row['text'])).'</div>';
		}
	}

	// create field sets for notes form
	$fieldsets =
	array(
		array(
			'params' => 'disableGroups',
			'fields' => array(
				array(
				'name' => 'text',
				'type' => 'textarea',
				'size' => 'big',
				'params' => 'hideLabel'
				),
				array(
				'name' => 'importance',
				'type' => 'checkbox',
				'options' => array('1' => L::_(108)),
				'class' => 'checkbox',
				'params' => 'hideLabel',
				'selected' => $data['type']
				),
				array(
				'value' => L::_(43),
				'name' => 'submit',
				'type' => 'submit',
				'class' => 'submit3',
				'params' => 'hideLabel',
				'size' => 'medium'
				)

			)
		)
	);

	$formEngine = new coreFormEngine();
	$notesCode .= $formEngine->requestForm($fieldsets, 'crmIntern',
		'addNote', array('type' => $idCol, 'id' => $id), false);
	$notesCode = '<div class="notes">'.$notesCode.'</div>';

	return $notesCode;
}
// -----------------------------------------------------------------------------
public function getAssignments($type, $id) {
	$i=0;
	$crm = new crm();
	$user = new user();
	$label = new label();
	$amrs = $crm->getAssignmentsRs($type, $id);
	$amCode = "\n".$this->geth2(L::_(107));
	$amCode .= "\n".'<div id="assignments">';
	while($row = $amrs->fetch()) {

		$jsDescName = 'amDesc'.$row['amid'];
		$amCode .= "\n".'<div class="entry">';
		$amCode .= "\n".'<a class="showHide" onclick="showHide(\''.$jsDescName.'\');">'
			.'<h4>'.$this->html($row['caption']).'</h4></a>';
		$amCode .= "\n".'<div id="'.$jsDescName.'" style="display: none;"><p>';
		// add edit button
		$amCode .= "\n".'<a href="'.urlHelper::makeCoreURL('crmIntern', 'addAssignment',
			array('amid' => $row['amid'], 'type' => $type,
			'id' => $row[$type])).
			'"><img src="img/icons/edit.gif" /></a>';

		// add del button
		$amCode .= "\n".'<a href="'.urlHelper::makeCoreURL('crmIntern', 'delAssignment',
			array('amid' => $row['amid'], 'type' => $type,
			'id' => $row[$type])).
			'"><img src="img/icons/del.gif" /></a>';

		$amCode .= ' &nbsp;'.$row['creationDate'].' - '.
			$user->getNameByUid($row['uid'].'</p>');

		// remove indexes for view
		unset($row['uid']); unset($row['cid']); unset($row['pid']);
		unset($row['modifyDate']); unset($row['creationDate']);
		unset($row['amid']); unset($row['deleted']);

		$row = $label->resolveLabels($row);
		$amCode .= viewHelper::genEasyTableWithTextids('', $row, 2, '', true);
		$amCode .= "\n".'</div></div>';
		$i++;
	}
	$amCode .= "\n".'</div>';
	if($i==0) {
		return '';
	}
	return $amCode;
}
// -----------------------------------------------------------------------------
public function getPossibleContract ($idco) {

	// $idco könnte auch null sein 
	if(isset($idco)) 
	{

		$c = new contract();
		$contract = $c->get($idco);
		$pid = $contract['pid'];

		$posCon = $c->getCRMInsurances($idco);
		S::set('CRM', 'posCon', $posCon);

		$statusOptions = array(0 => L::_(25));
		$statusOptionsNotAvail = array(0 => L::_(25));

		if(isset($posCon['tariffs_accepted']))
		{
			foreach($posCon['tariffs_accepted'] as $key => $value)
			{
				$statusOptions[$value['idt']] = '['.number_format(($value['bonusbase'] + $value['surplus']), 2, '.', '').' &euro;] ('.$value['idt'].') '.$value['name'];
			}
		}

		if(isset($posCon['tariffs_denied']))
		{
			foreach($posCon['tariffs_denied'] as $key => $value)
			{
				$statusOptionsNotAvail[$value['idt']] = '['.number_format(($value['bonusbase'] + $value['surplus']), 2, '.', '').' &euro;] ('.$value['idt'].') '.$value['name'];
			}
		}

/*
		$c = new contract();
		$contract = $c->get($idco);
		$pid = $contract['pid'];
		$ary = $c->getPossibleContract ($idco, $contract['wishedBegin']);

		$statusOptions = array(0 => L::_(25));
		$statusOptionsNotAvail = array(0 => L::_(25));
		foreach($ary['t'] as $idt=>$key)
		{
			$price = $ary['b'][$idt]['activeBonusBase'] + $ary['bonus'][$idt];
			$price = number_format($price, 2, '.', '');

			if($key == 1 || $key == 2) {
				if(is_numeric($idt) == 1 and $idt>0) {
				$statusOptions[$idt] = '['.$price.' &euro;] ('.$idt.') '.project::gtn($idt);
				}
			}
			else {
				if(is_numeric($idt) == 1 and $idt>0) {
					$statusOptionsNotAvail[$idt] = '['.$price.' &euro;] ('.$idt.') '.project::gtn($idt);			
				}
			}
		}
*/
	}

	$newContractsCode = $this->geth2('Neuer Antrag aus Daten');

	$fieldsets =
	array(
		array(
			'params' => 'disableGroups',
			'fields' => array(
				array(
				'label' => 'Antrag möglich',
				'name' => 'idt',
				'type' => 'select',
				'size' => 'medium',
				'options' => $statusOptions,
				'selected' => $currentStatus
				),
				array(
				'label' => 'Antrag abgelehnt / nicht im Rechner',
				'name' => 'idtNotPossible',
				'type' => 'select',
				'size' => 'medium',
				'options' => $statusOptionsNotAvail,
				'selected' => $currentStatus
				),
				array(
				'name' => 'mail',
				'type' => 'radio',
				'options' => array('1' => 'normal', '2' => 'Alternativangebot', '3' => 'normal (leer)', '4' => 'Alternativangebot (leer)'),
				'class' => 'radioShortItem',
				'orientation' => 'vertical',
				'description' => 'Benachrichtigung',
				'params' => 'hideLabel'
				),
				array(
				'value' => L::_(583),
				'name' => 'submit',
				'type' => 'submit',
				'class' => 'submit3',
				'params' => 'hideLabel',
				'size' => 'medium'
				)

			)
		)
	);

	$formEngine = new coreFormEngine();
	$newContractsCode .= $formEngine->requestForm($fieldsets, 'crmIntern',
		'newContract',array('pid' => $pid, 'idco' => $idco), false);
	$newContractsCode = '<div class="newContracts">'.$newContractsCode.'</div>';

	return $newContractsCode;
}

// -----------------------------------------------------------------------------


public function couple($left, $right) {
	return
	'
	<div class="lr">
	<div class="left">'.$left.'	</div>
	<div class="right">'.$right.'</div>
	<div class="clearBoth"></div>
	</div>
	';
}
// -----------------------------------------------------------------------------
public function getInvoiceMenu() {
	$menuCode .= '
	<ul>
	<li>'.urlHelper::makeLink('crmIntern', 'listShowInvoices',
		L::_(131)).'</li>
	<li>'.urlHelper::makeLink('crmIntern', 'listShowInvoices',
		L::_(155), array('viewStatus' => 'payed')).'</li>
	<li>'.urlHelper::makeLink('crmIntern', 'sendWaitingInvoices',
		L::_(133)).'</li>
	<li>'.urlHelper::makeLink('crmIntern', 'listShowInvoiceItems',
		L::_(132)).'</li>
	</ul>
	';

	return $menuCode;
}
// -----------------------------------------------------------------------------
public function getInvoiceItemMenu() {
	$menuCode .= '
	<ul>
	<li>'.urlHelper::makeLink('crmIntern', 'listShowInvoices',
		L::_(131)).'</li>
	<li>'.urlHelper::makeLink('crmIntern', 'generateInvoices',
		L::_(134)).'</li>
	</ul>
	';

	return $menuCode;
}
// -----------------------------------------------------------------------------
public function getCompanyMenu($cid) {
	$menuCode = $this->geth2(L::_(71));
	$menuCode .= '
	<ul>
	<li>'.urlHelper::makeLink('crmIntern', 'editCompany', L::_(70),
		array('cid' => $cid)).'</li>
	<li>'.urlHelper::makeLink('crmIntern', 'deleteCompany', L::_(171),
		array('cid' => $cid)).'</li>
	<li>'.urlHelper::makeLink('crmIntern', 'addPerson', L::_(44),
		array('cid' => $cid)).'</li>
	<li>'.urlHelper::makeLink('crmIntern', 'addAssignment', L::_(103),
		array('type' => 'cid', 'id' => $cid)).'</li>
	<li>'.urlHelper::makeLink('crmIntern', 'addIntActivity', L::_(163),
		array('type' => 'cid', 'id' => $cid)).'</li>

	</ul>
	';

	return $menuCode;
}

// -----------------------------------------------------------------------------
// single person stripped


public function getInsuranceLinks($pid, $idco=null) {
	$menuCode = $this->geth2(L::_(71));


	$wzm = new wzm();
	$tariffs = $wzm->getActiveCrmTariffs();


	$menuCode .= '<p><strong>'.L::_(343).'</strong>
	<div class="ulfloat">
	<form id="complete" action="controller.php" method="get">
			<input type=hidden name=cm value=crmIntern />
			<input type=hidden name=event value=genContractPdfByImage />
			<input type=hidden name=pid value='.$pid.' />
			<input type=hidden name=idco value='.$idco.' />
			<select name=pdfTemplate>
			';

	foreach($tariffs as $id => $tariff)
	{
			$menuCode .= '<option value='.$tariff['idt'].'>'.$tariff['name'].'</option>';
	}
	$menuCode .= '</select><br /><br />
		<select name=option>
			<option name=contract value=contract>PDF Antrag</option>
			<option name=amde value=amde>Versicherungsdetails bearbeiten</option>
		</select>&nbsp;
			<br /><input type=submit class=submit3 /></form></div>';


	return $menuCode;
}


public function duplicateDetails($returnToPid) {
	$menuCode = $this->geth2(L::_(702));

	$wzm = new wzm();

	$activeTariffs = $wzm->getActiveCrmTariffs();
	$tariffsWithValidDetails = $wzm->getTariffsWithValidDetails();

	foreach($tariffsWithValidDetails as $id => $tariffwd) {
		$tariffs[$tariffwd['idt']] = 1;
	}

	foreach($activeTariffs as $id => $tariff)
	{
		if($tariffs[$tariff['idt']] == 1) {
			$menuCodeSource .= "<option value=".$tariff['idt'].">".$tariff['name']."</option>";	
		} else {
			$menuCodeAim .= "<option value=".$tariff['idt'].">".$tariff['name']."</option>";
		}
	}

	$menuCode .= '<form id="complete" action="controller.php" method="get">
			<input type=hidden name=cm value=crmIntern />
			<input type=hidden name=event value=addTariffDetails />
			<input type=hidden name=returnToPid value='.$returnToPid.' />';


	$menuCode .= '<p><strong>1. Quelle wählen</strong><p>';
	$menuCode .= '<select name=duplicateDetailsSource>';

	$menuCode .= $menuCodeSource;
	$menuCode .= '</select>';

	$menuCode .= '<p><strong>2. Ziel wählen</strong><p>';
	$menuCode .= '<select name=duplicateDetailsAim>';

	$menuCode .= $menuCodeAim;
	$menuCode .= '</select><br /><input type=submit class=submit3 value="3. Details hinzufügen" /></form>';
	
	return $menuCode;
}



// -----------------------------------------------------------------------------
// single person stripped
public function getSinglePersonMenu($pid, $idco=null, $contractData=null) {
	$menuCode = $this->geth2(L::_(71));
	$menuCode .= '
	<div class="ulfloat"><ul>
	<li>'.urlHelper::makeLink('crmIntern', 'editPerson', L::_(109),
		array('pid' => $pid)).'</li>';

	if(!empty($idco)) {
		$menuCode .= '
		<li>'.urlHelper::makeLink('crmIntern', 'editContract', L::_(364),
			array('idco' => $idco)).'</li>
		<li>'.urlHelper::makeLink('crmIntern', 'editContractTeethData', L::_(680),
			array('idco' => $idco)).'</li>';
	}

	if($contractData)
		{
			/* Prüfung, welche Druckoption gewählt sein soll */
			/* zzv.de, zzv.com, mobil? */
			$checked[$contractData['sourcePage']] = 'checked';

			/* 1/2 Seite(n), leer / complete, mit / ohne LB */
			if(isset($contractData['landingpage']) && $contractData['landingpage'] == 'Ja' && (isset($contractData['sourcePage']) && $contractData['sourcePage'] == 'zzv'))
			{
				$selected['zzv-landingpage'] = 'selected';
			} else {
				$selected[$contractData['sourcePage']] = 'selected';
			}
		}

	$menuCode .= '
 	<li>'.urlHelper::makeLink('crmIntern', 'deletePerson', L::_(111),
 		array('pid' => $pid)).'</li>
	</ul><ul>
	<li>'.urlHelper::makeLink('crmIntern', 'createCoverLetter', L::_(379),
		array('pid' => $pid)).'</li>
	<li>'.urlHelper::makeLink('crmIntern', 'createAdviceProtocol', L::_(368),
		array('pid' => $pid)).'</li>
	<li>'.urlHelper::makeLink('crmIntern', 'genContractPdfByImage', L::_(343),
		array('pid' => $pid, 'idco' => $idco)).'</li>
	<li>'.urlHelper::makeLink('crmIntern', 'genRefundApplication', L::_(614),
			array('idco' => $idco, 'prefilled' => 0)).'</li>
	<li>'.urlHelper::makeLink('crmIntern', 'genRefundApplication', L::_(615),
			array('idco' => $idco, 'prefilled' => 1)).'</li>
	<li>'.urlHelper::makeLink('crmIntern', 'genBenefitsLongPdf', L::_(672),
			array('pid' => $pid)).'</li>';

	$menuCode .= '</ul>';


	$menuCode .='
		<p id="clear">
		<form id="complete" action="controller.php" method="get">
			<input type=hidden name=cm value=crmIntern />
			<input type=hidden name=event value=genContractPdf />
			<input type=hidden name=pid value='.$pid.' />
			<input type=hidden name=idco value='.$idco.' />

		<div id="complete_sourcePage">
			<input type="radio" id="zzv-neu" name=sourcePage value="zzv-neu" '.$checked['zzv-neu'].'><label for="zzv-neu">.com</label></input>
			<input type="radio" id="zzv" name=sourcePage value=zzv '.$checked['zzv'].$checked['zzv-experten.de'].'><label for="zzv">.de</label></input>
			<input type="radio" id="mobile" name=sourcePage value=mobil '.$checked['mobile'].'><label for="mobile">mobil</label></input>
		</div>
		<div id="complete_detail">
			<select name = "complete">
				<option value = "0_1_1">1 Seite, leer</option>
				<option value = "0_1_0">1 Seite, normal</option>
				<option value = "0_2_1" '.$selected['mobile'].$selected['zzv-landingpage'].'>2 Seiten, leer</option>
				<option value = "0_2_0" '.$selected['zzv'].$selected['zzv-neu'].$selected['zzv-experten.de'].'>2 Seiten, normal</option>
				<option value = "1_2_1">LB, 2 Seiten, leer</option>
				<option value = "1_2_0">LB, 2 Seiten, normal</option>
			</select>
		</div>
		<div id="complete_submit" class=input>
			<input type="submit" class=submit3 />
		</div>
		</form>
		</p>
		';

	$menuCode .= '</div>
		<div class="end"></div>';
	return $menuCode;

}

// person, which works at a company
public function getPersonMenu($pid, $cid=null) {
	$menuCode = $this->geth2(L::_(71));
	$menuCode .= '
	<ul>
	<li>'.urlHelper::makeLink('crmIntern', 'editPerson', L::_(109),
		array('pid' => $pid, 'cid' => $cid)).'</li>
	<li>'.urlHelper::makeLink('crmIntern', 'deletePerson', L::_(111),
		array('pid' => $pid)).'</li>
	</ul>
	';

	return $menuCode;
}
// -----------------------------------------------------------------------------
protected function getYesNoMenu($linkYes, $linkNo) {
	$code = '
	<ul id="yesNoMenu">
	<li class="yes"><a href="'.$linkYes.'">'.L::_(100).'</a></li>
	<li class="no"><a href="'.$linkNo.'">'.L::_(99).'</a></li>
	</ul>
	<div class="clearLeft"></div>
	';
	return $code;
}
// -----------------------------------------------------------------------------
//FW -------------------------------------------------------------------------------

public function getButton($iconClass,$idt,$iconText,$status='notPossible',$type='request',$view = 'short')
	{
		if ($type == 'request') {
		$url = 'href="#">';
		if ($status == 'possible') {
			$url = 'href="/'.urlHelper::makeCoreURL('wmOnlineSignup', 'start', array('idt' => $idt)).'">';
		}
		elseif ($status == 'nowCalc') {
			$url = 'href="/">';
		}
		$button = '<a class="'.$iconClass.'" '.$url.$iconText.'</a>';
		}
		else {
			$url = 'href="/'.urlHelper::makeCoreURL('wmOnlineCalc', 'compare', array('idt' => $idt, 'type' => $view)).'">';
			$button = '<a class="'.$iconClass.'" '.$url.$iconText.'</a>';
		}
		return $button;
	}
// -----------------------------------------------------------------------------
} // end class


