<?php

class crmIntern {

public $pdfCfg;
public $path;

// -----------------------------------------------------------------------------
public function __construct() {
	$this->pdfCfg['fontSize'] = 9;
	$this->pdfCfg['fontSizeCaption3'] = 10;
	$this->pdfCfg['fontSizeCaption2'] = 12;
	$this->pdfCfg['fontSizeCaption1'] = 15;
	$this->pdfCfg['fontMarginCaption3'] = 10;
	$this->pdfCfg['fontMarginCaption2'] = 0;
	$this->pdfCfg['fontMarginCaption1'] = 0;
	#$this->pdfCfg['fontFamily'] = 'Arial';
	$this->pdfCfg['fontFamily'] = 'DejaVu';
	$this->pdfCfg['fontLineHeight'] = 5;
	$this->pdfCfg['fontColor'] = 10;
	$this->pdfCfg['fontColorGrey'] = 155;
	$this->pdfCfg['fontColorGreyLight'] = 181;
	$this->pdfCfg['border'] = 0;
	$this->pdfCfg['leftMargin'] = 30;
	$this->pdfCfg['childLineMargin'] = 10;


}
// -----------------------------------------------------------------------------
// function getReminderPdfData
//
// Identisch zu getContractPdfData nur ohne ContractData
// Verwendet durch Reminder-Funktion

public function getReminderPdfData($pid) {
	$person = new person();
	$crm = new crm();

	$personData = $person->get($pid);
	$address = $crm->getAddressPrimary('pid', $pid);
	$contact = $crm->getContactPrimary('pid', $pid);

	if(!empty($personData['isFatherOfPid'])) {
		$personInsuredData = $person->get($personData['isFatherOfPid']);
		$personInsuredData['bonus'] = $wzm->getBonusBaseWithDate($contractData['idt'], $personInsuredData['birthdate'], $contractData['wishedBegin']);
	}
	else {
		$personInsuredData = $personData;
		$personInsuredData['bonus'] = $wzm->getBonusBaseWithDate($contractData['idt'], $personInsuredData['birthdate'], $contractData['wishedBegin']);
	}

	$ret = array(
		'name' => $personData['surname'].', '.$personData['forename'],
		'nameb' => $personData['forename'].', '.$personData['surname'],
		'namec' => $personData['forename'].' '.$personData['surname'],
		'gender' => ($personData['salutationLid']==9)?'male':'female',
		'street' => $address['street'],
		'postcode' => $address['postcode'],
		'city' => $address['city'],
		'phone' => $contact['phone'],
		'mobile' => $contact['mobile'],
		'email' => $contact['email'],
		'birthdate' => stringHelper::makeGermanDate(
			$personData['birthdate']),
		'birthdate2' => stringHelper::makeGermanDate(
			$personInsuredData['birthdate']),
		'gender' => ($personData['salutationLid']==9)?'male':
			'female',
		'gender2' => ($personInsuredData['salutationLid']==9)?'male':
			'female',
		'married' => ($personData['familyStatusLid']==38)?true:false,
		'widowed' => ($personData['familyStatusLid']==39)?true:false,
		'married2' => ($personInsuredData['familyStatusLid']==38)?true:false,
		'widowed2' => ($personInsuredData['familyStatusLid']==39)?true:false,
		'name2' => $personInsuredData['forename'].' '.
			$personInsuredData['surname'],
		'name2b' => $personInsuredData['surname'].', '.
			$personInsuredData['forename'],
		'nation' => ((empty($personData['nationString']))?
			$personData['nationCode']:
			$personData['nationString']),
		'nation2' => ((empty($personInsuredData['nationString']))?
			$personInsuredData['nationCode']:
			$personInsuredData['nationString']),
		'job' => $personData['job'],
		'insurance' => $personData['insurance'],
		'insuredSince' => str_replace('00.', '   ',
			stringHelper::makeGermanDate($personData['insuredSince'])),
		'insuredStrict' => ($personData['insuranceStatusLid']==34)?
			true:false,
		'insuredFree' => ($personData['insuranceStatusLid']==35)?
			true:false,
		'insuredFamily' => ($personData['insuranceStatusLid']==36)?
			true:false,
		'job2' => $personInsuredData['job'],
		'insurance2' => $personInsuredData['insurance'],
		'insuredSince2' => str_replace('00.', '   ',
			stringHelper::makeGermanDate($personInsuredData['insuredSince'])),
		'insuredStrict2' => ($personInsuredData['insuranceStatusLid']==34)?
			true:false,
		'insuredFree2' => ($personInsuredData['insuranceStatusLid']==35)?
			true:false,
		'insuredFamily2' => ($personInsuredData['insuranceStatusLid']==36)?
			true:false,
		'person' => $personData,
		'personInsured' => (empty($personData['isFatherOfPid'])?null:
			$person->get($personData['isFatherOfPid'])),
	);

	return $ret;
}


// -----------------------------------------------------------------------------
public function getContractPdfData($idco) {
	$contract = new contract();
	$person = new person();
	$crm = new crm();
	$wzm = new wzm();
	$label = new label();
	$project = new project();

	$contractData = $contract->get($idco);
	$personData = $person->get($contractData['pid']);
	$address = $crm->getAddressPrimary('pid', $contractData['pid']);
	$contact = $crm->getContactPrimary('pid', $contractData['pid']);
	$tariff = $wzm->getTariff($contractData['idt']);

	/*
	if(isset($tariff['highlights_raw']))
	{
		@$json = json_decode($tariff['highlights_raw'], true);
		
		if (is_array($json))
		{
			$highlights = current($json);
		} else {
			$highlights = '';
		}
	}
	*/

	$highlights = '';
	$highlightsFromDb = $contract->getInsuranceHighlights($contractData['idt']);

	if(isset($highlightsFromDb))
	{
		$highlights = array();
		for($i=0;$i<count($highlightsFromDb);$i++)
		{
			$highlights[] = $highlightsFromDb[$i]['highlight_text'];
		}
	}


	if(!empty($personData['isFatherOfPid'])) {
		$personInsuredData = $person->get($personData['isFatherOfPid']);
		$personInsuredData['bonus'] = $wzm->getBonusBaseWithDate($contractData['idt'], $personInsuredData['birthdate'], $contractData['wishedBegin']);
	}
	else {
		$personInsuredData = $personData;
		$personInsuredData['bonus'] = $wzm->getBonusBaseWithDate($contractData['idt'], $personInsuredData['birthdate'], $contractData['wishedBegin']);
	}

	$ret = array(
		'idt' => $contractData['idt'],
		'rank' => $contractData['rank'],
		'name' => $personData['surname'].', '.$personData['forename'],
		'nameb' => $personData['forename'].', '.$personData['surname'],
		'namec' => $personData['forename'].' '.$personData['surname'],
		'gender' => ($personData['salutationLid']==9)?'male':'female',
		'street' => $address['street'],
		'postcode' => $address['postcode'],
		'city' => $address['city'],
		'phone' => $contact['phone'],
		'mobile' => $contact['mobile'],
		'email' => $contact['email'],
		'birthdate' => stringHelper::makeGermanDate(
			$personData['birthdate']),
		'birthdate2' => stringHelper::makeGermanDate(
			$personInsuredData['birthdate']),
		'age' => $wzm->getAgeByIdt($contractData['idt'], $personData['birthdate']),
		'age2' => $wzm->getAgeByIdt($contractData['idt'], $personInsuredData['birthdate']),
		'gender' => ($personData['salutationLid']==9)?'male':
			'female',
		'gender2' => ($personInsuredData['salutationLid']==9)?'male':
			'female',
		'married' => ($personData['familyStatusLid']==38)?true:false,
		'widowed' => ($personData['familyStatusLid']==39)?true:false,
		'married2' => ($personInsuredData['familyStatusLid']==38)?true:false,
		'widowed2' => ($personInsuredData['familyStatusLid']==39)?true:false,
		'name2' => $personInsuredData['forename'].' '.
			$personInsuredData['surname'],
		'name2b' => $personInsuredData['surname'].', '.
			$personInsuredData['forename'],
		'nation' => ((empty($personData['nationString']))?
			$personData['nationCode']:
			$personData['nationString']),
		'nation2' => ((empty($personInsuredData['nationString']))?
			$personInsuredData['nationCode']:
			$personInsuredData['nationString']),
		'job' => $personData['job'],
		'insurance' => $personData['insurance'],
		'insuredSince' => str_replace('00.', '   ',
			stringHelper::makeGermanDate($personData['insuredSince'])),
		'insuredStrict' => ($personData['insuranceStatusLid']==34)?
			true:false,
		'insuredFree' => ($personData['insuranceStatusLid']==35)?
			true:false,
		'insuredFamily' => ($personData['insuranceStatusLid']==36)?
			true:false,
		'job2' => $personInsuredData['job'],
		'insurance2' => $personInsuredData['insurance'],
		'insuredSince2' => str_replace('00.', '   ',
			stringHelper::makeGermanDate($personInsuredData['insuredSince'])),
		'insuredStrict2' => ($personInsuredData['insuranceStatusLid']==34)?
			true:false,
		'insuredFree2' => ($personInsuredData['insuranceStatusLid']==35)?
			true:false,
		'insuredFamily2' => ($personInsuredData['insuranceStatusLid']==36)?
			true:false,
		'begin' => substr(stringHelper::makeGermanDate(
			$contractData['wishedBegin']), -7),
		'wishedBegin' => $contractData['wishedBegin'],
		'changeInsurance' => $contractData['changeInsurance'],
		'previousTarif' => $contractData['previousTarif'],
		'previousTarifInsured' => $contractData['previousTarifInsured'],
		'signupDate' => stringHelper::makeGermanDate(
			substr($contractData['signupTime'], 0, 10)),
		'price' => (empty($contractData['price'])?'':
			number_format($contractData['price'], 2, ',', '.')),
		'bonusbase' => number_format($contractData['bonusbase'], 2, ',', '.'),
		'bonus' => number_format($contractData['bonus'], 2, ',', '.'),
		'bonus_tooth' => number_format($contractData['bonus'] - ($contractData['tooth5'] * 13), 2, ',', '.'),
		'bonus_allianz740' => number_format($contractData['tooth5'] * 13, 2, ',', '.'),
		'bonusEuroFromPercent' => number_format($contractData['bonusbase'] *	$contractData['bonus'], 2, ',', '.'),
		't1' => $contractData['tooth1'] == -1 ? null : $contractData['tooth1'],
		't2' => $contractData['tooth2'] == -1 ? null : $contractData['tooth2'],
		'rwzz_top_1' => $contractData['rwzz_top_1'],
		'rwzz_top_2' => $contractData['rwzz_top_2'],
		'aufbiss_paradontose' => $contractData['aufbiss_paradontose'],
		'toothselect' => str_replace('z', '',
			$contractData['toothSelect']),
		'incare' => ($contractData['incare']=='yes')?true:false,
		'person' => $personData,
		'familyStatus' => $label->getLabelName($personData['familyStatusLid']),
		'personInsured' => $personInsuredData,
		'isearch' => $contractData['isearch'],
		'adviceIdt' => $contractData['adviceIdt'],
		'tariffName' => $tariff['name'],
		'highlightsName' => $tariff['highlightsName'],
		'highlights' => $highlights,
		'benefits' => json_decode($tariff['benefits_raw'], true),
		'series' => $contractData['series'],
		'sourcePage' => $contractData['sourcePage'],
		'contractComplete' => $contractData,
		'financetestLogoFilename' => $project->getFinancetestLogoFilename($contractData['idt'])
	);

	return $ret;
}
// -----------------------------------------------------------------------------
public function getRefundApplicationPdfData($idco) {
	$wzm = new wzm();
	$crm = new crm();
	$contract = new contract();
	$person = new person();
	$contractData = $contract->get($idco);
	$personData = $person->get($contractData['pid']);
	$insurance = $wzm->getInsuranceByTariffId($contractData['idt']);
	$tariff = $wzm->getTariff($contractData['idt']);
	$address = $crm->getAddressPrimary('pid', $contractData['pid']);
	$contact = $crm->getContactPrimary('pid', $contractData['pid']);

	$return = array(
		'insurance' => array(
			'name' => $insurance['name'],
			'street' => $insurance['street'],
			'postCode' => $insurance['zip'],
			'postBox' => $insurance['post_box'],
			'city' => $insurance['city']
		),
		'tariff' => array(
			'idt' => $tariff['idt']
		),
		'person' => array(
			'salutationId' => $personData['salutationLid'],
			'familyStatusId' => $personData['familyStatusLid'],
			'forename' => $personData['forename'],
			'surname' => $personData['surname'],
			'nationCode' => $personData['nationCode'],
			'job' => $personData['job']
		),
		'address' => array(
			'street' => $address['street'],
			'postcode' => $address['postcode'],
			'city' => $address['city']
		),
		'contact' => array(
			'phone' => $contact['phone'],
			'mobile' => $contact['mobile'],
			'email' => $contact['email']
		)
	);

	return $return;
}
// -----------------------------------------------------------------------------
private function setPTSansFont($size, $style = 'R', $pdf)
{
	if ($style == 'R') {
		$font = 'PTSans-Regular';
	} else {
		$font = 'PTSans-Bold';
	}
	$pdf->SetFont($font, '', $size);
	return $pdf;
}
// -----------------------------------------------------------------------------
private function pdfRect($x, $y, $w, $pdf, $h=5)
{
		$pdf->SetAlpha(0.45);
		$pdf->SetFillColor(235,235,0);
		$pdf->Rect($x,$y,$w,$h,'F');
		$pdf->SetFillColor(0);
		$pdf->SetAlpha(1);

		return $pdf;
}

private function pdfClean($x, $y, $w, $pdf, $r=255, $g=255, $b=255, $h=5, $fill='F')
{
		$pdf->SetAlpha(1);
		$pdf->SetFillColor($r,$g,$b);
		$pdf->SetDrawColor($r,$g,$b);
		$pdf->SetLineWidth(0.4);
		$pdf->Rect($x,$y,$w,$h,$fill);
		$pdf->SetFillColor(0);
		//$pdf->SetAlpha(1);
		return $pdf;
}

private function createPdf($o, $f, $s) {
	if($o != 'P' && $o != 'L') unset($o);
	$orientation = $o==null?'P':$o;

	$pdf = new PDF($orientation, $f, $s);
	$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
	$pdf->AddFont('DejaVu','B','DejaVuSansCondensed-Bold.ttf',true);
	$pdfCfg = &$this->pdfCfg;
	$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
	$pdf->SetTextColor($pdfCfg['fontColor']);
	
	return $pdf;

}

private function getBenefitByAge($idt, $birthdate, $wishedBegin)
{
	/* This function is used for the glasses Offer starting with R+V and shall return the needed calculation amounts */
	$wzm = new wzm();

	$bonus = $wzm->getBonusBaseWithDate($idt, $birthdate, $wishedBegin);

	return $bonus;
}

// -----------------------------------------------------------------------------
public function genRefundApplication($idco, $prefilled) {

	// get data
	$data = $this->getRefundApplicationPdfData($idco);

	$pdf = $this->createPdf('P');

	include ('contracts/refund-application.php.inc');

	return $pdf;
}

public function generateBenefitsLongPdf($data)
{
	$c = new contract();
	$c->getBenefitsLong($data);
}

public function getBenefitsLongPdf($data) {
	$this->generateBenefitsLongPdf($data);

	// Open it
	$name = 'files/benefits-long/local.pdf';
	$content = file_get_contents($name);
	header('Content-Type: application/pdf');
	header('Content-Length: '.strlen($content));
	header('Content-disposition: inline; filename="' . $name . '"');
	header('Cache-Control: public, must-revalidate, max-age=0');
	header('Pragma: public');
	header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
	header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
	echo $content;
}

// -----------------------------------------------------------------------------
public function genContractPdfByImage($idco, $pdfTemplate=null, $complete=null, $reset=null, $pdf=null, $path=null, $benefitDescription=null, $sourcePage=null) {
	/*
	** $idco                Vertrag ID
	** $pdfTemplate         Im CRM kann so für jeden Antrag eine Versicherung gewält werden
	** $complete            Complete-Funktion
	** $reset               Antrag soll in Kombination mit Complete leer gedruckt werden
	** $pdf                 Objekt pdf -> wird für Seriendruck verwendet.
	** $path                Pfadangabe, normalisierung zwischen / und /crm
	** $benefitDescription  
	** $sourcePage 		    Anpassung der Source - für die Ausgabe der Anträge der unterschiedlichen Webseiten 
	*/


	// keine Übergabe -> aus project holen!
	if($path == null)
		$path = project::getProjectRoot();

	// hier sollten wir einen Pfad haben, abbruch, falls nicht.
	if($path == null || $path == '/') {
		errorHandler::throwErrorDb('error in crmIntern (function genContractPdfByImage), path not present.', $idco);
		return -1;
	} else {
		if(substr($path, -1) != '/')
			$path .= '/';
	}

	$this->path = $path;

	#$titles = array('Original - Bitte zurückschicken', 'Kopie - Für Ihre Unterlagen');
	$titles = array('Original - Bitte zurückschicken');

	#print_r($path);
	// get data
	$data = $this->getContractPdfData($idco);

	if($sourcePage) {
		// sourcePage überschreiben, wenn bei Aufruf übergeben wird.
		$data['contractComplete']['sourcePage'] = $data['sourcePage'] = $sourcePage;
	}

	// determine template of contract pdf
	if(empty($pdfTemplate)) {
		$pdfTemplate = $data['idt'];
	}

	// set default orientation, format and size
	$orientation = 'P';
	$format = 'mm';
	$size = 'A4';

	// change size for some tariffs
	if($pdfTemplate == project::gti('gothaer-mediz-plus-medi-prophy') || $pdfTemplate == project::gti('gothaer-mediz-plus') || $pdfTemplate == project::gti('gothaer-mediz-basis-medi-prophy') || $pdfTemplate == project::gti('gothaer-mediz-basis')) {
		$format = 'mm';
		#$size = array('210', '472');
		$size = array('210', '290');
	}

	// load pdf
	if(!$pdf) {
		$pdf = $this->createPdf($orientation, $format, $size);
	}
	$pdfCfg = &$this->pdfCfg;

	if($reset) {
	/*
	* Reset all Questions to null, so that the customer can fill the fields
	* All, but InsuranceType (adult / kids)
	*/
		$contractType = $data['contractComplete']['contractType'];

		unset($data['t1']);
		unset($data['t2']);
		unset($data['rwzz_top_1']);
		unset($data['rwzz_top_2']);
		unset($data['aufbiss_paradontose']);
		unset($data['toothselect']);
		unset($data['incare']);
		unset($data['contractComplete']);

		$data['contractComplete']['contractType'] = $contractType;
		$data['contractComplete']['reset'] = $reset;
	}


	// 20181021 Cover sheet should be used for all source pages
	
		// Benötigte Variablen für CoverSheet
		// tariff-id
		// first-name
		// last-name
		// birthdate (dd.mm.yyyy)
		// insurance-begin-date (dd.mm.yyyy)
		// series
		// missing-teeth
		// replaced-teeth-removable
  		// replaced-teeth-fix
        	// source-page
		// price
  		// uid

		$c = new contract();
		$personInsured = $data['personInsured'];
		$c->getCoverSheet(array(
			'tariff-id'=>$data['idt'], 
			'first-name'=>stringHelper::str_encode_utf8($personInsured['forename']),
			'last-name'=>stringHelper::str_encode_utf8($personInsured['surname']), 
			'birthdate'=>$data['birthdate2'], 
			'insurance-begin-date'=>stringHelper::makeGermanDate($data['wishedBegin']),
			'series'=>$data['series'],
                	'missing-teeth' => $data['contractComplete']['tooth1'],
                	'replaced-teeth-removable' => $data['contractComplete']['tooth3'],
                	'replaced-teeth-fix' => $data['contractComplete']['tooth4'],
                	'source-page' => $data['sourcePage'],
			'price' => $data['price'],
			'uid'=>uniqid(),
			'path'=>$path
		));


	/* Complete - AddOn
	// lt. email 13.12. soll der Aufbau folgendermassen sein
	// Anpassung 07/2014
	//
	// 1 x Anschreiben
	// 1 x Deckblatt Vorder- und Rueckseite (seit 07/2014)
	// 1 x Antrag Original
	// 1 x Beratungsprotokoll mit Markierung Unterschrift
	// 1 x Antrag Kopie
	// 1 x Beratungsprotokoll ohne Markierung
	*/

	if(isset($complete)) {
		include ($path.'web-modules/crmIntern/contracts/anschreiben.php.inc');
		include ($path.'web-modules/crmIntern/contracts/deckblatt_neu.php.inc'); 
	}

	$beratungsprotokoll = 0;

	// loop for two versions of pages in pdf: original and copy
	for($i=0; $i<count($titles); $i++) {
		switch($pdfTemplate) {

	// -------------------------------------------------------------------------
	case project::gti('allianz-dent-plus'):
	case project::gti('allianz-dent-best'):
	case project::gti('allianz-zb-fit'):
	case project::gti('allianz-zahn-plus-zahn-fit'):
	case project::gti('allianz-zb'):
	case project::gti('allianz-zahn-plus'):
		include ($path.'web-modules/crmIntern/contracts/allianz_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('allianz740'):
		include ($path.'web-modules/crmIntern/contracts/allianz740_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('allianz-meinzahnschutz100'):
	case project::gti('allianz-meinzahnschutz100ar'):
	case project::gti('allianz-meinzahnschutz-90'):
	case project::gti('allianz-meinzahnschutz-90ar'):
	case project::gti('allianz-meinzahnschutz-75'):
	case project::gti('allianz-meinzahnschutz-75ar'):
		include ($path.'web-modules/crmIntern/contracts/allianz_meinzahnschutz_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('aragz90'):
	case project::gti('aragz100'):
	case project::gti('arag-dental-pro-z70'):
	case project::gti('arag-dental-pro-z50'):
		include ($path.'web-modules/crmIntern/contracts/arag_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('arag-dent100'):
	case project::gti('arag-dent90plus'):
	case project::gti('arag-dent90'):
	case project::gti('arag-dent70'):
		include ($path.'web-modules/crmIntern/contracts/arag-dent_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------	
	case project::gti('astra-mega'):
	case project::gti('astra-sieger'):
	case project::gti('astra-plus'):
	case project::gti('astra-perfekt'):
		include ($path.'web-modules/crmIntern/contracts/astra_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('axa-dent-premium'):
	case project::gti('axa-dent-komfort'):
	case project::gti('axa-dent'):
	case project::gti('axa-dent-smile-inlay'):
		include ($path.'web-modules/crmIntern/contracts/axa-dent_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------	
	case project::gti('advigon-ideal'):
		include ($path.'web-modules/crmIntern/contracts/advigon_ideal_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('barzgplus'):
	case project::gti('barmenia-az-plus'):
	case project::gti('barmenia-prophy'):
		include ($path.'web-modules/crmIntern/contracts/barzg_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('barmenia-zahn100'):
	case project::gti('barmenia-zahn100-zv'):
	case project::gti('barmenia-zahn90'):
	case project::gti('barmenia-zahn80'):
	case project::gti('barmenia-zahn90-zv'):
	case project::gti('barmenia-zahn80-zv'):
	case project::gti('barmenia-mehrzahnvorsorge'):
		include ($path.'web-modules/crmIntern/contracts/barmenia_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('bayerische-vip-dental-komfort'):
	case project::gti('bayerische-vip-dental-smart'):
		include ($path.'web-modules/crmIntern/contracts/bayerische-vdent_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('bayerische-vdent-prestige'):
		include ($path.'web-modules/crmIntern/contracts/bayerische-vdent-prestige_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('bayerische-zahn-smart'):
	case project::gti('bayerische-zahn-komfort'):
	case project::gti('bayerische-zahn-prestige'):
	case project::gti('bayerische-zahn-prestige-plus'):
	case project::gti('bayerische-zahn-smart-sofort'):
	case project::gti('bayerische-zahn-komfort-sofort'):
	case project::gti('bayerische-zahn-prestige-sofort'):
	case project::gti('bayerische-zahn-prestige-plus-sofort'):

		include ($path.'web-modules/crmIntern/contracts/bayerische-zahn_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('bayerische-dentplus'):
		include ($path.'web-modules/crmIntern/contracts/bayerische-dentplus_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('concordia-zt'):
	case project::gti('concordia-zt-zb'):
	case project::gti('concordia-ze'):
	case project::gti('concordia-ze-zb'):
	case project::gti('concordia-zahn-sorglos'):
		include ($path.'web-modules/crmIntern/contracts/concordia_complete.php.inc');
		$beratungsprotokoll = 1;
		break;	
	// -------------------------------------------------------------------------
	case project::gti('conticezp-u'):
	case project::gti('conticezk-u'):
	case project::gti('continentale-ceze'):
		include ($path.'web-modules/crmIntern/contracts/continentale_complete.php.inc');
		$beratungsprotokoll = 1;
		break;	
	// -------------------------------------------------------------------------
	case project::gti('css-ideal'):
		include ($path.'web-modules/crmIntern/contracts/css_ideal_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('css-premium-zgp'):
		include ($path.'web-modules/crmIntern/contracts/csstopzb_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('dadirekt-komf'):
	case project::gti('dadirekt-prem'):
	case project::gti('dadirekt-prem+'):
		include ($path.'web-modules/crmIntern/contracts/da_direkt_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('dfv-exklusiv'):
	case project::gti('dfv-basis50'):
	case project::gti('dfv-komf70'):
	case project::gti('dfv-prem90'):
		include ($path.'web-modules/crmIntern/contracts/dfv-exklusiv_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('dfv-exkl-plus'):
		include ($path.'web-modules/crmIntern/contracts/dfv-exkl-plus_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('dkv-kdt85'):
	case project::gti('dkv-kdt85-kdbe'):
	case project::gti('dkv-kdt50'):
	case project::gti('dkv-kdt50-kdbe'):
	case project::gti('dkv-kdt'):
	case project::gti('dkv-kdbe'):
	case project::gti('dkv-kdtp100'):
	case project::gti('dkv-kdtp100-kdbe'):
		include ($path.'web-modules/crmIntern/contracts/dkv_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('ergo-direkt-premium'):
	case project::gti('ergo-direkt-zab-zae'):
	case project::gti('ergo-zahn-ersatz-zab'):
	case project::gti('ergo-direkt-dental-zuschuss'):
	case project::gti('ergo-direkt-zahn-erhalt-permium'):
	case project::gti('ergo-direkt-zahn-erhalt'):
		include ($path.'web-modules/crmIntern/contracts/ergo_complete.php.inc');
		break;
	// -------------------------------------------------------------------------
	case project::gti('ergo-direkt-zez'):
		include ($path.'web-modules/crmIntern/contracts/ergo_zez_complete.php.inc');
		break;
	// -------------------------------------------------------------------------
	case project::gti('gothaer-mediz-prem-medi-prophy'):
	case project::gti('gothaer-mediz-prem'):
	case project::gti('gothaer-mediz-plus-medi-prophy'):
	case project::gti('gothaer-mediz-plus'):
	case project::gti('gothaer-mediz-basis-medi-prophy'):
	case project::gti('gothaer-mediz-basis'):
		include ($path.'web-modules/crmIntern/contracts/gothaer_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('gothaer-mediz-duo'):
		include ($path.'web-modules/crmIntern/contracts/gothaer-mediz-duo_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('gothaer-mediz-duo-80'):
	case project::gti('gothaer-mediz-duo-90'):
	case project::gti('gothaer-mediz-duo-100'):
		include ($path.'web-modules/crmIntern/contracts/gothaer-mediz-duo80-100_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('gothaer-mediz-smile-75'):
	case project::gti('gothaer-mediz-smile-85'):
		include ($path.'web-modules/crmIntern/contracts/gothaer-mediz-smile75-85_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('hallesche-gigadent'):
	case project::gti('hallesche-megadent'):
	case project::gti('hallesche-dentze100-dentpro90'):
	case project::gti('hallesche-dentze90-dentpro80'):
	case project::gti('hallesche-dentze100'):
	case project::gti('hallesche-dentze90'):
		include ($path.'web-modules/crmIntern/contracts/hallesche_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('hansemerkur-ez-ezt'):
	case project::gti('hansemerkur-ez-ezt-ezp'):
	case project::gti('hansemerkur-ez'):
	case project::gti('hansemerkur-ez-eze'):
	case project::gti('hansemerkur-ez-eze-ezp'):
	case project::gti('hansemerkur-ez-ezp'):
	case project::gti('hansemerkur-ezp'):
		include ($path.'web-modules/crmIntern/contracts/hansemerkur_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('hansemerkur-ezl'):
	case project::gti('hansemerkur-ezk'):
		include ($path.'web-modules/crmIntern/contracts/hansemerkur-ezl-ezk_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('inter-z90'):
	case project::gti('inter-z80'):
	case project::gti('inter-z70'):
	case project::gti('inter-z90-zpro'):
	case project::gti('inter-z80-zpro'):
	case project::gti('inter-z70-zpro'):
		include ($path.'web-modules/crmIntern/contracts/inter_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('janitos-ja-dental-plus'):
	case project::gti('janitos-dentmax'):
	case project::gti('janitos-dental'):
		include ($path.'web-modules/crmIntern/contracts/janitos_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('lkh-zu90'):
	case project::gti('lkh-zu90-l'):
	case project::gti('lkh-zu70'):
	case project::gti('lkh-zu70-l'):
		include ($path.'web-modules/crmIntern/contracts/lkh_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('mv-571-572-573-574'):
	case project::gti('mv-571-574'):
	case project::gti('mv-571'):
	case project::gti('mv-570'):
	case project::gti('mv-570-572-573-574'):
	case project::gti('mv-570-574'):
	case project::gti('mv-572-573-574'):
	case project::gti('mv-560'):
	case project::gti('mv-561'):
		include ($path.'web-modules/crmIntern/contracts/mv_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('mv-577'):
	case project::gti('mv-578'):
	case project::gti('mv-579'):
		include ($path.'web-modules/crmIntern/contracts/mv_zg_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('nuern-z80'):
	case project::gti('nuern-z90'):
	case project::gti('nuern-z100'):
		include ($path.'web-modules/crmIntern/contracts/nuernberger_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('nuernzp80'):
		include ($path.'web-modules/crmIntern/contracts/nuernzp80_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('nuern-zep80'):
	case project::gti('nuern-zep80-zv'):
	case project::gti('nuern-zr-zv'):
	case project::gti('nuern-zr'):
		include ($path.'web-modules/crmIntern/contracts/nuernberger_zep80_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('ottonova-zahn70'):
	case project::gti('ottonova-zahn85'):
	case project::gti('ottonova-zahn100'):
		include ($path.'web-modules/crmIntern/contracts/ottonova_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('rv-premium-z1'):
	case project::gti('rv-comfort-z2'):
	case project::gti('rv-premium-z1zv'):
	case project::gti('rv-comfort-z2zv'):
	case project::gti('rv-classic-z3u-zv'):
	case project::gti('rv-classic-z3u'):
		include ($path.'web-modules/crmIntern/contracts/rv_beiblatt_brille.php.inc');
		include ($path.'web-modules/crmIntern/contracts/rv_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('rv-zv'):
		include ($path.'web-modules/crmIntern/contracts/rv_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('sdk-zahn100-zp1'):
	case project::gti('sdk-zahn90-zp9'):
	case project::gti('sdk-zahn70-zp7'):
		include ($path.'web-modules/crmIntern/contracts/sdk_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('sigidu-komstart'):
	case project::gti('sigidu-komplus'):
	case project::gti('sigidu-komtop'):
		include ($path.'web-modules/crmIntern/contracts/sigidu_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('sigidu-zahn-top-pur'):
	case project::gti('sigidu-zahn-plus-pur'):
	case project::gti('sigidu-zahn-start-pur'):
	case project::gti('sigidu-zahn-basis-pur'):
	case project::gti('sigidu-zahntop'):
	case project::gti('sigidu-zahnplus'):
	case project::gti('signal-iduna-zahn-exklusiv'):
	case project::gti('signal-iduna-zahn-exklusiv-pur'):
		include ($path.'web-modules/crmIntern/contracts/sigidu_pur_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('stuttgarter-premium'):
	case project::gti('stuttgarter-komfort'):
		include ($path.'web-modules/crmIntern/contracts/stuttgarter_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('ukv-zp-premium'):
	case project::gti('ukv-zp-optimal'):
	case project::gti('ukv-zp-kompakt'):
		include ($path.'web-modules/crmIntern/contracts/ukv-zp_complete.php.inc');
		$beratungsprotokoll = 1;
		break;

	// -------------------------------------------------------------------------
	case project::gti('universa-uni-dent-privat'):
	case project::gti('universa-uni-dent-komfort'):
		include ($path.'web-modules/crmIntern/contracts/universa_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('vkb-zp-premium'):
	case project::gti('vkb-zp-optimal'):
	case project::gti('vkb-zp-kompakt'):
		include ($path.'web-modules/crmIntern/contracts/vkb-zp_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('wuerttembergische-v1'):
	case project::gti('wuerttembergische-v2'):
	case project::gti('wuerttembergische-v3'):
	case project::gti('wuerttembergische-z1'):
	case project::gti('wuerttembergische-z2'):
	case project::gti('wuerttembergische-z3'):
	case project::gti('wuerttemberische-ze90-zbe'):
	case project::gti('wuerttemberische-ze70-zbe'):
	case project::gti('wuerttemberische-ze50-zbe'):
		include ($path.'web-modules/crmIntern/contracts/wuerttembergische_complete.php.inc');
		$beratungsprotokoll = 1;
		break;
	// -------------------------------------------------------------------------
	case project::gti('clinic-s-prem'):
		include ($path.'web-modules/crmIntern/contracts/gothaer_stationaer_anschreiben.php.inc');
		include ($path.'web-modules/crmIntern/contracts/gothaer_stationaer.php.inc');
		include ($path.'web-modules/crmIntern/contracts/gothaer_stationaer_beratungsdokumentation.php.inc');
		break;
	// -------------------------------------------------------------------------

	default:
		include ($path.'web-modules/crmIntern/contracts/beratungsprotokoll.php.inc');
	break;

		} // end switch
	} // end for


	if($beratungsprotokoll == 1)
		include ($path.'web-modules/crmIntern/contracts/beratungsprotokoll.php.inc');



	if($benefitDescription == 1)
	{
		/*
			$fields = array(
		  	'tariffId'                => 23,
		 	'series'                   => 0,
		 	'sourcePage'			   => 'zzv-neu'
			);
		*/

		$data = array(
			'tariffId'  => $data['idt'],
			'series'     => $data['series'],
			'sourcePage' => $data['sourcePage']
			);

		$this->generateBenefitsLongPdf($data);
		include ($path.'web-modules/crmIntern/contracts/leistungsbeschreibung.php.inc');
	}

	return $pdf;
}
// -----------------------------------------------------------------------------

// RW ADD 17112009
// Ausgabe des erzeugten PDF Dokumentes
// fpdf Output : function Output($dest='', $name='', $isUTF8=false)

function outputPDF($pdf, $idco=null) {
	if($idco) {
		$pdf->Output('F', $this->path.'pdf/Antrag - '.$idco.'.pdf', true);
	} else {
		//$pdf->Output('D', null, true);
		$pdf->Output();
	}
}

// -----------------------------------------------------------------------------
private function pdfAddCaption($pdf, $caption, $level=1) {
	$pdfCfg = $this->pdfCfg;
	$this->pdfAddSpace($pdf);
	$pdfCaptionClassName = 'fontSizeCaption'.$level;
	$pdfMarginClassName = 'fontMarginCaption'.$level;
	$margin = $pdfCfg[$pdfMarginClassName];
	if(!empty($margin)) {
		$pdf->Cell($margin, 0, '', 0, 0);
	}
	$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg[$pdfCaptionClassName],
		0, 2);
	$pdf->SetTextColor($pdfCfg['fontColorGrey']);
	$pdf->Cell(40,10, $caption, $pdfCfg['border'], 1);
}

private function pdfAddChildLine($pdf, $string) {
	$pdfCfg = $this->pdfCfg;
	$pdf->Cell($pdfCfg['childLineMargin'], 0, '', 0, 0);
	$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
	$pdf->SetTextColor($pdfCfg['fontColor']);
	$pdf->Cell(0, $pdfCfg['fontLineHeight'], $string, $pdfCfg['border'], 1);
}

private function pdfAddSpace($pdf) {
	$pdfCfg = $this->pdfCfg;
	$pdf->Cell(1, 2, '', 0, 1);
}

// -----------------------------------------------------------------------------
public static function sendFileToBrowser($content, $filename=null, $filetype='csv') {
	if(empty($filename)) {
		$filename = 'download.txt';
	}

	// send file to browser
	if(headers_sent() || ob_get_length()) {
		echo 'Some data has already been output, can\'t send PDF file';
		exit;
	}
	//flush(); ob_flush();
	header('Content-Type: application/'.$filetype);
	header('Content-Length: '.strlen($content));
	header('Content-Disposition: attachment; filename="'.$filename.'"');
	print $content;
}
// -----------------------------------------------------------------------------

public function getEMailBySourcePage($sourcePage)
{
	switch ($sourcePage)
	{
		case 'zzv-de':
		case 'mobile':
			return 'info@zahnzusatzversicherung-experten.de';
			break;
		case 'zzv-neu':
			return 'info@zahnzusatzversicherung-experten.com';
			break;
		default:
			return 'info@zahnzusatzversicherung-experten.de';	
	}
}

public function getWebBySourcePage($sourcePage)
{
	switch ($sourcePage)
	{
		case 'zzv-de':
		case 'mobile':
			return 'www.zahnzusatzversicherung-experten.de';
			break;
		case 'zzv-neu':
			return 'www.zahnzusatzversicherung-experten.com';
			break;
		default:
			return 'www.zahnzusatzversicherung-experten.de';	
	}
}


public function getCompanyLogo($x, $y, $pdf, $sourcePage, $i, $width=50)
{
	switch ($sourcePage)
	{
		case 'zzv-de':
		case 'mobile':
			if($i==0) {
				$pdf->Image($this->path.'/files/logo.png', $x, $y, $width);
			} else {
				$pdf->Image($this->path.'/files/logo_sw.png', $x, $y, $width);
			}
			break;
		case 'zzv-neu':
			$pdf->Image($this->path.'/files/logo_com.png', $x, $y + 12, $width);
			break;
		default:
			if($i==0) {
				$pdf->Image($this->path.'/files/logo.png', $x, $y, $width);
			} else {
				$pdf->Image($this->path.'/files/logo_sw.png', $x, $y, $width);
			}	
	}

}


/*
** input 
**		x, y : coordinates
**		pdf : our pdf
**		sourcePage : zzv-de, mobile, zzv-neu
**		size : size of 
**		orientation : vertical, horizontal
**		style : wide, normal
*/

public function getCompanyStamp($x, $y, $pdf, $sourcePage, $size=8, $orientation='vertical', $style='normal') 
{
	switch ($orientation)
	{
		case 'vertical':
			$pdf->SetFont($this->pdfCfg['fontFamily'], '', $size);
			$pdf->setXY($x, $y);
			$pdf->Cell(45, ($size-2)/2, 'Versicherungsmakler Experten GmbH', 0, 1);
			$pdf->setX($x);
			$pdf->Cell(45, ($size-2)/2, 'Feursstr. 56 / RGB', 0, 1);
			$pdf->setX($x);
			$pdf->Cell(45, ($size-2)/2, '82140 Olching', 0, 1);
			$pdf->setX($x);
			$pdf->Cell(45, ($size-2)/2, 'Tel.: 08142 - 651 39 28', 0, 1);
			$pdf->setX($x);
			$pdf->Cell(45, ($size-2)/2, $this->getEMailBySourcePage($sourcePage), 0, 1);

		break;

		case 'horizontal':
			if($style == 'normal')
			{
				$pdf->SetFont($this->pdfCfg['fontFamily'], '', $size);
				$pdf->setXY($x, $y);
				$pdf->Cell(45, ($size-2)/2, 'Versicherungsmakler Experten GmbH', 0, 1);
				$pdf->setX($x);
				$pdf->Cell(45, ($size-2)/2, 'Feursstr. 56 / RGB', 0, 1);
				$pdf->setX($x);
				$pdf->Cell(45, ($size-2)/2, '82140 Olching', 0);
				$pdf->setXY($x+51, $y);
				$pdf->Cell(55, ($size-2)/2, 'Tel.: 08142 - 651 39 28', 0);
				$pdf->setXY($x+51, $y+(($size-2)/2));
				$pdf->Cell(55, ($size-2)/2, $this->getEMailBySourcePage($sourcePage), 0);

			}

			if($style == 'wide')
			{
				$pdf->SetFont($this->pdfCfg['fontFamily'], 'B', $size);
				$pdf->Text($x+($size), $y, 'Versicherungsmakler Experten GmbH');
				$pdf->SetFont($this->pdfCfg['fontFamily'], '', $size);
				
				$pdf->setXY($x-12, $y+2);
				$pdf->MultiCell(0, ($size-2)/1.5, 'Feursstr. 56 / RGB 82140 Olching');

				$pdf->setXY($x+(32+$size), $y+2);
				$pdf->MultiCell(0, ($size-2)/1.5, 'Tel.: 08142 - 651 39 28', 0, 'L', 0);

				$pdf->setXY($x + $size, $y+($size/1.5));
				$pdf->MultiCell(0, ($size-2)/1.5, $this->getEMailBySourcePage($sourcePage), 0, 'L', 0);
			}	

		break;
	}
}

public function getCompanyFullStamp($x, $y, $pdf, $sourcePage, $size=8)
{
	$pdf->SetFont($this->pdfCfg['fontFamily'], '', $size);
	$pdf->setXY($x, $y);
	$pdf->MultiCell(0, ($size-2)/2, 'Versicherungsmakler Experten GmbH
Feursstr. 56 / RGB
82140 Olching
Tel: 08142 - 651 39 28
Fax: 08142 - 651 39 29
Mail: '.$this->getEMailBySourcePage($sourcePage).'
web: '.$this->getWebBySourcePage($sourcePage), 0, 'L', 0);
}

/* obsolete Functions */
public function companyStamp($x, $y, $pdf, $size=8)
{
	$this->getCompanyStamp($x, $y, $pdf, 'zzv-de', $size);
}
public function companyStampWide($x, $y, $pdf, $size=8)
{
	$this->getCompanyStamp($x, $y, $pdf, 'zzv-de', $size, 'horizontal', 'wide');
}
public function companyStampHorizontal($x, $y, $pdf, $size=8)
{
	$this->getCompanyStamp($x, $y, $pdf, 'zzv-de', $size, 'horizontal');
}

// -----------------------------------------------------------------------------
public function contractInfoTitle($i, $pdf, $title, $x=83, $y=2) {
	$pdf->SetFont($this->pdfCfg['fontFamily'], 'B', $this->pdfCfg['fontSize']+1, 0, 2);
	if($i==0) $this->pdfRect($x, $y, 54, $pdf, 4.0);
	$pdf->Text($x+1, $y+3, $title);
}
// -----------------------------------------------------------------------------
public function imgZZVLogo($i, $pdf, $x=152, $y=3, $width=50) {
	if ($i != 1) {
	    // color
	    $pdf->Image($this->path.'files/logo.png', $x, $y, $width);
	} else {
	    // black & white
	    $pdf->Image($this->path.'files/logo_sw.png', $x, $y, $width);
	}
}
// -----------------------------------------------------------------------------
public function imgLogo($i, $pdf, $logo, $x=16, $y=3, $width=50) {
	if ($i != 1) {
	    // color
	    $pdf->Image($this->path.'files/'. $logo .'.png', $x, $y, $width);
	} else {
	    // black & white
	    $pdf->Image($this->path.'files/'. $logo .'_bw.png', $x, $y, $width);
	}
}
// -----------------------------------------------------------------------------
public function getCompanyLogoFile($idt, $height = 100) {

	$c = new contract();

	if (empty($idt)) {
		throw new Exception("No IDT is set.");
	}
	$ident = $c->getInsuranceIdent($idt);

	if ( !isset($ident) ) {
		throw new Exception("No company logo assigned for IDT: ". $idt .". Please check that Insurance is set in `wzm_insurance` and that an ident is set.");
	}

	$path = $this->path.'files/'. $ident .'/logos/'. $height .'.png';

	if (file_exists($path)) {
		return $path;
	} else return $path .' does not exist';

}
// -----------------------------------------------------------------------------
/*
** $benefits = from -> to
*/
public function benefitsScale($benefits, $x, $y, $pdf, $idt=null) {

	if (is_array($benefits)) {
	 	$pdf->SetFont('PTSans-Regular', '', 10);

		if (end($benefits) === 0) {
			$pdf->Image($this->path.'files/icons/cross_i.png', $x, $y, 5);
			$x = $x+6;
			$y = $y+3;
			$pdf->Text($x, $y, 'keine Leistung');
		} else {
			$path = $this->path.'files/benefits-scale/'. end($benefits) .'.png';
			$pdf->Image($path, $x, $y, 15);
			$x = $x+17;
			$y = $y+3;
			$suffix = ' %';
			// Nach Wuerttembergische soll ein Plus Zeichen
			if (($idt == 25 || $idt == 26 || $idt == 27 || $idt == 28 || $idt == 29 || $idt == 30) && ($benefits[0] != 100)) {
				$suffix = ' % +';
			}
			if (count($benefits) === 1) {
				$pdf->Text($x, $y, $benefits[0] . $suffix);
			} else {
				$pdf->Text($x, $y, $benefits[0] .'-'. $benefits[1] . $suffix);
			}
		}
	}

}
// -----------------------------------------------------------------------------
/*
** $benefits = yes / no
*/
public function benefitsYesNo($bool, $x, $y, $pdf) {
	$path = $this->path.'files/icons/';
	if ($bool == 1) {
		$file = 'checked_i.png';
	} else {
		$file = 'checked_i.png';
	}
	$pdf->Image($path . $file, $x, $y, 5);
}
// -----------------------------------------------------------------------------
public function maxBenefits(array $maxBenefits, $x, $y, $pdf, $options = null) {
	if (null === $options) {
		$options = array(
			'head' => 'Leistungsbegrenzungen',
			'introduction' => 'Die maximalen Leistungen betragen in den ersten:',
			'timeIntervals' => array(12, 24, 36, 48),
			'suffix' => 'Monaten'
		);
	}
	$pdf->SetY($y+5);
	$pdf->SetX($x);
	$pdf->cMargin = 2;
	$pdf->SetTextColor(58, 105, 135);
	$pdf->SetFont('PTSans-Bold', '', 10);
	$pdf->MultiCell(90, 4, $options['head'], 0, 'L', 0);
	$pdf->Ln(3);
	$pdf->SetX($x);
	$pdf->SetTextColor(40);
	if ($options['introduction']) {
		$pdf->SetFont('PTSans-Regular', '', 10);
		$pdf->MultiCell(90, 4, $options['introduction'], 0, 'L', 0);
		$pdf->Ln(4);
	}

	$i = 0;
	foreach ($maxBenefits as $maxBenefit) {
		$pdf->cMargin = 2;
		$this->benefitsCell(array(
			'left' => $options['timeIntervals'][$i++] .' '.$options['suffix'],
			'right' => 'max. '. $maxBenefit .' €'),
			$x, $pdf, $i);
	}
	if ($options['outro']) {
		$pdf->Ln(4); $pdf->SetX($x);
		$pdf->SetFont('PTSans-Regular', '', 10);
		$pdf->MultiCell(90, 4, $options['outro'], 0, 'L', 0);
	}
}
// -----------------------------------------------------------------------------
public function benefitsCell(array $text, $x, $pdf, $i = 1) {
	$oddEven = ($i % 2 == 0) ? 0 : 1;
	$pdf->cMargin = 2;
	$pdf->SetFillColor(234, 247, 255);
	$pdf->SetTextColor(40);
	$pdf->SetX($x+2);
	$pdf->SetFont('PTSans-Regular', '', 10);
	$pdf->Cell(38, 6, $text['left'], 1, 0, 'L', ($i % 2 == 0) ? 0 : 1);
	$pdf->Cell(46, 6, $text['right'], 1, 2, 'L', ($i % 2 == 0) ? 0 : 1);
}
// -----------------------------------------------------------------------------
public function textField($text, $x, $y, $length, $pdf) {
	$pdf->SetTextColor(60);
	$pdf->SetDrawColor(120);
	$pdf->SetFont($pdfCfg['fontFamily'], '', 8, 0, 2);
	$pdf->Line($x, $y, $x + $length, $y);
	$pdf->Text($x, $y += 3, $text);
}
// -----------------------------------------------------------------------------
public function createAdviceProtocol($pid) {
	$crm = new crm();
	$wzm = new wzm();
	$person = new person();
	$contract = new contract();
	$p = $person->get($pid);
	$c = $contract->getByPid($pid);
	$t = $wzm->getTariff($c['idt']);
	$a = $crm->getAddressPrimary('pid', $pid);
	if(!empty($p['isFatherOfPid'])) {
		$p2 = $person->get($p['isFatherOfPid']);
	}
	if(empty($c)) {
		return false;
	}

	// refresh workspace temp files
	exec('sh files/beratungs-protokoll/refresh-temp.sh', $output);

	// ODT XML snippets
	$xmlBr = '<text:line-break/>';
	$txtYes = 'JA';
	$txtNo = 'NEIN';

	// read file
	$contentFile = 'files/beratungs-protokoll/temp/content.xml';
	$contentFileWrite = $contentFile;
	if(!$data = file_get_contents($contentFile)) {
		return false;
	}

	// ${person-address}
	$temp = $p['forename'].' '.$p['surname'].$xmlBr.
		$a['street'].$xmlBr.$a['postcode'].' '.$a['city'];
	if(!empty($p['isFatherOfPid'])) {
		$temp .= $xmlBr.L::_(316,null,true).': '.$p2['forename'].' '.
			$p2['surname'];
	}
	$data = str_replace('${person-address}', $temp, $data);

	// ${date-signup}
	$temp = substr($c['signupTime'], 0, 10);
	$temp = stringHelper::makeGermanDate($temp);
	$data = str_replace('${date-signup}', $temp, $data);

	// toothes and more
	$data = str_replace('${tooth1}', $c['tooth1'], $data);
	$data = str_replace('${tooth2}', $c['tooth2'], $data);

	$temp = ($c['insureTooth1']=='no')?$txtNo:$txtYes;
	$data = str_replace('${insure-tooth1}', $temp, $data);

	$temp = ($c['sickness']=='no')?$txtNo:$txtYes;
	$data = str_replace('${sickness}', $temp, $data);

	$temp = ($c['reha']=='no')?$txtNo:$txtYes;
	$data = str_replace('${reha}', $temp, $data);

	$temp = ($c['incare']=='no')?$txtNo:$txtYes;
	$data = str_replace('${incare}', $temp, $data);

	$temp = ($c['problem']==2)?$txtYes:$txtNo;
	$data = str_replace('${kfo}', $temp, $data);

	$temp = ($c['problem']==1)?$txtYes:$txtNo;
	$data = str_replace('${problem}', $temp, $data);

//Frage 8
	$temp = ($c['rwzz_top_1']=='no')?$txtNo:$txtYes;
	$data = str_replace('${rwzz_top_1}', $temp, $data);

//Frage 9
	$temp = ($c['rwzz_top_2']=='no')?$txtNo:$txtYes;
	$data = str_replace('${rwzz_top_2}', $temp, $data);

//Frage 10
	if($c['rwzz_top_2']=='yes') {
	$temp = ($c['aufbiss_paradontose']==1)?"Aufbissschiene":"Paradontosebehandlung";
	$data = str_replace('${aufbiss_paradontose}', $temp, $data);
					}
	else $data = str_replace('${aufbiss_paradontose}', '', $data);

	$data = str_replace('${tariff-name}', $t['name'], $data);
	$data = str_replace('${tariff-text}', $t['adviceText'], $data);

	$interestedParty = '';
	if($p['salutationLid']==9) {
		// man
		$interestedParty = L::_(371,null,true);
	}
	else {
		// woman
		$interestedParty = L::_(372,null,true);
	}

	$data = str_replace('${interested-party}', $interestedParty,
		 $data);

	$data = str_replace('${person-search}',
		utf8_encode(L::_(373+$c['isearch'], null, true)), $data);

	// customer follows recommendation and took tariff rank #1?
	$temp = ($c['rank']==1)?L::_(369,null,true):L::_(370,null,true);
	$temp = str_replace('#tariff#', $t['name'], $temp);
	$temp = $interestedParty.' '.$temp;
	$data = str_replace('${person-choice}', utf8_encode($temp), $data);

	// our recommendation
	if($c['adviceIdt']>0 && $c['adviceIdt'] != $c['idt']) {
		$t2 = $wzm->getTariff($c['adviceIdt']);
		$data = str_replace('${advice-tariff-name}',utf8_encode($t2['name']),
			$data);
		$data = str_replace('${advice-tariff-text}',
			utf8_encode($t2['adviceText']), $data);
	}
	else {
		$data = str_replace('${advice-tariff-name}',utf8_encode($t['name']),
			$data);
		$data = str_replace('${advice-tariff-text}', utf8_encode($t['adviceText']),
			$data);
	}

	// save content file
	if(!$data = file_put_contents($contentFileWrite, $data)) {
		return false;
	}

	// make odt file
	$filename = 'beratungsprotokoll-'.$c['idco'].'.odt';
	exec('sh files/beratungs-protokoll/make-file.sh '.$filename, $output);

	crmIntern::sendFileToBrowser(file_get_contents(
		'files/beratungs-protokoll/output/'.$filename), $filename, 'odt');
}
// -----------------------------------------------------------------------------
public function createCoverLetter($pid) {
	$crm = new crm();
	$wzm = new wzm();
	$person = new person();
	$contract = new contract();
	$crmIntern = new crmIntern();

	$p = $person->get($pid);
	$c = $contract->getByPid($pid);
	$a = $crm->getAddressPrimary('pid', $pid);
	if(!empty($p['isFatherOfPid'])) {
		$p2 = $person->get($p['isFatherOfPid']);
	}
	if(empty($c)) {
		return false;
	}

	// refresh workspace temp files
 	exec('sh files/anschreiben/refresh-temp.sh', $output);

	// ODT XML snippets
	$xmlBr = '<text:line-break/>';
	$txtYes = 'JA';
	$txtNo = 'NEIN';

	// read file
	$contentFile = 'files/anschreiben/temp/content.xml';
	$contentFileWrite = $contentFile;
	if(!$data = file_get_contents($contentFile)) {
		return false;
	}

	// ${person-address}
	$temp = $p['forename'].' '.$p['surname'].$xmlBr.
		$a['street'].$xmlBr.$a['postcode'].' '.$a['city'];
	$data = str_replace('${person-address}', utf8_encode($temp), $data);

	// ${salutation}
	$salutation= '';
	$personSalutation= '';
	if($p['salutationLid']==9) {
		// man
		$salutation = L::_(378, null, true).' '.$p['surname'];
		$personSalutation = L::_(380, null, true);
	}
	else {
		// woman
		$salutation = L::_(377, null, true).' '.$p['surname'];
		$personSalutation = L::_(381, null, true);
	}
	$data = str_replace('${salutation}', utf8_encode($salutation), $data);
	$data = str_replace('${person-salutation}',
		utf8_encode($personSalutation), $data);

	// email and web
	$data = str_replace('${company-email-address}', $crmIntern->getEMailBySourcePage($c['sourcePage']), $data);
	$data = str_replace('${company-web-address}', $crmIntern->getWebBySourcePage($c['sourcePage']), $data);

	// ${date}
	$data = str_replace('${date}', utf8_encode(date('d.m.Y')), $data);

	// save content file
	if(!$data = file_put_contents($contentFileWrite, $data)) {
		return false;
	}

	// make odt file
	$filename = 'anschreiben-'.$c['idco'].'.odt';
	exec('sh files/anschreiben/make-file.sh '.$filename, $output);

	crmIntern::sendFileToBrowser(file_get_contents(
		'files/anschreiben/output/'.$filename), $filename, 'odt');
}
// -----------------------------------------------------------------------------
public function sendMailContractArrived($pid) {
	$crm = new crm();
	$person = new person();
	$p = $person->get($pid);
	$c = $crm->getContactPrimary('pid', $pid);

	#if($c['email']=='radek.wirth@googlemail.com' || $c['email']=='maximilian.waizmann@gmx.de') {
		$file = 'templates/contractArrived.tpl';
	#} else
	#	$file = 'files/sendMailContractArrived.txt';

	if(!$message = file_get_contents($file)) {
		$err = array('ERR', 'File_not_found');
	}

	if(empty($c['email'])) {
		$err = array('ERR','Email_not_present');
	}

	$subject = 'Ihr Antrag auf Zahnzusatzversicherung ist bei uns eingegangen'
		.' - Zahnzusatzversicherung-Experten.de';

	$message = str_replace('{%AUSGEZEICHNET%}' , '<a href="https://www.ausgezeichnet.org/bewerten-zahnzusatzversicherung-experten.de-38UQJF" target="_new">Bitte bewerten Sie uns bei Ausgezeichnet.org!</a>', $message);
	if($p['salutationLid']==9) {
		$message = str_replace('{*ANREDE_NAME*}', 'Sehr geehrter Herr '.$p['surname'], $message);
	}
	else {
		$message = str_replace('{*ANREDE_NAME*}', 'Sehr geehrte Frau '.$p['surname'], $message);
	}

	$message = str_replace('{*SIGNATURE*}', project::gts('html'), $message);

	#if($c['email']=='radek.wirth@googlemail.com' || $c['email']=='maximilian.waizmann@gmx.de') {
		emailHelper::sendMail('"Zahnzusatzversicherung-Experten.de" <info@Zahnzusatzversicherung-Experten.de>', $c['email'], $subject, $message);
	#} else
	#	return $crm->sendMail($c['email'], $subject, $message);

}


// -----------------------------------------------------------------------------
public function genAnschreibenPDF($pid)
	{
		$pdf = new AlphaPDF('P');
		$pdfCfg = &$this->pdfCfg;
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
		$pdf->SetTextColor($pdfCfg['fontColor']);

		foreach($pid as $idp)
		{
			$pdf->AddPage('P');
			$data = $this->getReminderPdfData($idp['pid']);
			include ('contracts/erinnerungsschreiben.php.inc');
		}
		return $pdf;
	}


// -----------------------------------------------------------------------------
public function search($search) {
	$search = trim($search);

	// search for id?
	if($search[0]=='#') {
		$contract = new contract();
		// grep id from #+1 to end
		$c = $contract->get(substr($search, strlen($search)*(-1)+1));
		if(empty($c)) {
			return null;
		}
		return array($c['pid']);
	}
	else {
		$keywordsTemp = explode(' ', $search);
		$keywords = array();
		if(count($keywordsTemp))
		foreach($keywordsTemp as $word) {
			$word = trim($word);
			if(!empty($word)) {
				$keywords[] = $word;
			}
		}

		if(!count($keywords)) {
			return null;
		} 

		if(count($keywords) == 1) 
		{
			$contract = new contract();
			$c = $contract->getPidByContractNumber($keywords[0]);

			if(is_array($c) && count($c))
			{
				return $c;
			}		
		}	

		$person = new person();
		return $person->search($keywords);
	}
}
// -----------------------------------------------------------------------------
} // end class