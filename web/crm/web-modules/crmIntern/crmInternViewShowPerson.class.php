<?php

class crmInternViewShowPerson
	extends crmInternView
{

function __construct($dataArray=null) {
	parent::__construct();
	$this->dataArray = $dataArray;
}

function processData() {

$pid = $this->dataArray['pid'];


$replCode = '';
$possibilites = '';
$groupName = '';

$label = new label();
$crm = new crm();
$db = project::getDb();


$person = new person();
$per = $person->get($pid);
if(!empty($per['isFatherOfPid'])) {
	$per2 = $person->get($per['isFatherOfPid']);
	$per['isFatherOfPid'] = urlHelper::makeLink('crmIntern', 'showPerson',
		$per2['surname'].', '.$per2['forename'],
		array('pid' => $per['isFatherOfPid']));
}
else {
	$fatherPid = $person->getFatherPid($pid);
}

$left = '';
unset($per['cid']);
unset($per['pid']);
unset($per['uid']);

$per['salutationLid'] = $label->getLabelName($per['salutationLid']);
$per['titleLid'] = $label->getLabelName($per['titleLid']);
$per['familyStatusLid'] = $label->getLabelName($per['familyStatusLid']);
$per['jobPositionId'] = $crm->getJobPositionName($per['jobPositionId']);
$per['birthdate'] = stringHelper::makeGermanDate($per['birthdate']);
$per['insuredSince'] = substr(stringHelper::makeGermanDate(
	$per['insuredSince']), -4);
$per['nationCode'] = $crm->getCountry($per['nationCode']);
$per['insuranceStatusLid'] = $label->getLabelName($per['insuranceStatusLid']);

if($per['insuredSince'] == '0000' || $per['insuredSince'] == '0001') {
	unset($per['insuredSince']);
}

if($per['insuranceStatusLid'] == '0') {
	unset($per['insuranceStatusLid']);
}

if($per['birthdate'] == '0000-00-00') {
	unset($per['birthdate']);
}

$addrp = $crm->getAddressPrimary('pid', $pid, true);
$addrp['stateId'] = $crm->getStateName($addrp['stateId']);
$addrp['country'] = $crm->getCountry($addrp['country']);
$contp = $crm->getContactPrimary('pid', $pid, true);





if(!empty($contp['homepage'])) {
	$contp['homepage'] = '<a href="go.php?www='.$contp['homepage'].'" target="_blank">'.
		$contp['homepage'].'</a>';
}

// assemble contract
if(empty($fatherPid)) {
$contract = new contract();
$wzm = new wzm();
$contractData = $contract->getByPid($pid);
$tariffData = $wzm->getTariff($contractData['idt']);

$akt_idco = $contractData['idco'];
	if(file_exists('pdf/Antrag - '.$akt_idco.'.pdf'))
	{
	$file_link_beg = '<a href=\'pdf/Antrag - '.$akt_idco.'.pdf\'>';
	$file_link_end = '</a>';
	}


// Falls Beratungsgespräch gewünscht...
if($contractData['recallIsDesired']==1)
{
	$conttemp['recallisDesired'] = 'Ja, bitte';
	$contp = array_merge($conttemp, $contp);
}


// allgemeiner Teil
$contractData['idt'] = $tariffData['name'];
$contractData['birthdate'] = stringHelper::makeGermanDate(
	$contractData['birthdate']);
$contractData['gender'] = L::_($contractData['gender']);
$contractData['wishedBegin'] = stringHelper::makeGermanDate(
	$contractData['wishedBegin']);
$contractData['price'] = number_format($contractData['price'], 2, ',',
	'.').' &euro;';

if(!isset($contractData['insurance'])) {
	unset($contractData['insurance']);
} else {
	switch($contractData['insurance']) {
		case 'heil': $contractData['insurance'] = 'Freie Heilfürsorge';
		break;
		case 'pkv': $contractData['insurance'] = 'Private Krankenversicherung';
		break;
		default: $contractData['insurance'] = 'Gesetzliche Krankenversicherung';
		}
	}


if(!isset($contractData['problem'])) {
	unset($contractData['problem']);
} else {
	$contractData['problem'] = ($contractData['problem']==2)?L::_(199):
		(($contractData['problem']==1)?L::_(200):L::_(342));
}


/* Vorversicherung wechseln? */
	if(!isset($contractData['previousTarifInsured']) ) {
		unset($contractData['previousTarifInsured']);
	} else {
		$contractData['previousTarifInsured'] = $contractData['previousTarifInsured']==1?L::_(625):L::_(624);
	}

	if(!isset($contractData['previousTarif']) ) {
		unset($contractData['previousTarif']);
	} else {
		$contractData['previousTarif'] = L::_(626 + (int)($contractData['previousTarifInsured']));
	}

	if($contractData['changeInsurance'] != 'yes')
	{
		unset($contractData['changeInsurance']);
		unset($contractData['previousTarif']);
		unset($contractData['previousTarifInsured']);
	}



$statusDates = array('48' => $contractData['dateSent'],
	'49' => $contractData['dateRec'],
	'50' => $contractData['datePolice']);

// Angebot
$rs =  $label->getLabels('sentType','lid');
while($row = $db->fetchPDO($rs))
	{
		$statusAngebot[] = $row;
	}
$contractData['angebot'] = $file_link_beg.$statusAngebot[$contractData['angebot']-1]['name'].$file_link_end;
// Angebot Ende

$idco = $contractData['idco'];
$currentStatus = $contractData['contractStatusLid'];

// add "where do you know us from?"
$lead = $wzm->getLead($contractData['pid']);
if(!empty($lead)) {
	$contractData['lead'] = $label->getLabelName($lead['leadLid']);
	if(!empty($lead['comment'])) {
		$contractData['lead'] .= ' | '.$this->html($lead['comment']);
	}
}

#print_r($contractData);
	// delete entries which were not viewed
	foreach($contractData as $id => $value)
	{
		if(!isset($value) || empty($value) || $value == -1)
		{
			$t = array('tooth1', 'tooth3', 'tooth4', 'tooth5');
			if(!in_array($id, $t))
				unset($contractData[$id]);
			elseif(!isset($value) || $value == -1)
			{
				unset($contractData[$id]);
			}

		} elseif($value == 'yes' || $value == 'no')
		{
			if($value == 'yes')
			{
				$contractData[$id] = 'Ja';
				if($id == 'periodontitisCured')
					$contractData[$id] = 'ausgeheilt';
			}
			if($value == 'no')
			{
				$contractData[$id] = 'Nein';
				if($id == 'periodontitisCured')
					$contractData[$id] = 'chronisch';
			}
		}
		
	}

	// fix periodentitisCured
	if($contractData['periodontitis'] == 'Nein' || !isset($contractData['periodontitis']))
		unset($contractData['periodontitisCured']);


if(!isset($contractData['dentistVisit']) || $contractData['dentistVisit']=='0000-00-00')
	unset($contractData['dentistVisit']);
if(!isset($contractData['mailSentDate']) || $contractData['mailSentDate']=='0000-00-00')
	unset($contractData['mailSentDate']);
if(!isset($contractData['contractMailSent']) || $contractData['contractMailSent']=='0000-00-00')
	unset($contractData['contractMailSent']);

unset($contractData['pid']);
unset($contractData['bonusbase']);
unset($contractData['bonus']);
unset($contractData['contractStatusLid']);
unset($contractData['dateSent']);
unset($contractData['dateRec']);
unset($contractData['datePolice']);
unset($contractData['dateEmail']);
unset($contractData['toothFixed']);
unset($contractData['tooth2']);
unset($contractData['recallIsDesired']);



if($contractData['contractType']=='adult') {
	unset($contractData['orthodontics']);
	if($contractData['ambulant']=='no') {
		unset($contractData['sickness']);
		unset($contractData['reha']);
	}
	
} elseif($contractData['contractType']=='kids') {
	#unset($contractData['tooth1']);
	#unset($contractData['tooth2']);
	#unset($contractData['tooth3']);
	#unset($contractData['tooth4']);
	#unset($contractData['tooth5']);
	#unset($contractData['periodontitis']);
	#unset($contractData['periodontitisCured']);
	#unset($contractData['biteSplint']);
	#unset($contractData['docControl']);
	#unset($contractData['ambulant']);
}


// assemble support msg
$supportLeft = $supportRight = array();
$rs = $wzm->getSupportMessages($pid);
while($row = $db->fetchPDO($rs)) {
	$supportLeft[] = $row['datetime'];
	$supportRight[] = $row['message'];
}

} // end if is father

// assemble left part 1
if(!empty($fatherPid)) {
	$per3 = $person->get($fatherPid);
	$per['isChildOfPid'] = urlHelper::makeLink('crmIntern', 'showPerson',
		$per3['surname'].', '.$per3['forename'],
		array('pid' => $fatherPid));
	unset($per['isFatherOfPid']);
}
else {
	$left .= $this->geth2($this->html($tariffData['name']).' | '
		.L::_(357).' #'.$contractData['idco']);
}

// assemble left part 2

//Name
$left .= $this->geth1($this->html($per['surname'].', '.$per['forename']));


//Kennzeichnen bei AddNotes und sourcePage
if($contractData['sourcePage'])
	{
		$hlp_var = "[".$contractData['sourcePage']."]";

		if(isset($contractData['landingpage']) && $contractData['landingpage'] == 'yes' && (isset($contractData['sourcePage']) && $contractData['sourcePage'] == 'zzv'))
		{
			$hlp_var .= ' Achtung: Tarif ohne Gesundeitsangaben angefordert';
		}

		if($contractData['sourcePage'] == 'mobile') $hlp_var .= " - LEER drucken!";
	}

if($contractData['addNotes'])
	{
		$hlp_var .= "[".$contractData['sourcePage']."] ".$contractData['addNotes'];
	}

$left .= $this->getalerth2($this->html($hlp_var));
$left .= crmInternView::getImportantNotesMessage('pid', $pid);

$browserData['browserName'] = $contractData['browserName']; unset($contractData['browserName']);
$browserData['browserVersion'] = $contractData['browserVersion']; unset($contractData['browserVersion']);
$browserData['browserPlatform'] = $contractData['browserPlatform']; unset($contractData['browserPlatform']);
$browserData['browserDevice'] = $contractData['browserDevice']; unset($contractData['browserDevice']);
$browserData['browserDeviceDetail'] = $contractData['browserDeviceDetail']; unset($contractData['browserDeviceDetail']);
$browserData['browserAgent'] = $contractData['browserAgent']; unset($contractData['browserAgent']);

#var_dump($contractData);

if(!empty($supportLeft)) {
	$left .= viewHelper::genEasyTable(L::_(358), $supportLeft, $supportRight, 3);
}
$left .= viewHelper::genEasyTableWithTextids(L::_(59), $per, 3, '', true);
$left .= viewHelper::genEasyTableWithTextids(L::_(31), $addrp, 3, '', true);
if(isset($contp)) {
	$left .= viewHelper::genEasyTableWithTextids(L::_(36), $contp, 3, '', true);
}
$left .= viewHelper::genEasyTableWithTextids(L::_(324), $contractData, 3, '');
$left .= viewHelper::genEasyTableWithTextids(L::_(573), $browserData, 3, '');

// assemble right
$right  = crmInternView::getSinglePersonMenu($pid, $idco, $contractData);
$right .= crmInternView::getAssignments('pid', $pid);
$right .= crmInternView::getStatus('contractStatus', $currentStatus, $idco, $pid, $statusDates);
$right .= crmInternView::getNotes('pid', $pid);
$right .= crmInternView::getPossibleContract($idco);
$right .= crmInternView::getInsuranceLinks($pid, $idco);
$right .= crmInternView::DuplicateDetails($pid);
$replCode .= crmInternView::couple($left, $right);


// finish
$this->replace('content', $replCode);

}

} // end class

?>

<script>
    <?php require_once("js/lazyInsuranceData.js");?>
</script>


