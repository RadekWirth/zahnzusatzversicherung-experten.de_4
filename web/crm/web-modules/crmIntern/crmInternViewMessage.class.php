<?php

class crmInternViewMessage
	extends crmInternView
{

function __construct($dataArray=null) {
	parent::__construct();
  	$this->dataArray = $dataArray;
}

public function processData() {

$replCode = $this->geth1($this->dataArray['caption']);
$replCode .= $this->getp($this->dataArray['text']);
$replCode .= $this->dataArray['link'];

// finish
$this->replace('content', $replCode);

}

} // end class

?>