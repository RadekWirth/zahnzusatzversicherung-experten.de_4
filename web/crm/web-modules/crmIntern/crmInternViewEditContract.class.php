<?php

class crmInternViewEditContract
	extends crmInternView
{

function __construct($dataArray=null) {
	parent::__construct();
	$this->dataArray = $dataArray;
}

function processData() {
// get data for prefilling fields
$data = (empty($this->dataArray['data']))?array():$this->dataArray['data'];

$replCode = $this->geth1(L::_(364));
if(isset($this->dataArray['errors']))
	$replCode .= $this->getErrorHtml($this->dataArray['errors']);

$idt = $this->dataArray['data']['contract']['idt'];

// set defaults or prefilling with calc inputs
$toothSelectParams = '';
$problemParams = 'hideEntry';
$aufbissParams = 'hideEntry';


$toothOption = array('-1' => 'kein Wert');
for($i=0; $i<29; $i++) { $toothOption[$i] = $i;}


/*
	prefilling answers - if no data given
		no data set - do not fill values
*/

if(!isset($data['contract']['tooth1']) || $data['contract']['tooth1'] === null)
	$data['contract']['tooth1'] = 'kein Wert';

if($data['contract']['existDentures']=='yes') {
	$params['tooth3']=$params['tooth4']=$params['tooth5'] = null;
}

if($data['contract']['periodontitis']=='yes') {
	$params['periodontitisCured']='';
} else {
	$params['periodontitisCured']='hideEntry';
}
if($data['contract']['ambulant']=='yes') {
	$params['sickness']='';
	$params['reha']='';
} else {
	$params['sickness']='hideEntry';
	$params['reha']='hideEntry';
}




$fields = array();

$fields[] =
	array(
	'label' => L::_(223),
	'name' => 'wishedBegin',
	'type' => 'text',
	'value' => $data['contract']['wishedBegin'],
	'size' => 'medium-big'
	);

$fields[] =
	array(
	'label' => L::_(122),
	'name' => 'price',
	'type' => 'text',
	'value' => $data['contract']['price'],
	'size' => 'medium-big'
	);

$fields[] =
	array(
	'label' => L::_(671),
	'name' => 'contractNumber',
	'type' => 'text',
	'value' => $data['contract']['contractNumber'],
	'size' => 'medium-big'
	);


if($data['type'] == 'teeth')
{
$fields = array();

// Frage 1
// Anzahl fehlender - nicht ersetzter - Zähne (ohne Weisheitszähne oder vollständigen Lückenschluss)?
$fields[] =
			array(
			'label' => L::_(493),
			'name' => 'tooth1',
			'type' => 'select',
			'options' => array('-1' => 'kein Wert', '0' => '0' ,'1' => 1,'2' => 2,'3' => 3,'4' => 4,'5' => 5,'6' => 6,'7' => 7,'8' => 8,9=>'9 und mehr'),
			'selected' => $data['contract']['tooth1'],
			'size' => 'medium-big'
			);

// Frage 2
// Fehlende Zähne versichern?
/*
$fields[] =
			array(
			'label' => L::_(202),
			'name' => 'insureTooth1',
			'type' => 'radio',
			'options' => array(null => 'kein Wert',
				'no' => L::_(99),
				'yes' => L::_(100)),
			'selected' => $data['contract']['insureTooth1'],
			'class' => 'radioShortItem',
			'entryId' => 'insureTooth1',
			'params' => 'hideEntry'
			);
*/
// Frage 3
// Sind Zähne mit Zahnersatz ersetzt (z.B. Kronen, Brücken, Implantate, Prothesen o.Ä.)?
$fields[] =
			array(
			'label' => L::_(495),
			'name' => 'existDentures',
			'type' => 'radio',
			'options' => array(
				'no' => L::_(99),
				'yes' => L::_(100)),
			'selected' => $data['contract']['existDentures'],
			'onclick' => "javascript:if(this.value=='yes') showMultiple('tooth3,tooth4,tooth5'); "
				." else hideMultiple('tooth3,tooth4,tooth5'); "
			);
// Frage 4
// Wie viele Ihrer Zähne sind durch "herausnehmbaren" Zahnersatz ersetzt (Prothesen, Gebiss)?
$fields[] =
			array(
			'label' => L::_(496),
			'name' => 'tooth3',
			'type' => 'select',
			'options' => $toothOption,
			'selected' => $data['contract']['tooth3'],
			'entryId' => 'tooth3',
			'params' => $params['tooth3'],
			'size' => 'medium-big'
			);

// Frage 5
// Wie viele Ihrer Zähne sind durch festsitzenden Zahnersatz ersetzt (z.B. Kronen, Brücken, Implantate o.Ä.)?
$fields[] =
			array(
			'label' => L::_(498),
			'name' => 'tooth4',
			'type' => 'select',
			'options' => $toothOption,
			'selected' => $data['contract']['tooth4'],
			'entryId' => 'tooth4',
			'params' => $params['tooth4'],
			'size' => 'medium-big'
			);

// Frage 6
// Wieviel Zahnersatz ist älter als 10 Jahre?
$fields[] =
			array(
			'label' => L::_(500),
			'name' => 'tooth5',
			'type' => 'select',
			'options' => array('-1' => 'kein Wert', '2' => '0 - 2', '5' => '3 - 5', '6' => '6', '7' => '7', '8' => '8', '9' => '9 oder mehr'),
			'selected' => $data['contract']['tooth5'],
			'entryId' => 'tooth5',
			'params' => $params['tooth5'],
			'size' => 'medium-big'
			);

// Frage 7
// Sind Sie momentan in laufender zahnärztlicher Behandlung (z.B. Zahnersatz, Füllungen, Parodontosebehandlung o.Ä.) oder sind zahnärztliche Behandlungen notwendig, angeraten oder geplant worden?
$fields[] =
			array(
			'label' => L::_(502),
			'name' => 'incare',
			'type' => 'radio',
			'options' => array(
				'no' => L::_(99),
				'yes' => L::_(100)),
			'selected' => $data['contract']['incare']
   			);

// Frage 8
// Fand bei Ihnen in der Vergangenheit schon einmal einer Parodontose- bzw. Parodontitisbehandlung statt bzw. wurde eine Parodontitis diagnostiziert?
$fields[] =
			array(
			'label' => L::_(504),
			'name' => 'periodontitis',
			'type' => 'radio',
			'options' => array(
				'no' => L::_(99),
				'yes' => L::_(100)),
			'selected' => $data['contract']['periodontitis'],
			'onclick' => "javascript:if(this.value=='yes') show('periodontitisCured');"
				." else hide('periodontitisCured');"
			);

// Frage 9
// Ist die Parodontitis-Erkrankung derzeit vollständig ausgeheilt oder handelt es sich um einen chronischen Verlauf?
$fields[] =
			array(
			'label' => L::_(506),
			'name' => 'periodontitisCured',
			'type' => 'select',
			'options' => array(null => 'kein Wert', 'no' => 'chronische Parodontitis', 'yes' => 'vollst&auml;ndig ausgeheilt'),
			'selected' => $data['contract']['periodontitisCured'],
			'entryId' => 'periodontitisCured',
			'params' => $params['periodontitisCured'],
			'size' => 'medium-big'
			);

// Frage 10
// Tragen Sie nachts beim Schlafen regelmäßig eine Aufbissschiene (sog. Knirscherschiene) oder wurde Ihnen in den letzten 3 Jahren eine solche Schiene vom Zahnarzt angeraten oder angefertigt?
$fields[] =
			array(
			'label' => L::_(508),
			'name' => 'biteSplint',
			'type' => 'radio',
			'options' => array(
				'no' => L::_(99),
				'yes' => L::_(100)),
			'selected' => $data['contract']['biteSplint']
			);

// Frage 11
// Hatten sie in den letzten 24 Monaten eine Zahnarztkontrolle?
$fields[] =
			array(
			'label' => L::_(538),
			'name' => 'docControl',
			'type' => 'radio',
			'options' => array(
				'no' => L::_(99),
				'yes' => L::_(100)),
			'selected' => $data['contract']['docControl']
			);

// Checkbox - Signal Iduna Fragen
// Soll unser Vergleichsrechner auch Tarife einbeziehen, die neben Zahnleistungen auch ambulante Ergänzungsleistungen beinhalten (z.B. Brille, Heilpraktiker, o.Ä.)?
$fields[] =
			array(
			'label' => L::_(523),
			'name' => 'ambulant',
			'type' => 'radio',
			'options' => array(
				'no' => L::_(99),
				'yes' => L::_(100)),
			'selected' => $data['contract']['ambulant'],
			'onclick' => "javascript:if(this.value=='yes') showMultiple('reha,sickness'); "
				." else hideMultiple('reha,sickness'); "
			);

// Frage 12
// Waren Sie während der letzten 5 Jahre auf einer Kur- oder Rehamaßnahme (nur relevant für Tarife mit ambulanten Zusatzleistungen)?
$fields[] =
			array(
			'label' => L::_(510),
			'name' => 'reha',
			'type' => 'radio',
			'options' => array(
				'no' => L::_(99),
				'yes' => L::_(100)),
			'selected' => $data['contract']['reha'],
			'entryId' => 'reha',
			'params' => $params['reha']
			);

// Frage 13
// Leiden Sie an einer schweren Erkrankung (siehe Liste) oder waren während der letzten 5 Jahre Behandlungen aufgrund einer der aufgeführten Erkrankungen erforderlich (nur relevant für Tarife mit ambulanten Zusatzleistungen)?
$fields[] =
			array(
			'label' => L::_(512),
			'name' => 'sickness',
			'type' => 'radio',
			'options' => array(
				'no' => L::_(99),
				'yes' => L::_(100)),
			'selected' => $data['contract']['sickness'],
			'entryId' => 'sickness',
			'params' => $params['sickness']
			);
}


// create field sets
$fieldsets =
array(
	array(
		'legend' => L::_(365),
		'group' => 'contract',
		'params' => 'numbering',
		'fields' => $fields
	),
	array(
		'params' => 'disableGroups',
		'fields' => array(
			array(
			'value' => L::_(43),
			'name' => 'submit',
			'type' => 'submit',
			'class' => 'submit3',
			'params' => 'hideLabel'
			)
		)
	)
);

$formEngine = new coreFormEngine();
$replCode .= $formEngine->requestForm($fieldsets, 'crmIntern', 'editContract',
	array('idco' => $data['idco']));


// finish
$this->replace('content', $replCode);

}

} // end class

?>
