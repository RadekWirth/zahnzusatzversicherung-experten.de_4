<?php

class crmInternViewListShowAllCompanies
	extends myInternView
{

function __construct($dataArray=null) {
	parent::__construct();
	$this->dataArray = $dataArray;
}

function processData() {
// get data for prefilling fields
$data = (empty($this->dataArray['data']))?array():$this->dataArray['data'];

$replCode = '';
$possibilites = '';


$label = new label();
$crm = new crm();
$company = new company();
$person = new person();

// companies
$replCode .= $this->geth1(L::_(52));
$replCode .= "\n".'<table class="dataTable">';
$companyRs = $company->getAllRs();
$i=0;
while($com = $companyRs->fetch()) {
	$addrp = $crm->getAddressPrimary('cid', $com['cid']);
	$contp = $crm->getContactPrimary('cid', $com['cid']);
	$tableRowData = array('name' => $com['name'], 'street' => $addrp['street'],
		'postcode' => $addrp['postcode'], 'city' => $addrp['city'],
		'phone' => $contp['phone'], 'fax' => $contp['fax'],
		'email' => $contp['email'], 'homepage' => $contp['homepage']);
	$tableRowData['firstcell'] = urlHelper::makeLink('crmIntern',
		'showCompany', $com['name'], array('cid' => $com['cid']));
	$replCode .= $this->getTableRow(&$tableRowData);
}
$replCode .= "\n".'</table>';



// finish
$this->replace('content', $replCode);

}


private function getTableRow($data) {
	return "\n".'
		<tr>
		<td class="name">'.$data['firstcell'].'</td>
		<td class="street">'.$this->html($data['street']).'</td>
		<td class="postcode">'.$this->html($data['postcode']).'</td>
		<td class="city">'.$this->html($data['city']).'</td>
		<td class="phone">'.$this->html($data['phone']).'</td>
		<td class="fax">'.$this->html($data['fax']).'</td>
		<td class="email">'.$this->html($data['email']).'</td>
		<td class="homepage">'.$this->html($data['homepage']).'</td>
		</tr>
		<!-- end row -->
		';

	return "\n".'
		<div class="row">
		<div class="name">'.$this->html($data['name']).'</div>
		<div class="street">'.$this->html($data['street']).'</div>
		<div class="postcode">'.$this->html($data['postcode']).'</div>
		<div class="city">'.$this->html($data['city']).'</div>
		<div class="phone">'.$this->html($data['phone']).'</div>
		<div class="fax">'.$this->html($data['fax']).'</div>
		<div class="email">'.$this->html($data['email']).'</div>
		<div class="homepage">'.$this->html($data['homepage']).'</div>
		<div class="clearBoth"></div>
		</div>
		<!-- end row -->
		';
}

} // end class

?>