<?php

class crmInternViewListShowAssignments
	extends crmInternView
{

private $counter;

function __construct($dataArray=null) {
	parent::__construct();
	$this->dataArray = $dataArray;
}

function processData() {
// get data for prefilling fields
$data = (empty($this->dataArray['data']))?array():$this->dataArray['data'];

$replCode = '';
$possibilites = '';
$firstLetter = null;
if(!empty($this->dataArray['firstLetter'])) {
	$firstLetter = $this->dataArray['firstLetter'];
}

$label = new label();
$crm = new crm();
$company = new company();
$person = new person();

$replCode .= $this->geth1(L::_(115));

// COMPANIES
$replCode .= $this->geth2(L::_(52));
$replCode .= "\n".'<div class="dataTable">';

$companyRs = $company->getAllWithAssignmentsRs();
$this->counter = 0;
while($com = $companyRs->fetch()) {
	$addrp = $crm->getAddressPrimary('cid', $com['cid']);
	$contp = $crm->getContactPrimary('cid', $com['cid']);
	$tableRowData = array('name' => $com['name'], 'street' => $addrp['street'],
		'postcode' => $addrp['postcode'], 'city' => $addrp['city'],
		'phone' => $contp['phone'], 'fax' => $contp['fax'],
		'email' => $contp['email'], 'homepage' => $contp['homepage'],
		'group' => $crm->getGroupName('cid', $com['cid']),
		'cid' => $com['cid']);
	$tableRowData['firstcell'] = urlHelper::makeLink('crmIntern',
		'showCompany', $this->html($com['name']), array('cid' => $com['cid']));

 	$tableRowData['importantNotice'] = $crm->hasImportantNotes('cid',
 		$com['cid']);

	$tableRowData['persons'] = $person->getByCompanyFullArray($com['cid']);
	$replCode .= $this->getTableRow(&$tableRowData);
	$this->counter++;
}
// $replCode .= "\n".'</table>';
$replCode .= "\n".'</div>';

// no results
if($this->counter == 0) {
	$replCode .= $this->getp(L::_(69));
}


// PERSONS
$replCode .= $this->geth2(L::_(53));
$replCode .= "\n".'<div class="dataTable">';

$personRs = $person->getAllWithAssignmentsRs();
$this->counter = 0;
while($per = $personRs->fetch()) {
	$addrp = $crm->getAddressPrimary('pid', $per['pid']);
	$contp = $crm->getContactPrimary('pid', $per['pid']);
	$tableRowData = array('name' => $per['surname'].', '.$per['forename'],
		'street' => $addrp['street'],
		'postcode' => $addrp['postcode'], 'city' => $addrp['city'],
		'phone' => $contp['phone'], 'fax' => $contp['fax'],
		'email' => $contp['email'], 'homepage' => $contp['homepage'],
		'group' => $crm->getGroupName('pid', $per['cid']));

	$tableRowData['firstcell'] = urlHelper::makeLink('crmIntern',
		'showPerson', $this->html($tableRowData['name']),
		array('pid' => $per['pid']));

 	$tableRowData['importantNotice'] = $crm->hasImportantNotes('pid',
 		$per['pid']);

	$replCode .= $this->getTableRow(&$tableRowData);
	$this->counter++;
}
// $replCode .= "\n".'</table>';
$replCode .= "\n".'</div>';

// no results
if($this->counter == 0) {
	$replCode .= $this->getp(L::_(69));
}

// finish
$this->replace('content', $replCode);

}


private function getTableRow($data) {
	$rowClass = ($this->counter % 2)?'rowOdd':'rowEven';

	return "\n".'
	<div class="'.$rowClass.'" >
	<div class="name"><span class="company">'.$data['firstcell'].'</span></div>
	<div class="importantNotice"><span class="company">'.(($data['importantNotice'])?'<img src="img/notice.png" alt="notice" />':'&nbsp;').'</span></div>
	<div class="assignments"><span class="company"><img src="img/icons/copy.gif" alt="assignments" /></span></div>
	<div class="street"><span class="company">'.$this->html($data['street'],1).'</span></div>
	<div class="postcode"><span class="company">'.$this->html($data['postcode'],1).'</span></div>
	<div class="city"><span class="company">'.$this->html($data['city'],1).'</span></div>
	<div class="phone"><span class="company">'.$this->html($data['phone'],1).'</span></div>
	<div class="group"><span class="company">'.$this->html($data['group'],1).'</span></div>
	<div class="clearBoth"></div>
	</div>
	<!-- end row -->
	';
}


} // end class

?>