<?php
/* VARS */

$tariffName = $data['tariffName'];
$idt = $data['idt'];
$icon = array(
	'check' => $path.'/files/icons/checked_blue.png'
);
$logo = $path.'/files/logo.png';
$lh = 3.6; // line height
$lhb = $lh*2;

$pdf->AddFont('PTSans-Regular', '','PTSans-Regular.php');
$pdf->AddFont('PTSans-Bold', '','PTSans-Bold.php');
$pdf->SetFont('PTSans-Bold', '', 17);
$pdf->addPage('P');

$pdf->SetDrawColor(200);
$pdf->RoundedRect(16, 40, 110, 50, 4, '1234');

$pdf->Image($this->getCompanyLogoFile($idt), 16, 5, 0, 22);
$pdf->Image($logo, 140, 5, 60);

$pdf->SetTextColor(58, 105, 135);
$pdf->Text(22, 48, html_entity_decode($tariffName));


$pdf->SetFont('PTSans-Regular', '', 10);
$y = 59.5;
foreach($data['highlights'] as $highlight) {
	$pdf->Image($icon['check'], 22, $y - 4.5, 5);
	$pdf->Text(30, $y, html_entity_decode($highlight));
	$y = $y + 7.5;
}

// Persönliches Angebot für ...
$pdf->SetFont('PTSans-Bold', '', 12);
$pdf->Text(135, 48, 'Persönliches Angebot für');
$pdf->SetFont('PTSans-Regular', '', 10);
$pdf->SetTextColor(40);  
$pdf->Text(135, 56, $data['namec']);
$pdf->Text(135, 61, 'geb. '. $data['birthdate']);
$pdf->Text(135, 66, 'Versicherungsbeginn: '. stringHelper::makeDateFromSignUp($data['wishedBegin']));

$pdf->Text(135, 75, 'Monatlicher Beitrag: '. $data['price'] .' €');


$pdf->SetY(260);
$pdf->SetX(16);
$pdf->Write($lh, 'Sie haben Fragen? Dann rufen Sie uns doch unter folgender Nummer zurück: 08142 - 651 39 28. Auf der Rückseite finden Sie wertvolle Hinweise zum Ausfüllen des Antrages!');





