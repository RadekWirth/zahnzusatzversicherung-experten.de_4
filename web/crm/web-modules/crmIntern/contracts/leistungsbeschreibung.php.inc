<?php

	$pagecount = $pdf->setSourceFile($path.'crm/files/benefits-long/local.pdf');

	// Restliche Seiten des Antrages hinzufügen
	if($pagecount>2) {
	for ($var = 1; $var<=$pagecount; $var++)
		{
			$tplidx = $pdf->importPage($var, '/MediaBox');
			$pdf->addPage('P');
			$pdf->useTemplate($tplidx, 0, 0.1, 209.9);
		}
	}

	if($complete)
	{
		if(($pdf->PageNo() % 2) == 1)
			$pdf->addPage('P');
	}

?>