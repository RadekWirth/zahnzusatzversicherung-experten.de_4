<?php
// File for generating Contract PDF for Signal Iduna Kompakt (Start & Plus & Top)

        /*
         * Constants
         */
        // SQUARE XY
        define('X_Y', 1.5);

        // RECT Y
        define('Y', 2.7);

        $data['person']['age'] = actionHelper::getAge($data['contractComplete']['birthdate']);


            $pagecount = $pdf->setSourceFile($path.'files/signal-iduna/antrag/2023/5da5bf53d72fb85a691e6ccf6d9e45ec6544578c.pdf');

        $tplidx = $pdf->importPage(1, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
        $pdf->SetMargins(0, 0, 0);

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);

        if($i==0) $this->pdfRect(75.0, 3.0, 70, $pdf);
        $pdf->Text(75.0, 7.0, $titles[$i]);

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-1, 0, 2);

        // -> Seite 2
        $pdf->Text(90, 14.0, "Versicherungsmakler Experten GmbH");

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-3, 0, 2);

        //mark section
        //Selbstst�ndiger/Firma/Verein
        
        if($i==0) {
            $this->pdfRect(28.5, 64, X_Y, $pdf, X_Y);
            $this->pdfRect(47, 64, X_Y, $pdf, X_Y);
            $this->pdfRect(62.6, 64, X_Y, $pdf, X_Y);
            $this->pdfRect(74.9, 64, X_Y, $pdf, X_Y);
            $this->pdfRect(88, 64, X_Y, $pdf, X_Y);
        }
        

        //freiwillig versichert?
        if($i==0) {
             $this->pdfRect(72.2, 75.9, X_Y, $pdf, X_Y);
             $this->pdfRect(90.9, 75.9, X_Y, $pdf, X_Y);
             $this->pdfRect(72.2, 78.4, X_Y, $pdf, X_Y);
             $this->pdfRect(72.2, 80.6, X_Y, $pdf, X_Y);
        }

        //versichert seit...
        //if($i==0) $this->pdfRect(162, 71.2, 27.5, $pdf, Y);


        //mark section end
        $pdf->Text(158, 65.3, 'X'); // Beihilfe
        $pdf->Text(185.4, 65.3, 'X'); // Heilfuersorge

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);    

        if($data['gender']=='male') {
            $pdf->Text(16.4, 47.8, 'X');
        } else {
            $pdf->Text(16.4, 51.0, 'X');
        }

        if($data['widowed']) {
            $pdf->Text(16.4, 63.8, 'X');
        } else if($data['married']) {
            $pdf->Text(16.4, 60.2, 'X');
        } else {
            $pdf->Text(16.4, 57.4, 'X');
        }

        // Bereits Kunde?
        $pdf->Text(146.0, 37.6, 'X');
        
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
        $pdf->Text(30, 48.0, $data['name']);
        $pdf->Text(143.8, 48.0, $data['birthdate']);

        if($data['nation']=='DEU') {
            $pdf->Text(189.5, 44.6, 'x');
        } elseif(empty($data['nation'])) {
            $this->pdfRect(167.5, 44.6, 30, $pdf, 3.5);
        } else {
            $pdf->Text(168, 48.0, $data['nation']);
        }
 

        $pdf->Text(29.0, 55.0, $data['street']);

        $pdf->Text(116, 55.0, $data['postcode'].' '.$data['city']);

        if( ! empty($data['job'])) {
            $pdf->Text(29.0, 61.2, $data['job']);
        } else {
           if($i==0) 
                $this->pdfRect(28.5, 58.0, 30, $pdf, 4);
        }

        //Telefonnummer
        if(!empty($data['phone']) || $data['phone']=='')
        {
            if($i==0) $this->pdfRect(15.8, 86, 39, $pdf, 3);
        } else
            $pdf->Text(16.5, 88.4, $data['phone']);

        //eMail
        if(!empty($data['email']) || $data['email']=='')
        {
            if($i==0) $this->pdfRect(102, 86, 53, $pdf, 3);
        } else 
            $pdf->Text(105.0, 88.4, $data['email'].'email');

        $pdf->Text(162, 88.4, $data['mobile']);

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);    


        if(!empty($data['insurance'])) {
            $pdf->Text(29.0, 82.5, $data['insurance']);
        } else {
            if($i==0) $this->pdfRect(28.5, 79.9, 40, $pdf, 3.0);
        }

        // Insured since
        if($i==0) $this->pdfRect(132, 80.2, 20, $pdf, 3.0);






    // 3 Beitragszahlung

        //mark section

        //Zahlungsweise
        if($i==0) {
            $this->pdfRect(36.0, 116.1, X_Y, $pdf, X_Y);
            $this->pdfRect(50.3, 116.1, X_Y, $pdf, X_Y);
            $this->pdfRect(66.7, 116.1, X_Y, $pdf, X_Y);
            $this->pdfRect(83.6, 116.1, X_Y, $pdf, X_Y);
        }

        //Konto
        if($i==0) {
            $this->pdfRect(17.5, 146.4, 100, $pdf, Y);
            $this->pdfRect(156.0, 146.4, 38, $pdf, Y);
            $this->pdfRect(17.5, 154.7, 89, $pdf, Y);
            $this->pdfRect(117.0, 154.7, 24, $pdf, Y);
            $this->pdfRect(149.5, 154.7, 47, $pdf, Y);
        }
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
        if($i==0) $pdf->Text(149, 157, 'X');

        // check for child
        if($data['name2b'] != $data['name']) {
            // setze marker bei Antragsteller / VN
            $pdf->Text(116.1, 41.2, 'X');            
            $data['personInsured']['age'] = actionHelper::getAge($data['personInsured']['birthdate']);

        //mark section

	     //Beziehung zum Antragsteller
	     if($i==0) {
                $this->pdfRect(28.8, 195.5, X_Y, $pdf, X_Y);
                $this->pdfRect(45.1, 195.5, X_Y, $pdf, X_Y);
                $this->pdfRect(65.9, 195.5, X_Y, $pdf, X_Y);
                $this->pdfRect(86.8, 195.5, X_Y, $pdf, X_Y);
                $this->pdfRect(103.8, 195.5, X_Y, $pdf, X_Y);
                $this->pdfRect(122.2, 195.5, X_Y, $pdf, X_Y);

                $this->pdfRect(28.8, 198.3, X_Y, $pdf, X_Y);
                $this->pdfRect(45.1, 198.3, X_Y, $pdf, X_Y);
                $this->pdfRect(65.9, 198.3, X_Y, $pdf, X_Y);
                $this->pdfRect(86.8, 198.3, X_Y, $pdf, X_Y);
                $this->pdfRect(103.8, 198.3, X_Y, $pdf, X_Y);

                $this->pdfRect(28.8, 201.8, X_Y, $pdf, X_Y);
                $this->pdfRect(100.8, 201.4, 30, $pdf, 3);

		  $this->pdfRect(28.8, 205.4, X_Y, $pdf, X_Y);
		  $this->pdfRect(48.8, 205.4, 50, $pdf, 3);

	     }


        //Selbstst�ndiger/Firma/Verein

            if($i==0) {
                $this->pdfRect(28.9, 210.7, X_Y, $pdf, X_Y);
                $this->pdfRect(47.4, 210.7, X_Y, $pdf, X_Y);
                $this->pdfRect(62.8, 210.7, X_Y, $pdf, X_Y);
                $this->pdfRect(75.0, 210.7, X_Y, $pdf, X_Y);
                $this->pdfRect(88, 210.7, X_Y, $pdf, X_Y);
            }
            

            //freiwillig versichert?
            if($i==0) {
                $this->pdfRect(72.2, 222.5, X_Y, $pdf, X_Y);
                $this->pdfRect(91.1, 222.5, X_Y, $pdf, X_Y);
                $this->pdfRect(72.2, 225.0, X_Y, $pdf, X_Y);
                $this->pdfRect(72.2, 227.5, X_Y, $pdf, X_Y);
            }
                        
                //mark section end


                
                $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);

                $pdf->Text(29.0, 179, $data['name2b']);
                $pdf->Text(29.0, 185, $data['street'].', '.$data['postcode'].' '.$data['city']);
                $pdf->Text(148.0, 179, $data['birthdate2']);
                if($data['gender2']=='male')    {
                    $pdf->Text(16.5, 182.6, 'X');
                    $pdf->Text(16.5, 204.8, 'X');
                } else {
                    $pdf->Text(16.5, 185.9, 'X');
                    $pdf->Text(16.5, 208.1, 'X');
                }

                if($data['widowed2']) {
                    $pdf->Text(16.5, 198.5, 'X');
                } else if($data['married2']) {
                    $pdf->Text(16.5, 195.2, 'X');
                } else {
                    $pdf->Text(16.5, 192.0, 'X');
                }

                $pdf->Text(158.0, 212.0, 'x'); // Beihilfe
                $pdf->Text(185.5, 212.0, 'x'); // Heilfuersorge
                
                if($data['nation2']=='DEU') {
                    $pdf->Text(189.5, 175.5, 'x');
                } elseif(empty($data['nation2'])) {
                    $this->pdfRect(167.5, 175.8, 30, $pdf, 3.5);
                } else {
                    $pdf->Text(168, 179, $data['nation2']);
                }

                if(!empty($data['job2'])) {
                    $pdf->Text(29.0, 190.5, $data['job2']);
                } else {
                    if($i==0) $this->pdfRect(28.5, 188, 80, $pdf, 3);
                }

                if(!empty($data['insurance2'])) {
                    $pdf->Text(29.0, 228.8, $data['insurance2']);
                } else {
                    if($i==0) $this->pdfRect(28.5, 226.0, 40, $pdf, 3);
                }

                // Insured since
                #if($i==0) $this->pdfRect(180, 222.2, 20, $pdf, 3.5);

                #if($i==0) $this->pdfRect(163, 186.0, 26, $pdf, 3);
                $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        }
        // end child
        else {
            // setze marker bei Antragsteller / VN
            $pdf->Text(70.1, 41.2, 'X');
        }


// SEITE 2
        $tplidx = $pdf->importPage(2, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);


    // 4 Versicherungsbeginn und beantragter Versicherungsschutz
        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);


        if($data['name2b'] == $data['name']) {
            $this->pdfClean(49.3, 67.8, 10, $pdf, 255, 255, 255, 3.7);
            $pdf->Text(50.4, 70.9, $data['price']);
        }
        else {
            $this->pdfClean(99.9, 67.8, 10, $pdf, 255, 255, 255, 3.7);
            $pdf->Text(101.0, 70.9, $data['price']);
        }
        $this->pdfClean(177.8, 67.8, 10, $pdf, 255, 255, 255, 3.7);
        $pdf->Text(178.8, 70.9, $data['price']);

        $pdf->Text(170, 29.8, '01.'. $data['begin']);


        // choose tariff
        if($pdfTemplate == project::gti('sigidu-komplus')) {
            if($data['name2b'] == $data['name']) {
                $pdf->Text(18.7, 48.7, 'X');
                $pdf->Text(18.7, 53.6, 'X');
                $pdf->Text(18.7, 63.7, 'X');
            } else {
                $pdf->Text(68.6, 48.7, 'X');
                $pdf->Text(68.6, 53.6, 'X');
                $pdf->Text(68.6, 63.7, 'X');
            }
        }
        else if($pdfTemplate == project::gti('sigidu-komstart')){
            if($data['name2b'] == $data['name'])
            {
                $pdf->Text(18.7, 48.7, 'X');
                $pdf->Text(18.7, 63.7, 'X');
            } else {
                $pdf->Text(68.6, 48.7, 'X');
                $pdf->Text(68.6, 63.7, 'X');
            }
        }
        else if($pdfTemplate == project::gti('sigidu-komtop')){
            if($data['name2b'] == $data['name'])
            {
                $pdf->Text(18.7, 48.7, 'X');
                $pdf->Text(18.7, 58.7, 'X');
                $pdf->Text(18.7, 63.7, 'X');
            } else {
                $pdf->Text(68.6, 48.7, 'X');
                $pdf->Text(68.6, 58.7, 'X');
                $pdf->Text(68.6, 63.7, 'X');
            }
        }

        $this->getCompanyStamp(100.0, 5, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize'], 'horizontal', 'wide');

        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        $pdf->Text(33.0, 7.1, $data['name']);

        $pdf->Text(16.3, 144.3, 'X');

        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+20, 0, 2);
        $pdf->SetTextColor(35,122,78);

           $pdf->Text(9, 118.4, '!');
           $pdf->Text(203.6, 118.4, '!');

        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
        $pdf->SetTextColor(0,0,0);


        // Fragen an die zu versichernde Person
        
        //ja / nein
        if($data['name2b'] != $data['name'])
        {  //mark section
            if($i==0) {
                    $this->pdfRect(171.6, 111.9, X_Y, $pdf, X_Y);
                    $this->pdfRect(179.8, 111.9, X_Y, $pdf, X_Y);

                    $this->pdfRect(171.6, 114.7, X_Y, $pdf, X_Y);
                    $this->pdfRect(179.8, 114.7, X_Y, $pdf, X_Y);

                    $this->pdfRect(171.6, 119.9, X_Y, $pdf, X_Y);
                    $this->pdfRect(179.8, 119.9, X_Y, $pdf, X_Y);
            }
        } else {
                //mark section
                if($i==0) {
                    $this->pdfRect(156.6, 111.9, X_Y, $pdf, X_Y);
                    $this->pdfRect(164.7, 111.9, X_Y, $pdf, X_Y);

                    $this->pdfRect(156.6, 114.7, X_Y, $pdf, X_Y);
                    $this->pdfRect(164.7, 114.7, X_Y, $pdf, X_Y);

                    $this->pdfRect(156.6, 119.9, X_Y, $pdf, X_Y);
                    $this->pdfRect(164.7, 119.9, X_Y, $pdf, X_Y);
                }
        }
        

// SEITE 3
        $tplidx = $pdf->importPage(3, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        $this->getCompanyStamp(100.0, 5, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize'], 'horizontal', 'wide');

        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);

        $pdf->Text(33, 7.8, $data['name']);

        // Unterschriften
        
        // Antragsteller
        if($i==0) $this->pdfRect(18.5, 169.5, 28.0, $pdf, Y);
        if($i==0) $this->pdfRect(52.0, 169.5, 44.0, $pdf, Y);
        $pdf->Text(52, 172.2, 'X');



        // Unterschrift der mitzuversichernden Person ab 16 Jahre
        if(isset($data['personInsured']['age']))
        {
            if($data['personInsured']['age'] >= 16)
            {
                if($i==0) $this->pdfRect(100.5, 169.5, 44.0, $pdf, Y);
                $pdf->Text(102, 172.2, 'X');
            }
        }


        // Antragsteller minderj�hrig oder versicherte Person minderj�hrig
        if($data['person']['age'] <= 18 || ($data['personInsured']['age'] <=18) && (isset($data['personInsured']['age'])))
        {
            if($i==0) $this->pdfRect(152, 169.5, 44.0, $pdf, Y);
            $pdf->Text(153, 172.2, 'X');


            if($data['person']['age'] <= 18)
            {
                $this->pdfRect(113.0, 196.3, 82.0, $pdf, 3.2);
                $pdf->Text(115.0, 197, 'X');
            }
        }

        // Empfangsbest�tigung
        if($i==0) {
            $this->pdfRect(17.5, 194.7, 82.0, $pdf, 3.2);
            $pdf->Text(18.5, 197, 'X');
        }

        // Reset
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
        // Makler
        $pdf->Text(148.4, 210.2, 'X');
        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
        // Vmtl-Nr
        $pdf->Text(18.5, 218, '226 / 1500');


// SEITE 4
        $tplidx = $pdf->importPage(4, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);


// SEITE 5
        if($data['contractComplete']['reha'] == 'yes')
        {
            $pdf->setSourceFile($path.'/files/1200802_kurtagegeld.pdf');
            $tplidx = $pdf->importPage(1, '/MediaBox');

            $pdf->addPage('P');
            $pdf->useTemplate($tplidx, 0, 10, 200);

            $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);
            $pdf->Text(20, 69, $data['signupDate']);
            $pdf->Text(123, 69, $data['namec']);
            if($data['name2b'] != $data['name']) {
                $pdf->Text(20, 88, $data['name2']);
                $pdf->Text(123, 88, $data['birthdate2']);
            }
            else {
                $pdf->Text(20, 88, $data['namec']);
                $pdf->Text(123, 88, $data['birthdate']);
            }

            if($i==0) $this->pdfRect(16.5, 262, 38, $pdf, 6);
            if($i==0) $this->pdfRect(59.2, 262, 110, $pdf, 6);

            $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+2, 0, 2);
            $pdf->Text(18, 267, 'X');
            $pdf->Text(61, 267, 'X');


         $pdf->addPage('P');
        }

/*
        $pdf->Text(15, 160, 'Hinweis :');
        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
        if($data['name2b'] != $data['name']) {
            $pdf->Text(15, 164, 'Anzahl ben�tigter Unterschriften bei Versicherung von Kindern: 6 St�ck (2 x links Vorderseite, 2 x rechts Vorderseite,');
            $pdf->Text(15, 167, '1 x links auf der R�ckseite, 1 x rechts auf der R�ckseite = 6 Unterschriften!!!)');
        } else {
            $pdf->Text(15, 164, 'Anzahl ben�tigter Unterschriften bei Versicherung Erwachsener: 3 St�ck (2 x links Vorderseite, 1 x links auf der R�ckseite = 3 Unterschriften)');
        }
*/