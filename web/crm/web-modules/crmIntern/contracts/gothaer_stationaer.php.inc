<?php


        $pagecount = $pdf->setSourceFile($path.'/files/gothaer/Stationaer/gothaer.Mediclinic.Antrag.2020.pdf');

        $tplidx = $pdf->importPage(1, '/MediaBox');

        $pdf->addPage('P');  // idx, x, y, w, h
        $pdf->useTemplate($tplidx, 0, 0, 210);
        $pdf->SetMargins(0, 0, 0);

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

	 $this->pdfClean(0.5, 16.5, 24.0, $pdf, 255, 255, 255, 8, 'F');
	 $this->pdfClean(133, 3, 40.0, $pdf, 255, 255, 255, 7, 'F');
	 $this->pdfClean(101, 19.5, 3.0, $pdf, 255, 255, 255, 3, 'F');


#print_r($data);

	#$this->pdfRect(75.0, 3.0, 70, $pdf);

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);
	 $pdf->Text(102.0, 22.5, '36484');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

	 $pdf->Text(26.0, 30.0, $data['nameb']);
	 $pdf->Text(26.0, 37.4, $data['street']);
	 $pdf->Text(26.0, 45.0, $data['postcode']);
	 $pdf->Text(56.0, 45.0, $data['city']);
	 $pdf->Text(121.0, 45.0, $data['email']);

	 $pdf->Text(26.0, 57.0, $data['birthdate']);

	 if(!isset($data['nation']))
	 {
		 if($i == 0) $this->pdfRect(68.5, 53.5, 20, $pdf);
	 } else {
		 $pdf->Text(70.0, 57.0, $data['nation']);
	 }
	

	 if(!isset($data['job']))
	 {
		 if($i == 0) $this->pdfRect(94.5, 53.5, 30, $pdf);
	 } else {
		 $pdf->Text(96.0, 57.0, $data['job']);
	 }



        if($data['gender']=='male') {
            $pdf->Text(181.0, 30.4, 'X');
        } else {
            $pdf->Text(181.0, 33.9, 'X');
        }

	 if($data['person']['pid'] == $data['personInsured']['pid'])
	 {
		$pdf->Text(181.0, 45.4, 'X');
	 } else {
	 	$pdf->Text(26.0, 66.8, $data['name2']);	
	 	$pdf->Text(26.0, 74.4, $data['birthdate2']);
	 	$pdf->Text(70.0, 74.4, $data['nation2']);
	 	$pdf->Text(96.0, 74.4, $data['job2']);

        	if($data['gender']=='male') {
        	    $pdf->Text(174.7, 66.4, 'X');
        	} else {
        	    $pdf->Text(174.7, 69.8, 'X');
        	}

	 }

	#Zahlweise
	$this->pdfRect(25.0, 104.2, 3, $pdf, 3);
	$this->pdfRect(63.0, 104.2, 3, $pdf, 3);
	$this->pdfRect(84.8, 104.2, 3, $pdf, 3);
	$this->pdfRect(107.2, 104.2, 3, $pdf, 3);
	#SEPA
	 $pdf->Text(107.7, 111.4, 'X');



	$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+3, 0, 2);
	$pdf->Text(40.0, 173.0, $data['begin'][0].' '.$data['begin'][1]);
	$pdf->Text(66.0, 173.4, $data['begin'][5].' '.$data['begin'][6]);
	$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

	/* Ausnahme - dieser Zusatztarif muss manuell berechnet werden, da dieser nicht gespeichert wird */
	$wzm = new wzm();
	$price = $wzm->getBonusBaseWithDate($pdfTemplate, $personInsured['birthdate'], $data['wishedBegin']);

	if($price['activeBonusBase'])
	{
	 $pdf->Text(31.0, 186.4, 'Gothaer Mediclinic S Premium');
	 $pdf->Text(111.3, 186.4, 'EUR '.stringHelper::makeGermanFloat($price['activeBonusBase']));
	 $pdf->Text(180.0, 186.4, 'EUR '.stringHelper::makeGermanFloat($price['activeBonusBase']));
	 $pdf->Text(180.0, 210.4, 'EUR '.stringHelper::makeGermanFloat($price['activeBonusBase']));
	 $pdf->Text(180.0, 246.4, 'EUR '.stringHelper::makeGermanFloat($price['activeBonusBase']));
	}

        $tplidx = $pdf->importPage(2, '/MediaBox');

        $pdf->addPage('P');  // idx, x, y, w, h
        $pdf->useTemplate($tplidx, 0, 0, 210);
        $pdf->SetMargins(0, 0, 0);


	$this->pdfRect(81.0, 36.2, 10, $pdf, 3);
	$this->pdfRect(106.2, 36.2, 14, $pdf, 3);

	$this->pdfRect(182.4, 42.6, 2.5, $pdf, 2.5);
	$this->pdfRect(188.9, 42.6, 2.5, $pdf, 2.5);

	$this->pdfRect(182.4, 48.6, 2.5, $pdf, 2.5);
	$this->pdfRect(188.9, 48.6, 2.5, $pdf, 2.5);

	$this->pdfRect(182.4, 54.8, 2.5, $pdf, 2.5);
	$this->pdfRect(188.9, 54.8, 2.5, $pdf, 2.5);

	$this->pdfRect(182.4, 60.9, 2.5, $pdf, 2.5);
	$this->pdfRect(188.9, 60.9, 2.5, $pdf, 2.5);

	$this->pdfRect(182.4, 67.1, 2.5, $pdf, 2.5);
	$this->pdfRect(188.9, 67.1, 2.5, $pdf, 2.5);

	$this->pdfRect(182.4, 73.2, 2.5, $pdf, 2.5);
	$this->pdfRect(188.9, 73.2, 2.5, $pdf, 2.5);

	$this->pdfRect(182.4, 84.2, 2.5, $pdf, 2.5);
	$this->pdfRect(188.9, 84.2, 2.5, $pdf, 2.5);

	$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+13, 0, 2);
	$this->pdfRect(18.9, 109.2, 4, $pdf, 8);
		$pdf->Text(20.0, 116.0, '!');
	$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);


	#Derzeitiger Versicherungsschutz
	$this->pdfRect(39.9, 227.1, 2.5, $pdf, 2.5);
	$this->pdfRect(59.7, 227.1, 2.5, $pdf, 2.5);
	$this->pdfRect(80.9, 227.1, 2.5, $pdf, 2.5);
	$this->pdfRect(95.9, 226.7, 35, $pdf, 2.5);
	$this->pdfRect(156.9, 226.7, 14, $pdf, 2.5);
	$this->pdfRect(182.9, 226.7, 14, $pdf, 2.5);

	#Beendigung Ablehnung in den letzten 5 Jahren
	$this->pdfRect(35.9, 284.8, 2.5, $pdf, 2.5);
	$this->pdfRect(42.9, 284.8, 16, $pdf, 2.5);
	$this->pdfRect(72.7, 284.8, 2.5, $pdf, 2.5);
	$this->pdfRect(79.7, 284.8, 16, $pdf, 2.5);
	$this->pdfRect(116.9, 284.8, 40, $pdf, 2.5);


        $tplidx = $pdf->importPage(3, '/MediaBox');

        $pdf->addPage('P');  // idx, x, y, w, h
        $pdf->useTemplate($tplidx, 0, 0, 210);
        $pdf->SetMargins(0, 0, 0);        

	$this->pdfRect(195.7, 13.9, 2.5, $pdf, 2.5);
	$this->pdfRect(185.9, 13.9, 2.5, $pdf, 2.5);
	$pdf->Text(90.6, 41.0, 'X');

	$this->pdfRect(24.9, 120.0, 20 , $pdf, 2.5);
	$this->pdfRect(74.9, 120.0, 20, $pdf, 2.5);
	$this->pdfRect(140.0, 120.0, 20 , $pdf, 2.5);

	$pdf->Text(25.6, 137.0, 'Versicherungsmakler Experten GmbH, Feursstr. 56 / RGB, 82140 Olching');
	$pdf->Text(139.6, 137.0, '36484');

        $tplidx = $pdf->importPage(4, '/MediaBox');

        $pdf->addPage('P');  // idx, x, y, w, h
        $pdf->useTemplate($tplidx, 0, 0, 210);
        $pdf->SetMargins(0, 0, 0);

	$pdf->Text(25.4, 100.7, 'X');

	 $this->pdfClean(24.5, 104.0, 45.0, $pdf, 255, 255, 255, 8, 'F');	
	 $this->pdfClean(36, 280.5, 120.0, $pdf, 255, 255, 255, 8, 'F');
	 $this->pdfRect(24.9, 106.0, 26 , $pdf);	

	 $pdf->Text(26.0, 122.0, $data['nameb']);
	 $pdf->Text(26.0, 132.0, $data['street']);
	 $pdf->Text(47.0, 142.6, $data['postcode']);
	 $pdf->Text(76.0, 142.6, $data['city']);
	 $pdf->Text(26.0, 142.6, $data['country']);

	$this->pdfRect(24.9, 150, 100 , $pdf, 3.5);

	$this->pdfRect(24.9, 161.1, 40 , $pdf, 3.5);
	$this->pdfRect(88.9, 161.1, 70 , $pdf, 3.5);

	$this->pdfRect(63.9, 177.4, 20 , $pdf, 3.5);
	$this->pdfRect(24.9, 177.4, 30 , $pdf, 3.5);
	$this->pdfRect(104.9, 177.4, 30 , $pdf, 3.5);


        $tplidx = $pdf->importPage(5, '/MediaBox');

        $pdf->addPage('P');  // idx, x, y, w, h
        $pdf->useTemplate($tplidx, 0, 0, 210);
        $pdf->SetMargins(0, 0, 0);                


        $tplidx = $pdf->importPage(6, '/MediaBox');

        $pdf->addPage('P');  // idx, x, y, w, h
        $pdf->useTemplate($tplidx, 0, 0, 210);
        $pdf->SetMargins(0, 0, 0);  


        $tplidx = $pdf->importPage(7, '/MediaBox');

        $pdf->addPage('P');  // idx, x, y, w, h
        $pdf->useTemplate($tplidx, 0, 0, 210);
        $pdf->SetMargins(0, 0, 0);  


        $tplidx = $pdf->importPage(8, '/MediaBox');

        $pdf->addPage('P');  // idx, x, y, w, h
        $pdf->useTemplate($tplidx, 0, 0, 210);
        $pdf->SetMargins(0, 0, 0);  
