<?
// File for generating Contract PDF for Bayrische V.I.P. dental Prestige

    define('CROWNS', $data['contractComplete']['tooth4'] ? $data['contractComplete']['tooth4'] : null);
    define('PROSTHESES', $data['contractComplete']['tooth3'] ? $data['contractComplete']['tooth3'] : null);
    define('MISSING_TOOTH', $data['t1'] ? $data['t1']  : null);
    define('AGE_PERSONINSURED', actionHelper::getAge($data['personInsured']['birthdate']));

    if($i==0) {
        $pagecount = $pdf->setSourceFile($path.'/files/bayerische-dental-prestige/die-bayerische-vip-dental-prestige-2016.pdf');
    } else {
        $pagecount = $pdf->setSourceFile($path.'/files/bayerische-dental-prestige/die-bayerische-vip-dental-prestige-2016.pdf');
    }

/**
 * Page 1
 */
    $tplidx = $pdf->importPage(1, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 5, 16, 200);
    $pdf->SetMargins(0, 0, 0);

    $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);

    // gelbe markierungen nur auf r�ckantwort drucken!
    if($i==0) $this->pdfRect(72, 0.5, 68, $pdf);
    $pdf->Text(72, 4.5, $titles[$i]);

    // Logo "DieBayerische"
    if ($i==0) {
        $pdf->Image($path.'/files/logos/die-bayerische/logo_diebayerische.png', 5, 1, 50);
        $pdf->SetTextColor(91, 103, 127);
    } else {
        $pdf->Image($path.'/files/logos/die-bayerische/logo_diebayerische_bw.png', 5, 1, 50);
        $pdf->SetTextColor(91, 103, 127);
    }

    $pdf->AddFont('helvetica','','helvetica.php');
    $pdf->SetFont('helvetica','',14);
    $pdf->Text(7, 17, 'VIP dental prestige');
    $pdf->SetTextColor(0, 0, 0);

    $this->getCompanyStamp(76.0, 9.5, $pdf, $data['contractComplete']['sourcePage'], 7, 'horizontal', 'wide');

    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

    // 1.AV Nummer
    //$pdf->Text(28.7, 28.6, '1  6   3   6  0  2');

    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);

    // Antrag auf Tarif V.I.P. dental
    $pdf->Text(36.8, 30, "x");
    $bg = explode(".", $data['begin']);
    $pdf->Text(106, 29.9, $bg[0][0]);
    $pdf->Text(110.5, 29.9, $bg[0][1]);
    $pdf->Text(115, 29.9, $bg[1][0]);
    $pdf->Text(119.2, 29.9, $bg[1][1]);
    $pdf->Text(123.5, 29.9, $bg[1][2]);
    $pdf->Text(127.6, 29.9, $bg[1][3]);

    // Antragsteller / Versicherungsnehmer
    ($data['gender']=='male') ? $pdf->Text(14.5, 57.4, 'x') : $pdf->Text(24.7, 57.4, 'x');
    $bd = explode(".", $data['birthdate']);
    $pdf->Text(149.6, 57.4, $bd[0][1]);
    $pdf->Text(145.5, 57.4, $bd[0][0]);
    $pdf->Text(153.3, 57.4, $bd[1][0]);
    $pdf->Text(157.3, 57.4, $bd[1][1]);
    $pdf->Text(162.4, 57.4, $bd[2][0]);
    $pdf->Text(166.3, 57.4, $bd[2][1]);
    $pdf->Text(170.7, 57.4, $bd[2][2]);
    $pdf->Text(174.8, 57.4, $bd[2][3]);

    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);

    $pdf->Text(16, 66.5, $data['namec']);
    $pdf->Text(16, 75.2, $data['street']);
    $pdf->Text(16, 83.7, $data['postcode']);
    $pdf->Text(38, 83.7, $data['city']);

    if(!empty($data['phone'])) {
        $pdf->Text(145, 66.5, $data['phone']);
    } else {
        if($i==0) $this->pdfRect(145, 63.8, 50, $pdf, 3);
    }

    if(!empty($data['email'])) {
        if(strlen($data['email']) > 25) {
            $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
        }
        $pdf->Text(145, 84.4, $data['email']);
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);
    } else {
        if($i==0) $this->pdfRect(145.8, 81.5, 50, $pdf, 3);
    }

    // Zu versichernde Person
    $pdf->Text(52, 97.3, $data['personInsured']['surname']);
    $pdf->Text(52, 105.7, $data['personInsured']['forename']);

    $pdf->Text(145, 97.7, $data['birthdate2']);
    ($data['gender2']=='male') ? $pdf->Text(161.5, 104.6, 'x') : $pdf->Text(178.2, 104.6, 'x');

    // Kreuz bei VIP dental prestige
    $pdf->Text(24.4, 126, 'x');

    $pdf->Text(179, 129, $data['price']);

    // Zahlungsweise
    if($i==0) $this->pdfRect(92.2, 124.4, 1.7, $pdf, 1.7);
    if($i==0) $this->pdfRect(111.9, 124.4, 1.7, $pdf, 1.7);
    if($i==0) $this->pdfRect(92.2, 128.6, 1.7, $pdf, 1.7);
    if($i==0) $this->pdfRect(111.9, 128.6, 1.7, $pdf, 1.7);

    // Gesundheitsfragen

    // 1. Fehlen Zähne?
    if($i==0) $this->pdfRect(101, 172, 3, $pdf, 3);
    if($i==0) $this->pdfRect(109.4, 172, 3, $pdf, 3);
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+3, 0, 2);
    if ($i==0 && MISSING_TOOTH !== null) {
        $this->pdfRect(163, 172, 21, $pdf, 3);
        $pdf->Text(101.7, 174.5, 'x');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
        $pdf->Text(172.5, 174.5, MISSING_TOOTH);
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);
    } else {
        $pdf->Text(110, 174.5, 'x');
    }

    // 2. Teilprothese?
    if($i==0) $this->pdfRect(101, 190.8, 3, $pdf, 3);
    if($i==0) $this->pdfRect(109.4, 190.8, 3, $pdf, 3);
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+3, 0, 2);
    if ($i==0 && PROSTHESES !== null) {
        $pdf->Text(101.7, 193.5, 'x');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
    } else {
        $pdf->Text(110, 193.5, 'x');
    }

    // 3. Derzeit Zahnbehandlungen?
    if($i==0) $this->pdfRect(101, 200.8, 3, $pdf, 3);
    if($i==0) $this->pdfRect(109.4, 200.8, 3, $pdf, 3);
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+3, 0, 2);
    if ($data['contractComplete']['incare'] == 'yes') {
        $this->pdfRect(120, 208.3, 75.3, $pdf, 12.5);
        $pdf->Text(101.7, 203.5, 'x');
    } else {
        $pdf->Text(110, 203.5, 'x');
    }

    // 4. Erkrankungen des Zahnhalteapparates?
    if($i==0) $this->pdfRect(101, 226.6, 3, $pdf, 3);
    if($i==0) $this->pdfRect(109.4, 226.6, 3, $pdf, 3);

    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+3, 0, 2);
    if ($data['contractComplete']['periodontitis'] == 'yes') {
        $this->pdfRect(120, 234, 75.3, $pdf, 31.3);
        $pdf->Text(101.7, 228.9, 'x');
    } else {
        $pdf->Text(110, 228.9, 'x');
    }

/**
 * Page 2
 */
    $tplidx = $pdf->importPage(2, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
    if ($data['series'] == 1)
    {
        $pdf->Text(9.9, 174.9, 'x');
    }
    elseif ($data['series'] == 2)
    {
        $pdf->Text(35.3, 174.9, 'x');
    }

    // Unterschrift Antragsteller und gegebenenfalls Gesetzlicher Vertreter
    if($i==0) $this->pdfRect(72, 177, 60, $pdf, 6);

    $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+5, 0, 2);
    $pdf->Text(72, 181.4, 'x');
    if (AGE_PERSONINSURED < 18) {
        if($i==0) $this->pdfRect(138, 177, 60, $pdf, 6);
        if ($i == 0) $this->pdfRect(140, 213, 58, $pdf, 4);
    }

    // SEPA
    if ($i == 0) $this->pdfRect(20, 250.2, 88, $pdf, 4);
    if ($i == 0) $this->pdfRect(11, 262, 114, $pdf, 4);
    if ($i == 0) $this->pdfRect(11, 275, 48, $pdf, 4);
    if ($i == 0) $this->pdfRect(63, 275, 55, $pdf, 4);
    if ($i == 0) $this->pdfRect(132, 256, 65, $pdf, 22);

    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
/**
 * Page 3
 */
    $tplidx = $pdf->importPage(3, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

    //if ($i == 0) $this->pdfRect(11, 49.8, 48, $pdf, 4);
    //if ($i == 0) $this->pdfRect(63, 49.8, 55, $pdf, 4);

    if ($i == 0) $this->pdfRect(11, 111, 48, $pdf, 4);
    if ($i == 0) $this->pdfRect(63, 111, 55, $pdf, 4);

    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);
    if ($i == 0) $pdf->Text(10, 83.5, 'x');
    if ($i == 0) $pdf->Text(10, 100.5, 'x');

    // Einwilligung in Datenverwendung
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
    
    if($i==0) $this->pdfRect(14.6, 235.6, 2, $pdf, 2);
    if($i==0) $this->pdfRect(14.6, 240.2, 2, $pdf, 2);
    if ($i == 0) $pdf->Text(8, 239, 'oder');

    if($i==0) $this->pdfRect(14.6, 249.6, 2, $pdf, 2);
    if($i==0) $this->pdfRect(14.6, 254.2, 2, $pdf, 2);
    if ($i == 0) $pdf->Text(8, 253, 'oder');


    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);

/**
 * Page 4
 */
    $tplidx = $pdf->importPage(4, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

    // Schlusserklärungen des Antragstellers und der zu versichernden Personen
    if($i==0) {
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+3, 0, 2);
        $this->pdfRect(10.2, 43, 38, $pdf, 5.2);
        $this->pdfRect(63.8, 43, 53, $pdf, 5.2);
        if(!empty($data['personInsured'])) {
            $this->pdfRect(132, 43, 55, $pdf, 5.2);
            $pdf->Text(133, 47.4, 'x');
        }
        $pdf->Text(11.4, 47.4, 'x');
        $pdf->Text(65.8, 47.4, 'x');
    }

    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);

    // 1.AV Nummer
    $pdf->Text(28.7, 65.5, '1   6   3   6  0   2');
    
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);
    
    //Stempel
    // Maximilian Waizmann, Begonienstr 1 ...
    $pdf->Image($path.'/files/logo.png', 132, 61, 32.0);

    $pdf->Text(132, 71, "Versicherungsmakler Experten GmbH");
    $pdf->Text(132, 74, "Feursstr. 56 / RGB");
    $pdf->Text(132, 77, "82140 Olching");
    $pdf->Text(132, 80, "Tel.: 08142 - 651 39 28");
    $pdf->Text(132, 83, "info@zahnzusatzversicherung-experten.de");

/**
 * Page 5
 */
    $tplidx = $pdf->importPage(5, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

    if ($complete == 2) {
        $pdf->addPage('P');
    }
?>