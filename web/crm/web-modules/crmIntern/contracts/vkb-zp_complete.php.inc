<?

// File for generating Contract PDF for VKB-zp  (VKB + UKV sind vom Tarif gleich)

    if($i==0) {
        $pagecount = $pdf->setSourceFile($path.'/files/vkb/antrag/antrag_201512_bw.pdf');
    } else {
        $pagecount = $pdf->setSourceFile($path.'/files/vkb/antrag/antrag_201512_bw.pdf');
    }

// Seite 1

        $pdf->addPage('P');
        $tplidx = $pdf->importPage(1, '/MediaBox');
        $pdf->useTemplate($tplidx, 6, 6.8, 200);

        $this->getCompanyStamp(88.0, 15, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']);

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+2, 0, 2);

        if($i==0) $this->pdfRect(86.0, 0, 62, $pdf);
        $pdf->Text(87.0, 4.0, $titles[$i]);

        // Partnernummer
        $pdf->SetFont($pdfCfg['fontFamily'], '', 12, 0, 2);
        $pdf->Text(159, 87, '1  8  8  9  6  3  3');

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

        // Antragsteller
        $pdf->Text(20.0, 70.5, $data[person][surname]);
        $pdf->Text(20.0, 78, $data[person][forename]);

        if($data[gender]=="female")
            $pdf->Text(102, 62.5, "x");
        else
            $pdf->Text(86.4, 62.5, "x");

        $pdf->Text(114, 78.5, $data[birthdate]);
        $pdf->Text(20.0, 85.7, $data[street]);
        $pdf->Text(30, 93.8, $data[postcode]);
        $pdf->Text(52, 93.8, $data[city]);
        if ( ! empty($data['phone'])) 
        {
            $pdf->Text(20, 101.0, $data[phone]);
        }
        else
        {
            if ($i == 0) $this->pdfRect(20, 98, 50, $pdf, 4);
        }
        $pdf->Text(20, 108.5, $data[email]);

        // Zahlungsweise Ergaenzung jaehrlich "kein Skonto"
        $pdf->SetFont($pdfCfg['fontFamily'], '', 6, 0, 2);
        $pdf->Text(148.4, 127.3, '(kein Skonto!)');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

        // 2. zu versichernde Person
        $pdf->Text(66.5, 175.5, $data[personInsured][forename]);
        $pdf->Text(66.5, 181, $data[personInsured][surname]);
        $pdf->Text(66.5, 205.0, $data['birthdate2']);
        $pdf->Text(66.5, 210.0, $data[personInsured][nationCode]);

        if ( ! empty($data['personInsured']['job']))
        {
            $pdf->Text(66.5, 215.5, $data[personInsured][job]);
        }
        else
        {
            if ($i == 0) $this->pdfRect(65, 213, 36, $pdf, 3.5);
        }

        if($data[personInsured][salutationLid]=="9")
        {
            $pdf->Text(107.2, 170.5, "x");
        }
        else
        {
            $pdf->Text(91.2, 170.5, "x");
        }

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-1, 0, 2);

        // 3. Tarife / Beitrag
        // Versicherungsbeginn/Datum
            $signUp = explode("-", $data['wishedBegin']);
                // reset font
                $pdf->SetFont($pdfCfg['fontFamily'], 'B', 10, 0, 2);
                $pdf->Text(71.5, 237.8, $signUp[1]);
                $pdf->Text(83.54, 237.8, $signUp[0]);

        // Tarifabfrage was für ein Kreuz gesetzt werden soll bei ZahnPrivat-Kompakt oder  ZahnPrivat-Optimal oder ZahnPrivat-Premium
                switch($pdfTemplate) {
                    case project::gti('ukv-zp-premium'):
                        $pdf->Text(19, 252.8, "x");
                        $pdf->Text(79.5, 248.8, $data[price]);
                        break;
                    case project::gti('ukv-zp-optimal'):
                        $pdf->Text(19, 248, "x");
                        $pdf->Text(79.5, 248.8, $data[price]);
                        break;
                    case project::gti('ukv-zp-kompakt'):
                        $pdf->Text(19, 243.3, "x");
                        $pdf->Text(79.5, 248.8, $data[price]);
                        break;
                    case project::gti('vkb-zp-premium'):
                        $pdf->Text(19, 252.8, "x");
                        $pdf->Text(79.5, 248.8, $data[price]);
                        break;
                    case project::gti('vkb-zp-optimal'):
                        $pdf->Text(19, 248, "x");
                        $pdf->Text(79.5, 248.8, $data[price]);
                        break;
                    case project::gti('vkb-zp-kompakt'):
                        $pdf->Text(19, 243.3, "x");
                        $pdf->Text(79.5, 248.8, $data[price]);
                    break;
                }



// Seite 2
        $pdf->addPage('P');
        $tplidx = $pdf->importPage(2, '/MediaBox');
        $pdf->useTemplate($tplidx, 6, 6.8, 200);

    // Frage zu 4.1.1 -> Fehlen Zaehne?
        $this->pdfRect(137.3, 45.5, 2, $pdf, 2); //Nein
        $this->pdfRect(151.5, 45.5, 2, $pdf, 2); //Ja
        if( ! empty($data[t1]))
        {
            $pdf->Text(151.5, 47.0, "x"); //Ja
            $pdf->Text(156.5, 53, $data[t1]);
        }
        #else{
        #     $pdf->Text(137.3, 47.0, "x"); // Nein
        #}

    // Frage zu 4.1.2 -> Behandlungen?
        $this->pdfRect(137.3, 56, 2, $pdf, 2); //Nein
        $this->pdfRect(151.5, 56, 2, $pdf, 2); //Ja
        if ( ! empty($data['incare']))
        {
            $pdf->Text(151.5, 58, "x");
        }
        #else 
        #{
        #    $pdf->Text(137.3, 58, "x");
        #}

    // Frage zu 4.1.2 -> Paradontose
        $this->pdfRect(137.3, 71.3, 2, $pdf, 2); //Nein
        $this->pdfRect(151.5, 71.3, 2, $pdf, 2); //Ja
        if ( ! empty($data['periodontitis']))
        {
            $pdf->Text(151.5, 73.3, 'x');
        }
        #else
        #{
        #    $pdf->Text(137.3, 73.3, 'x');
        #}

    // Frage zu  4.1.4 Person 1
        if ($pdfTemplate == project::gti('vkb-zp-premium') || $pdfTemplate == project::gti('ukv-zp-premium'))
        {
            if (actionHelper::getAge($data['personInsured']['birthdate']) < 19)
            {
    
            }
        }

    // Frage zu  5 Person 1
        $this->pdfRect(65.8, 124.1, 2, $pdf, 2); //Nein
        $this->pdfRect(76.3, 124.1, 2, $pdf, 2); //Ja
        $this->pdfRect(93.5, 126, 34, $pdf, 3.7); //Wenn Ja, Vers.Nr.

    // Frage zu  5 Person 1
        $this->pdfRect(66, 132.3, 2, $pdf, 2); //AOK Bayern
        $this->pdfRect(90, 132.9, 2, $pdf, 2); //Sonstige
        $this->pdfRect(93.5, 136.3, 34, $pdf, 3.7); //Sonstige Eingabefeld

    // Frage zu  5 - Zusatzversicherung Person 1
        $this->pdfRect(65.9, 148.7, 2, $pdf, 2); //Nein
        $this->pdfRect(76.4, 148.7, 2, $pdf, 2); //Ja

    // Frage zu  5 - Zusatzversicherung - Wo? Person 1
        $this->pdfRect(87, 147.1, 25.3, $pdf, 3.7); //Wo - oben
        $this->pdfRect(87, 151.7, 25.3, $pdf, 3.7); //Wo - unten
        $this->pdfRect(115, 147, 12.9, $pdf, 3.7); //Höhe der Leistung

    // Frage zu  5 - Wurde der Antrag... Person 1
        $this->pdfRect(65.9, 157.9, 2, $pdf, 2); //Nein
        $this->pdfRect(76.0, 157.9, 2, $pdf, 2); //abgelehnt
        $this->pdfRect(92, 157.9, 2, $pdf, 2); //beendet
        $this->pdfRect(106.3, 157.9, 2, $pdf, 2); //gekündigt
        $this->pdfRect(86, 161.4, 42, $pdf, 3.7); //Warum

    // Frage zu  6.1 - Wurde der Antrag... ja
        $pdf->Text(23.9, 181.4, 'x');

    // Frage zu  6.2 - Wurde der Antrag... ja
        $pdf->Text(23.9, 198.7, 'x');



// Seite 3
        $pdf->addPage('P');
        $tplidx = $pdf->importPage(3, '/MediaBox');
        $pdf->useTemplate($tplidx, 6, 6.8, 200);

        //Möglichkeit I
        $this->pdfRect(19.77, 68.8, 2.7, $pdf, 2.7); 
        // Möglichkeit II
        $this->pdfRect(19.77, 116.4, 2.7, $pdf, 2.7); 

        $pdf->Text(13, 85, "O");
        $pdf->Text(13, 90, "D");
        $pdf->Text(13, 95, "E");
        $pdf->Text(13, 100, "R");




// Seite 4
        $pdf->addPage('P');
        $tplidx = $pdf->importPage(4, '/MediaBox');
        $pdf->useTemplate($tplidx, 6, 6.8, 200);
        // Datum
        $this->pdfRect(19, 45, 35.48, $pdf, 8);
        // Unterschrift Antragsteller
        $this->pdfRect(57, 45, 67, $pdf, 8);
        // Unterschrift Antragsteller ab 16
        $this->pdfRect(126, 45, 66.8, $pdf, 8);

        // Kontoinhaber
        $this->pdfRect(47.5, 85.3, 99.51, $pdf, 4.2);
        // Straße & Hausnummer
        $this->pdfRect(47.5, 91.1, 99.51, $pdf, 4.2);
        // PLZ
        $this->pdfRect(30, 96.64, 14.19, $pdf, 4.2);
        // Ort
        $this->pdfRect(50.8, 96.64, 96.15, $pdf, 4.2);
        // IBAN
        $this->pdfRect(26.3, 102.5, 76, $pdf, 4.2);
        // BIC
        $this->pdfRect(109.26, 102.5, 38, $pdf, 4.2);
        // Kreditinstitut
        $this->pdfRect(31, 108.17, 116.2, $pdf, 4.2);
        // Ort / Datum
        $this->pdfRect(29.5, 120.4, 52.8, $pdf, 6.9);
        // Unterschrift
        $this->pdfRect(122, 120.4, 70.5, $pdf, 6.9);

        // Unterschrift Kreuzchen bei Datum / Unterschrift Angestellter / Unterschrift zu versichernde Person
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);
        $pdf->Text(19, 53, 'X');
        $pdf->Text(56.8, 53, 'X');
        $pdf->Text(126.2, 53, 'X');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        // Unterschrift Kreuzchen bei Ort/Datum / Unterschrift Kontoinhaber
         $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);
        $pdf->Text(29.6, 127, 'X');
        $pdf->Text(122, 127, 'X');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);


// Seite 5
        $pdf->addPage('P');
        $tplidx = $pdf->importPage(5, '/MediaBox');
        $pdf->useTemplate($tplidx, 6, 6.8, 200);




// Seite 6
        $pdf->addPage('P');
        $tplidx = $pdf->importPage(6, '/MediaBox');
        $pdf->useTemplate($tplidx, 6, 6.8, 200);


// Seite 7
        $pdf->addPage('P');
        $tplidx = $pdf->importPage(7, '/MediaBox');
        $pdf->useTemplate($tplidx, 6, 6.8, 200);

        


// Seite 8
        if($complete==2)
        $pdf->addPage('P');














?>