<?
// File for generating Contract PDF for Ergo Premium ZEZ

// request data just once


if($i==0)
{
	require_once($path.'/libs/soap/nusoap.php');					      // hier sind alle wichtigen Funktionen drinnen

	// �bergabe der Variablen zur Antragsgenerierung
	$Eingabe=array();

	$Eingabe['EingabeDaten']['MandantID']			="1";    // 1 = ErgoDirekt;

	$Eingabe['EingabeDaten']['VertragsBeginn']		=$data['wishedBegin'];      // Beginn (immer der Monatserste)

	$Eingabe['EingabeDaten']['VermittlungsID']		="WAIZMANN_MAX";    // Ihre ID f�r Firma Maximilian Waizmann;
	
	$Eingabe['EingabeDaten']['PersonenVP']['PersonVP']['PersonID'] = "VP1";
	$Eingabe['EingabeDaten']['PersonenVP']['PersonVP']['GeburtsDatum'] = $data['personInsured']['birthdate'] // Geburtsdatum der versicherten Person
	$Eingabe['EingabeDaten']['PersonenVP']['PersonVP']['Anrede'] = $data['personInsured']['salutationLid']==10?"Frau":"Herr";
	$Eingabe['EingabeDaten']['PersonenVP']['PersonVP']['Vorname'] = $data['personInsured']['forename'];
	$Eingabe['EingabeDaten']['PersonenVP']['PersonVP']['Nachname'] = $data['personInsured']['surname'];
	$Eingabe['EingabeDaten']['PersonenVP']['PersonVP']['Strasse'] = $data['street'];
	$Eingabe['EingabeDaten']['PersonenVP']['PersonVP']['Hausnummer'] = "";
	$Eingabe['EingabeDaten']['PersonenVP']['PersonVP']['Plz'] = $data['postcode'];
	$Eingabe['EingabeDaten']['PersonenVP']['PersonVP']['Ort'] = $data['city'];

	$Eingabe['EingabeDaten']['PersonenVN']['GeburtsDatum'] = $data['person']['birthdate'] // Geburtsdatum des VNs
	$Eingabe['EingabeDaten']['PersonenVN']['Anrede'] = $data['person']['salutationLid']==10?"Frau":"Herr";
	$Eingabe['EingabeDaten']['PersonenVN']['Vorname'] = $data['person']['forename'];
	$Eingabe['EingabeDaten']['PersonenVN']['Nachname'] = $data['person']['surname'];
	$Eingabe['EingabeDaten']['PersonenVN']['Strasse'] = $data['street'];
	$Eingabe['EingabeDaten']['PersonenVN']['Hausnummer'] = "";
	$Eingabe['EingabeDaten']['PersonenVN']['Plz'] = $data['postcode'];
	$Eingabe['EingabeDaten']['PersonenVN']['Ort'] = $data['city'];

	$Eingabe['EingabeDaten']['Tarife']				="ZEZ"; // Tarifkombination

	$Eingabe['EingabeDaten']['Zahlungsweise']			="MONATLICH";	      // immer monatlich
	



	
	// Neue URL ab 05.07.2014
	$ServiceURL   = "https://btob1.ergodirekt.de/ws-pdf-angebote/tarif-zahn?wsdl";

	// URL f�r den Webservicezugriff
	//$ServiceURL	= "https://ergodirekt.tough-werbeagentur.de/vertrieb/ZahnzusatzWebservice2013/zzangebot.php?wsdl";

	
	// Webserviceverbindung herstellen
	$Client 	= new nusoap_client($ServiceURL, true);			

	// Testen auf Fehler bei der Verbindung
	$Err = $Client->getError();
	if ($Err) {	echo '<h2>Constructor error</h2><pre>' . $Err . '</pre>'; 		die;	}
	
	// Die Methode (hier "AntragPPZ") aufrufen (hier passiert das Wesentliche)
	$Result = $Client->call('AntragPPZ', $Eingabe);
	
	// Testen auf Fehler
	$Err = $Client->getError();
	if ($Err) {		
		echo '<h2>Error</h2><pre>' . $Err . '</pre>'; 
		echo '<h2>Response</h2>.<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
	}
	
	$handle = fopen ($path.'/libs/soap/ergo_SEPA_direkt_zez.pdf', 'wb');
	fwrite($handle, base64_decode($Result['AusgabeDaten']['PDFAngebot']));
	if(filesize($path.'/libs/soap/ergo_SEPA_direkt_zez.pdf')==0) die('Error with File creation! '.$Result['AusgabeDaten']['Fehler']);
	fclose($handle);
}

	// Pr�fung, ob Datei vorhanden ist...
	$pagecount = $pdf->setSourceFile($path.'/libs/soap/ergo_SEPA_direkt_zez.pdf');
	$tplidx = $pdf->importPage(1, '/MediaBox');


	$pdf->addPage('P');
	$pdf->useTemplate($tplidx, 0, 0.1, 209.9);
       $pdf->SetMargins(0, 0, 0);
		// set title and font to big and bold!
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);

		//gelbe markierungen nur auf r�ckantwort drucken! 
		if($i==0) $this->pdfRect(90, 1.5, 68, $pdf);
		
		$pdf->Text(91.0, 5.5, $titles[$i]);

		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);

	// PP Nummer Waizmann
		//$pdf->Text(164.0, 10.0, 'PP-Nr: 15700');

	    $this->getCompanyStamp(70.0, 11.5, $pdf, $data['contractComplete']['sourcePage'], 7, 'horizontal', 'wide');



	//Antragsteller
		if($data['gender']=='female')
			$pdf->Text(94.2, 45.1, 'X');
		else
			$pdf->Text(94.2, 48.2, 'X');

		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);

		$name = strpos(utf8_decode($data['name']),'?')!==false?$data['name']:utf8_decode($data['name']);
		$pdf->Text(26.0, 47.5, $name);
	 
		$pdf->Text(26.0, 56.5, $data['street']);
		$pdf->Text(26.0, 65.5, $data['postcode'].' '.$data['city']);

		$pdf->Text(68.5, 84.0, $data['birthdate']);
		
		if($data['phone'])
			$pdf->Text(26.0, 74.5, $data['phone']);
		else 
			if($i==0) $this->pdfRect(25.0, 71.0, 60, $pdf);


	//Zu versichernde Person
		if($data['personInsured'])
		{
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
			if($data['gender2']=='female')	
				$pdf->Text(180.2, 45.1, 'X');
			else 
				$pdf->Text(180.2, 48.2, 'X');
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);

			$name = $data['personInsured']['surname'].", ".$data['personInsured']['forename'];
			$name = strpos(utf8_decode($name),'?')!==false?$name:utf8_decode($name);
			$pdf->Text(110.0, 47.5, $name); unset($name);

			$pdf->Text(110.0, 56.5, $data['street']);
			$pdf->Text(110.0, 65.5, $data['postcode'].' '.$data['city']);

			#$this->pdfClean(156.5, 80, 20, $pdf);
			#$pdf->Text(154.5, 84, $data['birthdate2']);

			if($data['phone'])
				$pdf->Text(110.0, 74.5, $data['phone']);
			else if($i==0) $this->pdfRect(109.0, 71.0, 60, $pdf);
		} else {
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
			if($data['gender']=='female')	
				$pdf->Text(180.2, 45.1, 'X');
			else 
				$pdf->Text(180.2, 48.2, 'X');
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);

			$pdf->Text(110.0, 47.5, $name); unset($name);

			$pdf->Text(110.0, 56.5, $data['street']);
			$pdf->Text(110.0, 65.5, $data['postcode'].' '.$data['city']);

			$this->pdfClean(153.5, 79.7, 25, $pdf);
			$pdf->Text(154.5, 84, $data['birthdate']);

			if($data['phone'])
				$pdf->Text(110.0, 74.5, $data['phone']);
			else if($i==0) $this->pdfRect(109.0, 71.0, 60, $pdf);
		}


		//$this->pdfClean(106, 135.9, 24, $pdf);
		//$this->pdfClean(169.0, 136.4, 20, $pdf);

		//$pdf->Text(107, 140.0, '01.'.$data['begin']);
		//$pdf->Text(170, 140.3, $data['price'].' �');


		// Einverst�dniserkl�rung
		if($i==0) $this->pdfRect(22.5, 187.3, 34.0, $pdf);
		if($i==0) $this->pdfRect(61.5, 187.3, 60, $pdf);
		if($i==0) $this->pdfRect(125.5, 187.3, 60, $pdf);

		// SEPA Lastschriftmandat
		if($i==0) $this->pdfRect(23.0, 232, 64.0, $pdf);
		if($i==0) $this->pdfRect(106.0, 232, 80.0, $pdf);
		if($i==0) $this->pdfRect(23.0, 242, 64.0, $pdf);
		if($i==0) $this->pdfRect(97.0, 242, 85.0, $pdf);

		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+3, 0, 2);
		if($i==0) $pdf->Text(62.1, 191.0, 'X');
		if($i==0) $pdf->Text(97.6, 247.1, 'X');
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		$tplidx = $pdf->importPage(2, '/MediaBox');
		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);

		$tplidx = $pdf->importPage(3, '/MediaBox');
		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);

	if($complete==2)
		$pdf->addPage('P');

		include ('beratungsprotokoll.php.inc');





/*

	// Pr�fung, ob Datei vorhanden ist...
	$pagecount = $pdf->setSourceFile($path.'/libs/soap/ergo_SEPA_direkt_zez_statisch.pdf');
	$tplidx = $pdf->importPage(1, '/MediaBox');

	$pdf->addPage('P');
	$pdf->useTemplate($tplidx, 0, 0.1, 209.9);
       $pdf->SetMargins(0, 0, 0);
		// set title and font to big and bold!
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);

		//gelbe markierungen nur auf r�ckantwort drucken! 
		if($i==0) $this->pdfRect(90, 1.5, 68, $pdf);
		
		$pdf->Text(91.0, 5.5, $titles[$i]);

		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);

	// PP Nummer Waizmann
		//$pdf->Text(164.0, 10.0, 'PP-Nr: 15700');

	    $this->getCompanyStamp(74.0, 13.5, $pdf, $data['contractComplete']['sourcePage'], 6, 'horizontal', 'wide');



	//Antragsteller
		if($data['gender']=='female')
			$pdf->Text(94.1, 44.6, 'X');
		else
			$pdf->Text(94.1, 48.0, 'X');

		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);

		$name = strpos(utf8_decode($data['name']),'?')!==false?$data['name']:utf8_decode($data['name']);
		$pdf->Text(26.0, 47.5, $name);
	 
		$pdf->Text(26.0, 56.5, $data['street']);
		$pdf->Text(26.0, 65.5, $data['postcode'].' '.$data['city']);

		$pdf->Text(68.5, 84.0, $data['birthdate']);
		
		if($data['phone'])
			$pdf->Text(26.0, 74.5, $data['phone']);
		else 
			if($i==0) $this->pdfRect(25.0, 71.0, 60, $pdf);


	//Zu versichernde Person
		if($data['personInsured'])
		{
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
			if($data['gender2']=='female')	
				$pdf->Text(180.2, 44.6, 'X');
			else 
				$pdf->Text(180.2, 48.0, 'X');
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);

			$name = $data['personInsured']['surname'].", ".$data['personInsured']['forename'];
			$name = strpos(utf8_decode($name),'?')!==false?$name:utf8_decode($name);
			$pdf->Text(110.0, 47.5, $name); unset($name);

			$pdf->Text(110.0, 56.5, $data['street']);
			$pdf->Text(110.0, 65.5, $data['postcode'].' '.$data['city']);

			#$this->pdfClean(156.5, 80, 20, $pdf);
			$pdf->Text(154.5, 84, $data['birthdate2']);

			if($data['phone'])
				$pdf->Text(110.0, 74.5, $data['phone']);
			else if($i==0) $this->pdfRect(109.0, 71.0, 60, $pdf);
		} else {
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
			if($data['gender']=='female')	
				$pdf->Text(177.2, 45.3, 'X');
			else 
				$pdf->Text(177.2, 48.1, 'X');
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);

			$pdf->Text(110.0, 47.5, $name); unset($name);

			$pdf->Text(110.0, 56.5, $data['street']);
			$pdf->Text(110.0, 65.5, $data['postcode'].' '.$data['city']);

			$this->pdfClean(153.5, 79.7, 25, $pdf);
			$pdf->Text(154.5, 84, $data['birthdate']);

			if($data['phone'])
				$pdf->Text(110.0, 74.5, $data['phone']);
			else if($i==0) $this->pdfRect(109.0, 71.0, 60, $pdf);
		}


		$pdf->Text(48.0, 127.7, '01.'.$data['begin']);
		$pdf->Text(130.0, 127.7, $data['price'].' �');


		// Einverst�dniserkl�rung
		if($i==0) $this->pdfRect(22.5, 186.8, 34.0, $pdf);
		if($i==0) $this->pdfRect(61.5, 186.8, 60, $pdf);
		if($i==0) $this->pdfRect(125.5, 186.8, 60, $pdf);

		// SEPA Lastschriftmandat
		if($i==0) $this->pdfRect(23.0, 231.7, 64.0, $pdf);
		if($i==0) $this->pdfRect(106.0, 231.7, 80.0, $pdf);
		if($i==0) $this->pdfRect(23.0, 241.5, 64.0, $pdf);
		if($i==0) $this->pdfRect(97.0, 241.5, 85.0, $pdf);

		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+3, 0, 2);
		if($i==0) $pdf->Text(62.1, 191.0, 'X');
		if($i==0) $pdf->Text(97.6, 246.1, 'X');
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		// Vermittlernummer
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+3, 0, 2);
		$pdf->Text(23.0, 279.5, '1   5   7   0   0');
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
		

		// Seite 2
		$tplidx = $pdf->importPage(2, '/MediaBox');
		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);

		// Seite 3
		if($complete==2)
			$pdf->addPage('P');

		// Seite 4
		if($complete==2)
			$pdf->addPage('P');
		
		// Seite 5
		include ('beratungsprotokoll.php.inc');

		

		// Restliche Seiten des Antrages hinzuf�gen
		if($pagecount>2) {
		for ($ergovar = 3; $ergovar<=$pagecount; $ergovar++)
		{
			$tplidx = $pdf->importPage($ergovar, '/MediaBox');
			$pdf->addPage('P');
			$pdf->useTemplate($tplidx, 0, 0.1, 209.9);
		}

	}
*/


?>