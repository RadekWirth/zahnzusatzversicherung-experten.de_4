<?php


    $pagecount = $pdf->setSourceFile($path.'/files/stuttgarter/antrag/2023/Antrag_ZahnKomfort_ZahnPremium_7_2_008A.pdf');


    $age = actionHelper::getAge($data['personInsured']['birthdate']);

/**
 * Page 1
 */
	$tplidx = $pdf->importPage(1, '/MediaBox');
	$pdf->addPage('P');
	$pdf->useTemplate($tplidx, 0, 0.1, 209.9);
	#$this->pdfClean(10.0, 3.0, 40.0, $pdf, 255, 255, 255, 7.0);
	#$this->pdfClean(45.0, 5.0, 140.0, $pdf, 255, 255, 255, 11.0);

	#$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-1, 0, 2);
	#	$pdf->setXY(43.0, 12.4);
	#	$pdf->Write(5, 'Antrag auf Zahnzusatzversicherung');
	
	#$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);
	#	$pdf->Write(5, ' f�r gesetzlich Krankenversicherte bei der Stuttgarter Versicherung AG');
	
	#// set title and font to big and bold!
	#$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);

	#	if($i==0) $this->pdfRect(10.0, 1.0, 54, $pdf, 4.0);
	#		$pdf->Text(11.0, 4.0, $titles[$i]);

	$this->getCompanyStamp(105.0, 3, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']-3, 'horizontal');


		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);


	// Neuantrag
	$pdf->Text(115.9, 21.8, 'X');

	if($data['gender']=='female') {
		$pdf->Text(19.3, 30.5, "x");
	} else {
		$pdf->Text(11.0, 30.5, "x");
	}

	// Versicherte Leistungen
	if($pdfTemplate == project::gti('stuttgarter-komfort')) {
               $pdf->Text(160.0, 28.7, 'x');
       }
       if($pdfTemplate == project::gti('stuttgarter-premium')) {
               $pdf->Text(180.4, 28.7, 'x');
       }


	$pdf->Text(13.0, 35.7, $data['name']); // +6
	$pdf->Text(13.0, 43.0, $data['street']);
	
	$pdf->Text(13.0, 50.8, $data['postcode']);
	$pdf->Text(36.0, 50.8, $data['city']);

	$pdf->Text(13.0, 58.0, $data['birthdate']);
	$pdf->Text(36.0, 58.0, $data['nation']);

	if(!empty($data['job'])) {
		$pdf->Text(57.0, 58.0, $data['job']);
	} else
		if($i==0) $this->pdfRect(56.0, 55.0, 44.0, $pdf, 3.5);

	if(!empty($data['phone'])) {
		$pdf->Text(13.0, 66.8, $data['phone']);
	} else
		if($i==0) $this->pdfRect(11.0, 64.0, 42.0, $pdf, 3.5);

	if(!empty($data['email'])) {
		$pdf->Text(57.0, 66.8, $data['email']);
	} else
		if($i==0) $this->pdfRect(56.0, 64.0, 44.0, $pdf, 3.5);




	if($data['name'] != $data['name2'])
	{

		// Zu versichernde Person
		if($data['gender2']=='female') {
			$pdf->Text(19.3, 131.3, "x");
		} else {
			$pdf->Text(11.0, 131.3, "x");
		}

		$pdf->Text(13.0, 135.7, $data['name2']);
				
		$pdf->Text(13.0, 143.7, $data['birthdate2']);
		$pdf->Text(36.0, 143.7, $data['nation2']);


		if(!empty($data['job2'])) {
			$pdf->Text(57.0, 143.7, $data['job2']);
		} else
			if($i==0) $this->pdfRect(56.0, 141.0, 44.0, $pdf, 3.5);

	}

	// Beginn der Versicherung			
	// reset font
	$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+3, 0, 2);
		$begin = explode(".", $data['begin']);
		$pdf->Text(147, 131.5, '01');
		$pdf->Text(157, 131.5, $begin[0]);
		$pdf->Text(171, 131.5, $begin[1]);
	// reset font
	$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);


		// Zahlweise
		if($i==0) $this->pdfRect(130.2, 141.2, 2.0, $pdf, 2.0);
		if($i==0) $this->pdfRect(145.6, 141.2, 2.0, $pdf, 2.0);
		if($i==0) $this->pdfRect(130.2, 144.5, 2.0, $pdf, 2.0);
		if($i==0) $this->pdfRect(145.6, 144.5, 2.0, $pdf, 2.0);


		// Gesundheitsfragen
		if($i==0) $this->pdfRect(86.5, 166.4, 2.5, $pdf, 2.5);
		if($i==0) $this->pdfRect(94.2, 166.4, 2.5, $pdf, 2.5);
		if($i==0) $this->pdfRect(100.0, 166.4, 5.5, $pdf, 2.5);

		if($i==0) $this->pdfRect(86.5, 174.0, 2.5, $pdf, 2.5);
		if($i==0) $this->pdfRect(94.2, 174.0, 2.5, $pdf, 2.5);
		if($i==0) $this->pdfRect(100.0, 174.0, 5.5, $pdf, 2.5);

		if($i==0) $this->pdfRect(188.2, 166.2, 2.5, $pdf, 2.5);
		if($i==0) $this->pdfRect(195.8, 166.2, 2.5, $pdf, 2.5);
		if($i==0) $this->pdfRect(188.2, 174.0, 2.5, $pdf, 2.5);
		if($i==0) $this->pdfRect(195.8, 174.0, 2.5, $pdf, 2.5);

		// Zahnzustand (vorerst deaktiviert)
		/*
		if($data['contractComplete']['tooth1']>0)
		{
			$pdf->Text(188.6, 128.1, "X");
			$pdf->Text(195.3, 128.0, $data['contractComplete']['tooth1']);
		} else {
			$pdf->Text(180.9, 128.1, "X");			
		}

		if($data['contractComplete']['tooth3'] + $data['contractComplete']['tooth4'] >0)
		{
			$pdf->Text(188.6, 136.2, "X");
			$pdf->Text(195.3, 136.1, $data['contractComplete']['tooth3'] + $data['contractComplete']['tooth4']);
		} else {
			$pdf->Text(180.9, 136.2, "X");
		}
		*/



		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);
		#$this->pdfClean(72.0, 239.5, 14.0, $pdf, 255, 255, 255, 3.5);
		$pdf->Text(180.0, 144.0, $data['price']);

		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);


		if($pdfTemplate == project::gti('stuttgarter-premium')) 
		{
			$left = 22.9;
		} else $left = 2.8;


		if($age < 22) {
			$pdf->Text(160.3 + $left, 106.8, "X");
		} else if($age < 31)	{
			$pdf->Text(160.3 + $left, 110.4, "X");
		} else if($age < 41)	{
			$pdf->Text(160.3 + $left, 114.3, "X");
		} else if($age < 51) {
			$pdf->Text(160.3 + $left, 118.1, "X");
		} else if($age < 61) {
			$pdf->Text(160.3 + $left, 121.9, "X");
		} else if($age <100) {
			$pdf->Text(160.3 + $left, 125.8, "X");
		}


		// Bank
		if($i==0) $this->pdfRect(12.0, 209.0, 80.0, $pdf, 3.0);
		if($i==0) $this->pdfRect(113.5, 209.0, 40.0, $pdf, 3.0);
		#if($i==0) $this->pdfRect(172.0, 166.0, 26.0, $pdf, 3.0);

		// Unterschrift
		if($i==0) $this->pdfRect(112.5, 221.0, 24.0, $pdf, 4.0);
		if($i==0) $this->pdfRect(145.5, 221.0, 50.0, $pdf, 4.0);


/**
 * Page 2
 */
	$tplidx = $pdf->importPage(2, '/MediaBox');

	$pdf->addPage('P');
	$pdf->useTemplate($tplidx, 0, 0.1, 209.9);


/**
 * Page 3
 */
	$tplidx = $pdf->importPage(3, '/MediaBox');

	$pdf->addPage('P');
	$pdf->useTemplate($tplidx, 0, 0.1, 209.9);

	if($i==0) $this->pdfRect(16.6, 173.5, 50.0, $pdf, 4.0);
	if($i==0) $this->pdfRect(87.0, 173.5, 80.0, $pdf, 4.0);

	if($i==0) $this->pdfRect(18.6, 216.7, 40.0, $pdf, 4.0);
	if($i==0) $this->pdfRect(77.0, 216.7, 40.0, $pdf, 4.0);
	if($i==0) $this->pdfRect(127.0, 216.7, 40.0, $pdf, 4.0);

	

$pdf->Text(85.5, 232.8, "x");
$pdf->Text(86.2, 239.5, "x");
$pdf->Text(85.3, 245.8, "x");

	// reset font
	$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);


$pdf->Text(97.0, 259.1, "00115447");
$pdf->Text(114.8, 259.1, "D-YIRL-NDZ7C-33");

/**
 * Page 4
 */
	$tplidx = $pdf->importPage(4, '/MediaBox');

	$pdf->addPage('P');
	$pdf->useTemplate($tplidx, 0, 0.1, 209.9);


?>
