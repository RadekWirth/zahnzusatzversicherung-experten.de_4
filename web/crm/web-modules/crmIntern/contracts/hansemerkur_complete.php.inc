<?
// File for generating Contract PDF for Hanse Merkur

    if($i==0) {
        $pagecount = $pdf->setSourceFile($path.'/files/hanse-merkur/antrag/2020/a3fc10bb766e78f3623f43d4b85525f8b0342036.pdf');
    } else {
        $pagecount = $pdf->setSourceFile($path.'/files/hanse-merkur/antrag/2020/a3fc10bb766e78f3623f43d4b85525f8b0342036.pdf');
    }
        $tplidx = $pdf->importPage(1, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
        $pdf->SetMargins(0, 0, 0);

        if($i==0) $this->pdfRect(8, 0, 68, $pdf);
        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);

        $pdf->Text(10.0, 4.0, $titles[$i]);

        $my = explode(".", $data['begin']);
        // monat
        $pdf->Text(169.6, 23.6, substr($my[0],0,1));
        $pdf->Text(174.6, 23.6, substr($my[0],1,1));
        // Jahr
        $pdf->Text(190.1, 23.6, substr($my[1], 2,1));
        $pdf->Text(195.5, 23.6, substr($my[1], 3,1));
        unset($my);

    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);
        //$pdf->Text(171.0, 8.0, "PP-Nr. 14754");
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

        // Makler Daten schreiben

        $pdf->Text(82.0, 35.0, "3     2     1    9     4    0    9");

        $this->getCompanyStamp(6.0, 9.5, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']-2.5, 'horizontal', 'wide');

    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

        if($data['gender']=='male') {
            $pdf->Text(25.5, 89.3, 'X');
        } else {
            $pdf->Text(25.5, 92.8, 'X');
        }

        $pdf->Text(63.0, 71.6, $data['person']['surname']);
        $pdf->Text(139.0, 71.6, $data['person']['forename']);


        $pdf->Text(63.0, 77.2, $data['street']);
        $pdf->Text(173.0, 77.2, $data['birthdate']);
        $pdf->Text(63.0, 83.0, $data['postcode'].'             '.$data['city']);

        if(empty($data['nation'])) {
            if($i==0) $this->pdfRect(61, 84.5, 50.0, $pdf, 4.0);
        } elseif($data['nation']=='DEU') {
            $pdf->Text(63, 88.2, 'deutsch');
        } else {
            $pdf->Text(63, 88.2, $data['nation']);
        }

        if(empty($data['phone']))
        {
            if($i==0) $this->pdfRect(60.7, 90.2, 55, $pdf, 4.0);
        } else {
            $pdf->Text(63.0, 93.5, 'phone'.$data['phone']);
        }

        if(empty($data['email']))
        {
            if($i==0) $this->pdfRect(61, 96.0, 50.0, $pdf, 4.0);
        } else {
            $pdf->Text(63.0, 99, $data['email']);
        }



    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

    //Beitragszahlung



        // IBAN
        $x = 56.8;
        for($c=0; $c<4; $c++) {
            if($i==0) $this->pdfRect($x += 3.9, 118.0, 3.4, $pdf, 3.2);
        }

        $x = 73.0;
        for($c=0; $c<4; $c++) {
            if($i==0) $this->pdfRect($x += 3.9, 118.0, 3.4, $pdf, 3.2);
        }

        $x = 89.4;
        for($c=0; $c<4; $c++) {
            if($i==0) $this->pdfRect($x += 3.9, 118.0, 3.4, $pdf, 3.2);
        }

        $x = 106.0;
        for($c=0; $c<4; $c++) {
            if($i==0) $this->pdfRect($x += 3.9, 118.0, 3.4, $pdf, 3.2);
        }

        $x = 122.0;
        for($c=0; $c<4; $c++) {
            if($i==0) $this->pdfRect($x += 3.9, 118.0, 3.4, $pdf, 3.2);
        }

        $x = 138.6;
        for($c=0; $c<2; $c++) {
            if($i==0) $this->pdfRect($x += 3.9, 118.0, 3.4, $pdf, 3.2);
        }

        // DATUM
        if($i==0) $this->pdfRect(60, 126.5, 28, $pdf, 4.0);

        //Unterschrift des Kontoinhabers
        if($i==0) $this->pdfRect(117, 126.5, 83, $pdf, 4.0);

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);
        $pdf->Text(118, 129.9, 'X');
        $pdf->Text(61, 129.9, 'X');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

    //Zahlungsweise
        //monatlich
        if($i==0) $this->pdfRect(60.3, 137.2, 2, $pdf, 2);

        //vierteljährlich
        if($i==0) $this->pdfRect(80.0, 137.2, 2, $pdf, 2);

        //halbjährlich
        if($i==0) $this->pdfRect(103.2, 137.2, 2, $pdf, 2);

        //jährlich
        if($i==0) $this->pdfRect(123.8, 137.2, 2, $pdf, 2);


    //Zu versichernde Person 1
        // person insured

        if(isset($data['personInsured']))
        {
            // Uebernahme antragsteller
            $pdf->Text(46.0, 162.2, $data['name2']);
            $pdf->Text(37.0, 173.1, $data['birthdate2']);

            // Bei Auszubildenden
            //if($i==0) $this->pdfRect(34, 165.5, 80, $pdf, 4.0);

    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
            if($data['gender2']=='male')    {
                $pdf->Text(34.3, 160.5, 'X');
            } else {
                $pdf->Text(34.3, 163.0, 'X');
            }
    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

            if($data['nation2']=='DEU') {
                $pdf->Text(64.0, 173.1, 'deutsch');
            } else {
                $pdf->Text(64.0, 173.1, $data['nation2']);
            }

            if ($data['personInsured']['insurance']) {
                $pdf->Text(37, 185.3, $data['personInsured']['insurance']);
            }

        } // Ende Person1
        else {

            // Uebernahme antragsteller
            $pdf->Text(46.0, 162.2, $data['nameb']);
            $pdf->Text(37.0, 173.1, $data['birthdate']);

            // Bei Auszubildenden
            //if($i==0) $this->pdfRect(34, 165.5, 80, $pdf, 4.0);

    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
            if($data['gender']=='male') {
                $pdf->Text(34.3, 160.2, 'X');
            } else {
                $pdf->Text(34.3, 163.0, 'X');
            }
    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

            if($data['nation']=='DEU') {
                $pdf->Text(64.0, 173.1, 'deutsch');
            } else {
                $pdf->Text(64.0, 173.1, $data['nation2']);
            }

            if ($data['personInsured']['insurance']) {
                $pdf->Text(37, 185.3, $data['personInsured']['insurance']);
            }


        }

/*
			// EZ
	              $pdf->Text(51.5, 210.7, 'X');
			// EZE
	              $pdf->Text(51.5, 216.2, 'X');
         	       // EZT
  	              $pdf->Text(51.5, 222.0, 'X');
         	       // EZP
	              $pdf->Text(51.5, 227.7, 'X');
*/


        switch($pdfTemplate)
	 {
		case project::gti('hansemerkur-ez-ezt'):
			// EZ
	              $pdf->Text(51.5, 210.7, 'X');
         	       // EZT
  	              $pdf->Text(51.5, 222.0, 'X');
		break;
		case project::gti('hansemerkur-ez-ezt-ezp'):
			// EZ
	              $pdf->Text(51.5, 210.7, 'X');
         	       // EZT
  	              $pdf->Text(51.5, 222.0, 'X');
         	       // EZP
	              $pdf->Text(51.5, 227.7, 'X');
		break;
		case project::gti('hansemerkur-ez'):
			// EZ
	              $pdf->Text(51.5, 210.7, 'X');
		break;
		case project::gti('hansemerkur-ez-eze'):
			// EZ
	              $pdf->Text(51.5, 210.7, 'X');
         	       // EZE
  	              $pdf->Text(51.5, 216.2, 'X');
		break;
		case project::gti('hansemerkur-ez-eze-ezp'):
			// EZ
	              $pdf->Text(51.5, 210.7, 'X');
         	       // EZE
  	              $pdf->Text(51.5, 216.2, 'X');
         	       // EZP
	              $pdf->Text(51.5, 227.7, 'X');
		break;
		case project::gti('hansemerkur-ez-ezp'):
			// EZ
	              $pdf->Text(51.5, 210.7, 'X');
        	       // EZP
	              $pdf->Text(51.5, 227.7, 'X');
		break;
		case project::gti('hansemerkur-ezp'):
        	       // EZP
	              $pdf->Text(51.5, 227.7, 'X');
		break;
	 }


        // Betrag
        $pdf->Text(68.3, 235.2, $data['price']);


    // page 2
        $tplidx = $pdf->importPage(2, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        // Angaben zum Zahnstatus
        // erst markierungen
        if($i==0) $this->pdfRect(148.6, 45.1 , 2, $pdf, 2);
        if($i==0) $this->pdfRect(148.6, 48.2 , 2, $pdf, 2);
        if($i==0) $this->pdfRect(148.6, 62.2 , 2, $pdf, 2);
        if($i==0) $this->pdfRect(156.9, 62.2 , 2, $pdf, 2);
        if($i==0) $this->pdfRect(148.6, 76.8 , 2, $pdf, 2);
        if($i==0) $this->pdfRect(156.9, 76.8 , 2, $pdf, 2);

if(!$reset) {
        if($data['t1'] > 0)
        {
            $pdf->Text(148.7, 47.5, 'X');
            $pdf->Text(163.0, 47.6, $data['contractComplete']['tooth1']);
        } else {
            $pdf->Text(148.7, 50.3, 'X');
        }

        // incare?
        if($data['incare'])
        {
            $pdf->Text(148.8, 64.2, 'X');
            $pdf->Text(148.8, 78.8, 'X');
        } else  {
            $pdf->Text(157.4, 64.2, 'X');
            $pdf->Text(157.4, 78.8, 'X');
        }
}

        // Original-Datum verdecken
        #if ($i == 0) $this->pdfClean(165.3, 83.3, 12, $pdf, 217, 238, 230, 2, 'F');

        #$pdf->SetFont($pdfCfg['fontFamily'], 'B', 6, 0, 2);
        #$pdf->Text(165, 85, 'Januar 2016');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);


        // Zahn Ergaenzung erhalten
        $pdf->Text(50, 98.8, $data['signupDate']);
        $pdf->Text(128.0, 105.3, 'X');
        $pdf->Text(156, 105.3, 'August 2020');


//print_r($data);

        // Unterschrift

    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);
        if($i==0) $this->pdfRect(130, 118, 64, $pdf, 4.0);
        if($i==0) $this->pdfRect(15.0, 153, 32, $pdf, 4.0);
        if($i==0) $this->pdfRect(50.0, 153, 45, $pdf, 4.0);
        if($i==0) $this->pdfRect(99.0, 153, 45, $pdf, 4.0);

        // Makler Daten schreiben
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);
	 $pdf->Text(60.2, 204.2, 'x');
        $pdf->Text(168, 208.2, "3 2 1 9 4 0 9");


    // page 3
        $tplidx = $pdf->importPage(3, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

    // page 4
        $tplidx = $pdf->importPage(4, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

?>
