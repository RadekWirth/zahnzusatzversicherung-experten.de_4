<?php
/**
* 04.05.2015
* R+V Beiblatt fuer Brillenversicherung 
*
* ZAHN ==================================== */

$teeth = array(
	'Z1U' => 18,
	'Z2U' => 19,
	'Z3U' => 73
);
 
/*
*
* Z1UZV => 20,
* Z2UZV => 21
*
*/

/* BRILLE ================================== */

$glasses = array(
	'BC1U' => 125,
	'BC2U' => 126,
	'BC3U' => 127,
	'P1U' => 225,
	'P2U' => 226,
	'P3U' => 236
);


/* MOEGLICHE KOMBINATIONEN =================
* 
* Z1U(+ZV) + BC1U === P1U Premium
* Z2U(+ZV) + BC2U === P2U Comfort
* Z3U(+ZV) + BC3U === P3U Classic
*
* ANMERKUNG ===============================
*
* Grundsaetzlich werden immer folgende Tariffe angzeigt:
*
* Premium BC1U
* Comfort BC2U
* Classic BC3U
*
* Ausnahme -> wenn gewaehlter Tarif eine der moeglichen Kombinationen ergibt (Vorteilspreis)
*
**/

/* Prüfung, ob für richtige IDT aufgerufen... */
if($data['idt'] == project::gti('rv-premium-z1') || $data['idt'] == project::gti('rv-comfort-z2') || $data['idt'] == project::gti('rv-premium-z1zv') || $data['idt'] == project::gti('rv-comfort-z2zv') || $data['idt'] == project::gti('rv-classic-z3u-zv') || $data['idt'] == project::gti('rv-classic-z3u'))
{

	if ($i == 0) {

		$birthdate = $data['person']['birthdate'];
		$wishedBegin = strtotime($data['wishedBegin']);

		/* holen der Daten */
		// $this->getBenefitByAge(128, $birthdate /* <- has to be filled format YYYY-MM-DD */, $wishedBegin /* <- has to be filled format time() */);

		foreach ($glasses as $k => $v) {
			$tmp[$k] = $this->getBenefitByAge($v, $birthdate, $wishedBegin);
			$amount[$k] = $tmp[$k]['activeBonusBase'];
		}

		foreach ($teeth as $k => $v) {
			$tmp[$k] = $this->getBenefitByAge($v, $birthdate, $wishedBegin);
			$amount[$k] = $tmp[$k]['activeBonusBase'];
		}  

		// Default
		$surplus['P1U'] = $amount['BC1U'];
		$surplus['P2U'] = $amount['BC2U'];
		$surplus['P3U'] = $amount['BC3U'];
		$advantage['P1U'] = $advantage['P2U'] = $advantage['P3U'] = false;
		$regularAmountStr = 'Preis regulär:';
		$advantageAmountStr = 'Ihr Vorteilspreis:';

		// Special cases
		switch ($data['idt']) {
		    case 18:
		    case 20:

		    	$advantage['P1U'] = true;
		        $surplus['P1U'] = $amount['P1U'] - $amount['Z1U'];
		        break;
		    case 19:
		    case 21:
		   		$advantage['P2U'] = true;
		        $surplus['P2U'] = $amount['P2U'] - $amount['Z2U']; 
		        break;
		    case 72:
		    case 73:
		   		$advantage['P3U'] = true;
		        $surplus['P3U'] = $amount['P3U'] - $amount['Z3U']; 

		        break;
		}

#print_r($data);
#print_r($amount);
#print_r($tmp);


		/**
		 * Page 1
		 */
		$pagecount = $pdf->setSourceFile($path.'/files/rv_beiblatt_brille.pdf');
		$tplidx = $pdf->importPage(1, '/MediaBox');
		$pdf->addPage('P');
		$pdf->useTemplate($tplidx);

		$pdf->SetTextColor(0, 0, 0); 
		$pdf->SetDrawColor(255, 0, 0); 
		$pdf->SetLineWidth(1);
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);

		// Angebot fuer
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+3, 0, 2);	
		$pdf->Text(13, 77.5, 'Für '. $data['namec']);

		// BC1U
		$pdf->Text(155, 93, $regularAmountStr);
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+16, 0, 2);
		$pdf->Text(155, 101, $amount['BC1U'] .' €');
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);

		if ($advantage['P1U']) {

			$pdf->Line(154, 102, 184, 95); 
			$pdf->SetTextColor(11, 142, 11); 
			$pdf->Text(155, 112, $advantageAmountStr);
			$pdf->SetTextColor(0, 0, 0); 
			$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+16, 0, 2);
			$pdf->Text(155, 120, number_format($surplus['P1U'], 2) .' €');
			$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);

		} 

		// BC2U
		$pdf->Text(155, 144, $regularAmountStr);
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+16, 0, 2);
		$pdf->Text(155, 152, number_format($amount['BC2U'], 2) .' €');
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);

		if ($advantage['P2U']) {

			$pdf->Line(154, 153, 184, 145); 
			$pdf->SetTextColor(11, 142, 11); 
			$pdf->Text(155, 164, $advantageAmountStr);
			$pdf->SetTextColor(0, 0, 0); 
			$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+16, 0, 2);
			$pdf->Text(155, 172, number_format($surplus['P2U'], 2) .' €');
			$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
			

		} 

		// BC3U
		$pdf->Text(155, 196, $regularAmountStr);
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+16, 0, 2);
		$pdf->Text(155, 204, number_format($amount['BC3U'], 2) .' €');
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);

		if ($advantage['P3U']) {

			$pdf->Line(154, 204, 184, 195); 
			$pdf->SetTextColor(11, 142, 11); 
			$pdf->Text(155, 215, $advantageAmountStr);
			$pdf->SetTextColor(0, 0, 0); 
			$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+16, 0, 2);
			$pdf->Text(155, 223, number_format($surplus['P3U'], 2) .' €');
			$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
			

		}

		if (isset($complete) && $complete == 2) {
			/**
			 * Page 2
			 */
			$pdf->addPage('P');
		}


	}

}



