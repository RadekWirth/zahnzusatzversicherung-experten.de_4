<?
// File for generating Contract PDF for Allianz


       $pagecount = $pdf->setSourceFile($path. 'files/allianz/antrag/2022/Allianz.Zahn.Antrag.01-2022.pdf');
	$page = 1; 

	 #if ($complete)
	 #{
	 #      $pagecount = $pdf->setSourceFile($path. 'files/allianz/antrag/allianz_antrag_2020.pdf');
        #	$page = 1;
	 #}
        $tplidx = $pdf->importPage($page, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 1, 209);


	 $this->pdfClean(70, 1, 80, $pdf, 255, 255, 255, 20);


        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);

        //gelbe markierungen nur auf r ckantwort drucken! 
        if($i==0) $this->pdfRect(75, 4, 68, $pdf);
        
        $pdf->Text(75.0, 8.0, $titles[$i]);


        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

        #$this->getCompanyLogo(28.0, 21.0, $pdf, $data['contractComplete']['sourcePage'], $i, 50);
        $this->getCompanyStamp(8, 6, $pdf, $data['contractComplete']['sourcePage'], 8);




    // 1. Antragsteller

        #if($i==0) $this->pdfRect(12.6, 76.7, 1.7, $pdf, 1.7);
        #if($i==0) $this->pdfRect(19, 76.7, 1.7, $pdf, 1.7);

        if($data['gender']=='male') {
            $pdf->Text(36.4, 60.9, 'X');
        } else {
            $pdf->Text(45.7, 60.9, 'X');
        }

        //  nderung bisheriger Angaben
        $pdf->Text(193.9, 60.9, "X");

        $pdf->Text(23.0, 69.4, $data['person']['surname']);
        $pdf->Text(23.0, 75.5, $data['person']['forename']);
        $pdf->Text(23.0, 82.1, $data['street']);

        $pdf->Text(23.0, 88.6, $data['postcode'].'                                 '.$data['city']);

        if($data['mobile']) {
            $pdf->Text(120.0, 88.6, $data['mobile']);
        } else {
            if($i==0) $this->pdfRect(120.0, 84.6, 60.0, $pdf, 3.5);
        }

        $pdf->Text(120, 75.5, $data['birthdate']);

        $pdf->Text(120.0, 82.1, $data['email']);

	 if($i==0) $this->pdfRect(12.1, 102.6, 2.0, $pdf, 2.0);
	 if($i==0) $this->pdfRect(12.1, 115.8, 2.0, $pdf, 2.0);


    // 2. Versicherungs- /  nderungsbeginn

        $begin = explode('.', $data['begin']);

    // 3. Krankenzusatzversicherung
        $pdf->Text(12.4, 187.9, 'x');
	
        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        $pdf->Text(85, 173.7, $begin[0]);
        $pdf->Text(99, 173.7, $begin[1]);

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

    // 4. Krankenzusatzversicherung

        //$pdf->Text(12, 90.9, 'X');

    // 5. Zu versichernde Personen
/*
        if(!empty($data['personInsured']['pid']))
        {
*/          
            $pdf->Text(27, 222.5, $data['name2']);

            if($data['gender2']=='male')    {
                $pdf->Text(27.4, 234.4, 'x');
            } else {
                $pdf->Text(35.4, 234.4, 'x');
            }

            $pdf->Text(57.5, 235.5, $data['birthdate2']); 

            $pdf->Text(35.0, 291, $data['name2b']);

            if($data['nation']!=$data['nation2'])
            {
                if($data['nation2']!='DEU') 
                {
                    $pdf->Text(12.3, 244.5, 'X');
                    $pdf->Text(28.2, 243.3, $data['nation2']);
                } else {
                    $pdf->Text(141.6, 235.2, 'X');
                }
            }
/*
    // 5. Tarif / Beitr ge
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
            #$pdf->Text(80.0, 40.2, $data['bonusbase']);

        $bonus = 0;
        switch($pdfTemplate)
        {
            case project::gti('allianz-dent-plus'):
                $pdf->Text(12.3, 236.7, 'X');
                # plus
                # je fehlendem Zahn 3,40  aufschlag
                $riskSurchargeFactor = 3.4;
                $riskSurcharge = $riskSurchargeFactor * (float)$data['t1'];
                $pdf->Text(82, 251.6, $data['bonusbase']);
            break;
            case project::gti('allianz-dent-best'):
                $pdf->Text(47.2, 236.7, 'X');
                # best
                # je fehlendem Zahn 5,40  aufschlag
                $riskSurchargeFactor = 5.4;
                $riskSurcharge = stringHelper::makeGermanFloat($riskSurchargeFactor * $data['t1']);
                $pdf->Text(82, 251.6, $data['bonusbase']);
            break;
            case project::gti('allianz-zb-fit'):
                $pdf->Text(45.7, 242.2, 'X');
                $pdf->Text(82, 242.2, 'X');
                # best + fit
                # je fehlendem Zahn 4,10  aufschlag
                $riskSurchargeFactor = 4.1;
                $riskSurcharge = stringHelper::makeGermanFloat($riskSurchargeFactor * $data['t1']);
                $pdf->Text(82, 251.6, $data['bonusbase']);
            break;
            case project::gti('allianz-zahn-plus-zahn-fit'):
                $pdf->Text(12.4, 242.2, 'X');
                $pdf->Text(82, 242.2, 'X');
                # p + fit
                # je fehlendem Zahn 2,90  aufschlag
                $riskSurchargeFactor = 2.9;
                $riskSurcharge = stringHelper::makeGermanFloat($riskSurchargeFactor * $data['t1']);
                $pdf->Text(82, 251.6, $data['bonusbase']);
            break;
            case project::gti('allianz-zb'):
                $pdf->Text(45.7, 242.2, 'X');
                # je fehlendem Zahn 4,10  aufschlag
                $riskSurchargeFactor = 4.1;
                $riskSurcharge = stringHelper::makeGermanFloat($riskSurchargeFactor * $data['t1']);
                $pdf->Text(82, 251.6, $data['bonusbase']);
            break;
            case project::gti('allianz-zahn-plus'):
                $pdf->Text(12.4, 242.2, 'X');
                # je fehlendem Zahn 2,90  aufschlag
                $riskSurchargeFactor = 2.9;
                $riskSurcharge = stringHelper::makeGermanFloat($riskSurchargeFactor * $data['t1']);
                $pdf->Text(82, 251.6, $data['bonusbase']);
            break;
        } 

        #$pdf->Text(80.0, 55.0, stringHelper::makeGermanFloat($bonus));

        $this->pdfClean(80.0, 267.7, 15.0, $pdf, 255, 255, 255, 8.1, 'F');

        if ($riskSurcharge > 0) {
            $pdf->Text(82, 265.5, $riskSurcharge);
        }
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
        $pdf->Text(82, 274.6, stringHelper::makeGermanFloat(stringHelper::toFloat($data['bonusbase']) + stringHelper::toFloat($riskSurcharge)));
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

	 // Betragsteigerung?
	 if($data['personInsured']['bonus']['nextBonusBase'] && $data['personInsured']['bonus']['nextBonusBase'] != $data['personInsured']['bonus']['activeBonusBase'])
	 {
		$pdf->Text(92.5, 274.1, '*');
		$pdf->Text(120, 290.5, '* Achtung: Beitragsanpassung zum '.date('d.m.Y', strtotime($data['personInsured']['bonus']['nextBonusDate'])).' = '.stringHelper::makeGermanFloat(stringHelper::toFloat($data['personInsured']['bonus']['nextBonusBase']) + stringHelper::toFloat($riskSurcharge)).' €');
	 }
*/

// SEITE 2


        $tplidx = $pdf->importPage($page+1, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 1, 209);

	 if($i==0) {
		$this->pdfRect(160.2, 51.2, 2, $pdf, 2);
		$this->pdfRect(168.9, 51.2, 2, $pdf, 2);
	 }

    // 5. Tarif / Beitr ge
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
            #$pdf->Text(80.0, 40.2, $data['bonusbase']);

        $bonus = 0;

#$pdf->Text(83, 126.0, $data['bonusbase']);

        switch($pdfTemplate)
        {
            case project::gti('allianz-dent-plus'):
                $pdf->Text(12.3, 87.8, 'X');
                # plus
                # je fehlendem Zahn 3,40  aufschlag
                $riskSurchargeFactor = 3.4;
                $riskSurcharge = $riskSurchargeFactor * (float)$data['t1'];
                $pdf->Text(83, 126.0, $data['bonusbase']);
            break;
            case project::gti('allianz-dent-best'):
                $pdf->Text(47.4, 87.8, 'X');
                # best
                # je fehlendem Zahn 5,40  aufschlag
                $riskSurchargeFactor = 5.4;
                $riskSurcharge = stringHelper::makeGermanFloat($riskSurchargeFactor * $data['t1']);
                $pdf->Text(83, 126.0, $data['bonusbase']);
            break;
            case project::gti('allianz-zb-fit'):
                $pdf->Text(45.7, 92.9, 'X');
                $pdf->Text(82, 92.9, 'X');
                # best + fit
                # je fehlendem Zahn 4,10  aufschlag
                $riskSurchargeFactor = 4.1;
                $riskSurcharge = stringHelper::makeGermanFloat($riskSurchargeFactor * $data['t1']);
		  $pdf->Text(83, 126.0, $data['bonusbase']);
            break;
            case project::gti('allianz-zahn-plus-zahn-fit'):
                $pdf->Text(12.4, 92.9, 'X');
                $pdf->Text(82, 92.9, 'X');
                # p + fit
                # je fehlendem Zahn 2,90  aufschlag
                $riskSurchargeFactor = 2.9;
                $riskSurcharge = stringHelper::makeGermanFloat($riskSurchargeFactor * $data['t1']);
		  $pdf->Text(83, 126.0, $data['bonusbase']);
            break;
            case project::gti('allianz-zb'):
                $pdf->Text(45.7, 92.9, 'X');
                # je fehlendem Zahn 4,10  aufschlag
                $riskSurchargeFactor = 4.1;
                $riskSurcharge = stringHelper::makeGermanFloat($riskSurchargeFactor * $data['t1']);
                $pdf->Text(82, 126.0, $data['bonusbase']);
            break;
            case project::gti('allianz-zahn-plus'):
                $pdf->Text(12.4, 92.9, 'X');
                # je fehlendem Zahn 2,90  aufschlag
                $riskSurchargeFactor = 2.9;
                $riskSurcharge = stringHelper::makeGermanFloat($riskSurchargeFactor * $data['t1']);
		  $pdf->Text(83, 126.0, $data['bonusbase']);
            break;
        }


    // 6. Zahlweise



    // 8. Andere Zahn-Zusatzversicherungen
        if($i==0) $this->pdfRect(25.5, 197.6, 75, $pdf);
        if($i==0) $this->pdfRect(126, 197.6, 70, $pdf);
/*

*/

        $pdf->Text(35.0, 291.0, $data['name']); 
// SEITE 3

        $tplidx = $pdf->importPage($page+2, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 1, 209);

	 if($i==0) {
		$this->pdfRect(162.8, 30.8, 2, $pdf, 2);
		$this->pdfRect(162.8, 44.6, 2, $pdf, 2);
		$this->pdfRect(171.4, 30.8, 2, $pdf, 2);
		$this->pdfRect(171.4, 44.6, 2, $pdf, 2);
		$this->pdfRect(163.5, 48.9, 9, $pdf, 2);

		$this->pdfRect(22.5, 60.9, 99, $pdf, 3);
	 }

/*
    // 9. Fragen zum Gebisszustand
        // setzen der Markierungen

        if($i==0) $this->pdfRect(162.0, 146.4, 12.0, $pdf);
        if($i==0) $this->pdfRect(162.0, 160, 12.0, $pdf);
        if($i==0) $this->pdfRect(161.5, 165.6, 12.0, $pdf, 2.5);

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);

        $pdf->Text(25.0, 243.2, '0  6  2  2  7  5  0');
        $pdf->Text(25.0, 261.8, '7  5  8  5  3  6  2  1');

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

        if ($data['contractComplete']['biteSplint'] === 'yes')
        {
            $this->pdfClean(10, 201, 200, $pdf, 255, 255, 255, 20, 'F');
            $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);
            $pdf->Text(12, 205, 'Ich bin damit einverstanden, dass Leistungen für Aufbissbehelfe / Schienen (einschließlich der in diesem Zusammenhang anfallenden ');
            $pdf->Text(12, 209, 'funktionsanalytischen und -therapeutischen Leistungen / Gnathologie) vom Versicherungsschutz ausgeschlossen werden.');
            $pdf->Text(12, 216, 'Unterschrift  ___________________________________');
            if($i==0) $this->pdfRect(28, 212.5, 56, $pdf, 3.5);
            $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);
        }
*/

        $pdf->Text(35.0, 291.0, $data['name']); 

// SEITE 4

        $tplidx = $pdf->importPage($page+3, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 1, 209);

/*
        if($i==0) $this->pdfRect(13.2, 61.4, 2.0, $pdf, 2.0);
        if($i==0) $this->pdfRect(13.2, 111, 2.0, $pdf, 2.0);

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+25, 0, 2);

        $pdf->Text(7.0, 59.2, '!');
        $pdf->Text(7.0, 125.8, '!');

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);
*/

        $pdf->Text(35.0, 291.0, $data['name']);

// SEITE 5

        $tplidx = $pdf->importPage($page+4, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 1, 209);

        if($i==0) $this->pdfRect(13.2, 61.4, 2.0, $pdf, 2.0);
        if($i==0) $this->pdfRect(13.2, 111, 2.0, $pdf, 2.0);

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+25, 0, 2);

        $pdf->Text(7.0, 59.2, '!');
        $pdf->Text(7.0, 125.8, '!');

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

/*

        #if($i==0) $this->pdfRect(13.0, 30.5, 85.0, $pdf);
        #if($i==0) $this->pdfRect(13.0, 51.7, 85.0, $pdf);


    // C + D Unterschriften
        if($i==0) $this->pdfRect(109.0, 162.0, 40.0, $pdf);
        if($i==0) $this->pdfRect(157.0, 162.0, 42.0, $pdf);

        if($i==0) $this->pdfRect(112.0, 171.2, 80.0, $pdf);
        if($i==0) $this->pdfRect(112.0, 189.2, 80.0, $pdf);

        if($i==0) $this->pdfRect(115.0, 244.8, 80.0, $pdf);

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+3, 0, 2);
            #$pdf->Text(156.0, 38.0, 'X');
            #$pdf->Text(110.0, 48.0, 'X');
            $pdf->Text(110.0, 167.6, 'X');
            #$pdf->Text(112.0, 136.0, 'X');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);
*/

        $pdf->Text(35.0, 291, $data['name']);

// Seite 6 - SEPA

        $tplidx = $pdf->importPage($page+5, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 1, 209);

        #if($i==0) $this->pdfRect(13.0, 30.5, 85.0, $pdf);
        #if($i==0) $this->pdfRect(13.0, 51.7, 85.0, $pdf);


    // C + D Unterschriften
        if($i==0) $this->pdfRect(109.0, 162.0, 40.0, $pdf);
        if($i==0) $this->pdfRect(157.0, 162.0, 42.0, $pdf);

        if($i==0) $this->pdfRect(112.0, 171.2, 80.0, $pdf);
        if($i==0) $this->pdfRect(112.0, 189.2, 80.0, $pdf);

        if($i==0) $this->pdfRect(115.0, 244.8, 80.0, $pdf);

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+3, 0, 2);
            #$pdf->Text(156.0, 38.0, 'X');
            #$pdf->Text(110.0, 48.0, 'X');
            $pdf->Text(110.0, 167.6, 'X');
            #$pdf->Text(112.0, 136.0, 'X');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

/*
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);

        $pdf->Text(66, 172.4, $data['person']['surname'] . ', ' . $data['person']['forename']);

        if($i==0) $this->pdfRect(65, 181.5, 82, $pdf, 4);
        if($i==0) $this->pdfRect(46, 187.2, 101.5, $pdf, 4);
        if($i==0) $this->pdfRect(54, 193, 93.5, $pdf, 4);
        if($i==0) $this->pdfRect(32, 198.7, 23, $pdf, 4);
        if($i==0) $this->pdfRect(70, 198.7, 78, $pdf, 4);
        if($i==0) $this->pdfRect(44, 208, 103, $pdf, 4);

        if($i==0) $this->pdfRect(23.5, 215, 125, $pdf, 5);
        if($i==0) $this->pdfRect(23.5, 225.5, 61, $pdf, 5);

        if($i==0) $this->pdfRect(23.5, 243.5, 46, $pdf, 5);
        if($i==0) $this->pdfRect(80, 243.5, 67, $pdf, 5);
*/
	
	$pdf->Text(35.0, 291, $data['name']);
	#$this->pdfClean(110, 255.8, 110, $pdf, 255, 255, 255, 10);
