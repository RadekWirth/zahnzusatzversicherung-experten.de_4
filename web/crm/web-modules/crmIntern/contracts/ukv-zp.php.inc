<?



// File for generating Contract PDF for VKB-zp  (VKB + UKV sind vom Tarif gleich)

    $pagecount = $pdf->setSourceFile($path. '/files/ukv/antrag/2023/Kranken_BBKK-UKV_342759_Antrag-ZahnPRIVAT_ungeschützt.pdf');
    $startingPage = 2;

// Seite 1

        $pdf->addPage('P');
        $tplidx = $pdf->importPage($startingPage, '/MediaBox');
        $pdf->useTemplate($tplidx, 6, 6.8, 200);


        $this->getCompanyStamp(88.0, 15, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']);

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+2, 0, 2);

        if($i==0) $this->pdfRect(87.0, 2, 62, $pdf);
        $pdf->Text(88.0, 6.0, $titles[$i]);

        // Partnernummer
        $pdf->SetFont($pdfCfg['fontFamily'], '', 12, 0, 2);
        $pdf->Text(137.3, 80.2, '1  8  8  9  6  3  3');

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

        // Antragsteller
        $pdf->Text(20.0, 55.8, $data['person']['surname']); // 77.8
        $pdf->Text(20.0, 64.3, $data['person']['forename']);

        if($data['gender']=="male")
            $pdf->Text(98.4, 47.4, "x");
        else
            $pdf->Text(83.0, 47.4, "x");

        $pdf->Text(114.3, 63.3, $data['birthdate']);
        $pdf->Text(20.0, 72, $data['street']);
        $pdf->Text(30.0, 80.8, $data['postcode']);
        $pdf->Text(52.0, 80.8, $data['city']);
        if ( ! empty($data['phone'])) 
        {
            $pdf->Text(20, 90.3, $data['phone']);
        } else {
            if ($i == 0) $this->pdfRect(20, 87.3, 50, $pdf, 4);
        }
        $pdf->Text(20, 97.8, $data['email']);

        // Zahlungsweise Ergaenzung jaehrlich "kein Skonto"
        #$pdf->SetFont($pdfCfg['fontFamily'], '', 6, 0, 2);
        #$pdf->Text(150.4, 131.5, '(kein Skonto!)');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);
	
	 $pdf->Text(108.8, 140.8, 'x');

        // 2. zu versichernde Person
        $pdf->Text(66.5, 160.1, $data['personInsured']['forename']);
        $pdf->Text(66.5, 165.6, $data['personInsured']['surname']);
        $pdf->Text(66.5, 182.2, $data['birthdate2']);
        $pdf->Text(66.5, 187.7, $data['personInsured']['nationCode']);

        if ( ! empty($data['personInsured']['job']))
        {
            $pdf->Text(66.5, 192.8, $data['personInsured']['job']);
        } else {
            if ($i == 0) $this->pdfRect(65, 190.1, 36, $pdf, 3.5);
        }

	 if($i == 0) {
		 $this->pdfRect(64.6, 195.4, 2, $pdf, 2);
		 $this->pdfRect(64.6, 197.9, 2, $pdf, 2);
		 $this->pdfRect(64.6, 200.4, 2, $pdf, 2);
		 $this->pdfRect(64.6, 206.1, 2, $pdf, 2);
		 $this->pdfRect(86.8, 205.8, 20, $pdf, 2);
	 }

        if($data['personInsured']['salutationLid']=="9")
        {
            $pdf->Text(107.2, 155.4, "x");
        }
        else
        {
            $pdf->Text(91.2, 155.4, "x");
        }

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-1, 0, 2);

        // 3. Tarife / Beitrag
        // Versicherungsbeginn/Datum
            $signUp = explode("-", $data['wishedBegin']);
                // reset font
                $pdf->SetFont($pdfCfg['fontFamily'], 'B', 10, 0, 2);
                $pdf->Text(71.5, 220.6, $signUp[1]);
                $pdf->Text(83.54, 220.6, $signUp[0]);

        // Tarifabfrage was f r ein Kreuz gesetzt werden soll bei ZahnPrivat-Kompakt oder  ZahnPrivat-Optimal oder ZahnPrivat-Premium
                switch($pdfTemplate) {
                    case project::gti('ukv-zp-premium'):
                        $pdf->Text(19.2, 235.7, "x");
                        $riskSurchargeFactor = 8.6;
                        break;
                    case project::gti('ukv-zp-optimal'):
                        $pdf->Text(19.2, 230.9, "x");
                        $riskSurchargeFactor = 6.7;
                        break;
                    case project::gti('ukv-zp-kompakt'):
                        $pdf->Text(19.2, 226.2, "x");
                        $riskSurchargeFactor = 0;
                        break;
                }

                $pdf->Text(79.5, 231.6, $data['bonusbase']);

                if ($data['bonus_tooth'] == '0,00')
                {
                    $pdf->Text(79.5, 246.4, $data['bonusbase']);
                }
                else 
                {
                    if ($data['t1'] > 0)
                    {
                        #$riskSurcharge = stringHelper::makeGermanFloat($data['t1'] * $riskSurchargeFactor);
                        $pdf->Text(79.5, 240.4, $data['bonus']);
                    }
                    $pdf->Text(79.5, 246.4, stringHelper::makeGermanFloat(stringHelper::toFloat($data['price'])));
                }

#print_r($data); print $riskSurcharge;

// Seite 2
        $pdf->addPage('P');
        $tplidx = $pdf->importPage($startingPage + 1, '/MediaBox');
        $pdf->useTemplate($tplidx, 6, 6.8, 201);

    // Frage zu 4.1.1 -> Fehlen Zaehne?
        $this->pdfRect(137.8, 46.4, 2, $pdf, 2); //Nein
        $this->pdfRect(152.2, 46.4, 2, $pdf, 2); //Ja
        if( ! empty($data['t1']) && $data['t1'] > 0)
        {
            $pdf->Text(152.1, 48.4, "x"); //Ja
            $pdf->Text(156.5, 54.2, $data['t1']);
        }

    // Frage zu 4.1.2 -> Behandlungen?
        $this->pdfRect(137.8, 57.6, 2, $pdf, 2); //Nein
        $this->pdfRect(152.2, 57.6, 2, $pdf, 2); //Ja
        if ( ! empty($data['incare']))
        {
            $pdf->Text(152.2, 59.7, "x");
        }

	 $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+5, 0, 2);
	 $this->pdfRect(43.6, 67.6, 79.9, $pdf, 2.2);
	 $pdf->Text(18.5, 68.9, '!'); 
	 $pdf->Text(132.5, 68.9, '!');
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-1, 0, 2);


    // Frage zu 4.1.3 -> Parodontose
        $this->pdfRect(137.8, 71.7, 2, $pdf, 2); //Nein
        $this->pdfRect(152.2, 71.7, 2, $pdf, 2); //Ja
        if ( ! empty($data['periodontitis']))
        {
            $pdf->Text(152.0, 70.6, 'x');
        }

    // Frage zu  4.1.4 Person 1
        if ($pdfTemplate == project::gti('vkb-zp-premium') || $pdfTemplate == project::gti('ukv-zp-premium'))
        {
            if (($data['personInsuredData'] && $age2 < 20) || $age < 20)
            {
               $this->pdfRect(137.8, 84.3, 2, $pdf, 2); //Nein
               $this->pdfRect(152.2, 84.3, 2, $pdf, 2); //Ja             
            }
        }

    // Frage zu  5 Person 1
	 #$this->pdfRect(66.1, 103.2, 2, $pdf, 2); //AOK Bayern
        $this->pdfRect(66.1, 106.5, 2, $pdf, 2); //AOK Bayern
        $this->pdfRect(66.1, 110.0, 2, $pdf, 2); //AOK Bayern
        $this->pdfRect(66.1, 113.3, 2, $pdf, 2); //AOK Bayern
        $this->pdfRect(66.1, 116.6, 2, $pdf, 2); //AOK Bayern
        $this->pdfRect(66.1, 119.8, 2, $pdf, 2); //AOK Bayern
        $this->pdfRect(66.3, 129.0, 2, $pdf, 2); //AOK Bayern
        $this->pdfRect(76.8, 129.0, 2, $pdf, 2); //AOK Bayern

    // Frage zu  5 - Zusatzversicherung - Wo? Person 1
        #$this->pdfRect(87.4, 134.1, 25.3, $pdf, 3.7); //Wo - oben
        #$this->pdfRect(87.4, 139.3, 25.3, $pdf, 3.7); //Wo - unten
        #$this->pdfRect(115.4, 135.1, 12.9, $pdf, 3.7); //Höhe der Leistung



    // Frage zu  6.1 - Wurde der Antrag... ja
        $pdf->Text(24.2, 149.8, 'x'); // 222.3

    // Frage zu  6.2 - Wurde der Antrag... ja
        $pdf->Text(24.2, 167.4, 'x');

        // Datum
        $this->pdfRect(19, 241.7, 35.48, $pdf, 4);
        // Unterschrift Antragsteller
        $this->pdfRect(57, 241.7, 67, $pdf, 4);
        // Unterschrift Antragsteller ab 16
        $this->pdfRect(126, 241.7, 66.8, $pdf, 4);



// Seite 3
        $pdf->addPage('P');
        $tplidx = $pdf->importPage($startingPage + 2, '/MediaBox');
        $pdf->useTemplate($tplidx, 6, 6.8, 199);

        //M glichkeit I
        #$this->pdfRect(20.27, 176.4, 2.7, $pdf, 2.7); 
        //M glichkeit II
        #$this->pdfRect(20.27, 228.4, 2.7, $pdf, 2.7); 

        #$pdf->Text(13.5, 194.4, "O");
        #$pdf->Text(13.5, 199.4, "D");
        #$pdf->Text(13.5, 204.4, "E");
        #$pdf->Text(13.5, 209.4, "R");

        $pdf->Text(32.0, 141.0, $data['nameb']); // 77.8

        // Kontoinhaber
        $this->pdfRect(47.5, 171.3, 99.51, $pdf, 3.0);
        // Stra e & Hausnummer
        $this->pdfRect(47.5, 177.1, 99.51, $pdf, 3.0);
        // PLZ
        $this->pdfRect(30.0, 182.8, 14.19, $pdf, 3.0);
        // Ort
        $this->pdfRect(55.8, 182.8, 96.15, $pdf, 3.0);
        // IBAN
        $this->pdfRect(30.0, 188.5, 76, $pdf, 3.0);
        // BIC
        $this->pdfRect(112.26, 188.5, 38, $pdf, 3.0);
        // Kreditinstitut
        $this->pdfRect(31, 194.37, 116.2, $pdf, 3.0);
        // Ort / Datum
        $this->pdfRect(35.5, 216.9, 60.8, $pdf, 5.0);
        // Unterschrift
        $this->pdfRect(122, 216.9, 60.5, $pdf, 5.0);

        // Unterschrift Kreuzchen bei Datum / Unterschrift Angestellter / Unterschrift zu versichernde Person
        //$pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);
        //$pdf->Text(19, 140.0, 'X');
        //$pdf->Text(56.8, 140.0, 'X');
        //$pdf->Text(126.2, 140.0, 'X');
        //$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        // Unterschrift Kreuzchen bei Ort/Datum / Unterschrift Kontoinhaber
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);
        $pdf->Text(36.6, 221.5, 'X');
        $pdf->Text(123, 221.5, 'X');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);




// Seite 4
        $pdf->addPage('P');
        $tplidx = $pdf->importPage($startingPage + 3, '/MediaBox');
        $pdf->useTemplate($tplidx, 6, 6.8, 200);


// Seite 5
        $pdf->addPage('P');
        $tplidx = $pdf->importPage($startingPage + 4, '/MediaBox');
        $pdf->useTemplate($tplidx, 6, 6.8, 200);


// Seite 6
        $pdf->addPage('P');
        $tplidx = $pdf->importPage($startingPage + 5, '/MediaBox');
        $pdf->useTemplate($tplidx, 6, 6.8, 200);


// Seite 7
        $pdf->addPage('P');
        $tplidx = $pdf->importPage($startingPage + 6, '/MediaBox');
        $pdf->useTemplate($tplidx, 6, 6.8, 200);

        
// Seite 8
        $pdf->addPage('P');
        $tplidx = $pdf->importPage($startingPage + 7, '/MediaBox');
        $pdf->useTemplate($tplidx, 6, 6.8, 200);
