<?
// File for generating Contract PDF for Ergo Premium

print_r($data); exit;


// request data just once
if($i==0)
{

	require_once($path.'/libs/soap/nusoap.php');					      // hier sind alle wichtigen Funktionen drinnen

	// Übergabe der Variablen zur Antragsgenerierung
	$Eingabe=array();

	$Eingabe['EingabeDaten']['VertragsBeginn']		= $data['wishedBegin'];      // Beginn (immer der Monatserste)
	$Eingabe['EingabeDaten']['Zahlungsweise']			= "MONATLICH";	      // immer monatlich
	$Eingabe['EingabeDaten']['VermittlungsID']		= "WAIZMANN_MAX";    // Ihre ID für Firma Maximilian Waizmann;

	// Tarifkombination
	switch($pdfTemplate)
	{
		case (project::gti('ergo-direkt-premium')):
			$Eingabe['EingabeDaten']['Tarife'] = "ZAB;ZAE;ZBB;ZBE";
			break;
		case (project::gti('ergo-direkt-zab-zae')):
			$Eingabe['EingabeDaten']['Tarife'] = "ZAB;ZAE";
			break;
		case (project::gti('ergo-zahn-ersatz-zab')):
			$Eingabe['EingabeDaten']['Tarife'] = "ZAB";
			break;
		case (project::gti('ergo-direkt-zez')):
			$Eingabe['EingabeDaten']['Tarife'] = "ZEZ";
			break;
		case (project::gti('ergo-direkt-dental-zuschuss')):
			$Eingabe['EingabeDaten']['Tarife'] = "ZEF";
			break;
		case (project::gti('ergo-direkt-zahn-erhalt-permium')):
			$Eingabe['EingabeDaten']['Tarife'] = "ZBB;ZBE";
			break;
		case (project::gti('ergo-direkt-zahn-erhalt')):
			$Eingabe['EingabeDaten']['Tarife'] = "ZBB";
			break;										
	}

	$Eingabe['EingabeDaten']['PersonenVP']['PersonVP']['PersonID'] = "VP1";
	$Eingabe['EingabeDaten']['PersonenVP']['PersonVP']['GeburtsDatum'] = $data['personInsured']['birthdate']; // Geburtsdatum der versicherten Person
	$Eingabe['EingabeDaten']['PersonenVP']['PersonVP']['Anrede'] = $data['personInsured']['salutationLid']==10?"Frau":"Herr";
	$Eingabe['EingabeDaten']['PersonenVP']['PersonVP']['Vorname'] = $data['personInsured']['forename'];
	$Eingabe['EingabeDaten']['PersonenVP']['PersonVP']['Nachname'] = $data['personInsured']['surname'];
	$Eingabe['EingabeDaten']['PersonenVP']['PersonVP']['Strasse'] = $data['street'];
	$Eingabe['EingabeDaten']['PersonenVP']['PersonVP']['Hausnummer'] = "1";
	$Eingabe['EingabeDaten']['PersonenVP']['PersonVP']['Plz'] = $data['postcode'];
	$Eingabe['EingabeDaten']['PersonenVP']['PersonVP']['Ort'] = $data['city'];

	$Eingabe['EingabeDaten']['PersonVN']['GeburtsDatum'] = $data['person']['birthdate']; // Geburtsdatum des VNs
	$Eingabe['EingabeDaten']['PersonVN']['Anrede'] = $data['person']['salutationLid']==10?"Frau":"Herr";
	$Eingabe['EingabeDaten']['PersonVN']['Vorname'] = $data['person']['forename'];
	$Eingabe['EingabeDaten']['PersonVN']['Nachname'] = $data['person']['surname'];
	$Eingabe['EingabeDaten']['PersonVN']['Strasse'] = $data['street'];
	$Eingabe['EingabeDaten']['PersonVN']['Hausnummer'] = "1";
	$Eingabe['EingabeDaten']['PersonVN']['Plz'] = $data['postcode'];
	$Eingabe['EingabeDaten']['PersonVN']['Ort'] = $data['city'];


	// Neue URL ab Mitte Januar 2013
	$ServiceURL   = "https://btob1.ergodirekt.de/ws-pdf-angebote/tarif-zahn?wsdl";

	// Webserviceverbindung herstellen
	$Client 	= new nusoap_client($ServiceURL, true);

	// Testen auf Fehler bei der Verbindung
	$Err = $Client->getError();
	if ($Err) {	echo '<h2>Constructor error</h2><pre>' . $Err . '</pre>'; 		die;	}

	// Die Methode (hier "AntragPPZ") aufrufen (hier passiert das Wesentliche)
	$Result = $Client->call('AntragPPZ', $Eingabe);

	// Testen auf Fehler
	$Err = $Client->getError();
	if ($Err) {		
		echo '<h2>Error</h2><pre>' . $Err . '</pre>'; 
		echo '<h2>Response</h2>.<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
		emailHelper::sendMail('radek.wirth@consulting-wirth.de', 'Fehler bei der Erstellung der ERGO Dateien', '<h2>Response</h2>.<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre><pre>'. print_r($data, true) .'</pre>' );
	}

	if(isset($Result['AusgabeDaten']['Fehler']))
	{
		print_r($Result['AusgabeDaten']); print "<p>";
		emailHelper::sendMail('radek.wirth@consulting-wirth.de', 'Fehler bei der Erstellung der ERGO Dateien', '<h2>Response</h2><pre>'. print_r($Result['AusgabeDaten'], true) .'</pre>'
	}


	$handle = fopen ($path.'/libs/soap/ergo_SEPA_premium_ppz.pdf', 'wb');
	fwrite($handle, base64_decode($Result['AusgabeDaten']['PDFAngebot']));
	fclose($handle);
}

	// Prüfung, ob Datei vorhanden ist...
	$pagecount = $pdf->setSourceFile($path.'/libs/soap/ergo_SEPA_premium_ppz.pdf');
	$tplidx = $pdf->importPage(1, '/MediaBox');

	$pdf->addPage('P');
	$pdf->useTemplate($tplidx, 0, 0.1, 209.9);

		// set title and font to big and bold!
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);

		//gelbe markierungen nur auf rückantwort drucken! 
		if($i==0) $this->pdfRect(90, 1.5, 68, $pdf);
		
		$pdf->Text(91.0, 5.5, $titles[$i]);

		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);

	// PP Nummer Waizmann
		//$pdf->Text(164.0, 10.0, 'PP-Nr: 15700');

	    $this->getCompanyStamp(95.0, 11.5, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']-2);

	//Antragsteller
		if($data['gender']=='female')	{
			$pdf->Text(92, 52.5, 'X');
		}
		else
			$pdf->Text(92, 55.7, 'X');

		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);

		$name = $data['name'];
		$pdf->Text(26.0, 55, $name);
	 
		$pdf->Text(26.0, 64, $data['street']);
		$pdf->Text(26.0, 73, $data['postcode'].' '.$data['city']);

		$pdf->Text(68.5, 91.5, $data['birthdate']);
		
		if($data['phone'])
			$pdf->Text(26.0, 82, $data['phone']);
		else 
			if($i==0) $this->pdfRect(25.0, 78.5, 60, $pdf);


	//Zu versichernde Person
		if($data['personInsured'])
		{
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
			if($data['gender2']=='female')	
				$pdf->Text(176.5, 52.5, 'X');
			else 
				$pdf->Text(176.5, 55.7, 'X');
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);

			$name = $data['personInsured']['surname'].", ".$data['personInsured']['forename'];
			$name = $name;
			$pdf->Text(110.0, 55, $name); unset($name);

			$pdf->Text(110.0, 64, $data['street']);
			$pdf->Text(110.0, 73, $data['postcode'].' '.$data['city']);

			$this->pdfClean(154.0, 87.7, 30, $pdf);
			$pdf->Text(154.6, 92.1, $data['birthdate2']);

			if($data['phone'])
				$pdf->Text(110.0, 82, $data['phone']);
			else if($i==0) $this->pdfRect(109.0, 78.5, 60, $pdf);
		} else {
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
			if($data['gender']=='female')	
				$pdf->Text(176.3, 52.5, 'X');
			else 
				$pdf->Text(176.3, 55.7, 'X');
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);

			$pdf->Text(110.0, 55, $name); unset($name);

			$pdf->Text(110.0, 64, $data['street']);
			$pdf->Text(110.0, 73, $data['postcode'].' '.$data['city']);

			$this->pdfClean(153.5, 87.2, 25, $pdf);
			$pdf->Text(154.5, 91.5, $data['birthdate']);

			if($data['phone'])
				$pdf->Text(110.0, 82, $data['phone']);
			else if($i==0) $this->pdfRect(109.0, 78.5, 60, $pdf);
		}


		//$this->pdfClean(106, 135.9, 24, $pdf);
		//$this->pdfClean(169.0, 136.4, 20, $pdf);

		//$pdf->Text(107, 140.0, '01.'.$data['begin']);
		//$pdf->Text(170, 140.3, $data['price'].' €');


		// Einverstädniserklärung
		if($i==0) $this->pdfRect(22.5, 209.2, 34.0, $pdf);
		if($i==0) $this->pdfRect(61.5, 209.3, 120.0, $pdf);

		// SEPA Lastschriftmandat
		if($i==0) $this->pdfRect(23.0, 247.2, 64.0, $pdf);
		if($i==0) $this->pdfRect(106.0, 247.2, 80.0, $pdf);
		if($i==0) $this->pdfRect(23.0, 257.2, 64.0, $pdf);
		if($i==0) $this->pdfRect(97.0, 257.2, 85.0, $pdf);

		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+3, 0, 2);
		if($i==0) $pdf->Text(62.1, 213.2, 'X');
		if($i==0) $pdf->Text(97.6, 261.3, 'X');
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		$tplidx = $pdf->importPage(2, '/MediaBox');
		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);

		$tplidx = $pdf->importPage(3, '/MediaBox');
		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);

		$pdf->addPage('P');
		
		include ('beratungsprotokoll.php.inc');

	// Restliche Seiten des Antrages hinzufügen
	if($pagecount>3) {
	for ($ergovar = 4; $ergovar<=$pagecount; $ergovar++)
		{
		$tplidx = $pdf->importPage($ergovar, '/MediaBox');
		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);
		}
		$pdf->addPage('P');
	}

	// Add Page
	//if ($complete==2) {
	//	$pdf->addPage('P');
	//}
	?>
