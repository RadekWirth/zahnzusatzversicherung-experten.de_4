<?
	$pagecount = $pdf->setSourceFile($path. '/files/rv/antrag/2023/b75445dd5ef93cfa6b9ccb8ceca8e494db9c893f.pdf');
	$tplidx = $pdf->importPage(2, '/MediaBox');

/**
 * Page 1
 */
    $pdf->addPage('P');

    $pdf->useTemplate($tplidx, -3.6, 0, 210, 297, true);
    #$this->pdfClean(30.0, 2.0, 160.0, $pdf, 215, 255, 255, 4.0);

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);

        if($i==0) $this->pdfRect(10.0, 1.0, 54, $pdf, 4.0);
            $pdf->Text(11.0, 4.0, $titles[$i]);

        $this->getCompanyStamp(130, 2, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']-2.5, 'horizontal', 'wide');

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);

        $pdf->SetMargins(15.8, 9.0);
        $pdf->SetY(63.0);

        $pdf->Write(4.0, '4  1  4');

        $pdf->SetX(103.2);
        $pdf->Write(4.4, '8  0  4  4  9  2');

	 $pdf->SetX(150.0);
	 $pdf->Write(3.5, $data['nation']);


	 #$pdf->SetY(53.4); $pdf->SetX(7.0);
	 $pdf->Text(16.8, 75.8, $data['name']);

        if(!empty($data['phone'])) {
            $pdf->Text(154.0, 75.0, $data['phone']);
        } else
            if($i==0) $this->pdfRect(150.0, 73.2, 44.0, $pdf, 3.5);

        $pdf->Text(16.8, 85.0, $data['street']);
        $pdf->Text(154.0, 85.0, $data['birthdate']);

        $pdf->Text(16.8, 95.4, $data['postcode']);
        $pdf->Text(44.0, 95.4, $data['city']);

    // Angaben zu den versichernden Personen / Versicherungsumfang
        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+3, 0, 2);
            $begin = explode(".", $data['begin']);
            $pdf->Text(72.2, 122.0, $begin[0]);
            $pdf->Text(79.2, 122.0, $begin[1]);

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

        if(!empty($data['personInsured'])) {

            $pdf->Text(72.0, 137.2, $data['name2']);
            $pdf->Text(72.0, 141.8, $data['birthdate2']);

            if(!empty($data['job2'])) {
                $pdf->Text(72.0, 147.0, $data['job2']);
            } else
                if($i==0) $this->pdfRect(70.0, 144.5, 40.0, $pdf, 3.5);

            if($data['gender2']=='female') {
                $pdf->Text(118.7, 142.2, "x");
            } else
                $pdf->Text(125.6, 142.2, "x");
        } 

	if($data['person']['forename'] != $data['personInsured']['forename'] || 
		$data['person']['surname'] != $data['personInsured']['surname'])
	{
		 if($i==0) $this->pdfRect(124.2, 165.3, 2.0, $pdf, 2);
		 if($i==0) $this->pdfRect(124.2, 160.5, 2.0, $pdf, 2);
	}

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+3, 0, 2);
        $pdfTemplate = isset($pdfTemplate)?$pdfTemplate:$data['idt'];
        switch($pdfTemplate) {
            case project::gti('rv-classic-z3u-zv'):
		  $pdf->Text(106, 234.5, "X");
                $pdf->Text(106, 243.3, "X");
                break;  
            case project::gti('rv-comfort-z2zv'):
		  $pdf->Text(106, 239.5, "X");
                $pdf->Text(106, 243.3, "X");
                break;
            case project::gti('rv-premium-z1zv'):
                $pdf->Text(106, 244.2, "X"); //+44
                $pdf->Text(106, 249.3, "X");
                break;
            case project::gti('rv-premium-z1'):
                $pdf->Text(106, 244.2, "X");
                break;
            case project::gti('rv-comfort-z2'):
                $pdf->Text(106, 239.0, "X");
                break;
            case project::gti('rv-classic-z3u'):
                $pdf->Text(106, 234.5, "X");
                break;
            case project::gti('rv-zv'):
                $pdf->Text(106, 249.3, "X");
                break;
            }
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-1, 0, 2);
$this->pdfClean(184, 288, 14.0, $pdf, 255, 255, 255, 4.0);


/**
 * Page 2
 */
    $tplidx = $pdf->importPage(3, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 210, 297, true);

    $this->pdfClean(121.6, 66.8, 13.0, $pdf, 255, 255, 255, 3.4);
    $this->pdfClean(186.6, 73.8, 13.0, $pdf, 255, 255, 255, 3.4);

    $pdf->Text(126.3, 69.0, $data['price']);
    $pdf->Text(191.7, 76.0, $data['price']);

// Unterschrift
    if ($i == 0) $this->pdfRect(20.0, 228.0, 40.0, $pdf, 5.0);
    if ($i == 0) $this->pdfRect(80.0, 228.0, 40.0, $pdf, 5.0);
    if ($i == 0) $this->pdfRect(150.0, 228.0, 40.0, $pdf, 5.0);

    if ($i == 0) $this->pdfRect(20.0, 242.8, 70.0, $pdf, 5.0);
    if ($i == 0) $this->pdfRect(116.0, 242.8, 70.0, $pdf, 5.0);

	
    if ($i == 0) $this->pdfRect(186.8, 111.7, 2.0, $pdf, 2.0);
    if ($i == 0) $this->pdfRect(197.4, 111.7, 2.0, $pdf, 2.0);


/*

    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'] + 12, 0, 2);
    if ($i == 0) $this->pdfRect(2.0, 42.1, 5.0, $pdf, 10.0);
    $pdf->Text(3.0, 49.2, '!');
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);


    #Ort, Datum, Unterschriften
    if($i==0) $this->pdfRect(12.4, 132.9, 47.0, $pdf, 3.0);
    if($i==0) $this->pdfRect(67.3, 132.9, 71.0, $pdf, 3.0);
    if($i==0) $this->pdfRect(152.0, 132.9, 46.0, $pdf, 3.0);

    if($i==0) $this->pdfRect(12.4, 149.1, 55, $pdf, 3.0);
    if($i==0) $this->pdfRect(110.3, 149.1, 55, $pdf, 3.0);

    #IBAN
    if($i==0) $this->pdfRect(11.4, 194.5, 154, $pdf, 3.0);
    if($i==0) $this->pdfRect(10.2, 202.5, 2.8, $pdf, 2.8);
    if($i==0) $this->pdfRect(10.2, 207.5, 2.8, $pdf, 2.8);

    #Name, ...
    if($i==0) $this->pdfRect(11.4, 224.0, 104.0, $pdf, 3.0);    
    if($i==0) $this->pdfRect(11.4, 230.3, 104.0, $pdf, 3.0);
    if($i==0) $this->pdfRect(11.4, 236.8, 104.0, $pdf, 3.0);
    if($i==0) $this->pdfRect(11.4, 243.3, 12.0, $pdf, 3.0); 
    if($i==0) $this->pdfRect(30.4, 243.3, 20.0, $pdf, 3.0);
    if($i==0) $this->pdfRect(60.4, 243.3, 100.0, $pdf, 3.0);

    #Ort, ...
    if($i==0) $this->pdfRect(11.4, 268.2, 40.0, $pdf, 3.0);
    if($i==0) $this->pdfRect(60.4, 268.2, 30.0, $pdf, 3.0);
    if($i==0) $this->pdfRect(98.4, 268.2, 100.0, $pdf, 3.0);
*/
// Hide pagenum
$this->pdfClean(184, 288, 16.0, $pdf, 255, 255, 255, 4.0);



/**
 * Page 3
 */
    $tplidx = $pdf->importPage(4, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 210, 297, true);

// Unterschrift
    //if ($i == 0) $this->pdfRect(20.0, 228.0, 40.0, $pdf, 5.0);

    $pdf->Text(20.8, 64.6, $data['name']);

        #$pdf->Text(20.8, 74.6, $data['street']);
        #$pdf->Text(154.0, 74.6, $data['birthdate']);

        #$pdf->Text(38.8, 94.0, $data['postcode']);
        #$pdf->Text(68.0, 94.0, $data['city']);

    if($i==0) $this->pdfRect(20.4, 35.5, 110.0, $pdf, 4.0);
    if($i==0) $this->pdfRect(20.4, 61.5, 110.0, $pdf, 4.0);
    if($i==0) $this->pdfRect(20.4, 71.5, 110.0, $pdf, 4.0);
    if($i==0) $this->pdfRect(20.4, 81.5, 110.0, $pdf, 4.0);
    if($i==0) $this->pdfRect(20.4, 91.5, 110.0, $pdf, 4.0);
    if($i==0) $this->pdfRect(20.4, 114.5, 150.0, $pdf, 4.0);


// Empfangsbestätigung
    $pdf->Text(124.9, 215.0, 'X');
    $pdf->Text(163.5, 215.4, '0  4   1  8');
    
    if($i==0) $this->pdfRect(20.4, 220.5, 49.0, $pdf, 3.0);
    if($i==0) $this->pdfRect(75.4, 220.5, 110.0, $pdf, 3.0);


// Hide pagenum
$this->pdfClean(187, 288, 14.0, $pdf, 255, 255, 255, 4.0);

/**
 * Page 4
 */
    $tplidx = $pdf->importPage(5, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 210, 297, true);

#    $pdf->SetDrawColor(0, 0, 0);
#    $pdf->SetLineWidth(1);
#    $pdf->Line(10, 280, 200, 10)

$this->pdfClean(187, 288, 14.0, $pdf, 255, 255, 255, 4.0);


/**
 * Page 5
 */
    $tplidx = $pdf->importPage(6, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 210, 297, true);
$this->pdfClean(187, 288, 14.0, $pdf, 255, 255, 255, 4.0);


/**
 * Page 6
 */
    $tplidx = $pdf->importPage(7, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 210, 297, true);
$this->pdfClean(187, 288, 14.0, $pdf, 255, 255, 255, 4.0);


/**
 * Page 7
 */
    $tplidx = $pdf->importPage(8, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 210, 297, true);
$this->pdfClean(187, 288, 14.0, $pdf, 255, 255, 255, 4.0);


/**
 * Page 8
 */
    $tplidx = $pdf->importPage(9, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 210, 297, true);
$this->pdfClean(187, 288, 14.0, $pdf, 255, 255, 255, 4.0);


