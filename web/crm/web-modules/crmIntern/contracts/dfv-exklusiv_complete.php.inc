<?
// File for generating Contract PDF for DFV Zahnschutz-Exklusiv

define('FAMILY_STATUS_SINGLE', 37);
define('FAMILY_STATUS_MARRIED', 38);
define('FAMILY_STATUS_WIDOWED', 39);
define('FAMILY_STATUS_DIVORCED', 40);

$pdf->SetTextColor(50);

$pagecount = $pdf->setSourceFile($path.'/files/dfv/2019/Antrag_DFV_ZahnSchutz.pdf');
$tplidx = $pdf->importPage(1, '/MediaBox');
$pdf->addPage('P');
$pdf->useTemplate($tplidx, 0, 10, 210, 285);

$this->contractInfoTitle($i, $pdf, $titles[$i]);

#$this->getCompanyLogo(9.0, 0.0, $pdf, $data['contractComplete']['sourcePage'], $i, 40);
$this->getCompanyStamp(5.0, 12, $pdf, $data['contractComplete']['sourcePage'], 8.0, 'horizontal');


$pdf->SetFont($pdfCfg['fontFamily'], '', 12, 0, 2);
$pdf->Text(165, 30.6, '0   1   0   3   1   5');
if ($data['gender'] === 'female') {
    $pdf->Text(38.4, 48.7, 'X');
} else {
    $pdf->Text(52.8, 48.7, 'X');
}
$pdf->Text(40, 55.5, $data['person']['forename']);
$pdf->Text(40, 62.2, $data['person']['surname']);
$pdf->Text(40, 68.7, $data['street']);
$pdf->Text(40, 75.7, $data['postcode']);
$pdf->Text(76, 75.7, $data['city']);

$pdf->Text(39.4, 83, $data['birthdate2'][0]);
$pdf->Text(45.6, 83, $data['birthdate2'][1]);
$pdf->Text(51.8, 83, $data['birthdate2'][3]);
$pdf->Text(58, 83, $data['birthdate2'][4]);
$pdf->Text(63, 83, $data['birthdate2'][6]);
$pdf->Text(69.2, 83, $data['birthdate2'][7]);
$pdf->Text(75.4, 83, $data['birthdate2'][8]);
$pdf->Text(81.6, 83, $data['birthdate2'][9]);


$begin = explode('.', $data['begin']);
$pdf->Text(118.2, 82.4, '0  1');
$pdf->Text(129.2, 82.4, $begin[0][0].'   '.$begin[0][1]);
$pdf->Text(140.0, 82.4, $begin[1][0].'   '.$begin[1][1].'   '.$begin[1][2].'   '.$begin[1][3]);



if ($data['phone']) {
    $pdf->Text(40, 89.5, $data['phone']);
} else {
    if ($i == 0) $this->pdfRect(39.5, 85.1, 54, $pdf, 5);
}
if ($data['email']) {
    $pdf->Text(40, 95.6, $data['email']);
} else {
    if ($i == 0) $this->pdfRect(39.5, 91.5, 54, $pdf, 5);
}

$pdf->Text(40, 112.9, $data['personInsured']['forename']);
$pdf->Text(40, 120.1, $data['personInsured']['surname']);

$pdf->Text(39.4, 126.4, $data['personInsured']['birthdate'][8]);
$pdf->Text(45.6, 126.4, $data['personInsured']['birthdate'][9]);
$pdf->Text(51.8, 126.4, $data['personInsured']['birthdate'][5]);
$pdf->Text(58, 126.4, $data['personInsured']['birthdate'][6]);
$pdf->Text(63, 126.4, $data['personInsured']['birthdate'][0]);
$pdf->Text(69.2, 126.4, $data['personInsured']['birthdate'][1]);
$pdf->Text(75.4, 126.4, $data['personInsured']['birthdate'][2]);
$pdf->Text(81.6, 126.4, $data['personInsured']['birthdate'][3]);


if ($data['personInsured']['salutationLid'] == 10) // female
{
    $pdf->Text(124.4, 127, 'X');
}
else
{
    $pdf->Text(105, 127, 'X');
}

// Gesetzlich krankenversichert
#$pdf->Text(73.2, 149.4, 'X');
if ($i == 0) $this->pdfRect(73.0, 146.5, 2.5, $pdf, 2.5);

switch($pdfTemplate)
{
	case project::gti('dfv-exklusiv'):
		// Tarifvariante Exklusiv
		$pdf->Text(120.2, 170.4, '1   0   0');

	break;
	case project::gti('dfv-basis50'):
		// Tarifvariante Basis
		$pdf->Text(120.2, 170.4, '0   5   0');

	break;
	case project::gti('dfv-komf70'):
		// Tarifvariante Komfort
		$pdf->Text(120.2, 170.4, '0   7   0');

	break;
	case project::gti('dfv-prem90'):
		// Tarifvariante Premium
		$pdf->Text(120.2, 170.4, '0   9   0');

	break;
}

// Price
$pdf->Text(120, 178.3, $data['price'][0]);
$pdf->Text(126, 178.3, $data['price'][1]);
$pdf->Text(133.4, 178.3, $data['price'][3]);
$pdf->Text(139.4, 178.3, $data['price'][4]);

if ($i == 0) $this->pdfRect(50, 189, 11, $pdf, 5);
if ($i == 0) $this->pdfRect(67.4, 189, 45.8, $pdf, 5);
if ($i == 0) $this->pdfRect(120.7, 189, 58, $pdf, 5);
if ($i == 0) $this->pdfRect(38.8, 200.4, 2.5, $pdf, 2.5);
if ($i == 0) $this->pdfRect(61.9, 200.4, 2.5, $pdf, 2.5);
if ($i == 0) $this->pdfRect(92.1, 200.4, 2.5, $pdf, 2.5);
if ($i == 0) $this->pdfRect(120.6, 200.4, 2.5, $pdf, 2.5);
if ($i == 0) $this->pdfRect(37.9, 208, 140, $pdf, 5);

if ($i == 0) $this->pdfRect(37.9, 217.5, 40, $pdf, 5);
if ($i == 0) $this->pdfRect(93.0, 217.5, 100, $pdf, 5);


// Ich willige bis auf Widerruf ...
$pdf->Text(12.4, 233.4, 'X');

if ($i == 0) $this->pdfRect(18.6, 263.9, 40, $pdf, 5);
if ($i == 0) $this->pdfRect(69, 263.9, 50, $pdf, 5);
if ($i == 0) $this->pdfRect(139, 263.9, 50, $pdf, 5);

$pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);
if ($i == 0) $pdf->Text(94, 221.5, 'X');
if ($i == 0) $pdf->Text(71, 268.5, 'X');
if ($i == 0) $pdf->Text(140, 268.5, 'X');


$tplidx = $pdf->importPage(2, '/MediaBox');
$pdf->addPage('P');
$pdf->useTemplate($tplidx);


//$pdf->addPage('P');


