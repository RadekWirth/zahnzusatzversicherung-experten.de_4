<?
// File for generating Contract PDF for Bayrische V.I.P. dental Prestige

    define('CROWNS', $data['contractComplete']['tooth4'] ? $data['contractComplete']['tooth4'] : null);
    define('MISSING_TOOTH', $data['t1'] ? $data['t1']  : null);
    define('AGE_PERSONINSURED', actionHelper::getAge($data['personInsured']['birthdate']));


    if($i==0) {
        $pagecount = $pdf->setSourceFile($path.'/files/bayerische-vdent-34/antrag/die_bayerische_antrag_2014.pdf');
    } else {
        $pagecount = $pdf->setSourceFile($path.'/files/bayerische-vdent-34/antrag/die_bayerische_antrag_2014_bw.pdf');
    }

/**
 * Page 1
 */
    $tplidx = $pdf->importPage(1, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 5, 16, 200);
    $pdf->SetMargins(0, 0, 0);

    $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);

    // gelbe markierungen nur auf r�ckantwort drucken!
    if($i==0) $this->pdfRect(72, 0.5, 68, $pdf);
    $pdf->Text(72, 4.5, $titles[$i]);

    // Logo "DieBayerische"
    if ($i==0) {
        $pdf->Image($path.'/files/logos/die-bayerische/logo_diebayerische.png', 5, 1, 50);
        $pdf->SetTextColor(91, 103, 127);
    } else {
        $pdf->Image($path.'/files/logos/die-bayerische/logo_diebayerische_bw.png', 5, 1, 50);
        $pdf->SetTextColor(91, 103, 127);
    }

    $pdf->AddFont('helvetica','','helvetica.php');
    $pdf->SetFont('helvetica','',14);
    $pdf->Text(7, 17, 'VIP dental prestige');
    $pdf->SetTextColor(0, 0, 0);

    #$this->getCompanyLogo(150.0, 5.0, $pdf, $data['contractComplete']['sourcePage'], $i, 30);
    $this->getCompanyStamp(76.0, 9.5, $pdf, $data['contractComplete']['sourcePage'], 7, 'horizontal', 'wide');

    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

    // 1.AV Nummer
    $pdf->Text(28.7, 28.6, '1  6   3   6  0  2');

    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);

    // Antrag auf Tarif V.I.P. dental
    $pdf->Text(12, 51, "x");
    $bg = explode(".", $data['begin']);
    $pdf->Text(59, 51.6, $bg[0][0]);
    $pdf->Text(63.5, 51.6, $bg[0][1]);
    $pdf->Text(68, 51.6, $bg[1][0]);
    $pdf->Text(71.8, 51.6, $bg[1][1]);
    $pdf->Text(76.1, 51.6, $bg[1][2]);
    $pdf->Text(80.2, 51.6, $bg[1][3]);

    // Antragsteller / Versicherungsnehmer
    ($data['gender']=='male') ? $pdf->Text(13.3, 71.5, 'x') : $pdf->Text(26, 71.5, 'x');
    $bd = explode(".", $data['birthdate']);
    $pdf->Text(53.7, 72, $bd[0][1]);
    $pdf->Text(49.6, 72, $bd[0][0]);
    $pdf->Text(57.4, 72, $bd[1][0]);
    $pdf->Text(61.4, 72, $bd[1][1]);
    $pdf->Text(66.5, 72, $bd[2][0]);
    $pdf->Text(70.4, 72, $bd[2][1]);
    $pdf->Text(74.8, 72, $bd[2][2]);
    $pdf->Text(78.9, 72, $bd[2][3]);

    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);

    $pdf->Text(14, 81.5, $data['namec']);
    $pdf->Text(14, 90.2, $data['street']);
    $pdf->Text(14, 98.7, $data['postcode']);
    $pdf->Text(36, 98.7, $data['city']);

    if(!empty($data['phone'])) {
        $pdf->Text(145, 81.8, $data['phone']);
    } else {
        if($i==0) $this->pdfRect(146, 78.7, 50, $pdf, 3);
    }

    if(!empty($data['email'])) {
        if(strlen($data['email']) > 25) {
            $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
        }
        $pdf->Text(145, 98.7, $data['email']);
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);
    } else {
        if($i==0) $this->pdfRect(145.8, 95.8, 50, $pdf, 3);
    }

    // Zu versichernde Person
    $pdf->Text(62, 118.7, $data['personInsured']['surname']);
    $pdf->Text(62, 126.1, $data['personInsured']['forename']);

    $pdf->Text(127.3, 118.5, $data['birthdate2']);
    ($data['gender2']=='male') ? $pdf->Text(147.7, 125.2, 'x') : $pdf->Text(167, 125.2, 'x');

    // SEPA-Lastschriftmandat
    if($i==0) $this->pdfRect(13.6, 154.4, 58, $pdf, 3);
    if($i==0) $this->pdfRect(75.5, 154.4, 35, $pdf, 3);
    if($i==0) $this->pdfRect(20.2, 161.4, 64, $pdf, 3);
    if($i==0) $this->pdfRect(13.6, 169.2, 108, $pdf, 3);
    if($i==0) $this->pdfRect(119, 153, 3, $pdf, 3);
    if($i==0) $this->pdfRect(130, 169.2, 66, $pdf, 3);
    if($i==0) $pdf->Text(15, 172, 'X');

	// Welcher Tarif?
	$pdfTemplate = isset($pdfTemplate)?$pdfTemplate:$data['idt'];
	switch($pdfTemplate) {
		case project::gti('bayerische-vdent-prestige'):
			$pdf->Text(46.4, 187, 'x');
			break;
		case project::gti('bayerische-vip-dental-komfort'):
    			$pdf->Text(61.2, 187, 'x');
			break;
		case project::gti('bayerische-vip-dental-smart'):
			$pdf->Text(76.0, 187, 'x');
			break;
	}

    $pdf->Text(93, 188, $data['price']);

    // Gesundheitsfragen
    if($i==0) $this->pdfRect(78, 200, 1.7, $pdf, 1.7);
    if($i==0) $this->pdfRect(83.5, 200, 4.5, $pdf, 1.7);
    if($i==0) $this->pdfRect(92.2, 199.7, 1.7, $pdf, 1.7);
    if($i==0) $this->pdfRect(78, 211.2, 1.7, $pdf, 1.7);
    if($i==0) $this->pdfRect(92.2, 211.2, 1.7, $pdf, 1.7);

if($data['contractComplete']['tooth3'] > 0 || null !== CROWNS) {
        if($i==0) $this->pdfRect(78, 219, 1.7, $pdf, 1.7);
        if($i==0) $this->pdfRect(78, 223.9, 1.7, $pdf, 1.7);
        if($i==0) $this->pdfRect(78, 228.1, 1.7, $pdf, 1.7);
        if($i==0) $this->pdfRect(78, 232.2, 1.7, $pdf, 1.7);
        if($i==0) $this->pdfRect(84, 228.1, 4.2, $pdf, 1.7);
        if($i==0) $this->pdfRect(84, 232.8, 4.2, $pdf, 1.7);
        if($i==0) $this->pdfRect(173, 219, 1.7, $pdf, 1.7);
        if($i==0) $this->pdfRect(189, 219, 1.7, $pdf, 1.7);
        if($i==0) $this->pdfRect(179, 223.9, 4.2, $pdf, 1.7);
        if($i==0) $this->pdfRect(179, 228.1, 4.2, $pdf, 1.7);
        if($i==0) $this->pdfRect(179, 232.8, 4.2, $pdf, 1.7);
    }
    if($i==0) $this->pdfRect(147.4, 244, 1.7, $pdf, 1.7);
    if($i==0) $this->pdfRect(159.5, 244, 1.7, $pdf, 1.7);
    if($i==0) $this->pdfRect(147.4, 272, 1.7, $pdf, 1.7);
    if($i==0) $this->pdfRect(159.5, 272, 1.7, $pdf, 1.7);

	if($data['contractComplete']) {
    if(MISSING_TOOTH !== null) {
        $pdf->Text(77.8, 201.7, 'x');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
        $pdf->Text(85, 201.7, MISSING_TOOTH);
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);
    } else {
        $pdf->Text(92, 201.8, 'x');
    }

    if($data['contractComplete']['existDentures'] == 'yes') {
        $pdf->Text(77.8, 213.5, 'x');
    } else {
        $pdf->Text(92.3, 213.5, 'x');
    }

    if($data['contractComplete']['incare'] == 'yes') {
        $pdf->Text(148.8, 246, 'x');
    } else {
        $pdf->Text(159.8, 246, 'x');
    }

    if($data['contractComplete']['periodontitis'] == 'yes') {
        $pdf->Text(148.8, 273.5, 'x');
    } else {
        $pdf->Text(159.8, 273.5, 'x');
    }

    if(CROWNS !== null) {
        $pdf->Text(85, 234.6, CROWNS);
        $pdf->Text(77.4, 234.6, 'x');
    }

	}

/**
 * Page 2
 */
    $tplidx = $pdf->importPage(2, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

    // Keine Leistungsstaffel
    if ((MISSING_TOOTH <= 1 || (CROWNS == 1 || CROWNS == 2)) && CROWNS < 3) {

    }

    // Leistungsstaffel 1
    elseif ((MISSING_TOOTH === null && (CROWNS == 3 || CROWNS == 4)) || (MISSING_TOOTH == 1 && CROWNS <= 2) || MISSING_TOOTH == 2) {
        $pdf->Text(9.3, 215.2, 'x');
        if ($i == 0) $this->pdfRect(85, 213, 45, $pdf, 4);
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+2, 0, 2);
        $pdf->Text(85.5, 217, 'X');
        if (AGE_PERSONINSURED < 18) {
            if ($i == 0) $this->pdfRect(140, 213, 58, $pdf, 4);
            $pdf->Text(141, 217, 'X');
        }
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);
    }

    // Leistungsstaffel 2
    elseif ((MISSING_TOOTH == 3) || (MISSING_TOOTH == 2 && (CROWNS == 1 || CROWNS == 2)) || (MISSING_TOOTH == 1 && (CROWNS == 3 || CROWNS == 4)) || (MISSING_TOOTH === null && (CROWNS == 5 || CROWNS == 6))) {
        $pdf->Text(35, 215.2, 'x');
        if ($i == 0) $this->pdfRect(85, 213, 45, $pdf, 4);
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+2, 0, 2);
        $pdf->Text(85.5, 217, 'X');
        if (AGE_PERSONINSURED < 18) {
            if ($i == 0) $this->pdfRect(140, 213, 58, $pdf, 4);
            $pdf->Text(141, 217, 'X');
        }
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);
    }

    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

/**
 * Page 3
 */
    $tplidx = $pdf->importPage(3, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

    // Einwilligung in die Verwendung von Gesundheitsdaten...
    if($i==0) $this->pdfRect(13.8, 106.5, 1.5, $pdf, 1.5);
    if($i==0) $this->pdfRect(13.8, 173.3, 1.5, $pdf, 1.5);
    if($i==0) $this->pdfRect(13.8, 277, 1.5, $pdf, 1.5);
    if($i==0) $this->pdfRect(112.2, 14.8, 1.5, $pdf, 1.5);

    if($i==0) {
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
        $pdf->Text(60, 169, "oder");
        $pdf->Text(60, 294, "oder");
    }

/**
 * Page 4
 */
    $tplidx = $pdf->importPage(4, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

    // Schlusserkl�rungen des Antragstellers und der zu versichernden Personen
    if($i==0) {
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+3, 0, 2);
        $this->pdfRect(8.8, 209.2, 28, $pdf, 5.2);
        $this->pdfRect(42, 209.2, 49, $pdf, 5.2);
        if(!empty($data['personInsured'])) {
            $this->pdfRect(96, 209.2, 48, $pdf, 5.2);
            $pdf->Text(97, 213.6, 'x');
        }
        $pdf->Text(10, 213.6, 'x');
        $pdf->Text(44, 213.6, 'x');

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);

        $this->pdfRect(8, 268.4, 27, $pdf, 5.2);
        $this->pdfRect(41, 268.4, 97, $pdf, 5.2);
        $pdf->Text(8.4, 234.6, 'x');
        $pdf->Text(8.4, 248.1, 'x');
        $pdf->Text(8.4, 255.5, 'x');

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+3, 0, 2);

        $pdf->Text(10, 273, 'x');
        $pdf->Text(43, 273, 'x');
    }

?>