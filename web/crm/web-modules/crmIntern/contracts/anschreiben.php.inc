<?
		/* VARS */
		$lh = 3.6; // line height
		$lhb = $lh*2;

		$data['contractComplete'] = isset($data['contractComplete']) ? $data['contractComplete'] : null;
		$data['gender'] = isset($data['gender']) ? $data['gender'] : array();

		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		$pdf->AddPage('P', 'A4');
		$pdf->SetMargins(0, 0, 0);


		// set color to black!
		$pdf->SetTextColor(120.0);
		$pdf->SetDrawColor(190.0);
		$pdf->SetLineWidth(0.1);

		$pdf->Text(20.4, 55.5, 'Versicherungsmakler Experten GmbH');
		$pdf->Text(20.4, 59.5, 'Feursstr. 56 / RGB - 82140 Olching');

		// Zeichnen der benötigten Linien
		$pdf->Line(20.0, 45.0, 190.0, 45.0);
		$pdf->Line(135.0, 45.0, 135.0, 257.0);
		$pdf->Line(20.0, 257.0, 190.0, 257.0);


		// set adress bold!
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', 11, 0, 2);

		// set color to black!
		$pdf->SetTextColor(10);

		// Überschriften
		$pdf->Text(138.0, 55.5, 'Anschrift');
		$pdf->Text(138.0, 80.0, 'Kontaktdaten');
		$pdf->Text(20.4, 116.8, 'Ihr Angebot für eine Top-Zahnzusatzversicherung');



		$this->getCompanyLogo(20.0, 20.0, $pdf, $data['contractComplete']['sourcePage'], isset($i) ? $i : 0, 100.0);

	switch($data['gender']) {
		case 'male':
		$pdf->Text(20.4, 64.5, "Herrn");
		break;
		case 'female':
		$pdf->Text(20.4, 64.5, "Frau");
		break;
				}

		$pdf->Text(20.4, 69, isset($data['namec']) ? $data['namec'] : '');
		$pdf->Text(20.4, 73.5, isset($data['street']) ? $data['street'] : '');
		$pdf->Text(20.4, 78, (isset($data['postcode']) ? $data['postcode'] : '')." ".(isset($data['city']) ? $data['city'] : ''));


		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		// rechte Seite
		$pdf->Text(138.0,61.5, 'Versicherungsmakler Experten GmbH');
		$pdf->Text(138.0,65.0, 'Geschäftsführer: Maximilian Waizmann');
		$pdf->Text(138.0,68.5, 'Feursstr. 56 / RGB');
		$pdf->Text(138.0,72.0, '82140 Olching');

		$pdf->Text(138.0,86.0, 'Tel');
		$pdf->Text(138.0,89.5, 'Fax');
		$pdf->Text(146.0,86.0, '08142 - 651 39 28');
		$pdf->Text(146.0,89.5, '08142 - 651 39 29');
		$pdf->Text(138.0,96.0, $this->getEMailBySourcePage($data['contractComplete']['sourcePage']));
		$pdf->Text(138.0,99.5, $this->getWebBySourcePage($data['contractComplete']['sourcePage']));


		switch($data['gender']){
			case 'male':
			$pdf->Text(20.4, 129, "Sehr geehrter Herr ".$data['person']['surname'].",");
			break;
			case 'female':
			$pdf->Text(20.4, 129, "Sehr geehrte Frau ".$data['person']['surname'].",");
			break;
				}
			$pdf->Text(138.0 , 116.8, "Olching, ".date("d.m.Y",time()));


	// main form
		$pdf->SetRightMargin(75.0); $pdf->SetLeftMargin(19.4); 

		$pdf->SetY(132.0);
		$pdf->Write($lh, 'Vielen Dank für das uns entgegengebrachte Vertrauen. Anbei erhalten Sie die Antragsunterlagen und das zugehörige Informationsmaterial.');

		$pdf->SetY($pdf->GetY()+$lhb);
		$pdf->Write($lh, 'Ihre Vorteile beim Abschluss über uns :');

		// block 1
		$pdf->SetLeftMargin(34.0);
		$pdf->SetY($pdf->GetY()+$lhb);
		$pdf->Image($path.'/files/icon-ok.png', 30.0, $pdf->GetY()+0.7, 2.0);
		$pdf->SetFont('','B');
			$pdf->Write($lh, 'absolute Spezialisierung');

		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
		$pdf->Write($lh, ' - Im Gegensatz zu unseren meisten Mitbewerbern sind wir ausschließlich auf das Thema Zahnzusatzversicherung spezialisiert und können Ihnen daher auch komplizierte Fragen sehr schnell und fachkompetent beantworten');
		
		// block 2
		$pdf->SetY($pdf->GetY()+$lhb);
		$pdf->Image($path.'/files/icon-ok.png', 30.0, $pdf->GetY()+0.7, 2.0);
		$pdf->Write($lh, 'Wir lassen Sie nach einem Abschluss nicht im Regen stehen!! Wir unterstützen Sie auch später im Leistungsfall, z.B. wenn es um ');
		$pdf->SetFont('','B');
		$pdf->Write($lh, 'Reklamationen');

		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
		$pdf->Write($lh, ' geht');

		// block 3
		$pdf->SetY($pdf->GetY()+$lhb);
		$pdf->Image($path.'/files/icon-ok.png', 30.0, $pdf->GetY()+0.7, 2.0);
		$pdf->Write($lh, 'unsere ');
		$pdf->SetFont('','B');
		$pdf->Write($lh, 'Preisgarantie');

		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
		$pdf->Write($lh, ' - Sie zahlen bei uns keinen Cent mehr als wenn Sie über die Versicherung direkt abschließen');
		
		// block 4
		$pdf->SetY($pdf->GetY()+$lhb);
		$pdf->Image($path.'/files/icon-ok.png', 30.0, $pdf->GetY()+0.7, 2.0);
		$pdf->SetFont('','B');
		$pdf->Write($lh, 'keine weiteren Kosten für Sie');
		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
		$pdf->Write($lh, ' - alle unsere Dienstleistungen sind für Sie als Endverbraucher kostenlos - Sie zahlen nur die vereinbarten Versicherungsbeiträge');

		$pdf->SetLeftMargin(19.4);
		$pdf->SetY($pdf->GetY()+$lhb);
		$pdf->Write($lh, 'Falls Sie noch Fragen zu unserem Angebot haben, kontaktieren Sie uns einfach');

		$pdf->SetLeftMargin(34.0);
		$pdf->SetY($pdf->GetY()+$lhb);
		$pdf->Image($path.'/files/icon-pfeil.png', 30.0, $pdf->GetY()+0.7, 2.0);
		$pdf->Write($lh, 'per E-Mail an '.$this->getEMailBySourcePage($data['contractComplete']['sourcePage']));
		$pdf->SetY($pdf->GetY()+$lhb);
		$pdf->Image($path.'/files/icon-pfeil.png', 30.0, $pdf->GetY()+0.7, 2.0);
		$pdf->Write($lh, 'oder rufen Sie uns einfach an unter 08142 - 651 39 28');

		$pdf->SetLeftMargin(19.4);
		$pdf->SetY($pdf->GetY()+$lhb);
		$pdf->Write($lh, 'Wir würden uns freuen, Sie bald als neuen Kunden begrüßen zu dürfen.');

		$pdf->SetY($pdf->GetY()+$lhb);
		$pdf->Write($lh, 'Mit freundlichen Grüßen');

		$pdf->Image($path.'/files/sign_mw.png', 20.0, $pdf->GetY()+$lh, 25.0);

		$pdf->SetY($pdf->GetY()+$lhb*2);
		$pdf->SetFont('','B');
		$pdf->Write($lh, 'Maximilian Waizmann');
	// footer
		// reset font to very small
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-4, 0, 2);
		$pdf->SetXY(19.5, 258.0); 
		$pdf->MultiCell(171.0, 2.2, project::gts('antrag'));


	// complete = 2 - drucken mit Duplex
	if(isset($complete) && $complete==2) { $pdf->AddPage('P'); }
