<?php
// File for generating Contract for DKV


	$bonus['nextBonusBase'] = ( ! empty($data['personInsured']['bonus']['nextBonusBase'])) ? $data['personInsured']['bonus']['nextBonusBase'] : null; 
	$bonus['nextBonusDate'] = ( ! empty($data['personInsured']['bonus']['nextBonusDate'])) ? $data['personInsured']['bonus']['nextBonusDate'] : null; 

    // Pr�fung, ob Datei vorhanden ist...
    if ($i != 1) {
		$pagecount = $pdf->setSourceFile($path.'/files/dkv/Antrag_Kurz_VF114_1A_08_14.pdf');
    } else {
        // black & white
        $pagecount = $pdf->setSourceFile($path.'/files/dkv/Antrag_Kurz_VF114_1A_08_14_bw.pdf');
    }

	$tplidx = $pdf->importPage(1, '/MediaBox');

	$pdf->addPage('P');
	$pdf->useTemplate($tplidx, 5, 3.5, 206.5);

		// set title and font to big and bold!
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);

		//gelbe markierungen nur auf r�ckantwort drucken!
		if($i==0) $this->pdfRect(25, 3, 68, $pdf);

		$pdf->Text(25.0, 7, $titles[$i]);

		$this->getCompanyStamp(127.0, 3.5, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']-4, 'horizontal', 'wide');

		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
			$pdf->Text(15, 11, 'X');		
			$pdf->Text(172, 23, 'X');

		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		$pdf->Text(51, 19.5, '898 - 8721');

	// A. Angaben zum Antragsteller
		$pdf->Text(40, 38, $data['namec']);

		$pdf->Text(40, 44.7, $data['street']);
		$pdf->Text(105, 44.7, $data['postcode']);
		$pdf->Text(128, 44.7, $data['city']);

		$bday = explode(".", $data['birthdate']);
		$pdf->Text(16.6, 51.7, $bday[0]);
		$pdf->Text(23.6, 51.7, $bday[1]);
		$pdf->Text(29.6, 51.7, $bday[2]);

		if($data['person']['salutationLid']==10) {
			$pdf->Text(50.6, 50.8, 'x');
		} else {
			$pdf->Text(44.6, 50.8, 'x');
		}

		$pdf->Text(61.5, 51.7, $data['person']['nationCode']);

		if($data['phone'])
			$pdf->Text(94, 51.7, $data['phone']);
		else
			if($i==0) $this->pdfRect(77, 49, 59, $pdf, 3.0);

		if($data['email']) {
			$pdf->Text(142.6, 51, $data['email']);
		} else {
			if($i==0) $this->pdfRect(144, 48.6, 55, $pdf, 3.0);
		}

		if($data['job']==null || $data['job']=='')
		{
			if($i==0) $this->pdfRect(15, 55.7, 83,$pdf, 3.0);
		} else
			$pdf->Text(16.6, 58, $data['job']);

		// Arbeitnehmer, ... gelb markieren
		if($i==0) $this->pdfRect(140.7, 53 , 1.4, $pdf, 1.4);
		if($i==0) $this->pdfRect(140.7, 56, 1.4, $pdf, 1.4);
		if($i==0) $this->pdfRect(162, 53, 1.4, $pdf, 1.4);
		if($i==0) $this->pdfRect(162, 56, 1.4, $pdf, 1.4);
		if($i==0) $this->pdfRect(103.7, 54.9, 34, $pdf, 3);


	// B Angaben zum beantragten Versicherungsschutz
	// 
			// versicherte Person...
			$pdf->Text(32, 73.8, $data['name2']);

			$bday = explode(".", $data['birthdate2']);

			$pdf->Text(155, 73.8, $bday[0]);
			$pdf->Text(162.0, 73.8, $bday[1]);
			$pdf->Text(167.4, 73.8, $bday[2]);

			if ($data['personInsured']['salutationLid'] == 10) {
				$pdf->Text(183.8, 72.8, 'X');
			} else {
				$pdf->Text(177.4, 72.8, 'X');
			} 

			$pdf->Text(190.5, 73.8, $data['personInsured']['nationCode']);

			// Versicherungsbeginn
			$signUp = explode("-", $data['wishedBegin']);
				// reset font
				$pdf->SetFont($pdfCfg['fontFamily'], 'B', 9, 0, 2);
				$pdf->Text(16.6, 93.6, $signUp[2]);
				$pdf->Text(25.0, 93.6, $signUp[1]);
				$pdf->Text(39.3, 93.6, substr($signUp[0], 2,2));

			$this->pdfClean(181.4, 90.6, 20, $pdf, 255, 255, 255, 3.5);
			$this->pdfClean(181.4, 102.2, 20, $pdf, 255, 255, 255, 3.5);

			// Kreizerl KDT
			// reset font
			$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+3, 0, 2);

			// Welcher Tarif?
			$pdfTemplate = isset($pdfTemplate)?$pdfTemplate:$data['idt'];
			switch($pdfTemplate) {
				case project::gti('dkv-kdt85'):
					$pdf->Text(64.3, 86.7, 'x');
					break;
				case project::gti('dkv-kdt85-kdbe'):
					$pdf->Text(64.3, 86.7, 'x');
					$pdf->Text(116.4, 86.7, 'x');
					break;
				case project::gti('dkv-kdt50'):
					$pdf->Text(89.3, 86.7, 'x');
					break;
				case project::gti('dkv-kdt50-kdbe'):
					$pdf->Text(89.3, 86.7, 'x');
					$pdf->Text(116.4, 86.7, 'x');
					break;
				case project::gti('dkv-kdbe'):
					$pdf->Text(116.4, 86.7, 'x');
					break;
				case project::gti('dkv-kdt'):
					$pdf->Text(141.6, 86.7, 'x');
					break;
			}


		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);
			//$pdf->Text(70.5, 98.8, $data['price'] .' �');
			$pdf->Text(186.5, 92.8, $data['price'] .' �');
			$pdf->Text(186.5, 104.8, $data['price'] .' �');

		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);


		if ($bonus !== null && ! empty($bonus['nextBonusBase'])) {
			if ($i==0) $pdf->SetTextColor(255,69,0);  
			
			$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
			$pdf->Text(138, 110, 'Beitragsanpassung zum '. date('d.m.Y', strtotime($bonus['nextBonusDate'])) .':');
			$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
			$pdf->Text(191, 110, $bonus['nextBonusBase'] .' Euro');
			$pdf->SetTextColor(25,25,25);  	
			$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);
		}
		
	if($i==0 && !isset($data[personInsured][insurance]))
		$this->pdfRect(22.8, 129.6, 130, $pdf, 3.6);
	else {
		if($i==0) $this->pdfRect(130.0, 129.6, 24, $pdf, 3.6);
		$pdf->Text(25.5, 132.4, $data['personInsured']['insurance']);
	}

		if($i==0) $this->pdfRect(157.3, 130.3, 1.6, $pdf, 1.6);
		if($i==0) $this->pdfRect(167.8, 130.3, 1.6, $pdf, 1.6);


	// D Fragen zur Gesundheit

		if($data['contractComplete']['incare'] == 'yes') {
			$pdf->Text(189.7, 180.4, 'x');
		} else {
	if($data['contractComplete'])
			$pdf->Text(184.2, 180.4, 'x');
		}
		
		if($data['contractComplete']['tooth1'] >0) {
			$pdf->Text(189.7, 188.7, 'x');
		} else {
	if($data['contractComplete'])
			$pdf->Text(184.2, 188.7, 'x');
		}
		$pdf->Text(184.0, 192.4, $data['contractComplete']['tooth1']);

		if($i==0) $this->pdfRect(16.0, 200.9, 185.6, $pdf, 3);
		if($i==0) $this->pdfRect(16.0, 205.4, 185.6, $pdf, 3);

	// E Beitragszahlung und Schlu�erkl�rung
		if($i==0) $this->pdfRect(47.3, 216.7, 1.6, $pdf, 1.6);
		if($i==0) $this->pdfRect(66.9, 216.7, 1.6, $pdf, 1.6);
		if($i==0) $this->pdfRect(90.7, 216.7, 1.6, $pdf, 1.6);
		if($i==0) $this->pdfRect(112.3, 216.7, 1.6, $pdf, 1.6);

		// SEPA beigef�gt
		$pdf->Text(13.8, 221.5, 'X');


	//$this->pdfClean(15, 207, 20, $pdf, 245, 245, 221, 15.2);

		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);

		// Unterschriften
		if($i==0) $this->pdfRect(16.0, 252.8, 125, $pdf, 4);
		$pdf->Text(17, 256.4, 'X');
		$pdf->Text(52, 256.4, 'X');
		if($i==0) $this->pdfRect(16.0, 288.8, 104, $pdf, 4);
		$pdf->Text(17, 292, 'X');
		$pdf->Text(52, 292, 'X');

// SEPA
	$pagecount = $pdf->setSourceFile($path.'/files/dkv/A83_50065679_Antrag_SEPA.pdf');
	$tplidx = $pdf->importPage(1, '/MediaBox');

	$pdf->addPage('P');
	$pdf->useTemplate($tplidx, 5, 3.5, 206.5);

	// Versicherungsnehmer...
	$pdf->Text(25, 35, $data['name2']);

	if ($data['personInsured']['salutationLid'] == 10) {
		$pdf->Text(25.8, 151.9, 'x');
	} else {
		$pdf->Text(39.7, 152.0, 'x');
	} 

	$pdf->Text(69, 151.5, $data['person']['forename']);
	$pdf->Text(27, 161.5, $data['person']['surname']);
	$pdf->Text(27, 172.5, $data['street']);
	$pdf->Text(27, 183.0, $data['postcode'].' '.$data['city']);


	if($i==0) $this->pdfRect(35, 202.1, 8, $pdf, 4);
	if($i==0) $this->pdfRect(48, 202.1, 19, $pdf, 4);
	if($i==0) $this->pdfRect(72.7, 202.1, 19, $pdf, 4);
	if($i==0) $this->pdfRect(98, 202.1, 19, $pdf, 4);
	if($i==0) $this->pdfRect(123, 202.1, 19, $pdf, 4);
	if($i==0) $this->pdfRect(147, 202.1, 9, $pdf, 4);

	if($i==0) $this->pdfRect(26.5, 213, 19, $pdf, 4);
	if($i==0) $this->pdfRect(55.5, 213, 24, $pdf, 4);

	if($i==0) $this->pdfRect(26.5, 223.6, 162, $pdf, 4);
	if($i==0) $this->pdfRect(26.5, 240.9, 162, $pdf, 4);

	if ($i == 0) {
		$pdf->Text(27, 244.7, 'X');
		$pdf->Text(128, 244.7, 'X');
	}

    	// Pr�fung, ob Datei vorhanden ist...
    	if ($i != 1) {
		$pagecount = $pdf->setSourceFile($path.'/files/dkv/contract_dkv_2014.pdf');
    	} else {
        // black & white
        $pagecount = $pdf->setSourceFile($path.'/files/dkv/contract_dkv_2014_bw.pdf');
    	}

	$tplidx = $pdf->importPage(2, '/MediaBox');

	$pdf->addPage('P');
	$pdf->useTemplate($tplidx, 5, 4, 206);

	$tplidx = $pdf->importPage(3, '/MediaBox');

	$pdf->addPage('P');
	$pdf->useTemplate($tplidx, 5, 4, 206);
?>