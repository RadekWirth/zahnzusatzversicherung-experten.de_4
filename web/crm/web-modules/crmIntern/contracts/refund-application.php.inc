<?php

// Provisional Data
$insuranceAddress = array(
	'name' => $data['insurance']['name'],
	'postCode' => $data['insurance']['postCode'],
	'postBox' => $data['insurance']['postBox'],
	'street' => $data['insurance']['street'],
	'city' => $data['insurance']['city']
);

$idt = $data['tariff']['idt']; 

$pdf->cMargin = 0;

// Add Fonts
$pdf->AddFont('PTSans-Regular', '','PTSans-Regular.php');
$pdf->AddFont('PTSans-Bold', '','PTSans-Bold.php');

/**
 * Page 1
 */
$pagecount = $pdf->setSourceFile($path.'/files/erstattungsantrag.pdf');
$tplidx = $pdf->importPage(1, '/MediaBox');

$pdf->addPage('P');
$pdf->useTemplate($tplidx, 0, 0);  

$pdf->SetTextColor(58, 105, 135);

$this->setPTSansFont(20, 'R', $pdf);
$pdf->Text(10, 16, 'Erstattungsantrag');
$this->setPTSansFont(11, 'R', $pdf);
  
$textFieldX = 8;
$ln = 6;
$y = 35;
$x = 10;
$pdf->SetTextColor(40);
$pdf->Image($this->getCompanyLogoFile($idt), 140, 28, 0, 22);
$pdf->Text($x, $y, $insuranceAddress['name']);
if ($insuranceAddress['postBox']) {
	$pdf->Text($x, $y + $ln, 'Postfach '. $insuranceAddress['postBox']);
}
if ($insuranceAddress['street']) {
	$pdf->Text($x, $y + $ln * 2, $insuranceAddress['street']);
}
$pdf->Text($x, $y + $ln * 3, $insuranceAddress['postCode'] . ' ' . $insuranceAddress['city']);

$pdf->SetTextColor(58, 105, 135);
$this->setPTSansFont(13, 'B', $pdf);
$pdf->Text($x, $y += 29, 'Ihre Angaben');
$pdf->SetY($y += 3);
$pdf->SetX($x);
$this->setPTSansFont(11, 'R', $pdf);
$pdf->MultiCell(190, 5, html_entity_decode('Bitte lesen Sie auch die Hinweise auf der R�ckseite und Unterschreiben Sie den Antrag, bevor Sie die Unterlagen einreichen!'), 0, 'L', 0);

$pdf->SetY($y += 20);
$this->textField('Name, Vorname', $x, $y, 90, $pdf);
$this->textField('Stra�e, Hausnummer', $x, $y + $textFieldX, 90, $pdf);
$this->textField('Telefonnummer', $x, $y + $textFieldX * 2, 90, $pdf);
$this->textField('Versicherungsnummer', $x + 100, $y, 90, $pdf);
$this->textField('PLZ, Wohnort', $x + 100, $y + $textFieldX, 90, $pdf);

$pdf->SetTextColor(58, 105, 135);
$this->setPTSansFont(13, 'B', $pdf);
$pdf->Text($x, $y += 35, 'Erstattung von Behandlungs-Rechnungen');

$pdf->SetY($y += 13);
$pdf->Line($x, $y, $x + 190, $y);
$pdf->Line($x, $y + $textFieldX/1.2, $x + 190, $y + $textFieldX/1.2);
$pdf->Line($x, $y + $textFieldX/1.2 * 2, $x + 190, $y + $textFieldX/1.2 * 2);
$pdf->Line($x, $y + $textFieldX/1.2 * 3, $x + 190, $y + $textFieldX/1.2 * 3);
$pdf->Line($x + 15, $y - 7, $x + 15, $y + 25);
$pdf->Line($x + 160, $y - 7, $x + 160, $y + 25);

$pdf->SetTextColor(60);
$pdf->SetFont('Arial', '', 9, 0, 2);
$pdf->Text($x + 3, $y - 2, 'Nr.');
$pdf->Text($x + 20, $y - 2, 'Zahnarzt / Labor');
$pdf->Text($x + 164, $y - 2, 'Betrag');
$pdf->Text($x + 134, $y += 25, 'Gesamtbetrag');

$this->setPTSansFont(11, 'R', $pdf);
$pdf->Text($x, $y += 7, 'Bitte �berweisen Sie den Erstattungsbeitrag auf folgendes Konto:');
$pdf->SetFont('Arial', '', 10, 0, 2);

$pdf->Rect($x, $y + 5, 3.5, 3.5);
$pdf->Rect($x, $y + 11, 3.5, 3.5);
$pdf->Text($x + 6, $y + 7.7, 'das Ihnen bekannte Beitragszahlungskonto');
$pdf->Text($x + 6, $y + 13.7, 'folgendes Konto (abweichend):');
 
$pdf->SetY($y += 30);
$this->textField('Kontoinhaber', $x, $y, 90, $pdf);
$this->textField('IBAN (International Bank Account Number)', $x, $y + $textFieldX, 190, $pdf);
$this->textField('Name des Kreditinsituts', $x, $y + $textFieldX * 2, 190, $pdf);
$this->textField('BIC (Business Identifier Code)', $x + 100, $y, 90, $pdf);

$pdf->SetTextColor(58, 105, 135);
$this->setPTSansFont(13, 'B', $pdf);
$pdf->Text($x, $y += 35, 'Vorlage eines Heil- und Kostenplans');
$pdf->SetY($y += 3);
$pdf->SetX($x);
$pdf->SetTextColor(40); 
$this->setPTSansFont(11, 'R', $pdf);
$pdf->MultiCell(190, 5, html_entity_decode('Bitte �berpfr�fen Sie den von meiner gesetzlichen Krankenkasse bearbeiteten zahn�rztlichen Heil- und Kostenplan.
Ich erwarte Ihre Antwort in sp�testens 4 Wochen.'), 0, 'L', 0); 

$pdf->SetY($y += 34);
$this->textField('Ort / Datum', $x, $y, 90, $pdf);
$this->textField('Unterschrift', $x + 100, $y, 90, $pdf);
$pdf->SetFont('Arial', 'B', 13, 0, 2);
$pdf->Text($x + 102, $y - 1, 'X');

if ($prefilled == 1) {
	$pdf->SetTextColor(40); 
	$pdf->SetFont('Arial', '', 9, 0, 2); 
	$pdf->Text(12, 86, $data['person']['surname'] .', '. $data['person']['forename']); 
	$pdf->Text(12, 94, $data['address']['street']); 
	$pdf->Text(12, 102, $data['contact']['phone']); 

	$pdf->Text(113, 94, $data['address']['postcode'] .' '. $data['address']['city']); 
}


/**
 * Page 2
 */
$pdf->addPage('P');

$y = 6;
$x = 10;

$pdf->SetTextColor(58, 105, 135);
$this->setPTSansFont(13, 'B', $pdf);
$pdf->Text($x, $y += 18, 'Erstattung von Rechnungen');
$pdf->SetY($y += 3);
$pdf->SetX($x);
$pdf->SetTextColor(40); 
$this->setPTSansFont(11, 'R', $pdf);
$pdf->MultiCell(190, 5, html_entity_decode('Nach der Abschluss einer Behandlung oder Teilschrittes, bekommen Sie von Ihrem Zahnarzt eine Rechnung in Doppelter Ausfertigung (Original und Duplikat).'), 0, 'L', 0);
$this->setPTSansFont(11, 'B', $pdf);
$pdf->MultiCell(190, 10, html_entity_decode('Bitte reichen Sie immer die Original-Rechnung ein!'), 0, 'L', 0);
$this->setPTSansFont(11, 'R', $pdf);
$pdf->MultiCell(190, 5, html_entity_decode('In der Regel gilt ein Zahlungsziel von 3-4 Wochen. Der Versicherer erstattet immer an den Versicherungsnehmer.'), 0, 'L', 0);
