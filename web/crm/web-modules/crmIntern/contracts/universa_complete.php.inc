<?
// File for generating Contract PDF for Universa Dent Privat

if(!$complete && file_exists($path.'/web-modules/crmIntern/contracts/universa.php.inc'))
{
	include('universa.php.inc'); 
} else {
        $pagecount = $pdf->setSourceFile($path.'/files/universa/antrag/2024/Universa - neuer Antrag.pdf');
        $pdf->addPage('L');
        $tplidx = $pdf->importPage(1, '/MediaBox');
        $pdf->useTemplate($tplidx, 0.6, 0.6, 297.0);
           $pdf->SetMargins(0, 0, 0);

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+2, 0, 2);

        if($i==0) $this->pdfRect(86.0, 0, 62, $pdf);
        $pdf->Text(87.0, 4.0, $titles[$i]);


        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

        // $this->pdfClean(145, 20.8, 20, $pdf, 255, 255, 255, 4, 'F');
        // $this->pdfClean(170, 20.8, 20, $pdf, 255, 255, 255, 4, 'F');

        // AV-Nummer
        $pdf->Text(35.5, 13.7, "54920"); 

        // Antragsteller
        $pdf->Text(24, 44.5, $data['person']['surname']);
        $pdf->Text(125, 44.6, $data['person']['forename']);

        if($data['gender']=="female")
            $pdf->Text(20.2, 38.6, "x");
        else
            $pdf->Text(12.4, 38.6, "x");

        $pdf->Text(162, 38.6, $data['birthdate']);

        $pdf->Text(16, 51, $data['street']);

        $pdf->Text(104, 51, $data['postcode']);
        $pdf->Text(125, 51, $data['city']);




        // zu versichernde Person
        $pdf->Text(22, 75.8, $data['personInsured']['surname']);
        $pdf->Text(125, 75.8, $data['personInsured']['forename']);
        $pdf->Text(162, 69.9, $data['birthdate2']);
        if($data['gender2']=="female")
            $pdf->Text(32.5, 69.4, "x");
        else
            $pdf->Text(24.5, 69.4, "x");



        // Begin
        //$this->pdfClean(155.3, 64.0, 34, $pdf, 255,255,255, 5.2);
        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+2, 0, 2);

        $pdf->Text(152, 20.3, substr($data['begin'], 0, 2));
        $pdf->Text(164, 20.3, substr($data['begin'], 5, 2));

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-1, 0, 2);

        // Welcher Tarif?
        $pdfTemplate = isset($pdfTemplate)?$pdfTemplate:$data['idt'];
        switch($pdfTemplate) {
            case project::gti('universa-uni-dent-privat'):
                // Person 1 bei uni-dent|Privat
                $pdf->Text(125.8, 137.8, "X");

                // uni-dent|Komfort durchstreichen
                $pdf->SetLineWidth(3.0);
                $pdf->SetDrawColor(246,246,246);
                $pdf->Line(12, 148, 80, 106);
                break;
            case project::gti('universa-uni-dent-komfort'):
                // Person 1 bei uni-dent|Komfort
                $pdf->Text(47, 137.8, "X");

                // uni-dent|Privat durchstreichen
                $pdf->SetLineWidth(3.0);
                $pdf->SetDrawColor(246,246,246);
                $pdf->Line(124, 148, 185, 106);
                break;
            }

        if($i==0) {
            $this->pdfRect(27.3, 171.5, 1.7, $pdf, 1.7);
            $this->pdfRect(36.8, 171.5, 1.7, $pdf, 1.7);
        }



    // Seite 2
        $pdf->addPage('L');
        $tplidx = $pdf->importPage(2, '/MediaBox');
        //$pdf->useTemplate($tplidx, 6.5, 6, 283);
        $pdf->useTemplate($tplidx, 0.6, 0.6, 300.0);

        $this->pdfClean(200, 170, 85, $pdf, 255, 255, 255, 50, 'F');


        $this->getCompanyStamp(204.0, 160, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']);
        
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

        // Vor- und Zuname Kontoinhaber
        if($i==0) $this->pdfRect(103.7, 114.7, 90, $pdf, 3);
        // Adresse Kontoinhaber
        if($i==0) $this->pdfRect(103.7, 123.6, 90, $pdf, 3);
        // Geldinstitut
        if($i==0) $this->pdfRect(103.7, 132.6, 90, $pdf, 3);
        // IBAN
        if($i==0) $this->pdfRect(110.7, 143.2, 81, $pdf, 3);

        // Ort und Datum
        if($i==0) $this->pdfRect(103.7, 150.1, 90, $pdf, 3);
        // Kontoinhaber
        if($i==0) $this->pdfRect(103.7, 160.0, 90, $pdf, 3);

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);
        #if($i==0) $pdf->Text(104.7, 125.7, "X");


        //Unterschrift
        if($i==0) $this->pdfRect(201.7, 35.9, 88, $pdf);
        if($i==0) $this->pdfRect(201.7, 46.6, 88, $pdf);
        if($i==0) $this->pdfRect(201.7, 57.9, 88, $pdf);
        #if($i==0) $pdf->Text(102.2, 171.7, "X");
        #if($i==0) $pdf->Text(102.2, 183, "X");

        if (project::gti('universa-uni-dent-privat'))
        {
            #$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);
            #if($i==0) $pdf->Text(196, 158.5, '*');
            #$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
            #if($i==0) $pdf->Text(100, 195, '* auf die Beitragsanpassung zum 1.1.2018 wurde ich hingewiesen!');
        }




        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

}
