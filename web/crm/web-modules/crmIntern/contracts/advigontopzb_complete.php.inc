<?
// File for generating Contract PDF for ADVIGON
        if($i==0) {
            $pagecount = $pdf->setSourceFile($path.'/files/advigon-premium-zgp/antrag/advigon_antrag_20131014.pdf');
        } else {
            $pagecount = $pdf->setSourceFile($path.'/files/advigon-premium-zgp/antrag/advigon_antrag_20131014_bw.pdf');
        }

        $tplidx = $pdf->importPage(1, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);

        //gelbe markierungen nur auf rückantwort drucken!
        if($i==0) $this->pdfRect(75, 4, 68, $pdf);

        $pdf->Text(75.0, 8.0, $titles[$i]);

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        $pdf->Text(33, 23.9, '80001938');

    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
        // Maximilian Waizmann, Begonienstr 1 ...
        $pdf->Text(75, 14.0, "Versicherungsmakler Experten GmbH");
        $pdf->Text(75, 16.5, "Feursstr. 56 / RGB");
        $pdf->Text(75, 19.0, "82140 Olching");

        $pdf->Text(75, 22.0, "Tel.: 08142 - 651 39 28");
        $pdf->Text(75, 24.5, "info@zahnzusatzversicherung-experten.de");

    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);


    //Antrag auf ..
        $pdf->Text(10.5, 43.2, 'X');
    //Neuantrag
        $pdf->Text(15.3, 50.8, 'X');

    //Antragsteller

        // Links
        $pdf->Text(12.5, 105, $data['name']);

        if($data['job']==null || $data['job']=='')
        {
            if($i==0) $this->pdfRect(12,112.8,124,$pdf);
        }
        else
            $pdf->Text(12.5, 117, $data['job']);

        $pdf->Text(12.5, 129, $data['street']);
        $pdf->Text(12.5, 141, $data['postcode']);
        $pdf->Text(38.5, 141, $data['city']);

        // Rechts
        if($data['gender']=='male') {
            $pdf->Text(180.3, 106.3, 'X');
        }
        else {
            $pdf->Text(190, 106.3, 'X');
        }
        $pdf->Text(141.5, 117, $data['birthdate']);
        if($data['phone'])
            $pdf->Text(141.5, 129, $data['phone']);
        else if($i==0) $this->pdfRect(140.6, 125, 58.4, $pdf);
        if($data['email'])
            $pdf->Text(141.5, 141, $data['email']);
        else if($i==0) $this->pdfRect(140.6, 137.5, 58.4,$pdf);



    //Zu versichernde Personen
        $name = $data['name'];
        if(!empty($data['name2b'])) {
            $name = $data['name2b'];
        }
        $pdf->Text(12.5, 169.5, $name); unset($name);

        $bdate = $data['birthdate'];
        if(!empty($data['birthdate2'])) {
            $bdate = $data['birthdate2'];
        }
        $pdf->Text(97, 169.5, $bdate); unset($bdate);

        if($data['gender2'])
            $genderid = 'gender2';
        else $genderid = 'gender';

        if($data[$genderid]=='male')
            $pdf->Text(183, 170.8, 'X');
        else
            $pdf->Text(192.8, 170.8, 'X');
        unset($genderid);

        if(!isset($data['personInsured']))
        {
            $pdf->Text(127, 169.5, $data['job']);
            if(empty($data['job']) && $i==0)
                $this->pdfRect(126.0, 165.5, 42, $pdf);
        } else {
            $pdf->Text(127, 169.5, $data['job2']);
            if(empty($data['job2']) && $i==0)
                $this->pdfRect(126.0, 165.5, 42, $pdf);
        }

        if(!isset($data['personInsured']))
        {
            $pdf->Text(127, 178.5, $data['insurance']);
            if(empty($data['insurance']) && $i==0)
                $this->pdfRect(126.0, 175.0, 72.0, $pdf);
        } else {
            $pdf->Text(127, 178.5, $data['insurance2']);
            if(empty($data['insurance2']) && $i==0)
                $this->pdfRect(126.0, 175.0, 72.0, $pdf);
        }

        //Wurde in den letzten 3 Jahren eine Versicherung gekündigt?
        if($i==0) $this->pdfRect(179.5, 186.7, 2.7 ,$pdf, 2.7);
        if($i==0) $this->pdfRect(189.2, 186.7, 2.7 ,$pdf, 2.7);
        if($i==0) $this->pdfRect(179.6, 197.1, 2.7 ,$pdf, 2.7);
        if($i==0) $this->pdfRect(189.3, 197.1, 2.7 ,$pdf, 2.7);

            //Welcher Versicherungsschutz?
            if($i==0) $this->pdfRect(100.6, 203.1, 2.5,$pdf,2.5);
            if($i==0) $this->pdfRect(152.6, 203.1, 2.5,$pdf,2.5);

        //Bestand eine Versicherung?
        //if($i==0) $this->pdfRect(179.5, 216.6, 2.7 ,$pdf, 2.7);
        //if($i==0) $this->pdfRect(189.2, 216.6, 2.7 ,$pdf, 2.7);

            //Angaben
            //if($i==0) $this->pdfRect(14.8, 232, 55,$pdf);
            //if($i==0) $this->pdfRect(74, 232, 55,$pdf);
            //if($i==0) $this->pdfRect(133.8, 232, 30,$pdf);
            //if($i==0) $this->pdfRect(169, 232, 30,$pdf);

        if($i==0) $this->pdfClean(10, 228, 190.0, $pdf, 0, 0, 0, 0.3, 'F');
        if($i==0) $this->pdfClean(10, 238, 190.0, $pdf, 0, 0, 0, 0.3, 'F');
        if($i==0) $this->pdfClean(10, 248, 190.0, $pdf, 0, 0, 0, 0.3, 'F');
        if($i==0) $this->pdfClean(10, 259, 190.0, $pdf, 0, 0, 0, 0.3, 'F');
        if($i==0) $this->pdfClean(10, 265, 190.0, $pdf, 0, 0, 0, 0.3, 'F');

    //SEITE 2
        $tplidx = $pdf->importPage(2, '/MediaBox');
        $pdf->addPage('P');
        if($i==0) {
            $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
        } else {
            $pdf->useTemplate($tplidx, 1, 1.2, 209.9);
        }

        //Name, Vorname Antragsteller
        $pdf->Text(109, 13.6, $data['name']);

    //Versicherungsschutz

        //Baustein Zahngesundheit Premium und Zahnersatz Premium
        $pdf->Text(71.6, 43.7, 'X');
        $pdf->Text(123.6, 48.6, 'X');

        $pdf->Text(77.5, 78, $data['price']);
        $pdf->Text(35, 78, $data['begin']);

        //Zahlungsweise monatlich
        $pdf->Text(123.0, 79.7, 'X');

    // Beitragszahlung
        // //Kontoinhaber
        // if($i==0) $this->pdfRect(11.5,158.8,82.5,$pdf);
        // //Kontonummer
        // if($i==0) $this->pdfRect(98,158.8,57.5,$pdf);
        // //BLZ
        // if($i==0) $this->pdfRect(160,158.8,38.4,$pdf);

        // //Bankinstitut
        // if($i==0) $this->pdfRect(11.5,171,82.5,$pdf);
        // //Signature
        // if($i==0) $this->pdfRect(98,171,100,$pdf);

    //Gesundheitsverhältnisse und Zusatzfragen

        // Frage 1
        if($i==0) $this->pdfRect(165.7, 123.8, 2.5,$pdf,2.5);
        if($i==0) $this->pdfRect(173.4, 123.8, 2.5,$pdf,2.5);

        if($data['contractComplete']['incare'] == 'yes' || $data['contractComplete']['orthodontics'] == 'yes') {
            $pdf->Text(166.2, 125.8, 'x');
        } else {
            $pdf->Text(173.5, 125.8, 'x');
        }

        // Frage 2
        if(actionHelper::getAge($data['personInsured']['birthdate']) > 7 || !isset($data['personInsured'])) {
            if($i==0) $this->pdfRect(165.7, 141.8, 2.5,$pdf,2.5);
            if($i==0) $this->pdfRect(173.4, 141.8, 2.5,$pdf,2.5);
            // 20140222 Soll laut Maximilian pauschal immer mit NEIN markiert werden
/*            if($data['contractComplete']['docControl'] != 'yes') {
                $pdf->Text(165.7, 143.4, 'x');
            } else {*/
                $pdf->Text(173.5, 143.4, 'x');
/*            }*/
        }

        // Frage 3
        if(actionHelper::getAge($data['personInsured']['birthdate']) > 15 || !isset($data['personInsured'])) {
            if($i==0) $this->pdfRect(165.7, 158.8, 2.5,$pdf,2.5);
            if($i==0) $this->pdfRect(173.4, 158.8, 2.5,$pdf,2.5);
            if($data['contractComplete']['peridontitis'] == 'yes' || $data['contractComplete']['biteSplint'] == 'yes') {
                $pdf->Text(165.7, 160.6, 'x');
            } else {
                $pdf->Text(173.5, 160.6, 'x');
            }
        }


        // Frage 4
        if(actionHelper::getAge($data['personInsured']['birthdate']) > 15 || !isset($data['personInsured'])) {
            $Q4_missingT = $data['contractComplete']['tooth1'] + $data['contractComplete']['tooth3'];
	     if($i==0) $this->pdfRect(163.2, 189.1, 2.5, $pdf, 2.5);
            if($i==0) $this->pdfRect(171.0, 189.1, 2.5, $pdf, 2.5);
            if($i==0) $this->pdfRect(163.7, 202, 16,$pdf,4);
            if($Q4_missingT > 0) {
                $pdf->Text(163.7, 191, 'x');
		  $pdf->Text(163.7, 198, $Q4_missingT);
            } else {
                $pdf->Text(171.5, 191, 'x');
            }
        }

        // Frage 5
        if(actionHelper::getAge($data['personInsured']['birthdate']) > 15 || !isset($data['personInsured'])) {
            if($i==0) $this->pdfRect(165.7, 234, 2.5,$pdf,2.5);
            if($i==0) $this->pdfRect(173.5, 234, 2.5,$pdf,2.5);
            if($data['contractComplete']['tooth4'] > 6 && $data['contractComplete']['tooth5'] == 1) {
                $pdf->Text(165.7, 236.4, 'x');
            } else {
                $pdf->Text(173.5, 236.4, 'x');
            }
        }



    //SEITE 3
        $tplidx = $pdf->importPage(3, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        //Name, Vorname Antragsteller
        $pdf->Text(109, 13.6, $data['name']);





    //SEITE 4
        $tplidx = $pdf->importPage(4, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        //Name, Vorname Antragsteller
        $pdf->Text(109, 13.6, $data['name']);

    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+16, 0, 2);

        if($i==0) {
            $pdf->Text(7, 92, '!');
            $pdf->Text(7, 163, '!');
        }
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        if($i==0) $this->pdfRect(68.9,88,2.3,$pdf,2.3);
        if($i==0) $this->pdfRect(68.8,159.3,2.3,$pdf,2.3);
        if($i==0) $this->pdfRect(68.8,253.2,2.3,$pdf,2.3);


    //SEITE 5
        $tplidx = $pdf->importPage(5, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        //Name, Vorname Antragsteller
        $pdf->Text(109, 13.6, $data['name']);

        //Markieren der Datumsfelder
        $agePersonInsured = actionHelper::getAge($data['personInsured']['birthdate']);

            $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);

            // Antragsteller Ort, Datum, Unterschrift
            if($i==0) $this->pdfRect(11.8,124.2,75,$pdf);
            if($i==0) $pdf->Text(12, 128, 'x');
            if($i==0) $this->pdfRect(90.5,124.2,107,$pdf);
            if($i==0) $pdf->Text(91, 128, 'x');

            if($agePersonInsured > 17) {
                // Mitzuversichernder Person I, Ort, Datum, Unterschrift
                if($i==0) $this->pdfRect(11.8,141,75,$pdf);
                if($i==0) $pdf->Text(12, 144.8, 'x');
                if($i==0) $this->pdfRect(90.5,141,107,$pdf);
                if($i==0) $pdf->Text(91, 144.8, 'x');
            } else {
                if($agePersonInsured > 15 && $agePersonInsured < 18) {
                    // Gesetzlich vertretene Person I, Ort, Datum, Unterschrift
                    if($i==0) $this->pdfRect(11.8,157,75,$pdf);
                    if($i==0) $pdf->Text(12, 160.8, 'x');
                    if($i==0) $this->pdfRect(90.5,157,107,$pdf);
                    if($i==0) $pdf->Text(91, 160.8, 'x');
                }
                    // Gesetzlicher Vertreter von vertretener Person I, Ort, Datum, Unterschrift
                    if($i==0) $this->pdfRect(11.8,170.6,75,$pdf);
                    if($i==0) $pdf->Text(12, 174.4, 'x');
                    if($i==0) $this->pdfRect(90.5,170.6,107,$pdf);
                    if($i==0) $pdf->Text(91, 174.4, 'x');
            }

        if($i==0) $this->pdfClean(10, 190.5, 190.0, $pdf, 0, 0, 0, 0.3, 'F');
        if($i==0) $this->pdfClean(10, 207.0, 190.0, $pdf, 0, 0, 0, 0.3, 'F');
        if($i==0) $this->pdfClean(10, 220.3, 190.0, $pdf, 0, 0, 0, 0.3, 'F');


    //SEITE 6 --- SEPA
        $tplidx = $pdf->importPage(6, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        //Name, Vorname Antragsteller
        $pdf->Text(109, 13.6, $data['name']);

        // Name, Vorname (Kontoinhaber)
        $pdf->Text(12, 113, $data['namec']);

        // Anschrift
        $pdf->Text(12, 125, $data['postcode'] .' '. $data['city'] .', '. $data['street']);

        //if($i==0) $this->pdfRect(11.5,109.5,187.5,$pdf);
        //if($i==0) $this->pdfRect(11.5,121.5,187.5,$pdf);
        if($i==0) $this->pdfRect(11.5,133.5,187.5,$pdf);

        if($i==0) {
            $x = 17.5;
            for($c = 0; $c < 20; $c++) {
                $this->pdfRect($x += 5.6,145.5,4.9,$pdf);
            }
            $x = 133.2;
            for($c = 0; $c < 11; $c++) {
                $this->pdfRect($x += 5.6,145.5,4.9,$pdf);
            }
        }

        if($i==0) $this->pdfRect(11.5,157.5,75,$pdf);
        if($i==0) $this->pdfRect(91,157.5,108.5,$pdf);

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);

        if($i==0) $pdf->Text(13, 162, 'x');
        if($i==0) $pdf->Text(92, 162, 'x');

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

    //SEITE 7
        $tplidx = $pdf->importPage(7, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        $pdf->Text(109, 13.6, $data['name']);

        //Markieren der Datumsfelder
        if($i==0) $this->pdfRect(10.8,204.5,76,$pdf);
        if($i==0) $this->pdfRect(10.8,217.6,76,$pdf);

        //Markieren der Unterschriftfelder
        if($i==0) $this->pdfRect(89.5,204.5,110,$pdf);
        if($i==0) $this->pdfRect(89.5,217.6,110,$pdf);

        //Hinzufügen von X
    if($i==0) {

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);

        $pdf->Text(11, 209.6, 'x'); $pdf->Text(90, 209.6, 'x');
        $pdf->Text(11, 222, 'x'); $pdf->Text(90, 222, 'x');

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        //Hinzufügen München als Ort
        $pdf->Text(12, 234.5, 'Olching, ');
    }

    //SEITE 8
        $tplidx = $pdf->importPage(8, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);