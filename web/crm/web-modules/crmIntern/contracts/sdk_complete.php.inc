<?php
// File for generating Contract PDF for SDK

        /*
         * Constants
         */
        // SQUARE XY
        define('X_Y', 1.5);

        // RECT Y
        define('Y', 2.7);

if(!$complete && file_exists($path.'/web-modules/crmIntern/contracts/sdk.php.inc'))
{
	include('sdk.php.inc');
} else {


        $data['person']['age'] = actionHelper::getAge($data['contractComplete']['birthdate']);

        $pagecount = $pdf->setSourceFile($path.'files/sdk/antrag/2024/Antrag_Zahn-Zusatz_Makler_SDK_01.24_1.770azi.pdf');




        $tplidx = $pdf->importPage(1, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
        $pdf->SetMargins(0, 0, 0);
	 
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);   
	 #$this->getCompanyStamp(30, 170, $pdf, 'zzv', 12);

	 /* Verm. Nr verbergen */
 	 #$this->pdfClean(173.0, 42.2, 10, $pdf, 255, 255, 255, 6);
	 $pdf->Text(174.0, 50.0, '903305'); // +18

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);

        if($i==0) $this->pdfRect(75.0, 3.0, 70, $pdf);
        $pdf->Text(75.0, 6.0, $titles[$i]);



        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);    

        if($data['gender']=='male') {
            $pdf->Text(12.0, 59.8, '1');
        } else {
            $pdf->Text(12.0, 59.8, '2');
        }
        
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
        $pdf->Text(27, 60.0, $data['name']);

        $pdf->Text(27.0, 71.6, $data['street']);

        $pdf->Text(27.0, 82.0, $data['postcode'].'                 '.$data['city']);

        //Telefonnummer
        if(empty($data['phone']) || $data['phone']=='')
        {
            if($i==0) $this->pdfRect(127.8, 68.9, 55, $pdf, 3);
        } else
            $pdf->Text(130.5, 71.6, $data['phone']);

        //eMail
        if(empty($data['email']) || $data['email']=='')
        {
            if($i==0) $this->pdfRect(127, 79, 53, $pdf, 3);
        }
        else 
            $pdf->Text(130.0, 82.0, $data['email']);

        #$pdf->Text(162, 80.4, $data['mobile']);

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);    

        	$pdf->Text(20, 100.8, $data['begin']);

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2); 

    // zu versichernde Person
            // setze marker bei Antragsteller / VN
            $pdf->Text(23.1, 122.9, $data['name2']);            
	     $pdf->Text(86.1, 122.9, $data['birthdate2']); 

        if($data['gender2']=='male') {
            $pdf->Text(111.0, 121.2, 'x');
        } else {
            $pdf->Text(111.0, 124.2, 'x');
        }
	 if($i==0) $this->pdfRect(140, 120.2, 37.5, $pdf, Y);


	 /* Art und Umfang ... verbergen */
	 $this->pdfClean(9.0, 175.5, 200, $pdf, 255, 255, 255, 21.8);

	 /* Kreuzerl entfernen */
	 #$this->pdfClean(10.5, 177.0, 2.1, $pdf, 255, 255, 255, 2.1);

	 switch($pdfTemplate)
	 {
		case project::gti('sdk-zahn100-zp1'):
			 $pdf->Text(110.4, 231.5, 'X');
    		        $pdf->Text(119.4, 231.5, $data['bonusbase']);
		break;

		case project::gti('sdk-zahn90-zp9'):
			 $pdf->Text(110.4, 226.5, 'X');
    		        $pdf->Text(119.4, 226.5, $data['bonusbase']);
		break;

		case project::gti('sdk-zahn70-zp7'):
			 $pdf->Text(110.4, 221.5, 'X');
    		        $pdf->Text(119.4, 221.5, $data['bonusbase']);
		break;

		case project::gti('sdk-zahn50-zp5'):
			 $pdf->Text(110.4, 216.5, 'X');
		        $pdf->Text(119.4, 216.5, $data['bonusbase']);
		break;

	 }




	 $p = explode(',', $data['bonusbase']);
        $pdf->Text(185.4, 242.3, $p[0].'   '.$p[1]);


        if ($data['bonus_tooth'] == '0,00')
        {
            #$pdf->Text(79.5, 257.7, $data['bonusbase']);
        }
        else 
        {
            if ($data['t1'] > 0)
            {
		     #$pdf->Text(156.5, 243.0, $data['t1']);

                   #$pdf->Text(155.5, 285.0, $data['t1'] * 20);

		$pdf->Text(75.7, 252.0, 'Gesamtbeitrag inkl. Risikozuschlag für fehlende Zähne'); 
		$pdf->Text(183.7, 252.0, $data['price'].' €');

            } else {}

         }

/*
	 if($i==0) $this->pdfRect(152.8, 239.7, 6.5, $pdf, Y);
	 if($i==0) $this->pdfRect(166.3, 240.3, Y, $pdf, Y);

	 if($i==0) $this->pdfRect(152.5, 245.0, Y, $pdf, Y);
	 if($i==0) $this->pdfRect(166.3, 245.0, Y, $pdf, Y); 

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+20, 0, 2);    

              if($i==0) $this->pdfRect(3.0, 216.6, 4, $pdf, 10);
	       if($i==0) $this->pdfRect(203.0, 216.6, 4, $pdf, 10);

        	$pdf->Text(4, 223.8, '!');
		$pdf->Text(203, 223.8, '!');

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2); 
*/


	 /* Seite verbergen */
	 #$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 245, 255, 6);


// SEITE 2
        $tplidx = $pdf->importPage(2, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
        $pdf->SetMargins(0, 0, 0);


        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+20, 0, 2);    

              if($i==0) $this->pdfRect(3.0, 45.6, 4, $pdf, 10);
	       if($i==0) $this->pdfRect(203.0, 45.6, 4, $pdf, 10);

        	$pdf->Text(4, 52.8, '!');
		$pdf->Text(203, 52.8, '!');

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2); 


	 if($i==0) $this->pdfRect(152.8, 43.7, 6.5, $pdf, Y);
	 if($i==0) $this->pdfRect(165.3, 43.7, Y, $pdf, Y);

	 if($i==0) $this->pdfRect(152.5, 70.0, 6.5, $pdf, Y);
	 #if($i==0) $this->pdfRect(166.3, 70.0, Y, $pdf, Y); 



        if ($data['bonus_tooth'] == '0,00')
        {
            #$pdf->Text(79.5, 257.7, $data['bonusbase']);
        }
        else 
        {
            if ($data['t1'] > 0)
            {
		     $pdf->Text(156.5, 46.0, $data['t1']);

                   $pdf->Text(155.5, 72.0, $data['t1'] * 20);

		#$pdf->Text(75.7, 252.0, 'Gesamtbeitrag inkl. Risikozuschlag für fehlende Zähne'); 
		#$pdf->Text(183.7, 252.0, $data['price'].' €');

            } else {}

         }


	 // Zahlweise
	 #$pdf->Text(120.3, 14.5, 'X');
	 if($i==0) $this->pdfRect(126.8, 102.4, Y, $pdf, Y);
	 if($i==0) $this->pdfRect(144.2, 102.4, Y, $pdf, Y);


	 if($i==0) $this->pdfRect(15.3, 125.0, 60, $pdf, 4);
	 if($i==0) $this->pdfRect(15.3, 140.0, 50, $pdf, 4);
	 if($i==0) $this->pdfRect(100.3, 140.0, 20, $pdf, 4);

	 if($i==0) $this->pdfRect(15.3, 160.0, 60, $pdf, 4);
	 if($i==0) $this->pdfRect(15.3, 176.0, 40, $pdf, 4);
	 if($i==0) $this->pdfRect(78.3, 176.0, 40, $pdf, 4);

	 if($i==0) $this->pdfRect(15.3, 204.0, 40, $pdf, 4);
	 if($i==0) $this->pdfRect(70.3, 204.0, 40, $pdf, 4);


	    

	/* Seite verbergen */
	#$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);



// SEITE 3
        $tplidx = $pdf->importPage(3, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);


	 if($i==0) $this->pdfRect(19.3, 183.0, 38, $pdf, 4);
	 if($i==0) $this->pdfRect(70.3, 183.0, 60, $pdf, 4);

	 if($i==0) $this->pdfRect(19.3, 247.0, 38, $pdf, 4);
	 if($i==0) $this->pdfRect(70.3, 247.0, 60, $pdf, 4);



	 #if($i==0) $this->pdfRect(54.1, 244.8, Y, $pdf, Y);
	 #if($i==0) $this->pdfRect(79.1, 244.8, Y, $pdf, Y); 


	/* Seite verbergen */
	#$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);


// SEITE 4
        $tplidx = $pdf->importPage(4, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

	 if($i==0) $this->pdfRect(54.1, 186, Y, $pdf, Y);
	 #if($i==0) $this->pdfRect(79.1, 8.6, Y, $pdf, Y); 

	 if($i==0) $this->pdfRect(54.1, 231.4, Y, $pdf, Y);
	 #if($i==0) $this->pdfRect(79.1, 68.0, Y, $pdf, Y); 


	/* Seite verbergen */
	#$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);

// SEITE 5
        $tplidx = $pdf->importPage(5, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

	 if($i==0) $this->pdfRect(54.1, 25.0, Y, $pdf, Y);
	 
	 if($i==0) $this->pdfRect(54.1, 37.2, Y, $pdf, Y);



	 if($age2 && $age2 < 18 && $age2 >=16)
	 {

		 #if($i==0) $this->pdfRect(15.3, 60.0, 30, $pdf, 6);
		 #if($i==0) $this->pdfRect(63.3, 60.0, 60, $pdf, 6);

		 #if($i==0) $this->pdfRect(15.3, 77.0, 30, $pdf, 6);
		 #if($i==0) $this->pdfRect(63.3, 77.0, 60, $pdf, 6);

	 }

       #$pdf->SetDrawColor(0);
	#$pdf->Line(10, 270, 200, 60);


	/* Seite verbergen */
	#$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);

// SEITE 6
        $tplidx = $pdf->importPage(6, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

	 if($i==0) $this->pdfRect(15.3, 35.0, 20, $pdf, 6);
	 if($i==0) $this->pdfRect(63.3, 35.0, 30, $pdf, 6);


       #$pdf->SetDrawColor(0);
	#$pdf->Line(10, 120, 200, 80);

	/* Seite verbergen */
	#$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);

// SEITE 7
        $tplidx = $pdf->importPage(7, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

       #$pdf->SetDrawColor(0);
	#$pdf->Line(10, 270, 200, 60);

	/* Seite verbergen */
	#$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);

// SEITE 8
        $tplidx = $pdf->importPage(8, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

       #$pdf->SetDrawColor(0);
	#$pdf->Line(10, 270, 200, 60);

	/* Seite verbergen */
	#$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);

// SEITE 9
        $tplidx = $pdf->importPage(9, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

       #$pdf->SetDrawColor(0);
	#$pdf->Line(10, 270, 200, 60);

	/* Seite verbergen */
	#$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);

if($complete == 2)
	$pdf->addPage('P');

}
