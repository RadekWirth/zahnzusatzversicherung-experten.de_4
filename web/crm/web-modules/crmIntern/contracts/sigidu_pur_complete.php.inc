<?php
// File for generating Contract PDF for Signal Iduna Pur (Start & Plus & Top & Basis)

        /*
         * Constants
         */
        // SQUARE XY
        define('X_Y', 1.2);

        // RECT Y
        define('Y', 2.7);

        $data['person']['age'] = actionHelper::getAge($data['contractComplete']['birthdate']);

        $pagecount = $pdf->setSourceFile($path.'/files/signal-iduna/antrag/2023/ada92360dc05ed492e3e74d3478407989aec9da5.pdf');

        $tplidx = $pdf->importPage(1, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
        $pdf->SetMargins(0, 0, 0);
        

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);

        if($i==0) $this->pdfRect(75.0, 3.0, 70, $pdf);
        $pdf->Text(75.0, 7.0, $titles[$i]);

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-1, 0, 2);

        // -> Seite 2
        $pdf->Text(88, 14.0, "Versicherungsmakler Experten GmbH");

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-3, 0, 2);


        // Bereits Kunde?
        $pdf->Text(151.2, 39.6, 'X');


        //Selbstständiger/Angestellter/Arbeiter
        
        if($i==0) {
            $this->pdfRect(142.8, 58.8, X_Y, $pdf, X_Y);
            $this->pdfRect(162.0, 58.8, X_Y, $pdf, X_Y);
            $this->pdfRect(177.3, 58.8, X_Y, $pdf, X_Y);
            $this->pdfRect(189.6, 58.8, X_Y, $pdf, X_Y);
        }
        

        //freiwillig versichert?
        if($i==0) {
             $this->pdfRect(72.4, 67.4, X_Y, $pdf, X_Y);
             $this->pdfRect(91.2, 67.4, X_Y, $pdf, X_Y);
             $this->pdfRect(72.4, 70.0, X_Y, $pdf, X_Y);
             $this->pdfRect(72.4, 72.4, X_Y, $pdf, X_Y);
        }

        //mark section end
        $pdf->Text(150.5, 62.7, 'x'); // Beihilfe
        $pdf->Text(176.1, 62.7, 'x'); // Heilfuersorge

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);    

        if($data['gender']=='male') {
            $pdf->Text(16.4, 49.2, 'x');
        } else {
            $pdf->Text(16.4, 52.0, 'x');
        }

        if($data['widowed']) {
            $pdf->Text(16.4, 63.4, 'x');
        } else if($data['married']) {
            $pdf->Text(16.4, 60.4, 'x');
        } else {
            $pdf->Text(16.4, 57.6, 'x');
        }

        
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
        $pdf->Text(29.0, 50.0, $data['name']);
        $pdf->Text(143.8, 50.0, $data['birthdate']);

        if($data['nation']=='DEU') {
            $pdf->Text(188.0, 47.2, 'x');
        } elseif(empty($data['nation'])) {
            $this->pdfRect(167.5, 47.0, 30, $pdf, 3.5);
        } else {
            $pdf->Text(168, 50.0, $data['nation']);
        }
 

        $pdf->Text(29.0, 57.0, $data['street']);

        $pdf->Text(116, 57.0, $data['postcode'].' '.$data['city']);

        if( ! empty($data['job'])) {
            $pdf->Text(29.0, 63.2, $data['job']);
        } else {
           if($i==0) $this->pdfRect(28.5, 60.0, 30, $pdf, 4);
        }

        //Telefonnummer
        if(!empty($data['phone']) || $data['phone']=='')
        {
            if($i==0) $this->pdfRect(15.8, 87.6, 39, $pdf, 3);
        } else
            $pdf->Text(16.5, 90.2, $data['phone']);

        //eMail
        if(!empty($data['email']) || $data['email']=='')
        {
            if($i==0) $this->pdfRect(102, 87.6, 53, $pdf, 3);
        } else 
            $pdf->Text(105.0, 90.2, $data['email']);

        $pdf->Text(162, 90.2, $data['mobile']);

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);    


        if(!empty($data['insurance'])) {
            $pdf->Text(29.0, 75.5, $data['insurance']);
        } else {
            if($i==0) $this->pdfRect(28.5, 72.9, 40, $pdf, 3.0);
        }

        // Insured since
        #if($i==0) $this->pdfRect(132, 80.2, 20, $pdf, 3.0);






    // 3 Beitragszahlung

        //mark section

        //Zahlungsweise
        if($i==0) {
            $this->pdfRect(36.1, 117.7, X_Y, $pdf, X_Y);
            $this->pdfRect(50.4, 117.7, X_Y, $pdf, X_Y);
            $this->pdfRect(66.8, 117.7, X_Y, $pdf, X_Y);
            $this->pdfRect(83.7, 117.7, X_Y, $pdf, X_Y);
        }

        //Konto
        if($i==0) {
            $this->pdfRect(17.5, 147.9, 100, $pdf, Y);
            $this->pdfRect(156.0, 147.9, 38, $pdf, Y);
            $this->pdfRect(17.5, 156.2, 89, $pdf, Y);
            $this->pdfRect(117.0, 156.2, 24, $pdf, Y);
            $this->pdfRect(149.5, 156.2, 47, $pdf, Y);
        }
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
        if($i==0) $pdf->Text(149, 158.5, 'X');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);


        // check for child
        if($data['name2b'] != $data['name']) {
            // setze marker bei Antragsteller / VN
            $pdf->Text(116.1, 43.5, 'X');            
            $data['personInsured']['age'] = actionHelper::getAge($data['personInsured']['birthdate']);

        //mark section

	     //Beziehung zum Antragsteller
	     if($i==0) {
                $this->pdfRect(28.8, 220.0, X_Y, $pdf, X_Y); //+24,5
                $this->pdfRect(45.1, 220.0, X_Y, $pdf, X_Y);
                $this->pdfRect(65.9, 220.0, X_Y, $pdf, X_Y);
                $this->pdfRect(86.8, 220.0, X_Y, $pdf, X_Y);
                $this->pdfRect(103.8, 220.0, X_Y, $pdf, X_Y);
                $this->pdfRect(122.2, 220.0, X_Y, $pdf, X_Y);

                $this->pdfRect(28.8, 222.4, X_Y, $pdf, X_Y);
                $this->pdfRect(45.1, 222.4, X_Y, $pdf, X_Y);
                $this->pdfRect(65.9, 222.4, X_Y, $pdf, X_Y);
                $this->pdfRect(86.8, 222.4, X_Y, $pdf, X_Y);
                $this->pdfRect(103.8, 222.4, X_Y, $pdf, X_Y);

                $this->pdfRect(28.8, 225.3, X_Y, $pdf, X_Y);
                $this->pdfRect(100.8, 225.3, 30, $pdf, 3);

		  $this->pdfRect(28.8, 228.6, X_Y, $pdf, X_Y);
		  $this->pdfRect(48.8, 228.6, 50, $pdf, 3);

	     }


        //Selbstständiger/Firma/Verein

            if($i==0) {
	            $this->pdfRect(142.8, 189.3, X_Y, $pdf, X_Y);
	      	     $this->pdfRect(161.5, 189.3, X_Y, $pdf, X_Y);
      		     $this->pdfRect(177.0, 189.3, X_Y, $pdf, X_Y);
       	     $this->pdfRect(189.1, 189.3, X_Y, $pdf, X_Y);
            }
            

            //freiwillig versichert?
            if($i==0) {
                $this->pdfRect(72.3, 198.2, X_Y, $pdf, X_Y);
                $this->pdfRect(91.1, 198.2, X_Y, $pdf, X_Y);
                $this->pdfRect(72.3, 200.4, X_Y, $pdf, X_Y);
                $this->pdfRect(72.3, 203.2, X_Y, $pdf, X_Y);
            }
                        
                //mark section end


                
                $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);

                $pdf->Text(29.0, 180.8, $data['name2b']);
                $pdf->Text(29.0, 187, $data['street'].', '.$data['postcode'].' '.$data['city']);
                $pdf->Text(148.0, 180.8, $data['birthdate2']);
                if($data['gender2']=='male')    {
                    $pdf->Text(16.5, 183.9, 'X');
                    $pdf->Text(16.5, 203.8, 'X');
                } else {
                    $pdf->Text(16.5, 186.7, 'X');
                    $pdf->Text(16.5, 206.5, 'X');
                }

                if($data['widowed2']) {
                    $pdf->Text(16.5, 198.2, 'X');
                } else if($data['married2']) {
                    $pdf->Text(16.5, 195.2, 'X');
                } else {
                    $pdf->Text(16.5, 192.3, 'X');
                }

                $pdf->Text(151.3, 193.7, 'x'); // Beihilfe
                $pdf->Text(178.3, 193.9, 'x'); // Heilfuersorge
                
                if($data['nation2']=='DEU') {
                    $pdf->Text(189.5, 177.3, 'x');
                } elseif(empty($data['nation2'])) {
                    $this->pdfRect(167.5, 177.2, 30, $pdf, 3.5);
                } else {
                    $pdf->Text(168, 180.2, $data['nation2']);
                }

                if(!empty($data['job2'])) {
                    $pdf->Text(29.0, 193.5, $data['job2']);
                } else {
                    if($i==0) $this->pdfRect(28.5, 191, 50, $pdf, 3);
                }

                if(!empty($data['insurance2'])) {
                    $pdf->Text(29.0, 206.0, $data['insurance2']);
                } else {
                    if($i==0) $this->pdfRect(28.5, 203.8, 40, $pdf, 3);
                }

                // Insured since
                #if($i==0) $this->pdfRect(180, 222.2, 20, $pdf, 3.5);

                #if($i==0) $this->pdfRect(163, 186.0, 26, $pdf, 3);
                $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        }
        // end child
        else {
            // setze marker bei Antragsteller / VN
            $pdf->Text(70.1, 43.5, 'X');
        }


/*
// 2 Zu versichernde Personen
        // check for child
        if($data['name2b'] != $data['name']) {
            // setze marker bei Antragsteller / VN
            $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
            $pdf->Text(112.6, 45.8, 'X');            
            $data['personInsured']['age'] = actionHelper::getAge($data['personInsured']['birthdate']);

        //mark section
        //Selbstständiger/Firma/Verein

            if($i==0) {
                $this->pdfRect(138.5, 180.4, X_Y, $pdf, X_Y);
                $this->pdfRect(156.2, 180.4, X_Y, $pdf, X_Y);
                $this->pdfRect(170.8, 180.4, X_Y, $pdf, X_Y);
                $this->pdfRect(182.8, 180.4, X_Y, $pdf, X_Y);
            }
           

            //freiwillig versichert?
            if($i==0) {
                $this->pdfRect(88.3, 186.6, X_Y, $pdf, X_Y);
                $this->pdfRect(88.3, 189.0, X_Y, $pdf, X_Y);
                $this->pdfRect(108.4, 186.6, X_Y, $pdf, X_Y);
            }

	     //Beziehung zum Antragsteller
	     if($i==0) {
                $this->pdfRect(28.8, 195.2, X_Y, $pdf, X_Y);
                $this->pdfRect(44.1, 195.2, X_Y, $pdf, X_Y);
                $this->pdfRect(64.7, 195.2, X_Y, $pdf, X_Y);
                $this->pdfRect(84.8, 195.2, X_Y, $pdf, X_Y);
                $this->pdfRect(100.8, 195.2, X_Y, $pdf, X_Y);
                $this->pdfRect(118.6, 195.2, X_Y, $pdf, X_Y);

                $this->pdfRect(28.8, 198.2, X_Y, $pdf, X_Y);
                $this->pdfRect(44.1, 198.2, X_Y, $pdf, X_Y);
                $this->pdfRect(64.7, 198.2, X_Y, $pdf, X_Y);
                $this->pdfRect(84.8, 198.2, X_Y, $pdf, X_Y);
                $this->pdfRect(100.8, 198.2, X_Y, $pdf, X_Y);

                $this->pdfRect(28.8, 201.4, X_Y, $pdf, X_Y);
                $this->pdfRect(98.8, 200.8, 30, $pdf, 3);

		  $this->pdfRect(28.8, 204.8, X_Y, $pdf, X_Y);
		  $this->pdfRect(48.8, 204.5, 50, $pdf, 3);

	     }
                        
                //mark section end


                
                $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);

                $pdf->Text(29.0, 171.8, $data['name2b']);
                $pdf->Text(29.0, 178.5, $data['street'].', '.$data['postcode'].' '.$data['city']);
                $pdf->Text(146.0, 171.8, $data['birthdate2']);

                if($data['gender2']=='male')    {
                    $pdf->Text(16.9, 174.5, 'X');
		      $pdf->Text(16.9, 195.8, 'X');
                } else {
                    $pdf->Text(16.9, 177.7, 'X');
                    $pdf->Text(16.9, 199.0, 'X');
                }

                if($data['widowed2']) {
                    $pdf->Text(16.9, 190.0, 'X');
                } else if($data['married2']) {
                    $pdf->Text(16.9, 186.8, 'X');
                } else {
                    $pdf->Text(16.9, 183.9, 'X');
                }

                $pdf->Text(146.3, 184.8, 'X'); // Beihilfe
                $pdf->Text(172.3, 184.8, 'X'); // Heilfuersorge
                
                if($data['nation2']=='DEU') {
                    $pdf->Text(169, 172.0, 'deutsch');
                } else if(isset($data['nation2'])) {
                    $pdf->Text(169.0, 172.0, $data['nation2']);
                } else {
                    if($i==0) $this->pdfRect(163.5, 168.3, 30, $pdf, 4);
                }

                if(isset($data['job2']) && strlen($data['job2'])>0) {
                    $pdf->Text(29.0, 185.0, $data['job2']);
                } else {
                    if($i==0) $this->pdfRect(28.5, 182.3, 50, $pdf, 3);
                }

                if(isset($data['insurance2']) && strlen($data['insurance2'])>0 ) {
                    $pdf->Text(29.0, 191, $data['insurance2']);
                } else {
                    if($i==0) $this->pdfRect(28.5, 188.0, 40, $pdf, 3);
                }
                // Insured since
                if($i==0) $this->pdfRect(174, 188.2, 19, $pdf, 3);

                #if($i==0) $this->pdfRect(163, 202.0, 26, $pdf, 3);
                $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        }
        // end child
        else 
            // setze marker bei Antragsteller / VN
            $pdf->Text(68.5, 45.8, 'X');

     $pdf->Text(68.5, 45.8, 'X');
        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
*/

       // $this->getCompanyStamp(100.0, 257.7, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize'], 'horizontal', 'wide');

// SEITE 2
        $tplidx = $pdf->importPage(2, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        $this->getCompanyStamp(100.0, 5, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize'], 'horizontal', 'wide');



        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        $pdf->Text(35.0, 6.0, $data['name']);
        $pdf->Text(35.0, 10.9, $data['signupDate']);

        $pdf->Text(170, 30.5, '01.'. $data['begin']);


        

        // choose tariff
        if($pdfTemplate == project::gti('sigidu-zahn-start-pur')){
            if($data['name2b'] == $data['name']) {
                $pdf->Text(17.4, 70.5, 'X'); // ZahnStartPur
                $pdf->Text(56.8, 70.5, $data['price']);  // ZahnSTARTpur
            } else {
                $pdf->Text(79.6, 70.5, 'X');
                $pdf->Text(118.2, 70.5, $data['price']);
            }
        }
        else if($pdfTemplate == project::gti('sigidu-zahn-plus-pur')){
            if($data['name2b'] == $data['name']) {
                $pdf->Text(17.4, 75.5, 'X'); // ZahnPlusPur
                $pdf->Text(56.8, 75.6, $data['price']);  // ZahnPluspur
            } else {
                $pdf->Text(79.6, 75.5, 'X');
                $pdf->Text(118.2, 75.6, $data['price']); 
            }
        }
        else if($pdfTemplate == project::gti('sigidu-zahn-top-pur')) {
            if($data['name2b'] == $data['name']) {
                $pdf->Text(17.4, 85.4, 'X'); // ZahnTOPPur
                $pdf->Text(56.8, 85.4, $data['price']);  // ZahnTOPpur
              } else {
                $pdf->Text(79.6, 85.4, 'X');
                $pdf->Text(118.2, 85.4, $data['price']); 
            }
        }
        else if($pdfTemplate == project::gti('signal-iduna-zahn-exklusiv-pur')) {
            if($data['name2b'] == $data['name']) {
                #$pdf->Text(17.4, 65.4, 'X'); // ZahnBasisPur
                #$pdf->Text(17.4, 70.5, 'X'); // ZahnStartPur
                #$pdf->Text(17.4, 75.5, 'X'); // ZahnPlusPur
                #$pdf->Text(17.4, 80.5, 'X'); // ZahnPlus
                #$pdf->Text(17.4, 85.4, 'X'); // ZahnTOPPur
                #$pdf->Text(17.4, 90.5, 'X'); // ZahnTOP
                $pdf->Text(17.4, 95.6, 'X'); // Exkl. PUR
                #$pdf->Text(17.4, 100.6, 'X'); // Exkl.

                #$pdf->Text(56.8, 65.4, $data['price']);  // ZahnBASISpur
                #$pdf->Text(56.8, 70.5, $data['price']);  // ZahnSTARTpur
                #$pdf->Text(56.8, 75.6, $data['price']);  // ZahnPluspur
                #$pdf->Text(56.8, 80.6, $data['price']);  // ZahnPlus
                #$pdf->Text(56.8, 85.4, $data['price']);  // ZahnTOPpur
                #$pdf->Text(56.8, 90.5, $data['price']);  // ZahnTOP
                $pdf->Text(56.8, 95.6, $data['price']);  // ZahnExkl.pUR
                #$pdf->Text(56.8, 100.6, $data['price']);  // ZahnExkl.
            } else {
                $pdf->Text(79.6, 95.6, 'X');
                $pdf->Text(118.2, 95.6, $data['price']); 
            }
        }
        else if($pdfTemplate == project::gti('signal-iduna-zahn-exklusiv')) {
            if($data['name2b'] == $data['name']) {
                $pdf->Text(17.4, 100.6, 'X'); // Exkl.
                $pdf->Text(56.8, 100.6, $data['price']);  // ZahnExkl.
            } else {
                $pdf->Text(79.6, 100.6, 'X');
                $pdf->Text(118.2, 100.6, $data['price']); 
            }
        }

	
	if($data['name2b'] == $data['name']) {
		$pdf->Text(56.8, 140.7, $data['price']); 
	} else {
		$pdf->Text(118.2, 140.7, $data['price']); 
	}
	
	$pdf->Text(184.8, 148.0, $data['price']); 



        // Hiermit erkläre ich...
        $pdf->Text(20.1, 217.8, 'X');


// SEITE 3
        $tplidx = $pdf->importPage(3, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        $this->getCompanyStamp(100.0, 5, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize'], 'horizontal', 'wide');


        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        $pdf->Text(35.0, 6.0, $data['name']);
        $pdf->Text(35.0, 10.9, $data['signupDate']);

        //ja / nein
      
        if($data['name2b'] != $data['name']) {  //mark section
            if($i==0) {

                    $this->pdfRect(175.2, 28.4, X_Y, $pdf, X_Y);
                    $this->pdfRect(181.5, 28.4, X_Y, $pdf, X_Y);

                    $this->pdfRect(175.2, 37.2, X_Y, $pdf, X_Y);
                    $this->pdfRect(181.5, 37.2, X_Y, $pdf, X_Y);

                    $this->pdfRect(175.2, 40.2, X_Y, $pdf, X_Y);
                    $this->pdfRect(181.5, 40.2, X_Y, $pdf, X_Y);

                    $this->pdfRect(175.2, 42.8, X_Y, $pdf, X_Y);
                    $this->pdfRect(181.5, 42.8, X_Y, $pdf, X_Y);

                    $this->pdfRect(123.0, 45.6, 5.0, $pdf, 2.5);
                    $this->pdfRect(123.0, 51.4, 5.0, $pdf, 2.5);

                    $this->pdfRect(175.2, 64.4, X_Y, $pdf, X_Y);
                    $this->pdfRect(181.5, 64.4, X_Y, $pdf, X_Y);

			$pdf->Text(171.6, 77.0, 'x');


			$this->pdfRect(156.6, 93.2, X_Y, $pdf, X_Y);
                    	$this->pdfRect(164.6, 93.2, X_Y, $pdf, X_Y);

                 }
            } else {
                if($i==0) {


                    $this->pdfRect(162.2, 28.4, X_Y, $pdf, X_Y);
                    $this->pdfRect(168.4, 28.4, X_Y, $pdf, X_Y);



                    $this->pdfRect(162.2, 37.2, X_Y, $pdf, X_Y);
                    $this->pdfRect(168.4, 37.2, X_Y, $pdf, X_Y);

                    $this->pdfRect(162.2, 40.2, X_Y, $pdf, X_Y);
                    $this->pdfRect(168.4, 40.2, X_Y, $pdf, X_Y);

                    $this->pdfRect(162.2, 42.8, X_Y, $pdf, X_Y);
                    $this->pdfRect(168.4, 42.8, X_Y, $pdf, X_Y);

                    $this->pdfRect(105.0, 45.6, 5.0, $pdf, 2.5);
                    $this->pdfRect(105.0, 51.4, 5.0, $pdf, 2.5);

                    $this->pdfRect(162.2, 64.4, X_Y, $pdf, X_Y);
                    $this->pdfRect(168.4, 64.4, X_Y, $pdf, X_Y);

			$pdf->Text(159.0, 77.0, 'x');

			$this->pdfRect(136.6, 93.2, X_Y, $pdf, X_Y);
                    	$this->pdfRect(144.6, 93.2, X_Y, $pdf, X_Y);



		      if($pdfTemplate == project::gti('signal-iduna-zahn-exklusiv-pur')) {
			
		      }
                }

        }





// SEITE 4
        $tplidx = $pdf->importPage(4, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        $this->getCompanyStamp(100.0, 5, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize'], 'horizontal', 'wide');


        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
        $pdf->Text(35.0, 6.0, $data['name']);
        $pdf->Text(35.0, 10.9, $data['signupDate']);



        // Ich willige ein...
        #$pdf->Text(16.5, 21.8, 'X');

// SEITE 5
        $tplidx = $pdf->importPage(5, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        $this->getCompanyStamp(100.0, 5, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize'], 'horizontal', 'wide');

        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
        $pdf->Text(35.0, 6.0, $data['name']);
        $pdf->Text(35.0, 10.9, $data['signupDate']);


        // Unterschriften
        
        // Antragsteller
        if($i==0) $this->pdfRect(19.5, 67.3, 28.0, $pdf, Y); //219.3
        if($i==0) $this->pdfRect(52.0, 67.3, 44.0, $pdf, Y);
        $pdf->Text(52, 69.5, 'X');



        // Unterschrift der mitzuversichernden Person ab 16 Jahre
        if(isset($data['personInsured']['age']))
        {
            if($data['personInsured']['age'] >= 16)
            {
                if($i==0) $this->pdfRect(102.5, 67.3, 44.0, $pdf, Y);
                $pdf->Text(103, 69.5, 'X');
            }
        }

        // Antragsteller minderjährig oder versicherte Person minderjährig
        if($data['person']['age'] <= 18 || ($data['personInsured']['age'] <=18) && (isset($data['personInsured']['age'])))
        {
            if($i==0) $this->pdfRect(151, 67.3, 44.0, $pdf, Y);
            $pdf->Text(151, 69.5, 'X');

            if($data['person']['age'] <= 18)
            {
                $this->pdfRect(113.0, 93.7, 82.0, $pdf, 3.2);
                $pdf->Text(115.0, 96.2, 'X');
            }
        }

        // Empfangsbestätigung
        if($i==0) {
            $this->pdfRect(21.5, 93.7, 82.0, $pdf, 3.2);
            $pdf->Text(21.5, 96.2, 'X');
        }

        // Reset
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
            // Makler
            $pdf->Text(148.7, 106.5, 'X');

        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
        // Vmtl-Nr
        $pdf->Text(21.7, 114.0, '226 / 1500');


    if($complete==2) { $pdf->AddPage('P'); }