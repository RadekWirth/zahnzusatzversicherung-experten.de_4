<?php

$deckblatt = true;
#if (empty($data['benefits']) && $data['idt'] != 31 && $data['sourcePage'] != 'zzv-neu') {
#	$deckblatt = false;
#
#	emailHelper::sendMail(project::getProjectEmail(), 'radek.wirth@gmail.com', 'Achtung - Antrag ohne Deckblatt : IDT '.$data['idt'], print_r($data['benefits'], true));
#	
#	/*
#      * print_r('Kein Deckblatt vorhanden. Bzw. keine Tarifleistungen vorhanden, die angezeigt werden k�nnen.'); die();
#	*/
#}



if ($deckblatt)
{
emailHelper::sendMail(project::getProjectEmail(), 'radek.wirth@gmail.com', 'Achtung - Antrag mit Deckblatt : IDT '.$data['idt'], print_r($data['benefits'], true));



if($data['sourcePage'] == 'zzv-neu' || $data['sourcePage'] == 'zzv' || $data['sourcePage'] == 'zzv-experten.de')    
{

    if(file_exists($path.'crm/files/deckblatt/local.pdf'))
    {
	    emailHelper::sendMail(project::getProjectEmail(), 'radek.wirth@gmail.com', 'Achtung - Deckblatt konnte gefunden werden - '.$path.'crm/files/deckblatt/local.pdf');
	    $pagecount = $pdf->setSourceFile($path.'crm/files/deckblatt/local.pdf');
	    $tplidx = $pdf->importPage(1, '/MediaBox');

	    $pdf->addPage('P');
	    $pdf->useTemplate($tplidx);
     } else {
	emailHelper::sendMail(project::getProjectEmail(), 'radek.wirth@gmail.com', 'Achtung - Deckblatt konnte nicht gefunden werden - '.$path.'crm/files/deckblatt/local.pdf');
     }

    /**
     * Page 2
     */
    if ($data['sourcePage'] == 'zzv-neu') 
    {
        $pagecount = $pdf->setSourceFile($path.'files/deckblatt-rueckseite-zzvcom.pdf');
    }
    else
    {
        $pagecount = $pdf->setSourceFile($path.'files/deckblatt-rueckseite-zzvde.pdf');
    }
    $tplidx = $pdf->importPage(1, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx);

}
else 
{
    /**
      *  ACHTUNG PROVISORIUM 
      *  Der Tarif ERGO ZEZ wird hier z.B direkt behandelt
      */

    $highlightsName = isset($data['highlightsName']) ? $data['highlightsName'] : array();

    $idt = $data['idt'];
    $icon = array(
        'check' => $path.'/files/icons/check_fotolia.jpg',
        'post' => $path.'/files/icons/post.png',
        'mail' => $path.'/files/icons/arroba2.png',
        'phone' => $path.'/files/icons/phone16.png',
        'die-bayerische-keine-wartezeit' => $path.'/files/graphics/keine-Wartezeit-und-keine-Summenstaffel.png'
    );
    $logo = $path.'/files/logo.png';
    $lh = 3.6; // line height
    $lhb = $lh*2;

    $toothScale = 0;

    $data['series'] = isset($data['series']) ? $data['series'] : 0;

    if ($data['series'] == 1) {
        $toothScale = 1;
    } elseif ($data['series'] == 2) {
        $toothScale = 2;
    }

    // Add Fonts
    $pdf->AddFont('PTSans-Regular', '','PTSans-Regular.php');
    $pdf->AddFont('PTSans-Bold', '','PTSans-Bold.php');

    $pdf->SetMargins(12, 0); 

    /**
     * Page 1
     */
    $pagecount = $pdf->setSourceFile($path.'/files/deckblatt.pdf');
    $tplidx = $pdf->importPage(1, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx);

    // Head
    $pdf->SetDrawColor(200);
    $pdf->SetFillColor(255, 255, 255); 
    $pdf->RoundedRect(14, 5, 90, 86, 4, '1234', 'FD');

    // Kopf Links
    $pdf->Image($this->getCompanyLogoFile($idt, 100), 18, 8, 0, 22);
    $pdf->Image($logo, 140, 5, 60);

    $pdf->SetTextColor(58, 105, 135);
    $this->setPTSansFont(15, 'B', $pdf);

    // Bei CSS ideal soll Tarifname rechts neben Logo
    if ($data['idt'] == 38) {
        $pdf->Text(44, 24.6, $highlightsName);
    } else {
        $pdf->SetY(30);
        $pdf->SetX(18);
        $pdf->MultiCell(80, 6, $highlightsName, 0, 'L', 0); 
    }

    $this->setPTSansFont(10, 'R', $pdf);
    $pdf->SetY(44);

    if(is_array($data['highlights'])) {
    	foreach($data['highlights'] as $highlight) {
       	$y = $pdf->getY();
    		$pdf->Image($icon['check'], 18, $y-2, 8);
       	$pdf->SetX(26);
       	$pdf->MultiCell(77, 5, $highlight, 0, 'L', 0); 
       	$pdf->Ln(4);
    	}
    }

#print_r($data);
    // Kopf Rechts
    $x = 120;
    $y = 30;
    $this->setPTSansFont(14, 'B', $pdf);
    $pdf->Text($x, $y, 'Pers�nliches Angebot f�r');
    $this->setPTSansFont(12, 'R', $pdf);
    $pdf->SetTextColor(40);  
    $pdf->Text($x, $y+8, $data['name2']);
    $pdf->Text($x, $y+14, 'geb. '. $data['birthdate2']);
#    $pdf->Text($x, $y+20, 'Versicherungsbeginn: '. stringHelper::makeDateFromSignUp($data['wishedBegin']));

    $this->setPTSansFont(14, 'R', $pdf);
    $pdf->Text($x, $y+34, 'Monatlicher Beitrag:');
    $this->setPTSansFont(14, 'B', $pdf);
    $pdf->Text($x+44.8, $y+34, $data['price'] .' �');
    $this->setPTSansFont(14, 'R', $pdf);
    $pdf->Image($path.'/files/icons/coins24.png', $x+64, $y+27.5, 8);

    #if ($data['financetestLogoFilename']) {
    #    if (file_exists($path.'/files/Finanztest-Logos/'. $data['financetestLogoFilename'] .'_wide.png')) {
    #        $pdf->Image($path.'/files/Finanztest-Logos/'. $data['financetestLogoFilename'] .'_wide.png', $x, $y+40, 45);  
    #    } elseif (file_exists($path.'/files/Finanztest-Logos/'. $data['financetestLogoFilename'] .'_small.png')) {
    #        $pdf->Image($path.'/files/Finanztest-Logos/'. $data['financetestLogoFilename'] .'_small.png', $x, $y+40, 35);  
    #    }
    #}

    // F�r die ERGO ZEZ gilt ein Ausnahmefall!
    if ($data['idt'] != 31) {

        // Linke Spalte
        $x = 14;
        $benefitsX = 72;
        $y = 100; 
        $pdf->SetY($y);
        $pdf->SetX($x);
        $pdf->SetTextColor(58, 105, 135);
        $this->setPTSansFont(14, 'B', $pdf);
#        $pdf->cMargin = 2;
        $pdf->SetFillColor(58, 105, 135); 
        $pdf->RoundedRect($x, $pdf->getY()-1, 90, 6, 2, '1234', 'FD');
        $pdf->SetTextColor(255, 255, 255);
        $this->setPTSansFont(12, 'B', $pdf);
        $pdf->MultiCell(90, 4, 'Zahnersatz', 0, 'L', 0); 
        $this->setPTSansFont(10, 'R', $pdf);
        $pdf->SetTextColor(40); 
        $pdf->Ln(4); $pdf->SetX($x);
        // MV (verdopplung Festzuschuss)
        if ($data['idt'] == 78) {
            $pdf->Cell(50, 4, 'Verdopplung GKV-Festzuschuss', 0, 0, 'L');
        }
        else 
        {

	$data['benefits']['dentures'] = isset($data['benefits']['dentures']) ? $data['benefits']['dentures'] : '';
	$data['benefits']['dentures']['implants'] = isset($data['benefits']['dentures']['implants']) ? $data['benefits']['dentures']['implants'] : '';
	$data['benefits']['dentures']['inlays'] = isset($data['benefits']['dentures']['inlays']) ? $data['benefits']['dentures']['inlays'] : '';
	$data['benefits']['dentures']['dentures'] = isset($data['benefits']['dentures']['dentures']) ? $data['benefits']['dentures']['dentures'] : '';

	$data['benefits']['prophylaxis'] = isset($data['benefits']['prophylaxis']) ? $data['benefits']['prophylaxis'] : '';



            $pdf->Cell(50, 4, 'Implantate', 0, 0, 'L');
            $this->benefitsScale($data['benefits']['dentures']['implants']['value'], $benefitsX, $pdf->getY(), $pdf, $data['idt']);
            $pdf->Ln(6); $pdf->SetX($x);
            $pdf->Cell(50, 4, 'Inlays', 0, 0, 'L');
            $this->benefitsScale($data['benefits']['dentures']['inlays']['value'], $benefitsX, $pdf->getY(), $pdf, $data['idt']);
            $pdf->Ln(6); $pdf->SetX($x);
            $this->setPTSansFont(10, 'R', $pdf);
            $pdf->Cell(50, 4, 'Kronen, Br�cken & Prothesen', 0, 0, 'L');
            $this->benefitsScale($data['benefits']['dentures']['dentures']['value'], $benefitsX, $pdf->getY(), $pdf, $data['idt']);
            $pdf->Ln(6); $pdf->SetX($x);
            $this->setPTSansFont(10, 'R', $pdf);
            $pdf->Cell(50, 4, 'Keramikverblendungen', 0, 0, 'L');
            $this->benefitsScale($data['benefits']['dentures']['facings']['value'], $benefitsX, $pdf->getY(), $pdf, $data['idt']);
            if ($data['benefits']['dentures']['facings']['text']) {
                $pdf->Ln(6); $pdf->SetX($benefitsX);
                #$pdf->cMargin = 0;
                $this->setPTSansFont(9, 'R', $pdf); 
                $pdf->Cell(30, 4, $data['benefits']['dentures']['facings']['text'], 0, 0, 'L');
                #$pdf->cMargin = 2;
            }
            $pdf->Ln(6); $pdf->SetX($x);
            $this->setPTSansFont(10, 'R', $pdf);
            $pdf->Cell(50, 4, 'Knochenaufbau', 0, 0, 'L');
            $this->benefitsYesNo($data['benefits']['dentures']['boneBuilding']['value'], $benefitsX, $pdf->getY(), $pdf);
            $pdf->Ln(6); $pdf->SetX($x);
            $this->setPTSansFont(10, 'R', $pdf);
            $pdf->Cell(50, 4, 'Funktionsanalyse/Funktionstherapie', 0, 0, 'L');
            $this->benefitsYesNo($data['benefits']['dentures']['functionalTheraphy']['value'], $benefitsX, $pdf->getY(), $pdf);
            $pdf->Ln(8); $pdf->SetX($x);
            // Signal Iduna
            if ($data['idt'] == 33 || $data['idt'] == 32) {
                $pdf->Cell(30, 4, 'realistische Sch�tzwerte inkl. Vorleistung GKV', 0, 0, 'L');
                $pdf->Ln(6); $pdf->SetX($x);
            }
            if ($data['benefits']['gkvInc'] === 'after') {
                 $pdf->Cell(50, 4, '* Alle Leistungen vom erstattungsf�higen Restbetrag ', 0, 0, 'L');
                  $pdf->Ln(4); $pdf->SetX($x);
                 $pdf->Cell(50, 4, 'nach Vorleistung der GKV', 0, 0, 'L');
            } else {
                if ($data['benefits']['gkvInc'] == true) {
                    $pdf->Cell(50, 4, '* Alle Leistungen sind inkl. Vorleistung der GKV', 0, 0, 'L');
                } else {
                    $pdf->Cell(50, 4, '* Alle Leistungen + Vorleistung GKV (max. 100%)', 0, 0, 'L');
                }
            }

            // If type equals kids show orthodontic services
            if ($data['contractComplete']['contractType'] === 'kids') {
                $pdf->Ln(12); $pdf->SetX($x);
                #$pdf->cMargin = 2;
                $pdf->SetFillColor(58, 105, 135); 
                $pdf->RoundedRect($x, $pdf->getY()-1, 90, 6, 2, '1234', 'FD');
                $pdf->SetTextColor(255, 255, 255);
                $this->setPTSansFont(12, 'B', $pdf);
                $pdf->MultiCell(90, 4, 'Kieferorthop�die (Zahnspangen)', 0, 'L', 0); 
                $this->setPTSansFont(10, 'R', $pdf);
                $pdf->SetTextColor(40); 
                $pdf->Ln(4); $pdf->SetX($x);
                $pdf->Cell(50, 4, 'KIG 2 (Zahnspangen)', 0, 0, 'L');
                $this->benefitsScale($data['benefits']['orthodontics']['orthodontics_kig2']['value'], $benefitsX, $pdf->getY(), $pdf);
                if ($data['benefits']['orthodontics']['orthodontics_kig2']['text']) {
                    $pdf->Ln(6); $pdf->SetX($benefitsX);
                    #$pdf->cMargin = 0;
                    $this->setPTSansFont(9, 'R', $pdf); 
                    $pdf->Cell(30, 4, $data['benefits']['orthodontics']['orthodontics_kig2']['text'], 0, 0, 'L');
                    #$pdf->cMargin = 2;
                }
                $pdf->Ln(6); $pdf->SetX($x);
                $pdf->Cell(50, 4, 'KIG 3-5 (Zahnspangen)', 0, 0, 'L');
                $this->benefitsScale($data['benefits']['orthodontics']['orthodontics_kig35']['value'], $benefitsX, $pdf->getY(), $pdf);
                if ($data['benefits']['orthodontics']['orthodontics_kig35']['text']) {
                    $pdf->Ln(6); $pdf->SetX($benefitsX);
                    #$pdf->cMargin = 0;
                    $this->setPTSansFont(9, 'R', $pdf); 
                    $pdf->Cell(30, 4, $data['benefits']['orthodontics']['orthodontics_kig35']['text'], 0, 0, 'L');
                   # $pdf->cMargin = 2;
                }
            }
        }

        $pdf->Ln(12); $pdf->SetX($x);
        $pdf->SetFillColor(58, 105, 135); 
        $pdf->RoundedRect($x, $pdf->getY()-1, 90, 6, 2, '1234', 'FD');
        $pdf->SetTextColor(255, 255, 255);
        $this->setPTSansFont(12, 'B', $pdf);
        $pdf->MultiCell(90, 4, 'Zahnbehandlung', 0, 'L', 0); 
        $this->setPTSansFont(10, 'R', $pdf);
        $pdf->SetTextColor(40); 
        $pdf->Ln(4); $pdf->SetX($x);
        $pdf->Cell(50, 4, 'Professionelle Zahnreinigung', 0, 0, 'L');
        $this->benefitsScale($data['benefits']['prophylaxis']['dentalClean']['value'], $benefitsX, $pdf->getY(), $pdf);
        if ($data['benefits']['prophylaxis']['dentalClean']['text']) {
            #$pdf->cMargin = 0;
            $pdf->Ln(6); $pdf->SetX($benefitsX);
            $this->setPTSansFont(9, 'R', $pdf);
            $pdf->Cell(30, 4, $data['benefits']['prophylaxis']['dentalClean']['text'], 0, 0, 'L');
            $this->setPTSansFont(10, 'R', $pdf);
            #$pdf->cMargin = 2;
        }
        $pdf->Ln(6); $pdf->SetX($x);
        $pdf->Cell(50, 4, 'Hochwertige Kunststofff�llungen', 0, 0, 'L');
        $this->benefitsScale($data['benefits']['prophylaxis']['plasticFillings']['value'], $benefitsX, $pdf->getY(), $pdf, $data['idt']);
        // If type equals kids we need more space
        if ($data['contractComplete']['contractType'] !== 'kids') {
            $pdf->Ln(6); $pdf->SetX($x);
            $pdf->MultiCell(50, 4, 'Wurzelbehandlung ohne Kassenvorleistung', 0, 'L', 0);
            $this->benefitsScale($data['benefits']['prophylaxis']['rootWithoutAdvancePayment']['value'], $benefitsX, $pdf->getY()-4, $pdf);
            if ($data['benefits']['prophylaxis']['rootWithoutAdvancePayment']['text']) {
                #$pdf->cMargin = 0;
                $pdf->Ln(2);$pdf->SetX($benefitsX-10);
                $this->setPTSansFont(9, 'R', $pdf);
                $pdf->Cell(30, 4, $data['benefits']['prophylaxis']['rootWithoutAdvancePayment']['text'], 0, 0, 'L');
                $pdf->Ln(4); 
                $this->setPTSansFont(10, 'R', $pdf);
                #$pdf->cMargin = 2;
            }
            $pdf->Ln(2); $pdf->SetX($x);
            $pdf->MultiCell(50, 4, 'Wurzelbehandlung mit Kassenvorleistung', 0, 'L', 0);
            $this->benefitsScale($data['benefits']['prophylaxis']['rootAdvancePayment']['value'], $benefitsX, $pdf->getY()-4, $pdf);
            $pdf->Ln(2); $pdf->SetX($x);
            $pdf->MultiCell(50, 4, 'Paradontosebehandlung ohne Kassenvorleistung', 0, 'L', 0);
            $this->benefitsScale($data['benefits']['prophylaxis']['peridontitisWithoutAdvancePayment']['value'], $benefitsX, $pdf->getY()-4, $pdf);
            if ($data['benefits']['prophylaxis']['peridontitisWithoutAdvancePayment']['text']) {
                #$pdf->cMargin = 0;
                $pdf->Ln(2);$pdf->SetX($benefitsX-10);
                $this->setPTSansFont(9, 'R', $pdf);
                $pdf->Cell(30, 4, $data['benefits']['prophylaxis']['peridontitisWithoutAdvancePayment']['text'], 0, 0, 'L');
                $pdf->Ln(4); 
                $this->setPTSansFont(10, 'R', $pdf);
                #$pdf->cMargin = 2;
            }
            $pdf->Ln(2); $pdf->SetX($x);
            $pdf->MultiCell(50, 4, 'Paradontosebehandlung mit Kassenvorleistung', 0, 'L', 0);
            $this->benefitsScale($data['benefits']['prophylaxis']['peridontitisAdvancePayment']['value'], $benefitsX, $pdf->getY()-4, $pdf);
        }


        $pdf->Ln(8); $pdf->SetX($x);
        $pdf->SetFillColor(58, 105, 135); 
        $pdf->RoundedRect($x, $pdf->getY()-1, 90, 6, 2, '1234', 'FD');
        $pdf->SetTextColor(255, 255, 255);
        $this->setPTSansFont(12, 'B', $pdf);
        $pdf->MultiCell(90, 4, 'Allgemeine Infos', 0, 'L', 0); 
        $pdf->Ln(4); 

        $waitingTime = html_entity_decode($data['benefits']['periodsAndLimits']['waitingTime']['value'], ENT_QUOTES | ENT_XML1, 'UTF-8');

        if ($data['benefits']['periodsAndLimits']['waitingTime']['value'] == false || $waitingTime == '') {
            $waitingTime = 'keine Wartezeiten!';
        } 

        $pdf->SetX($x);
        $this->setPTSansFont(10, 'R', $pdf);
        $pdf->SetTextColor(40); 
        $pdf->MultiCell(80, 4, 'Wartezeit: '. $waitingTime, 0, 'L', 0);


        // Rechte Spalte
        $x = 110;
        $y = 100; 

        $pdf->SetY($y);
        $pdf->SetX($x);
        $this->setPTSansFont(12, 'B', $pdf); 

        if ($data['benefits']['periodsAndLimits'] != '') {
            // Leistungsbegrenzungen  
            $pdf->SetFont('PTSans-Bold', '', 12);
	     #$pdf->cMargin = 2;
            $pdf->SetFillColor(58, 105, 135); 
            $pdf->RoundedRect($x, $pdf->getY()-1, 90, 6, 2, '1234', 'FD');
            $pdf->SetTextColor(255, 255, 255);
            $pdf->MultiCell(90, 4, 'Leistungsbegrenzungen', 0, 'L', 0);
            if ($data['benefits']['periodsAndLimits']['maxBenefitsIntro']) {
                $pdf->SetFont('PTSans-Regular', '', 10);
                $pdf->Ln(4); 
                $pdf->SetTextColor(40);$pdf->SetX($x);
                $pdf->MultiCell(90, 4, $data['benefits']['periodsAndLimits']['maxBenefitsIntro'], 0, 'L', 0);
            }
            if ($data['benefits']['periodsAndLimits']['maxBenefits']) {
                if ($toothScale === 0) {
                    foreach ($data['benefits']['periodsAndLimits']['maxBenefits'] as $maxBenefit) {
                        if ($maxBenefit['showOnlyChild'] !== true || $maxBenefit['showOnlyChild'] === true && $data['contractComplete']['contractType'] === 'kids') {
                            $this->maxBenefits($maxBenefit['maxBenefits'], $x, $pdf->GetY(), $pdf, array(
                                'head' => $maxBenefit['head'],
                                'introduction' => $maxBenefit['introduction'], 
                                'timeIntervals' => $maxBenefit['timeIntervals'],
                                'suffix' => $maxBenefit['suffix'],
                                'outro' => $maxBenefit['outro']));
                        }
                    }
                    $pdf->Ln(3); $pdf->SetX($x);
                    $pdf->MultiCell(90, 4,$data['benefits']['periodsAndLimits']['maxBenefitsOutro'], 0, 'L', 0);
                    $pdf->Ln(3); 

                    if ($data['benefits']['periodsAndLimits']['maxBenefitsExtraText']) {
                        foreach ($data['benefits']['periodsAndLimits']['maxBenefitsExtraText'] as $extra) {
                            $pdf->SetTextColor(58, 105, 135);
                            $pdf->SetFont('PTSans-Bold', '', 10);
                            $pdf->SetX($x);
                            $pdf->MultiCell(90, 4, html_entity_decode($extra['head'], ENT_QUOTES | ENT_XML1, 'UTF-8'), 0, 'L', 0); 
                            $pdf->Ln(3); 
                            $pdf->SetX($x);
                            $pdf->SetTextColor(40);  
                            $pdf->SetFont('PTSans-Regular', '', 10);
                            $pdf->MultiCell(90, 4, html_entity_decode($extra['text'], ENT_QUOTES | ENT_XML1, 'UTF-8'), 0, 'L', 0); 
                            $pdf->Ln(4); 
                        }
                    }
                }

                if ($toothScale === 1) {
                    foreach ($data['benefits']['periodsAndLimits']['maxBenefits'] as $maxBenefit) {
                        // Janitos 
                        if ($data['idt'] == 15) {
                            unset($maxBenefit['outro']);
                        }
                        if ($maxBenefit['showOnlyChild'] !== true || $maxBenefit['showOnlyChild'] === true && $data['contractComplete']['contractType'] === 'kids') {
                            $this->maxBenefits($maxBenefit['maxBenefits1'], $x, $pdf->GetY(), $pdf, array(
                                'head' => html_entity_decode($maxBenefit['head']),
                                'introduction' => html_entity_decode($maxBenefit['introduction']), 
                                'timeIntervals' => $maxBenefit['timeIntervals1'],
                                'outro' => html_entity_decode($maxBenefit['outro1']),
                                'suffix' => $maxBenefit['suffix']));
                        }
                    }
                    $pdf->Ln(3); $pdf->SetX($x);
                    $pdf->MultiCell(90, 4, html_entity_decode($data['benefits']['periodsAndLimits']['maxBenefitsOutro']), 0, 'L', 0);
                    $pdf->Ln(3); 

                    if ($data['benefits']['periodsAndLimits']['maxBenefitsExtraText1']) {
                        foreach ($data['benefits']['periodsAndLimits']['maxBenefitsExtraText1'] as $extra) {
                            $pdf->SetTextColor(58, 105, 135);
                            $pdf->SetFont('PTSans-Bold', '', 10);
                            $pdf->SetX($x);
                            $pdf->MultiCell(90, 4, html_entity_decode($extra['head']), 0, 'L', 0); 
                            $pdf->Ln(3); 
                            $pdf->SetX($x);
                            $pdf->SetTextColor(40);  
                            $pdf->SetFont('PTSans-Regular', '', 10);
                            $pdf->MultiCell(90, 4, html_entity_decode($extra['text']), 0, 'L', 0); 
                            $pdf->Ln(4); 
                        }
                    }
                }

                if ($toothScale === 2) {
                    foreach ($data['benefits']['periodsAndLimits']['maxBenefits'] as $maxBenefit) {
                        // Janitos 
                        if ($data['idt'] == 15) {
                            unset($maxBenefit['outro']);
                        }
                        $outro = $maxBenefit['outro1'];
                        if ($maxBenefit['outro2']) {
                            $outro = $maxBenefit['outro2'];
                        }
                        if ($maxBenefit['showOnlyChild'] !== true || $maxBenefit['showOnlyChild'] === true && $data['contractComplete']['contractType'] === 'kids') {
                            $this->maxBenefits($maxBenefit['maxBenefits1'], $x, $pdf->GetY(), $pdf, array(
                                'head' => $maxBenefit['head'],
                                'introduction' => $maxBenefit['introduction'], 
                                'timeIntervals' => $maxBenefit['timeIntervals2'],
                                'outro' => $outro,
                                'suffix' => $maxBenefit['suffix']));
                        } 
                    }
                    $pdf->Ln(3); $pdf->SetX($x);
                    $pdf->MultiCell(90, 4, $data['benefits']['periodsAndLimits']['maxBenefitsOutro'], 0, 'L', 0);
                    $pdf->Ln(3); 

                    if ($data['benefits']['periodsAndLimits']['maxBenefitsExtraText1']) {
                        foreach ($data['benefits']['periodsAndLimits']['maxBenefitsExtraText1'] as $extra) {
                            $pdf->SetTextColor(58, 105, 135);
                            $pdf->SetFont('PTSans-Bold', '', 10);
                            $pdf->SetX($x);
                            $pdf->MultiCell(90, 4, html_entity_decode($extra['head']), 0, 'L', 0); 
                            $pdf->Ln(3); 
                            $pdf->SetX($x);
                            $pdf->SetTextColor(40);  
                            $pdf->SetFont('PTSans-Regular', '', 10);
                            $pdf->MultiCell(90, 4, $extra['text'], 0, 'L', 0); 
                            $pdf->Ln(4); 
                        }
                    }
                }
            }
        } else {
            $pdf->SetFont('PTSans-Regular', '', 10);
            $pdf->Ln(4); 
            $pdf->SetTextColor(40);$pdf->SetX($x);
            $pdf->MultiCell(90, 4, '', 0, 'L', 0);
            $pdf->Image($icon['die-bayerische-keine-wartezeit'], 115, $y-1, 80);
        }


    } else {
        // ERGO ZEZ
        $x = 14;
        $benefitsX = 72;
        $y = 100; 
        $pdf->SetY($y);
        $pdf->SetX($x);
        $pdf->SetTextColor(58, 105, 135);
        $this->setPTSansFont(14, 'B', $pdf);
#            $pdf->cMargin = 4;
        $pdf->SetFillColor(58, 105, 135); 
        $pdf->RoundedRect($x, $pdf->getY()-1, 180, 6, 2, '1234', 'FD');
        $pdf->SetTextColor(255, 255, 255);
        $this->setPTSansFont(12, 'B', $pdf);
        $pdf->MultiCell(180, 4, 'Zahnersatz', 0, 'L', 0); 
        $this->setPTSansFont(11, 'R', $pdf);
        $pdf->SetTextColor(40); 
        $pdf->Ln(4); $pdf->SetX($x);
        $pdf->MultiCell(0, 5, 'Hier handelt es sich um den einzigen Tarif am deutschen Markt f�r private Zahnzusatzversicherungen, wo auch schon Sch�den mit versichert werden k�nnen, die bereits vor Abschluss der Versicherung eingetreten und vom Zahnarzt diagnostiziert worden sind.

    Alle anderen Zahntarife, die Sie am deutschen Markt finden (die auch wir teilweise in unserem Online-Vergleichsportal anbieten) leisten nicht f�r Behandlungen, die bereits vor Abschluss der Versicherung vom Zahnarzt angeraten oder geplant worden sind, bzw. die bereits vor Abschluss der Versicherung begonnen worden sind.

    Dieser sogenannte �erweiterte Versicherungsschutz� im Tarif ERGO-Direkt Zahnersatz Sofort besteht f�r Zahnersatz-Ma�nahmen, die innerhalb von sechs Monaten vor dem vereinbarten Versicherungsbeginn begonnen wurden und bei Versicherungsbeginn noch andauerten bzw. noch nicht durchgef�hrt worden sind.', 0, 'L', 0); 
        $pdf->SetTextColor(58, 105, 135);
        $pdf->SetFont('PTSans-Bold', '', 12);
        $pdf->Ln(6); $pdf->SetX($x);
#	    $pdf->cMargin = 4;
        $pdf->MultiCell(90, 4, 'H�he der vereinbarten tariflichen Leistungen', 0, 'L', 0); 
        $pdf->Ln(3); 
        $pdf->SetX($x);
        $pdf->SetTextColor(40);  $this->setPTSansFont(11, 'R', $pdf);
        $pdf->MultiCell(180, 5, 'Die ERGO-Direkt Versicherung erstattet eine Leistung in gleicher H�he wie den von der GKV erstatteten befundbezogenen Festzuschuss d.h. der Zuschuss der gesetzlichen Krankenkasse f�r eine Zahnersatzma�nahme wird von der ERGO-Direkt verdoppelt.', 0, 'L', 0); 
        $pdf->Ln(10); 
        $this->setPTSansFont(14, 'B', $pdf);
#            $pdf->cMargin = 6;
        $pdf->SetFillColor(58, 105, 135); 
        $pdf->RoundedRect($x, $pdf->getY()-1, 180, 6, 2, '1234', 'FD');
        $pdf->SetTextColor(255, 255, 255);
        $this->setPTSansFont(12, 'B', $pdf);
        $pdf->MultiCell(90, 4, 'Wartezeiten & Begrenzungen', 0, 'L', 0); 
        $this->setPTSansFont(11, 'R', $pdf);
        $pdf->SetTextColor(40);     
        $pdf->Ln(4); $pdf->SetX($x);
#           $pdf->cMargin = 4;
        $pdf->MultiCell(0, 5, 'Keine anf�ngliche Wartezeit!

    Keine anf�nglichen (oder dauerhaften) Summenbegrenzungen!', 0, 'L', 0); 
    }



    // Text Footer
    $y = 268;
    $x = 14;
    $this->pdfClean(0, $y-5, 210, $pdf, 236, 236, 236, 37, 'F');
    $pdf->SetX($x);
    $this->setPTSansFont(10, 'B', $pdf);
    $pdf->Text(16, $y+1, 'Sie haben Fragen?');
    $this->setPTSansFont(10, 'R', $pdf);
    $pdf->Image($icon['phone'], 16, $y+5, 5);
    $pdf->Text(24, $y+10, '08142 - 651 39 28');
    $pdf->Image($icon['mail'], 16, $y+12, 5);
    $pdf->Text(24, $y+17, 'info@zahnzusatzversicherung-experten.de');
    //$this->setPTSansFont(10, 'B', $pdf);
    $this->setPTSansFont(10, 'B', $pdf);
    $pdf->Text(16, 293, 'Auf der R�ckseite finden Sie eine Checkliste mit wertvollen Tipps zum Ausf�llen des Antragsformulares!');
    //$this->setPTSansFont(10, 'R', $pdf);




    /**
     * Page 2
     */
        $pagecount = $pdf->setSourceFile($path.'/files/deckblatt-rueckseite-zzvde.pdf');
        $tplidx = $pdf->importPage(1, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx);
}

}
?>
