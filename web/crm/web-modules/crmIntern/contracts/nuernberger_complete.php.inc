<?php

if(!$complete && file_exists($path.'/web-modules/crmIntern/contracts/nuernberger.php.inc'))
{
	include('nuernberger.php.inc');
} else {


$pagecount = $pdf->setSourceFile($path.'files/nuernberger/Antrag/2024/KV049_001_202306_pf.pdf');


$tplidx = $pdf->importPage(1, '/MediaBox');

$pdf->addPage('P');
$pdf->useTemplate($tplidx, 5, 17, 190);

// Title
if($i==0) $this->pdfRect(74, 2, 70, $pdf);
$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);
$pdf->Text(74.0, 6.0, $titles[$i]);

$this->getCompanyStamp(72.0, 11, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']-2, 'horizontal', 'wide');

$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);


$pdf->Text(23.5, 70.0, '115/38/3392');

// Neuantrag
$pdf->Text(23.5, 76.9, 'X');



$pdf->Text(70, 138.5, $data['birthdate']);

$pdf->Text(24, 150.3, $data['person']['forename']);
$pdf->Text(106, 150.3, $data['person']['surname']);

$pdf->Text(24, 172.5, $data['street']);

$pdf->Text(106, 172.5, $data['postcode']. ' ' .$data['city']);

if(empty($data['phone'])) {
    if($i==0) $this->pdfRect(22.5, 182.0, 36, $pdf, 3);
} else {
    $pdf->Text(24, 184.5, $data['phone']);
}

if(empty($data['email'])) {
    if($i==0) $this->pdfRect(104.5, 182.5, 36, $pdf, 3);
} else {
    $pdf->Text(106.5, 184.5, $data['email']);
}


if($data['gender'] == 'male') {
    $pdf->Text(24.2, 195.7, 'X');
} else {
    $pdf->Text(63.6, 195.7, 'X');
}

if(empty($data['nation'])) {
    if($i==0) $this->pdfRect(23, 134.4, 36, $pdf, 3);
} elseif($data['nation']=='DEU') {
    $pdf->Text(102.7, 195.7, 'X');
} else {
    $pdf->Text(142.1, 195.7, 'X');
}

$pdf->Text(24.1, 206.7, $data['job']);


$this->pdfClean(160, 270.0, 20, $pdf,  255,255,255,3.6);



$tplidx = $pdf->importPage(2, '/MediaBox');

$pdf->addPage('P');
$pdf->useTemplate($tplidx, 5, 7.4, 197.6);


/* Person Insured */
$pdf->Text(34, 67.6, '1');

$pdf->Text(27, 77.6, $data['personInsured']['forename']);

if($data['personInsured']['surname'] !== $data['person']['surname'])
	$pdf->Text(107, 77.6, $data['personInsured']['surname']);


$pdf->Text(107, 88.6, $data['birthdate2']);

if ($data['personInsured']['salutationLid'] == 9) {
    $pdf->Text(25.0, 89.0, 'x');
} else {
    $pdf->Text(65.7, 89.0, 'x');
}


if ($data['personInsured']['job']) {
    $pdf->Text(25, 101.0, $data['personInsured']['job']);
} else {
    if($i==0) $this->pdfRect(23, 99.0, 50, $pdf, 3);
}

if ($i == 0) 
{
	$this->pdfRect(24.2, 110.3, 2, $pdf, 2);
	$this->pdfRect(65.2, 110.3, 2, $pdf, 2);
	$this->pdfRect(106.0, 110.3, 2, $pdf, 2);
	$this->pdfRect(147.0, 110.3, 2, $pdf, 2);
}


$this->pdfClean(162, 269.8, 22, $pdf,  255,255,255,3.3);



$tplidx = $pdf->importPage(3, '/MediaBox');

$pdf->addPage('P');
$pdf->useTemplate($tplidx, 5, 7.4, 197.6);


$pdf->Text(33.5, 60.0, '1');

$begin = explode('.', $data['begin']);
$pdf->Text(70.5, 60.0, $begin[0]);
$pdf->Text(76.8, 60.0, $begin[1]);



// Welcher Tarif?
$pdfTemplate = isset($pdfTemplate)?$pdfTemplate:$data['idt'];
switch($pdfTemplate) {
    case project::gti('nuern-z80'):
        $pdf->Text(110.0, 60.0, 'Z80');
        break;
    case project::gti('nuern-z90'):
        $pdf->Text(110.0, 60.0, 'Z90');
        break;
    case project::gti('nuern-z100'):
        $pdf->Text(110.0, 60.0, 'Z100');
        break;
}

$pdf->Text(170, 60, $data['price']. ' €');

$this->pdfClean(147, 75.5, 36.0, $pdf,  255,255,255, 5.8);
$pdf->Text(170, 80, $data['price']. ' €');


// sonstige KV
$pdf->Text(35.5, 197.6, '1');

if (empty($data['personInsured']['insurance'])) 
{
	if($i == 0) $this->pdfRect(32.2, 203.8, 80, $pdf, 3);
} else { 
	$pdf->Text(33.5, 206.0, $data['personInsured']['insurance']);
}

$this->pdfClean(162, 269.8, 22, $pdf,  255,255,255,3.3);

for($p=4;$p<=9;$p++)
{
    $tplidx = $pdf->importPage($p, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 4.7, 6.4, 199);

    $this->pdfClean(161, 270.9, 24, $pdf,  255,255,255,3.3);


    if($p == 4)
    {
	if ($i == 0) $this->pdfRect(24.2, 60.9, 2, $pdf, 2);
	if ($i == 0) $this->pdfRect(52.0, 60.9, 2, $pdf, 2);
	if ($i == 0) $this->pdfRect(79.2, 60.9, 2, $pdf, 2);
	if ($i == 0) $this->pdfRect(106.4, 60.9, 2, $pdf, 2);
	if ($i == 0) $this->pdfRect(134.0, 60.9, 2, $pdf, 2);

       if ($i == 0) $this->pdfRect(27.5, 146.0, 40, $pdf, 3);
       if ($i == 0) $this->pdfRect(106.9, 146.0, 40, $pdf, 3);
       if ($i == 0) $this->pdfRect(27.5, 157.1, 80, $pdf, 3);


       if ($i == 0) $this->pdfRect(27.5, 169.1, 40, $pdf, 3);
       if ($i == 0) $this->pdfRect(106.9, 169.1, 14, $pdf, 3);
       if ($i == 0) $this->pdfRect(130.9, 169.1, 40, $pdf, 3);

       if ($i == 0) $this->pdfRect(27.9, 223.1, 100, $pdf, 3);
       if ($i == 0) $this->pdfRect(27.9, 233.1, 100, $pdf, 3);
       if ($i == 0) $this->pdfRect(27.9, 255.1, 60, $pdf, 3);
       if ($i == 0) $this->pdfRect(106.9, 255.1, 60, $pdf, 3);
    }

    if($p == 9)
    {
	$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+3, 0, 2);

	 $pdf->Text(24.5, 59.2, 'Versicherungsmakler Experten GmbH');
        $pdf->Text(24.5, 68.5, '');
        $pdf->Text(24.5, 83.3, 'Feursstr. 56 / RGB');
        $pdf->Text(114.5, 83.3, '82140 Olching');
        $pdf->Text(24.5, 95.0, 'allgemein@vm-experten.de');
	 $pdf->Text(114.5, 95.0, '08142 651 39 28');

	$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);
    }

    if($p == 8)
    {
        if ($i == 0) $this->pdfRect(27.5, 114.7, 40, $pdf, 3);
        if ($i == 0) $this->pdfRect(112.9, 114.7, 33, $pdf, 3);

        if ($i == 0) $this->pdfRect(27.5, 134.4, 40, $pdf, 3);
        if ($i == 0) $this->pdfRect(112.9, 134.4, 33, $pdf, 3);

        if ($i == 0) $this->pdfRect(27.5, 177.6, 40, $pdf, 3);
        if ($i == 0) $this->pdfRect(112.9, 177.6, 33, $pdf, 3);


        $pdf->Text(24.5, 200.7, 'x');
        $pdf->Text(30.3, 210.2, 'x');
        //$pdf->Text(84.2, 141.5, 'x');


        #$pdf->Text(30, 236.1, 'Versicherungsmakler Experten GmbH, Feursstr. 56 / RGB, 82140 Olching');
        #$pdf->Text(30, 239.3, 'allgemein@vm-experten.de, 08142 651 39 28');
        
    }


}

if($complete)	
	    $pdf->addPage('P');
}
