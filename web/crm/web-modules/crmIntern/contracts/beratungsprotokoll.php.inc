<?php


if(isset($complete))
{

		$i = 0;
		/* VARS */

			$lh = 3.5;
			$hlh = $lh +2;
		/* VARS END */

		$pdf->AddPage('P');
		$pdf->SetMargins(19.0, 10.0, 19.0);
		$pdf->SetAutoPageBreak(false);
		$pdf->SetDrawColor(50.0);

	// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+5, 0, 2);
		$pdf->Text(20.0, 15.5, 'Beratungsdokumentation');


		$pdf->SetY(25.0);
	/* Angaben zum Vermittler */
	// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);
		$pdf->Write($lh, 'Angaben zum Vermittler');

		$pdf->SetY($pdf->GetY()+$hlh);
	// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);
		$pdf->Write($lh, 'Versicherungsmakler Experten GmbH'); $pdf->Ln();
		$pdf->Write($lh, 'Geschäftsführer: Maximilian Waizmann'); $pdf->Ln();
		$pdf->Write($lh, 'Feursstr. 56 / RGB'); $pdf->Ln();
		$pdf->Write($lh, '82140 Olching'); $pdf->Ln(); $pdf->Ln();
		$pdf->Write($lh, 'Tel.: 08142 - 651 39 28'); $pdf->Ln();
		$pdf->Write($lh, 'Fax.: 08142 - 651 39 29'); $pdf->Ln();
		$pdf->Write($lh, 'Mail: '.$this->getEMailBySourcePage($data['contractComplete']['sourcePage'])); $pdf->Ln();
	$pdf->Ln();


	/* Angaben zum Interessenten */
	// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);
		$pdf->Text(108.5, 27.5, 'Angaben zum Interessenten');

	// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);
		$pdf->Text(108.5, 32.0, $data['namec']);
		$pdf->Text(108.5, 35.5, $data['street']);
		$pdf->Text(108.5, 39.0, $data['postcode']." ".$data['city']);

    // zu versichernde Person
        $person = (isset($data['personInsured'])) ? $data['name2'] : $data['namec'];

        $pdf->Text(108.5, 46, 'zu versichernde Person: '. $person);


	/* Präambel */
		$pdf->SetY($pdf->GetY()+$hlh);
	// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);
		$pdf->Write($lh, 'Präambel');
		$pdf->SetY($pdf->GetY()+$hlh);
	// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);
		$pdf->Write($lh, "Die Versicherungsmakler Experten GmbH ist Versicherungsmakler gem. §93 HGB. Wir sind Spezialmakler für Zahnzusatzversicherungen. Für andere Versicherungssparten (z.B. Altersfürsorge, Berufsunfähigkeit, Sachversicherungen, etc.) können wir keine ausführliche Beratung anbieten und somit begrenzt sich auch unsere Maklerhaftung ausschließlich auf die Zahnzusatzversicherung. Die Beratung erfolgt ausschließlich über fernmediale Kommunikation, z.B. Telefon, Fax oder Internet (Webplatform und E-mail).");
	$pdf->Ln();


	/* Beratungsanlass */
		$pdf->SetY($pdf->GetY()+$hlh);
	// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);
		$pdf->Write($lh, 'Beratungsanlass');
		$pdf->SetY($pdf->GetY()+$hlh);
	// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);
		$interestedParty = '';

		if($data['person']['salutationLid']==9) {
			// man
			$interestedParty = L::_(371,null,true);
		}
		else {
			// woman
			$interestedParty = L::_(372,null,true);
		}

		$interestedTextLine = "{$interestedParty} ist auf der Suche nach einer hochwertigen Zahnzusatzversicherung und hat am {$data['signupDate']} selbständig aus einem umfassenden Marktvergleich ausgewählt.";

		$pdf->Write($lh, $interestedTextLine);
	$pdf->Ln();


	/* Bedürfnisse, Wünsche und gesundheitliche... */
		$pdf->SetY($pdf->GetY()+$hlh);
	// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);
		$pdf->Write($lh, 'Bedürfnisse, Wünsche und gesundheitliche Voraussetzungen des Interessenten');

	// reset font
		$pdf->SetY($pdf->GetY()+$hlh);
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);
		$pdf->Write($lh, "{$interestedParty} hat in einem Online-Berechnungstool Angaben zum Zahnzustand und zu gewünschten Leistungen eingegeben. Im Online-Vergleichsrechner wurden dem Interessenten auf Basis seiner Eingaben daraufhin mehrere in Frage kommende Zahnzusatzversicherungen aufgeführt und mit detaillierten Leistungsangaben gegenübergestellt. Für die Angaben zum Zahnzustand ist allein der Interessent verantwortlich.");
	$pdf->Ln();


	/* Empfehlung des Maklers ... */
		$pdf->SetY($pdf->GetY()+$hlh);
	// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);
		$pdf->Write($lh, 'Empfehlung des Maklers / Auswahl des Interessenten');

	// reset font
		$pdf->SetY($pdf->GetY()+$hlh);
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);
		$pdf->Write($lh, "Der Makler bietet auf seinem Online-Vergleichsportal verschiedene Zahnzusatzversicherungen unterschiedlicher Versicherungsgesellschaften an.");

		$pdf->Ln(); $pdf->Ln();
		$pdf->Write($lh, "{$interestedParty} hat aus dem Vergleich selbstständig das Angebot ");

	// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-1, 0, 2);
		$pdf->Write($lh, html_entity_decode($data['tariffName']));

	// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);
		$pdf->Write($lh, " ausgewählt.");
	$pdf->Ln();

#print_r($data);


if($pdfTemplate == project::gti('ergo-direkt-zez')) {

	/* Anmerkungen */
		$pdf->SetY($pdf->GetY()+$hlh);
	// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);
		$pdf->Write($lh, 'Anmerkungen');

		// set font to default!
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

		$pdf->SetXY(19.0, $pdf->GetY()+$hlh);
		$pdf->MultiCell(171.0, $lh, 'Beim Tarif ERGO Direkt Zahnersatz Sofort (ZEZ) handelt es sich um den einzigen Tarif am deutschen Markt für private Zahnzusatzversicherungen, wo auch schon Schäden mit versichert werden können, die bereits vor Abschluss der Versicherung eingetreten und vom Zahnarzt diagnostiziert worden sind.');
		$pdf->SetXY(19.0, $pdf->GetY()+$lh);
		$pdf->MultiCell(171.0, $lh, 'Dieser sogenannte „erweiterte Versicherungsschutz“ im Tarif ERGO-Direkt Zahnersatz Sofort besteht für Zahnersatz-Maßnahmen, die innerhalb von sechs Monaten vor dem vereinbarten Versicherungsbeginn begonnen wurden und bei Versicherungsbeginn noch andauerten bzw. noch nicht durchgeführt worden sind.');
		$pdf->SetXY(19.0, $pdf->GetY()+$lh);
		$pdf->MultiCell(171.0, $lh, 'Der Tarif ERGO-Direkt Zahnersatz Sofort ist nur dann empfehlenswert, wenn Behandlungen in entsprechendem Umfang angeraten / geplant sind, welche nicht mehr anderweitig versichert werden können.');
		$pdf->SetXY(19.0, $pdf->GetY()+$lh);

}
else {

	/* Anmerkungen */
		$pdf->SetY($pdf->GetY()+$hlh);
	// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);
		$pdf->Write($lh, 'Anmerkungen');

		$pdf->SetXY(98, $pdf->GetY());
		if($i==0) $this->pdfRect($pdf->GetX(), $pdf->GetY()-0.4, 6, $pdf);

		$pdf->Write($lh, '-O-');
		$pdf->SetXY(105, $pdf->GetY());
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);
		$pdf->MultiCell(82.0, $lh, 'Ja, bitte informieren Sie meinen Zahnarzt über den Leistungsumfang der Versicherung');

		// set font to default!
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

		$pdf->SetY($pdf->GetY()+$hlh*1.5);
		$pdf->SetDrawColor(50.0);
		if($i==0) $this->pdfRect(115, $pdf->GetY()-4.8, 60, $pdf);
		$pdf->Line(20.0, $pdf->GetY(), 190.0,  $pdf->GetY());  
			$pdf->Ln(); $pdf->Ln();
		if($i==0) $this->pdfRect(115, $pdf->GetY()-4.8, 60, $pdf);
		$pdf->Line(20.0, $pdf->GetY(), 190.0,  $pdf->GetY()); 
			$pdf->Ln(); $pdf->Ln();
		if($i==0) $this->pdfRect(115, $pdf->GetY()-4.8, 60, $pdf);
		$pdf->Line(20.0, $pdf->GetY(), 190.0,  $pdf->GetY()); 
			$pdf->Ln();

		$this->pdfClean(90, $pdf->GetY()-(7.1*$lh), 26.0, $pdf, 255, 255, 255, (7*$lh));	
		$pdf->Text(98.4, $pdf->GetY() - (5.1*$lh), 'Zahnarzt');
		$pdf->Text(98.4, $pdf->GetY() - (3.1*$lh), 'Straße, HNr.');
		$pdf->Text(98.4, $pdf->GetY() - (1.1*$lh), 'PLZ, Ort');
}


		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);
		$pdf->Write($lh, 'An den Versicherungsnehmer übergebene Unterlagen:');

		// set font to default!
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

		$pdf->SetXY(39.5, $pdf->GetY()+2*$lh);
		$pdf->MultiCell(151.0, $lh, "gesetzlich vorgeschriebene Verbraucherinformationen zum Tarif ".html_entity_decode($data['highlightsName']));
		$pdf->Image($path.'/files/circle.png', 37.0, $pdf->GetY() -2.6, 1.5);
			$pdf->Ln();
		$pdf->SetXY(39.5, $pdf->GetY());
		$pdf->MultiCell(151.0, $lh, "Antragsunterlagen des gewünschten Tarifes");
		$pdf->Image($path.'/files/circle.png', 37.0, $pdf->GetY() -2.6, 1.5);
			$pdf->Ln();
		$pdf->SetXY(39.5, $pdf->GetY());
		$pdf->MultiCell(151.0, $lh, "ausführliche Leistungsbeschreibung zum gewünschten Tarif");
		$pdf->Image($path.'/files/circle.png', 37.0, $pdf->GetY() -2.6, 1.5);
			$pdf->Ln();

	if($pdfTemplate == project::gti('ergo-direkt-zez')) {
		$pdf->SetY($pdf->GetY()+$lh-2.0);
	} else {
		$pdf->SetY($pdf->GetY()+$lh-1.2);
	}		
		$pdf->MultiCell(171.0, 2.9, 'it meiner Unterschrift bestätige ich die Richtigkeit der Angaben und den Erhalt der angegebenen Informationen. Ein Exemplar dieser Beratungsdokumentation habe ich erhalten. Sofern Unklarheiten bestanden, habe ich die Möglichkeit genutzt, mit dem Makler telefonisch oder per E-Mail in Kontakt zu treten und diese auszuräumen. Der Makler weist ausdrücklich darauf hin, dass die Angaben zum Gesundheitszustand im Antragsformular vollständig und wahrheitsgemäß erfolgen müssen, da ansonsten der Versicherungsschutz nachhaltig gefährdet ist. Für die Beantwortung der Gesundheitsfragen ist allein der Interessent verantwortlich. Für Versicherungsfälle, die bereits vor Vertragsabschluss eingetreten sind, besteht grundsätzlich keine Leistungspflicht des Versicherers.');

		if($i==0) $this->pdfRect(21.9, $pdf->GetY()+$lh, 41, $pdf);
		if($i==0) $this->pdfRect(66.0, $pdf->GetY()+$lh, 72, $pdf);

		$pdf->SetDrawColor(50.0);
		$pdf->Line(22.0, $pdf->GetY()+$lh+4.7, 62, $pdf->GetY()+$lh+4.7);
		$pdf->Line(66.0, $pdf->GetY()+$lh+4.7, 138, $pdf->GetY()+$lh+4.7);
		$pdf->Line(142.0, $pdf->GetY()+$lh+4.7, 190, $pdf->GetY()+$lh+4.7);

		$pdf->Image($path.'/files/sign_mw.png', 143.0, $pdf->GetY(), 24);
		$pdf->Text(23.0, $pdf->GetY()+$lh*3+1, 'Datum'); $pdf->Text(67.0, $pdf->GetY()+$lh*2+.4, 'X');
		$pdf->Text(67.0, $pdf->GetY()+$lh*3+1, 'Unterschrift Interessent');
		$pdf->Text(143.0, $pdf->GetY()+$lh*3+1, 'Unterschrift Vermittler');

	// footer
		// reset font to very small
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-4, 0, 2);
		$pdf->SetXY(19.5, 270);
		$pdf->MultiCell(171.0, 2.15, project::gts('antrag'));




		if(isset($l)) unset($l);
		unset($interestedTextLine1); unset($interestedTextLine2); unset($interestedInsurance); unset($isearch);

	if($complete==2) { $pdf->AddPage('P'); }
}
