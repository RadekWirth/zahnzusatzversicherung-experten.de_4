<?

/**
 * Inter Antrag
 */

if(!$complete && file_exists($path.'/web-modules/crmIntern/contracts/inter.php.inc'))
{
	include('inter.php.inc');
} else {


// Vars
$personInsuredOther = $data['person']['pid'] !== $data['personInsured']['pid'] ? true : false;
$isGerman = (strtolower($data['nation']) === 'deutsch' || $data['nation'] === 'DEU') ? true : false;
$personIsMale = $data['gender'] === 'male' ? true : false;
$personInsuredIsMale = $data['gender2'] === 'male' ? true : false;

// Const
define('CROWNS', $data['contractComplete']['tooth4'] ? $data['contractComplete']['tooth4'] : null);
define('MISSING_TOOTH', $data['t1'] ? $data['t1']  : null);
define('AGE_PERSONINSURED', actionHelper::getAge($data['personInsured']['birthdate']));
define('FAMILY_STATUS_SINGLE', 37);
define('FAMILY_STATUS_MARRIED', 38);
define('FAMILY_STATUS_WIDOWED', 39);
define('FAMILY_STATUS_DIVORCED', 40);


    $pagecount = $pdf->setSourceFile($path.'files/inter/2023/84fd88935044445ab4a30c8e938ce705418d5c4b.pdf');

/**
 * Page 1
 */
    $tplidx = $pdf->importPage(2, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

    $this->getCompanyStamp(36.0, 11.8, $pdf, $data['contractComplete']['sourcePage'], 9, 'horizontal', 'wide');

    //gelbe markierungen nur auf rückantwort drucken! 
    $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);
    if($i==0) $this->pdfRect(75, 2, 68, $pdf);
    $pdf->Text(75.0, 6, $titles[$i]);

    $pdf->SetFont($pdfCfg['fontFamily'], '', 9, 0, 2);

    $pdf->Text(164.0, 7.0, 'Vermittlernummer: 675363');

    // PBD / OBD
    $pdf->Text(149.4, 34.0, '678');
    $pdf->Text(157.4, 34.0, '678');

    // Person Data - Left
    $personIsMale ? $pdf->Text(90.2, 71, 'X') : $pdf->Text(98.0, 71, 'X');
    $pdf->Text(45, 75.2, $data['person']['forename']);
    $pdf->Text(25, 85.5, $data['person']['surname']);
    $pdf->Text(25, 95.2, $data['birthdate']);
    
    if ($data['person']['familyStatusLid']) {
        switch ($data['person']['familyStatusLid']) {
            case FAMILY_STATUS_SINGLE:
                $text = 1;
                break;
            case FAMILY_STATUS_MARRIED:
                $text = 2;
                break;
            case FAMILY_STATUS_DIVORCED:
                $text = 3;
                break;
            case FAMILY_STATUS_WIDOWED:
                $text = 4;
                break;
        }   
    } else {
        if ($i == 0) $this->pdfRect(37, 97.9, 3, $pdf, 3); // +10
    }

    $pdf->Text(37.8, 100.3, $text);

    if ($isGerman) {
        $pdf->Text(50, 110.1, 'X');
    } elseif ($data['nation']) {
        $pdf->Text(68, 109.9, $data['nation']);
    }

    if ($i == 0) $this->pdfRect(23, 117.0, 3, $pdf, 3);  
    if ($i == 0) $this->pdfRect(51.4, 117.0, 3, $pdf, 3);
    if ($i == 0) $this->pdfRect(89.6, 116.8, 19, $pdf, 3);

    if ($i == 0) $this->pdfRect(23, 122.0, 3, $pdf, 3); 
    if ($i == 0) $this->pdfRect(51.4, 122.0, 3, $pdf, 3); 
    if ($i == 0) $this->pdfRect(89.6, 121.9, 19, $pdf, 3);

    if ($i == 0) $this->pdfRect(23, 126.9, 3, $pdf, 3);  
    if ($i == 0) $this->pdfRect(51.4, 126.9, 3, $pdf, 3);
    if ($i == 0) $this->pdfRect(85.2, 127.0, 3, $pdf, 3);

    if ($i == 0) $this->pdfRect(23, 131.9, 3, $pdf, 3); 
    if ($i == 0) $this->pdfRect(51.4, 131.9, 3, $pdf, 3);
    if ($i == 0) $this->pdfRect(67.6, 131.7, 40, $pdf, 3); 
      
    if ($i == 0) $this->pdfRect(113.3, 112.5, 3, $pdf, 3); 
    if ($i == 0) $this->pdfRect(142.0, 112.5, 3, $pdf, 3);
      
    if ($i == 0) $this->pdfRect(113.3, 117.0, 3, $pdf, 3); 

    if ($i == 0) $this->pdfRect(113.3, 127.2, 3, $pdf, 3); 
    if ($i == 0) $this->pdfRect(122.3, 127.2, 3, $pdf, 3); 
 
#    if ($i == 0) $this->pdfRect(23, 155.2, 3, $pdf, 3); 
#    if ($i == 0) $this->pdfRect(32, 155.2, 3, $pdf, 3);      

    if ($data['job']) {
        $pdf->Text(115, 75.1, $data['job']);
    } else {
        if ($i == 0) $this->pdfRect(114, 72.9, 80, $pdf, 3);
    }

    // Person Data - Right
    $pdf->Text(115, 85, $data['street']);
    $pdf->Text(115, 104.7, $data['postcode']);
    $pdf->Text(132, 104.7, $data['city']);
    if ($data['phone']) {
        $pdf->Text(41, 146.3, $data['phone']);
    } else {
        if ($i == 0) $this->pdfRect(39, 144.4, 40, $pdf, 3);
    }
    if (!empty($data['email'])) {
        $pdf->Text(41, 159.5, $data['email']);
    } else {
        if ($i == 0) $this->pdfRect(39, 157.4, 40, $pdf, 3);
    }

    // SEPA
    $personIsMale ? $pdf->Text(90.2, 233.0, 'X') : $pdf->Text(98.4, 233.0, 'X');
    $pdf->Text(42, 236.5, $data['person']['forename']); // +51
    $pdf->Text(25, 245.6, $data['person']['surname']); // +51
    $pdf->Text(25, 254.2, $data['street']);
    $pdf->Text(25, 263.5, $data['postcode']);
    $pdf->Text(45, 263.5, $data['city']);

    if ($i == 0) $this->pdfRect(114, 251.5, 80, $pdf, 4);
    if ($i == 0) $this->pdfRect(114, 260.5, 80, $pdf, 4);
    if ($i == 0) $this->pdfRect(114, 232.6, 80, $pdf, 4);
    if ($i == 0) $this->pdfRect(114, 242, 8, $pdf, 4);
    if ($i == 0) $this->pdfRect(125, 242, 8, $pdf, 4);
    if ($i == 0) $this->pdfRect(136, 242, 8, $pdf, 4);
    if ($i == 0) $this->pdfRect(147, 242, 8, $pdf, 4);
    if ($i == 0) $this->pdfRect(158, 242, 8, $pdf, 4);
    if ($i == 0) $this->pdfRect(171, 242, 8, $pdf, 4);
/*
    $pdf->SetFont($pdfCfg['fontFamily'], 'B', 11, 0, 2);
    $pdf->Text(115, 215.8, 'X');
    $pdf->SetFont($pdfCfg['fontFamily'], '', 9, 0, 2);

    // Person Insure
    $pdf->Text(70, 251.4, $data['personInsured']['forename'] .' '. $data['personInsured']['surname']);
    if ($isGerman) {
        $pdf->Text(68.8, 257, 'X');
    } elseif ($data['nation']) {
        $pdf->Text(94, 256.6, $data['nation']);
    }

    if ($data['personInsured']['familyStatusLid']) {
        switch ($data['personInsured']['familyStatusLid']) {
            case FAMILY_STATUS_SINGLE:
                $text = 1;
                break;
            case FAMILY_STATUS_MARRIED:
                $text = 2;
                break;
            case FAMILY_STATUS_DIVORCED:
                $text = 3;
                break;
            case FAMILY_STATUS_WIDOWED:
                $text = 4;
                break;
        }   
    } 

   // $pdf->Text(83, 270.1, $text);




    if ($i == 0) $this->pdfRect(68.7, 259.8, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(78.7, 259.8, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(89.2, 259.8, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(68.7, 262.8, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(78.7, 262.8, 2, $pdf, 2);

*/
    // Hide PageNum
    $this->pdfClean(180, 286, 40, $pdf, 255, 255, 255, 5, 'F');


/**
 * Page 2
 */
    $tplidx = $pdf->importPage(3, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

    $pdf->Text(70, 21.1, $data['personInsured']['forename']);
    if($data['person']['pid'] != $data['personInsured']['pid'])
    {
	    $pdf->Text(70, 25.6, $data['personInsured']['surname']);
	    $isGerman = (strtolower($data['nation2']) === 'deutsch' || $data['nation2'] === 'DEU') ? true : false;
    }

    #if ($isGerman) {
        $pdf->Text(68.8, 30.4, 'X');
    #} elseif ($data['nation2']) {
        $pdf->Text(94, 30.2, $data['nation2']);
    #}

    if ($data['personInsured']['familyStatusLid']) {
        switch ($data['personInsured']['familyStatusLid']) {
            case FAMILY_STATUS_SINGLE:
                $text = 1;
                break;
            case FAMILY_STATUS_MARRIED:
                $text = 2;
                break;
            case FAMILY_STATUS_DIVORCED:
                $text = 3;
                break;
            case FAMILY_STATUS_WIDOWED:
                $text = 4;
                break;
        }   
    }

    if ($i == 0 && $text == 0) 
	$this->pdfRect(82.6, 55.0, 3, $pdf, 3);
    $pdf->Text(83, 57.5, $text); //+52.8
    $personInsuredIsMale ? $pdf->Text(88.9, 71.6, 'X') : $pdf->Text(96.7, 71.6, 'X');

    $pdf->Text(69.5, 71.2, $data['birthdate2']);



    if ($data['personInsured']['job']) {
        $pdf->Text(70, 75.9, $data['personInsured']['job']);
    } else {
        if ($i == 0) $this->pdfRect(69, 73.5, 40, $pdf, 3);
    }

    if ($i == 0) $this->pdfRect(68.7, 33.4, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(76.8, 33.4, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(90.0, 33.4, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(101.8, 33.4, 2, $pdf, 2);

    if ($i == 0) $this->pdfRect(68.7, 36.8, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(68.7, 43.3, 2, $pdf, 2);

    if ($i == 0) $this->pdfRect(68.7, 84, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(88.6, 84, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(68.7, 87.2, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(88.6, 87.2, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(68.7, 90.9, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(88.6, 90.9, 2, $pdf, 2);

    $pdf->Text(23.8, 109.2, 'X');
    $pdf->Text(58.6, 132.1, stringHelper::makeGermanDate($data['wishedBegin']));

    switch($pdfTemplate)
    {
        case project::gti('inter-z90'):
            $pdf->Text(23.6, 165.3, 'X');
	     break;
        case project::gti('inter-z90-zpro'):
            $pdf->Text(23.6, 165.3, 'X');
            $pdf->Text(25.9, 170.3, 'X');
            break;
        case project::gti('inter-z80'):
            $pdf->Text(23.6, 160.3, 'X');
        case project::gti('inter-z80-zpro'):
            $pdf->Text(23.6, 160.3, 'X');
            $pdf->Text(25.9, 170.3, 'X');
            break;
        case project::gti('inter-z70'):
            $pdf->Text(23.6, 155.4, 'X');

        case project::gti('inter-z70-zpro'):
            $pdf->Text(23.6, 155.4, 'X');
            $pdf->Text(25.9, 170.3, 'X');
            break;
    }

    $pdf->Text(58.7, 181.2, $data['price'] .' €');
    $pdf->Text(183.4, 186.4, $data['price']);

    # Zahlweise
    if ($i == 0) $this->pdfRect(47.8, 184.4, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(63.3, 184.4, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(79.9, 184.4, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(96.9, 184.4, 2, $pdf, 2);


    # Gesundheitsfragen
    if ($i == 0) $this->pdfRect(167, 237, 8, $pdf, 3); // +38
    if ($i == 0) $this->pdfRect(167, 254.3, 8, $pdf, 3);
    if ($i == 0) $this->pdfRect(167, 258.8, 8, $pdf, 3);
    if ($i == 0) $this->pdfRect(166.2, 233.4, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(173.0, 233.4, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(166.2, 268.8, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(173.0, 268.8, 2, $pdf, 2);

    if ($data['contractComplete']['tooth1'] > 0) {
        #$pdf->Text(166.5, 197.1, 'X');
        #$pdf->Text(168, 201.5, $data['contractComplete']['tooth1']);
    }

/*
    $pdf->Text(59, 264.4, '1');
    if ($data['personInsured']['insurance']) {
        $pdf->Text(110, 264.1, $data['personInsured']['insurance']);
    } else {
        if ($i == 0) $this->pdfRect(109, 261.3, 90, $pdf, 3);
    }
*/

    // Hide PageNum
    $this->pdfClean(180, 287, 40, $pdf, 255, 255, 255, 5, 'F');

/**
 * Page 3
 */
    $tplidx = $pdf->importPage(4, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

    $pdf->Text(59, 26.0, '1');
    if ($data['personInsured']['insurance']) {
        $pdf->Text(110, 26.0, $data['personInsured']['insurance']);
    } else 
        if ($i == 0) $this->pdfRect(109, 23.6, 90, $pdf, 3);

    // Hide PageNum
    $this->pdfClean(180, 287, 40, $pdf, 255, 255, 255, 5, 'F');

/**
 * Page 4
 */
    $tplidx = $pdf->importPage(5, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

    if ($i == 0) $this->pdfRect(84, 92.3, 55, $pdf, 5); // -68.5
    if ($i == 0) $this->pdfRect(84, 110.3, 55, $pdf, 5);
    if ($i == 0) $this->pdfRect(144, 110.3, 55, $pdf, 5);

    $pdf->SetFont($pdfCfg['fontFamily'], 'B', 12, 0, 2);

    if (AGE_PERSONINSURED < 18) {
        if ($i == 0) $this->pdfRect(144, 92.3, 55, $pdf, 5);
        $pdf->Text(145, 96.3, 'X');
    }

    $pdf->Text(85, 96.3, 'X');
    $pdf->Text(85, 115.3, 'X');
    $pdf->Text(145, 115.3, 'X');

    if ($i == 0) $this->pdfRect(24, 185.8, 55, $pdf, 5);
    if ($i == 0) $this->pdfRect(84, 185.8, 55, $pdf, 5);

    $pdf->Text(25, 189.0, 'X');
    $pdf->Text(85, 189.0, 'X');

    // PBD / OBD
    $pdf->SetFont($pdfCfg['fontFamily'], '', 10, 0, 2);
    $pdf->Text(25, 96.4, '675363');
    $pdf->Text(25, 114.4, 'D-YIRL-NDZ7C-33');


    // Hide PageNum
    $this->pdfClean(180, 287, 40, $pdf, 255, 255, 255, 5, 'F');

/**
 * Page 5
 */
    $tplidx = $pdf->importPage(6, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

/**
 * Page 6
 */
    $tplidx = $pdf->importPage(7, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);


/**
 * Page 7
 */
    $tplidx = $pdf->importPage(8, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);


if($complete)
	$pdf->addPage('P');

}
