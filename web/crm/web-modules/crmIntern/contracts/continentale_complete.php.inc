<?

$data['person']['age'] = actionHelper::getAge($data['contractComplete']['birthdate']);
$data['personInsured']['age'] = actionHelper::getAge($data['personInsured']['birthdate']);

if(!$complete && file_exists($path.'/web-modules/crmIntern/contracts/continentale.php.inc'))
{
	include('continentale.php.inc');
} else {


// File for generating Contract PDF for Conti CEZP CEZK
        $pagecount = $pdf->setSourceFile($path.'files/continentale/antrag/2024/Continentale Antrag Zahn-gültig ab 2203-2024.pdf');

        $tplidx = $pdf->importPage(1, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        $fontSize = 8;

	 if($i==0) $this->pdfClean(19, 7.2, 27, $pdf, 255, 255, 255, 7.5);
        
        #if($i==0) $this->pdfRect(29, 1.0, 68, $pdf);
        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $fontSize+5, 0, 2);
        $pdf->Text(20.0, 6.0, $titles[$i]);



    // reset font
        $pdfCfg['fontSize'] = $fontSize;
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        $this->getCompanyStamp(100.0, 5.0, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']-1.5);

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);

        $pdf->Text(101, 20, 'Insuro-Partner 11580');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);

	 $pdf->Text(30.6, 42.0, '8409785');
	 $pdf->Text(122.6, 62.0,'VM Experten                08142 651 3928');


        if($data['gender']=='male') {
            $pdf->Text(63.2, 78.7, 'X');
        } else {
            $pdf->Text(52.5, 78.7, 'X');
        }   

        $pdf->Text(30, 82.8, $data['person']['surname']); // 47.1 + 35.7
        $pdf->Text(110, 82.8, $data['person']['forename']);
        $pdf->Text(170, 82.8, $data['birthdate']);

        $pdf->Text(30, 90.0, $data['street']);
        $pdf->Text(105, 90.0, $data['postcode']);
        $pdf->Text(123, 90.0, $data['city']);

        
        
        if(empty($data['phone']))
        {
            if($i==0) $this->pdfRect(61.0, 95.0, 51.8, $pdf, 3.5);
        } else {
            $pdf->Text(63, 97.8, $data['phone']);
        }

        if(empty($data['email']))
        {
            if($i==0) $this->pdfRect(121.0, 95.0, 51.8, $pdf, 3.5);
        } else {
            $pdf->Text(123, 97.8, $data['email']);
        }

        



    //Zu versichernde Person 1
        // person insured

        $pdf->Text(35.0, 121.0, $data['personInsured']['surname']);
        $pdf->Text(108.0, 121.0, $data['personInsured']['forename']);
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);
        $pdf->Text(171.0, 121.0, $data['birthdate2']);
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);

        $pdf->Text(35.0, 138.2, $data['street']); // 108
        $pdf->Text(108.0, 138.2, $data['postcode']);
        $pdf->Text(134.0, 138.2, $data['city']);

        if($data['gender2']=='male')    {
            $pdf->Text(55.3, 117.0, 'X');
        } else {
            $pdf->Text(44.3, 117.0, 'X');
        }

        // Angehörigenstatus
        if($i==0) $this->pdfRect(58.5, 122.9, 3.0, $pdf, 3.0);
        if($i==0) $this->pdfRect(58.5, 127.6, 3.0, $pdf, 3.0);

        // Berufsstatus
        if($i==0) $this->pdfRect(184.0, 135.5, 12.0, $pdf, 3.5);

        // tariff, prices
        if($pdfTemplate == project::gti('conticezp-u')) {
            $pdf->Text(32.5, 250.9, 'CEZP-U');
        }
        if($pdfTemplate == project::gti('conticezk-u')) {           
            $pdf->Text(32.5, 250.9, 'CEZK-U');
        } 
        if($pdfTemplate == project::gti('continentale-ceze')) {           
            $pdf->Text(32.5, 250.9, 'CEZE'); 
        }   
        $pdf->Text(90.5, 250.9, $data['price']);
        $pdf->Text(90.5, 276.0, $data['price']);
        
        if($i==0) $this->pdfRect(147.0, 273.2, 8.0, $pdf, 3.5);

        $pdf->Text(113, 276.0, $data['begin']);


    // page 2
        $tplidx = $pdf->importPage(2, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        // A. Besteht für die versicherten Personen eine gesetzliche KV?
        $pdf->Text(23.0, 53.7, "1");
	 $pdf->Text(160.0, 26.0, "1");
        
        if ($data['personInsured']['insurance']) {
	     $pdf->Text(143.0, 26.6, "X");  

            $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
            $pdf->Text(35, 53.7, $data['personInsured']['insurance']);
            $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);
        } else {
            if ($i == 0) $this->pdfRect(142.6, 24.4, 3, $pdf, 3);
            #if ($i == 0) $this->pdfRect(162, 231.1, 3, $pdf, 3);

            if ($i == 0) $this->pdfRect(32, 51.4, 47, $pdf, 3);
            if ($i == 0) $this->pdfRect(91, 51.4, 38, $pdf, 3);
            if ($i == 0) $this->pdfRect(161, 51.4, 35, $pdf, 3);
        }

        // B. Bestanden in den letzten 5 Jahren...
        if($i==0) $this->pdfRect(172.0, 33.9, 3.0, $pdf, 3.0);
        if($i==0) $this->pdfRect(193.6, 33.9, 3.0, $pdf, 3.0);



        // Gesundheitszustand der zu versichernden Personen (sollen einfach nur markiert werden)
        // A
        if($i==0) $this->pdfRect(170.2, 196.9, 13.0, $pdf, 3.5);
        if($i==0) $this->pdfRect(186.4, 196.9, 13.0, $pdf, 3.5);
        // B
        if($i==0) $this->pdfRect(170.2, 206.4, 13.0, $pdf, 3.5);
        if($i==0) $this->pdfRect(186.4, 206.4, 13.0, $pdf, 3.5);
        // C
        if($i==0) $this->pdfRect(170.2, 216.5, 13.0, $pdf, 3.5);
        if($i==0) $this->pdfRect(186.4, 216.5, 13.0, $pdf, 3.5);
        // D
        #if($i==0) $this->pdfRect(170.2, 152.2, 13.0, $pdf, 3.5);
        #if($i==0) $this->pdfRect(186.4, 152.2, 13.0, $pdf, 3.5);

        if($i==0) $this->pdfRect(20.2, 242.6, 5.0, $pdf, 3.0);
        if($i==0) $this->pdfRect(30.4, 242.6, 5.0, $pdf, 3.0);
        if($i==0) $this->pdfRect(39.3, 242.6, 100.0, $pdf, 3.5);

    // page 3
        $tplidx = $pdf->importPage(3, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

    // page 4
        $tplidx = $pdf->importPage(4, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

    // page 5
        $tplidx = $pdf->importPage(5, '/MediaBox'); 
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

    // page 6
        $tplidx = $pdf->importPage(6, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        $pdf->Text(30.0, 72.1, $data['person']['surname']);
        $pdf->Text(120.0, 72.1, $data['person']['forename']);
        $pdf->Text(30.0, 79.9, $data['street']);
        $pdf->Text(114.0, 79.9, $data['postcode']);
        $pdf->Text(138.0, 79.9, $data['city']);

    // Bankdaten
        if($i==0) $this->pdfRect(24.3, 105.7, 120, $pdf, 3);
        if($i==0) $this->pdfRect(24.3, 114.2, 60, $pdf, 3);
        if($i==0) $this->pdfRect(24.3, 121.8, 30, $pdf, 3);
        if($i==0) $this->pdfRect(66, 121.8, 50, $pdf, 3);
        if($i==0) $this->pdfRect(140, 121.8, 50, $pdf, 3);

    // Erklärung zur Leistungsauszahlung
        #if($i==0) $this->pdfRect(22.3, 144.9, 60, $pdf, 3);
        #if($i==0) $this->pdfRect(116.0, 144.9, 60, $pdf, 3);
        #if($i==0) $this->pdfRect(22.3, 151.9, 38, $pdf, 3);
        #if($i==0) $this->pdfRect(68.0, 151.9, 38, $pdf, 3);
        #if($i==0) $this->pdfRect(115.6, 151.9, 38, $pdf, 3);

    // Versicherungsantrag mit Empfangsbestätigung
        if($i==0) $this->pdfRect(61.3, 165.4, 50, $pdf, 3);
    

    // Schlusserklärungen und Antragsunterschriften
        if($i==0) $this->pdfRect(21.3, 204.7, 40, $pdf, 3);
        if($i==0) $this->pdfRect(67.0, 204.7, 50, $pdf, 3);
        if($i==0) $this->pdfRect(21.3, 214.3, 35, $pdf, 3);
        if($i==0) $this->pdfRect(67.0, 214.3, 50, $pdf, 3);

        if(($data['person']['age'] <= 18 || $data['personInsured']['age'] <=18) && isset($data['personInsured']['age']))
        {
            if($i==0) $this->pdfRect(132.0, 215.3, 50, $pdf, 3);
        }
}
?>
