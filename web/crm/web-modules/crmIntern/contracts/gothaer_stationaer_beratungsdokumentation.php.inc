<?php


        $pagecount = $pdf->setSourceFile($path.'/files/gothaer/Stationaer/gothaer.Aktion.mediclinik.Beratungsdoku.04-2020.pdf');

        $tplidx = $pdf->importPage(1, '/MediaBox');

        $pdf->addPage('P');  // idx, x, y, w, h
        $pdf->useTemplate($tplidx, 0, 0, 210);
        $pdf->SetMargins(0, 0, 0);

	 $this->pdfClean(56, 63.5, 40.0, $pdf, 255, 255, 255, 16, 'F');

	 $pdf->Text(57.0, 66.0, $data['nameb']);
	 $pdf->Text(57.0, 70.0, $data['street']);
	 $pdf->Text(57.0, 74.0, $data['postcode'].' '.$data['city']);


	 $this->pdfClean(55, 112.5, 30.0, $pdf, 255, 255, 255, 5, 'F');

	 $pdf->Text(57.0, 115.6, date("d.m.Y"));