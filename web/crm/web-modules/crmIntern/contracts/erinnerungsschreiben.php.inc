<?
	
	$lines[] = "vor einigen Wochen haben Sie �ber unser Vergleichsportal ein Angebot f�r eine";
	$lines[] = "Zahnzusatzversicherung angefordert.";
	$lines[] = "";	
	$lines[] = "Wir hoffen, dass Sie zwischenzeitlich die Zeit gefunden haben, sich mit den";
	$lines[] = "Angebotsunterlagen zu befassen.";
	$lines[] = "";
	$lines[] = "Sollten Sie noch nicht restlos �berzeugt sein, w�rden wir uns sehr �ber eine";
	$lines[] = "R�ckmeldung von Ihnen freuen:";
	$lines[] = "";
	$lines[] = "     Sind Ihnen einzelne Details der Leistungen noch unklar?";
	$lines[] = "     Entsprechen die Leistungen der Versicherung nicht Ihren Vorstellungen?";
	$lines[] = "     K�nnen Sie sich zwischen verschiedenen Angeboten nicht entscheiden?";
	$lines[] = "";
	$lines[] = "Sprechen Sie uns an! Wir stehen Ihnen jederzeit gerne f�r ein Beratungs-";
	$lines[] = "gespr�ch zur Verf�gung, um den passenden Zahntarif f�r Sie zu finden.";
	$lines[] = "";
	$lines[] = "Rufen Sie einfach an unter 08142 - 651 39 28!";
	$lines[] = "";
	$lines[] = "Wir w�rden uns sehr freuen, wenn Sie uns Ihr Vertrauen schenken und den";
	$lines[] = "Abschluss der Zahnversicherung �ber uns realisieren.";
	$lines[] = "";
	$lines[] = "";
	$lines[] = "Mit freundlichen Gr��en";


	$pdf->Image($path.'/files/logo.png', 20.0, 20.0, 100.0);
	
	$pdf->SetLineWidth(0.05);
	$pdf->SetDrawColor(140);
	$pdf->Line(20.0, 42.0, 200.0, 42.0); 
	$pdf->Line(140.0, 42.0, 140.0, 255.0);
	$pdf->Line(20.0, 255.0, 200.0, 255.0);

	$pdf->SetTextColor(120);	
	$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

	$pdf->Text(20.0, 50.0, "Zahnzusatzversicherung-Experten.de");
	$pdf->Text(20.0, 53.0, "Maximilian Waizmann - Feursstr. 56 / RGB - 82140 Olching");

	$pdf->SetTextColor($pdfCfg['fontColor']);
	$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+2, 0, 2);
	
	$pdf->Text(20.0, 58.0, $data['gender']=='male'?"Herrn":"Frau");
	$pdf->Text(20.0, 62.5, $data['namec']);
	$pdf->Text(20.0, 67.0, $data['street']);
	$pdf->Text(20.0, 71.5, $data['postcode']." ".$data['city']);


	/* rechte Seite: Anschrift mw */
	$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
	
	$pdf->Text(143.0, 50.0, "Anschrift");
	$pdf->Text(143.0, 74.0, "Kontaktdaten");

	$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
	$pdf->Text(143.0, 56.0, "Versicherungsmakler Experten GmbH");
	$pdf->Text(143.0, 60.0, "Gesch�ftsf�hrer: Maximilian Waizmann");
	$pdf->Text(143.0, 64.0, "Feursstr. 56 / RGB");
	$pdf->Text(143.0, 68.0, "82140 Olching");		

	$pdf->Text(143.0, 80.0, "Telefon");
	$pdf->Text(156.0, 80.0, "08142 - 651 39 28");	
	$pdf->Text(143.0, 84.0, "Fax");
	$pdf->Text(156.0, 84.0, "08142 - 651 39 29");
	$pdf->Text(143.0, 90.0, "info@zahnzusatzversicherung-experten.de");
	$pdf->Text(143.0, 94.0, "www.zahnzusatzversicherung-experten.de");

	$pdf->Text(143.0, 102.0, "Olching, ".date('d.m.Y', time()));

	/* rechte Seite fertig */

	$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+3, 0, 2);	

	$pdf->Text(20.0, 102.0, "Unser Angebot f�r eine Top Zahnzusatzversicherung");

	$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1.5, 0, 2);
	
	$sgdh = $data['gender']=='male'?"r Herr ".$data['person']['surname']:" Frau ".$data['person']['surname'];
	$pdf->Text(20.0, 118.5, "Sehr geehrte".$sgdh.",");

	$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+0.5, 0, 2);

	for($li=0; $li<count($lines); $li++)
	{
		$pdf->Text(20.0, (124.50+$li*3.5), $lines[$li]);
	}
	$pdf->Image($path.'/files/circle.png', 22.0, 154.3, 1.2);
	$pdf->Image($path.'/files/circle.png', 22.0, 157.7, 1.2);
	$pdf->Image($path.'/files/circle.png', 22.0, 161.2, 1.2);

	$pdf->Image($path.'/files/sign_mw.png', 20.0, 205.0, 27.0);

	$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+0.5, 0, 2);
	$pdf->Text(20.0, 219.0, "Maximilian Waizmann");

	$pdf->Text(20.0, 234.0, "PS:");
 
	$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+0.5, 0, 2);
	$pdf->Text(26.0, 234.0, "Reagieren Sie rechtzeitig! Schieben Sie den Abschluss einer Zahn- ");
	$pdf->Text(20.0, 237.5, "versicherung nicht auf die lange Bank! Wenn es erst einmal so weit ist, dass der ");
	$pdf->Text(20.0, 241.0, "Zahnarzt Behandlungsbedarf diagnostiziert hat, ist es f�r eine Versicherung "); 
	$pdf->Text(20.0, 244.5, "leider zu sp�t. "); 

	
	/* Block Footer */
	$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-3, 0, 2);
	$pdf->SetXY(20.0, 257.0); 
	$pdf->MultiCell(171.0, 2.2, project::gts('antrag'));
	$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

	unset($lines);
?>