<?php
/* VARS */

$tariffName = $data['tariffName'];
$idt = $data['idt'];
$icon = array(
	'check' => $path.'/files/icons/check_fotolia.jpg',
    'post' => $path.'/files/icons/post.png',
    'mail' => $path.'/files/icons/arroba2.png',
    'phone' => $path.'/files/icons/phone16.png'
);
$logo = $path.'/files/logo.png';
$lh = 3.6; // line height
$lhb = $lh*2;
 


$toothScale = 0;
if ($data['contractComplete']['addNotes'] === 'Verl&auml;ngerung Summenstaffel auf 8 Jahre!') {
    $toothScale = 2;
}
if ($data['contractComplete']['addNotes'] === 'Geringere Summenstaffel w&auml;hrend der ersten 4 Jahre bei 2 - 3 fehlenden Z&auml;hnen' ||
    $data['contractComplete']['addNotes'] === 'Verminderte Leistungsstaffel in den ersten 4 Jahren!' ||
    $data['contractComplete']['addNotes'] === 'Verl&auml;ngerung Summenstaffel auf 6 Jahre!' ||
    $data['contractComplete']['addNotes'] === '8 Jahre Summenstaffel bei 2 - 3 fehlenden Zähnen') {
    $toothScale = 1;
}
$toothScale = 1;


// Add Fonts
$pdf->AddFont('PTSans-Regular', '','PTSans-Regular.php');
$pdf->AddFont('PTSans-Bold', '','PTSans-Bold.php');

$pdf->SetMargins(12, 0); 

/**
 * Page 1
 */
$pagecount = $pdf->setSourceFile($path.'/files/deckblatt.pdf');
$tplidx = $pdf->importPage(1, '/MediaBox');

$pdf->addPage('P');
$pdf->useTemplate($tplidx);

// Head
$pdf->SetDrawColor(200);
$pdf->SetFillColor(255, 255, 255); 
$pdf->RoundedRect(14, 5, 90, 84, 4, '1234', 'FD');

// Kopf Links
$pdf->Image($this->getCompanyLogoFile($idt), 18, 8, 0, 22);
$pdf->Image($logo, 140, 5, 60);

$pdf->SetTextColor(58, 105, 135);
$this->setPTSansFont(15, 'B', $pdf);


// Bei CSS ideal soll Tarifname rechts neben Logo
if ($data['idt'] == 38) {
    $pdf->Text(44, 24.6, html_entity_decode($tariffName));
} else {
    $pdf->SetY(30);
    $pdf->SetX(18);
    $pdf->MultiCell(80, 6, html_entity_decode($tariffName), 0, 'L', 0); 
}


$this->setPTSansFont(10, 'R', $pdf);
$pdf->SetY(44);
foreach($data['highlights'] as $highlight) {
    $y = $pdf->getY();
	$pdf->Image($icon['check'], 18, $y-2, 8);
    $pdf->SetX(26);
    $pdf->MultiCell(77, 5, html_entity_decode($highlight), 0, 'L', 0); 
    $pdf->Ln(4);
}

// Kopf Rechts
$x = 120;
$y = 30;
$this->setPTSansFont(14, 'B', $pdf);
$pdf->Text($x, $y, 'Persönliches Angebot für');
$this->setPTSansFont(12, 'R', $pdf);
$pdf->SetTextColor(40);  
$pdf->Text($x, $y+8, $data['namec']);
$pdf->Text($x, $y+14, 'geb. '. $data['birthdate']);
$pdf->Text($x, $y+20, 'Versicherungsbeginn: '. stringHelper::makeDateFromSignUp($data['wishedBegin']));

$this->setPTSansFont(14, 'R', $pdf);
$pdf->Text($x, $y+34, 'Monatlicher Beitrag:');
$this->setPTSansFont(14, 'B', $pdf);
$pdf->Text($x+44.8, $y+34, $data['price'] .' €');
$this->setPTSansFont(14, 'R', $pdf);
$pdf->Image($path.'/files/icons/coins24.png', $x+64, $y+27.5, 8);

//print_r($data)

//$pdf->RoundedRect(14, 150, 177, 30, 4, '1234', 'FD');

// Linke Spalte
$x = 14;
$benefitsX = 72;
$y = 100; 
$pdf->SetY($y);
$pdf->SetX($x);
$pdf->SetTextColor(58, 105, 135);
$this->setPTSansFont(14, 'B', $pdf);
$pdf->cMargin = 2;
$pdf->SetFillColor(58, 105, 135); 
$pdf->RoundedRect($x, $pdf->getY()-1, 90, 6, 2, '1234', 'FD');
$pdf->SetTextColor(255, 255, 255);
$this->setPTSansFont(12, 'B', $pdf);
$pdf->MultiCell(90, 4, 'Zahnersatz', 0, 'L', 0); 
$this->setPTSansFont(10, 'R', $pdf);
$pdf->SetTextColor(40); 
$pdf->Ln(4); $pdf->SetX($x);
$pdf->Cell(50, 4, 'Implantate', 0, 0, 'L');
$this->benefitsScale($data['benefits']['dentures']['implants']['value'], $benefitsX, $pdf->getY(), $pdf);
$pdf->Ln(6); $pdf->SetX($x);
$pdf->Cell(50, 4, 'Inlays', 0, 0, 'L');
$this->benefitsScale($data['benefits']['dentures']['inlays']['value'], $benefitsX, $pdf->getY(), $pdf);
$pdf->Ln(6); $pdf->SetX($x);
$this->setPTSansFont(10, 'R', $pdf);
$pdf->Cell(50, 4, 'Kronen, Brücken & Prothesen', 0, 0, 'L');
$this->benefitsScale($data['benefits']['dentures']['dentures']['value'], $benefitsX, $pdf->getY(), $pdf);
$pdf->Ln(6); $pdf->SetX($x);
$this->setPTSansFont(10, 'R', $pdf);
$pdf->Cell(50, 4, 'Keramikverblendungen', 0, 0, 'L');
$this->benefitsScale($data['benefits']['dentures']['facings']['value'], $benefitsX, $pdf->getY(), $pdf);
if ($data['benefits']['dentures']['facings']['text']) {
    $pdf->Ln(6); $pdf->SetX($benefitsX);
    $pdf->cMargin = 0;
    $this->setPTSansFont(9, 'R', $pdf); 
    $pdf->Cell(30, 4, $data['benefits']['dentures']['facings']['text'], 0, 0, 'L');
    $pdf->cMargin = 2;
}
$pdf->Ln(6); $pdf->SetX($x);
$this->setPTSansFont(10, 'R', $pdf);
$pdf->Cell(50, 4, 'Knochenaufbau', 0, 0, 'L');
$this->benefitsYesNo($data['benefits']['dentures']['boneBuilding']['value'], $benefitsX, $pdf->getY(), $pdf);
$pdf->Ln(6); $pdf->SetX($x);
$this->setPTSansFont(10, 'R', $pdf);
$pdf->Cell(50, 4, 'Funktionsanalyse/Funktionstherapie', 0, 0, 'L');
$this->benefitsYesNo($data['benefits']['dentures']['functionalTheraphy']['value'], $benefitsX, $pdf->getY(), $pdf);
$pdf->Ln(8); $pdf->SetX($x);
// Signal Iduna
if ($data['idt'] == 33 || $data['idt'] == 32) {
    $pdf->Cell(30, 4, 'realistische Schätzwerte inkl. Vorleistung GKV', 0, 0, 'L');
    $pdf->Ln(6); $pdf->SetX($x);
}
if ($data['benefits']['gkvInc'] == true) {
    $pdf->Cell(50, 4, '* Alle Leistungen sind inkl. Vorleistung der GKV', 0, 0, 'L');
} else {
    $pdf->Cell(50, 4, '* Alle Leistungen + Vorleistung GKV (max. 100%)', 0, 0, 'L');
}
// If type equals kids show orthodontic services
if ($data['contractComplete']['contractType'] === 'kids') {
    $pdf->Ln(12); $pdf->SetX($x);
    $pdf->cMargin = 2;
    $pdf->SetFillColor(58, 105, 135); 
    $pdf->RoundedRect($x, $pdf->getY()-1, 90, 6, 2, '1234', 'FD');
    $pdf->SetTextColor(255, 255, 255);
    $this->setPTSansFont(12, 'B', $pdf);
    $pdf->MultiCell(90, 4, 'Kieferorthopädie (Zahnspangen)', 0, 'L', 0); 
    $this->setPTSansFont(10, 'R', $pdf);
    $pdf->SetTextColor(40); 
    $pdf->Ln(4); $pdf->SetX($x);
    $pdf->Cell(50, 4, 'KIG 2 (Zahnspangen)', 0, 0, 'L');
    $this->benefitsScale($data['benefits']['orthodontics']['orthodontics_kig2']['value'], $benefitsX, $pdf->getY(), $pdf);
    if ($data['benefits']['orthodontics']['orthodontics_kig2']['text']) {
        $pdf->Ln(6); $pdf->SetX($benefitsX);
        $pdf->cMargin = 0;
        $this->setPTSansFont(9, 'R', $pdf); 
        $pdf->Cell(30, 4, $data['benefits']['orthodontics']['orthodontics_kig2']['text'], 0, 0, 'L');
        $pdf->cMargin = 2;
    }
    $pdf->Ln(6); $pdf->SetX($x);
    $pdf->Cell(50, 4, 'KIG 3-5 (Zahnspangen)', 0, 0, 'L');
    $this->benefitsScale($data['benefits']['orthodontics']['orthodontics_kig35']['value'], $benefitsX, $pdf->getY(), $pdf);
    if ($data['benefits']['orthodontics']['orthodontics_kig35']['text']) {
        $pdf->Ln(6); $pdf->SetX($benefitsX);
        $pdf->cMargin = 0;
        $this->setPTSansFont(9, 'R', $pdf); 
        $pdf->Cell(30, 4, $data['benefits']['orthodontics']['orthodontics_kig35']['text'], 0, 0, 'L');
        $pdf->cMargin = 2;
    }
}
$pdf->Ln(12); $pdf->SetX($x);
$pdf->SetFillColor(58, 105, 135); 
$pdf->RoundedRect($x, $pdf->getY()-1, 90, 6, 2, '1234', 'FD');
$pdf->SetTextColor(255, 255, 255);
$this->setPTSansFont(12, 'B', $pdf);
$pdf->MultiCell(90, 4, 'Zahnbehandlung', 0, 'L', 0); 
$this->setPTSansFont(10, 'R', $pdf);
$pdf->SetTextColor(40); 
$pdf->Ln(4); $pdf->SetX($x);
$pdf->Cell(50, 4, 'Professionelle Zahnreinigung', 0, 0, 'L');
$this->benefitsScale($data['benefits']['prophylaxis']['dentalClean']['value'], $benefitsX, $pdf->getY(), $pdf);
if ($data['benefits']['prophylaxis']['dentalClean']['text']) {
    $pdf->cMargin = 0;
    $pdf->Ln(6); $pdf->SetX($benefitsX);
    $this->setPTSansFont(9, 'R', $pdf);
    $pdf->Cell(30, 4, $data['benefits']['prophylaxis']['dentalClean']['text'], 0, 0, 'L');
    $this->setPTSansFont(10, 'R', $pdf);
    $pdf->cMargin = 2;
}
$pdf->Ln(6); $pdf->SetX($x);
$pdf->Cell(50, 4, 'Hochwertige Kunststofffüllungen', 0, 0, 'L');
$this->benefitsScale($data['benefits']['prophylaxis']['plasticFillings']['value'], $benefitsX, $pdf->getY(), $pdf, $data['idt']);
// If type equals kids we need more space
if ($data['contractComplete']['contractType'] !== 'kids') {
    $pdf->Ln(6); $pdf->SetX($x);
    $pdf->MultiCell(50, 4, 'Wurzelbehandlung ohne Kassenvorleistung', 0, 'L', 0);
    $this->benefitsScale($data['benefits']['prophylaxis']['rootWithoutAdvancePayment']['value'], $benefitsX, $pdf->getY()-4, $pdf);
    $pdf->Ln(2); $pdf->SetX($x);
    $pdf->MultiCell(50, 4, 'Wurzelbehandlung mit Kassenvorleistung', 0, 'L', 0);
    $this->benefitsScale($data['benefits']['prophylaxis']['rootAdvancePayment']['value'], $benefitsX, $pdf->getY()-4, $pdf);
    $pdf->Ln(2); $pdf->SetX($x);
    $pdf->MultiCell(50, 4, 'Paradontosebehandlung ohne Kassenvorleistung', 0, 'L', 0);
    $this->benefitsScale($data['benefits']['prophylaxis']['peridontitisWithoutAdvancePayment']['value'], $benefitsX, $pdf->getY()-4, $pdf);
    $pdf->Ln(2); $pdf->SetX($x);
    $pdf->MultiCell(50, 4, 'Paradontosebehandlung mit Kassenvorleistung', 0, 'L', 0);
    $this->benefitsScale($data['benefits']['prophylaxis']['peridontitisAdvancePayment']['value'], $benefitsX, $pdf->getY()-4, $pdf);
}


$pdf->Ln(12); $pdf->SetX($x);
$pdf->SetFillColor(58, 105, 135); 
$pdf->RoundedRect($x, $pdf->getY()-1, 90, 6, 2, '1234', 'FD');
$pdf->SetTextColor(255, 255, 255);
$this->setPTSansFont(12, 'B', $pdf);
$pdf->MultiCell(90, 4, 'Allgemeine Infos', 0, 'L', 0); 
$pdf->Ln(4); 

$waitingTime = $data['benefits']['periodsAndLimits']['waitingTime']['value'];
if ($data['benefits']['periodsAndLimits']['waitingTime']['value'] == false) {
    $waitingTime = 'keine Wartezeiten!';
} 
$pdf->SetX($x);
$this->setPTSansFont(10, 'R', $pdf);
$pdf->SetTextColor(40);  
$pdf->MultiCell(80, 4, 'Wartezeit: '. $waitingTime, 0, 'L', 0);



// Rechte Spalte
$x = 110;
$y = 100; 

$pdf->SetY($y);
$pdf->SetX($x);
$this->setPTSansFont(12, 'B', $pdf); 

// Leistungsbegrenzungen  
$pdf->SetFont('PTSans-Bold', '', 12);$pdf->cMargin = 2;
$pdf->SetFillColor(58, 105, 135); 
$pdf->RoundedRect($x, $pdf->getY()-1, 90, 6, 2, '1234', 'FD');
$pdf->SetTextColor(255, 255, 255);
$pdf->MultiCell(90, 4, 'Leistungsbegrenzungen', 0, 'L', 0);

if ($toothScale === 0) {
    foreach ($data['benefits']['periodsAndLimits']['maxBenefits'] as $maxBenefit) {
        if ($maxBenefit['showOnlyChild'] !== true || $maxBenefit['showOnlyChild'] === true && $data['contractComplete']['contractType'] === 'kids') {
            $this->maxBenefits($maxBenefit['maxBenefits'], $x, $pdf->GetY(), $pdf, array(
                'head' => html_entity_decode($maxBenefit['head']),
                'introduction' => html_entity_decode($maxBenefit['introduction']), 
                'timeIntervals' => $maxBenefit['timeIntervals'],
                'suffix' => $maxBenefit['suffix'],
                'outro' => html_entity_decode($maxBenefit['outro'])));
        }
    }
    $pdf->Ln(3); $pdf->SetX($x);
    $pdf->MultiCell(90, 4, html_entity_decode($data['benefits']['periodsAndLimits']['maxBenefitsOutro']), 0, 'L', 0);
    $pdf->Ln(3); 

    if ($data['benefits']['periodsAndLimits']['maxBenefitsExtraText']) {
        foreach ($data['benefits']['periodsAndLimits']['maxBenefitsExtraText'] as $extra) {
            $pdf->SetTextColor(58, 105, 135);
            $pdf->SetFont('PTSans-Bold', '', 10);
            $pdf->SetX($x);
            $pdf->MultiCell(90, 4, html_entity_decode($extra['head']), 0, 'L', 0); 
            $pdf->Ln(3); 
            $pdf->SetX($x);
            $pdf->SetTextColor(40);  
            $pdf->SetFont('PTSans-Regular', '', 10);
            $pdf->MultiCell(90, 4, html_entity_decode($extra['text']), 0, 'L', 0); 
            $pdf->Ln(4); 
        }
    }
}

if ($toothScale === 1) {
    foreach ($data['benefits']['periodsAndLimits']['maxBenefits'] as $maxBenefit) {
        if ($maxBenefit['showOnlyChild'] !== true || $maxBenefit['showOnlyChild'] === true && $data['contractComplete']['contractType'] === 'kids') {
            $this->maxBenefits($maxBenefit['maxBenefits1'], $x, $pdf->GetY(), $pdf, array(
                'head' => html_entity_decode($maxBenefit['head']),
                'introduction' => html_entity_decode($maxBenefit['introduction']), 
                'timeIntervals' => $maxBenefit['timeIntervals1'],
                'suffix' => $maxBenefit['suffix']));
        }
    }
    $pdf->Ln(3); $pdf->SetX($x);
    $pdf->MultiCell(90, 4, html_entity_decode($data['benefits']['periodsAndLimits']['maxBenefitsOutro']), 0, 'L', 0);
    $pdf->Ln(3); 
}

if ($toothScale === 2) {
    foreach ($data['benefits']['periodsAndLimits']['maxBenefits'] as $maxBenefit) {
        if ($maxBenefit['showOnlyChild'] !== true || $maxBenefit['showOnlyChild'] === true && $data['contractComplete']['contractType'] === 'kids') {
            $this->maxBenefits($maxBenefit['maxBenefits1'], $x, $pdf->GetY(), $pdf, array(
                'head' => html_entity_decode($maxBenefit['head']),
                'introduction' => html_entity_decode($maxBenefit['introduction']), 
                'timeIntervals' => $maxBenefit['timeIntervals1'],
                'suffix' => $maxBenefit['suffix']));
        }
    }
    $pdf->Ln(3); $pdf->SetX($x);
    $pdf->MultiCell(90, 4, html_entity_decode($data['benefits']['periodsAndLimits']['maxBenefitsOutro']), 0, 'L', 0);
    $pdf->Ln(3); 
}





// Text Footer
$y = 268;
$x = 14;
$this->pdfClean(0, $y-5, 210, $pdf, 236, 236, 236, 37, 'F');
$pdf->SetX($x);
$this->setPTSansFont(10, 'B', $pdf);
$pdf->Text(16, $y+1, 'Sie haben Fragen?');
$this->setPTSansFont(10, 'R', $pdf);
$pdf->Image($icon['phone'], 16, $y+5, 5);
$pdf->Text(24, $y+10, '08142 - 651 39 28');
$pdf->Image($icon['mail'], 16, $y+12, 5);
$pdf->Text(24, $y+17, 'info@zahnzusatzversicherung-experten.de');
//$this->setPTSansFont(10, 'B', $pdf);
$this->setPTSansFont(10, 'B', $pdf);
$pdf->Text(16, 293, 'Auf der Rückseite finden Sie eine Checkliste mit wertvollen Tipps zum Ausfüllen des Antragsformulares!');
//$this->setPTSansFont(10, 'R', $pdf);




/**
 * Page 2
 */
    $pagecount = $pdf->setSourceFile($path.'/files/deckblatt.pdf');
    $tplidx = $pdf->importPage(2, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx);



