<?
// File for generating Contract PDF for DFV Zahnschutz-Exklusiv

define('FAMILY_STATUS_SINGLE', 37);
define('FAMILY_STATUS_MARRIED', 38);
define('FAMILY_STATUS_WIDOWED', 39);
define('FAMILY_STATUS_DIVORCED', 40);

$pdf->SetTextColor(50);  

if ($i !== 1) {
	$prefix = '';
} else {
	$prefix = '_bw';
}
$pagecount = $pdf->setSourceFile($path .'/files/dfv/dfv-exkl-plus_2015'. $prefix .'.pdf');

// Seite 1
$tplidx = $pdf->importPage(1, '/MediaBox');
$pdf->addPage('P');
$pdf->useTemplate($tplidx, -5, 5, 220, 296);

	$this->contractInfoTitle($i, $pdf, $titles[$i]);
	#$this->getCompanyLogo(9.0, 0.0, $pdf, $data['contractComplete']['sourcePage'], $i, 40);
    $this->getCompanyStamp(5.0, 12, $pdf, $data['contractComplete']['sourcePage'], 8.0, 'horizontal', 'wide');

	$pdf->SetFont($pdfCfg['fontFamily'], '', 12, 0, 2);

	// Vermittlernummer
	$pdf->Text(167, 41.6, '0   1   0   3   1   5');

	if ($data['gender'] === 'female') {
		$pdf->Text(39.4, 42, 'X');
	} else {
		$pdf->Text(52.8, 42, 'X');
	}
	$pdf->Text(40, 48.2, $data['person']['forename']);
	$pdf->Text(40, 54.9, $data['person']['surname']);
	$pdf->Text(40, 61.6, $data['street']);
	$pdf->Text(40, 68.4, $data['postcode']);
	$pdf->Text(76, 68.4, $data['city']);
	if ($data['job']) {
		$pdf->Text(40, 74.7, $data['job']);	
	} else {
		if ($i == 0) $this->pdfRect(39.5, 70.5, 160.5, $pdf, 5);
	}
	$pdf->SetFont($pdfCfg['fontFamily'], '', 11, 0, 2);
	if ($data['person']['familyStatusLid'] === FAMILY_STATUS_SINGLE) {
		$pdf->Text(39.3, 83.2, 'X');
	} elseif ($data['person']['familyStatusLid'] === FAMILY_STATUS_MARRIED) {
		$pdf->Text(52.8, 83.2, 'X');
	} elseif ($data['person']['familyStatusLid'] == FAMILY_STATUS_DIVORCED) {
		$pdf->Text(75, 83.2, 'X');
	} elseif ($data['person']['familyStatusLid'] == FAMILY_STATUS_WIDOWED) {
		$pdf->Text(97.8, 83.2, 'X');
	} else {
		if ($i == 0) $this->pdfRect(39.4, 80, 2.5, $pdf, 2.5);
		if ($i == 0) $this->pdfRect(52.6, 80, 2.5, $pdf, 2.5);
		if ($i == 0) $this->pdfRect(74.8, 80, 2.5, $pdf, 2.5);
		if ($i == 0) $this->pdfRect(97.6, 80, 2.5, $pdf, 2.5);
	}
	if ($data['nation'] === 'DEU' || strtolower($data['nation']) === 'deutsch') {
		$pdf->Text(40, 89.5, 'Deutsch');
	}




	$pdf->Text(40, 96.3, $data['birthdate'][0]);
	$pdf->Text(46.2, 96.3, $data['birthdate'][1]);
	$pdf->Text(52.4, 96.3, $data['birthdate'][3]);
	$pdf->Text(58.6, 96.3, $data['birthdate'][4]);
	$pdf->Text(64.8, 96.3, $data['birthdate'][6]);
	$pdf->Text(71, 96.3, $data['birthdate'][7]);
	$pdf->Text(77.2, 96.3, $data['birthdate'][8]);
	$pdf->Text(83.4, 96.3, $data['birthdate'][9]);

	$beginMonth = substr($data['begin'], 0, 2);
	$beginYear = substr($data['begin'], 6, 1);
	$pdf->Text(136.7, 96.3, $beginMonth[0]);
	$pdf->Text(142.3, 96.3, $beginMonth[1]);
	$pdf->Text(166, 96.3, $beginYear);
	
	if ($data['phone']) {
		$pdf->Text(40, 103.3, $data['phone']);
	} else {
		if ($i == 0) $this->pdfRect(39.5, 98.9, 54, $pdf, 5);
	}
	if ($data['email']) {
		$pdf->Text(40, 116.3, $data['email']);
	} else {
		if ($i == 0) $this->pdfRect(39.5, 112.2, 54, $pdf, 5);	
	}


	if($data['person']['pid'] != $data['personInsured']['pid'])
	{
		// VN <> AS
		$pdf->Text(40, 142.6, $data['person']['forename']);
		$pdf->Text(40, 136.2, $data['person']['surname']);

		$pdf->Text(40, 150.0, $data['birthdate2'][0]);
		$pdf->Text(46.2, 150.0, $data['birthdate2'][1]);
		$pdf->Text(52.4, 150.0, $data['birthdate2'][3]);
		$pdf->Text(58.6, 150.0, $data['birthdate2'][4]);
		$pdf->Text(64.8, 150.0, $data['birthdate2'][6]);
		$pdf->Text(71, 150.0, $data['birthdate2'][7]);
		$pdf->Text(77.2, 150.0, $data['birthdate2'][8]);
		$pdf->Text(83.4, 150.0, $data['birthdate2'][9]);

		// Add Height for next Question
		$ah = 11.4;
	}



$tplidx = $pdf->importPage(2, '/MediaBox');
$pdf->addPage('P');
$pdf->useTemplate($tplidx);

	$age = actionHelper::getAge($data['personInsured']['birthdate']);


	switch($age) {
		case $age <= 20:
			$pdf->Text(50.6, 47.4 + $ah, 'X');
			break;
		case $age <= 30:
			$pdf->Text(70.8, 47.4 + $ah, 'X');
			break;
		case $age <= 40:
			$pdf->Text(92, 47.4 + $ah, 'X');
			break;
		case $age <= 50:
			$pdf->Text(113, 47.4 + $ah, 'X');
			break;
		case $age <= 60:
			$pdf->Text(134.3, 47.4 + $ah, 'X');
			break;
		case $age <= 70:
			$pdf->Text(155.5, 47.4 + $ah, 'X');
			break;
		case $age > 70:
			$pdf->Text(176.5, 47.4 + $ah, 'X');
			break;
	}

	// Markierungen
	if ($i == 0) $this->pdfRect(53.6, 99, 11, $pdf, 5);
	if ($i == 0) $this->pdfRect(70.7, 99, 45.8, $pdf, 5);
	if ($i == 0) $this->pdfRect(123, 99, 56, $pdf, 5);
	if ($i == 0) $this->pdfRect(42.8, 110.5, 62, $pdf, 5);

	// Zahlweise Markierungen
	if ($i == 0) $this->pdfRect(41.7, 118.5, 2.5, $pdf, 2.5);
	if ($i == 0) $this->pdfRect(62.4, 118.5, 2.5, $pdf, 2.5);
	if ($i == 0) $this->pdfRect(87.5, 118.5, 2.5, $pdf, 2.5);
	if ($i == 0) $this->pdfRect(110.2, 118.5, 2.5, $pdf, 2.5);

	$pdf->Text(43, 128.4, $data['namec']);
	$pdf->Text(43, 135.4, $data['street']);
	$pdf->Text(43, 142, $data['postcode']);
	$pdf->Text(79.8, 142, $data['city']);


	if ($i == 0) $this->pdfRect(93, 148.8, 88, $pdf, 5);
	$pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);
	if ($i == 0) $pdf->Text(94, 153.1, 'X');

	if ($i == 0) $this->pdfRect(93, 188.7, 88, $pdf, 5);
	if ($i == 0) $pdf->Text(94, 193, 'X');

	if ($i == 0) $this->pdfRect(14, 263.6, 73, $pdf, 5);
	if ($i == 0) $this->pdfRect(94, 263.6, 88, $pdf, 5);
	if ($i == 0) $pdf->Text(94, 267.8, 'X');

$tplidx = $pdf->importPage(3, '/MediaBox');
$pdf->addPage('P');
$pdf->useTemplate($tplidx);

if($complete == '2')
{
	$pdf->addPage('P');
}