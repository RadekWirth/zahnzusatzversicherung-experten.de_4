<?

if($complete)
{
// File for generating Contract PDF for ARAGz90
        if ($i==0) {
            $pagecount = $pdf->setSourceFile($path.'files/arag/antrag/2022/ARAG.Dent-Tarife.Antrag.01-2022.pdf');
        } else {
            $pagecount = $pdf->setSourceFile($path.'files/arag/antrag/2022/ARAG.Dent-Tarife.Antrag.01-2022.pdf');
        }
        $tplidx = $pdf->importPage(1, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        $fontSize = 8;
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $fontSize+3, 0, 2);
        $pdf->Text(168.0, 4.5, 'Insuro Partner 11580');

        if($i==0) $this->pdfRect(29, 1.0, 68, $pdf);
        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $fontSize+5, 0, 2);
        $pdf->Text(29.0, 5.0, $titles[$i]);

        $pdf->Text(41, 172.4, $data['begin']);

    // reset font
        $pdfCfg['fontSize'] = $fontSize;
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        // Makler Daten schreiben
        #$pdf->Text(40.5, 24.5, "5");
        #$pdf->Text(44.5, 24.5, "1");
        #$pdf->Text(49.0, 24.5, "7");
        #$pdf->Text(53.5, 24.5, "3");
        #$pdf->Text(58.0, 24.5, "0");

        #$pdf->Text(68.5, 24.5, "6");
        #$pdf->Text(73.0, 24.5, "0");
        #$pdf->Text(77.0, 24.5, "0");

    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-2, 0, 2);

        $this->getCompanyStamp(116, 12, $pdf, $data['contractComplete']['sourcePage'], 8);

    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);

        // Neuantrag
        $pdf->Text(31.8, 60.7, 'X');

        // Fernabsatz
        $pdf->Text(89.8, 60.7, 'X');

        if($data['gender']=='male') {
            $pdf->Text(31.8, 73.7, 'X');
        } else {
            $pdf->Text(31.8, 77.2, 'X');
        }   

        $pdf->Text(44, 78, $data['person']['forename']); //+9
        $pdf->Text(44, 87, $data['person']['surname']);

        $pdf->Text(38, 96, $data['street']);
        $pdf->Text(147.6, 87.0, $data['birthdate']);

        if(empty($data['phone']))
        {
            if($i==0) $this->pdfRect(145, 92, 50, $pdf);
        } else
            $pdf->Text(146, 96.3, $data['phone']);

        if(empty($data['email']))
        {
            if($i==0) $this->pdfRect(32, 110, 68, $pdf);
        } else
            $pdf->Text(38, 114, $data['email']);

        $pdf->Text(38, 105, $data['postcode']);
        $pdf->Text(60, 105, $data['city']);

        if(empty($data['job']))
        {
            if($i==0) $this->pdfRect(64.0, 120, 68, $pdf);
        } else
            $pdf->Text(66, 123.3, $data['job']);

        //mark Angestellter / Selbständiger / Freiberufler
        if($i==0) $this->pdfRect(31.7, 116.8, 2, $pdf, 2);
        if($i==0) $this->pdfRect(31.7, 120.4, 2, $pdf, 2);
        if($i==0) $this->pdfRect(31.7, 123.7, 2, $pdf, 2);

        // Ich bin damit einverstanden....
        if($i==0) $this->pdfRect(31.7, 145.3, 2, $pdf, 2);
        

    //Zahlungsweise
        //jährlich (4% skonto)
        if($i==0) $this->pdfRect(31.7, 178.9, 2, $pdf, 2);

        //halbjährlich (2% skonto)
        if($i==0) $this->pdfRect(65.4, 178.9, 2, $pdf, 2);

        //vierteljährlich
        if($i==0) $this->pdfRect(102.4, 178.9, 2, $pdf, 2);

        //monatlich
        if($i==0) $this->pdfRect(123.7, 178.9, 2, $pdf, 2);

        //zum ersten eines Monats
        if($i==0) $this->pdfRect(102.4, 182.3, 2, $pdf, 2);

        //zum 15. eines Monats
        if($i==0) $this->pdfRect(131.5, 182.3, 2, $pdf, 2);

    //Zu versichernde Person 1
        // person insured

        //mark Angestellter / Selbständiger / Freiberufler
        if($i==0) $this->pdfRect(31.7, 220.6, 2, $pdf, 2);
        if($i==0) $this->pdfRect(31.7, 224.1, 2, $pdf, 2);
        if($i==0) $this->pdfRect(31.7, 227.6, 2, $pdf, 2);

        $pdf->Text(44, 200.5, $data['personInsured']['surname']);
        $pdf->Text(44, 209.5, $data['personInsured']['forename']);

        $pdf->Text(35, 218, $data['street'].', '.
            $data['postcode'].' '.$data['city']);

        $pdf->Text(148, 209.5, $data['birthdate2']);

        if(empty($data['job2']))
        {
            if($i==0) $this->pdfRect(64.5, 223.4, 68, $pdf);
        } else
            $pdf->Text(66, 227.5, $data['job2']);


        if($data['gender2']=='male')    {
            $pdf->Text(31.7, 195.7, 'X');
        } else {
            $pdf->Text(31.7, 199.4, 'X');
        }


	// Angehörigenstatus
        if($i==0) $this->pdfRect(31.7, 241.8, 2, $pdf, 2);
        if($i==0) $this->pdfRect(31.7, 245.4, 2, $pdf, 2);
        if($i==0) $this->pdfRect(31.7, 249.1, 2, $pdf, 2);
	
        if($i==0) $this->pdfRect(105.2, 241.8, 2, $pdf, 2);
        if($i==0) $this->pdfRect(105.2, 245.4, 2, $pdf, 2);
        if($i==0) $this->pdfRect(105.2, 249.1, 2, $pdf, 2);

        if($i==0) $this->pdfRect(109.7, 251.1, 40, $pdf, 5);
        

    // page 2
        $tplidx = $pdf->importPage(2, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

       // tariff
        switch($pdfTemplate)
        {
            case project::gti('arag-dent70'):
                $pdf->Text(45.5, 152.4, "X");
                break;
            case project::gti('arag-dent90'):
                $pdf->Text(64.6, 152.4, "X");
                break;
            case project::gti('arag-dent90plus'):
                $pdf->Text(83.8, 152.4, "X");
                break;
            case project::gti('arag-dent100'):
                $pdf->Text(105.2, 152.4, "X");
                break;
        }

        // prices
        $pdf->Text(143, 157.6, $data['bonusbase']);
        $pdf->Text(180, 157.6, $data['bonus']);
        $pdf->Text(180, 193.5, $data['price']);
 

/*

*/

    // page 3
        $tplidx = $pdf->importPage(3, '/MediaBox');
        $pdf->addPage('P', array(210, 320));
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        // Person 1 2
        if($i==0) $this->pdfRect(45.0, 58.8, 2.6, $pdf, 2.6); // -43.8
        if($i==0) $this->pdfRect(52.7, 58.8, 2.6, $pdf, 2.6);

        #if(empty($data['insurance2']))
        #{
        #   if($i==0) $this->pdfRect(58, 65, 86, $pdf);
        #} else
        #   $pdf->Text(60.0, 69.0, 'X'.$data['insurance2']);

        //2a, Person 1 94
        if($i==0) $this->pdfRect(131.3, 75.2, 2, $pdf, 2);
        if($i==0) $this->pdfRect(142.5, 75.2, 2, $pdf, 2);
        if($i==0) $this->pdfRect(131.3, 84.2, 2, $pdf, 2);
        if($i==0) $this->pdfRect(142.5, 84.2, 2, $pdf, 2);
        if($i==0) $this->pdfRect(131.3, 114.2, 2, $pdf, 2);
        if($i==0) $this->pdfRect(142.5, 114.2, 2, $pdf, 2);
/*
    if($data['contractComplete']) {
        if($data['incare']) {
            $pdf->Text(131.1, 77.4, 'X');
        } else
            $pdf->Text(142.3, 77.4, 'X');


        // Paradontose Behandlung
        // "yes" 20140314
        if ($data['contractComplete']['periodontitis'] == 'yes') {
            $pdf->Text(131.1, 86.6, 'X');
        } else
            $pdf->Text(142.3, 86.6, 'X');

    }
*/
        //2c
        if($i==0) $this->pdfRect(131.6, 97.1, 8, $pdf, 4);
/*
    if($data['contractComplete']) {
        $tsum = $data['t1'] + $data['t2'];
        if ($tsum == 0) {
            $pdf->Text(142.4, 101.7, 'X');
        } 

        $pdf->Text(134.1, 100.8, $tsum);

        if ($data['contractComplete']['periodontitis'] != 'yes' && ( ! $data['incare']) && $tsum > 0) {
            $pdf->Text(131.1, 116.6, 'X'); // b, insure toothes, yes
        } else
            $pdf->Text(142.3, 116.6, 'X'); // b, insure toothes, no

    }
*/


    // page 4 
        $tplidx = $pdf->importPage(4, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

    //Unterschriften
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $fontSize+5, 0, 2);

        //Ort,Datum
        if($i==0) $this->pdfRect(33, 121.0, 65, $pdf, 6);

        //Antragsteller
        if($i==0) $this->pdfRect(117.0, 121.0, 65, $pdf, 6);

        //1. zu versichernde volljährige Person
        if($i==0) $this->pdfRect(33, 130.0, 65, $pdf, 6);


            $pdf->Text(33.5, 125.8, 'X');
            $pdf->Text(119.5, 125.8, 'X');
            $pdf->Text(33.5, 135, 'X');

        $agePersonInsured = actionHelper::getAge($data['personInsured']['birthdate']);

        if($agePersonInsured >= 16 && $agePersonInsured < 18) {
            if($i==0) $this->pdfRect(117, 130.0, 65, $pdf, 6);
            $pdf->Text(119.5, 135, 'X');
        }


        // Empfangsbestätigung
        //Ort,Datum
        if($i==0) $this->pdfRect(33.5, 199.1, 65, $pdf, 5);

        //Antragsteller
        if($i==0) $this->pdfRect(117, 199.1, 65, $pdf, 5);

        //1. zu versichernde volljährige Person
        if($i==0) $this->pdfRect(33.5, 216.4, 65, $pdf, 5);


            $pdf->Text(33.5, 203.1, 'X');
            $pdf->Text(119.5, 203.1, 'X');
            $pdf->Text(33.5, 220.4, 'X');

        $agePersonInsured = actionHelper::getAge($data['personInsured']['birthdate']);

        #if($agePersonInsured >= 16 && $agePersonInsured < 18) {
            if($i==0) $this->pdfRect(117, 216.4, 65, $pdf, 5);
            $pdf->Text(119.5, 220.4, 'X');
        #}

        $pdf->SetFont($pdfCfg['fontFamily'], '', $fontSize+3, 0, 2);



    // page 5
        $tplidx = $pdf->importPage(5, '/MediaBox'); 
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        $pdf->Text(10.4, 48.6, 'x');
        $pdf->Text(40, 54, $data['signupDate'][0]);
        $pdf->Text(43, 54, $data['signupDate'][1]);
        $pdf->Text(48.0, 54, $data['signupDate'][3]);
        $pdf->Text(51, 54, $data['signupDate'][4]);
        $pdf->Text(63, 54, $data['signupDate'][8]);
        $pdf->Text(66, 54, $data['signupDate'][9]);

        if($data['gender']=='male') {
            $pdf->Text(31.6, 82.9, 'X');
        } else {
            $pdf->Text(31.6, 85.9, 'X');
        }   

        $pdf->Text(46, 87, $data['name']);
        $pdf->Text(46, 96, $data['street']);
        $pdf->Text(46, 105, $data['postcode']);
        $pdf->Text(70, 105, $data['city']);

        if($i==0) $this->pdfRect(34, 144, 90, $pdf, 5);
        if($i==0) $this->pdfRect(34, 153, 90, $pdf, 5);
        if($i==0) $this->pdfRect(34, 162, 40, $pdf, 5);
        
        if($i==0) $this->pdfRect(34, 189, 77, $pdf, 8);
        if($i==0) $this->pdfRect(119, 189, 77, $pdf, 8);

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $fontSize+4, 0, 2);
        $pdf->Text(34, 195, 'X');
        $pdf->Text(122, 195, 'X');


    // page 6
        $tplidx = $pdf->importPage(6, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
    // page 7 SEPA
        $tplidx = $pdf->importPage(7, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
    // page 8
        $tplidx = $pdf->importPage(8, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
    // page 9
        $tplidx = $pdf->importPage(9, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
    // page 10
        $tplidx = $pdf->importPage(10, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
    // page 11
        $tplidx = $pdf->importPage(11, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

    if($complete)
        $pdf->addPage('P');

} else {
	include('arag-dent.php.inc');
}
?>
