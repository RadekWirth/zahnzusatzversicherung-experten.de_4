<?
// File for generating Contract PDF for Wuerttembergische

$personInsuredOther = ($data['person']['pid'] !== $data['personInsured']['pid']) ? true : false;

// SEITE 1
        if ($i==0) {
            $pagecount = $pdf->setSourceFile($path.'/files/wuerttembergische/2022/Wuerttembergische.IGV.Zahn.Antrag.11-2021.pdf');
        } else {
            $pagecount = $pdf->setSourceFile($path.'/files/wuerttembergische/2022/Wuerttembergische.IGV.Zahn.Antrag.11-2021.pdf');
        }

        $tplidx = $pdf->importPage(2, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 3.4, 209.9);
         $pdf->SetMargins(0, 0, 0);

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);

        //gelbe markierungen nur auf rückantwort drucken!
        if($i==0) $this->pdfRect(20, 2, 70, $pdf);
        $pdf->Text(20.0, 6.0, $titles[$i]);

        $this->getCompanyStamp(139.0, 2.5, $pdf, $data['contractComplete']['sourcePage'], 6.5, 'horizontal', 'wide');

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        //Gesch.-St.Nr 2000-3539-9
        if($i==0) $pdf->Text(109, 61.8, '2'); // +17
        if($i==0) $pdf->Text(112.2, 61.8, '0');
        if($i==0) $pdf->Text(115.4, 61.8, '0');
        if($i==0) $pdf->Text(118.6, 61.8, '0');
        if($i==0) $pdf->Text(129.6, 61.8, '3');
        if($i==0) $pdf->Text(132.4, 61.8, '5');
        if($i==0) $pdf->Text(135.8, 61.8, '3');
        if($i==0) $pdf->Text(138.3, 61.8, '9');
        if($i==0) $pdf->Text(150, 61.8, '9');

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);

    // 1. Antragsteller
        ($data['gender']=='male') ? $pdf->Text(26.0, 77.1, 'X') : $pdf->Text(26.0, 81.1, 'X');
            $pdf->Text(70.6, 76.8, $data['namec']);
        $pdf->Text(70.6, 83.7, $data['street']);
        $pdf->Text(70.6, 91.3, $data['postcode'].'        '.$data['city']);
        $pdf->Text(70.0, 99.1, $data['birthdate']);

        ($data['nation']=='DEU') ? $pdf->Text(96, 99.1, 'deutsch') : $pdf->Text(96, 99.1, $data['nation']);
        $pdf->Text(130.0, 99.1, $data['email']);
	

        //Phone
        if($i==0) $this->pdfRect(70.4, 103.6, 32, $pdf, 3.5);

        if (!empty($data['insurance'])) {
            $pdf->Text(130, 106.5, $data['insurance']);
        } else 
            if($i==0) $this->pdfRect(129, 103.6, 30, $pdf, 4.0);
        

        // Zu versichernde Person
        if($personInsuredOther) {
            ($data['gender2']=='male') ? $pdf->Text(26.0, 129.5, 'X') : $pdf->Text(26.0, 133.5, 'X');
                $pdf->Text(72, 126.3, $data['name2b']);
            $pdf->Text(168, 126.3, $data['birthdate2']);

            if (!empty($data['insurance2'])) {
                $pdf->Text(72, 141.0, $data['insurance2']);
            } else 
                if($i==0) $this->pdfRect(70, 137.9, 38, $pdf, 4.0);

	      if($i==0) $this->pdfRect(69.8, 153.5, 2, $pdf, 2);
        } else {
           $pdf->Text(26.0, 87.4, 'X');
        }

        // Beratungsdoku
        $pdf->Text(69.5, 176.0, 'X');
        if ($i == 0) $this->pdfRect(70, 185.8, 54, $pdf, 4.7); // +75
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);
        $pdf->Text(70, 190.1, 'X');
        $pdf->SetFont($pdfCfg['fontFamily'], '', 10, 0, 2);

        // Stand der erhaltenen Unterlagen
        $pdf->Text(166.8, 261.4, 'Februar 2018');


        // Tarif?
        $pdfTemplate = isset($pdfTemplate) ? $pdfTemplate : $data['idt'];
        switch($pdfTemplate) {
            case project::gti('wuerttembergische-v1'):
                $pdf->Text(72.6, 232.0, "X");
                break;
            case project::gti('wuerttembergische-v2'):
                $pdf->Text(72.6, 236.0, "X");
                break;
            case project::gti('wuerttembergische-v3'):
                $pdf->Text(72.6, 240.0, "X");
                break;
            case project::gti('wuerttembergische-z1'):
                $pdf->Text(126.4, 232.0, "X");
                break;
            case project::gti('wuerttembergische-z2'):
                $pdf->Text(126.4, 236.0, "X");
                break;
            case project::gti('wuerttembergische-z3'):
                $pdf->Text(126.4, 240.0, "X");
                break;
            case project::gti('wuerttemberische-ze90-zbe'):
		   $pdf->Text(98.4, 232.0, "X");
                break;
            case project::gti('wuerttemberische-ze70-zbe'):
		   $pdf->Text(98.4, 236.0, "X");
                break;
            case project::gti('wuerttemberische-ze50-zbe'):
		   $pdf->Text(98.4, 240.0, "X");
                break;
            }


	  //$pdf->Text(153.6, 240.0, "X");

        // Ort / Datum
        if($i==0) $this->pdfRect(72, 252, 60, $pdf, 3.0);
        if($i==0) $this->pdfRect(72, 258.9, 60, $pdf, 3.0);
/*
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);
        if($i==0) $pdf->Text(106, 178.3, 'X');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        //Versicherungsbeginn
        $my = explode(".", $data['begin']);
        // monat
        $pdf->Text(175, 185.3, substr($my[0],0,1));
        $pdf->Text(180.2, 185.3, substr($my[0],1,1));
        // Jahr
        $pdf->Text(184, 185.3, substr($my[1], 0,1));
        $pdf->Text(188, 185.3, substr($my[1], 1,1));
        $pdf->Text(192, 185.3, substr($my[1], 2,1));
        $pdf->Text(195.6, 185.3, substr($my[1], 3,1));
        unset($my);

        switch($pdfTemplate) {
            case project::gti('wuerttembergische-v1'):
            case project::gti('wuerttembergische-v2'):
            case project::gti('wuerttembergische-v3'):
                ($personInsuredOther) ? $pdf->Text(71.5, 280.5, $data['price']) : $pdf->Text(71.5, 274.4, $data['price']);
            break;
            case project::gti('wuerttembergische-z1'):
            case project::gti('wuerttembergische-z2'):
            case project::gti('wuerttembergische-z3'):
                (!$personInsuredOther) ? $pdf->Text(122.5, 280.5, $data['price']) : $pdf->Text(122.5, 274.4, $data['price']);
            break;
            case project::gti('wuerttemberische-ze50-zbe'):
            case project::gti('wuerttemberische-ze70-zbe'):
            case project::gti('wuerttemberische-ze90-zbe'):
                (!$personInsuredOther) ? $pdf->Text(98.5, 280.5, $data['price']) : $pdf->Text(98.5, 274.4, $data['price']);
            break;
        }

        //Zahlweise
        if($i==0) $this->pdfRect(69, 284.1, 3.0, $pdf, 3.0);
        if($i==0) $this->pdfRect(116, 284.1, 3.0, $pdf, 3.0);
        if($i==0) $this->pdfRect(138.1, 284.1, 3.0, $pdf, 3.0);
        if($i==0) $this->pdfRect(158, 284.1, 3.0, $pdf, 3.0);
*/
        // Seitenzahl verdecken 
        $this->pdfClean(182, 286, 28, $pdf, 255, 255, 255, 6, 'F');

// SEITE 2
        $tplidx = $pdf->importPage(3, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        //Versicherungsbeginn
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 12, 0, 2);
	        $pdf->Text(154.6, 16.8, $data['begin']);
	 $this->pdfClean(90, 288, 28, $pdf, 255, 255, 255, 8, 'F');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        // Tarif?
        $pdfTemplate = isset($pdfTemplate) ? $pdfTemplate : $data['idt'];
        switch($pdfTemplate) {
            case project::gti('wuerttembergische-v1'):
                ($personInsuredOther) ? $pdf->Text(54.5, 47.6, "X") : $pdf->Text(50.2, 47.6, "X");
		   ($personInsuredOther) ? $pdf->Text(60.3, 143.0, $data['price']) : $pdf->Text(60.3, 135.5, $data['price']);
                break;
            case project::gti('wuerttembergische-v2'):
                ($personInsuredOther) ? $pdf->Text(54.5, 76.0, "X") : $pdf->Text(50.2, 76.0, "X");
		   ($personInsuredOther) ? $pdf->Text(60.3, 143.0, $data['price']) : $pdf->Text(60.3, 135.5, $data['price']);
                break;
            case project::gti('wuerttembergische-v3'):
                ($personInsuredOther) ? $pdf->Text(54.5, 105.0, "X") : $pdf->Text(50.2, 105.0, "X");
		   ($personInsuredOther) ? $pdf->Text(60.3, 143.0, $data['price']) : $pdf->Text(60.3, 135.5, $data['price']);
                break;
            case project::gti('wuerttembergische-z1'):
                ($personInsuredOther) ? $pdf->Text(114.2, 47.6, "X") : $pdf->Text(110.0, 47.6, "X");
		   ($personInsuredOther) ? $pdf->Text(120.3, 143.0, $data['price']) : $pdf->Text(120.3, 135.5, $data['price']);
                break;
            case project::gti('wuerttembergische-z2'):
                ($personInsuredOther) ? $pdf->Text(114.2, 76.0, "X") : $pdf->Text(110.0, 76.0, "X");
		   ($personInsuredOther) ? $pdf->Text(120.3, 143.0, $data['price']) : $pdf->Text(120.3, 135.5, $data['price']);
                break;
            case project::gti('wuerttembergische-z3'):
                ($personInsuredOther) ? $pdf->Text(114.2, 105.0, "X") : $pdf->Text(110.0, 105.0, "X");
		   ($personInsuredOther) ? $pdf->Text(120.3, 143.0, $data['price']) : $pdf->Text(120.3, 135.5, $data['price']);
                break;
            case project::gti('wuerttemberische-ze90-zbe'):
                ($personInsuredOther) ? $pdf->Text(84.5, 47.6, "X") : $pdf->Text(80.2, 47.6, "X");
		   ($personInsuredOther) ? $pdf->Text(90.3, 143.0, $data['price']) : $pdf->Text(90.3, 135.5, $data['price']);
                break;
            case project::gti('wuerttemberische-ze70-zbe'):
                ($personInsuredOther) ? $pdf->Text(84.5, 76.0, "X") : $pdf->Text(80.2, 76.0, "X");
		   ($personInsuredOther) ? $pdf->Text(90.3, 143.0, $data['price']) : $pdf->Text(90.3, 135.5, $data['price']);
                break;
            case project::gti('wuerttemberische-ze50-zbe'):
                ($personInsuredOther) ? $pdf->Text(84.5, 105.0, "X") : $pdf->Text(80.2, 105.0, "X");
		   ($personInsuredOther) ? $pdf->Text(90.3, 143.0, $data['price']) : $pdf->Text(90.3, 135.5, $data['price']);
                break;
            }

/*
        $pdf->Text(114.2, 47.6, "X");
        $pdf->Text(110.0, 47.6, "X");

        $pdf->Text(54.5, 76.0, "X");
        $pdf->Text(50.2, 76.0, "X");

        $pdf->Text(54.5, 105.0, "X");
        $pdf->Text(50.2, 105.0, "X");
	 ($personInsuredOther) ? $pdf->Text(60.3, 143.0, $data['price']) : $pdf->Text(60.3, 135.5, $data['price']);
	 ($personInsuredOther) ? $pdf->Text(120.3, 143.0, $data['price']) : $pdf->Text(120.3, 135.5, $data['price']);
*/

        if($i==0) $this->pdfRect(69.5, 149.5, 2, $pdf, 2);
        if($i==0) $this->pdfRect(113.1, 149.5, 2, $pdf, 2);       
        if($i==0) $this->pdfRect(137.4, 149.5, 2, $pdf, 2);
        if($i==0) $this->pdfRect(159.2, 149.5, 2, $pdf, 2);

        // Seitenzahl verdecken 
        $this->pdfClean(182, 285, 28, $pdf, 255, 255, 255, 6, 'F');

// SEITE 3
        $tplidx = $pdf->importPage(4, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        if($i==0) $this->pdfRect(45.9, 20.4, 2, $pdf, 2);
        if($i==0) $this->pdfRect(69.1, 20.4, 2, $pdf, 2);

        if($i==0) $this->pdfRect(45.9, 108.5, 2, $pdf, 2);
        if($i==0) $this->pdfRect(69.1, 108.5, 2, $pdf, 2);

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 10, 0, 2);
        $pdf->Text(13, 26, "O");
        $pdf->Text(13, 30, "D");
        $pdf->Text(13, 34, "E");
        $pdf->Text(13, 38, "R");
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);


        // Seitenzahl verdecken 
        $this->pdfClean(182, 285, 28, $pdf, 255, 255, 255, 6, 'F');

// SEITE 4
        $tplidx = $pdf->importPage(5, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);


        //Gesundheitsangaben



        if ( ! $personInsuredOther) {

	// Markierungen
        // Wurde eine anderweitig beantragte private Zahnzusatzversicherung abgelehnt?
        if ($i == 0) $this->pdfRect(29, 174.2, 90, $pdf, 3.7);
            if ($i == 0) $this->pdfRect(149.8, 162.4, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(159.8, 162.4, 2, $pdf, 2);

	// Fragen 1 und 2
            if ($i == 0) $this->pdfRect(149.6, 191.0, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(159.8, 191.0, 2, $pdf, 2);

            if ($i == 0) $this->pdfRect(149.6, 201.5, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(159.8, 201.5, 2, $pdf, 2);
	     if ($i == 0) $this->pdfRect(158.2, 207.6, 7.0, $pdf, 3.0);

            if ($i == 0) $this->pdfRect(149.6, 214.3, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(159.8, 214.3, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(158.2, 217.6, 7.0, $pdf, 3.0);

	// Fragen 3 und 4
            if ($i == 0) $this->pdfRect(149.6, 243.6, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(159.8, 243.6, 2, $pdf, 2);

            if ($i == 0) $this->pdfRect(149.6, 256.3, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(159.8, 256.3, 2, $pdf, 2);

            if ($i == 0) $this->pdfRect(149.6, 274.3, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(149.8, 277.9, 2, $pdf, 2);

        } 
        else 
        {
        // Wurde eine anderweitig beantragte private Zahnzusatzversicherung abgelehnt?
        if ($i == 0) $this->pdfRect(29, 174.2, 90, $pdf, 3.7);
            if ($i == 0) $this->pdfRect(176.4, 162.4, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(186.5, 162.4, 2, $pdf, 2);

	// Fragen 1 und 2
            if ($i == 0) $this->pdfRect(176.4, 191.0, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(186.5, 191.0, 2, $pdf, 2);

            if ($i == 0) $this->pdfRect(176.4, 201.5, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(186.5, 201.5, 2, $pdf, 2);
	     if ($i == 0) $this->pdfRect(185.2, 207.6, 7.0, $pdf, 3.0);

            if ($i == 0) $this->pdfRect(176.4, 214.3, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(186.5, 214.3, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(185.2, 217.6, 7.0, $pdf, 3.0);

	// Fragen 3 und 4
            if ($i == 0) $this->pdfRect(176.4, 243.6, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(186.5, 243.6, 2, $pdf, 2);

            if ($i == 0) $this->pdfRect(176.4, 256.3, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(186.5, 256.3, 2, $pdf, 2);

            if ($i == 0) $this->pdfRect(176.4, 274.3, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(176.4, 277.9, 2, $pdf, 2);

        }


/*
        if ( ! $personInsuredOther) {
                        if ($i == 0) $this->pdfRect(130.6, 13.4, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(140.6, 13.4, 2, $pdf, 2);
                        
                        if ($i == 0) $this->pdfRect(115.6, 32.4, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(131.6, 32.49, 2, $pdf, 2);

            // Summenstaffel 6 Jahre
            if ($data['series'] == 1 || $data['contractComplete']['addNotes'] == 'Verl&auml;ngerung Summenstaffel auf 6 Jahre!') {
                $pdf->Text(115.5, 34.3, 'x');
            }

            // Summenstaffel 8 Jahre
            if ($data['series'] == 2 || $data['contractComplete']['addNotes'] == 'Verl&auml;ngerung Summenstaffel auf 8 Jahre!') {
                $pdf->Text(132.4, 34.3, 'x');
            }
        }else
        {
             if ($i == 0) $this->pdfRect(178.8, 13.4, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(188.8, 13.4, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(163.6, 32.4, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(180.1, 32.49, 2, $pdf, 2);
            // Summenstaffel 6 Jahre
            if ($data['series'] == 1 || $data['contractComplete']['addNotes'] == 'Verl&auml;ngerung Summenstaffel auf 6 Jahre!') {
                $pdf->Text(163.5, 34.3, 'x');
            }

            // Summenstaffel 8 Jahre
            if ($data['series'] == 2 || $data['contractComplete']['addNotes'] == 'Verl&auml;ngerung Summenstaffel auf 8 Jahre!') {
                $pdf->Text(181, 34.3, 'x');
            }
        }


       if($i==0) $pdf->Text(70.2, 116.8, 'x');

        // Kontodaten

        if($i==0) $this->pdfRect(69.4, 163.2, 82, $pdf, 3.0);
        if($i==0) $this->pdfRect(156, 163.2, 37, $pdf, 3.0);
        if($i==0) $this->pdfRect(69.4, 173.0, 127, $pdf, 3.0);

        // Unterschriften
        if($i==0) $this->pdfRect(69.7, 255, 124, $pdf, 3.4);
        if($i==0) $this->pdfRect(69.7, 264.7, 62, $pdf, 6);
        if($i==0) $this->pdfRect(134.4, 264.7, 64, $pdf, 6);


        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);
        if($i==0) $pdf->Text(70.4, 269.2, 'X');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

*/
        // Seitenzahl verdecken 
        $this->pdfClean(182, 285, 28, $pdf, 255, 255, 255, 8, 'F');


// SEITE 5
        $tplidx = $pdf->importPage(6, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);


        // Frage 5 durchstreichen
        $pdf->SetDrawColor(0);
        $pdf->Line(25, 40, 195, 13);

        if($i==0) $pdf->Text(69.8, 109.8, 'x');


        // Kontodaten
        if($i==0) $this->pdfRect(69.4, 128.4, 127, $pdf, 3.0);
        if($i==0) $this->pdfRect(69.4, 135.6, 127, $pdf, 3.0);
        if($i==0) $this->pdfRect(69.4, 142.0, 127, $pdf, 3.0);

        // Kontodaten
        if($i==0) $this->pdfRect(69.4, 154.2, 82, $pdf, 3.0);
        if($i==0) $this->pdfRect(164, 154.2, 27, $pdf, 3.0);
        if($i==0) $this->pdfRect(69.4, 161.0, 127, $pdf, 3.0);

 	 if($i==0) $this->pdfRect(69.4, 169.6, 127, $pdf, 7.0);
	 #if($i==0) $this->pdfRect(69.4, 168.0, 127, $pdf, 7.0);

        // Unterschriften
        if($i==0) $this->pdfRect(69.7, 254, 124, $pdf, 3.4);
        if($i==0) $this->pdfRect(69.7, 263.7, 62, $pdf, 6);
        if($i==0) $this->pdfRect(134.4, 263.7, 64, $pdf, 6);


        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);
        if($i==0) $pdf->Text(70.4, 268.2, 'X');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);


        // Seitenzahl verdecken 
        $this->pdfClean(182, 285, 28, $pdf, 255, 255, 255, 8, 'F');

// SEITE 6
        $tplidx = $pdf->importPage(7, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        // Seitenzahl verdecken 
        $this->pdfClean(182, 285, 28, $pdf, 255, 255, 255, 8, 'F');

// SEITE 7
        $tplidx = $pdf->importPage(8, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        // Seitenzahl verdecken 
        $this->pdfClean(182, 285, 28, $pdf, 255, 255, 255, 8, 'F');

        // $pdf->AddPage('P');

// SEITE 8
        $tplidx = $pdf->importPage(9, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        // Seitenzahl verdecken 
        $this->pdfClean(182, 285, 28, $pdf, 255, 255, 255, 8, 'F');

        // $pdf->AddPage('P');
        // 

// SEITE 9
        $tplidx = $pdf->importPage(10, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);


// SEITE 10 -- Initiative - Gesund - Versichert ---


        $tplidx = $pdf->importPage(1, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 6, 0.1, 209.9);

        // Data left
        $pdf->Text(32, 92.0, $data['namec']);
        $pdf->Text(32, 105.2, $data['street']);
        $pdf->Text(32, 118.1, $data['postcode']);
        $pdf->Text(52, 118.1, $data['city']); 

        // Data right
        ($data['gender']=='male') ? $pdf->Text(112.4, 90.5, 'X') : $pdf->Text(129.3, 90.5, 'X');
        $pdf->Text(113, 105.2, $data['birthdate']);
        $pdf->Text(113, 118.1, $data['email']);

        //Fördermittglied
        $pdf->Text(32, 156.7, 'Versicherungsmakler Experten GmbH');

        //Mitgliedsnummer
        $pdf->Text(154, 156.7, 'K 1331');

        //Unterschrift
        if($i==0) $this->pdfRect(33, 258.7, 32, $pdf, 7);

        if($i==0) $this->pdfRect(92, 258.7, 85.0, $pdf, 7);
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);
        if($i==0) $pdf->Text(94, 265.2, 'X');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        if($complete==2) {
            $pdf->AddPage('P');
        }

/*
        $tplidx = $pdf->importPage(11, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
*/
?>