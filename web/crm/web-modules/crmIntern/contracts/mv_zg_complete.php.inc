<?php
// File for generating Contract PDF for Münchner Verein ZahnGesund

        /*
         * Constants
         */
        // SQUARE XY
        define('X_Y', 1.5);

        // RECT Y
        define('Y', 2.7);

        $data['person']['age'] = actionHelper::getAge($data['contractComplete']['birthdate']);

        if ($i!=1) {
            $pagecount = $pdf->setSourceFile($path.'files/mv/antrag/2021/ZahnGesund_PDF Antrag.pdf');
        } else {
            $pagecount = $pdf->setSourceFile($path.'files/mv/antrag/2021/ZahnGesund_PDF Antrag.pdf');
        }

        $tplidx = $pdf->importPage(1, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
        $pdf->SetMargins(0, 0, 0);

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);

        if($i==0) $this->pdfRect(75.0, 3.0, 70, $pdf);
        $pdf->Text(75.0, 7.0, $titles[$i]);

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-1, 0, 2);

        // -> Seite 2
        $pdf->Text(90, 14.0, "Versicherungsmakler Experten GmbH");

        
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        //VermittlerNummer
        $pdf->Text(18, 30.5, '85/747/20370');


        if($data['gender']=='male') {
            $pdf->Text(18.0, 57.5, 'Herr'); 
        } else {
            $pdf->Text(18.0, 57.5, 'Frau');
        }
        $pdf->Text(178.8, 57.5, $data['birthdate']);

        $pdf->Text(18.0, 70.0, $data['person']['forename']);
        $pdf->Text(113, 70.0, $data['person']['surname']);
        $pdf->Text(18.0, 84.0, $data['street']);
        $pdf->Text(18.0, 97.7, $data['postcode'].'           '.$data['city']);


        //Telefonnummer
        if(!empty($data['email']) || $data['email']=='')
        {
            if($i==0) $this->pdfRect(16.8, 107.7, 39, $pdf, 4);
        } else
            $pdf->Text(18.0, 111.0, $data['email']);

        //eMail
        if(!empty($data['phone']) || $data['phone']=='')
        {
            if($i==0) $this->pdfRect(107, 107.7, 53, $pdf, 4);
        } else 
            $pdf->Text(109.0, 111.0, $data['phone']);


        #$pdf->Text(162, 80.4, $data['mobile']);

        $pdf->Text(18, 124.0, $data['begin']);


                $pdf->Text(18.0, 171.8, $data['personInsured']['forename']);
                $pdf->Text(110.0, 171.8, $data['personInsured']['surname']);

                $pdf->Text(60.0, 160.9, $data['birthdate2']);

                if($data['gender2']=='male')    {
                    $pdf->Text(18.0, 160.9, 'Herr'); // -23
                } else {
                    $pdf->Text(18.0, 160.9, 'Frau');
                }

        $pdf->Text(18.0, 182.7, project::gtfn($data['idt']));
	 $pdf->Text(110.0, 182.7, $data['bonusbase']);

	 if(empty($data['personInsured']['insurance']))
	 {
		$this->pdfRect(16, 190.2, 40.0, $pdf, 4);
	 } else
		$pdf->Text(18.0, 193.7, $data['personInsured']['insurance']);

	$this->pdfRect(16, 199.0, 60.0, $pdf, 4);

                $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);


// SEITE 2
        $tplidx = $pdf->importPage(2, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);



	//3
	if($i==0) $this->pdfRect(13.5, 112.4, 18.0, $pdf, 5);
	if($i==0) $this->pdfRect(62.5, 112.4, 18.0, $pdf, 5);
	#if($data['tooth1'] && $data['tooth1'] > 0)
	#{
	#	$pdf->Text(20.4, 115.9, 'Ja');
	#	$pdf->Text(64.4, 115.9, $data['tooth1'].'0');
	#} else {
	#	$pdf->Text(20.4, 115.9, 'Nein');
	#}

$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+16, 0, 2);
	if($i==0) $this->pdfRect(5.5, 110.3, 4.0, $pdf, 10);
	$pdf->Text(6.4, 117.9, '!');
$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

	//
	if($i==0) $this->pdfRect(13.5, 186.0, 40.0, $pdf, 5);
	if($i==0) $this->pdfRect(13.5, 199.0, 90.0, $pdf, 5);
	if($i==0) $this->pdfRect(13.5, 211.3, 44.0, $pdf, 5);

	if($i==0) $this->pdfRect(13.5, 224.5, 16.0, $pdf, 5);
	if($i==0) $this->pdfRect(38.5, 224.5, 80.0, $pdf, 5);

        

// SEITE 3
        $tplidx = $pdf->importPage(3, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

	 $pdf->Text(11.1, 46.0, 'X');
	 #$pdf->Text(11.1, 59.0, 'X');

	 if($i==0) $this->pdfRect(14.5, 118.7, 40.0, $pdf, 5);
	 if($i==0) $this->pdfRect(170.5, 118.7, 20.0, $pdf, 5);

	 if($i==0) $this->pdfRect(14.5, 130.0, 90.0, $pdf, 5);
	 if($i==0) $this->pdfRect(14.5, 143.0, 90.0, $pdf, 5);
	 if($i==0) $this->pdfRect(14.5, 155.7, 90.0, $pdf, 5);


// SEITE 4
        $tplidx = $pdf->importPage(4, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);


// SEITE 5
        $tplidx = $pdf->importPage(5, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);


// SEITE 6
        $tplidx = $pdf->importPage(6, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);


// SEITE 7
        $tplidx = $pdf->importPage(7, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

if($complete == 2)
	 $pdf->addPage('P');

?>