<?php
// File for generating Contract for DKV



    $bonus['nextBonusBase'] = ( ! empty($data['personInsured']['bonus']['nextBonusBase'])) ? $data['personInsured']['bonus']['nextBonusBase'] : null; 
    $bonus['nextBonusDate'] = ( ! empty($data['personInsured']['bonus']['nextBonusDate'])) ? $data['personInsured']['bonus']['nextBonusDate'] : null; 


if($complete) {
    // Pr�fung, ob Datei vorhanden ist...
    if ($i != 1) {
        $pagecount = $pdf->setSourceFile($path.'/files/dkv/antrag/2020/antrag.pdf');
    } else {
        // black & white
        $pagecount = $pdf->setSourceFile($path.'/files/dkv/antrag/2020/antrag.pdf');
    }

    $tplidx = $pdf->importPage(1, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 4, 9, 204);

    $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);

    //gelbe markierungen nur auf rueckantwort drucken!
    if($i==0) $this->pdfRect(75, 4, 68, $pdf);

    $pdf->Text(75.0, 8.0, $titles[$i]);

    // reset font
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-4, 0, 2);

    $this->getCompanyStamp(64, 16, $pdf, $data['contractComplete']['sourcePage'], 6.5, 'horizontal', 'wide');

    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);
    // Kreuze Antrag auf KV
    $pdf->Text(20.2, 35.1, 'X');
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
    
    // Kunde
    $pdf->Text(166.4, 60.8, 'X');
    
    // Vermittler-Nr
    $pdf->Text(21.0, 94.5, '8    9    8    8   7    2   1');

    // A. Angaben zum Antragsteller
    $pdf->Text(22, 148, $data['nameb']);
    $pdf->Text(22, 158, $data['street']);
    $postcode = str_split($data['postcode']);
    foreach ($postcode as $k => $letter) {
        $pdf->Text(21 + $k * 5.04, 166.8, $letter);
    }
    $pdf->Text(46.5, 166.8, $data['city']);
    if ( ! empty($data['nation'])) {
        $nation = str_split($data['nation']);
        foreach ($nation as $k => $letter) {
            $pdf->Text(177.1 + $k * 5, 149.3, $letter);
        }
    } else {
        if ($i == 0) $this->pdfRect(176, 146.8, 14, $pdf, 4);
    }

    $birthdate = str_split($data['birthdate']);
    $c = 0;
    foreach ($birthdate as $letter) {
        if ($letter !== '.') {
            $pdf->Text(137 + $c++ * 5.04, 149.3, $letter);
        }
    }
    $pdf->SetFont($pdfCfg['fontFamily'], '', 7, 0, 2);
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

    if ($data['gender'] === 'male')
    {
        $pdf->Text(112.4, 147.2, 'X');
    } else {
        $pdf->Text(124.6, 147.2, 'X');
    }

    if ($data['phone'])
    {
        $pdf->Text(138.6, 158, $data['phone']);
    } else {
        if ($i == 0) $this->pdfRect(138, 154.9, 50, $pdf, 4);
    }

    if ($data['email'])
    {
        $pdf->Text(138.4, 166.8, $data['email']);
    } else {
        if ($i == 0) $this->pdfRect(137.8, 164.1, 50, $pdf, 4);
    }

    if ($data['job']) {
        $pdf->Text(22, 175.6, $data['job']);
    } else {
        if ($i == 0) $this->pdfRect(22, 172.6, 48, $pdf, 4);
    }

    // Seit
    if ($i == 0) $this->pdfRect(151.4, 172.4, 39.2, $pdf, 4);

    // Angaben zum Beruf
    for ($c = 0; $c < 4; $c++) {
        if ($c === 2) {
            if ($i == 0) $this->pdfRect(20.3 + $c * 27.8, 188.4, 2, $pdf, 2);
        } else {
            if ($i == 0) $this->pdfRect(20.3 + $c * 29.0, 188.4, 2, $pdf, 2);
        }
    }




/**
 * Page 2
 */
    $tplidx = $pdf->importPage(2, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 4, 0, 204);

    // Same same
    if ($data['personInsured']['forename'] . ' ' . $data['personInsured']['surname'] === $data['namec']) 
    {
        $pdf->Text(71.8, 27.1, 'X');
    } else {
        for ($c=0; $c<3; $c++) {
            if ($c === 2) {
                if ($i == 0) $this->pdfRect(19.8 + $c * 26, 32.8, 2, $pdf, 2);
            } else {
                if ($i == 0) $this->pdfRect(19.8 + $c * 24, 32.8, 2, $pdf, 2);
            }
        }
        // Versicherte Person
        $pdf->Text(22, 48, $data['personInsured']['forename'] . ' ' . $data['personInsured']['surname']);
        $birthdate = str_split(date('d.m.Y', strtotime($data['personInsured']['birthdate'])));
        $c = 0;
        foreach ($birthdate as $letter) {
            if ($letter !== '.') {
                $pdf->Text(22 + $c++ * 5.04, 57, $letter);
            }
        }

        if ($data['personInsured']['salutationLid'] == 9)
        {
            $pdf->Text(154.1, 46.2, 'X');
        } else {
            $pdf->Text(166.6, 46.2, 'X');
        }

        if ($data['personInsured']['job']) {
            $pdf->Text(22, 65.8, $data['personInsured']['job']);
        } else {
            if ($i == 0) $this->pdfRect(20, 63.2, 55, $pdf, 4);
        }
        // Angaben zum Beruf
        for ($c = 0; $c < 4; $c++) {
            if ($c === 2) {
                if ($i == 0) $this->pdfRect(88.5 + $c * 24.1, 61.5, 2, $pdf, 2);
            } else {
                if ($i == 0) $this->pdfRect(88.5 + $c * 25.3, 61.5, 2, $pdf, 2);
            }
        }
        #if( ! empty($data['personInsured']['insurance'])) {
            $pdf->Text(20.6, 74.8, 'X');
            $pdf->Text(45.5, 76.3, $data['personInsured']['insurance']);
        #} else {
            if ($i == 0) $this->pdfRect(19.7, 73.6, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(43, 73.5, 60, $pdf, 4);
        #}

    }

        // Versicherung
        $pdf->Text(20.6, 74.8, 'X');
        if ( ! empty($data['insurance'])) {
            $pdf->Text(45.5, 76.3, $data['insurance']);
        } else {
            if ($i == 0) $this->pdfRect(43, 73.5, 60, $pdf, 4);
        }

        // Versicherungsbeginn
        $begin = str_split(date('d.m.Y', strtotime($data['wishedBegin'])));
        $c = 0;
        foreach ($begin as $letter) {
            if ($letter !== '.') {
                $pdf->Text(56.5 + $c++ * 5.07, 86.0, $letter);
            }
        }

            // Tarif
        switch($pdfTemplate)
        {
            case project::gti('dkv-kdt85'):
                $pdf->Text(66.5, 97.9, 'X');
                break;
            case project::gti('dkv-kdt85-kdbe'):
                $pdf->Text(66.5, 97.9, 'X');
                $pdf->Text(162.3, 97.9, 'X');
                break;
            case project::gti('dkv-kdt50'):
                $pdf->Text(109.5, 97.9, 'X');
                break;
            case project::gti('dkv-kdt50-kdbe'):
                $pdf->Text(162.3, 97.9, 'X');
                $pdf->Text(109.5, 97.9, 'X');
                break;
            case project::gti('dkv-kdt70'):
                $pdf->Text(87.4, 97.9, 'X');
                break;
            case project::gti('dkv-kdt'):
                $pdf->Text(107.4, 97.9, 'X');
                break;
            case project::gti('dkv-kdbe'):
                $pdf->Text(162.3, 97.9, 'X');
                break;
            case project::gti('dkv-kdtp100'):
                $pdf->Text(43.7, 97.9, 'X');
                break;
            case project::gti('dkv-kdtp100-kdbe'):
                $pdf->Text(43.7, 97.9, 'X');
                $pdf->Text(162.3, 97.9, 'X');
                break;
        }

        // Tagegeldhoehe
        $price = str_split($data['contractComplete']['price']);
        $c = 0;
        #foreach ($price as $letter) {
        #    if ($letter !== '.') {
        #        $pdf->Text(135.6 + $c++ * 5, 112.4, $letter);
        #    } 
        #}
	 $this->pdfClean(114.0, 116, 18.9, $pdf);
	 $this->pdfClean(171.0, 116, 20.2, $pdf);

        $c = 0;
	 $formatted = (sprintf("%01.2f", $data['contractComplete']['price']));

	 $pdf->Text(174.3, 120.4, '� '.str_replace('.', ',', $formatted));

/**
 * Page 3
 */
    $tplidx = $pdf->importPage(3, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 4, 0, 200);

    if ($pdfTemplate !== project::gti('dkv-kdt')) {

        if (
            $pdfTemplate === project::gti('dkv-kdbe') ||
            $pdfTemplate === project::gti('dkv-kdtp100-kdbe') ||
            $pdfTemplate === project::gti('dkv-kdt50-kdbe') ||
            $pdfTemplate === project::gti('dkv-kdt85-kdbe')
        ) {
            if ($i == 0) $this->pdfRect(172.2, 108.2, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(182.6, 108.2, 2, $pdf, 2);

            if (actionHelper::getAge($data['personInsured']['birthdate']) > 18) {
                if ($i == 0) $this->pdfRect(172.2, 125.4, 2, $pdf, 2);
                if ($i == 0) $this->pdfRect(182.6, 125.4, 2, $pdf, 2);
                    $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+16, 0, 2);
                    $this->pdfRect(5, 124, 9, $pdf, 16);
                    $pdf->Text(8.0, 136.4, '!');
                    $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
            }
            $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+16, 0, 2);
            $this->pdfRect(5, 107, 9, $pdf, 16);
            $pdf->Text(8.0, 119.4, '!');
            $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
        }

        if (
            $pdfTemplate === project::gti('dkv-kdt85') ||
            $pdfTemplate === project::gti('dkv-kdt85-kdbe') ||
            $pdfTemplate === project::gti('dkv-kdt50') ||
            $pdfTemplate === project::gti('dkv-kdt50-kdbe') ||
            $pdfTemplate === project::gti('dkv-kdt70') ||
            $pdfTemplate === project::gti('dkv-kdt') ||
            $pdfTemplate === project::gti('dkv-kdtp100') ||
            $pdfTemplate === project::gti('dkv-kdtp100-kdbe')
        ) {
            if ($i == 0) $this->pdfRect(172.2, 142.4, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(182.6, 142.4, 2, $pdf, 2);

            for ($c=0;$c<5;$c++) {
                if ($c === 4) {
                    if ($i == 0) $this->pdfRect(48 + $c * 16.2, 207.3, 2, $pdf, 2);
                } else {
                    if ($i == 0) $this->pdfRect(48.4 + $c * 14.95, 207.3, 2, $pdf, 2);
                }
            }

            $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+16, 0, 2);
            $this->pdfRect(5, 142, 9, $pdf, 16);
            $pdf->Text(8.0, 154.4, '!');
            $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);

            $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+16, 0, 2);
            $this->pdfRect(5, 207, 9, $pdf, 16);
            $pdf->Text(8.0, 219.4, '!');
            $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
        }

    }


/**
 * Page 4 - SEPA
 */
    $tplidx = $pdf->importPage(4, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 4, 0, 204);


    // Kreuz bei Kontoinhaber
    $pdf->Text(20.9, 89.1, 'x');
    // Markierungen
    if ($i == 0) $this->pdfRect(20.4, 101.2, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(42.3, 101.2, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(69.0, 101.2, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(93.6, 101.2, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(113.1, 101.2, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(124.3, 101.2, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(135.5, 101.2, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(148.0, 101.2, 2, $pdf, 2);

    if ($i == 0) $this->pdfRect(39.9, 133.1, 94, $pdf, 4);

 

/**
 * Page 5
 */
    $tplidx = $pdf->importPage(5, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 4, 0, 204);


    // Unterschriften
    if ($i == 0) $this->pdfRect(19.6, 169.5, 55, $pdf, 4);
    if ($i == 0) $this->pdfRect(79, 169.5, 110, $pdf, 4);
    if ($i == 0) $this->pdfRect(79, 192.2, 55, $pdf, 4);
    if ($i == 0) $this->pdfRect(79, 250.8, 110, $pdf, 4);
    if ($i == 0) $this->pdfRect(19.6, 250.8, 54, $pdf, 4);

    $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+3, 0, 2);
    $pdf->Text(20, 254, 'x');
    $pdf->Text(20, 172.8, 'x');
    $pdf->Text(80, 172.8, 'x');
    $pdf->Text(80, 195.5, 'x');
    $pdf->Text(80, 254, 'x');
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

    // TeleEml
    #if ($i == 0) $this->pdfRect(36.6, 229.3, 2, $pdf, 2);
    #if ($i == 0) $this->pdfRect(48.8, 229.3, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(66.8, 31.8, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(108.2, 31.8, 2, $pdf, 2);

/**
 * Page 6
 */
    $tplidx = $pdf->importPage(6, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 4, 0, 204);

/**
 * Page 7
 */
    $tplidx = $pdf->importPage(7, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 4, 0, 204);

/**
 * Page 8
 */
    $tplidx = $pdf->importPage(8, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 4, 0, 204);


/**
 * Page 9   
 */
    $tplidx = $pdf->importPage(9, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 4, 0, 200);

/**
 * Page 10
 */
    $tplidx = $pdf->importPage(10, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 4, 0, 204);

} else
	include('dkv.php.inc');