<?php

// File for generating Contract PDF for Bayrische V.I.P. dental Plus

define('CROWNS', $data['contractComplete']['tooth4'] ? $data['contractComplete']['tooth4'] : null);
define('MISSING_TOOTH', $data['t1'] ? $data['t1']  : null);
define('AGE_PERSONINSURED', actionHelper::getAge($data['personInsured']['birthdate']));
define('HAS_DENTURES', $data['contractComplete']['existDentures'] === "yes" ? true : false);
define('HAS_PERIODONTITIS', $data['contractComplete']['periodontitis'] === "no" ? false : true);
define('IS_IN_TREATMENT', $data['incare'] != '' ? true : false);


if($i==0) {
    $pagecount = $pdf->setSourceFile($path.'/files/bayerische-dental-plus/die-bayerische-dental-plus-2014.pdf');
} else {
    $pagecount = $pdf->setSourceFile($path.'/files/bayerische-dental-plus/die-bayerische-dental-plus-2014_bw.pdf');
}

/**
 * Page 1
 */


    $tplidx = $pdf->importPage(1, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

    // gelbe markierungen nur auf R�ckantwort drucken!
    $pdf->SetFont($pdfCfg['fontFamily'], 'B', 11, 0, 2);
    if($i==0) $this->pdfRect(72, 0, 68, $pdf);
    $pdf->Text(72, 4, $titles[$i]);

    // Hide "Bitte eine Telefonnummer für den Welcome-Call der BKK Mobil Oil angeben"
    if ($i == 0) $this->pdfClean(140, 55, 60, $pdf, 255, 255, 255, 12, 'F');

    #$this->getCompanyLogo(151.0, 26, $pdf, $data['contractComplete']['sourcePage'], $i, 30);
    $this->getCompanyStamp(151.0, 33, $pdf, $data['contractComplete']['sourcePage'], 8);

    $pdf->SetFont($pdfCfg['fontFamily'], '', 9, 0, 2);

    // Hide "(mind. 3 Jahre)" bei "Bestehender Versicherungsschutz...."
    if ($i == 0) $this->pdfClean(97, 178, 19, $pdf, 255, 255, 255, 5, 'F');

	// 1.AV Nummer
    $pdf->Text(23.9, 13, '1   6   3   6  0   2');

    $pdf->SetFont($pdfCfg['fontFamily'], '', 10, 0, 2);

    $pdf->Text(7, 40.3, "x");

    $pdf->SetFont($pdfCfg['fontFamily'], '', 12, 0, 2);

    $bg = explode(".", $data['begin']);
    $pdf->Text(122, 39.6, $bg[0][0]);
    $pdf->Text(127.5, 39.6, $bg[0][1]);
    $pdf->Text(131.6, 39.6, $bg[1][0]);
    $pdf->Text(135.9, 39.6, $bg[1][1]);
    $pdf->Text(140.1, 39.6, $bg[1][2]);
    $pdf->Text(144.7, 39.6, $bg[1][3]);

    $pdf->SetFont($pdfCfg['fontFamily'], '', 10, 0, 2);

    // Antragsteller / Versicherungsnehmer
    ($data['gender']=='male') ? $pdf->Text(9.3, 66.3, 'x') : $pdf->Text(22, 66.3, 'x');

    $pdf->SetFont($pdfCfg['fontFamily'], '', 12, 0, 2);

    $bd = explode(".", $data['birthdate']);
    $pdf->Text(64.8, 66.5, $bd[0][1]);
    $pdf->Text(61, 66.5, $bd[0][0]);
    $pdf->Text(69.4, 66.5, $bd[1][0]);
    $pdf->Text(73.4, 66.5, $bd[1][1]);
    $pdf->Text(78.5, 66.5, $bd[2][0]);
    $pdf->Text(83, 66.5, $bd[2][1]);
    $pdf->Text(86.8, 66.5, $bd[2][2]);
    $pdf->Text(90.9, 66.5, $bd[2][3]);

    $pdf->SetFont($pdfCfg['fontFamily'], '', 11, 0, 2);

    $pdf->Text(11, 74.5, $data['namec']);
    $pdf->Text(11, 83.2, $data['street']);
    $pdf->Text(11, 91.7, $data['postcode']);
    $pdf->Text(33, 91.7, $data['city']);

    if( ! empty($data['phone'])) {
        $pdf->Text(146.1, 74.4, $data['phone']);
    } else {
        if($i==0) $this->pdfRect(147.1, 71.4, 50, $pdf, 3);
    }

    if($i==0) $this->pdfRect(147.1, 80.2, 50, $pdf, 3);


    if(!empty($data['email'])) {
        if(strlen($data['email']) > 25) {
            $pdf->SetFont($pdfCfg['fontFamily'], '', 10, 0, 2);
        }
        $pdf->Text(146.1, 91.9, $data['email']);
    } else {
        if($i==0) $this->pdfRect(146.9, 89, 50, $pdf, 3);
    }

    // Zu versichernde Person
    $pdf->Text(62, 104.9, $data['personInsured']['surname']);
    $pdf->Text(62, 112.3, $data['personInsured']['forename']);

    $pdf->Text(127.3, 104.7, $data['birthdate2']);
    ($data['gender2']=='male') ? $pdf->Text(149.7, 111.8, 'x') : $pdf->Text(169, 111.8, 'x');

    // SEPA-Lastschriftmandat
    if($i==0) $this->pdfRect(10.8, 139.4, 60, $pdf, 3);
    if($i==0) $this->pdfRect(74.7, 139.4, 35, $pdf, 3);
    if($i==0) $this->pdfRect(16.4, 147.4, 66.5, $pdf, 3);
    if($i==0) $this->pdfRect(10.8, 155.2, 108, $pdf, 3);
    if($i==0) $this->pdfRect(119.2, 138.3, 3, $pdf, 3);
    if($i==0) $this->pdfRect(130.9, 142, 68, $pdf, 3);
    if($i==0) $pdf->Text(13, 158.5, 'X');

    // Versicherungsschutz
    $pdf->Text(174, 171, $data['price']);
    if ($i == 0) $this->pdfRect(50, 182, 150, $pdf, 3);
    if ($i == 0) $this->pdfRect(50, 188, 54, $pdf, 3);
    if ($i == 0) $this->pdfRect(116, 188, 30, $pdf, 3);
    if ($i == 0) $this->pdfRect(169.8, 188, 30, $pdf, 3);

    // Gesundheitsfragen
    if ($i == 0) $this->pdfRect(100.6, 202, 3.5, $pdf, 3.5);
    if ($i == 0) $this->pdfRect(108.6, 202, 3.5, $pdf, 3.5);    
    if ($i == 0) $this->pdfRect(100.6, 220.4, 3.5, $pdf, 3.5);
    if ($i == 0) $this->pdfRect(108.6, 220.4, 3.5, $pdf, 3.5);    
    if ($i == 0) $this->pdfRect(100.6, 226.6, 3.5, $pdf, 3.5);
    if ($i == 0) $this->pdfRect(108.6, 226.6, 3.5, $pdf, 3.5);    
    if ($i == 0) $this->pdfRect(100.6, 235.6, 3.5, $pdf, 3.5);
    if ($i == 0) $this->pdfRect(108.6, 235.6, 3.5, $pdf, 3.5);    
    if ($i == 0) $this->pdfRect(100.6, 249.5, 3.5, $pdf, 3.5);
    if ($i == 0) $this->pdfRect(108.6, 249.5, 3.5, $pdf, 3.5);    
    if ($i == 0) $this->pdfRect(100.6, 266.1, 3.5, $pdf, 3.5);
    if ($i == 0) $this->pdfRect(108.6, 266.1, 3.5, $pdf, 3.5);

/**
 * Page 2
 */
    $tplidx = $pdf->importPage(2, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

    if ($i==0) {
	    if ($i == 0) $this->pdfRect(11.6, 227, 2, $pdf, 2);
	    if ($i == 0) $this->pdfRect(107.8, 51, 2, $pdf, 2);
	    if ($i == 0) $this->pdfRect(107.8, 156, 2, $pdf, 2);
	    if ($i == 0) $this->pdfRect(107.8, 176, 2, $pdf, 2);
	    $pdf->Text(50, 280, "oder");
	    $pdf->Text(150, 171, "oder");
    }


/**
 * Page 3
 */
    $tplidx = $pdf->importPage(3, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

/**
 * Page 4
 */
    $tplidx = $pdf->importPage(4, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

    $pdf->SetFont($pdfCfg['fontFamily'], 'B', 11, 0, 2);

    if ($i == 0) $this->pdfRect(9, 78, 28, $pdf, 5);
    if ($i == 0) $pdf->Text(11, 82, 'X');
    if ($i == 0) $this->pdfRect(43, 78, 47, $pdf, 5);
    if ($i == 0) $pdf->Text(45, 82, 'X');

    if ($data['person']['forename'] !== $data['personInsured']['forename'] 
	   || $data['person']['surname'] !== $data['personInsured']['surname']
	   || $data['person']['birthdate'] !== $data['personInsured']['birthdate']) {
    	if ($i == 0) $this->pdfRect(97, 78, 48, $pdf, 5);
	    if ($i == 0) $pdf->Text(99, 82, 'X');
    }



    if ($i==0) $this->pdfRect(9.4, 136.4, 80, $pdf, 5.2);
    $pdf->SetFont($pdfCfg['fontFamily'], 'B', 11, 0, 2);
    if ($i == 0) $pdf->Text(11.8, 140.5, 'X');


	if ($i == 0) $this->pdfRect(97, 136.4, 102, $pdf, 5.2);
    if ($i == 0) $pdf->Text(100, 140.5, 'X');
    

    $pdf->SetFont($pdfCfg['fontFamily'], '', 11, 0, 2);
    $pdf->Text(9.8, 104.6, 'x');
    $pdf->Text(9.8, 118.1, 'x');
    $pdf->Text(9.8, 125.5, 'x');
?>