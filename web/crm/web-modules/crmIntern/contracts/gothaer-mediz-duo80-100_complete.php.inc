<?php
// File for generating Contract PDF 

        /*
         * Constants
         */
        // SQUARE XY
        define('X_Y', 2.0);

        // RECT Y
        define('Y', 3.5);

        $data['person']['age'] = actionHelper::getAge($data['person']['birthdate']);
        $data['personInsured']['age'] = actionHelper::getAge($data['personInsured']['birthdate']);


        $pagecount = $pdf->setSourceFile($path.'files/gothaer/Antrag/2023/Gothaer.Antrag.MediZ Duo 80_90_100.2206-2023.pdf');
	

        $tplidx = $pdf->importPage(1, '/MediaBox');

        $pdf->addPage('P');  // idx, x, y, w, h
        $pdf->useTemplate($tplidx, 0, 6, 210);
        $pdf->SetMargins(0, 0, 0);


        

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);

        if($i==0) $this->pdfRect(80.0, 3.0, 70, $pdf);
            $pdf->Text(83.0, 7.0, $titles[$i]);

	 if($i==0) $this->pdfClean(134.0, 10.6, 16.0 , $pdf, 255,255,255);
	 if($i==0) $this->pdfClean(140.0, 9.5, 30.0, $pdf, 255, 255, 255, 8);

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-1, 0, 2);


        #$pdf->Text(75.0, 28.0, "Versicherungsmakler Experten GmbH");
        #$this->getCompanyStamp(75.0, 24, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']-1, 'horizontal', 'wide');
        
	 
	 if($i==0) $this->pdfClean(0.0, 21.5, 60.0, $pdf, 255, 255, 255, 10);
	 if($i==0) $this->pdfClean(126.0, 19, 43.0, $pdf, 255, 255, 255, 8);
	 if($i==0) $this->pdfClean(0.0, 34, 24.0, $pdf, 255, 255, 255, 8);

	 if($i==0) $this->pdfClean(134.0, 36.7, 8.0, $pdf, 255, 255, 255, 5);

 	 $pdf->Text(136.0, 40.7, '3 6 4 8 4');


        if($data['gender']=='male') {
            $pdf->Text(181.0, 48.9, 'X');
        } else {
            $pdf->Text(181.0, 52.6, 'X');
        }

        $pdf->Text(29.0, 47.9, $data['name']); // +6.7

        $pdf->Text(29.0, 55.2, $data['street']);
        $pdf->Text(29.0, 62.9, $data['postcode'].'                        '.$data['city']);
            
        $pdf->Text(29.0, 74.9, $data['birthdate']);

        #if(empty($data['nation'])) {
        #    $pdf->Text(73, 68.2, 'deutsch');
        #} else {
        #    $pdf->Text(73, 68.2, $data['nation']);
        #}

     if(empty($data['email'])) {
        $this->pdfRect(120.0, 58.7, 40, $pdf);
     } else {
        $pdf->Text(122.0, 62.9, $data['email']);
     }

     // if(empty($data['job'])) {
     //    $this->pdfRect(95.0, 64, 60, $pdf);
     // } else {
     //    $pdf->Text(97.0, 68.2, $data['job']);
     // }

        // VP
     if($data['name'] == $data['name2b'])
     {
        // VP = VN
        $pdf->Text(181.0, 64.2, 'X');
     } else {

         $pdf->Text(29.0, 88.5, $data['name2b']);
         $pdf->Text(145.0, 88.5, $data['birthdate2']);
    
     if($data['gender2']=='male')    {
         $pdf->Text(192.4, 88.5, 'X');
         } else {
            $pdf->Text(192.4, 92.0, 'X');
         }
     }



        if($i==0) {
            $this->pdfRect(25.6, 136.7, X_Y, $pdf, X_Y);
            $this->pdfRect(63.7, 136.7, X_Y, $pdf, X_Y);
            $this->pdfRect(85.6, 136.7, X_Y, $pdf, X_Y);
            $this->pdfRect(107.6, 136.7, X_Y, $pdf, X_Y);
        }

     $pdf->Text(107.5, 143.1, 'X');



     if(empty($data['insurance'])) {
        #$this->pdfRect(54.0, 280.5, 26, $pdf);
	 $pdf->Text(26.0, 287, 'Für die zu versichernde Person besteht eine gesetzliche Krankenversicherung bei  _____________________');
        $this->pdfRect(147.0, 283.5, 26, $pdf);
     } else {
        #$pdf->Text(56.0, 204.1, $data['insurance']);
	 $pdf->Text(26.0, 287, 'Für die zu versichernde Person besteht eine gesetzliche Krankenversicherung bei '.$data['insurance']);
     }


        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+3, 0, 2);

        $pdf->Text(41, 154.4, substr($data['begin'], 0, 1)); // -25.5
        $pdf->Text(47, 154.4, substr($data['begin'], 1, 1));
        $pdf->Text(65, 154.4, substr($data['begin'], 5, 1));
        $pdf->Text(71, 154.4, substr($data['begin'], 6, 1));

	 switch($pdfTemplate)
	 {
		case project::gti('gothaer-mediz-duo-100'):
		$pdf->Text(65.4, 170.6, 'x');
		break;

		case project::gti('gothaer-mediz-duo-90'):
		$pdf->Text(55.4, 170.6, 'x');
		break;
	
		case project::gti('gothaer-mediz-duo-80'):
		$pdf->Text(45.4, 170.6, 'x');

		break;
	 }

        if($i==0) {
            $this->pdfRect(153.0, 242.7, X_Y, $pdf, X_Y);
            $this->pdfRect(159.1, 242.7, X_Y, $pdf, X_Y);

            $this->pdfRect(153.0, 253.3, X_Y, $pdf, X_Y);
            $this->pdfRect(159.1, 253.3, X_Y, $pdf, X_Y);

            $this->pdfRect(152.2, 259.1, 10, $pdf, X_Y);
 
            $this->pdfRect(153.0, 267.0, X_Y, $pdf, X_Y);
            $this->pdfRect(159.1, 267.0, X_Y, $pdf, X_Y);

            #$this->pdfRect(153.2, 233.8, 8, $pdf, 3);

            $this->pdfRect(153.0, 271.2, X_Y, $pdf, X_Y);
	     $this->pdfRect(159.1, 271.2, X_Y, $pdf, X_Y);

		#if($pdfTemplate == project::gti('gothaer-mediz-duo-100') && $data['personInsured']['age'] <= 21)
		#{
	       #     $this->pdfRect(153.0, 256.9, X_Y, $pdf, X_Y);
       	#     $this->pdfRect(159.1, 256.9, X_Y, $pdf, X_Y);
		#}

        }


        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
         $pdf->Text(180.0, 170.7, $data['price']);
         $pdf->Text(180.0, 203.0, $data['price']);
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
    

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);


// SEITE 2 
     $tplidx = $pdf->importPage(2, '/MediaBox');


        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        if($i==0) {
            $this->pdfRect(35.6, 93.2, X_Y, $pdf, X_Y);
            $this->pdfRect(45.6, 93.2, X_Y, $pdf, X_Y);

            $this->pdfRect(55.6, 93.3, 100, $pdf, X_Y);
	 }

        // Unterschriften
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
        $this->pdfRect(25.5, 240.2, 33, $pdf);
        $this->pdfRect(74.5, 240.2, 33, $pdf);
        $this->pdfRect(25.5, 254.9, 33, $pdf);

     $pdf->Text(25.0, 281, 'Versicherungsmakler Experten GmbH, 08142 - 651 39 28');
     $pdf->Text(178.0, 281, 'VN : 3 6 4 8 4');




// SEITE 3
        $tplidx = $pdf->importPage(3, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

     $pdf->Text(25.3, 103.5, 'X');

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2); 

       
        // Markieren der Bankzeilen
        #$this->pdfRect(25.5, 117.0, 40, $pdf);
     $pdf->Text(29.0, 122.9, $data['nameb']);
        // Name
        #$this->pdfRect(25.5, 127.0, 100, $pdf);
     $pdf->Text(29.0, 132.9, $data['street']);
        // Land / PLZ / Ort
        #$this->pdfRect(25.5, 138.0, 12, $pdf); $this->pdfRect(46.5, 138.0, 20, $pdf); $this->pdfRect(76.5, 138.0, 20, $pdf); 
     $pdf->Text(29.0, 143.6, $data['nation']);
     $pdf->Text(52.0, 143.6, $data['postcode']);
     $pdf->Text(86.0, 143.6, $data['city']);

        // IBAN
        $this->pdfRect(25.5, 150.4, 100, $pdf);
        // BIC / Name der Bank
        $this->pdfRect(25.5, 161.4, 52, $pdf); $this->pdfRect(90.5, 159.0, 80, $pdf);

        // Unterschriften
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);
        $this->pdfRect(25.5, 178.4, 33, $pdf); 
        $this->pdfRect(64.5, 178.4, 33, $pdf);
        $this->pdfRect(107.5, 178.4, 44, $pdf);

        #$pdf->Text(108.8, 153.9, 'X');
        
        if($data['name'] != $data['name2b']) {
        #    $this->pdfRect(25.5, 168.2, 50, $pdf);
        #    $pdf->Text(26.8, 172.8, 'X');
        }
        $this->pdfClean(43.0, 280.0, 130.0, $pdf, 255, 255, 255, 8);

     $this->getCompanyStamp(100.0, 262.4, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize'], 'horizontal', 'wide');

// SEITE 4
        $tplidx = $pdf->importPage(4, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
        $this->pdfClean(46.0, 284.0, 130.0, $pdf, 255, 255, 255, 8);

// SEITE 5
        $tplidx = $pdf->importPage(5, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
        $this->pdfClean(0.0, 250.0, 210.0, $pdf, 255, 255, 255, 70);

// SEITE 6
        $tplidx = $pdf->importPage(6, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
        $this->pdfClean(50.0, 263.5, 130.0, $pdf, 255, 255, 255, 8);


