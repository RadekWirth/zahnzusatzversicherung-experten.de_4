<?
// File for generating Contract PDF for Bayrische Zahn Smart, Kompakt, Prestige

    define('CROWNS', $data['contractComplete']['tooth4'] ? $data['contractComplete']['tooth4'] : null);
    define('PROSTHESES', $data['contractComplete']['tooth3'] ? $data['contractComplete']['tooth3'] : null);
    define('MISSING_TOOTH', $data['t1'] ? $data['t1']  : null);
    define('AGE_PERSONINSURED', actionHelper::getAge($data['personInsured']['birthdate']));

    $pagecount = $pdf->setSourceFile($path.'files/die-bayerische/antraege/2022/bayerische.zzv.antrag.neu.1-2022.pdf');


/**
 * Page 1
 */
    $tplidx = $pdf->importPage(1, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 1, 16, 199);
    $pdf->SetMargins(0, 0, 0);

    $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);

    // gelbe markierungen nur auf rückantwort drucken!
    if($i==0) $this->pdfRect(72, 0.5, 68, $pdf);
    $pdf->Text(72, 4.5, $titles[$i]);

    $this->getCompanyStamp(76.0, 9.5, $pdf, $data['contractComplete']['sourcePage'], 7, 'horizontal', 'wide');

    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

    // 1.AV Nummer
    //$pdf->Text(27.0, 28.6, '1  6   3   6  0  2');

    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);

    $pdf->Text(28.2, 37.6, "x");
    $bg = explode(".", $data['begin']);
    $pdf->Text(72.1, 37.4, $bg[0][0]);
    $pdf->Text(75.6, 37.4, $bg[0][1]);
    $pdf->Text(79.2, 37.4, $bg[1][0]);
    $pdf->Text(83.5, 37.4, $bg[1][1]);
    $pdf->Text(86.8, 37.4, $bg[1][2]);
    $pdf->Text(91.1, 37.4, $bg[1][3]);

    // Antragsteller / Versicherungsnehmer
    ($data['gender']=='male') ? $pdf->Text(13.4, 58, 'x') : $pdf->Text(23.6, 58, 'x');
        if($i==0) $this->pdfRect(54.7, 55.9, 2, $pdf, 2);
        if($i==0) $this->pdfRect(64.9, 55.9, 2, $pdf, 2);   
    $bd = explode(".", $data['birthdate']);
    $pdf->Text(137.1, 62.1, $bd[0][0]);
    $pdf->Text(141, 62.1, $bd[0][1]);
    $pdf->Text(145.1, 62.1, $bd[1][0]);
    $pdf->Text(148.8, 62.1, $bd[1][1]);
    $pdf->Text(152.8, 62.1, $bd[2][0]);
    $pdf->Text(156.5, 62.1, $bd[2][1]);
    $pdf->Text(160.4, 62.1, $bd[2][2]);
    $pdf->Text(164.4, 62.1, $bd[2][3]);


    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);

    $pdf->Text(16, 70.3, $data['name']);
    $pdf->Text(16, 78.5, $data['street']);
    $pdf->Text(16, 86.8, $data['postcode']);
    $pdf->Text(38, 86.8, $data['city']);

    if (!empty($data['phone'])) {
        $pdf->Text(137, 70.3, $data['phone']);
    } else {
        if($i==0) $this->pdfRect(137, 67.6, 50, $pdf, 3);
    }

    if (!empty($data['email'])) {
        if(strlen($data['email']) > 25) {
            $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
        }
        $pdf->Text(137, 86.8, $data['email']);
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);
    } else {
        if($i==0) $this->pdfRect(137, 84.1, 50, $pdf, 3);
    }

    // Zu versichernde Person
    $pdf->Text(16, 101.3, $data['personInsured']['surname']);
    $pdf->Text(16, 109.5, $data['personInsured']['forename']);

    $bd = explode(".", $data['birthdate2']);
    $pdf->Text(137.1, 101.3, $bd[0][0]);
    $pdf->Text(141, 101.3, $bd[0][1]);
    $pdf->Text(145.1, 101.3, $bd[1][0]);
    $pdf->Text(148.8, 101.3, $bd[1][1]);
    $pdf->Text(152.8, 101.3, $bd[2][0]);
    $pdf->Text(156.5, 101.3, $bd[2][1]);
    $pdf->Text(160.4, 101.3, $bd[2][2]);
    $pdf->Text(164.4, 101.3, $bd[2][3]);

    ($data['gender2']=='male') ? $pdf->Text(151.9, 109.1, 'x') : $pdf->Text(168.6, 109.1, 'x');

    // Kreuz bei entsprechendem Tarif
    if($pdfTemplate == project::gti('bayerische-zahn-prestige-plus') || 
	$pdfTemplate == project::gti('bayerische-zahn-prestige-plus-sofort')){
        $pdf->Text(23.8, 125.5, 'x');
    }
    if($pdfTemplate == project::gti('bayerische-zahn-prestige') ||
	$pdfTemplate == project::gti('bayerische-zahn-prestige-sofort')){
        $pdf->Text(42.6, 125.5, 'x');
    }
    if($pdfTemplate == project::gti('bayerische-zahn-komfort') ||
	$pdfTemplate == project::gti('bayerische-zahn-komfort-sofort')){
        $pdf->Text(56.1, 125.5, 'x');
    }
    if($pdfTemplate == project::gti('bayerische-zahn-smart') ||
	$pdfTemplate == project::gti('bayerische-zahn-smart-sofort')){
        $pdf->Text(69.8, 125.5, 'x');
    }

    $pdf->Text(164, 124.6, $data['price']);

    if($pdfTemplate == project::gti('bayerische-zahn-prestige-plus-sofort') || 
	$pdfTemplate == project::gti('bayerische-zahn-prestige-sofort') ||
	$pdfTemplate == project::gti('bayerische-zahn-komfort-sofort') ||
	$pdfTemplate == project::gti('bayerische-zahn-smart-sofort')){
        $pdf->Text(35.8, 134.0, 'x');
    }

	

    // Zahlungsweise
    if($i==0) $this->pdfRect(105.5, 123.5, 1.7, $pdf, 1.8);
    if($i==0) $this->pdfRect(105.5, 127.7, 1.7, $pdf, 1.8);
    
    if($i==0) $this->pdfRect(124.4, 123.5, 1.7, $pdf, 1.8);
    if($i==0) $this->pdfRect(124.4, 127.7, 1.7, $pdf, 1.8);

    // Gesundheitsfragen

    // 1. Fehlen Zähne?
    if($i==0) $this->pdfRect(156.4, 203.2, 2, $pdf, 2);
    if($i==0) $this->pdfRect(173.9, 203.2, 2, $pdf, 2);

    if($i==0) $this->pdfRect(70.8, 202.7, 12, $pdf, 3);
    if (MISSING_TOOTH !== null) {
    	$pdf->Text(156.5, 204.8, 'x'); 
    	$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
    	$pdf->Text(73.3, 205.1, MISSING_TOOTH);
    	$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);
    } 

    // 2. Teilprothese?
    if($i==0) $this->pdfRect(156.4, 209.6, 2, $pdf, 2);
    if($i==0) $this->pdfRect(173.9, 209.6, 2, $pdf, 2);
    #$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+3, 0, 2);
    #if (PROSTHESES !== null) {
    #    $pdf->Text(111.7, 193.5, 'x');
    #    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
    #} else {
    #    #$pdf->Text(110, 203.5, 'x');
    #}

    // 3. Derzeit Zahnbehandlungen?
    if($i==0) $this->pdfRect(156.4, 215.7, 2, $pdf, 2);
    if($i==0) $this->pdfRect(173.9, 215.7, 2, $pdf, 2);
    #$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+3, 0, 2);
    #if ($data['contractComplete']['incare'] == 'yes') {
    #    $this->pdfRect(120, 208.3, 75.3, $pdf, 12.5);
    #    $pdf->Text(101.7, 203.5, 'x');
    #} else {
    #    #$pdf->Text(110, 203.5, 'x');
    #}

    // 4. Bestand eine ZZVersicherung?
    if($i==0) $this->pdfRect(156.4, 231.9, 2, $pdf, 2);
    if($i==0) $this->pdfRect(173.9, 231.9, 2, $pdf, 2);


    // Betragsteigerung?
    if($data['personInsured']['bonus']['nextBonusBase'] && $data['personInsured']['bonus']['nextBonusBase'] != $data['personInsured']['bonus']['activeBonusBase'])
	 {
		$pdf->Text(173.5, 124.0, '*');
		$pdf->Text(110, 290.5, '* Achtung: Beitragsanpassung zum '.date('d.m.Y', strtotime($data['personInsured']['bonus']['nextBonusDate'])).' = '.stringHelper::makeGermanFloat(stringHelper::toFloat($data['personInsured']['bonus']['nextBonusBase']) + stringHelper::toFloat($riskSurcharge)).' €');
	 }


/**
 * Page 2
 */
    $tplidx = $pdf->importPage(2, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);



    // SEPA
    if ($i == 0) $this->pdfRect(20, 41.1, 77, $pdf, 4);
    if ($i == 0) $this->pdfRect(13, 50.4, 82, $pdf, 4);
    if ($i == 0) $this->pdfRect(13, 59.8, 36, $pdf, 4);
    if ($i == 0) $this->pdfRect(53, 59.8, 39, $pdf, 4);
    if ($i == 0) $this->pdfRect(102, 44.4, 92, $pdf, 19);

    // Unterschrift Antragsteller und gegebenenfalls Gesetzlicher Vertreter
    // if($i==0) $this->pdfRect(12.7, 139.9, 2, $pdf, 2);
    // if($i==0) $this->pdfRect(12.7, 155.3, 2, $pdf, 2);

    // $pdf->Text(12.9, 141.8, 'x');
    // $pdf->Text(12.9, 157, 'x');

    if($i==0) $this->pdfRect(13, 166.8, 35, $pdf, 4);
    if($i==0) $this->pdfRect(53, 166.8, 78, $pdf, 4);

    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

/**
 * Page 3
 */


    $tplidx = $pdf->importPage(3, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

    if ($i == 0) $this->pdfRect(13, 90.9, 34, $pdf, 4);
    if ($i == 0) $this->pdfRect(52, 90.9, 59, $pdf, 4);
    if ($data['personInsured']['forename'] != $data['person']['forename'])
    {
        if ($i == 0) $this->pdfRect(146, 90.9, 39, $pdf, 4);
    }


    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);

    // 1.AV Nummer
    $pdf->Text(14.3, 127.8, '1  6  3  6  0  2');
    
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);
    
    //Stempel
    // Maximilian Waizmann, Begonienstr 1 ...
    $pdf->Image($path.'/files/logo.png', 128, 109, 32.0);

    $pdf->Text(128, 119, "Versicherungsmakler Experten GmbH");
    $pdf->Text(128, 122, "Feursstr. 56 / RGB");
    $pdf->Text(128, 125, "82140 Olching");
    $pdf->Text(128, 128, "Tel.: 08142 - 651 39 28");
    $pdf->Text(128, 131, "info@zahnzusatzversicherung-experten.de");

/*
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);
    if ($i == 0) $pdf->Text(10, 83.5, 'x');
    if ($i == 0) $pdf->Text(10, 100.5, 'x');

    // Einwilligung in Datenverwendung
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
    
    if($i==0) $this->pdfRect(14.6, 235.6, 2, $pdf, 2);
    if($i==0) $this->pdfRect(14.6, 240.2, 2, $pdf, 2);
    if ($i == 0) $pdf->Text(8, 239, 'oder');

    if($i==0) $this->pdfRect(14.6, 249.6, 2, $pdf, 2);
    if($i==0) $this->pdfRect(14.6, 254.2, 2, $pdf, 2);
    if ($i == 0) $pdf->Text(8, 253, 'oder');

*/
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);

/**
 * Page 4
 */

    $tplidx = $pdf->importPage(4, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);
/*
    // Schlusserklärungen des Antragstellers und der zu versichernden Personen
    if($i==0) {
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+3, 0, 2);
        $this->pdfRect(10.2, 43, 38, $pdf, 5.2);
        $this->pdfRect(63.8, 43, 53, $pdf, 5.2);
        if(!empty($data['personInsured'])) {
            $this->pdfRect(132, 43, 55, $pdf, 5.2);
            $pdf->Text(133, 47.4, 'x');
        }
        $pdf->Text(11.4, 47.4, 'x');
        $pdf->Text(65.8, 47.4, 'x');
    }

    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);

    // 1.AV Nummer
    $pdf->Text(28.7, 65.5, '1   6   3   6  0   2');
*/    



