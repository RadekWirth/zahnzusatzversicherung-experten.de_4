<?php
// File for generating Contract PDF for Signal Iduna Pur (Start & Plus & Top & Basis)

        /*
         * Constants
         */
        // SQUARE XY
        define('X_Y', 2.0);

        // RECT Y
        define('Y', 3.5);

        $data['person']['age'] = actionHelper::getAge($data['person']['birthdate']);
        $data['personInsured']['age'] = actionHelper::getAge($data['personInsured']['birthdate']);

        //$pagecount = $pdf->setSourceFile($path.'/files/contract-sigidu.pdf');

        if ($i!=1) {
            $pagecount = $pdf->setSourceFile($path.'/files/gothaer/Antrag/2016/Gothaer-Blockpolice-Zahn.pdf');
        } else {
            $pagecount = $pdf->setSourceFile($path.'/files/gothaer/Antrag/2016/Gothaer-Blockpolice-Zahn_bw.pdf');
        }

        $tplidx = $pdf->importPage(1, '/MediaBox');

        $pdf->addPage('P');  // idx, x, y, w, h
        $pdf->useTemplate($tplidx, 0, 0, 210);
        $pdf->SetMargins(0, 0, 0);


        

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);

        if($i==0) $this->pdfRect(75.0, 3.0, 70, $pdf);
        $pdf->Text(75.0, 7.0, $titles[$i]);

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-1, 0, 2);


        #$pdf->Text(75.0, 28.0, "Versicherungsmakler Experten GmbH");
        $this->getCompanyStamp(75.0, 24, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']-1, 'horizontal', 'wide');
        
        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+3, 0, 2);

        $pdf->Text(40, 41.4, substr($data['begin'], 0, 2));
        $pdf->Text(59, 41.4, substr($data['begin'], 5, 2));


        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
        if($data['gender']=='male') {
            $pdf->Text(72.5, 41.6, 'X');
        } else {
            $pdf->Text(72.5, 44.9, 'X');
        }

        $pdf->Text(29.0, 52.0, $data['name']);
        
        if(empty($data['nation'])) {
            $pdf->Text(103, 41.1, 'deutsch');
        } else {
            $pdf->Text(103, 41.1, $data['nation']);
        }

        //Telefonnummer
        if(!empty($data['phone']) || $data['phone']=='')
        {
            if($i==0) $this->pdfRect(123.8, 38.7, 39, $pdf, 3);
        } else
            $pdf->Text(124.5, 41.1, $data['phone']);

        $pdf->Text(29.0, 60.9, $data['street'].
            ', '.$data['postcode'].' '.$data['city']);

        $pdf->Text(166.8, 61.9, $data['birthdate']);


        // VP
        $pdf->Text(33.0, 88.5, $data['name2b']);
        $pdf->Text(166.8, 88.5, $data['birthdate2']);

        if($data['gender2']=='male')    {
            $pdf->Text(148.6, 89.4, 'X');
        } else {
            $pdf->Text(153.6, 89.4, 'X');
        }



        if($pdfTemplate == project::gti('gothaer-mediz-plus-medi-prophy')){
            $pdf->Text(45.2, 141.4, 'X');
            $pdf->Text(113.6, 141.4, 'X');
        } else if($pdfTemplate == project::gti('gothaer-mediz-plus')){
            $pdf->Text(45.2, 141.4, 'X');
        }  else if($pdfTemplate == project::gti('gothaer-mediz-basis-medi-prophy')){
            $pdf->Text(35.2, 141.4, 'X');
            $pdf->Text(113.6, 141.4, 'X');
        }   else if($pdfTemplate == project::gti('gothaer-mediz-basis')){
            $pdf->Text(35.2, 141.4, 'X');
        }   else if($pdfTemplate == project::gti('gothaer-mediz-prem-medi-prophy')){
            $pdf->Text(55.6, 141.4, 'X');
            $pdf->Text(113.6, 141.4, 'X');
        }   else if($pdfTemplate == project::gti('gothaer-mediz-prem')){
            $pdf->Text(55.6, 141.4, 'X');
        }  

        if($i==0)
        {    
		$this->pdfRect(154.4, 137.2, 10, $pdf);
	       if( ! empty($data['t1']) && $data['t1'] > 0 )
       	{
	              $pdf->Text(156.2, 140.6, $data['t1']);
		}
        }         
        

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
        $pdf->Text(190.0, 140.6, $data['price']);
        $pdf->Text(190.0, 169.7, $data['price']);
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);        

        // Zahlweise
        $pdf->Text(175.8, 178.8, 'X');

        if($i==0) {
            $this->pdfRect(39.4, 181.9, X_Y, $pdf, X_Y);
            $this->pdfRect(69.6, 181.9, X_Y, $pdf, X_Y);
            $this->pdfRect(88.1, 181.9, X_Y, $pdf, X_Y);
            $this->pdfRect(106.6, 181.9, X_Y, $pdf, X_Y);
        }


        // Unterschriften


        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);
        $this->pdfRect(27.5, 270.2, 40, $pdf);
        $this->pdfRect(72.5, 270.2, 50, $pdf);
        
        $pdf->Text(28.8, 274.8, 'X');
        $pdf->Text(73.8, 274.8, 'X');

        if($data['personInsured']['age'] > 15 && $data['name'] != $data['name2b']) {
            $this->pdfRect(137.5, 270.2, 50, $pdf);
            $pdf->Text(138.8, 274.8, 'X');
        }
        
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);


// SEITE 2 (SEPA)
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, -290, 210);
        $pdf->SetMargins(0, 0, 0);

        $this->getCompanyStamp(100.0, 6, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']-1, 'horizontal', 'wide');

        // Markieren der Bankzeilen
        $this->pdfRect(25.5, 90.0, 40, $pdf);

        // Name
        $this->pdfRect(25.5, 101.2, 100, $pdf);
        // Adresse
        $this->pdfRect(25.5, 109.2, 100, $pdf);
        // Land / PLZ / Ort
        $this->pdfRect(25.5, 117.8, 12, $pdf); $this->pdfRect(46.5, 117.8, 20, $pdf); $this->pdfRect(76.5, 117.8, 20, $pdf); 
        // IBAN
        $this->pdfRect(25.5, 126.2, 100, $pdf);
        // BIC / Name der Bank
        $this->pdfRect(25.5, 134.5, 52, $pdf); $this->pdfRect(90.5, 134.5, 80, $pdf);

        // Unterschriften
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);
        $this->pdfRect(25.5, 149.0, 33, $pdf);
        $this->pdfRect(64.5, 149.0, 33, $pdf);
        $this->pdfRect(107.5, 149.0, 44, $pdf);

        $pdf->Text(108.8, 153.9, 'X');
        
        if($data['name'] != $data['name2b']) {
            $this->pdfRect(25.5, 168.2, 50, $pdf);
            $pdf->Text(26.8, 172.8, 'X');
        }
        
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);        



// SEITE 3
        $tplidx = $pdf->importPage(2, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        if($complete)   
            $pdf->addPage('P');
?>
