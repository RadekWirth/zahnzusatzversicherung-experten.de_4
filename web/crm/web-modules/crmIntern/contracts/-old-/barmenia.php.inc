<?
		$fontSize = 8;
// File for generating Contract PDF for Barmenia

              $pagecount = $pdf->setSourceFile($path.'/files/barmenia/antrag/2020/Barmenia.Zahn.Antrag.neu_092020.pdf');

		$tplidx = $pdf->importPage(1, '/MediaBox');
		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);


		#$this->pdfClean(50, 6.5, 95, $pdf, 255, 255, 255, 10.5);
	// reset font
		#$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-2, 0, 2);

   		#$this->getCompanyStamp(120, 6, $pdf, $data['contractComplete']['sourcePage'], 7.5, 'horizontal', 'wide');

	// reset font
		#$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		// set title and font to big and bold!
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $fontSize+5, 0, 2);

		if($i==0) $this->pdfRect(50.0, 1, 68, $pdf);
		$pdf->Text(50.0, 5.0, $titles[$i]);

		// Vermittlernummer
		$this->pdfClean(167, 16.8, 20, $pdf, 255, 255, 255, 2.5, 'F');
		$pdf->SetFont($pdfCfg['fontFamily'], '', $fontSize-1, 0, 2);
		$pdf->Text(176, 34.7, '0010 / 3545');

		// reset font
		//$pdfCfg['fontSize'] = $fontSize;
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		// private Telefonnummer
		#if($i==0) $this->pdfRect(109.7, 41.5, 42, $pdf, 3.2);
		// Mobil Telefon
		#if($i==0) $this->pdfRect(109.7, 48.8, 42, $pdf, 3.2);



		// Neuantrag
		#$pdf->Text(103.3, 27.9, 'X');

 		if($data['gender']=='male')	{
			$pdf->Text(17.9, 42.4, 'X');
		} else {
			$pdf->Text(25.7, 42.4, 'X');
		}

		$pdf->Text(36.0, 42.8, $data['name']);
 		$pdf->Text(17.5, 51.0, $data['street']);
 		$pdf->Text(17.5, 59.3, $data['postcode']);
 		$pdf->Text(40.5, 59.3, $data['city']);

		$pdf->Text(120.0, 42.8, $data['birthdate']);

		if(empty($data['phone'])) {
			if($i==0) $this->pdfRect(119.5, 49.0, 36, $pdf, 3.2);
		} else
			$pdf->Text(120.0, 51.0, $data['phone']);

		if(empty($data['mobile'])) {
			if($i==0) $this->pdfRect(119.5, 56.9, 36, $pdf, 3.2);
		} else
			$pdf->Text(120.0, 59.2, $data['mobile']);

 		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
		if(empty($data['email'])) {
			if($i==0) $this->pdfRect(159.0, 56.9, 38, $pdf, 3.2);
		} else
 			$pdf->Text(160.0, 59.0, $data['email']);

		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		if(empty($data['job']))
		{
			if($i==0) $this->pdfRect(16.8, 65.5, 44, $pdf, 3.2);
		} else
 		 	$pdf->Text(17.5, 67.8, $data['job']);


		//Berufsstellung
		if($i==0) $this->pdfRect(33.6, 70.9, 2.5, $pdf, 2.5);



		$pdf->Text(160.0, 42.8, $data['nation']);

		if($data['widowed']) {
			$pdf->Text(194.8, 42.8, 'X');
		}else if($data['married']) {
			$pdf->Text(180.5, 42.8, 'X');
		}
		#else
		#	$pdf->Text(173.5, 42.8, 'X');


	// Zu versichernde Person
		if($i==0) $this->pdfRect(193.2, 83.9, 3, $pdf, 3);
		// child
		$pdf->Text(44.0, 86.5, $data['name2b']);
		if($data['gender2']=='male') {
			$pdf->Text(106.8, 86.6, 'X');
		}else
			$pdf->Text(113.5, 86.6, 'X');

		$pdf->Text(124.0, 86.5, $data['birthdate2']);

		if(empty($data['job2'])) {
			if($i==0) $this->pdfRect(154.5, 83.8, 36, $pdf, 3.2);
		} else
 			$pdf->Text(156.0, 86.5, $data['job2']);


		$pdf->Text(78.5, 105.5, '01.'.$data['begin']);


	// Beantragte Versicherung
		// tariff
		switch($pdfTemplate)
		{
			case project::gti('barmenia-zahn100'):
				$pdf->Text(21.1, 118.0, 'x');
				$pdf->Text(48.6, 121.3, 'x');
				break;
			case project::gti('barmenia-zahn100-zv'):
				$pdf->Text(21.1, 118.0, 'x');
				$pdf->Text(48.6, 121.3, 'x');
				$pdf->Text(21.1, 126.5, 'x'); // -42.5
				break;

			case project::gti('barmenia-zahn90'):
				$pdf->Text(21.1, 118.0, 'x');
				$pdf->Text(37.6, 121.3, 'x');
				break;
			case project::gti('barmenia-zahn90-zv'):
				$pdf->Text(21.1, 118.0, 'x');
				$pdf->Text(37.6, 121.3, 'x');
				$pdf->Text(21.1, 126.5, 'x');
				break;

			case project::gti('barmenia-zahn80'):
				$pdf->Text(21.1, 118.0, 'x');
				$pdf->Text(26.4, 121.3, 'x');
				break;
			case project::gti('barmenia-zahn80-zv'):
				$pdf->Text(21.1, 118.0, 'x');
				$pdf->Text(26.4, 121.3, 'x');
				$pdf->Text(21.1, 126.5, 'x');
				break;

		}
		$this->pdfClean(65, 135.0, 9.0, $pdf, 255, 255, 255, 3.5);

		$pdf->Text(65.0, 121.3, $data['price']);
		$pdf->Text(65.0, 138.1, $data['price']);


		if($i==0) $this->pdfRect(141.0, 168.0, 3, $pdf, 3.2);
		if($i==0) $this->pdfRect(151.4, 168.0, 3, $pdf, 3.2);

		if($i==0) $this->pdfRect(141.0, 175.2, 3, $pdf, 3.2);
		if($i==0) $this->pdfRect(151.4, 175.2, 3, $pdf, 3.2);

		if($i==0) $this->pdfRect(151.8, 179.8, 5.5, $pdf, 3.2);	

		if($i==0) $this->pdfRect(141.0, 187.3, 3, $pdf, 3.2);
		if($i==0) $this->pdfRect(151.4, 187.3, 3, $pdf, 3.2);


		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+32, 0, 2);
			if($i==0) $this->pdfRect(7.5, 166.0, 7, $pdf, 13);
			$pdf->Text(9, 177.2, '!');
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);


		// Dieser Leistungseinschänkung stimme ich zu
		if($data['contractComplete']['tooth1'] == 2 || $data['contractComplete']['tooth1'] == 3)	
		{
			$pdf->Text(19.5, 257.0, 'X');
		}

		// Besondere Vereinbarungen
			$pdf->Text(18.0, 268.3, 'X');
/*


	// Sonstige Krankenversicherungen

		#$pdf->Text(13.5, 115.7, '1');
		$pdf->Text(30.8, 222.7, 'X');

		if(empty($data['insurance2'])) {
			if($i==0) $this->pdfRect(39.0, 220.1, 60, $pdf, 3.2);
		} else
 			$pdf->Text(40.0, 222.7, $data['insurance2']);

		if($data['insuredSince2']!="0000" && $data['insuredSince2']!="0001")
			$pdf->Text(140.0, 222.7, $data['insuredSince2']);
		if($data['insuredFamily2'] ||
			$data['insuredStrict2']) {
			$pdf->Text(170.5, 222.7, 'X'); // strict yes
		} else {
			$pdf->Text(176.5, 222.7, 'X'); // strict no
		}




	// Gesundheitsfragen an die zu versichernde Person
		if($i==0) $this->pdfRect(118.3, 139.8, 3.5, $pdf, 3.5);
		if($i==0) $this->pdfRect(130.2, 139.8, 3.5, $pdf, 3.5);
		if($i==0) $this->pdfRect(118.3, 148.9, 3.5, $pdf, 3.5);
		if($i==0) $this->pdfRect(130.2, 148.9, 3.5, $pdf, 3.5);
		if($i==0) $this->pdfRect(130.1, 153.7, 5.8, $pdf, 3.1);

		if($data['incare']) {
			$pdf->Text(131.0, 142.5, 'X');
		}else {
	if($data['contractComplete'])
			$pdf->Text(119.0, 142.5, 'X');
		}

		$pdf->Text(132.5, 156.2, $data['t1']);
		if($data['t1'] > 0) {
			$pdf->Text(131.0, 151.6, 'X');
		}else {
	if($data['contractComplete'])
			$pdf->Text(119.0, 151.6, 'X');
		}

	// Beitragsabruf
		//Bank / Sparkasse
		if($i==0) $this->pdfRect(20.9, 220.7, 82.5, $pdf, 3.2);
		//BIC
		if($i==0) $this->pdfRect(107.7, 220.7, 43.7, $pdf, 3.2);
		//Name des Kreditinstitutes
		if($i==0) $this->pdfRect(155.7, 220.7, 47.2, $pdf, 3.2);
		//Kontonummer
		if($i==0) $this->pdfRect(13.0, 229.9, 138, $pdf, 2.8);
		//Unterschrift d Kontoinhaber
		if($i==0) $this->pdfRect(156, 229.9, 46, $pdf, 2.8);
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);
		if($i==0) $pdf->Text(156, 232.8, 'X');

		//Datum
		if($i==0) $this->pdfRect(13.0, 273.5, 21, $pdf, 3.2);
		//Antragsteller
		if($i==0) $this->pdfRect(39.5, 273.5, 46, $pdf, 3.2);
		if($i==0) $pdf->Text(40, 276.6, 'X');
		if ($data['personInsured']['forename'] != $data['person']['forename'] && $data['person']['forename'] != $data['person']['surname']) {
			//Zu versich. Person
			if($i==0) $this->pdfRect(92.5, 273.5, 46, $pdf, 3.2);	
			if($i==0) $pdf->Text(93, 276.6, 'X');				
		}
		if (actionHelper::getAge($data['person']['birthdate']) < 18) {
			//ges. Vertreter
			if($i==0) $this->pdfRect(144.9, 273.5, 46, $pdf, 3.2);
			if($i==0) $pdf->Text(145.4, 276.6, 'X');	
		}
*/
		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		$pdf->Text(49.0, 285.6, $data['namec']);
		#$pdf->Text(110.0, 285.6, $data['signupDate']);

	// page 2
		$tplidx = $pdf->importPage(2, '/MediaBox');
		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);

		$pdf->Text(49.0, 9.5, $data['namec']);
		#$pdf->Text(110.0, 9.5, $data['signupDate']);


	// Sonstige Krankenversicherungen

		$pdf->Text(31.5, 29.3, 'X');

		if(empty($data['insurance2'])) {
			if($i==0) $this->pdfRect(39.0, 26.3, 60, $pdf, 3.2);
		} else
 			$pdf->Text(40.0, 29.3, $data['insurance2']);

		if($data['insuredSince2']!="0000" && $data['insuredSince2']!="0001")
			$pdf->Text(140.0, 29.3, $data['insuredSince2']);
		if($data['insuredFamily2'] || $data['insuredStrict2']) {
			#$pdf->Text(193.0, 29.3, 'X'); // strict yes
		} else {
			#$pdf->Text(187.2, 29.3, 'X'); // strict no
		}



	// Lastschriftmandat
		if($i==0) $this->pdfRect(27.0, 64.3, 70, $pdf, 3.2);
		if($i==0) $this->pdfRect(22.0, 73.6, 110, $pdf, 3.2);
		if($i==0) $this->pdfRect(110.0, 64.3, 70, $pdf, 3.2);
		if($i==0) $this->pdfRect(155.0, 73.6, 40, $pdf, 3.2);
		



		// Unterschriften
		if($i==0) $this->pdfRect(20.4, 114.5, 16, $pdf, 3.2);
		if($i==0) $this->pdfRect(47.4, 114.5, 36, $pdf, 3.2);
		
		if($i==0) $this->pdfRect(99.4, 114.5, 36, $pdf, 3.2);
		#if($i==0) $this->pdfRect(148.4, 114.5, 30, $pdf, 3.2);

		if (actionHelper::getAge($data['person']['birthdate']) < 18) {
			//ges. Vertreter
			if($i==0) $this->pdfRect(47.4, 121.5, 36, $pdf, 3.2);
			if($i==0) $this->pdfRect(99.4, 121.5, 36, $pdf, 3.2);
		}

		$pdf->Text(158.4, 116.1, '0010 / 3545');
		$pdf->Text(158.4, 120.5, 'D-YIRL-NDZ7C-33');
		$pdf->Text(158.4, 124.5, '08142 / 651 39 28');

/*
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);
		//Datum
		if($i==0) $this->pdfRect(13.0, 41.2, 21, $pdf, 3.2);
		//Antragsteller
		if($i==0) $this->pdfRect(40.5, 41.2, 46, $pdf, 3.2);
		if($i==0) $pdf->Text(41, 44.2, 'X');
		if ($data['personInsured']['forename'] != $data['person']['forename'] && $data['person']['forename'] != $data['person']['surname']) {
			//Zu versich. Person
			if($i==0) $this->pdfRect(95.5, 41.2, 48, $pdf, 3.2);	
			if($i==0) $pdf->Text(96, 44.2, 'X');				
		}
		if (actionHelper::getAge($data['person']['birthdate']) < 18) {
			//ges. Vertreter
			if($i==0) $this->pdfRect(150.5, 41.2, 45, $pdf, 3.2);
			if($i==0) $pdf->Text(151.4, 44.2, 'X');	
		}
		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
*/
	// page 3
		$tplidx = $pdf->importPage(3, '/MediaBox');
		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);

		$pdf->Text(49.0, 9.5, $data['namec']);
		#$pdf->Text(110.0, 9.5, $data['signupDate']);

		$pdf->Text(19.1, 224.0, 'X');

	// page 4
		$tplidx = $pdf->importPage(4, '/MediaBox');
		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);

		$pdf->Text(49.0, 9.5, $data['namec']);
		#$pdf->Text(110.0, 9.5, $data['signupDate']);


	// page 5
              $pagecount = $pdf->setSourceFile($path.'/files/barmenia/antrag/2020/Barmenia.Empfang.Dokumente.2020.pdf');

		$tplidx = $pdf->importPage(1, '/MediaBox');
		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);

/*
		// Produktinformationsblatt
		$pdf->Text(68.5, 114.5, '+ Antrag');

		// Allgemeine Kundeninformationen
		$pdf->Text(70.5, 123.0, '(K6045)');

		// gewählter Tarif
		$pdf->Text(28.5, 133.0, 'Tarif ZGU+ (K4639)');

		// clean
		$this->pdfClean(27.0, 137.0, 136, $pdf, 255,255,255, 7.2);
		$this->pdfClean(27.0, 145.5, 138, $pdf, 255,255,255, 7);

		$pdf->Text(28.5, 140.0, 'Allg. Versicherungsbedingungen für die Krankenheitskosten- und Krankenhaustagegeld-');
		$pdf->Text(28.5, 143.5, 'versicherung MB/KK09 und TB/KK13 (K4601)');
		$pdf->Text(28.5, 148.5, 'Merkblatt zur Datenverarbeitung (B 3997, Ausgabe 04/2011)');
*/

		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $fontSize+3, 0, 2);
		$pdf->Text(49.0, 77.8, $data['name']);
 		$pdf->Text(49.0, 85.6, $data['street']);
 		$pdf->Text(49.0, 93.5, $data['postcode'].' '.$data['city']);

		$pdf->Text(44.0, 174.8, 'X');
		$pdf->Text(53.0, 175.2, $data['email']);
		$pdf->Text(64.0, 183.0, 'X');

		$pdf->SetFont($pdfCfg['fontFamily'], '', $fontSize, 0, 2);
		// K5151 -> K5165
		$this->pdfClean(46.6, 160.5, 10, $pdf, 255,255,255, 4);
		$pdf->Text(46.6, 163.3, '(K5177)');


		//Datum
		if($i==0) $this->pdfRect(21.0, 230.3, 32, $pdf, 6.3);
		//Unterschrift
		if($i==0) $this->pdfRect(107.0, 230.3, 54, $pdf, 6.3);

		//Datum
		if($i==0) $this->pdfRect(21.0, 260.8, 32, $pdf, 6.3);
		//Unterschrift
		if($i==0) $this->pdfRect(107.0, 260.8, 54, $pdf, 6.3);


	if($complete==2) { $pdf->AddPage('P'); }

?>