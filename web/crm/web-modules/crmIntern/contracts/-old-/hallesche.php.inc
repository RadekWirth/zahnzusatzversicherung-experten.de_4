<?php 


    $pdf->SetTextColor(0,0,0); 
    // Prüfung, ob Datei vorhanden ist...
    if ($i != 1) {
        $pagecount = $pdf->setSourceFile($path.'/files/hallesche/Antrag/2020/Hallesche.Dent.Antrag.04-2020.pdf');
    } else {
        $pagecount = $pdf->setSourceFile($path.'/files/hallesche/Antrag/2020/Hallesche.Dent.Antrag.04-2020.pdf');
    }

/**
 * Page 1
 */
    $tplidx = $pdf->importPage(1, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 8, 8, 193);

    // set title and font to big and bold!
    $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);

    if($i == 0) $this->pdfRect(10.0, 1.0, 54, $pdf, 4.0);
    $pdf->Text(11.0, 4.0, $titles[$i]);

    //$this->getCompanyStamp(116.0, 5, $pdf, 'zzv-de', $pdfCfg['fontSize']-2, 'horizontal', 'wide');
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
    $pdf->Text(116, 5, 'Versicherungsmakler Experten GmbH');
    $pdf->Text(116, 8, 'Feursstr. 56 / RGB');
    $pdf->Text(116, 11, '82140 Olching');
    $pdf->Text(160, 8, 'Tel.: 08142 - 651 39 28');
    $pdf->Text(160, 11, $this->getEMailBySourcePage($data['contractComplete']['sourcePage']));

    // reset fontSize
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

    // Vermittlernummer
    $pdf->Text(113.0, 40.5, '470311');

    $pdf->Text(26, 55.3, $data['name']);
    $pdf->Text(26, 61.8, $data['street']);
    $pdf->Text(26, 68.7, $data['postcode']);
    $pdf->Text(47, 68.7, $data['city']);

    $pdf->Text(113, 55.1, $data['birthdate']);

    if($data['gender'] == 'female') {
        $pdf->Text(128.4, 62.0, 'x');
    } else {
        $pdf->Text(108, 62.0, 'x');
    }

    if( ! empty($data['phone'])) {
        $pdf->Text(150, 55.5, $data['phone']);
    } else {
        if($i == 0) $this->pdfRect(149.4, 52.4, 36, $pdf, 3.5);
    }

    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-3, 0, 2);
    if( ! empty($data['email'])) {
        $pdf->Text(150, 68.7, $data['email']);
    } else {
        if($i == 0) $this->pdfRect(149.4, 66.0, 36.0, $pdf, 3.5);    
    }

    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

    if ($data['personInsured']['surname'] . ', ' . $data['personInsured']['forename'] === $data['name']) {
        $pdf->Text(25.3, 85.7, 'x');
    } else {
        $pdf->Text(30.8, 94.2, 'x');
        $pdf->Text(46.2, 94.1, $data['personInsured']['surname'] .', '. $data['personInsured']['forename']);
        $pdf->Text(158.2, 94.1, $data['birthdate2']);
        if($data['gender2'] == 'female') {
            $pdf->Text(180.1, 94.3, 'x');
        } else {
            $pdf->Text(180.1, 91.0, 'x');
        }
    }


    // Beginn der Versicherung          
    // reset font
    $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+3, 0, 2);
        $begin = explode(".", $data['begin']);
        $pdf->Text(61.5, 130.3, $begin[0]);
        $pdf->Text(72, 130.3, $begin[1]);
    // reset font
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

    switch($pdfTemplate)
    {
        case project::gti('hallesche-megadent'):
            $pdf->Text(61.6, 158.5, 'X');
        break;
        case project::gti('hallesche-gigadent'):
            $pdf->Text(75.2, 158.5, 'X');
        break;
        case project::gti('hallesche-dentze90'):
            $pdf->Text(90.6, 158.5, 'X');
        break;
        case project::gti('hallesche-dentze100'):
            $pdf->Text(104.7, 158.5, 'X');
        break;
        case project::gti('hallesche-dentze100-dentpro90'):
            $pdf->Text(104.7, 158.5, 'X');
        break;
        case project::gti('hallesche-dentze90-dentpro80'):
            $pdf->Text(90.6, 158.5, 'X');
            $pdf->Text(146, 158.5, 'X');
        break;
    } 

    $pdf->Text(160.7, 158.0, $data['price'] . ' EUR');
    $pdf->Text(160.7, 184.0, $data['price'] . ' EUR');




/**
 * Page 2
 */
    $tplidx = $pdf->importPage(2, '/MediaBox');

    $pdf->addPage('P');
    //$pdf->useTemplate($tplidx, 0, 0, 209);
    $pdf->useTemplate($tplidx, 8, 8, 193);

    // Ausrufezeichen
    if ($i == 0) {
        $pdf->SetTextColor(56,230.7,0); 
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+11, 0, 2);
        $pdf->Text(20, 246, '!');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
        $pdf->SetTextColor(0,0,0); 
    }

    if ($i == 0) $this->pdfRect(133.6, 242.2, 2.5, $pdf, 2.5);
    if ($i == 0) $this->pdfRect(140.2, 242.2, 2.5, $pdf, 2.5);


/**
 * Page 3
 */
    $tplidx = $pdf->importPage(3, '/MediaBox');

    $pdf->addPage('P');
    //$pdf->useTemplate($tplidx, 0, 0, 209);
    $pdf->useTemplate($tplidx, 8, 8, 193);

    if (project::gti('hallesche-dentze100') !== $pdfTemplate && project::gti('hallesche-dentze100-dentpro90') !== $pdfTemplate && project::gti('hallesche-dentze90') !== $pdfTemplate && project::gti('hallesche-dentze90-dentpro80') !== $pdfTemplate) {
        if ($i == 0) $this->pdfRect(133.6, 27.5, 2.5, $pdf, 2.5);
        if ($i == 0) $this->pdfRect(140.2, 27.5, 2.5, $pdf, 2.5);
    }

    if ($i == 0) $this->pdfRect(133.6, 35.4, 2.5, $pdf, 2.5);
    if ($i == 0) $this->pdfRect(140.2, 35.4, 2.5, $pdf, 2.5);

    switch($pdfTemplate)
    {
        // Fragen
        case project::gti('hallesche-megadent'):
        case project::gti('hallesche-gigadent'):
        case project::gti('hallesche-dentze90'):
        case project::gti('hallesche-dentze90-dentpro80'):
        case project::gti('hallesche-dentze100'):
        case project::gti('hallesche-dentze100-dentpro90'):
            if ($i == 0) $this->pdfRect(133.6, 42.9, 2.5, $pdf, 2.5);
            if ($i == 0) $this->pdfRect(140.2, 42.9, 2.5, $pdf, 2.5);
            if ($i == 0) $this->pdfRect(134.0, 47.3, 8.5, $pdf, 2.5);
        break;
    }

    // Unterschriften
    if ($i == 0) $this->pdfRect(25.5, 126.6, 75, $pdf, 5);
    if ($i == 0) $this->pdfRect(110, 126.9, 75, $pdf, 5);
    if ($i == 0) $this->pdfRect(110, 137.3, 75, $pdf, 5);

    if ($i == 0) $this->pdfRect(25.5, 213.1, 75, $pdf, 5);
    if ($i == 0) $this->pdfRect(110, 213.4, 75, $pdf, 5);
    if ($i == 0) $this->pdfRect(110, 227.1, 75, $pdf, 5);
    if ($i == 0) $this->pdfRect(28.5, 227.1, 72, $pdf, 5);

/**
 * Page 4
 */
    $tplidx = $pdf->importPage(4, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);
    //$pdf->useTemplate($tplidx, 8, 8, 193);

    if ($i == 0) $this->pdfRect(18, 20.4, 2.5, $pdf, 2.5);
    if ($i == 0) $this->pdfRect(18, 31.8, 2.5, $pdf, 2.5);

    // Unterschriften
    if ($i == 0) $this->pdfRect(25.2, 48.5, 78, $pdf, 5);
    if ($i == 0) $this->pdfRect(109, 48.5, 74, $pdf, 5);




/**
 * Page 5 - 
 */
    $tplidx = $pdf->importPage(5, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, -5.6, 0, 222);

// Unterschriften
    if ($i == 0) $this->pdfRect(25.2, 130.0, 78, $pdf, 5);
    if ($i == 0) $this->pdfRect(109, 130.0, 74, $pdf, 5);

/**
 * Page 6 - SEPA
 */
    $tplidx = $pdf->importPage(6, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);


    $pdf->SetFont($pdfCfg['fontFamily'], '', 10, 0, 2);
    $pdf->Text(24, 151.5, $data['namec']);
    $pdf->Text(159, 151.5, $data['birthdate']);
    $pdf->Text(24, 161.2, $data['street']);
    $pdf->Text(112, 161.2, $data['postcode'] .' '. $data['city']);

    if ($i == 0) $this->pdfRect(22, 180.9, 165, $pdf, 4);
    if ($i == 0) $this->pdfRect(22, 192.9, 165, $pdf, 4);

    // Unterschriften
    if ($i == 0) $this->pdfRect(22, 216.2, 79, $pdf, 5);
    if ($i == 0) $this->pdfRect(111, 216.2, 79, $pdf, 5);
    $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);    
    $pdf->Text(22, 220.4, 'X');
    $pdf->Text(110, 220.4, 'X');

    // Daten Versicherte Person
    $pdf->SetFont($pdfCfg['fontFamily'], '', 10, 0, 2);
    $pdf->Text(24, 249.9, $data['namec']);
    $pdf->Text(158, 249.9, $data['birthdate']);
    $pdf->Text(24, 259.2, $data['street'] .', '. $data['postcode'] .' '. $data['city']);

    // Zahlungsweise
    if ($i == 0) $this->pdfRect(43.8, 263.2, 2.5, $pdf, 2.5);
    if ($i == 0) $this->pdfRect(63.5, 263.2, 2.5, $pdf, 2.5);
    if ($i == 0) $this->pdfRect(88.2, 263.2, 2.5, $pdf, 2.5);
    if ($i == 0) $this->pdfRect(110.6, 263.2, 2.5, $pdf, 2.5);


// Seite 8
    $tplidx = $pdf->importPage(8, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

// Seite 9
    $tplidx = $pdf->importPage(9, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

// Seite 10
    $tplidx = $pdf->importPage(10, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

// Seite 11
    $tplidx = $pdf->importPage(11, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

// Seite 12
    $tplidx = $pdf->importPage(12, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);


      