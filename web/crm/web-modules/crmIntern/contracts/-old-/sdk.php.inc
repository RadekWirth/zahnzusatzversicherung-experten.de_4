<?php
// File for generating Contract PDF for SDK

        /*
         * Constants
         */
        // SQUARE XY
        define('X_Y', 1.5);

        // RECT Y
        define('Y', 2.7);

        $data['person']['age'] = actionHelper::getAge($data['contractComplete']['birthdate']);
#print_r($data);

        $pagecount = $pdf->setSourceFile($path.'files/sdk/antrag/2020/Antrag-05-2020.pdf');



        $tplidx = $pdf->importPage(1, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
        $pdf->SetMargins(0, 0, 0);

	 $this->getCompanyStamp(30, 170, $pdf, 'zzv', 12);

	/* Seite verbergen */
	$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);


        $tplidx = $pdf->importPage(2, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
        $pdf->SetMargins(0, 0, 0);


	 /* Verm. Nr verbergen */
 	 $this->pdfClean(173.0, 10.2, 10, $pdf, 255, 255, 255, 6);
	 $pdf->Text(174.0, 14.0, '903305');

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);

        if($i==0) $this->pdfRect(75.0, 3.0, 70, $pdf);
        $pdf->Text(75.0, 7.0, $titles[$i]);

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-1, 0, 2);

        // -> Seite 2
        #$pdf->Text(10, 6.0, "Versicherungsmakler Experten GmbH");

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-3, 0, 2);

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);    

        if($data['gender']=='male') {
            $pdf->Text(12.0, 25.8, '1');
        } else {
            $pdf->Text(12.0, 25.8, '2');
        }
        
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
        $pdf->Text(27, 26.0, $data['name']);

        $pdf->Text(27.0, 37.6, $data['street']);

        $pdf->Text(27.0, 48.0, $data['postcode'].'                 '.$data['city']);

        //Telefonnummer
        if(empty($data['phone']) || $data['phone']=='')
        {
            if($i==0) $this->pdfRect(39.8, 57.9, 34, $pdf, 3);
        } else
            $pdf->Text(41.5, 60.4, $data['phone']);

        //eMail
        if(empty($data['email']) || $data['email']=='')
        {
            if($i==0) $this->pdfRect(27, 70, 53, $pdf, 3);
        }
        else 
            $pdf->Text(29.0, 72.4, $data['email']);

        #$pdf->Text(162, 80.4, $data['mobile']);

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);    

        	$pdf->Text(20, 96.8, $data['begin']);

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2); 

    // zu versichernde Person
            // setze marker bei Antragsteller / VN
            $pdf->Text(23.1, 124.2, $data['name2']);            
	     $pdf->Text(86.1, 124.2, $data['birthdate2']); 

        if($data['gender2']=='male') {
            $pdf->Text(111.0, 122.3, 'x');
        } else {
            $pdf->Text(111.0, 125.5, 'x');
        }
	 if($i==0) $this->pdfRect(140, 121.2, 37.5, $pdf, Y);
	    

	/* Seite verbergen */
	$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);



// SEITE 2
        $tplidx = $pdf->importPage(3, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);


	 switch($pdfTemplate)
	 {
		case project::gti('sdk-zahn100-zp1'):
			 $pdf->Text(110.4, 73.5, 'X');
    		        $pdf->Text(119.4, 73.5, $data['bonusbase']);
		break;

		case project::gti('sdk-zahn90-zp9'):
			 $pdf->Text(110.4, 68.0, 'X');
		        $pdf->Text(119.4, 68.0, $data['bonusbase']);
		break;

		case project::gti('sdk-zahn70-zp7'):
			 $pdf->Text(110.4, 62.9, 'X');
		        $pdf->Text(119.4, 62.9, $data['bonusbase']);
		break;

		case project::gti('sdk-zahn50-zp5'):
			 $pdf->Text(110.4, 57.5, 'X');
		        $pdf->Text(119.4, 57.5, $data['bonusbase']);
		break;

	 }


       $pdf->SetDrawColor(0);
	$pdf->Line(10, 270, 200, 80);


	/* Seite verbergen */
	$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);


// SEITE 3
        $tplidx = $pdf->importPage(4, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

	 $p = explode(',', $data['bonusbase']);
        $pdf->Text(185.4, 47.3, $p[0].'  '.$p[1]);

	 if($i==0) $this->pdfRect(151.8, 178.7, 10, $pdf, Y);
	 if($i==0) $this->pdfRect(164.5, 178.7, Y, $pdf, Y); 

        // Gesundheitsfragen Zahn
	 if(!empty($data['t1']))
	 {
		if($data['t1'] > 0)
	 	{
			$pdf->Text(154.4, 180.4, $data['t1']);
		} else 
		{
			$pdf->Text(164.7, 181.0, 'X');
	 	}
	 }


	 if($i==0) $this->pdfRect(151.8, 209.6, Y, $pdf, Y);
	 if($i==0) $this->pdfRect(164.5, 209.6, Y, $pdf, Y); 

	 // Beitragszuschlag
	 if($data['t1'] && $data['t1']>0) {
	 	$pdf->Text(154.7, 229.0, $data['t1'] * 20); 
		$pdf->Text(45.7, 236.0, 'Gesamtbeitrag inkl. Risikozuschlag für fehlende Zähne'); 
		$pdf->Text(153.7, 236.0, $data['price'].' €');
	 }

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+20, 0, 2);    

              if($i==0) $this->pdfRect(3.0, 174.6, 4, $pdf, 10);
	       if($i==0) $this->pdfRect(201.0, 174.6, 4, $pdf, 10);

        	$pdf->Text(4, 182.8, '!');
		$pdf->Text(202, 182.8, '!');

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2); 


	/* Seite verbergen */
	$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);

// SEITE 4
        $tplidx = $pdf->importPage(5, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

       $pdf->SetDrawColor(0);
	$pdf->Line(10, 270, 200, 60);


	/* Seite verbergen */
	$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);

// SEITE 5
        $tplidx = $pdf->importPage(6, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

       $pdf->SetDrawColor(0);
	$pdf->Line(10, 120, 200, 80);

	/* Seite verbergen */
	$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);

// SEITE 6
        $tplidx = $pdf->importPage(7, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

       $pdf->SetDrawColor(0);
	$pdf->Line(10, 270, 200, 60);

	/* Seite verbergen */
	$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);

// SEITE 7
        $tplidx = $pdf->importPage(8, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

       $pdf->SetDrawColor(0);
	$pdf->Line(10, 130, 200, 80);


	/* Seite verbergen */
	$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);

// SEITE 8
        $tplidx = $pdf->importPage(9, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

	 $pdf->Text(66.2, 15.8, 'X');
	 if($i==0) $this->pdfRect(94.8, 13.1, Y, $pdf, Y);
	 if($i==0) $this->pdfRect(110.2, 13.1, Y, $pdf, Y);

	 #$pdf->Text(18, 35, $data['city'].', '.date("d.m.Y"));
	 if($i==0) $this->pdfRect(18.3, 33.0, 38, $pdf, 4);
	 if($i==0) $this->pdfRect(70.3, 33.0, 70, $pdf, 4);



	 if($i==0) $this->pdfRect(19.3, 78.0, 60, $pdf, 4);
	 if($i==0) $this->pdfRect(19.3, 89.0, 60, $pdf, 4);
	 if($i==0) $this->pdfRect(92.3, 89.0, 20, $pdf, 4);
	 if($i==0) $this->pdfRect(19.3, 103.0, 60, $pdf, 4);
	 if($i==0) $this->pdfRect(19.3, 114.0, 40, $pdf, 4);
	 if($i==0) $this->pdfRect(70.3, 114.0, 40, $pdf, 4);

	 if($i==0) $this->pdfRect(19.3, 144.0, 40, $pdf, 4);
	 if($i==0) $this->pdfRect(70.3, 144.0, 60, $pdf, 4);

	/* Seite verbergen */
	$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);


// SEITE 9
        $tplidx = $pdf->importPage(10, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);


	 if($i==0) $this->pdfRect(19.3, 184.0, 37, $pdf, 4);
	 if($i==0) $this->pdfRect(65.3, 184.0, 60, $pdf, 4);

	 if($i==0) $this->pdfRect(19.3, 244.0, 37, $pdf, 4);
	 if($i==0) $this->pdfRect(65.3, 244.0, 60, $pdf, 4);


	/* Seite verbergen */
	$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);


// SEITE 10
        $tplidx = $pdf->importPage(11, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

	/* Seite verbergen */
	$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);


// SEITE 11
        $tplidx = $pdf->importPage(12, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

	/* Seite verbergen */
	$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);

// SEITE 12
        $tplidx = $pdf->importPage(13, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

	/* Seite verbergen */
	$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);

// SEITE 13
        $tplidx = $pdf->importPage(14, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        $pdf->Text(70, 13.0, $data['name']);
        $pdf->Text(70, 19.0, $data['birthdate']);

	 $pdf->Text(53, 28.3, $data['name2']);

	 if($i==0) $this->pdfRect(60.9, 189.8, 4, $pdf, 4);
	 #if($i==0) $this->pdfRect(85.5, 189.8, 4, $pdf, 4);

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+3, 0, 2); 

	 if($i==0) $this->pdfRect(4.0, 209.0, 11, $pdf, 4);
	 $pdf->Text(5, 212.3, 'oder..');	
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2); 

	 if($i==0) $this->pdfRect(60.9, 240.0, 4, $pdf, 4);
	 #if($i==0) $this->pdfRect(85.5, 240.0, 4, $pdf, 4);



	/* Seite verbergen */
	$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);

// SEITE 14
        $tplidx = $pdf->importPage(15, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

	 if($i==0) $this->pdfRect(60.9, 41.2, 4, $pdf, 4);
	 #if($i==0) $this->pdfRect(85.5, 40.8, 4, $pdf, 4);

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+3, 0, 2); 

	 if($i==0) $this->pdfRect(4.0, 48.0, 11, $pdf, 4);
	 $pdf->Text(5, 51.0, 'oder..');	
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2); 

	 if($i==0) $this->pdfRect(60.9, 55.4, 4, $pdf, 4);
	 #if($i==0) $this->pdfRect(85.5, 50.0, 4, $pdf, 4);



	/* Seite verbergen */
	$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);

// SEITE 15
        $tplidx = $pdf->importPage(16, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

	 if($i==0) $this->pdfRect(20, 110.4, 25, $pdf, 8);
	 if($i==0) $this->pdfRect(72, 110.4, 80, $pdf, 8);

	 if($age2 && $age2 < 18 && $age >=16)
	 {
		 if($i==0) $this->pdfRect(20, 137.7, 25, $pdf, 8);
		 if($i==0) $this->pdfRect(72, 137.7, 80, $pdf, 8);

		 if($i==0) $this->pdfRect(20, 154.7, 25, $pdf, 8);
		 if($i==0) $this->pdfRect(72, 154.7, 80, $pdf, 8);
        }

	/* Seite verbergen */
	$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);

// SEITE 16
        $tplidx = $pdf->importPage(17, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

	/* Seite verbergen */
	$this->pdfClean(198.0, 286.0, 10, $pdf, 255, 255, 255, 6);

if($complete == 2)
	$pdf->addPage('P');

