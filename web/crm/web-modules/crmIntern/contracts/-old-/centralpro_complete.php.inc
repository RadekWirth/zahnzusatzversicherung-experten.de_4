<?
// File for generating Contract PDF for CentralPro
		$fontSize = 8;
		$pdf->SetTopMargin(0);
		$pdf->addPage('P');
		$pdf->Image($path.'/files/central-prodent-1.png', 0, 0, 205);

    	#$this->getCompanyLogo(144.0, 23.5, $pdf, $data['contractComplete']['sourcePage'], $i, 40);
    	$this->getCompanyStamp(144.0, 34.0, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']-2);


		//$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		// set title and font to big and bold!
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $fontSize+5, 0, 2);
		
		if($i==0) $this->pdfRect(14.0, 15.0, 68, $pdf);
		$pdf->Text(14.5, 19.0, $titles[$i]);

		// reset font
		$pdfCfg['fontSize'] = $fontSize;
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		// Antrag Neu
		$pdf->Text(15.1, 42.6, 'X');

		// Antragsdatum
		if($i==0) $this->pdfRect(36.5, 44.0, 60.0, $pdf);


		$pdf->Text(16.0, 68.5, $data['name']);
 		$pdf->Text(16.0, 79.0, $data['street']);
 		$pdf->Text(16.0, 102.0, $data['birthdate']);

		if(empty($data['phone']))
		{
			if($i==0) $this->pdfRect(139.0, 54.5, 45.0, $pdf);
		}
 		else 
			$pdf->Text(140.0, 58.5, $data['phone']);

 		$pdf->Text(107.0, 102.0, $data['email']);

 		$pdf->Text(16.0, 90.5, 'DE');
 		$pdf->Text(30.0, 90.5, $data['postcode']);
 		$pdf->Text(59.0, 90.5, $data['city']);

 		if($data['gender']=='male')	{
			$pdf->Text(46.6, 101.2, 'X');
		}
		else {
			$pdf->Text(52.7, 101.2, 'X');
		}

		$pdf->Text(59.0, 102.0, $data['nation']);

		if(empty($data['job']))
		{
			if($i==0) $this->pdfRect(131.0, 109.0, 45.0, $pdf);
		}
 		else 
			$pdf->Text(132.0, 112.0, $data['job']);

		// Arbeitnehmer
		if($i==0) $this->pdfRect(16.0, 108.4, 3, $pdf, 3);

		// Selbständiger
		if($i==0) $this->pdfRect(43.4, 108.4, 3, $pdf, 3);

		// nicht erwerbstätig
		if($i==0) $this->pdfRect(72.1, 108.4, 3, $pdf, 3);

		// in Ausbildung
		if($i==0) $this->pdfRect(105.5, 108.4, 3, $pdf, 3);


	// A Zu versichernde Person(en)
		// person insured
 		$pdf->Text(28.5, 142.0, $data['name2']);
 		$pdf->Text(132.0, 142.0, substr($data['birthdate2'], 0 , 2));
 		$pdf->Text(152.0, 142.0, substr($data['birthdate2'], 3 , 2));
 		$pdf->Text(172.0, 142.0, substr($data['birthdate2'], 6 , 4));
 		$pdf->Text(15.5, 150.0, $data['nation2']);
 		$pdf->Text(132.5, 158.5, $data['job2']);
 		if($data['gender2']=='male')	{
			$pdf->Text(113.2, 150.3, 'X');
		}else {
			$pdf->Text(123.5, 150.3, 'X');
		}

		// handle begin dates, mm.YYYY
		// wished begin, month 2 digits
		$pdf->Text(136.0, 151.0, substr($data['begin'], 0, 2));

		// wished begin, year 2 digits
		$pdf->Text(167.0, 151.0, substr($data['begin'], -2));


		// Arbeitnehmer
		if($i==0) $this->pdfRect(16.0, 154.0, 3, $pdf, 3);

		// Selbständiger
		if($i==0) $this->pdfRect(43.4, 154.5, 3, $pdf, 3);

		// nicht erwerbstätig
		if($i==0) $this->pdfRect(72.1, 154.0, 3, $pdf, 3);

		// in Ausbildung
		if($i==0) $this->pdfRect(105.5, 154.5, 3, $pdf, 3);

	// B Abbuchungserlaubnis
		if($i==0) $this->pdfRect(15.0, 234.5, 170, $pdf);

		if($i==0) $this->pdfRect(15.0, 243.8, 170, $pdf);

	// Seite 2
		$pdf->addPage('P');
		$pdf->Image($path.'/files/central-prodent-2.png', 0, 0, 205);

	// C Gesundheitsfrage
		if($i==0) $this->pdfRect(134.3, 44.4, 4, $pdf);
		if($i==0) $this->pdfRect(142.5, 44.0, 7, $pdf);

		// toothes
		if($data['t1']==0) {
			$pdf->Text(135.0, 48.3, 'X');
		}
		$pdf->Text(143.0, 48.5, $data['t1']);

	// Bereich D
		// prices
		if($data['idt']==project::gti('centralpro')) {
			$pdf->Text(90.0, 78.0, $data['bonusbase']);
			$pdf->Text(90.0, 86.5, $data['bonus']);
			$pdf->Text(130.0, 94.5, $data['price']);
		}

	// Bereich E
		// War noch nicht versichert
		if($i==0) $this->pdfRect(31.5, 124.5, 22.5, $pdf, 7.5);

		$pdf->Text(35.3, 129.6, 'X');

	// Bereich F
		// Ort, Datum
		if($i==0) $this->pdfRect(14.0, 174.5, 26 , $pdf);
	
		// Unterschrift
		if($i==0) $this->pdfRect(48.0, 174.5, 120, $pdf);

		// autogram normal
		$pdf->SetFont($pdfCfg['fontFamily'], '', 13, 0, 2);
		$pdf->Text(48.5, 178.5, 'X');


	// Bereich G
		// reset font
		$pdfCfg['fontSize'] = $fontSize;
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		// empfangsbestaetigung
		$pdf->Text(16.0, 201.5, 'X');
		$pdf->Text(16.0, 212.4, 'X');
		$pdf->Text(16.0, 220.0, 'X');

		// Ort, Datum, Unterschrift
		if($i==0) $this->pdfRect(14.0, 226.5, 80 , $pdf);
	
		// Unterschrift des Betreuers nicht notwendig
		//if($i==0) $this->pdfRect(108.0, 226.5, 80, $pdf);

		$pdf->SetFont($pdfCfg['fontFamily'], '', 13, 0, 2);
		$pdf->Text(15.0, 231.0, 'X');

		// reset font
		$pdfCfg['fontSize'] = $fontSize;
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		// Name
		$pdf->Text(14.5, 246.0, 'Maximilian Waizmann');

		// Stellennummer
		$pdf->Text(153.0, 259.0, '263448');


	// Seite 3
		$pdf->addPage('P');
		$pdf->Image($path.'/files/central-prodent-3.png', 0, 0, 205);


	// Bereich II
		if($i==0) $this->pdfRect(145.0, 267.8, 3.0, $pdf, 3.0);
		if($i==0) $this->pdfRect(169.8, 267.8, 3.0, $pdf, 3.0);

	if($complete==2) { $pdf->AddPage('P'); }

?>