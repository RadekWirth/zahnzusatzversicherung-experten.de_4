<?php
	// Pr�fung, ob Datei vorhanden ist...
    if ($i!=1) {
        $pagecount = $pdf->setSourceFile($path.'/files/stuttgarter/antrag/stuttgarter-zahn-premium-komfort-antrag-01-2016.pdf');
    } else {
        $pagecount = $pdf->setSourceFile($path.'/files/stuttgarter/antrag/stuttgarter-zahn-premium-komfort-antrag-01-2016-bw.pdf');
    }
	

    $age = actionHelper::getAge($data['personInsured']['birthdate']);

/**
 * Page 1
 */
	$tplidx = $pdf->importPage(1, '/MediaBox');
	$pdf->addPage('P');
	$pdf->useTemplate($tplidx, 0, 0.1, 209.9);
	$this->pdfClean(10.0, 3.0, 40.0, $pdf, 255, 255, 255, 7.0);
	$this->pdfClean(45.0, 5.0, 140.0, $pdf, 255, 255, 255, 11.0);

	$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-1, 0, 2);
		$pdf->setXY(43.0, 12.4);
		$pdf->Write(5, 'Antrag auf Zahnzusatzversicherung');
	
	$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);
		$pdf->Write(5, ' f�r gesetzlich Krankenversicherte bei der Stuttgarter Versicherung AG');
	
	// set title and font to big and bold!
	$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);

		if($i==0) $this->pdfRect(10.0, 1.0, 54, $pdf, 4.0);
			$pdf->Text(11.0, 4.0, $titles[$i]);

    $this->getCompanyStamp(116.0, 5, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']-2, 'horizontal', 'wide');

		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);


		if($data['gender']=='female') {
			$pdf->Text(19.3, 25.5, "x");
		} else {
			$pdf->Text(11.0, 25.5, "x");
		}

		// Versicherte Leistungen
		if($pdfTemplate == project::gti('stuttgarter-komfort')) {
                $pdf->Text(154.6, 23.9, 'X');
        }
        if($pdfTemplate == project::gti('stuttgarter-premium')) {
                $pdf->Text(177.4, 23.9, 'X');
        }


		$pdf->Text(13.0, 29.7, $data['name']);
		$pdf->Text(13.0, 37.0, $data['street']);
		
		$pdf->Text(13.0, 45.2, $data['postcode']);
		$pdf->Text(36.0, 45.2, $data['city']);

		$pdf->Text(13.0, 52.3, $data['birthdate']);
		$pdf->Text(36.0, 52.3, $data['nation']);

		if(!empty($data['job'])) {
			$pdf->Text(57.0, 52.3, $data['job']);
		} else
			if($i==0) $this->pdfRect(56.0, 49.5, 44.0, $pdf, 3.5);

		if(!empty($data['phone'])) {
			$pdf->Text(13.0, 61.0, $data['phone']);
		} else
			if($i==0) $this->pdfRect(11.0, 58.0, 42.0, $pdf, 3.5);

		if(!empty($data['email'])) {
			$pdf->Text(57.0, 61.0, $data['email']);
		} else
			if($i==0) $this->pdfRect(56.0, 58.0, 44.0, $pdf, 3.5);



		// Zu versichernde Person
		if($data['personInsured']['gender']=='female') {
			$pdf->Text(19.3, 82.3, "x");
		} else {
			$pdf->Text(11.0, 82.3, "x");
		}

		$pdf->Text(13.0, 86.7, $data['name2']);
				
		$pdf->Text(13.0, 95.0, $data['birthdate2']);
		$pdf->Text(36.0, 95.0, $data['nation2']);

		if(!empty($data['job2'])) {
			$pdf->Text(57.0, 95.0, $data['job2']);
		} else
			if($i==0) $this->pdfRect(56.0, 93.0, 44.0, $pdf, 3.5);




		// Beginn der Versicherung			
		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+3, 0, 2);
			$begin = explode(".", $data['begin']);
			$pdf->Text(51, 100.5, '01');
			$pdf->Text(61, 100.5, $begin[0]);
			$pdf->Text(75, 100.5, $begin[1]);
		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);


		// Zahlweise
		if($i==0) $this->pdfRect(31.3, 110.5, 2.0, $pdf, 2.0);
		if($i==0) $this->pdfRect(46.5, 110.5, 2.0, $pdf, 2.0);
		if($i==0) $this->pdfRect(31.3, 113.7, 2.0, $pdf, 2.0);
		if($i==0) $this->pdfRect(46.5, 113.7, 2.0, $pdf, 2.0);


		// Bank
		if($i==0) $this->pdfRect(17.0, 166.0, 48.0, $pdf, 3.0);
		if($i==0) $this->pdfRect(71.5, 166.0, 89.0, $pdf, 3.0);
		if($i==0) $this->pdfRect(172.0, 166.0, 26.0, $pdf, 3.0);

		// Unterschrift
		if($i==0) $this->pdfRect(112.5, 178.0, 24.0, $pdf, 3.0);
		if($i==0) $this->pdfRect(145.5, 178.0, 50.0, $pdf, 3.0);



		if($i==0) $this->pdfRect(188.1, 125.9, 2.5, $pdf, 2.5);
		if($i==0) $this->pdfRect(193.0, 126.0, 5.5, $pdf, 2.5);
		if($i==0) $this->pdfRect(180.4, 125.9, 2.5, $pdf, 2.5);
		if($i==0) $this->pdfRect(188.1, 133.9, 2.5, $pdf, 2.5);
		if($i==0) $this->pdfRect(193.0, 134.0, 5.5, $pdf, 2.5);
		if($i==0) $this->pdfRect(180.4, 133.9, 2.5, $pdf, 2.5);

		// Zahnzustand (vorerst deaktiviert)
		/*
		if($data['contractComplete']['tooth1']>0)
		{
			$pdf->Text(188.6, 128.1, "X");
			$pdf->Text(195.3, 128.0, $data['contractComplete']['tooth1']);
		} else {
			$pdf->Text(180.9, 128.1, "X");			
		}

		if($data['contractComplete']['tooth3'] + $data['contractComplete']['tooth4'] >0)
		{
			$pdf->Text(188.6, 136.2, "X");
			$pdf->Text(195.3, 136.1, $data['contractComplete']['tooth3'] + $data['contractComplete']['tooth4']);
		} else {
			$pdf->Text(180.9, 136.2, "X");
		}
		*/


		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);
		#$this->pdfClean(72.0, 239.5, 14.0, $pdf, 255, 255, 255, 3.5);
		$pdf->Text(85.0, 114.0, $data['price']);

		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);


		if($pdfTemplate == project::gti('stuttgarter-premium')) 
		{
			$left = 22.9;
		} else $left = 0;


		if($age < 22) 
		{
			$pdf->Text(160.3 + $left, 98.5, "X");
		}
		else if($age < 31)
		{
			$pdf->Text(160.3 + $left, 109.8, "X");
		}
		else if($age < 41)
		{
			$pdf->Text(160.3 + $left, 113.7, "X");
		}
		else if($age < 51)
		{
			$pdf->Text(160.3 + $left, 117.5, "X");
		}
		else if($age <100)
		{
			$pdf->Text(160.3 + $left, 121.3, "X");
		}


/**
 * Page 2
 */
	$tplidx = $pdf->importPage(2, '/MediaBox');

	$pdf->addPage('P');
	$pdf->useTemplate($tplidx, 0, 0.1, 209.9);


/**
 * Page 3
 */
	$tplidx = $pdf->importPage(3, '/MediaBox');

	$pdf->addPage('P');
	$pdf->useTemplate($tplidx, 0, 0.1, 209.9);

	if($i==0) $this->pdfRect(12.6, 197.5, 50.0, $pdf, 3.0);
	if($i==0) $this->pdfRect(85.0, 197.5, 80.0, $pdf, 3.0);

	if($i==0) $this->pdfRect(12.6, 230.7, 40.0, $pdf, 3.0);
	if($i==0) $this->pdfRect(62.0, 230.7, 40.0, $pdf, 3.0);
	if($i==0) $this->pdfRect(108.0, 230.7, 40.0, $pdf, 3.0);

/**
 * Page 4
 */
	$tplidx = $pdf->importPage(4, '/MediaBox');

	$pdf->addPage('P');
	$pdf->useTemplate($tplidx, 0, 0.1, 209.9);

?>
