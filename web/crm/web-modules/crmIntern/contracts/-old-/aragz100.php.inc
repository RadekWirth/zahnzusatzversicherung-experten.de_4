<?
// File for generating Contract PDF for ARAGz90

		$fontSize = 8;
		$pdf->addPage('P');
		$pdf->Image($path.'/files/contract_arag_z90_page_1.png', 0.1, 10, 200);
		
		if($i==0) $this->pdfRect(28, 6, 68, $pdf);
		// set title and font to big and bold!
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $fontSize+5, 0, 2);
		$pdf->Text(28.0, 10.0, $titles[$i]);

		$pdf->Text(38.0, 62.5, $data['begin']);
		
	// reset font
		$pdfCfg['fontSize'] = $fontSize;
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		// Makler Daten schreiben
		$pdf->Text(39.0, 33.0, "5");
		$pdf->Text(43.0, 33.0, "1");
		$pdf->Text(47.0, 33.0, "7");
		$pdf->Text(51.0, 33.0, "3");
		$pdf->Text(55.0, 33.0, "0");

		$pdf->Text(69.0, 33.0, "6");
		$pdf->Text(73.0, 33.0, "0");
		$pdf->Text(77.0, 33.0, "0");
	
	// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-2, 0, 2);

   		$this->getCompanyStamp(128, 27, $pdf, $data['contractComplete']['sourcePage'], 8);

	// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		// Neuantrag
		$pdf->Text(28.7, 39.7, "X");

		$pdf->Text(38.0, 75.5, $data['name']);
 		$pdf->Text(30.0, 84.5, $data['street']);
 		$pdf->Text(140.0, 75.5, $data['birthdate']);

		if(empty($data['phone'])) 
		{ 
			if($i==0) $this->pdfRect(140, 89.5, 40, $pdf);
 		} else 
			$pdf->Text(140.0, 93.0, $data['phone']);

		if(empty($data['email'])) 
		{ 
			if($i==0) $this->pdfRect(30, 98, 68, $pdf);
		} else $pdf->Text(30.0, 101.5, $data['email']);

 		$pdf->Text(30.0, 93.0, $data['postcode']);
 		$pdf->Text(50.0, 93.0, $data['city']);

		if(empty($data['job'])) 
		{ 
			if($i==0) $this->pdfRect(49.0, 107.0, 40, $pdf);
 		} else 
			$pdf->Text(50.0, 110.0, $data['job']);
 		

 		if($data['gender']=='male')	{
			$pdf->Text(30.0, 72.0, 'X');
		}
		else {
			$pdf->Text(30.0, 76.0, 'X');
		}

		if($data['nation']=='DEU') {
			$pdf->Text(171.0, 75.5, $data['nation']);
		}

		if($data['married']) {
			$pdf->Text(184.0, 76.0, 'X');
		}
		else {
			$pdf->Text(184.0, 72.0, 'X');
		}

		//mark Angestellter / Selbst�ndiger
		if($i==0) $this->pdfRect(29.5, 104.6, 2, $pdf, 2);
 		if($i==0) $this->pdfRect(29.5, 108.2, 2, $pdf, 2);

	//Beitragszahlung
		//ich w�nsche die widerrufliche Abbuchung...
		if($i==0) $this->pdfRect(29.5, 117.2, 2, $pdf, 2);		

		//zum 01. eines Monats
		if($i==0) $this->pdfRect(122.8, 117.3, 2, $pdf, 2);

		//zum 15. eines Monats
		if($i==0) $this->pdfRect(154.4, 117.4, 2, $pdf, 2);

		//Geldinstitut
		if($i==0) $this->pdfRect(30, 123.5, 80, $pdf);

		//Name Kontoinhaber
		if($i==0) $this->pdfRect(124, 123.5, 60, $pdf);

		//Kontonummer
		if($i==0) $this->pdfRect(30, 132.0, 55, $pdf);

		//Bankleitzahl
		if($i==0) $this->pdfRect(92, 132.0, 27, $pdf);
		
		//Unterschrift des Kontoinhabers
		if($i==0) $this->pdfRect(124, 132.0, 60, $pdf);


	//Zahlungsweise
		//j�hrlich (4% skonto)
		if($i==0) $this->pdfRect(29.5, 145.8, 2, $pdf, 2);

		//halbj�hrlich (2% skonto)
		if($i==0) $this->pdfRect(61.0, 145.8, 2, $pdf, 2);

		//viertelj�hrlich
		if($i==0) $this->pdfRect(94.7, 145.8, 2, $pdf, 2);

		//monatlich
		if($i==0) $this->pdfRect(116.4, 145.8, 2, $pdf, 2);

	//Zu versichernde Person 1
		// person insured
		$pdf->Text(30.0, 168.0, $data['street'].', '.
			$data['postcode'].' '.$data['city']);

		//mark Angestellter / Selbst�ndiger
		if($i==0) $this->pdfRect(29.5, 171.2, 2, $pdf, 2);
 		if($i==0) $this->pdfRect(29.5, 175.0, 2, $pdf, 2);

		$pdf->Text(38.0, 160.0, $data['name2b']);
		$pdf->Text(140.0, 160.0, $data['birthdate2']);

		if(empty($data['job2'])) 
		{ 
			if($i==0) $this->pdfRect(50.0, 173.5, 40, $pdf);
 		} else 
			$pdf->Text(51.0, 177.5, $data['job2']);


 		if($data['gender2']=='male')	{
			$pdf->Text(29.7, 156.0, 'X');
		}
		else {
			$pdf->Text(29.7, 160.0, 'X');
		}

		if($data['nation2']=='DEU') {
			$pdf->Text(171.0, 160.0, $data['nation2']);
		}

		if($data['married2']) {
			$pdf->Text(184.0, 160.0, 'X');
		}
		else {
			$pdf->Text(184.0, 156.0, 'X');
		}

		// prices
		//if($data['idt']==project::gti('aragz90')) {
			$pdf->Text(42.6, 245.1, "X");
			$pdf->Text(120.0, 249.5, $data['bonusbase']);
			$pdf->Text(155.5, 249.5, $data['bonusEuroFromPercent']);
			$pdf->Text(180.0, 281.5, $data['price']);
		//}


	// page 2
		$pdf->AddPage('P');
		$pdf->Image($path.'/files/contract_arag_z90_page_2.png', 0.1, 10, 200);
		$pdf->Text(42.3, 46.0, "X");

		if(empty($data['insurance2'])) 
		{ 
			if($i==0) $this->pdfRect(52.0, 44.0, 40, $pdf);
 		} else 
			$pdf->Text(54.0, 48.0, $data['insurance2']);



		//2a, Person 1
		if($i==0) $this->pdfRect(148.7, 75.5, 10, $pdf);
		if($data['incare']) {
			$pdf->Text(150.2, 79.0, 'X');
		}
		else {
			$pdf->Text(155.2, 79.0, 'X');
		}

		//2b Person 1
		if($i==0) $this->pdfRect(44.5, 93.0, 2, $pdf, 2);
		if($i==0) $this->pdfRect(148.5, 100.0, 10, $pdf);

		//2b Anzahl
		if($i==0) $this->pdfRect(56.0, 95.0, 8, $pdf, 4);

		$tsum = $data['t1'] + $data['t2'];
		$pdf->Text(58.0, 98.0, $tsum);

		if($tsum > 0) {
			$pdf->Text(150.5, 103.3, 'X'); // b, insure toothes, yes
		}
		else {
			$pdf->Text(155.4, 103.3, 'X'); // b, insure toothes, no
			$pdf->Text(44.5, 95.0, 'X'); // b, no toothes
		}

	//Unterschriften
		//Ort,Datum
		if($i==0) $this->pdfRect(30, 264.0, 45, $pdf);

		//Antragsteller
		if($i==0) $this->pdfRect(81, 264.0, 80, $pdf);

		//1. zu versichernde vollj�hrige Person
		if($i==0) $this->pdfRect(30, 276.5, 45, $pdf);

		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $fontSize+5, 0, 2);

			$pdf->Text(30.5, 268.0, 'X');
			$pdf->Text(30.5, 280.0, 'X');
			$pdf->Text(82.5, 268.0, 'X');
		
		$pdf->SetFont($pdfCfg['fontFamily'], '', $fontSize, 0, 2);

	// page 3,4
		$pdf->Image($path.'/files/contract_arag_z90_page_3.png', null, null, 200);
		$pdf->Image($path.'/files/contract_arag_z90_page_4.png', null, null, 200);
?>