<?
    // Prüfung, ob Datei vorhanden ist...
    if ($i!=1) {
        $pagecount = $pdf->setSourceFile($path.'/files/rv/antrag/2020/rv_antrag_2020-2.pdf');
    } else {
        $pagecount = $pdf->setSourceFile($path. '/files/rv/antrag/2020/rv_antrag_2020-2.pdf');
    }
    $tplidx = $pdf->importPage(1, '/MediaBox');

/**
 * Page 1
 */
    $pdf->addPage('P');

    $pdf->useTemplate($tplidx, -3.6, 0, 210, 297, true);
    #$this->pdfClean(30.0, 2.0, 160.0, $pdf, 215, 255, 255, 4.0);

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);

        if($i==0) $this->pdfRect(10.0, 1.0, 54, $pdf, 4.0);
            $pdf->Text(11.0, 4.0, $titles[$i]);

        $this->getCompanyStamp(150, 2, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']-2.5);

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        $pdf->SetMargins(7.0, 9.0);
        $pdf->SetY(46.5);

        $pdf->Write(3.5, '4  1  4');

        $pdf->SetX(95.2);
        $pdf->Write(3.5, '8   0  4  4   9  2');

	 $pdf->SetX(148.0);
	 $pdf->Write(3.5, $data['nation']);
        #$pdf->Text(154.0, 51.9, $data['nation']);

	 $pdf->SetY(53.4); $pdf->SetX(7.0);
	 $pdf->Write(3.5, $data['name']);

        if(!empty($data['phone'])) {
            $pdf->Text(154.0, 56.6, $data['phone']);
        } else
            if($i==0) $this->pdfRect(150.0, 53.2, 44.0, $pdf, 3.5);

        $pdf->Text(9.0, 63.8, $data['street']);
        $pdf->Text(164.0, 63.8, $data['birthdate']);

        $pdf->Text(9.0, 70.4, $data['postcode']);
        $pdf->Text(36.0, 70.4, $data['city']);

    // Angaben zu den versichernden Personen / Versicherungsumfang
        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+3, 0, 2);
            $begin = explode(".", $data['begin']);
            $pdf->Text(72.2, 93.0, $begin[0]);
            $pdf->Text(79.2, 93.0, $begin[1]);

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

        if(!empty($data['personInsured'])) {

            $pdf->Text(54.0, 109.2, $data['name2']);
            $pdf->Text(54.0, 114.6, $data['birthdate2']);

            if(!empty($data['job2'])) {
                $pdf->Text(54.0, 119.0, $data['job2']);
            } else
                if($i==0) $this->pdfRect(53.0, 116.2, 40.0, $pdf, 3.5);

            if($data['gender2']=='female') {
                $pdf->Text(104.9, 114.0, "x");
            } else {
                $pdf->Text(111.6, 114.0, "x");
            }

        } else {
            $pdf->Text(54.0, 109.2, $data['nameb']);
            $pdf->Text(54.0, 114.6, $data['birthdate']);


            if(!empty($data['job'])) {
                $pdf->Text(54.0, 119.0, $data['job']);
            } else
                if($i==0) $this->pdfRect(53.0, 116.2, 40.5, $pdf, 3.5);

            if($data['gender']=='female') {
                $pdf->Text(104.9, 114.0, "x");
            } else {
                $pdf->Text(111.6, 114.0, "x");
            }
        }


        $pdfTemplate = isset($pdfTemplate)?$pdfTemplate:$data['idt'];
        switch($pdfTemplate) {
            case project::gti('rv-classic-z3u-zv'):
                $pdf->Text(100, 188.0, "X");
                $pdf->Text(100, 203.3, "X");
                break;  
            case project::gti('rv-comfort-z2zv'):
		  $pdf->Text(100, 193.0, "X");
                $pdf->Text(100, 203.3, "X");
                break;
            case project::gti('rv-premium-z1zv'):
                $pdf->Text(100, 198.2, "X");
                $pdf->Text(100, 203.3, "X");
                break;
            case project::gti('rv-premium-z1'):
                $pdf->Text(98, 198.2, "X");
                break;
            case project::gti('rv-comfort-z2'):
                $pdf->Text(98, 193.0, "X");
                break;
            case project::gti('rv-classic-z3u'):
                $pdf->Text(98, 188.0, "X");
                break;
            case project::gti('rv-zv'):
                $pdf->Text(100, 203.3, "X");
                break;
            }

    $this->pdfClean(97.6, 272.0, 16.0, $pdf, 255, 255, 255, 3.4);
    $this->pdfClean(175.9, 277.0, 16.0, $pdf, 255, 255, 255, 3.4);

    $pdf->Text(99.7, 275.0, $data['price']);
    $pdf->Text(176.7, 280.0, $data['price']);

$this->pdfClean(184, 290, 14.0, $pdf, 255, 255, 255, 4.0);

/**
 * Page 2
 */
    $tplidx = $pdf->importPage(2, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 210, 297, true);

    if ($i == 0) $this->pdfRect(183.8, 51.8, 2.0, $pdf, 2.0);
    if ($i == 0) $this->pdfRect(196.2, 51.8, 2.0, $pdf, 2.0);

    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'] + 12, 0, 2);
    if ($i == 0) $this->pdfRect(2.0, 50.1, 5.0, $pdf, 10.0);
    $pdf->Text(3.0, 57.2, '!');
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);


    #Ort, Datum, Unterschriften
    if($i==0) $this->pdfRect(12.4, 136.9, 47.0, $pdf, 3.0);
    if($i==0) $this->pdfRect(67.3, 136.9, 71.0, $pdf, 3.0);
    if($i==0) $this->pdfRect(152, 136.9, 46.0, $pdf, 3.0);

    if($i==0) $this->pdfRect(12.4, 152.1, 55, $pdf, 3.0);
    if($i==0) $this->pdfRect(110.3, 152.1, 55, $pdf, 3.0);

    #IBAN
    if($i==0) $this->pdfRect(11.4, 194.5, 154, $pdf, 3.0);
    if($i==0) $this->pdfRect(10.2, 202.5, 2.8, $pdf, 2.8);
    if($i==0) $this->pdfRect(10.2, 207.5, 2.8, $pdf, 2.8);

    #Name, ...
    if($i==0) $this->pdfRect(11.4, 224.0, 104.0, $pdf, 3.0);    
    if($i==0) $this->pdfRect(11.4, 230.3, 104.0, $pdf, 3.0);
    if($i==0) $this->pdfRect(11.4, 236.8, 104.0, $pdf, 3.0);
    if($i==0) $this->pdfRect(11.4, 243.3, 12.0, $pdf, 3.0); 
    if($i==0) $this->pdfRect(30.4, 243.3, 20.0, $pdf, 3.0);
    if($i==0) $this->pdfRect(60.4, 243.3, 100.0, $pdf, 3.0);

    #Ort, ...
    if($i==0) $this->pdfRect(11.4, 268.2, 40.0, $pdf, 3.0);
    if($i==0) $this->pdfRect(60.4, 268.2, 30.0, $pdf, 3.0);
    if($i==0) $this->pdfRect(98.4, 268.2, 100.0, $pdf, 3.0);

// Hide pagenum
$this->pdfClean(187, 290, 14.0, $pdf, 255, 255, 255, 4.0);



/**
 * Page 3
 */
    $tplidx = $pdf->importPage(3, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 210, 297, true);


    #Empfangsbest tigung
    $pdf->Text(122.2, 31.2, 'X');
    $pdf->Text(161.1, 31.0, '0  4   1  8');
    
    if($i==0) $this->pdfRect(10.4, 45.5, 49.0, $pdf, 3.0);
    if($i==0) $this->pdfRect(66.4, 45.5, 110.0, $pdf, 3.0);

// Hide pagenum
$this->pdfClean(187, 290, 14.0, $pdf, 255, 255, 255, 4.0);

/**
 * Page 4
 */
    $tplidx = $pdf->importPage(4, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 210, 297, true);

    $pdf->SetDrawColor(0, 0, 0);
    $pdf->SetLineWidth(1);
    $pdf->Line(10, 280, 200, 10);
$this->pdfClean(187, 290, 14.0, $pdf, 255, 255, 255, 4.0);


/**
 * Page 5
 */
    $tplidx = $pdf->importPage(5, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 210, 297, true);
$this->pdfClean(187, 290, 14.0, $pdf, 255, 255, 255, 4.0);


/**
 * Page 6
 */
    $tplidx = $pdf->importPage(6, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 210, 297, true);
$this->pdfClean(187, 290, 14.0, $pdf, 255, 255, 255, 4.0);

/**
 * Page 7
 */
    $tplidx = $pdf->importPage(7, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 210, 297, true);
$this->pdfClean(187, 290, 14.0, $pdf, 255, 255, 255, 4.0);
