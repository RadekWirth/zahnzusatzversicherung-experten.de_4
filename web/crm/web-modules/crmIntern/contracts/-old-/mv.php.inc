<?
$arrOccupations = array(
    'employed' => array(
        'angestellte',
        'angestellter',
        'büroangestellte'
    ),
    'selfEmployed' => array(
        'selbstständig'
    ),
    'notEmployed' => array(
        'hausfrau',
        'rentner',
        'rentnerin'
    )
);

// File for generating Form Münchener Verein

// Variablen
$black          = '0,0,0';
$lineWidth      = 0.3;
$fontSizeNormal = 10;
$fontSizeBigger = 12;
$fontSizeSmall  = 8;

// Versicherungsalter
$yearOfBirth          = substr($data['birthdate'], -4);
$yearOfInsuranceBegin = substr($data['begin'], -4);
$insuranceAge         = $yearOfInsuranceBegin - $yearOfBirth;


define('BULLET', chr(149));


// Page 1 -----------------------------------------------------------------------------------------

// Reset
$pdf->SetRightMargin(0);
$pdf->SetLeftMargin(12);
$pdf->SetY(0);
$pdf->SetTextColor(10);

$pdf->addPage('P');

$pdf->SetDrawColor($black);
$pdf->SetLineWidth($lineWidth);

// Logos
$this->imgZZVLogo($i, $pdf, 140, 6, 60);
$this->imgLogo($i, $pdf,'mv/logos/100', 13, 3, 60);


// Versicherungsnehmer

$y = 44;
$pdf->SetY($y);
$this->contractInfoTitle($i, $pdf, $titles[$i]);
$this->companyStamp(82, 9, $pdf);
$pdf->SetY($y=$y+10);
$pdf->SetFont('Arial', '', 16);
$pdf->Text(14, 42, 'Antrag Deutsche Zahnversicherung (DZV)');
$pdf->SetFont('Arial', 'B', $fontSizeBigger);
$pdf->Cell(0, 10, 'Antragsteller', 0, 1);



// Geschlecht
$pdf->SetFont('Arial', '', $fontSizeSmall);
$pdf->Text(61, $y + 15, 'männlich');
$pdf->Text(86, $y + 15, 'weiblich');
$pdf->SetFont('Arial', '', $fontSizeNormal);
if ($data['gender'] == 'male') {
    $pdf->Text(56.8, $y + 15, 'X');
} else {
    $pdf->Text(81.8, $y + 15, 'X');
}
$pdf->Cell(0, 8, 'Geschlecht', 0, 1);
$pdf->Rect(56, $y + 12, 3.5, 3.5, $pdf);
$pdf->Rect(81, $y + 12, 3.5, 3.5, $pdf);

// Name
$pdf->Text(54, $y + 23, $data['person']['surname'] . ' ' . $data['person']['forename']);
$pdf->Cell(0, 8, 'Name, Vorname', 0, 1);
$pdf->Line(52, $y + 24, 100, $y + 24);

// Telefon
if ($data['phone']) {
    $pdf->Text(160, $y + 23, $data['phone']);
} else {
    if ($i == 0)
        $this->pdfRect(156, $y + 17.3, 44, $pdf, 6);
}
$pdf->Text(106, $y + 23, 'Telefon privat');
$pdf->SetFont('Arial', '', $fontSizeSmall);
$pdf->Text(129, $y + 23, '(freiwillige Angabe)');
$pdf->SetFont('Arial', '', $fontSizeNormal);
$pdf->Line(156, $y + 24, 200, $y + 24);

// Strasse
$pdf->Text(54, $y + 31, $data['street']);
$pdf->Cell(0, 8, 'Straße, Hausnummer', 0, 1);
$pdf->Line(52, $y + 32, 100, $y + 32);

// Mobil
if ($data['mobile']) {
    $pdf->Text(160, $y + 31, $data['mobile']);
} else {
    if ($i == 0)
        $this->pdfRect(156, $y + 26, 44, $pdf, 6);
}
$pdf->Text(106, $y + 31, 'Telefon Handy');
$pdf->SetFont('Arial', '', $fontSizeSmall);
$pdf->Text(130, $y + 31, '(freiwillige Angabe)');
$pdf->SetFont('Arial', '', $fontSizeNormal);
$pdf->Line(156, $y + 32, 200, $y + 32);

// Postleitzahl - Wohnort
$pdf->Text(54, $y + 39, $data['postcode'] . ' ' . $data['city']);
$pdf->Cell(0, 8, 'PLZ / Wohnort', 0, 1);
$pdf->Line(52, $y + 40, 100, $y + 40);

// Geburtsdatum
$pdf->Text(54, $y + 47, $data['birthdate']);
$pdf->Cell(0, 8, 'Geburtsdatum', 0, 1);
$pdf->Line(52, $y + 48, 100, $y + 48);

// Beruf
$pdf->SetFont('Arial', '', $fontSizeSmall);
$pdf->Text(81, $y + 62, 'Selbstständig');
$pdf->Text(111, $y + 62, 'Arbeitnehmer / in');
$pdf->Text(146, $y + 62, 'Nicht erwerbstätig');
$pdf->SetFont('Arial', '', $fontSizeNormal);
$pdf->Text(78, $y + 54.5, $data['job']);
$pdf->Cell(0, 8, 'derzeitige Tätigkeit / ausgeübter Beruf', 0, 1);
if (empty($data['job'])) {
    if ($i == 0)
        $this->pdfRect(76, $y + 57, 124, $pdf, 6);
}
$pdf->Line(76, $y + 56, 200, $y + 56);
$pdf->Rect(76, $y + 59, 3.5, 3.5);
$pdf->Rect(106, $y + 59, 3.5, 3.5);
$pdf->Rect(140, $y + 59, 3.5, 3.5);
$strJob = strtolower($data['job']);
$strJob = trim($strJob);
if ($i == 0)
    $this->pdfRect(76.4, $y + 59, 3.5, $pdf, 3.5);
if ($i == 0)
    $this->pdfRect(106, $y + 59, 3.5, $pdf, 3.5);
if ($i == 0)
    $this->pdfRect(140, $y + 59, 3.5, $pdf, 3.5);
if (in_array($strJob, $arrOccupations['employed'])) {
    $pdf->Text(106.8, $y + 62, 'X');
}
if (in_array($strJob, $arrOccupations['notEmployed'])) {
    $pdf->Text(140.6, $y + 62, 'X');
}

// E-Mail
if ($data['email']) {
    $pdf->Text(128, $y + 39, $data['email']);
} else {
    if ($i == 0)
        $this->pdfRect(126, $y + 34, 74, $pdf, 6);
}
$pdf->Text(106, $y + 39, 'E-Mail');
$pdf->Line(126, $y + 40, 200, $y + 40);

// Familienstand
$pdf->SetFont('Arial', '', $fontSizeSmall);
$pdf->Text(137, $y + 47, 'unverheiratet');
$pdf->Text(176, $y + 47, 'verheiratet');
$pdf->SetFont('Arial', '', $fontSizeNormal);
$pdf->Text(106, $y + 47, 'Familienstand'); //
if ($data['person']['familyStatusLid'] && $data['person']['familyStatusLid'] < 39) {
    if ($data['person']['familyStatusLid'] == 37)
        $pdf->Text(133, $y + 47, 'X');
    if ($data['person']['familyStatusLid'] == 38)
        $pdf->Text(170.5, $y + 47, 'X');
} else {
    if ($i == 0)
        $this->pdfRect(132.5, $y + 44.3, 3.5, $pdf, 3.5);
    if ($i == 0)
        $this->pdfRect(170, $y + 44.3, 3.5, $pdf, 3.5);
}

$pdf->Rect(132, $y + 44, 3.5, 3.5);
$pdf->Rect(170, $y + 44, 3.5, 3.5);

$y = 120;
$pdf->SetY($y);

$pdf->SetFont('Arial', 'B', $fontSizeNormal);
$pdf->Cell(0, 8, 'Zu Versichernde Person', 0, 1);
$pdf->SetFont('Arial', '', $fontSizeNormal);

// Geschlecht
if ($data['gender2'] == 'male') {
    $pdf->Text(56.8, $y + 13, 'X');
} else {
    $pdf->Text(81.8, $y + 13, 'X');
}
$pdf->Cell(0, 8, 'Geschlecht', 0, 1);
$pdf->Rect(56, $y + 10, 3.5, 3.5, $pdf);
$pdf->Rect(81, $y + 10, 3.5, 3.5, $pdf);
$pdf->SetFont('Arial', '', $fontSizeSmall);
$pdf->Text(61, $y + 13, 'männlich');
$pdf->Text(86, $y + 13, 'weiblich');

// Name
$pdf->SetFont('Arial', '', $fontSizeNormal);
$pdf->Text(54, $y + 21.5, $data['personInsured']['surname'] . ' ' . $data['personInsured']['forename']);
$pdf->Cell(0, 8, 'Name, Vorname', 0, 1);
$pdf->Line(52, $y + 23, 100, $y + 23);

// Geburtsdatum
$pdf->Text(54, $y + 30, $data['birthdate2']);
$pdf->Cell(0, 8, 'Geburtsdatum', 0, 1);
$pdf->Line(52, $y + 31, 100, $y + 31);

// Beruf
$y = $pdf->getY() - 16;
$pdf->SetY($y);
$pdf->SetX(106);
$pdf->Text(128, $y + 5.5, $data['personInsured']['job']);
$pdf->Cell(0, 8, 'Beruf', 0, 1);
$pdf->Line(126, $y + 7, 200, $y + 7);
$pdf->SetX(106);
$pdf->Text(154, $y + 13.5, $data['personInsured']['insurance']);
$pdf->Cell(0, 8, 'Gesetzliche Krankenkasse', 0, 1);
$pdf->Line(152, $y + 15, 200, $y + 15);


$y = 160;
$pdf->SetY($y);


$pdf->SetFont('Arial', 'B', $fontSizeBigger);
$pdf->Cell(0, 10, 'Zahlungsweise und Bankverbindung', 0, 1);
$pdf->SetFont('Arial', '', $fontSizeNormal);

if ($i == 0)
    $this->pdfRect(12, $y + 10, 98, $pdf, 8);
$pdf->Cell(0, 8, ' Bitte füllen Sie das SEPA-Lastschrift-Mandat auf Seite 3 aus.', 0, 1);

$pdf->SetY($y + 24);

$pdf->ln(6);
$pdf->SetFont('Arial', 'B', $fontSizeBigger);
$pdf->Cell(0, 10, 'Gewünschter Versicherungsschutz', 0, 1);
$pdf->SetFont('Arial', '', $fontSizeNormal);

// Versicherungsbeginn
if ($data['begin'])
    $pdf->Text(52, $y + 43.8, '01.' . $data['begin']);
$pdf->Cell(0, 6, 'Versicherungsbeginn', 0, 1);
$pdf->Rect(50, $y + 40.5, 24, 4.5);


$pdf->SetY($y + 52);
if ($pdfTemplate == project::gti('mv-571-572-573-574')) {
    $pdf->SetFont('Arial', 'B', $fontSizeNormal);
    $pdf->Cell(0, 5, 'Versicherungsschutz: 571 + 572 + 573 + 574', 0, 1);
    $pdf->SetFont('Arial', '', $fontSizeNormal);
    $pdf->Ln(4); 
#    $pdf->cMargin = 2;
    $wLeft = 20; $wRight = 125;
    $pdf->SetLeftMargin(13);
    $pdf->Cell($wLeft, 8, '571', 1, 0, 'L', 0); 
    $pdf->Cell($wRight, 8, '90% für Zahnersatz (z.B. Kronen, Brücken, Implantate, u.a.)', 1, 1, 'L', 0); 
    $pdf->Cell($wLeft, 8, '572', 1, 0, 'L', 0); 
    $pdf->Cell($wRight, 8, '100% für Füllungen & Inlays, 90% Kieferorthopädie (für Kinder & Jugendliche)', 1, 1, 'L', 0);  
    $pdf->Cell($wLeft, 8, '573', 1, 0, 'L', 0); 
    $pdf->Cell($wRight, 8, '100% für Wurzel- und Parodontosebehandlung, Schienen- & Aufbissbehelfe', 1, 1, 'L', 0);  
    $pdf->Cell($wLeft, 8, '574', 1, 0, 'L', 0); 
    $pdf->Cell($wRight, 8, '100% für Prophylaxe, max. 170 € p.a. (z.B. professionelle Zahnreinigung)', 1, 1, 'L', 0); 
}
else if ($pdfTemplate == project::gti('mv-571-574')) {
    $pdf->SetFont('Arial', 'B', $fontSizeNormal);
    $pdf->Cell(0, 5, 'Versicherungsschutz: 571 + 574', 0, 1);
    $pdf->SetFont('Arial', '', $fontSizeNormal);
    $pdf->Ln(4); 
#    $pdf->cMargin = 2;
    $wLeft = 20; $wRight = 125;
    $pdf->SetLeftMargin(13);
    $pdf->Cell($wLeft, 8, '571', 1, 0, 'L', 0); 
    $pdf->Cell($wRight, 8, '90% für Zahnersatz (z.B. Kronen, Brücken, Implantate, u.a.)', 1, 1, 'L', 0); 
    $pdf->Cell($wLeft, 8, '574', 1, 0, 'L', 0); 
    $pdf->Cell($wRight, 8, '100% für Prophylaxe, max. 170 € p.a. (z.B. professionelle Zahnreinigung)', 1, 1, 'L', 0);  
}
else if ($pdfTemplate == project::gti('mv-571')) {
    $pdf->SetFont('Arial', 'B', $fontSizeNormal);
    $pdf->Cell(0, 5, 'Versicherungsschutz: 571', 0, 1);
    $pdf->SetFont('Arial', '', $fontSizeNormal);
    $pdf->Ln(4); 
#    $pdf->cMargin = 2;
    $wLeft = 20; $wRight = 125;
    $pdf->SetLeftMargin(13);
    $pdf->Cell($wLeft, 8, '571', 1, 0, 'L', 0); 
    $pdf->Cell($wRight, 8, '90% für Zahnersatz (z.B. Kronen, Brücken, Implantate, u.a.)', 1, 1, 'L', 0);
} 
else if ($pdfTemplate == project::gti('mv-570')) {
    $pdf->SetFont('Arial', 'B', $fontSizeNormal);
    $pdf->Cell(0, 5, 'Versicherungsschutz: 570', 0, 1);
    $pdf->SetFont('Arial', '', $fontSizeNormal);
    $pdf->Ln(4); 
#    $pdf->cMargin = 2;
    $wLeft = 20; $wRight = 125;
    $pdf->SetLeftMargin(13);
    $pdf->Cell($wLeft, 8, '570', 1, 0, 'L', 0); 
    $pdf->Cell($wRight, 8, '75% für Zahnersatz (z.B. Kronen, Brücken, Implantate, u.a.)', 1, 1, 'L', 0);
} 
else if ($pdfTemplate == project::gti('mv-570-572-573-574')) {
    $pdf->SetFont('Arial', 'B', $fontSizeNormal);
    $pdf->Cell(0, 5, 'Versicherungsschutz: 570 + 572 + 573 + 574', 0, 1);
    $pdf->SetFont('Arial', '', $fontSizeNormal);
    $pdf->Ln(4); 
#    $pdf->cMargin = 2;
    $wLeft = 20; $wRight = 125;
    $pdf->SetLeftMargin(13);
    $pdf->Cell($wLeft, 8, '570', 1, 0, 'L', 0); 
    $pdf->Cell($wRight, 8, '75% für Zahnersatz (z.B. Kronen, Brücken, Implantate, u.a.)', 1, 1, 'L', 0);
    $pdf->Cell($wLeft, 8, '572', 1, 0, 'L', 0); 
    $pdf->Cell($wRight, 8, '100% für Füllungen & Inlays, 90% Kieferorthopädie (für Kinder & Jugendliche)', 1, 1, 'L', 0);  
    $pdf->Cell($wLeft, 8, '573', 1, 0, 'L', 0); 
    $pdf->Cell($wRight, 8, '100% für Wurzel- und Parodontosebehandlung, Schienen- & Aufbissbehelfe', 1, 1, 'L', 0);  
    $pdf->Cell($wLeft, 8, '574', 1, 0, 'L', 0); 
    $pdf->Cell($wRight, 8, '100% für Prophylaxe, max. 170 € p.a. (z.B. professionelle Zahnreinigung)', 1, 1, 'L', 0); 
}
else if ($pdfTemplate == project::gti('mv-570-574')) {
    $pdf->SetFont('Arial', 'B', $fontSizeNormal);
    $pdf->Cell(0, 5, 'Versicherungsschutz: 570 + 574', 0, 1);
    $pdf->SetFont('Arial', '', $fontSizeNormal);
    $pdf->Ln(4); 
#    $pdf->cMargin = 2;
    $wLeft = 20; $wRight = 125;
    $pdf->SetLeftMargin(13);
    $pdf->Cell($wLeft, 8, '570', 1, 0, 'L', 0); 
    $pdf->Cell($wRight, 8, '75% für Zahnersatz (z.B. Kronen, Brücken, Implantate, u.a.)', 1, 1, 'L', 0);
    $pdf->Cell($wLeft, 8, '574', 1, 0, 'L', 0); 
    $pdf->Cell($wRight, 8, '100% für Prophylaxe, max. 170 € p.a. (z.B. professionelle Zahnreinigung)', 1, 1, 'L', 0); 
}
else if ($pdfTemplate == project::gti('mv-572-573-574')) {
    $pdf->SetFont('Arial', 'B', $fontSizeNormal);
    $pdf->Cell(0, 5, 'Versicherungsschutz: 572 + 573 + 574', 0, 1);
    $pdf->SetFont('Arial', '', $fontSizeNormal);
    $pdf->Ln(4); 
#    $pdf->cMargin = 2;
    $wLeft = 20; $wRight = 125;
    $pdf->SetLeftMargin(13);
    $pdf->Cell($wLeft, 8, '572', 1, 0, 'L', 0); 
    $pdf->Cell($wRight, 8, '100% für Füllungen & Inlays, 90% Kieferorthopädie (für Kinder & Jugendliche)', 1, 1, 'L', 0);  
    $pdf->Cell($wLeft, 8, '573', 1, 0, 'L', 0); 
    $pdf->Cell($wRight, 8, '100% für Wurzel- und Parodontosebehandlung, Schienen- & Aufbissbehelfe', 1, 1, 'L', 0);  
    $pdf->Cell($wLeft, 8, '574', 1, 0, 'L', 0); 
    $pdf->Cell($wRight, 8, '100% für Prophylaxe, max. 170 € p.a. (z.B. professionelle Zahnreinigung)', 1, 1, 'L', 0); 
}
else if ($pdfTemplate == project::gti('mv-560')) {
    $pdf->SetFont('Arial', 'B', $fontSizeNormal);
    $pdf->Cell(0, 5, 'Versicherungsschutz: 560', 0, 1);
    $pdf->SetFont('Arial', '', $fontSizeNormal);
    $pdf->Ln(4);
#    $pdf->cMargin = 2;
    $wLeft        = 20;
    $wRight       = 125;
    $pdf->SetLeftMargin(13);
    $pdf->Cell($wLeft, 8, '560', 1, 0, 'L', 0);
    $pdf->Cell($wRight, 8, 'Verdopplung GKV-Festzuschuss bei Zahnersatz', 1, 1, 'L', 0);
}
else if ($pdfTemplate == project::gti('mv-561')) {
    $pdf->SetFont('Arial', 'B', $fontSizeNormal);
    $pdf->Cell(0, 5, 'Versicherungsschutz: 561', 0, 1);
    $pdf->SetFont('Arial', '', $fontSizeNormal);
    $pdf->Ln(4);
#    $pdf->cMargin = 2;
    $wLeft        = 20;
    $wRight       = 125;
    $pdf->SetLeftMargin(13);
    $pdf->Cell($wLeft, 8, '561', 1, 0, 'L', 0);
    $pdf->Cell($wRight, 8, 'Verdreifachung GKV-Festzuschuss bei Zahnersatz', 1, 1, 'L', 0);
}

if($pdfTemplate == project::gti('mv-571-572-573-574') || $pdfTemplate == project::gti('mv-571-574') || $pdfTemplate == project::gti('mv-571') || $pdfTemplate == project::gti('mv-570') || $pdfTemplate == project::gti('mv-570-572-573-574') || $pdfTemplate == project::gti('mv-570-572-573-574') || $pdfTemplate == project::gti('mv-572-573-574') || $pdfTemplate == project::gti('mv-560') || $pdfTemplate == project::gti('mv-561'))
{
    $pdf->SetLeftMargin(12);    
#    $pdf->cMargin = 0;
    $pdf->Ln(8); 
    $pdf->SetFont('Arial', '', $fontSizeBigger);
    $pdf->Cell(62, 8, 'Ihr monatlicher Gesamtbeitrag:', 0, 0, 'L', 0);
    $pdf->Line(71, $pdf->getY() + 7, 92, $pdf->getY() + 7);
    $pdf->SetFont('Arial', 'B', $fontSizeBigger+2);
    $pdf->Cell(60, 8, $data['price'] . ' €');
    $pdf->SetFont('Arial', '', $fontSizeNormal, 0, 1, 'L', 0);
}

// Page 2 -----------------------------------------------------------------------------------------

$pdf->AddPage();

// Logos
$this->imgZZVLogo($i, $pdf, 140, 6, 60);
$this->imgLogo($i, $pdf, 'mv/logos/100', 13, 3, 60);

// Versicherungsnehmer

$pdf->SetY(22);

$pdf->SetFont('Arial', 'B', $fontSizeBigger);
$pdf->Cell(0, 8, 'Wichtige Vertragserklärungen zur Deutschen Zahnversicherung (DZV):', 0, 1);

$pdf->SetFont('Arial', '', $fontSizeNormal);
$pdf->Cell(0, 8, 'Mit meiner Unterschrift, gebe ich rechtsverbindlich folgende Erklärungen ab:
', 0, 1);
$pdf->Ln(3);
$pdf->SetFont('Arial', 'B', $fontSizeNormal);
$pdf->Cell(0, 8, '1. Bestehende Gesetzliche Krankenversicherung', 0, 1);
$pdf->SetFont('Arial', '', $fontSizeNormal);
$pdf->MultiCell(192, 5, 'Es besteht für die zu versichernde Person ein Leistungsanspruch bei einer deutschen gesetzlichen Krankenversicherung bzw. auf freie Heilfürsorge oder ein Anspruch auf truppenärztliche Versorgung.', 0, 1);
$pdf->Ln(4);
$pdf->SetFont('Arial', 'B', $fontSizeNormal);
$pdf->Cell(0, 8, '2. Vor Vertragsschluss eingetretene Versicherungsfälle & fehlende Zähne', 0, 1);
$pdf->SetFont('Arial', '', $fontSizeNormal);
$pdf->MultiCell(190, 5, 'Für bei Vertragsschluss fehlende und noch nicht ersetzte Zähne besteht kein Leistungsanspruch. 
Für Zahnersatzmaßnahmen, Zahnbehandlungen und kieferorthopädische Behandlungen, die vor Versicherungsbeginn angeraten oder begonnen worden sind(vor Vertragsabschluss eingetretene Versicherungsfälle), wird nicht geleistet.', 0, 1);
$pdf->Ln(4);
$pdf->SetFont('Arial', 'B', $fontSizeNormal);
$pdf->Cell(0, 8, '3. SEPA-Lastschriftmandat für wiederkehrende Zahlungen', 0, 1);
$pdf->SetFont('Arial', '', $fontSizeNormal);
$pdf->MultiCell(190, 5, 'Ich ermächtige Sie, Zahlungen von meinem angegebenen Konto (siehe Inkassodaten) mittels Lastschrift einzuziehen. Zugleich weise ich das genannte Kreditinstitut (siehe Inkassodaten) an, die von Ihnen auf das angegebene Konto gezogenen Lastschriften einzulösen.

Mir ist bekannt, dass, soweit ich nicht der Kontoinhaber bin, das vorgenannte Lastschriftmandat durch den Kontoinhaber unterzeichnet einzureichen ist.

Ich stimme zu, dass die Ankündigung des SEPA-Basislastschrift-Einzugs gemäß den "Bedingungen für den Lastschrifteinzug" gegenüber dem Kontoinhaber erfolgt und dem Kontoinhaber hiermit in Verbindung stehende Vertragsdaten übermittelt werden.', 0, 1);
$pdf->Ln(4);
$pdf->SetFont('Arial', 'B', $fontSizeNormal);
// ==================== Tarife 571+572+573+574
if ($data['idt'] == 43) {
    $pdf->Cell(0, 8, '4. Erhaltene Vertragsunterlagen für die Tarife 571, 572, 573, 574', 0, 1);
}
// ==================== Tarife 571+572+573+574
if ($data['idt'] == 44) {
    $pdf->Cell(0, 8, '4. Erhaltene Vertragsunterlagen für die Tarife 571, 572', 0, 1);
}
// ==================== Tarif 560
if ($data['idt'] == 78) {
    $pdf->Cell(0, 8, '4. Erhaltene Vertragsunterlagen für den Tarif 560', 0, 1);
}
$pdf->SetFont('Arial', '', $fontSizeNormal);
$pdf->MultiCell(190, 5, 'Ich bestätige, dass ich vor Antragstellung folgende Dokumente erhalten und zur Kenntnis genommenhabe: Produktinformationsblatt, Vertragsinformationen, Allgemeine Versicherungsbedingungen,Tarifbedingungen, Einwilligung in die Erhebung und Verwendung von Gesundheitsdaten und Erklärungenzur Entbindung von der Schweigepflicht, Erklärung zum Beginn des Versicherungsschutzes, Widerrufsrecht und Widerrufsfolgen.

Ich bestätige, dass ich die Unterlagen / Dokumente erhalten und gelesen habe und stimme den Inhalten zu. Ich bin damit einverstanden, dass der Münchener Verein den Beitrag von dem angegebenen Konto per Lastschrift einzieht. Für die außer mir zu versichernden Personen erkläre ich, dass ich von diesen bevollmächtigt bin, die Einwilligung in die Erhebung und Verwendung von Gesundheitsdaten und die Erklärung zur Entbindung von der Schweigepflicht auch in deren Namen abzugeben.

Ich willige darüber hinaus ein, dass die Unternehmen der MÜNCHENER VEREIN Versicherungsgruppe die allgemeinen Antrags-, Vertrags- und Leistungsdaten in einer gemeinsamen Datensammlung führen.', 0, 1);
$pdf->Ln(4);
$pdf->SetFont('Arial', 'B', $fontSizeNormal);
$pdf->Cell(0, 8, '5. Einwilligung in die Bonitätsprüfung', 0, 1);
$pdf->SetFont('Arial', '', $fontSizeNormal);
$pdf->MultiCell(190, 5, 'Ich willige ein, dass der Versicherer bei Vertragsschluss, im Rahmen der Vertragsabwicklung sowie bei Zahlungsverzug Informationen über mein allgemeines Zahlungsverhalten von einer Auskunftei (z.B. Creditreform, InFoScore) einholt und nutzt. Ich kann meine Einwilligung jederzeit mit Wirkung für die Zukunft widerrufen. Der Versicherer ist im Übrigen verpflichtet, mir Auskunft über die zu meiner Person gespeicherten Daten, deren Herkunft und Empfänger sowie zum Zweck der Speicherung zu geben. Zur Überprüfung meiner dort gespeicherten Daten kann ich mich auch direkt mit den Auskunft gebenden Unternehmen in Verbindung setzen.', 0, 1);


$pdf->SetFont('Arial', '', $fontSizeNormal);

$pdf->Text(10, 283, 'Ort, Datum:');
$pdf->Line(30, 284, 90, 284);
if ($i == 0)
    $this->pdfRect(30, 278, 60, $pdf, 6);

$pdf->Text(110, 283, 'Unterschrift:');
$pdf->Line(130, 284, 190, 284);
if ($i == 0)
    $this->pdfRect(130, 278, 60, $pdf, 6);

// Page 3 (SEPA - Mandat) --------------------------------------------------------------------------

$pdf->AddPage();

$pdf->setSourceFile($path.'files/mv/mv.sepa-mandat.pdf');
$tplidx = $pdf->importPage(1, '/MediaBox');
$pdf->useTemplate($tplidx, 0, 8, 206);

// Anschrift raus
$this->pdfClean(6, 30, 140, $pdf, 255, 255, 255, 60, 'F');

// Zeichen am Ende raus
$this->pdfClean(90, 270, 140, $pdf, 255, 255, 255, 20, 'F');

// Kundendaten raus
$this->pdfClean(20, 162.2, 80, $pdf, 255, 255, 255, 6, 'F');
$this->pdfClean(20, 173.3, 80, $pdf, 255, 255, 255, 6, 'F');
$this->pdfClean(20, 184.2, 80, $pdf, 255, 255, 255, 6, 'F');
$this->pdfClean(140, 162.2, 20, $pdf, 255, 255, 255, 6, 'F');
$this->pdfClean(140, 173.4, 20, $pdf, 255, 255, 255, 6, 'F');

$this->pdfClean(54, 196, 10, $pdf, 255, 255, 255, 6, 'F');
$this->pdfClean(85, 196, 11.3, $pdf, 255, 255, 255, 6, 'F');
$this->pdfClean(131, 196, 18.3, $pdf, 255, 255, 255, 6, 'F');

$this->pdfClean(54, 203, 80, $pdf, 255, 255, 255, 6, 'F');
$this->pdfClean(54, 214.2, 80, $pdf, 255, 255, 255, 6, 'F');
$this->pdfClean(54, 220.5, 80, $pdf, 255, 255, 255, 0.7, 'F');
$this->pdfClean(54, 225.2, 80, $pdf, 255, 255, 255, 6, 'F');
$this->pdfClean(54, 236.2, 80, $pdf, 255, 255, 255, 6, 'F');

// Gelbe Markierungen
if ($i == 0)
    $this->pdfRect(24, 163, 85, $pdf);
if ($i == 0)
    $this->pdfRect(24, 174.2, 85, $pdf);
if ($i == 0)
    $this->pdfRect(143, 163, 38, $pdf);
if ($i == 0)
    $this->pdfRect(143, 174.2, 38, $pdf);
if ($i == 0)
    $this->pdfRect(54.5, 237, 127, $pdf);

// Kreuze
if ($i == 0) {
    $pdf->SetFont($pdfCfg['fontFamily'], 'b', $pdfCfg['fontSize'] + 2, 0, 2);
    $pdf->Text(100, 241, 'X');
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
}

// Kundendaten
$pdf->Text(54.5, 201.3, $data['price']);
$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'] - 2, 0, 2);
$pdf->Text(85, 201.3, 'monatlich');
$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
if ($data['begin'])
    $pdf->Text(132.5, 201.3, '01.' . $data['begin']);
$pdf->Text(54.5, 208, $data['person']['surname'] . ', ' . $data['person']['forename']);
$pdf->Text(54.5, 219, $data['street']);
$pdf->Text(54.5, 230, $data['postcode'] . ' ' . $data['city']);

$pdf->addPage('P');