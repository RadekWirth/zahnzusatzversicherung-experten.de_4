<?
// File for generating Contract PDF for Allianz740

		$pagecount = $pdf->setSourceFile($path.'/files/Allianz-740-740_23.3.2012.pdf');

		$tplidx = $pdf->importPage(1, '/MediaBox');

		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 1, 209);

		// set title and font to big and bold!
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);

		//gelbe markierungen nur auf rückantwort drucken! 
		if($i==0) $this->pdfRect(75, 4, 68, $pdf);
		
		$pdf->Text(75.0, 8.0, $titles[$i]);


		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

	    #$this->getCompanyLogo(28.0, 21.0, $pdf, $data['contractComplete']['sourcePage'], $i, 50);
   		$this->getCompanyStamp(88, 17, $pdf, $data['contractComplete']['sourcePage'], 8);


		// Sind Sie bereits Kunde einer PKV?
		$pdf->Text(154.5, 72.5, "X");

	// 1. Antragsteller

		if($data['gender']=='male')	{
			$pdf->Text(36.4, 85.7, 'X');
		}
		else {
			$pdf->Text(46.1, 85.7, 'X');
		}

		$pdf->Text(23.0, 96.0, $data['person']['surname']);
		$pdf->Text(23.0, 102.4, $data['person']['forename']);
 		$pdf->Text(23.0, 108.8, $data['street']);

	if($data['phone']) {
		$pdf->Text(130.0, 108.8, $data['phone']);
	} else {
		if($i==0) $this->pdfRect(128.0, 106.0, 60.0, $pdf, 3.5);
	}

		$pdf->Text(23.0, 115.2, $data['postcode'].'               '.$data['city']);

	if($data['mobile']) {
		$pdf->Text(130.0, 115.2, $data['mobile']);
	} else {
		if($i==0) $this->pdfRect(128.0, 112.5, 60.0, $pdf, 3.5);
	}

 		$pdf->Text(94.5, 102.4, $data['birthdate']);

		if($data['job']) {
			$pdf->Text(23.0, 121.7, $data['job']);
		} else
			if($i==0) $this->pdfRect(23.0, 118.9, 60.0, $pdf, 3.5);


		if($i==0) $this->pdfRect(83.4, 116.5, 2.0, $pdf, 3.0);
		if($i==0) $this->pdfRect(96.9, 116.5, 2.0, $pdf, 5.5);

		$pdf->Text(23.0, 131.5, $data['email']);


		if($data['nation']=='DEU')
		{
			$pdf->Text(126.7, 120.7, 'x');
		} else {
			$pdf->Text(137.8, 120.7, 'x');
			$pdf->Text(147.0, 121.0, $data['nation']);
		}

		if($data['person']['familyStatusLid'] && $data['person']['familyStatusLid']<39)
		{
			if($data['person']['familyStatusLid'] == 37)
				$pdf->Text(126.7, 129.65, 'x');
			if($data['person']['familyStatusLid'] == 38)
				$pdf->Text(126.7, 132.25, 'x');
		} else
		{
			if($i==0) $this->pdfRect(126.2, 127.9, 2.0, $pdf, 5.5);
			if($i==0) $this->pdfRect(151.0, 127.9, 2.0, $pdf, 2.5);
		}


	// 2. Zahlungsweise

		if($i==0) $this->pdfRect(55.7, 138.0, 4.0, $pdf);
		if($i==0) $this->pdfRect(84.9, 138.0, 4.0, $pdf);
		if($i==0) $this->pdfRect(118.2, 138.0, 4.0, $pdf);
		if($i==0) $this->pdfRect(152.0, 138.0, 4.0, $pdf);

	// 3. Versicherungs- / Änderungsbeginn

		$begin = explode('.', $data['begin']);

		$pdf->Text(94.0, 152.0, $begin[0]);
		$pdf->Text(108.0, 152.0, $begin[1]);


	// 4. Krankenzusatzversicherung

		$pdf->Text(12.3, 166.3, 'X');

	// 5. Zu versichernde Personen

		if(!empty($data['personInsured']['pid']))
		{
			$pdf->Text(26.0, 199.0, $data['name2']);
			if($data['job2']) {
				$pdf->Text(96.0, 195.0, $data['job2']);
			} else
				if($i==0) $this->pdfRect(96.0, 192.2, 60.0, $pdf, 3.5);


			if($data['gender2']=='male')	{
				$pdf->Text(160.0, 193.4, 'x');
			}
			else {
				$pdf->Text(160.0, 197.2, 'x');
			}

			$pdf->Text(180.5, 199.0, $data['birthdate2']);	

			if($i==0) $this->pdfRect(92.8, 197.5, 2.5, $pdf, 2.5);
			if($i==0) $this->pdfRect(109.8, 197.5, 2.5, $pdf, 2.5);
			if($i==0) $this->pdfRect(130.0, 197.5, 2.5, $pdf, 2.5);

			$pdf->Text(35.0, 291.0, $data['name2b']);

			if($data['nation']!=$data['nation2'])
			{
				if($data['nation2']!='DEU') 
				{
					$pdf->Text(12.3, 265.3, 'X');
					$pdf->Text(28.2, 264.3, $data['nation2']);

				} else {
					$pdf->Text(141.6, 256.2, 'X');
				}
			}

		} else {
			$pdf->Text(26.0, 199.0, $data['name']);
			$pdf->Text(96.0, 195.0, $data['job']);

			if($data['gender']=='male')	{
				$pdf->Text(160.0, 193.4, 'x');
			}
			else {
				$pdf->Text(160.0, 197.2, 'x');
			}

			$pdf->Text(180.5, 199.0, $data['birthdate']);	

			if($i==0) $this->pdfRect(92.8, 197.5, 2.5, $pdf, 2.5);
			if($i==0) $this->pdfRect(109.8, 197.5, 2.5, $pdf, 2.5);
			if($i==0) $this->pdfRect(130.0, 197.5, 2.5, $pdf, 2.5);

			$pdf->Text(35.0, 291.0, $data['nameb']);
		}

// SEITE 2

		$tplidx = $pdf->importPage(2, '/MediaBox');

		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 1, 209);

	// 6. Tarif / Beitrag

		$pdf->Text(80.0, 25.0, $data['bonusbase']);
		if($data['bonus_tooth']>=0) {
			$pdf->Text(80.0, 32.0, $data['bonus_tooth']);
		}
		if($data['bonus_allianz740']) {
			$pdf->Text(80.0, 38.0, $data['bonus_allianz740']);
		}

		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-1, 0, 2);
			$pdf->Text(80.0, 44.0, $data['price']);
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

	// 7. Andere Krankenversicherungen
	    if($i==0) $this->pdfRect(20.0, 75.6, 2.5, $pdf, 2.5);
		if($i==0) $this->pdfRect(119.0, 75.6, 2.5, $pdf, 2.5);
        if($i==0) $this->pdfRect(119.0, 71.6, 2.5, $pdf, 2.5);
		if($i==0) $this->pdfRect(137.0, 74.6, 55.0, $pdf, 4.0);
		if($i==0) $this->pdfRect(22.0, 112.0, 55.0, $pdf, 4.0);
		if($i==0) $this->pdfRect(82.0, 112.0, 55.0, $pdf, 4.0);
		if($i==0) $this->pdfRect(142.0, 112.0, 55.0, $pdf, 4.0);

		if($i==0) $this->pdfRect(22.0, 142.0, 55.0, $pdf, 4.0);
		if($i==0) $this->pdfRect(82.0, 142.0, 55.0, $pdf, 4.0);
		if($i==0) $this->pdfRect(142.0, 142.0, 55.0, $pdf, 4.0);

		if(!empty($data['personInsured']['pid']))
		{
			$pdf->Text(54.0, 78.5, $data['insurance2']);
		} else {
			$pdf->Text(54.0, 78.5, $data['insurance']);
		}


	// 8. Fragen zum Gebisszustand
		// setzen der Markierungen
		if($i==0) $this->pdfRect(150.5, 170.2, 12.0, $pdf);
		if($i==0) $this->pdfRect(150.5, 184.4, 12.0, $pdf);
		if($i==0) $this->pdfRect(150.5, 202.0, 12.0, $pdf);

		if($data['incare']==1)
		{
			$pdf->Text(151.7, 173.8, 'X');
		} else {
			$pdf->Text(159.8, 173.8, 'X');
		}	

		if($data['t1']>0)
		{
			$pdf->Text(151.7, 187.3, 'X');
			$pdf->Text(155.0, 191.5, $data['t1']);
		} else {
			$pdf->Text(159.8, 187.3, 'X');
		}

		if($data['contractComplete']['tooth5']>0)
		{
			$pdf->Text(151.7, 205.5, 'X');
			if($i==0) $this->pdfRect(151.5, 207.8, 9.0, $pdf);

			$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);
			$pdf->Text(161.5, 212.0, '!');
			$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);
		} else {
			$pdf->Text(159.8, 205.5, 'X');
		}

		$pdf->Text(35.0, 291.0, $data['name']);	

// SEITE 3

		$tplidx = $pdf->importPage(3, '/MediaBox');

		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 1, 209);

		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);

		$pdf->Text(115.0, 225.2, '0  6  2  0  7  8  9');
		$pdf->Text(115.0, 243.8, '7  5  8  5  2  6  4  0');

		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);


		$pdf->Text(35.0, 291.0, $data['name']);	

// SEITE 4

		$tplidx = $pdf->importPage(4, '/MediaBox');

		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 1, 209);

		$pdf->Text(35.0, 291.0, $data['name']);

// SEITE 5

		$tplidx = $pdf->importPage(5, '/MediaBox');

		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 1, 209);

		if($i==0) $this->pdfRect(13.0, 30.5, 85.0, $pdf);
		if($i==0) $this->pdfRect(13.0, 51.7, 85.0, $pdf);


	// C + D Unterschriften
		if($i==0) $this->pdfRect(109.0, 34.0, 40.0, $pdf);
		if($i==0) $this->pdfRect(155.0, 34.0, 42.0, $pdf);

		if($i==0) $this->pdfRect(109.0, 44.0, 80.0, $pdf);
		if($i==0) $this->pdfRect(109.0, 64.0, 80.0, $pdf);

		if($i==0) $this->pdfRect(111.0, 132.0, 80.0, $pdf);

		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+3, 0, 2);
			$pdf->Text(156.0, 38.0, 'X');
			$pdf->Text(110.0, 48.0, 'X');
			$pdf->Text(110.0, 68.0, 'X');
			$pdf->Text(112.0, 136.0, 'X');
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

		$pdf->Text(35.0, 291.0, $data['name']);

if($complete == 2) $pdf->addPage('P');
