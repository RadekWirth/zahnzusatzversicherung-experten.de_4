<?php
// File for generating Contract PDF for Signal Iduna Pur (Start & Plus & Top & Basis)

        /*
         * Constants
         */
        // SQUARE XY
        define('X_Y', 1.2);

        // RECT Y
        define('Y', 2.7);

        $data['person']['age'] = actionHelper::getAge($data['contractComplete']['birthdate']);

        if ($i!=1) {
            $pagecount = $pdf->setSourceFile($path.'/files/signal-iduna/antrag/2020/ambulant.pdf');
        } else {
            $pagecount = $pdf->setSourceFile($path.'/files/signal-iduna/antrag/2020/ambulant.pdf');
        }

        $tplidx = $pdf->importPage(1, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
        $pdf->SetMargins(0, 0, 0);
        

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);

        if($i==0) $this->pdfRect(75.0, 3.0, 70, $pdf);
        $pdf->Text(75.0, 7.0, $titles[$i]);

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-1, 0, 2);

        // -> Seite 2
        $pdf->Text(83.0, 14.0, "Versicherungsmakler Experten GmbH");

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-3, 0, 2);

        //mark section
        //Selbstständiger/Firma/Verein
        
        if($i==0) {
            $this->pdfRect(142.9, 57.3, X_Y, $pdf, X_Y);
            $this->pdfRect(161.6, 57.3, X_Y, $pdf, X_Y);
            $this->pdfRect(177.1, 57.3, X_Y, $pdf, X_Y);
            $this->pdfRect(189.3, 57.3, X_Y, $pdf, X_Y);
        }
       

        //freiwillig versichert?
        if($i==0) {
            $this->pdfRect(90.5, 63.8, X_Y, $pdf, X_Y);
            $this->pdfRect(90.5, 66.2, X_Y, $pdf, X_Y);
            $this->pdfRect(111.4, 63.8, X_Y, $pdf, X_Y);
        }

        //mark section end
        $pdf->Text(151.4, 62.1, 'X'); // Beihilfe
        $pdf->Text(178.5, 62.1, 'X'); // Heilfuersorge

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);    

        if($data['gender']=='male') {
            $pdf->Text(16.4, 47.7, 'X');
        } else {
            $pdf->Text(16.4, 50.8, 'X');
        }

        if($data['widowed']) {
            $pdf->Text(16.4, 63.5, 'X');
        } else if($data['married']) {
            $pdf->Text(16.4, 60.4, 'X');
        } else {
             $pdf->Text(16.4, 57.2, 'X');
        }

        // Bereits Kunde?
        $pdf->Text(158.9, 37.9, 'X');
        
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
        $pdf->Text(29.0, 48, $data['name']);
        $pdf->Text(143.8, 48, $data['birthdate']);

        if($data['nation']=='DEU') {
            $pdf->Text(168, 48, 'deutsch');
        } else if(isset($data['nation'])) {
            $pdf->Text(168, 48, $data['nation']);
        } else {
           if($i==0) $this->pdfRect(166.5, 45, 30, $pdf, 4);
        }

        $pdf->Text(29.0, 54.8, $data['street']);

        $pdf->Text(116, 54.8, $data['postcode'].' '.$data['city']);

        if(!isset($data['job']) || $data['job']=='') {
           if($i==0) $this->pdfRect(28.5, 58.0, 40, $pdf, 4);
        } else {
            $pdf->Text(29.0, 61.2, $data['job']);
        }

        //Telefonnummer
        if(!isset($data['phone']) || $data['phone']=='')
        {
            if($i==0) $this->pdfRect(15.8, 79.1, 39, $pdf, 3);
        } else
            $pdf->Text(16.5, 82.0, $data['phone']);

        //eMail
        if(!isset($data['email']) || $data['email']=='')
        {
            if($i==0) $this->pdfRect(102, 79.1, 53, $pdf, 3);
        } else 
            $pdf->Text(105.0, 82.0, $data['email']);

        $pdf->Text(162, 82.0, $data['mobile']);

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);    


        if(!isset($data['insurance']) || $data['insurance']=='') {
            if($i==0) $this->pdfRect(28.5, 65, 44, $pdf, 3.5);
        } else {
            $pdf->Text(29.0, 68, $data['insurance']);
        }

        // Insured since
        if($i==0) $this->pdfRect(181, 65.3, 19, $pdf, 3.5);


        //Kontaktmöglichkeit
        if($i==0) {
            $this->pdfRect(59.2, 98.0, X_Y, $pdf, X_Y);
            $this->pdfRect(96.6, 98.0, X_Y, $pdf, X_Y);
            $this->pdfRect(137.8, 98.0, X_Y, $pdf, X_Y);
            $this->pdfRect(175.2, 98.0, X_Y, $pdf, X_Y);
        }

        //Zahlungsweise
        if($i==0) {
            $this->pdfRect(33.0, 111.6, X_Y, $pdf, X_Y);
            $this->pdfRect(46.5, 111.6, X_Y, $pdf, X_Y);
            $this->pdfRect(61.5, 111.6, X_Y, $pdf, X_Y);
            $this->pdfRect(76.8, 111.6, X_Y, $pdf, X_Y);
        }

        //Konto
        if($i==0) {
            $this->pdfRect(17.5, 138.6, 120, $pdf, Y);
            $this->pdfRect(156.0, 138.6, 38, $pdf, Y);
            $this->pdfRect(17.5, 148.7, 89, $pdf, Y);
            $this->pdfRect(117.0, 148.7, 24, $pdf, Y);
            $this->pdfRect(149.5, 148.7, 47, $pdf, Y);
        }
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
        if($i==0) $pdf->Text(149, 151, 'X');



// 2 Zu versichernde Personen
        // check for child
        if($data['name2b'] != $data['name']) {
            // setze marker bei Antragsteller / VN
            $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
            $pdf->Text(116.2, 41.6, 'X');            
            $data['personInsured']['age'] = actionHelper::getAge($data['personInsured']['birthdate']);

        //mark section
        //Selbstständiger/Firma/Verein

            if($i==0) {
                $this->pdfRect(142.8, 181.8, X_Y, $pdf, X_Y);
                $this->pdfRect(161.3, 181.8, X_Y, $pdf, X_Y);
                $this->pdfRect(176.8, 181.8, X_Y, $pdf, X_Y);
                $this->pdfRect(188.8, 181.8, X_Y, $pdf, X_Y);
            }
           

            //freiwillig versichert?
            if($i==0) {
                $this->pdfRect(90.5, 188.1, X_Y, $pdf, X_Y);
                $this->pdfRect(90.5, 190.4, X_Y, $pdf, X_Y);
                $this->pdfRect(111.4, 188.1, X_Y, $pdf, X_Y);
            }
                        
                //mark section end


                
                $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);

                $pdf->Text(29.0, 172.8, $data['name2b']);
                $pdf->Text(29.0, 179.8, $data['street'].', '.$data['postcode'].' '.$data['city']);
                $pdf->Text(148.0, 172.8, $data['birthdate2']);

                if($data['gender2']=='male')    {
                    $pdf->Text(16.4, 175.5, 'X');
                } else {
                    $pdf->Text(16.4, 178.7, 'X');
                }

                if($data['widowed2']) {
                    $pdf->Text(16.4, 191.5, 'X');
                } else if($data['married2']) {
                    $pdf->Text(16.4, 188.0, 'X');
                } else {
                    $pdf->Text(16.4, 184.9, 'X');
                }

                $pdf->Text(151.2, 186.3, 'X'); // Beihilfe
                $pdf->Text(178.1, 186.3, 'X'); // Heilfuersorge
                
                if($data['nation2']=='DEU') {
                    $pdf->Text(169, 172.0, 'deutsch');
                } else if(isset($data['nation2'])) {
                    $pdf->Text(169.0, 172.0, $data['nation2']);
                } else {
                    if($i==0) $this->pdfRect(166.5, 169.3, 30, $pdf, 4);
                }

                if(isset($data['job2']) && strlen($data['job2'])>0) {
                    $pdf->Text(29.0, 186.6, $data['job2']);
                } else {
                    if($i==0) $this->pdfRect(28.5, 183.8, 50, $pdf, 3);
                }

                if(isset($data['insurance2']) && strlen($data['insurance2'])>0 ) {
                    $pdf->Text(29.0, 193, $data['insurance2']);
                } else {
                    if($i==0) $this->pdfRect(28.5, 190.0, 40, $pdf, 3);
                }
                // Insured since
                if($i==0) $this->pdfRect(181, 190.0, 19, $pdf, 3);

                #if($i==0) $this->pdfRect(163, 202.0, 26, $pdf, 3);
                $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        }
        // end child
        else {
            // setze marker bei Antragsteller / VN
            $pdf->Text(70.1, 41.8, 'X');
        }
     
        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);

       // $this->getCompanyStamp(100.0, 257.7, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize'], 'horizontal', 'wide');

// SEITE 2
        $tplidx = $pdf->importPage(2, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        $this->getCompanyStamp(100.0, 5, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize'], 'horizontal', 'wide');



        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        $pdf->Text(33.0, 7.5, $data['name']);

        $pdf->Text(170, 30.0, '01.'. $data['begin']);


        

        // choose tariff
        if($pdfTemplate == project::gti('sigidu-zahn-basis-pur')){
            #if($data['name2b'] == $data['name']) {
            #    $pdf->Text(18.6, 259.7, 'X');
            #    $pdf->Text(47.4, 259.7, $data['price']); 

            #    $pdf->Text(17.3, 65.7, 'X');
            #    $pdf->Text(57.4, 65.5, $data['price']);                  
            #} else {
            #    $pdf->Text(68.4, 259.7, 'X');
            #    $pdf->Text(97.8, 259.7, $data['price']); 

            #    $pdf->Text(79.3, 65.7, 'X');
            #    $pdf->Text(119.2, 65.5, $data['price']);                  
            #}
        }
        else if($pdfTemplate == project::gti('sigidu-zahn-basis')){
            #if($data['name2b'] == $data['name']) {
            #    $pdf->Text(8.4, 214.9, 'X');
            #    $pdf->Text(47.4, 214.3, $data['price']);
            #} else {
            #    $pdf->Text(70.2, 214.9, 'X');
            #    $pdf->Text(109.8, 214.3, $data['price']);
            #}
        }
        else if($pdfTemplate == project::gti('sigidu-zahn-start-pur')){
            if($data['name2b'] == $data['name']) {
                $pdf->Text(17.9, 58.0, 'X');
                $pdf->Text(50.4, 58.2, $data['price']);                 
            } else {
                $pdf->Text(67.9, 58.0, 'X');
                $pdf->Text(100.2, 58.2, $data['price']); 
            }
        }
        else if($pdfTemplate == project::gti('sigidu-zahn-start')){
            #if($data['name2b'] == $data['name']) {
            #    $pdf->Text(8.4, 219.9, 'X');
            #    $pdf->Text(47.4, 219.3, $data['price']);
            #} else {
            #    $pdf->Text(70.2, 219.9, 'X');
            #    $pdf->Text(109.8, 219.3, $data['price']);
            #}
        }
        else if($pdfTemplate == project::gti('sigidu-zahn-plus-pur')){
            if($data['name2b'] == $data['name']) {
                $pdf->Text(17.9, 63.0, 'X');
                $pdf->Text(50.4, 63.2, $data['price']);                
            } else {
                $pdf->Text(67.9, 63.0, 'X');
                $pdf->Text(100.2, 63.2, $data['price']);
            }
        }
        else if($pdfTemplate == project::gti('sigidu-zahnplus')){
            if($data['name2b'] == $data['name']) {
                $pdf->Text(17.9, 68.0, 'X');
                $pdf->Text(50.4, 68.2, $data['price']);                
            } else {
                $pdf->Text(67.9, 68.0, 'X');
                $pdf->Text(100.2, 68.2, $data['price']);     
            }
        }        
        else if($pdfTemplate == project::gti('sigidu-zahn-top-pur')) {
            if($data['name2b'] == $data['name']) {
                $pdf->Text(17.9, 73.0, 'X');
                $pdf->Text(50.4, 73.2, $data['price']);    
            } else {
                $pdf->Text(67.9, 73.0, 'X');
                $pdf->Text(100.2, 73.2, $data['price']);  
            }
        }
        else if($pdfTemplate == project::gti('sigidu-zahntop')){
            if($data['name2b'] == $data['name']) {
                $pdf->Text(17.9, 78.0, 'X');
                $pdf->Text(50.4, 78.0, $data['price']);               
            } else {
                $pdf->Text(67.9, 78.0, 'X');
                $pdf->Text(100.2, 78.0, $data['price']);                   
            }
        }
	
	if($data['name2b'] == $data['name']) {
		$pdf->Text(50.4, 83.7, $data['price']); 
	} else {
		$pdf->Text(100.2, 83.7, $data['price']); 
	}
	
	$pdf->Text(175.2, 83.7, $data['price']); 


        //ja / nein
        
        if($data['name2b'] != $data['name'])
        {  //mark section
            if($i==0) {
                    $this->pdfRect(174.4, 123.2, X_Y, $pdf, X_Y);
                    $this->pdfRect(182.0, 123.2, X_Y, $pdf, X_Y);

                    $this->pdfRect(174.4, 126.4, X_Y, $pdf, X_Y);
                    $this->pdfRect(182.0, 126.4, X_Y, $pdf, X_Y);

                    $this->pdfRect(174.4, 130.0, X_Y, $pdf, X_Y);
                    $this->pdfRect(182.0, 130.0, X_Y, $pdf, X_Y);

                    $this->pdfRect(174.4, 133.6, X_Y, $pdf, X_Y);
                    $this->pdfRect(182.0, 133.6, X_Y, $pdf, X_Y);

                    $this->pdfRect(174.4, 137.2, X_Y, $pdf, X_Y);
                    $this->pdfRect(182.0, 137.2, X_Y, $pdf, X_Y);
                 }
            } 
            else {
                //mark section
                if($i==0) {

                    $this->pdfRect(161.0, 123.2, X_Y, $pdf, X_Y);
                    $this->pdfRect(168.4, 123.2, X_Y, $pdf, X_Y);

                    $this->pdfRect(161.0, 126.4, X_Y, $pdf, X_Y);
                    $this->pdfRect(168.4, 126.4, X_Y, $pdf, X_Y);

                    $this->pdfRect(161.0, 130.0, X_Y, $pdf, X_Y);
                    $this->pdfRect(168.4, 130.0, X_Y, $pdf, X_Y);

                    $this->pdfRect(161.0, 133.6, X_Y, $pdf, X_Y);
                    $this->pdfRect(168.4, 133.6, X_Y, $pdf, X_Y);

                    $this->pdfRect(161.0, 137.2, X_Y, $pdf, X_Y);
                    $this->pdfRect(168.4, 137.2, X_Y, $pdf, X_Y);

                    #$this->pdfRect(68.0, 249.1, 5.0, $pdf, 3.0);
                    #$this->pdfRect(68.0, 253.0, 5.0, $pdf, 3.0);

                    //if($data['personInsured']['age'] <= 20)   {
                    #    $this->pdfRect(162.5, 268.3, X_Y, $pdf, X_Y);
                    #    $this->pdfRect(168.9, 268.3, X_Y, $pdf, X_Y);
                    //}
                }

        }


        // Hiermit erkläre ich...
        $pdf->Text(16.3, 188.4, 'X');


// SEITE 3
        $tplidx = $pdf->importPage(3, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        $this->getCompanyStamp(100.0, 5, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize'], 'horizontal', 'wide');

        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
        $pdf->Text(33.0, 7.5, $data['name']);

        // Unterschriften
        
        // Antragsteller
        if($i==0) $this->pdfRect(18.5, 223.0, 28.0, $pdf, Y); 
        if($i==0) $this->pdfRect(52.0, 223.0, 44.0, $pdf, Y);
        $pdf->Text(52, 225.8, 'X');



        // Unterschrift der mitzuversichernden Person ab 16 Jahre
        if(isset($data['personInsured']['age']))
        {
            if($data['personInsured']['age'] >= 16)
            {
                if($i==0) $this->pdfRect(103.5, 223.0, 44.0, $pdf, Y);
                $pdf->Text(105, 225.8, 'X');
            }
        }

        // Antragsteller minderjährig oder versicherte Person minderjährig
        if($data['person']['age'] <= 18 || ($data['personInsured']['age'] <=18) && (isset($data['personInsured']['age'])))
        {
            if($i==0) $this->pdfRect(152, 223.0, 44.0, $pdf, Y);
            $pdf->Text(153, 225.8, 'X');

            if($data['person']['age'] <= 18)
            {
                $this->pdfRect(113.0, 255.0, 82.0, $pdf, 3.2);
                $pdf->Text(115.0, 257.2, 'X');
            }
        }

        // Empfangsbestätigung
        if($i==0) {
            $this->pdfRect(17.5, 255.0, 82.0, $pdf, 3.2);
            $pdf->Text(18.5, 257.2, 'X');
        }

        // Reset
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
            // Makler
            $pdf->Text(148.6, 270.6, 'X');

        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
        // Vmtl-Nr
        $pdf->Text(18.5, 278.0, '226 / 1500');




// SEITE 4
        $tplidx = $pdf->importPage(4, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        $this->getCompanyStamp(110.0, 25, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize'], 'horizontal', 'wide');

        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        $pdf->Text(35.0, 6.2, $data['name']);
	 $pdf->Text(160.0, 6.2, $data['signupDate']);

        // Ich willige ein...
        #$pdf->Text(16.5, 21.8, 'X');

// SEITE 5
        $tplidx = $pdf->importPage(5, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        $this->getCompanyStamp(100.0, 5, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize'], 'horizontal', 'wide');

        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

	 $pdf->Text(35.0, 6.2, $data['name']);
	 $pdf->Text(35.0, 12.0, $data['signupDate']);



    if($complete==2) { $pdf->AddPage('P'); }

?>