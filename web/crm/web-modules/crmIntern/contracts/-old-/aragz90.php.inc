<?

// File for generating Contract PDF for ARAGz90
        if ($i==0) {
            $pagecount = $pdf->setSourceFile($path.'/files/arag/antrag/arag_antrag_2014.pdf');
        } else {
            $pagecount = $pdf->setSourceFile($path.'/files/arag/antrag/arag_antrag_2014_bw.pdf');
        }
		$tplidx = $pdf->importPage(1, '/MediaBox');
		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);

		$fontSize = 8;
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $fontSize+3, 0, 2);
		$pdf->Text(168.0, 4.5, 'JDMS-PP-Nr. 15700');

		if($i==0) $this->pdfRect(29, 1.0, 68, $pdf);
		// set title and font to big and bold!
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $fontSize+5, 0, 2);
		$pdf->Text(29.0, 5.0, $titles[$i]);

		$pdf->Text(41, 134, $data['begin']);

	// reset font
		$pdfCfg['fontSize'] = $fontSize;
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		// Makler Daten schreiben
		#$pdf->Text(40.5, 24.5, "5");
		#$pdf->Text(44.5, 24.5, "1");
		#$pdf->Text(49.0, 24.5, "7");
		#$pdf->Text(53.5, 24.5, "3");
		#$pdf->Text(58.0, 24.5, "0");

		#$pdf->Text(68.5, 24.5, "6");
		#$pdf->Text(73.0, 24.5, "0");
		#$pdf->Text(77.0, 24.5, "0");

	// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-2, 0, 2);

		$this->getCompanyStamp(128, 24, $pdf, $data['contractComplete']['sourcePage'], 8);

	// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);

		// Neuantrag
		$pdf->Text(31.8, 70.9, 'X');

 		if($data['gender']=='male')	{
			$pdf->Text(31.8, 80.5, 'X');
		} else {
			$pdf->Text(31.8, 80.5, 'X');
		}	

		$pdf->Text(44, 85, $data['name']);
 		$pdf->Text(38, 94, $data['street']);
 		$pdf->Text(146, 85.5, $data['birthdate']);

		if(empty($data['phone']))
		{
			if($i==0) $this->pdfRect(146, 99, 50, $pdf);
 		} else
			$pdf->Text(146, 103, $data['phone']);

		if(empty($data['email']))
		{
			if($i==0) $this->pdfRect(32, 108, 68, $pdf);
		} else
			$pdf->Text(38, 112, $data['email']);

 		$pdf->Text(38, 103, $data['postcode']);
 		$pdf->Text(60, 103, $data['city']);

		if(empty($data['job']))
		{
			if($i==0) $this->pdfRect(53.0, 102.0, 68, $pdf);
		} else
			$pdf->Text(60, 121, $data['job']);

		//mark Angestellter / Selbständiger
		if($i==0) $this->pdfRect(31.1, 114.5, 2, $pdf, 2);
 		if($i==0) $this->pdfRect(31.1, 118.5, 2, $pdf, 2);


	//Zahlungsweise
		//jährlich (4% skonto)
		if($i==0) $this->pdfRect(31.2, 138.9, 2, $pdf, 2);

		//halbjährlich (2% skonto)
		if($i==0) $this->pdfRect(64.4, 138.9, 2, $pdf, 2);

		//vierteljährlich
		if($i==0) $this->pdfRect(101.7, 138.9, 2, $pdf, 2);

		//monatlich
		if($i==0) $this->pdfRect(123.2, 138.9, 2, $pdf, 2);

		//vierteljährlich
		if($i==0) $this->pdfRect(101.7, 142.9, 2, $pdf, 2);

		//monatlich
		if($i==0) $this->pdfRect(131.4, 142.9, 2, $pdf, 2);

	//Zu versichernde Person 1
		// person insured
		$pdf->Text(35, 169, $data['street'].', '.
			$data['postcode'].' '.$data['city']);

		//mark Angestellter / Selbständiger
		if($i==0) $this->pdfRect(31.3, 169.7, 2, $pdf, 2);
 		if($i==0) $this->pdfRect(31.3, 173.6, 2, $pdf, 2);

		$pdf->Text(44, 159.5, $data['name2b']);
		$pdf->Text(147, 159.5, $data['birthdate2']);

		if(empty($data['job2']))
		{
			if($i==0) $this->pdfRect(52.5, 172.0, 68, $pdf);
		} else
			$pdf->Text(60, 177.5, $data['job2']);


			//print_r($data);

 		if($data['gender2']=='male')	{
			$pdf->Text(31.8, 154.6, 'X');
		} else {
			$pdf->Text(31.8, 157.7, 'X');
		}

		// prices
		//if($data['idt']==project::gti('aragz90')) {
			$pdf->Text(59.9, 246.9, "X");
			$pdf->Text(137, 252, $data['bonusbase']);
			$pdf->Text(180, 252, $data['bonusEuroFromPercent']);
			$pdf->Text(189.0, 286, $data['price']);
		//}


	// page 2
		$tplidx = $pdf->importPage(2, '/MediaBox');
		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);

		$pdf->Text(48, 42.5, "X");

		if(empty($data['insurance2']))
		{
			if($i==0) $this->pdfRect(55, 44, 86, $pdf);
		} else
			$pdf->Text(60.0, 48, $data['insurance2']);

		//2a, Person 1
		if($i==0) $this->pdfRect(128, 94, 2, $pdf, 2);
		if($i==0) $this->pdfRect(141, 94, 2, $pdf, 2);
		if($i==0) $this->pdfRect(128, 107, 2, $pdf, 2);
		if($i==0) $this->pdfRect(141, 107, 2, $pdf, 2);
		if($i==0) $this->pdfRect(128, 142, 2, $pdf, 2);
		if($i==0) $this->pdfRect(141, 142, 2, $pdf, 2);

	if($data['contractComplete']) {
		if($data['incare']) {
			$pdf->Text(128.5, 96.3, 'X');
		} else {
			$pdf->Text(140.5, 96.3, 'X');
		}

		// Paradontose Behandlung
		// "yes" 20140314
		if ($data['contractComplete']['periodontitis'] == 'yes') {
			$pdf->Text(128.5, 108.8, 'X');
		} else {
			$pdf->Text(140.5, 108.8, 'X');
		}
	}
		//2c
		if($i==0) $this->pdfRect(130, 120.5, 8, $pdf, 4);

	if($data['contractComplete']) {
		$tsum = $data['t1'] + $data['t2'];
		if ($tsum == 0) {
			$pdf->Text(140, 125, 'X');
		} 

		$pdf->Text(133.2, 124, $tsum);

		if ($data['contractComplete']['periodontitis'] != 'yes' && ( ! $data['incare']) && $tsum > 0) {
			$pdf->Text(128.5, 144, 'X'); // b, insure toothes, yes
		} else {
			$pdf->Text(140.5, 144, 'X'); // b, insure toothes, no
		}
	}


	// page 3
		$tplidx = $pdf->importPage(3, '/MediaBox');
		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);

	//Unterschriften
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $fontSize+5, 0, 2);

		//Ort,Datum
		if($i==0) $this->pdfRect(40, 172.0, 65, $pdf, 6);

		//Antragsteller
		if($i==0) $this->pdfRect(118, 172.0, 65, $pdf, 6);

		//1. zu versichernde volljährige Person
		if($i==0) $this->pdfRect(40, 182, 65, $pdf, 6);


			$pdf->Text(40.5, 176.5, 'X');
			$pdf->Text(118.5, 176.5, 'X');
			$pdf->Text(40.5, 187.5, 'X');

		$agePersonInsured = actionHelper::getAge($data['personInsured']['birthdate']);

		if($agePersonInsured >= 16 && $agePersonInsured < 18) {
			if($i==0) $this->pdfRect(118, 183.0, 65, $pdf, 6);
			$pdf->Text(118.5, 187.5, 'X');
		}


		// Empfangsbestätigung
		//Ort,Datum
		if($i==0) $this->pdfRect(40, 247.2, 65, $pdf, 6);

		//Antragsteller
		if($i==0) $this->pdfRect(118, 247.2, 65, $pdf, 6);

		//1. zu versichernde volljährige Person
		if($i==0) $this->pdfRect(40, 265.5, 65, $pdf, 6);


			$pdf->Text(40.5, 252.2, 'X');
			$pdf->Text(118.5, 252.2, 'X');
			$pdf->Text(40.5, 270.5, 'X');

		$agePersonInsured = actionHelper::getAge($data['personInsured']['birthdate']);

		if($agePersonInsured >= 16 && $agePersonInsured < 18) {
			if($i==0) $this->pdfRect(118, 257.5, 65, $pdf, 6);
			$pdf->Text(118, 262.5, 'X');
		}

		$pdf->SetFont($pdfCfg['fontFamily'], '', $fontSize+3, 0, 2);

	// page 4 SEPA
		$tplidx = $pdf->importPage(4, '/MediaBox');
		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);

		$pdf->Text(11, 48.6, 'x');
		$pdf->Text(40, 53, $data['signupDate']);

 		if($data['gender']=='male')	{
			$pdf->Text(31.8, 75.5, 'X');
		} else {
			$pdf->Text(31.8, 78.5, 'X');
		}	

		$pdf->Text(46, 80, $data['name']);
 		$pdf->Text(46, 89, $data['street']);
 		$pdf->Text(46, 98, $data['postcode']);
 		$pdf->Text(70, 98, $data['city']);

		if($i==0) $this->pdfRect(32, 136, 90, $pdf, 5);
		if($i==0) $this->pdfRect(32, 145, 90, $pdf, 5);
		if($i==0) $this->pdfRect(32, 154, 40, $pdf, 5);
		
		if($i==0) $this->pdfRect(32, 181, 77, $pdf, 8);
		if($i==0) $this->pdfRect(119, 181, 77, $pdf, 8);

		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $fontSize+4, 0, 2);
		$pdf->Text(34, 186, 'X');
		$pdf->Text(122, 186, 'X');

	// page 5
		$tplidx = $pdf->importPage(5, '/MediaBox'); 
		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 9, -1, 193);
	// page 6
		$tplidx = $pdf->importPage(6, '/MediaBox');
		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);
?>