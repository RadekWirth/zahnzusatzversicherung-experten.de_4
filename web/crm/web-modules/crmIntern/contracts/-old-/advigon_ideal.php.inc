<?
$vermittlernummer = '80001938';
$age = actionHelper::getAge($data['personInsured']['birthdate']);

// File for generating Contract PDF for advigon 
    if($i==0) {
        $pagecount = $pdf->setSourceFile($path.'/files/advigon/advigon_zahnantrag_A.pdf');
    } else {
        $pagecount = $pdf->setSourceFile($path.'/files/advigon/advigon_zahnantrag_A_bw.pdf');
    }

/**
 * Page 1
 */
    $tplidx = $pdf->importPage(1, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

	$pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);

	//gelbe markierungen nur auf rückantwort drucken! 
	if($i==0) $this->pdfRect(75, 4, 68, $pdf);
	$pdf->Text(75.0, 8.0, $titles[$i]);

	$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);
	    #$this->getCompanyLogo(10, 3, $pdf, $data['contractComplete']['sourcePage'], $i, 50);
   		$this->getCompanyStamp(80, 16, $pdf, $data['contractComplete']['sourcePage'], 8, 'horizontal', 'wide');
	$pdf->SetFont($pdfCfg['fontFamily'], '', 11, 0, 2);

	$pdf->Text(32, 24, $vermittlernummer);

	$pdf->Text(10.2, 43.1, 'X');
	$pdf->Text(15.2, 50.5, 'X');

	$pdf->Text(13, 105, $data['name']);
	$pdf->Text(13, 117, $data['job']);
	$pdf->Text(13, 129, $data['street']);
	$pdf->Text(13, 141, $data['postcode']);
	$pdf->Text(39, 141, $data['city']);

	if ($data['gender'] !== 'male') {
		$pdf->Text(179.5, 106, 'X');
	} else {
		$pdf->Text(189.4, 106, 'X');
	}
	$pdf->Text(142, 117, $data['birthdate']);
	if (empty($data['phone'])) {
		if ($i == 0) $this->pdfRect(140, 125, 58, $pdf, 4);
	} else {
		$pdf->Text(142, 129, $data['phone']);
	}
	if (empty($data['email'])) {
		if ($i == 0) $this->pdfRect(140, 137, 58, $pdf, 4);
	} else {
		if (strlen($data['email']) > 30) {
			$pdf->SetFont($pdfCfg['fontFamily'], '', 8, 0, 2);
		}
		$pdf->Text(142, 141, $data['email']);
		$pdf->SetFont($pdfCfg['fontFamily'], '', 11, 0, 2);
	}

	$pdf->Text(13, 169, $data['name2b']);
	$pdf->Text(96, 169, $data['birthdate2']);
	if (strlen($data['job2']) > 25) {
			$pdf->SetFont($pdfCfg['fontFamily'], '', 8, 0, 2);
		}
	$pdf->Text(126, 169, $data['job2']);
	$pdf->SetFont($pdfCfg['fontFamily'], '', 11, 0, 2);
	if ($data['gender'] !== 'male') {
		$pdf->Text(181.5, 170, 'X');
	} else {
		$pdf->Text(191.4, 170, 'X');
	}
	$pdf->Text(126, 178, $data['insurance2']);

	if ($i == 0) $this->pdfRect(179, 186, 2.5, $pdf, 2.5);
	if ($i == 0) $this->pdfRect(189, 186, 2.5, $pdf, 2.5);	
	if ($i == 0) $this->pdfRect(179, 196, 2.5, $pdf, 2.5);
	if ($i == 0) $this->pdfRect(189, 196, 2.5, $pdf, 2.5);

    if($i==0) $this->pdfClean(10, 218, 190.0, $pdf, 0, 0, 0, 0.3, 'F');
    if($i==0) $this->pdfClean(10, 228, 190.0, $pdf, 0, 0, 0, 0.3, 'F');
    if($i==0) $this->pdfClean(10, 238, 190.0, $pdf, 0, 0, 0, 0.3, 'F');
    if($i==0) $this->pdfClean(10, 249, 190.0, $pdf, 0, 0, 0, 0.3, 'F');
    if($i==0) $this->pdfClean(10, 255, 190.0, $pdf, 0, 0, 0, 0.3, 'F');

	$this->pdfClean(94, 286, 20, $pdf, 255, 255, 255, 6, 'F');

/** 
 * Page 2
 */
    $tplidx = $pdf->importPage(2, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

	$pdf->Text(110, 14, $data['name']);

	$pdf->Text(71.5, 43, 'x');
	$pdf->Text(123, 48, 'x');

	$pdf->Text(34, 77.5, $data['begin']);
	$pdf->Text(75, 77.5, $data['price']);

	if ($i == 0) $this->pdfRect(122.5, 76.5, 2.5, $pdf, 2.5);
	if ($i == 0) $this->pdfRect(145, 76.5, 2.5, $pdf, 2.5);

	if ($i == 0) $this->pdfRect(165, 122.6, 2.5, $pdf, 2.5);
	if ($i == 0) $this->pdfRect(172.6, 122.6, 2.5, $pdf, 2.5);
	if ($data['contractComplete']['incare'] === 'yes') {
		$pdf->Text(165, 125, 'x');
	} else {
	if($data['contractComplete'])
		$pdf->Text(173, 125, 'x');
	}

	if ($age > 8) {
		if ($i == 0) $this->pdfRect(165, 140.6, 2.5, $pdf, 2.5);
		if ($i == 0) $this->pdfRect(172.6, 140.6, 2.5, $pdf, 2.5);
		if (($data['contractComplete']['periodontitis'] === 'yes' && empty($data['contractComplete']['periodontitisCured'])) || $data['contractComplete']['docControl'] === 'no') {
			$pdf->Text(165, 143, 'x');
		} else {
	if($data['contractComplete'])
			$pdf->Text(173, 143, 'x');
		} 
	} 
	if ($age > 16) {
		if ($i == 0) $this->pdfRect(165, 157.6, 2.5, $pdf, 2.5);
		if ($i == 0) $this->pdfRect(172.6, 157.6, 2.5, $pdf, 2.5);
		if ($i == 0) $this->pdfRect(162.2, 188.3, 2.5, $pdf, 2.5);
		if ($i == 0) $this->pdfRect(169.8, 188.3, 2.5, $pdf, 2.5);
		if ($i == 0) $this->pdfRect(163, 193.5, 16, $pdf, 4);
		if ($i == 0) $this->pdfRect(163, 201, 16, $pdf, 4);
		if ($data['contractComplete']['biteSplint'] === 'yes') {
			$pdf->Text(165, 159.8, 'x');
		}  
		if ($data['contractComplete']['tooth1'] > 0) {
			$pdf->Text(163, 190.4, 'x');
			$pdf->Text(163, 197, $data['contractComplete']['tooth1']);
		} else {
		if($data['contractComplete'])
			$pdf->Text(171, 190.4, 'x');
		}
		if ($i == 0) $this->pdfRect(164.3, 233.2, 2.5, $pdf, 2.5);
		if ($i == 0) $this->pdfRect(171.9, 233.2, 2.5, $pdf, 2.5);
	}

	$this->pdfClean(94, 287, 20, $pdf, 255, 255, 255, 6, 'F');

/**
 * Page 3
 */
    $tplidx = $pdf->importPage(3, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

	$pdf->Text(110, 14, $data['name']);
	if ($i == 0) $this->pdfClean(94, 287.2, 20, $pdf, 255, 255, 255, 6, 'F');

/**
 * Page 4
 */
    $tplidx = $pdf->importPage(4, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

	$pdf->Text(110, 14, $data['name']);
	$this->pdfClean(94, 287.2, 20, $pdf, 255, 255, 255, 6, 'F');

/**
 * Page 5
 */
    $tplidx = $pdf->importPage(5, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

	$pdf->Text(110, 14, $data['name']);

    //Markieren der Datumsfelder
    $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);

    // Antragsteller Ort, Datum, Unterschrift
    if($i==0) $this->pdfRect(11.8,85.4,75,$pdf);
    if($i==0) $pdf->Text(12, 89.2, 'x');
    if($i==0) $this->pdfRect(90.5,85.4,107,$pdf);
    if($i==0) $pdf->Text(91, 89.2, 'x');

    if($age > 17) {
        // Mitzuversichernder Person I, Ort, Datum, Unterschrift
        if($i==0) $this->pdfRect(11.8,102.2,75,$pdf);
        if($i==0) $pdf->Text(12, 106, 'x');
        if($i==0) $this->pdfRect(90.5,102.2,107,$pdf);
        if($i==0) $pdf->Text(91, 106, 'x');
    } else {
        if($age > 15 && $age < 18) {
            // Gesetzlich vertretene Person I, Ort, Datum, Unterschrift
            if($i==0) $this->pdfRect(11.8,118.2,75,$pdf);
            if($i==0) $pdf->Text(12, 122, 'x');
            if($i==0) $this->pdfRect(90.5,118.2,107,$pdf);
            if($i==0) $pdf->Text(91, 122, 'x');
        }
            // Gesetzlicher Vertreter von vertretener Person I, Ort, Datum, Unterschrift
            if($i==0) $this->pdfRect(11.8,131.8,75,$pdf);
            if($i==0) $pdf->Text(12, 135.6, 'x');
            if($i==0) $this->pdfRect(90.5,131.8,107,$pdf);
            if($i==0) $pdf->Text(91, 135.6, 'x');
    }

    if($i==0) $this->pdfClean(10, 151.7, 190.0, $pdf, 0, 0, 0, 0.3, 'F');
    if($i==0) $this->pdfClean(10, 168.2, 190.0, $pdf, 0, 0, 0, 0.3, 'F');
    if($i==0) $this->pdfClean(10, 181.5, 190.0, $pdf, 0, 0, 0, 0.3, 'F');

    $pdf->SetFont($pdfCfg['fontFamily'], '', 11, 0, 2);

	$this->pdfClean(94, 287.2, 20, $pdf, 255, 255, 255, 6, 'F');

/**
 * Page 6
 */
    $tplidx = $pdf->importPage(6, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

	$pdf->Text(110, 14, $data['name']);

    // Name, Vorname (Kontoinhaber)
    $pdf->Text(12, 113, $data['namec']);

    // Anschrift
    $pdf->Text(12, 125, $data['postcode'] .' '. $data['city'] .', '. $data['street']);

    //if($i==0) $this->pdfRect(11.5,109.5,187.5,$pdf);
    //if($i==0) $this->pdfRect(11.5,121.5,187.5,$pdf);
    if($i==0) $this->pdfRect(11.5,132.9,187.5,$pdf);

    if($i==0) {
        $x = 17.5;
        for($c = 0; $c < 20; $c++) {
            $this->pdfRect($x += 5.6,144.9,4.9,$pdf);
        }
        $x = 132.2;
        for($c = 0; $c < 11; $c++) {
            $this->pdfRect($x += 5.6,144.9,4.9,$pdf);
        }
    }

    if($i==0) $this->pdfRect(11.5,156.9,75,$pdf);
    if($i==0) $this->pdfRect(91,156.9,108.5,$pdf);

    $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);

    if($i==0) $pdf->Text(13, 161.4, 'x');
    if($i==0) $pdf->Text(92, 161.4, 'x');

	$pdf->SetFont($pdfCfg['fontFamily'], '', 11, 0, 2);

	$this->pdfClean(94, 287.2, 20, $pdf, 255, 255, 255, 6, 'F');

/**
 * Page 7
 */
    $tplidx = $pdf->importPage(7, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

	$pdf->Text(110, 14, $data['name']);

    //Markieren der Datumsfelder
    if($i==0) $this->pdfRect(10.8,204.1,76,$pdf);
    if($i==0) $this->pdfRect(10.8,217.2,76,$pdf);

    //Markieren der Unterschriftfelder
    if($i==0) $this->pdfRect(89.5,204.1,110,$pdf);
    if($i==0) $this->pdfRect(89.5,217.2,110,$pdf);

    //Hinzufügen von X
    if($i==0) {
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);

        $pdf->Text(11, 208.4, 'x'); $pdf->Text(90, 208.4, 'x');
        $pdf->Text(11, 220.8, 'x'); $pdf->Text(90, 220.8, 'x');

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
    }

	$this->pdfClean(94, 287.2, 20, $pdf, 255, 255, 255, 6, 'F');

	if ($complete == 2) {
		$pdf->addPage('P');
	}
