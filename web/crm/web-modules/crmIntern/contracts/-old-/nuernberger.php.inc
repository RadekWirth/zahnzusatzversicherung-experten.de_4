<?php

$pagecount = $pdf->setSourceFile($path.'files/nuernberger/Antrag/2020/Nuernberger.neuerAntrag.092020.pdf');


$tplidx = $pdf->importPage(1, '/MediaBox');

$pdf->addPage('P');
$pdf->useTemplate($tplidx, 5, 17, 190);

// Title
if($i==0) $this->pdfRect(74, 2, 68, $pdf);
$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);
$pdf->Text(74.0, 6.0, $titles[$i]);

$this->getCompanyStamp(72.0, 11, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']-2, 'horizontal', 'wide');

$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);


$pdf->Text(23.5, 66.0, '115/38/3392');

// Neuantrag
$pdf->Text(23.5, 73.9, 'X');


if($data['gender'] == 'male') {
    $pdf->Text(143.6, 111.7, 'X');
} else {
    $pdf->Text(143.6, 114.7, 'X');
}

$pdf->Text(24, 113.5, $data['name']);
$pdf->Text(24, 121.5, $data['street']);
$pdf->Text(24, 129.0, $data['postcode']);
$pdf->Text(48, 129.0, $data['city']);

$pdf->Text(112.5, 113.5, $data['birthdate']);



if(empty($data['nation'])) {
    if($i==0) $this->pdfRect(23, 134.4, 36, $pdf, 3);
} elseif($data['nation']=='DEU') {
    $pdf->Text(24, 136.4, 'deutsch');
} else {
    $pdf->Text(24, 136.4, $data['nation']);
}

if(empty($data['job'])) {
    if($i==0) $this->pdfRect(64, 134.4, 36, $pdf, 3);
} else {
    $pdf->Text(66, 136.4, $data['job']);
}


if(empty($data['email'])) {
    if($i==0) $this->pdfRect(111.5, 119.5, 36, $pdf, 3);
} else {
    $pdf->Text(112.5, 121.5, $data['email']);
}

if(empty($data['phone'])) {
    if($i==0) $this->pdfRect(111.5, 127.0, 36, $pdf, 3);
} else {
    $pdf->Text(112.5, 129.0, $data['phone']);
}


/* Person Insured */
$pdf->Text(25, 152.6, '1');


if($data['personInsured']['surname'] !== $data['person']['surname'])
{
    $pdf->Text(36, 152.6, $data['personInsured']['surname'] .', '. $data['personInsured']['forename']);
} else {
    $pdf->Text(36, 152.6, $data['personInsured']['forename']);
}


$pdf->Text(92, 152.6, $data['birthdate2']);

if ($data['personInsured']['salutationLid'] == 9) {
    $pdf->Text(82.7, 153.0, 'x');
} else {
    $pdf->Text(85.7, 153.0, 'x');
}


if ($i == 0) $this->pdfRect(23.0, 155.5, 1.2, $pdf, 1.2);
if ($i == 0) $this->pdfRect(43.0, 155.5, 1.2, $pdf, 1.2);
if ($i == 0) $this->pdfRect(65.0, 155.5, 1.2, $pdf, 1.2);
if ($i == 0) $this->pdfRect(91.0, 155.5, 1.2, $pdf, 1.2);


if ($data['personInsured']['job']) {
    $pdf->Text(116, 153.0, $data['personInsured']['job']);
} else {
    if($i==0) $this->pdfRect(114, 150.5, 50, $pdf, 3);
}


$pdf->Text(23.8, 201.0, '1');

$begin = explode('.', $data['begin']);
$pdf->Text(32.5, 201.0, $begin[0]);
$pdf->Text(37.9, 201.0, $begin[1]);



// Welcher Tarif?
$pdfTemplate = isset($pdfTemplate)?$pdfTemplate:$data['idt'];
switch($pdfTemplate) {
    case project::gti('nuern-z80'):
        $pdf->Text(54.0, 201.0, 'Z80');
        break;
    case project::gti('nuern-z90'):
        $pdf->Text(54.0, 201.0, 'Z90');
        break;
    case project::gti('nuern-z100'):
        $pdf->Text(54.0, 201.0, 'Z100');
        break;
}

$pdf->Text(68, 201, $data['price']. ' €');

$this->pdfClean(70, 218.5, 8, $pdf,  255,255,255,3.3);
$pdf->Text(68, 220, $data['price']. ' €');


$pdf->Text(33.8, 258.8, '1');
if($data['personInsured']['insurance'])
{
    $pdf->Text(26, 262, $data['personInsured']['insurance']);
} else {
    if($i==0) $this->pdfRect(24.5, 260.5, 34, $pdf, 3);
}

$this->pdfClean(160, 270.5, 20, $pdf,  255,255,255,3.6);



$tplidx = $pdf->importPage(2, '/MediaBox');

$pdf->addPage('P');
$pdf->useTemplate($tplidx, 5, 7.4, 197.6);

if ($i == 0) $this->pdfRect(38.8, 50.3, 1.4, $pdf, 1.4);
if ($i == 0) $this->pdfRect(59.9, 50.3, 1.4, $pdf, 1.4);
if ($i == 0) $this->pdfRect(81.4, 50.3, 1.4, $pdf, 1.4);

if ($i == 0) $this->pdfRect(24.5, 58.3, 1.4, $pdf, 1.4);
if ($i == 0) $this->pdfRect(40.0, 58.3, 1.4, $pdf, 1.4);

// Herr Frau Firma
if ($i == 0) $this->pdfRect(105.2, 107.5, 1.4, $pdf, 1.4);
if ($i == 0) $this->pdfRect(117.9, 107.5, 1.4, $pdf, 1.4);
if ($i == 0) $this->pdfRect(130.5, 107.5, 1.4, $pdf, 1.4);


// Name
if ($i == 0) $this->pdfRect(25.0, 111.0, 56, $pdf, 3);
if ($i == 0) $this->pdfRect(25.0, 120.0, 56, $pdf, 3);

if ($i == 0) $this->pdfRect(25.0, 129.3, 6, $pdf, 3);
if ($i == 0) $this->pdfRect(38.0, 129.3, 17, $pdf, 3);
if ($i == 0) $this->pdfRect(63.5, 129.3, 56, $pdf, 3);


if ($i == 0) $this->pdfRect(25.0, 160.3, 12, $pdf, 3);
if ($i == 0) $this->pdfRect(44.0, 160.3, 12, $pdf, 3);
if ($i == 0) $this->pdfRect(63.0, 160.3, 12, $pdf, 3);
if ($i == 0) $this->pdfRect(82.0, 160.3, 12, $pdf, 3);
if ($i == 0) $this->pdfRect(101.0, 160.3, 12, $pdf, 3);
if ($i == 0) $this->pdfRect(120.0, 160.3, 12, $pdf, 3);
if ($i == 0) $this->pdfRect(139.0, 160.3, 12, $pdf, 3);

if ($i == 0) $this->pdfRect(25.0, 169.3, 100, $pdf, 3);

if ($i == 0) $this->pdfRect(25.0, 183.3, 26, $pdf, 3);
if ($i == 0) $this->pdfRect(85.0, 183.3, 56, $pdf, 3);

$this->pdfClean(162, 269.8, 22, $pdf,  255,255,255,3.3);


for($p=3;$p<=10;$p++)
{
    $tplidx = $pdf->importPage($p, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 4.7, 6.4, 199);

    $this->pdfClean(161, 270.9, 24, $pdf,  255,255,255,3.3);


    if($p == 5)
    {
        if ($i == 0) $this->pdfRect(27.5, 92.4, 40, $pdf, 3);
        if ($i == 0) $this->pdfRect(105.9, 92.4, 40, $pdf, 3);
        if ($i == 0) $this->pdfRect(83.9, 92.4, 14, $pdf, 3);

        if ($i == 0) $this->pdfRect(27.5, 123.8, 40, $pdf, 3);
        if ($i == 0) $this->pdfRect(105.9, 123.8, 40, $pdf, 3);
        if ($i == 0) $this->pdfRect(83.9, 123.8, 14, $pdf, 3);

        $pdf->Text(24.1, 137.5, 'x');
        $pdf->Text(27.3, 141.5, 'x');
        $pdf->Text(84.2, 141.5, 'x');

        $pdf->Text(30, 177.1, 'Versicherungsmakler Experten GmbH, Feursstr. 56 / RGB, 82140 Olching');
        $pdf->Text(30, 184.3, 'allgemein@vm-experten.de, 08142 651 39 28');
        
    }
}