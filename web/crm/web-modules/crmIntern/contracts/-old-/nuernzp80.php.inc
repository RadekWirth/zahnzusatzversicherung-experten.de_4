<?
		$pagecount = $pdf->setSourceFile($path.'/files/Nuernberger.ZP80.ZV.Antrag.2012.pdf');
		$tplidx = $pdf->importPage(1, '/MediaBox');

		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);

		// set title and font to big and bold!
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);

		if($i==0) $this->pdfRect(12.0, 9.0, 68, $pdf);
		$pdf->Text(12.0, 13.0, $titles[$i]);
		
		$this->getCompanyStamp(20.0, 18, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']-2, 'horizontal', 'wide');

		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		// Neuantrag
		$pdf->Text(17.0, 65.4, "X");


		// Linke Seite
		// Begin
		$pdf->Text(73, 103.3, $data['begin']);

		// m/w Antragsteller
		$data['gender']=="female"?$pdf->Text(92.4, 109.3, "X"):$pdf->Text(75.7, 109.5, "X");

		$pdf->Text(20, 118, $data['name']);
		$pdf->Text(20, 127, $data['street']);
		$pdf->Text(20, 136, $data['postcode']);
		$pdf->Text(37, 136, $data['city']);
		$pdf->Text(48, 145, $data['nation']);
		$pdf->Text(20, 145, $data['birthdate']);

		if(empty($data['job'])) {
			if($i==0) $this->pdfRect(17.5, 169, 80, $pdf, 3.5);
		} else $pdf->Text(20, 172, $data['job']);

		//Beruf/Branche gelb markieren
		if($i==0) $this->pdfRect(17.5, 178, 80, $pdf, 3.5);

		if(empty($data['phone'])) {
			if($i==0) $this->pdfRect(17.5, 150.6, 41, $pdf, 3.5);
		} else $pdf->Text(20, 153.8, $data['phone']);

		if(empty($data['email'])) {
			if($i==0) $this->pdfRect(17.5, 160, 80, $pdf, 3.5);
		} else $pdf->Text(20, 163, $data['email']);

#print_r($data);
		if($data['name'] != $data['name2b'])
			$pdf->Text(18, 196.7, $data['name2']);
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
		$pdf->Text(18, 204, $data['birthdate2']);

		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		$data['gender2']=="female"?$pdf->Text(34, 204, "x"):$pdf->Text(34, 200, "x");
		if(empty($data['job2'])) {
			if($i==0) $this->pdfRect(17.5, 207.5, 60, $pdf, 3.5);
		} else $pdf->Text(18, 210.6, $data['job2']);


		// Selbstst�ndig,Arbeitnehmer,Nicht erwerbst�tig
		if($i==0) $this->pdfRect(42, 198.8, 2, $pdf, 2);
		if($i==0) $this->pdfRect(60, 198.8, 2, $pdf, 2);
		if($i==0) $this->pdfRect(42, 201.6, 2, $pdf, 2);

		$pdf->Text(90.0, 203, $data['price']);
		$pdf->Text(90.0, 260, $data['price']);

		// Rechte Seite
		if($i==0) $this->pdfRect(131, 72, 3.0, $pdf, 8.0);
		if($i==0) $this->pdfRect(144, 76, 26.0, $pdf, 3.5);

		if($data['t1']>0) {
		$pdf->Text(131.4, 79.3, "x");
		$pdf->Text(148, 79, $data['t1']);
		} else $pdf->Text(131.4, 75.5, "x");

		// 2. Inlays angeraten) Frage 6 Rechner
		if($i==0) $this->pdfRect(113, 112.7, 82, $pdf, 3.5);		
		if($i==0) $this->pdfRect(131, 105, 3.0, $pdf, 3.0);
		if($i==0) $this->pdfRect(131, 109, 3.0, $pdf, 3.0);
		if($data['incare']=='yes') {
			$pdf->Text(131.4, 110, "x");
		} else $pdf->Text(131.4, 108, "x");
#print_r($data);

		// Sonst. Versicherungen
		if($data['name'] == $data['name2b']) {
			if(empty($data['insurance'])) {
				if($i==0) $this->pdfRect(112, 250, 60, $pdf, 3.5);
				if($i==0) $this->pdfRect(178, 250, 18, $pdf, 3.5);
			} else {
				$pdf->Text(113, 253, $data['insurance']);			
				$pdf->Text(177, 253, $data['insuredSince']);
			}
		} 
		if($data['name'] != $data['name2b']) {
			if(empty($data['insurance2'])) {
				if($i==0) $this->pdfRect(112, 250, 60, $pdf, 3.5);
				if($i==0) $this->pdfRect(178, 250, 18, $pdf, 3.5);
			} else {
				$pdf->Text(113, 253, $data['insurance2']);
				$pdf->Text(177, 253, $data['insuredSince2']);
			}
		}


	// SEITE 2

		$tplidx = $pdf->importPage(2, '/MediaBox');

		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);

		// Zahlweise markieren
		if($i==0) $this->pdfRect(46, 10, 2, $pdf, 2);
		if($i==0) $this->pdfRect(46, 14, 2, $pdf, 2);
		if($i==0) $this->pdfRect(76.5, 10, 2, $pdf, 2);
		if($i==0) $this->pdfRect(76.5, 14, 2, $pdf, 2);

		// Bankverbindung markieren
		if($i==0) $this->pdfRect(17, 29, 40, $pdf);
		if($i==0) $this->pdfRect(63, 29, 40, $pdf);
		if($i==0) $this->pdfRect(109, 29, 85, $pdf);
		if($i==0) $this->pdfRect(17, 37, 2, $pdf, 2);
		if($i==0) $this->pdfRect(63, 39.5, 85, $pdf);
		if($i==0) $this->pdfRect(154, 39.5, 40, $pdf);
		if($i==0) $this->pdfRect(63, 49.4, 85, $pdf);
		if($i==0) $this->pdfRect(154, 49.4, 40, $pdf);

		// Ort, Datum markieren
		if($i==0) $this->pdfRect(17, 124.8, 64, $pdf);
		if($i==0) $this->pdfRect(85, 124.8, 17, $pdf);
		if($i==0) $this->pdfRect(110.5, 124.8, 85, $pdf);
		if($i==0) $this->pdfRect(110.5, 137.3, 85, $pdf);

		if($i==0) $this->pdfRect(17, 159, 64, $pdf);
		if($i==0) $this->pdfRect(85, 159, 17, $pdf);
		if($i==0) $this->pdfRect(110, 159, 85, $pdf);

		// set font to big and bold!
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);
		$pdf->Text(111, 129, "X");
		$pdf->Text(111, 141.9, "X");
		$pdf->Text(111, 163, "X");
	 
		// reset font!
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		// Verbraucherinformationen
		$pdf->Text(17.3, 177, "X");
		$pdf->Text(20.6, 181.2, "X"); 
		$pdf->Text(93.8, 181.2, "X");

		$pdf->Text(18, 200.8, "Versicherungsmakler Experten GmbH - Tel: 089 - 356 576 72");

	/*	// Abschlussstelle
		$pdf->Text(106.0, 116.0, "113");
		// Betreuungsstelle
		$pdf->Text(129.0, 116.0, "12");
		// Inkassostelle
		$pdf->Text(152.0, 116.0, "3404"); */

	// SEITE 3
		$tplidx = $pdf->importPage(3, '/MediaBox');

		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);


	// SEITE 4
	// Vorraussetzung ist, dass der Versicherungsnehmer fehlende Z�hne hat,
	// die nicht zu versichern sind

	if($data['t1']>0  && $data['contractComplete']['insureTooth1']=='no') { 
		$pdf->AddPage();
		$pdf->Image($path.'/files/Nuernberger.Leistungsausschluss.fehlende.Zaehne.png', 0, 0, 208);


		if($i==0) $this->pdfRect(20.0, 43.5, 40.0, $pdf, 5.0);

		$pdf->Text(74.0, 47.0, $data['name']);

		if(empty($data['name2b'])) {
		$pdf->Text(128.0, 47.0, $data['name']);
		} else 
		$pdf->Text(128.0, 47.0, $data['name2b']);

		if($i==0) $this->pdfRect(28.0, 80.0, 90.0, $pdf, 5.0);

		if($i==0) $this->pdfRect(34.0, 118.0, 42.0, $pdf, 5.0);
		if($i==0) $this->pdfRect(118.0, 118.0, 50.0, $pdf, 5.0);		

		
	} 
	elseif($complete==2) { $pdf->AddPage(); }
?>