<?

/**
 * Inter Antrag
 * Erstellungsdatum 28.11.2014
 * Neu 11.10.2016
 * Neu 02.09.2017
 * Neu 02.06.2018
 *
 */

// Vars
$personInsuredOther = $data['person']['pid'] !== $data['personInsured']['pid'] ? true : false;
$isGerman = (strtolower($data['nation']) === 'deutsch' || $data['nation'] === 'DEU') ? true : false;
$personIsMale = $data['gender'] === 'male' ? true : false;
$personInsuredIsMale = $data['gender2'] === 'male' ? true : false;

// Const
define('CROWNS', $data['contractComplete']['tooth4'] ? $data['contractComplete']['tooth4'] : null);
define('MISSING_TOOTH', $data['t1'] ? $data['t1']  : null);
define('AGE_PERSONINSURED', actionHelper::getAge($data['personInsured']['birthdate']));
define('FAMILY_STATUS_SINGLE', 37);
define('FAMILY_STATUS_MARRIED', 38);
define('FAMILY_STATUS_WIDOWED', 39);
define('FAMILY_STATUS_DIVORCED', 40);

if ($i==0) {
    $pagecount = $pdf->setSourceFile($path.'files/inter/antrag-4-2018.pdf');
} else {
    $pagecount = $pdf->setSourceFile($path.'files/inter/antrag-4-2018.pdf');
}


/**
 * Page 1
 */
    $tplidx = $pdf->importPage(1, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

    $this->getCompanyStamp(36.0, 11.8, $pdf, $data['contractComplete']['sourcePage'], 9, 'horizontal', 'wide');

    //gelbe markierungen nur auf rückantwort drucken! 
    $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);
    if($i==0) $this->pdfRect(75, 2, 68, $pdf);
    $pdf->Text(75.0, 6, $titles[$i]);

    $pdf->SetFont($pdfCfg['fontFamily'], '', 9, 0, 2);

    $pdf->Text(164.0, 7.0, 'Vermittlernummer: 675363');

    // PBD / OBD
    $pdf->Text(149.4, 34.0, '678');
    $pdf->Text(157.4, 34.0, '678');

    // Person Data - Left
    $personIsMale ? $pdf->Text(97.2, 71, 'X') : $pdf->Text(105.5, 71, 'X');
    $pdf->Text(25, 75.2, $data['namec']);
    $pdf->Text(25, 85.5, $data['birthdate']);
    
    if ($data['person']['familyStatusLid']) {
        switch ($data['person']['familyStatusLid']) {
            case FAMILY_STATUS_SINGLE:
                $text = 1;
                break;
            case FAMILY_STATUS_MARRIED:
                $text = 2;
                break;
            case FAMILY_STATUS_DIVORCED:
                $text = 3;
                break;
            case FAMILY_STATUS_WIDOWED:
                $text = 4;
                break;
        }   
    } else {
        if ($i == 0) $this->pdfRect(37, 87.9, 3, $pdf, 3);
    }

    $pdf->Text(37.8, 90.3, $text);

    if ($isGerman) {
        $pdf->Text(50, 99.1, 'X');
    } elseif ($data['nation']) {
        $pdf->Text(68, 98.9, $data['nation']);
    }

    if ($i == 0) $this->pdfRect(23, 106.1, 3, $pdf, 3);  
    if ($i == 0) $this->pdfRect(51.4, 106.1, 3, $pdf, 3);
    if ($i == 0) $this->pdfRect(23, 111.1, 3, $pdf, 3); 
    if ($i == 0) $this->pdfRect(51.4, 111.1, 3, $pdf, 3);  
    if ($i == 0) $this->pdfRect(23, 116.1, 3, $pdf, 3);  
    if ($i == 0) $this->pdfRect(51.4, 116.1, 3, $pdf, 3);
    if ($i == 0) $this->pdfRect(85.2, 116.1, 3, $pdf, 3);
    if ($i == 0) $this->pdfRect(23, 121.1, 3, $pdf, 3); 
    if ($i == 0) $this->pdfRect(51.4, 121.1, 3, $pdf, 3);      
    if ($i == 0) $this->pdfRect(23, 131, 3, $pdf, 3); 
    if ($i == 0) $this->pdfRect(51.4, 131, 3, $pdf, 3);      
    if ($i == 0) $this->pdfRect(23, 135.7, 3, $pdf, 3); 
    if ($i == 0) $this->pdfRect(67.6, 120.5, 40, $pdf, 3); 
    if ($i == 0) $this->pdfRect(39.4, 135.3, 71, $pdf, 3); 
    if ($i == 0) $this->pdfRect(89.6, 110.9, 19, $pdf, 3); 
    if ($i == 0) $this->pdfRect(89.6, 105.9, 19, $pdf, 3);
    if ($i == 0) $this->pdfRect(23, 145.2, 3, $pdf, 3); 
    if ($i == 0) $this->pdfRect(32, 145.2, 3, $pdf, 3);      

    if ($data['job']) {
        $pdf->Text(115, 75.1, $data['job']);
    } else {
        if ($i == 0) $this->pdfRect(114, 72.9, 80, $pdf, 3);
    }

    // Person Data - Right
    $pdf->Text(115, 85, $data['street']);
    $pdf->Text(115, 104.7, $data['postcode']);
    $pdf->Text(132, 104.7, $data['city']);
    if ($data['phone']) {
        $pdf->Text(115, 95.3, $data['phone']);
    } else {
        if ($i == 0) $this->pdfRect(114, 111.9, 40, $pdf, 3);
    }
    if (!empty($data['email'])) {
        $pdf->Text(115, 134.1, $data['email']);
    } else {
        if ($i == 0) $this->pdfRect(114, 131.4, 40, $pdf, 3);
    }

    // SEPA
    $pdf->Text(25, 185.5, $data['namec']);
    $pdf->Text(25, 195.5, $data['street']);
    $pdf->Text(25, 205.5, $data['postcode']);
    $pdf->Text(45, 205.5, $data['city']);

    if ($i == 0) $this->pdfRect(24, 212.2, 80, $pdf, 4);
    if ($i == 0) $this->pdfRect(114, 212.2, 80, $pdf, 4);
    if ($i == 0) $this->pdfRect(114, 182.3, 80, $pdf, 4);
    if ($i == 0) $this->pdfRect(114, 192, 8, $pdf, 4);
    if ($i == 0) $this->pdfRect(125, 192, 8, $pdf, 4);
    if ($i == 0) $this->pdfRect(136, 192, 8, $pdf, 4);
    if ($i == 0) $this->pdfRect(147, 192, 8, $pdf, 4);
    if ($i == 0) $this->pdfRect(158, 192, 8, $pdf, 4);
    if ($i == 0) $this->pdfRect(169, 192, 8, $pdf, 4);

    $pdf->SetFont($pdfCfg['fontFamily'], 'B', 11, 0, 2);
    $pdf->Text(115, 215.8, 'X');
    $pdf->SetFont($pdfCfg['fontFamily'], '', 9, 0, 2);

    // Person Insure
    $pdf->Text(70, 251.4, $data['personInsured']['forename'] .' '. $data['personInsured']['surname']);
    if ($isGerman) {
        $pdf->Text(68.8, 257, 'X');
    } elseif ($data['nation']) {
        $pdf->Text(94, 256.6, $data['nation']);
    }

    if ($data['personInsured']['familyStatusLid']) {
        switch ($data['personInsured']['familyStatusLid']) {
            case FAMILY_STATUS_SINGLE:
                $text = 1;
                break;
            case FAMILY_STATUS_MARRIED:
                $text = 2;
                break;
            case FAMILY_STATUS_DIVORCED:
                $text = 3;
                break;
            case FAMILY_STATUS_WIDOWED:
                $text = 4;
                break;
        }   
    } else {
        
    }


   // $pdf->Text(83, 270.1, $text);




    if ($i == 0) $this->pdfRect(68.7, 259.8, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(78.7, 259.8, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(89.2, 259.8, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(68.7, 262.8, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(78.7, 262.8, 2, $pdf, 2);

    // Hide PageNum
    $this->pdfClean(180, 286, 40, $pdf, 255, 255, 255, 5, 'F');


/**
 * Page 2
 */
    $tplidx = $pdf->importPage(2, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

    $pdf->Text(83, 17, $text);
    $personInsuredIsMale ? $pdf->Text(88.9, 31.6, 'X') : $pdf->Text(101.5, 31.6, 'X');
    $pdf->Text(69.5, 31.2, $data['birthdate2']);

    if ($i == 0 && $text == 0) $this->pdfRect(82.6, 15.3, 3, $pdf, 3);

    $pdf->Text(23.8, 69.8, 'X');

    $pdf->Text(58.6, 92.5, stringHelper::makeGermanDate($data['wishedBegin']));    

    if ($data['personInsured']['job']) {
        $pdf->Text(70, 35.9, $data['personInsured']['job']);
    } else {
        if ($i == 0) $this->pdfRect(69, 33.9, 40, $pdf, 3);
    }

    if ($i == 0) $this->pdfRect(68.7, 44.2, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(88.6, 44.2, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(68.7, 48, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(88.6, 48, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(68.7, 50.8, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(88.6, 50.8, 2, $pdf, 2);#

    # Zahlweise
    if ($i == 0) $this->pdfRect(47.2, 142.3, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(62.9, 142.3, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(79.9, 142.3, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(96.9, 142.3, 2, $pdf, 2);

    if($pdfTemplate == project::gti('inter-z90-zpro') || $pdfTemplate == project::gti('inter-z80-zpro') || $pdfTemplate == project::gti('inter-z70-zpro'))
    {
        $pdf->Text(25.9, 132.8, 'X');
        $pdf->Text(65, 132.5, 'X');
    }
    switch($pdfTemplate)
    {
        case project::gti('inter-z90'):
        case project::gti('inter-z90-zpro'):
            $pdf->Text(23.6, 127.9, 'X');
            $pdf->Text(65, 127.9, 'X');
            break;
        case project::gti('inter-z80'):
        case project::gti('inter-z80-zpro'):
            $pdf->Text(23.6, 122.9, 'X');
            $pdf->Text(65, 122.7, 'X');
            break;
        case project::gti('inter-z70'):
        case project::gti('inter-z70-zpro'):
            $pdf->Text(23.6, 117.9, 'X');
            $pdf->Text(65, 117.3, 'X');
            break;
    }

    $pdf->Text(59, 138.8, $data['price'] .' EUR');
    $pdf->Text(183, 143.8, $data['price']);

    if ($i == 0) $this->pdfRect(167, 199, 8, $pdf, 3);
    if ($i == 0) $this->pdfRect(167, 214.3, 8, $pdf, 3);
    if ($i == 0) $this->pdfRect(167, 218.8, 8, $pdf, 3);
    if ($i == 0) $this->pdfRect(167, 195, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(173, 195, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(167, 229, 2, $pdf, 2);
    if ($i == 0) $this->pdfRect(173, 229, 2, $pdf, 2);

    if ($data['contractComplete']['tooth1'] > 0) {
        $pdf->Text(166.5, 197.1, 'X');
        $pdf->Text(168, 201.5, $data['contractComplete']['tooth1']);
    }

    $pdf->Text(59, 264.4, '1');
    if ($data['personInsured']['insurance']) {
        $pdf->Text(110, 264.1, $data['personInsured']['insurance']);
    } else {
        if ($i == 0) $this->pdfRect(109, 261.3, 90, $pdf, 3);
    }


    // Hide PageNum
    $this->pdfClean(180, 287, 40, $pdf, 255, 255, 255, 5, 'F');

/**
 * Page 3
 */
    $tplidx = $pdf->importPage(3, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

    // Hide PageNum
    $this->pdfClean(180, 287, 40, $pdf, 255, 255, 255, 5, 'F');

/**
 * Page 4
 */
    $tplidx = $pdf->importPage(4, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

    if ($i == 0) $this->pdfRect(84, 160.8, 55, $pdf, 5);
    if ($i == 0) $this->pdfRect(84, 178.8, 55, $pdf, 5);
    if ($i == 0) $this->pdfRect(144, 178.8, 55, $pdf, 5);

    $pdf->SetFont($pdfCfg['fontFamily'], 'B', 12, 0, 2);

    if (AGE_PERSONINSURED < 18) {
        if ($i == 0) $this->pdfRect(144, 160.8, 55, $pdf, 5);
        $pdf->Text(145, 164.8, 'X');
    }

    $pdf->Text(85, 164.8, 'X');
    $pdf->Text(85, 182.8, 'X');
    $pdf->Text(145, 182.8, 'X');

    if ($i == 0) $this->pdfRect(24, 252.3, 55, $pdf, 5);
    if ($i == 0) $this->pdfRect(84, 252.3, 55, $pdf, 5);

    $pdf->Text(25, 256.3, 'X');
    $pdf->Text(85, 256.3, 'X');

    // PBD / OBD
    $pdf->SetFont($pdfCfg['fontFamily'], '', 10, 0, 2);
    $pdf->Text(25, 164.9, '675363');
    $pdf->Text(25, 182.9, 'D-YIRL-NDZ7C-33');


    // Hide PageNum
    $this->pdfClean(180, 287, 40, $pdf, 255, 255, 255, 5, 'F');

/**
 * Page 5
 */
    $tplidx = $pdf->importPage(5, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

/**
 * Page 6
 */
    $tplidx = $pdf->importPage(6, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);






