<?php
// File for generating Contract PDF 

        /*
         * Constants
         */
        // SQUARE XY
        define('X_Y', 2.0);

        // RECT Y
        define('Y', 3.5);

        $data['person']['age'] = actionHelper::getAge($data['person']['birthdate']);
        $data['personInsured']['age'] = actionHelper::getAge($data['personInsured']['birthdate']);



        if ($i!=1) {
            $pagecount = $pdf->setSourceFile($path.'files/gothaer/Antrag/2019/antrag_062019.pdf');
        } else {
            $pagecount = $pdf->setSourceFile($path.'files/gothaer/Antrag/2019/antrag_062019.pdf');
        }

        $tplidx = $pdf->importPage(1, '/MediaBox');

        $pdf->addPage('P');  // idx, x, y, w, h
        $pdf->useTemplate($tplidx, 0, 6, 210);
        $pdf->SetMargins(0, 0, 0);


        

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);

        if($i==0) $this->pdfRect(90.0, 3.0, 70, $pdf);
            $pdf->Text(90.0, 7.0, $titles[$i]);

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-1, 0, 2);


        #$pdf->Text(75.0, 28.0, "Versicherungsmakler Experten GmbH");
        #$this->getCompanyStamp(75.0, 24, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']-1, 'horizontal', 'wide');
        


     $this->pdfClean(88.0, 26, 30.0, $pdf, 255, 255, 255, 8);
     $pdf->Text(100.0, 34, '3 6 4 8 4');

        if($data['gender']=='male') {
            $pdf->Text(181.0, 42.1, 'X');
        } else {
            $pdf->Text(181.0, 45.9, 'X');
        }

        $pdf->Text(29.0, 41.2, $data['name']);

        $pdf->Text(29.0, 48.5, $data['street']);
     $pdf->Text(29.0, 56.2, $data['postcode'].'                        '.$data['city']);
            
        $pdf->Text(29.0, 68.2, $data['birthdate']);

        if(empty($data['nation'])) {
            $pdf->Text(73, 68.2, 'deutsch');
        } else {
            $pdf->Text(73, 68.2, $data['nation']);
        }

     if(empty($data['email'])) {
        $this->pdfRect(120.0, 52, 40, $pdf);
     } else {
        $pdf->Text(122.0, 56.2, $data['email']);
     }

     // if(empty($data['job'])) {
     //    $this->pdfRect(95.0, 64, 60, $pdf);
     // } else {
     //    $pdf->Text(97.0, 68.2, $data['job']);
     // }

        // VP
     if($data['name'] == $data['name2b'])
     {
        // VP = VN
        $pdf->Text(181.0, 57.2, 'X');
     } else {

         $pdf->Text(29.0, 78.5, $data['name2b']);
         $pdf->Text(29.0, 86.8, $data['birthdate2']);
    
         if($data['gender2']=='male')    {
        $pdf->Text(174.6, 78, 'X');
         } else {
            $pdf->Text(174.6, 81.6, 'X');
         }

     }



        if($i==0) {
            $this->pdfRect(25.6, 116.9, X_Y, $pdf, X_Y);
            $this->pdfRect(63.7, 116.9, X_Y, $pdf, X_Y);
            $this->pdfRect(85.6, 116.9, X_Y, $pdf, X_Y);
            $this->pdfRect(107.6, 116.9, X_Y, $pdf, X_Y);
        }

     $pdf->Text(107.5, 123.5, 'X');


        if($i==0) {
            $this->pdfRect(35.2, 203, X_Y, $pdf, X_Y);
            $this->pdfRect(45.2, 203, X_Y, $pdf, X_Y);
        }

     #if(empty($data['insurance'])) {
        $this->pdfRect(54.0, 200.5, 26, $pdf);
	 $pdf->Text(30.0, 255, 'Fuer die zu versichernde Person besteht eine gesetzliche Krankenversicherung bei _____________________');
        $this->pdfRect(143.0, 251.0, 26, $pdf);
     #} else {
     #   $pdf->Text(56.0, 204.1, $data['insurance']);
	#$pdf->Text(30.0, 255, 'Fuer die zu versichernde Person besteht eine gesetzliche Krankenversicherung bei '.$data['insurance']);
     #}



        if ( ! empty($data['t1'])) {
            $pdf->Text(145.0, 163.6, $data['t1']);
        } else {
            $this->pdfRect(142, 161.7, 10, $pdf, 3);
        }

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+5, 0, 2);
        $this->pdfRect(138.2, 161.7, X_Y, $pdf, 4);
        $this->pdfRect(152.8, 161.7, X_Y, $pdf, 4);
        $pdf->Text(139, 164.7, '!');
        $pdf->Text(153.6, 164.7, '!');
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);


        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+3, 0, 2);

        $pdf->Text(41, 134.5, substr($data['begin'], 0, 1));
        $pdf->Text(47, 134.5, substr($data['begin'], 1, 1));
        $pdf->Text(65, 134.5, substr($data['begin'], 5, 1));
        $pdf->Text(71, 134.5, substr($data['begin'], 6, 1));


        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
         $pdf->Text(180.0, 163.6, $data['price']);
         $pdf->Text(180.0, 177.6, $data['price']);
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
    

        // Unterschriften


        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);
        #$this->pdfRect(27.5, 270.2, 40, $pdf);
        #$this->pdfRect(72.5, 270.2, 50, $pdf);
        
        #$pdf->Text(28.8, 274.8, 'X');
        #$pdf->Text(73.8, 274.8, 'X');

        if($data['personInsured']['age'] > 15 && $data['name'] != $data['name2b']) {
        #    $this->pdfRect(137.5, 270.2, 50, $pdf);
        #    $pdf->Text(138.8, 274.8, 'X');
        }
        
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);


// SEITE 2 
     $tplidx = $pdf->importPage(2, '/MediaBox');


        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        // Unterschriften
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
        $this->pdfRect(25.5, 79.2, 33, $pdf);
        $this->pdfRect(74.5, 79.2, 33, $pdf);
        $this->pdfRect(139.5, 79.2, 44, $pdf);

     $pdf->Text(26.0, 99, 'Versicherungsmakler Experten GmbH, 08142 - 651 39 28');
     $pdf->Text(178.0, 99, '3 6 4 8 4');

     $this->getCompanyStamp(100.0, 262.4, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize'], 'horizontal', 'wide');


// SEITE 3
        $tplidx = $pdf->importPage(3, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

     $pdf->Text(25.3, 103.5, 'X');

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2); 

       
        // Markieren der Bankzeilen
        #$this->pdfRect(25.5, 117.0, 40, $pdf);
     $pdf->Text(29.0, 122.9, $data['nameb']);
        // Name
        #$this->pdfRect(25.5, 127.0, 100, $pdf);
     $pdf->Text(29.0, 132.9, $data['street']);
        // Land / PLZ / Ort
        #$this->pdfRect(25.5, 138.0, 12, $pdf); $this->pdfRect(46.5, 138.0, 20, $pdf); $this->pdfRect(76.5, 138.0, 20, $pdf); 
     $pdf->Text(29.0, 143.6, $data['nation']);
     $pdf->Text(52.0, 143.6, $data['postcode']);
     $pdf->Text(86.0, 143.6, $data['city']);

        // IBAN
        $this->pdfRect(25.5, 150.4, 100, $pdf);
        // BIC / Name der Bank
        $this->pdfRect(25.5, 161.4, 52, $pdf); $this->pdfRect(90.5, 159.0, 80, $pdf);

        // Unterschriften
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);
        $this->pdfRect(25.5, 178.4, 33, $pdf); 
        $this->pdfRect(64.5, 178.4, 33, $pdf);
        $this->pdfRect(107.5, 178.4, 44, $pdf);

        #$pdf->Text(108.8, 153.9, 'X');
        
        if($data['name'] != $data['name2b']) {
        #    $this->pdfRect(25.5, 168.2, 50, $pdf);
        #    $pdf->Text(26.8, 172.8, 'X');
        }

// SEITE 4
        $tplidx = $pdf->importPage(4, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

// SEITE 5
        $tplidx = $pdf->importPage(5, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);


// SEITE 6
        $tplidx = $pdf->importPage(6, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);


// SEITE 7
        // $tplidx = $pdf->importPage(7, '/MediaBox');

        // $pdf->addPage('P');
        // $pdf->useTemplate($tplidx, 0, 0.1, 209.9);


// SEITE 8
        // $tplidx = $pdf->importPage(8, '/MediaBox');

        // $pdf->addPage('P');
        // $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

?>
