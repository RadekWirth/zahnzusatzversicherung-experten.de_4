<?

// File for generating Contract PDF for VKB-zp  (VKB + UKV sind vom Tarif gleich)

    if($i==0) {
        $pagecount = $pdf->setSourceFile($path.'/files/ukv/antrag/Antrag_2019.pdf');
    } else {
        $pagecount = $pdf->setSourceFile($path. '/files/ukv/antrag/Antrag_2019.pdf');
    }

// Seite 1

        $pdf->addPage('P');
        $tplidx = $pdf->importPage(1, '/MediaBox');
        $pdf->useTemplate($tplidx, 6, 6.8, 200);


        $this->getCompanyStamp(88.0, 15, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']);

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+2, 0, 2);

        if($i==0) $this->pdfRect(86.0, 0, 62, $pdf);
        $pdf->Text(87.0, 4.0, $titles[$i]);

        // Partnernummer
        $pdf->SetFont($pdfCfg['fontFamily'], '', 12, 0, 2);
        $pdf->Text(137.3, 92.6, '1  8  8  9  6  3  3');

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

        // Antragsteller
        $pdf->Text(20.0, 77.8, $data['person']['surname']);
        $pdf->Text(20.0, 85.3, $data['person']['forename']);

        if($data['gender']=="female")
            $pdf->Text(98.6, 69.2, "x");
        else
            $pdf->Text(83, 69.2, "x");

        $pdf->Text(114.3, 85.3, $data['birthdate']);
        $pdf->Text(20.0, 93, $data['street']);
        $pdf->Text(30.0, 100.8, $data['postcode']);
        $pdf->Text(52.0, 100.8, $data['city']);
        if ( ! empty($data['phone'])) 
        {
            $pdf->Text(20, 108.3, $data['phone']);
        }
        else
        {
            if ($i == 0) $this->pdfRect(20, 105.3, 50, $pdf, 4);
        }
        $pdf->Text(20, 115.8, $data['email']);

        // Zahlungsweise Ergaenzung jaehrlich "kein Skonto"
        #$pdf->SetFont($pdfCfg['fontFamily'], '', 6, 0, 2);
        #$pdf->Text(150.4, 131.5, '(kein Skonto!)');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

        // 2. zu versichernde Person
        $pdf->Text(66.5, 178.1, $data['personInsured']['forename']);
        $pdf->Text(66.5, 183.6, $data['personInsured']['surname']);
        $pdf->Text(66.5, 205.1, $data['birthdate2']);
        $pdf->Text(66.5, 210.6, $data['personInsured']['nationCode']);

        if ( ! empty($data['personInsured']['job']))
        {
            $pdf->Text(66.5, 215.6, $data['personInsured']['job']);
        }
        else
        {
            if ($i == 0) $this->pdfRect(65, 213.1, 36, $pdf, 3.5);
        }


        if($data['personInsured']['salutationLid']=="9")
        {
            $pdf->Text(107.2, 173, "x");
        }
        else
        {
            $pdf->Text(91.2, 173, "x");
        }

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-1, 0, 2);

        // 3. Tarife / Beitrag
        // Versicherungsbeginn/Datum
            $signUp = explode("-", $data['wishedBegin']);
                // reset font
                $pdf->SetFont($pdfCfg['fontFamily'], 'B', 10, 0, 2);
                $pdf->Text(71.5, 229.6, $signUp[1]);
                $pdf->Text(83.54, 229.6, $signUp[0]);

        // Tarifabfrage was f r ein Kreuz gesetzt werden soll bei ZahnPrivat-Kompakt oder  ZahnPrivat-Optimal oder ZahnPrivat-Premium
                switch($pdfTemplate) {
                    case project::gti('ukv-zp-premium'):
                        $pdf->Text(19.2, 244.7, "x");
                        $riskSurchargeFactor = 8.6;
                        break;
                    case project::gti('ukv-zp-optimal'):
                        $pdf->Text(19.2, 239.9, "x");
                        $riskSurchargeFactor = 6.7;
                        break;
                    case project::gti('ukv-zp-kompakt'):
                        $pdf->Text(19.2, 235.2, "x");
                        $riskSurchargeFactor = 0;
                        break;
                }

                $pdf->Text(79.5, 240.6, $data['bonusbase']);

                if ($data['bonus_tooth'] == '0,00')
                {
                    $pdf->Text(79.5, 269.0, $data['bonusbase']);
                }
                else 
                {
                    if ($data['t1'] > 0)
                    {
                        #$riskSurcharge = stringHelper::makeGermanFloat($data['t1'] * $riskSurchargeFactor);
                        $pdf->Text(79.5, 249.0, $data['bonus']);
                    }
                    $pdf->Text(79.5, 269.0, stringHelper::makeGermanFloat(stringHelper::toFloat($data['price'])));
                }

#print_r($data); print $riskSurcharge;

// Seite 2
        $pdf->addPage('P');
        $tplidx = $pdf->importPage(2, '/MediaBox');
        $pdf->useTemplate($tplidx, 6, 6.8, 201);

    // Frage zu 4.1.1 -> Fehlen Zaehne?
        $this->pdfRect(137.3, 45.5, 2, $pdf, 2); //Nein
        $this->pdfRect(151.5, 45.5, 2, $pdf, 2); //Ja
        if( ! empty($data['t1']) && $data['t1'] > 0)
        {
            $pdf->Text(151.5, 47.0, "x"); //Ja
            $pdf->Text(156.5, 53, $data['t1']);
        }

    // Frage zu 4.1.2 -> Behandlungen?
        $this->pdfRect(137.3, 56, 2, $pdf, 2); //Nein
        $this->pdfRect(151.56, 56, 2, $pdf, 2); //Ja
        if ( ! empty($data['incare']))
        {
            $pdf->Text(151.5, 58.1, "x");
        }
        #else 
        #{
        #    $pdf->Text(137.3, 58.1, "x");
        #}

    // Frage zu 4.1.3 -> Parodontose
        $this->pdfRect(137.3, 70.7, 2, $pdf, 2); //Nein
        $this->pdfRect(151.56, 70.7, 2, $pdf, 2); //Ja
        if ( ! empty($data['periodontitis']))
        {
            $pdf->Text(151.5, 72.6, 'x');
        }

    // Frage zu  4.1.4 Person 1
        if ($pdfTemplate == project::gti('vkb-zp-premium') || $pdfTemplate == project::gti('ukv-zp-premium'))
        {
            if (($data['personInsuredData'] && $age2 < 20) || $age < 20)
            {
                $this->pdfRect(137.3, 83.3, 2, $pdf, 2); //Nein
               $this->pdfRect(151.56, 83.3, 2, $pdf, 2); //Ja             
            }
        }



    // Frage zu  5 Person 1

        $this->pdfRect(66, 168.5, 2, $pdf, 2); //AOK Bayern
        $this->pdfRect(66, 173, 2, $pdf, 2); //AOK Bayern
        $this->pdfRect(66, 176, 2, $pdf, 2); //AOK Bayern
        $this->pdfRect(66, 179, 2, $pdf, 2); //AOK Bayern
        $this->pdfRect(66, 182, 2, $pdf, 2); //AOK Bayern
        $this->pdfRect(66, 195.5, 2, $pdf, 2); //AOK Bayern
        $this->pdfRect(76.6, 195.5, 2, $pdf, 2); //AOK Bayern

    // Frage zu  5 - Zusatzversicherung - Wo? Person 1
        $this->pdfRect(87, 194.1, 25.3, $pdf, 3.7); //Wo - oben
        $this->pdfRect(87, 199.3, 25.3, $pdf, 3.7); //Wo - unten
        $this->pdfRect(115, 194.1, 12.9, $pdf, 3.7); //Höhe der Leistung



    // Frage zu  6.1 - Wurde der Antrag... ja
        $pdf->Text(24.2, 222.3, 'x');

    // Frage zu  6.2 - Wurde der Antrag... ja
        $pdf->Text(24.2, 239.6, 'x');



// Seite 3
        $pdf->addPage('P');
        $tplidx = $pdf->importPage(3, '/MediaBox');
        $pdf->useTemplate($tplidx, 6, 6.8, 199);

        //M glichkeit I
        $this->pdfRect(20.27, 176.4, 2.7, $pdf, 2.7); 
        //M glichkeit II
        $this->pdfRect(20.27, 228.4, 2.7, $pdf, 2.7); 

        $pdf->Text(13.5, 194.4, "O");
        $pdf->Text(13.5, 199.4, "D");
        $pdf->Text(13.5, 204.4, "E");
        $pdf->Text(13.5, 209.4, "R");




// Seite 4
        $pdf->addPage('P');
        $tplidx = $pdf->importPage(4, '/MediaBox');
        $pdf->useTemplate($tplidx, 6, 6.8, 200);
        // Datum
        $this->pdfRect(19, 133.7, 35.48, $pdf, 8);
        // Unterschrift Antragsteller
        $this->pdfRect(57, 133.7, 67, $pdf, 8);
        // Unterschrift Antragsteller ab 16
        $this->pdfRect(126, 133.7, 66.8, $pdf, 8);

        // Kontoinhaber
        $this->pdfRect(47.5, 173.3, 99.51, $pdf, 4.2);
        // Stra e & Hausnummer
        $this->pdfRect(47.5, 179.2, 99.51, $pdf, 4.2);
        // PLZ
        $this->pdfRect(30, 184.64, 14.19, $pdf, 4.2);
        // Ort
        $this->pdfRect(50.8, 184.64, 96.15, $pdf, 4.2);
        // IBAN
        $this->pdfRect(26.3, 190.5, 76, $pdf, 4.2);
        // BIC
        $this->pdfRect(109.26, 190.5, 38, $pdf, 4.2);
        // Kreditinstitut
        $this->pdfRect(31, 195.17, 116.2, $pdf, 4.2);
        // Ort / Datum
        $this->pdfRect(29.5, 207.4, 52.8, $pdf, 6.9);
        // Unterschrift
        $this->pdfRect(122, 207.4, 70.5, $pdf, 6.9);

        // Unterschrift Kreuzchen bei Datum / Unterschrift Angestellter / Unterschrift zu versichernde Person
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);
        $pdf->Text(19, 140.0, 'X');
        $pdf->Text(56.8, 140.0, 'X');
        $pdf->Text(126.2, 140.0, 'X');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        // Unterschrift Kreuzchen bei Ort/Datum / Unterschrift Kontoinhaber
         $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);
        $pdf->Text(29.6, 214.5, 'X');
        $pdf->Text(122, 214.5, 'X');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);


// Seite 5
        $pdf->addPage('P');
        $tplidx = $pdf->importPage(5, '/MediaBox');
        $pdf->useTemplate($tplidx, 6, 6.8, 200);




// Seite 6
        $pdf->addPage('P');
        $tplidx = $pdf->importPage(6, '/MediaBox');
        $pdf->useTemplate($tplidx, 6, 6.8, 200);


// Seite 7
        $pdf->addPage('P');
        $tplidx = $pdf->importPage(7, '/MediaBox');
        $pdf->useTemplate($tplidx, 6, 6.8, 200);

        


// Seite 8
        if($complete==2)
        $pdf->addPage('P');
