<?
// File for generating Contract PDF for Wuerttembergische

$personInsuredOther = ($data['person']['pid'] !== $data['personInsured']['pid']) ? true : false;

// SEITE 1
        if ($i==0) {
            $pagecount = $pdf->setSourceFile($path.'/files/wuerttembergische/2020/antrag_02-2020.pdf');
        } else {
            $pagecount = $pdf->setSourceFile($path.'/files/wuerttembergische/2020/antrag_02-2020.pdf');
        }

        $tplidx = $pdf->importPage(2, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 3.4, 209.9);
         $pdf->SetMargins(0, 0, 0);

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);

        //gelbe markierungen nur auf rückantwort drucken!
        if($i==0) $this->pdfRect(70, 2, 68, $pdf);
        $pdf->Text(70.0, 6.0, $titles[$i]);

        $this->getCompanyStamp(139.0, 2.5, $pdf, $data['contractComplete']['sourcePage'], 6.5, 'horizontal', 'wide');

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        //Gesch.-St.Nr 2000-3539-9
        if($i==0) $pdf->Text(109, 44.8, '2');
        if($i==0) $pdf->Text(112.2, 44.8, '0');
        if($i==0) $pdf->Text(115.4, 44.8, '0');
        if($i==0) $pdf->Text(118.6, 44.8, '0');
        if($i==0) $pdf->Text(123.6, 44.8, '3');
        if($i==0) $pdf->Text(126.4, 44.8, '5');
        if($i==0) $pdf->Text(129.8, 44.8, '3');
        if($i==0) $pdf->Text(133.3, 44.8, '9');
        if($i==0) $pdf->Text(138, 44.8, '9');

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);

    // 1. Antragsteller

        ($data['gender']=='male') ? $pdf->Text(52.9, 50.9, 'X') : $pdf->Text(52.9, 54.9, 'X');
            $pdf->Text(72, 54.3, $data['namec']);
        $pdf->Text(72, 61.3, $data['street']);
        ($data['nation']=='DEU') ? $pdf->Text(96, 75.1, 'deutsch') : $pdf->Text(96, 75.1, $data['nation']);
        
        //Phone
        if($i==0) $this->pdfRect(124, 72.3, 32, $pdf, 3.5);

        $pdf->Text(72, 75.1, $data['birthdate']);
        if (!empty($data['insurance'])) {
            $pdf->Text(159, 75.1, $data['insurance']);
        } else {
            if($i==0) $this->pdfRect(157, 71.8, 30, $pdf, 4.0);
        }
        $pdf->Text(72, 68.3, $data['postcode'].'        '.$data['city']);

        // Zu versichernde Person
        if($personInsuredOther) {
            ($data['gender2']=='male') ? $pdf->Text(52.9, 87.7, 'X') : $pdf->Text(52.9, 91.7, 'X');
                $pdf->Text(72, 90.5, $data['name2b']);
            $pdf->Text(159, 90.5, $data['birthdate2']);
            if (!empty($data['insurance2'])) {
                $pdf->Text(159, 97.5, $data['insurance2']);
            } else {
                if($i==0) $this->pdfRect(157, 94.9, 30, $pdf, 4.0);

            }
        } else {
           $pdf->Text(25.2, 60, 'X');
        }

        // Beratungsdoku
        $pdf->Text(69.8, 103.7, 'X');
        if ($i == 0) $this->pdfRect(70, 110.8, 60, $pdf, 4.7);
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);
        $pdf->Text(70, 115.1, 'X');
        $pdf->SetFont($pdfCfg['fontFamily'], '', 10, 0, 2);

        // Stand der erhaltenen Unterlagen
        $pdf->Text(166.8, 176.9, 'Februar 2018');

        // Tarif?
        //$personInsuredOther = true;
        $pdfTemplate = isset($pdfTemplate) ? $pdfTemplate : $data['idt'];
        switch($pdfTemplate) {
            case project::gti('wuerttembergische-v1'):
                $pdf->Text(72.3, 151.7, "X");


                ($personInsuredOther) ? $pdf->Text(73.4, 208.5, "X") : $pdf->Text(69.3, 208.5, "X");
                break;
            case project::gti('wuerttembergische-v2'):
                $pdf->Text(72.3, 154.7, "X");
                ($personInsuredOther) ? $pdf->Text(73.4, 230, "X") : $pdf->Text(69.3, 230, "X");
                break;
            case project::gti('wuerttembergische-v3'):
                $pdf->Text(72.3, 158.1, "X");
                ($personInsuredOther) ? $pdf->Text(73.4, 252, "X") : $pdf->Text(69.3, 252, "X");
                break;
            case project::gti('wuerttembergische-z1'):
                $pdf->Text(122.4, 151.7, "X");
                ($personInsuredOther) ? $pdf->Text(125.1, 208.5, "X") : $pdf->Text(121.3, 208.5, "X");
                break;
            case project::gti('wuerttembergische-z2'):
                $pdf->Text(122.4, 154.7, "X");
                ($personInsuredOther) ? $pdf->Text(125.1, 229.8, "X") : $pdf->Text(121.3, 229.8, "X");
                break;
            case project::gti('wuerttembergische-z3'):
                $pdf->Text(122.4, 158.1, "X");
                ($personInsuredOther) ? $pdf->Text(125.1, 251.5, "X") : $pdf->Text(121.3, 251.5, "X");
                break;
            case project::gti('wuerttemberische-ze50-zbe'):
                $pdf->Text(96.4, 158, "X");
                ($personInsuredOther) ? $pdf->Text(99.3, 251.5, "X") : $pdf->Text(95.5, 251.5, "X");
                break;
            case project::gti('wuerttemberische-ze70-zbe'):
                $pdf->Text(96.4, 154, "X");
                ($personInsuredOther) ? $pdf->Text(99.3, 230.5, "X") : $pdf->Text(95.5, 230.5, "X");
                break;
            case project::gti('wuerttemberische-ze90-zbe'):
                $pdf->Text(96.4, 150.4, "X");
                ($personInsuredOther) ? $pdf->Text(99.3, 208.5, "X") : $pdf->Text(95.5, 208.5, "X");
                break;
            }

        // Ort / Datum
        if($i==0) $this->pdfRect(69, 174, 30, $pdf, 4.0);
        if($i==0) $this->pdfRect(104, 174, 50, $pdf, 4.0);

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);
        if($i==0) $pdf->Text(106, 178.3, 'X');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        //Versicherungsbeginn
        $my = explode(".", $data['begin']);
        // monat
        $pdf->Text(175, 185.3, substr($my[0],0,1));
        $pdf->Text(180.2, 185.3, substr($my[0],1,1));
        // Jahr
        $pdf->Text(184, 185.3, substr($my[1], 0,1));
        $pdf->Text(188, 185.3, substr($my[1], 1,1));
        $pdf->Text(192, 185.3, substr($my[1], 2,1));
        $pdf->Text(195.6, 185.3, substr($my[1], 3,1));
        unset($my);

        switch($pdfTemplate) {
            case project::gti('wuerttembergische-v1'):
            case project::gti('wuerttembergische-v2'):
            case project::gti('wuerttembergische-v3'):
                ($personInsuredOther) ? $pdf->Text(71.5, 280.5, $data['price']) : $pdf->Text(71.5, 274.4, $data['price']);
            break;
            case project::gti('wuerttembergische-z1'):
            case project::gti('wuerttembergische-z2'):
            case project::gti('wuerttembergische-z3'):
                (!$personInsuredOther) ? $pdf->Text(122.5, 280.5, $data['price']) : $pdf->Text(122.5, 274.4, $data['price']);
            break;
            case project::gti('wuerttemberische-ze50-zbe'):
            case project::gti('wuerttemberische-ze70-zbe'):
            case project::gti('wuerttemberische-ze90-zbe'):
                (!$personInsuredOther) ? $pdf->Text(98.5, 280.5, $data['price']) : $pdf->Text(98.5, 274.4, $data['price']);
            break;
        }

        //Zahlweise
        if($i==0) $this->pdfRect(69, 284.1, 3.0, $pdf, 3.0);
        if($i==0) $this->pdfRect(116, 284.1, 3.0, $pdf, 3.0);
        if($i==0) $this->pdfRect(138.1, 284.1, 3.0, $pdf, 3.0);
        if($i==0) $this->pdfRect(158, 284.1, 3.0, $pdf, 3.0);

        // Seitenzahl verdecken 
        $this->pdfClean(90, 290, 28, $pdf, 255, 255, 255, 6, 'F');

// SEITE 2
        $tplidx = $pdf->importPage(3, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        if($i==0) $this->pdfRect(44.6, 209.4, 2.5, $pdf, 2.5);
        if($i==0) $this->pdfRect(68.3, 209.4, 2.5, $pdf, 2.5);        

        if($i==0) $this->pdfRect(132.2, 26.3, 2.5, $pdf, 2.5);
        if($i==0) $this->pdfRect(155.1, 26.3, 2.5, $pdf, 2.5);

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 10, 0, 2);
        $pdf->Text(11, 222, "O");
        $pdf->Text(11, 227, "D");
        $pdf->Text(11, 232, "E");
        $pdf->Text(11, 237, "R");
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        // Seitenzahl verdecken 
        $this->pdfClean(90, 288, 28, $pdf, 255, 255, 255, 8, 'F');

// SEITE 3
        $tplidx = $pdf->importPage(4, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        //Gesundheitsangaben

        // Wurde eine anderweitig beantragte private Zahnzusatzversicherung abgelehnt?
        if ($i == 0) $this->pdfRect(29, 210.2, 79, $pdf, 3.7);
        if ($i == 0) $this->pdfRect(115, 210.2, 80, $pdf, 3.7);

        if ( ! $personInsuredOther) {

            // Markierungen
            if ($i == 0) $this->pdfRect(130.5, 199.3, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(140.6, 199.3, 2, $pdf, 2);

            if ($i == 0) $this->pdfRect(130.5, 222.0, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(140.6, 222.0, 2, $pdf, 2);


            if ($i == 0) $this->pdfRect(115.0, 238.1, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(126.0, 238.1, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(133.6, 237, 12.5, $pdf, 3.5);

            if ($i == 0) $this->pdfRect(115.0, 251.0, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(126.0, 251.0, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(134.0, 250.0, 12.5, $pdf, 3.5);

            if ($i == 0) $this->pdfRect(130.2, 272.1, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(140.6, 272.1, 2, $pdf, 2);

            $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);

            // if ($data['contractComplete']['biteSplint'] === 'yes')
            // {
            //     $pdf->Text(141.6, 151.2, 'x');
            // }

            // if ($data['contractComplete']['tooth1'] > 0)
            // {
            //     $pdf->Text(139.6, 177.4, $data['contractComplete']['tooth1']);
            //     $pdf->Text(122.4, 177, 'x');
            // }

 

            $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
        } 
        else 
        {

            // Markierungen
            if ($i == 0) $this->pdfRect(178.8, 199.3, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(188.8, 199.3, 2, $pdf, 2);

            if ($i == 0) $this->pdfRect(178.8, 221.3, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(188.8, 221.3, 2, $pdf, 2);

            if ($i == 0) $this->pdfRect(164.1, 238, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(174.8, 238, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(182.3, 237, 12.5, $pdf, 3.5);


            if ($i == 0) $this->pdfRect(164.1, 250.7, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(174.8, 250.7, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(182.3, 249.7, 12.5, $pdf, 3.5);
 
             if ($i == 0) $this->pdfRect(178.8, 269.6, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(188.8, 269.6, 2, $pdf, 2);

            $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);

            // if ($data['contractComplete']['biteSplint'] === 'yes')
            // {
            //     $pdf->Text(185.6, 151.2, 'x');
            // }

            // if ($data['contractComplete']['tooth1'] > 0)
            // {
            //     $pdf->Text(183.6, 177.4, $data['contractComplete']['tooth1']);
            //     $pdf->Text(166.4, 177, 'x');
            // }

           


            $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        }



        // Seitenzahl verdecken 
        $this->pdfClean(90, 288, 28, $pdf, 255, 255, 255, 8, 'F');

// SEITE 4
        $tplidx = $pdf->importPage(5, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        if ( ! $personInsuredOther) {
                        if ($i == 0) $this->pdfRect(130.6, 13.4, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(140.6, 13.4, 2, $pdf, 2);
                        
                        if ($i == 0) $this->pdfRect(115.6, 32.4, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(131.6, 32.49, 2, $pdf, 2);

            // Summenstaffel 6 Jahre
            if ($data['series'] == 1 || $data['contractComplete']['addNotes'] == 'Verl&auml;ngerung Summenstaffel auf 6 Jahre!') {
                $pdf->Text(115.5, 34.3, 'x');
            }

            // Summenstaffel 8 Jahre
            if ($data['series'] == 2 || $data['contractComplete']['addNotes'] == 'Verl&auml;ngerung Summenstaffel auf 8 Jahre!') {
                $pdf->Text(132.4, 34.3, 'x');
            }
        }else
        {
             if ($i == 0) $this->pdfRect(178.8, 13.4, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(188.8, 13.4, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(163.6, 32.4, 2, $pdf, 2);
            if ($i == 0) $this->pdfRect(180.1, 32.49, 2, $pdf, 2);
            // Summenstaffel 6 Jahre
            if ($data['series'] == 1 || $data['contractComplete']['addNotes'] == 'Verl&auml;ngerung Summenstaffel auf 6 Jahre!') {
                $pdf->Text(163.5, 34.3, 'x');
            }

            // Summenstaffel 8 Jahre
            if ($data['series'] == 2 || $data['contractComplete']['addNotes'] == 'Verl&auml;ngerung Summenstaffel auf 8 Jahre!') {
                $pdf->Text(181, 34.3, 'x');
            }
        }

                // Frage 5 durchstreichen
        $pdf->SetDrawColor(0);
        $pdf->Line(25, 60, 195, 43);

       if($i==0) $pdf->Text(70.2, 116.8, 'x');

        // Kontodaten

        if($i==0) $this->pdfRect(69.4, 163.2, 82, $pdf, 3.0);
        if($i==0) $this->pdfRect(156, 163.2, 37, $pdf, 3.0);
        if($i==0) $this->pdfRect(69.4, 173.0, 127, $pdf, 3.0);

        // Unterschriften
        if($i==0) $this->pdfRect(69.7, 255, 124, $pdf, 3.4);
        if($i==0) $this->pdfRect(69.7, 264.7, 62, $pdf, 6);
        if($i==0) $this->pdfRect(134.4, 264.7, 64, $pdf, 6);


        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);
        if($i==0) $pdf->Text(70.4, 269.2, 'X');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        // Seitenzahl verdecken 
        $this->pdfClean(90, 288, 28, $pdf, 255, 255, 255, 8, 'F');


// SEITE 5
        $tplidx = $pdf->importPage(6, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);


        // Seitenzahl verdecken 
        $this->pdfClean(90, 288, 28, $pdf, 255, 255, 255, 8, 'F');

// SEITE 6
        $tplidx = $pdf->importPage(7, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        // Seitenzahl verdecken 
        $this->pdfClean(90, 288, 28, $pdf, 255, 255, 255, 8, 'F');

// SEITE 7
        $tplidx = $pdf->importPage(8, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        // Seitenzahl verdecken 
        $this->pdfClean(90, 288, 28, $pdf, 255, 255, 255, 8, 'F');

        // $pdf->AddPage('P');

// SEITE 8
        $tplidx = $pdf->importPage(9, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        // Seitenzahl verdecken 
        $this->pdfClean(90, 288, 28, $pdf, 255, 255, 255, 8, 'F');

        // $pdf->AddPage('P');
        // 
// SEITE 9 -- Initiative - Gesund - Versichert ---


        $tplidx = $pdf->importPage(1, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        // Data left
        $pdf->Text(22, 92.0, $data['namec']);
        $pdf->Text(22, 105.6, $data['street']);
        $pdf->Text(22, 118.4, $data['postcode']);
        $pdf->Text(48, 118.4, $data['city']); 

        // Data right
        ($data['gender']=='male') ? $pdf->Text(113.3, 90.8, 'X') : $pdf->Text(131.5, 90.8, 'X');
        $pdf->Text(113, 105.6, $data['birthdate']);
        $pdf->Text(113, 118.4, $data['email']);

        //Fördermittglied
        $pdf->Text(22, 156.7, 'Versicherungsmakler Experten GmbH');

        //Mitgliedsnummer
        $pdf->Text(154, 156.7, 'K 1331');

        //Unterschrift
        if($i==0) $this->pdfRect(21, 254, 38, $pdf, 7.5);

        if($i==0) $this->pdfRect(72, 254, 105.0, $pdf, 7.5);
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);
        if($i==0) $pdf->Text(74, 260, 'X');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        if($complete==2) {
            $pdf->AddPage('P');
        }

        $tplidx = $pdf->importPage(10, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        $tplidx = $pdf->importPage(11, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

?>