<?php

if($i==0) {
    $pagecount = $pdf->setSourceFile($path.'files/nuernberger/Antrag/2019/NuernbergerZEP80.neuerAntrag.092019.pdf');
} else {
    $pagecount = $pdf->setSourceFile($path.'files/nuernberger/Antrag/2019/NuernbergerZEP80.neuerAntrag.092019.pdf');
}

$tplidx = $pdf->importPage(1, '/MediaBox');

$pdf->addPage('P');
$pdf->useTemplate($tplidx, 0, 10, 200);

// Title
if($i==0) $this->pdfRect(74, 2, 68, $pdf);
$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);
$pdf->Text(74.0, 6.0, $titles[$i]);

$this->getCompanyStamp(72.0, 11, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']-2, 'horizontal', 'wide');

$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

$pdf->Text(19.8, 61.9, '115/38/3392');


// Spalte links
$pdf->Text(19.2, 69.9, 'X');


$beginDate = explode('-', $data['wishedBegin']);
$pdf->Text(27.6, 203.5, $beginDate[1]);
$pdf->Text(33.0, 203.5, $beginDate[0]);



if($data['gender'] == 'male') {
    $pdf->Text(145.8, 109.7, 'X');
} else {
    $pdf->Text(145.8, 113.2, 'X');
}

$pdf->Text(22, 112, $data['name']);
$pdf->Text(22, 120, $data['street']);
$pdf->Text(22, 128, $data['postcode']);
$pdf->Text(44, 128, $data['city']);

$pdf->Text(114.0, 112.0, $data['birthdate']);


if(empty($data['nation'])) {
    if($i==0) $this->pdfRect(20, 134.0, 38, $pdf, 3);
} elseif($data['nation']=='DEU') {
    $pdf->Text(22, 136.0, 'deutsch');
} else {
    $pdf->Text(22, 136.0, $data['nation']);
}

if(empty($data['job'])) {
    if($i==0) $this->pdfRect(63, 134.0, 38, $pdf, 3);
} else {
    $pdf->Text(63, 136.0, $data['job']);
}

if(empty($data['phone'])) {
    if($i==0) $this->pdfRect(114, 126.0, 40, $pdf, 3);
} else {
    $pdf->Text(114, 128.0, 'phone'.$data['phone']);
}

if(empty($data['email'])) {
    if($i==0) $this->pdfRect(114, 118.0, 40, $pdf, 3);
} else {
    $pdf->Text(114, 120.0, $data['email']);
}

$pdf->Text(20.2, 152.5, '1');
$pdf->Text(31.0, 152.5, $data['personInsured']['forename']) .', '.$data['personInsured']['surname'] ;
$displayBirthdate = explode('-', $data['personInsured']['birthdate']);
$pdf->Text(91.0, 152.5, $displayBirthdate[2] .'.'. $displayBirthdate[1] .'.'. $displayBirthdate[0]);
if ($data['personInsured']['salutationLid'] == 10) {
    $pdf->Text(85.2, 152.9, 'x');
} else {
    $pdf->Text(82.2, 152.9, 'x');
}


if ($i == 0) 
	$this->pdfRect(19.8, 155.5, 1.2, $pdf, 1.2);
if ($i == 0) 
	$this->pdfRect(41, 155.5, 1.2, $pdf, 1.2);
if ($i == 0) 
	$this->pdfRect(63, 155.5, 1.2, $pdf, 1.2);
if ($i == 0) 
	$this->pdfRect(91, 155.5, 1.2, $pdf, 1.2);

if ($data['personInsured']['job']) {
    $pdf->Text(115, 152.8, $data['personInsured']['job']);
} else {
    if($i==0) $this->pdfRect(113, 150.5, 60, $pdf, 3);
}

$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);

$pdf->Text(20.2, 203.5, '1');

// Welcher Tarif?
$pdfTemplate = isset($pdfTemplate)?$pdfTemplate:$data['idt'];
switch($pdfTemplate) {
	case project::gti('nuern-zep80'):
		$pdf->Text(44.6, 203.5, 'ZEP80');
		
		break;
	case project::gti('nuern-zep80-zv'):
		$pdf->Text(44.6, 203.5, 'ZEP80 ZV');
		
		break;
	#case project::gti('nuern-zr-zv'):
	#	$pdf->Text(76.6, 157.0, 'x');
	#	$pdf->Text(76.6, 152.6, 'x');
	#	break;
	#case project::gti('nuern-zr'):
	#	$pdf->Text(76.6, 152.6, 'x');
	#	break;
}

$pdf->Text(62.0, 203.5, $data['price']);
$pdf->Text(62.0, 235, $data['price']);


$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

$tplidx = $pdf->importPage(2, '/MediaBox');

$pdf->addPage('P');
$pdf->useTemplate($tplidx, 0, 0, 208);


// Beitragszahlweise
if ($i == 0) $this->pdfRect(35.6, 45.2, 1.5, $pdf, 1.5);
if ($i == 0) $this->pdfRect(57.7, 45.2, 1.5, $pdf, 1.5);
if ($i == 0) $this->pdfRect(79.9, 45.2, 1.5, $pdf, 1.5);


// Zahlweg
if ($i == 0) $this->pdfRect(20.1, 54.0, 1.5, $pdf, 1.5);
if ($i == 0) $this->pdfRect(37.0, 54.0, 1.5, $pdf, 1.5);

// Daten des Kontoinhabers
if ($i == 0) $this->pdfRect(20.1, 101.3, 1.5, $pdf, 1.5);
if ($i == 0) $this->pdfRect(31.4, 101.3, 1.5, $pdf, 1.5);
if ($i == 0) $this->pdfRect(42.5, 101.3, 1.5, $pdf, 1.5);

// Unterschriften
if ($i == 0) $this->pdfRect(20.5, 110.0, 82, $pdf, 2.5);
if ($i == 0) $this->pdfRect(20.5, 119.4, 82, $pdf, 2.5);
if ($i == 0) $this->pdfRect(20.5, 129.4, 15, $pdf, 2.5);
if ($i == 0) $this->pdfRect(41.0, 129.4, 55, $pdf, 2.5);
if ($i == 0) $this->pdfRect(106.0, 129.4, 22, $pdf, 2.5);
if ($i == 0) $this->pdfRect(139.0, 129.4, 50, $pdf, 2.5);

// IBAN
if ($i == 0) $this->pdfRect(111, 96.4, 76, $pdf, 2.5);
if ($i == 0) $this->pdfRect(106, 106, 82, $pdf, 2.5);



$tplidx = $pdf->importPage(3, '/MediaBox');

$pdf->addPage('P');
$pdf->useTemplate($tplidx, 0, 0, 208);

$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

// Frage 1-5 durchstreichen
$pdf->SetDrawColor(0);
$pdf->Line(20, 250, 190, 80);


#$this->pdfClean(6, 290, 40, $pdf, 255, 255, 255, 8, 'F');




$tplidx = $pdf->importPage(4, '/MediaBox');

$pdf->addPage('P');
$pdf->useTemplate($tplidx, 0, 0, 208);

// Frage 6 durchstreichen
$pdf->SetDrawColor(0);
$pdf->Line(20, 85, 190, 40);


// Fragen 11-12 durchstreichen
$pdf->SetDrawColor(0);
$pdf->Line(20, 206, 190, 178);


$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);


if ($pdfTemplate == project::gti('nuern-zep80') || $pdfTemplate == project::gti('nuern-zep80-zv')) 
{
// Frage 7
if ($i == 0) $this->pdfRect(109, 100.2, 12, $pdf, 2.5);
if ($i == 0) $this->pdfRect(136, 100.2, 12, $pdf, 2.5);
if ($i == 0) $this->pdfRect(163, 100.2, 12, $pdf, 2.5);

// Frage 8
if ($i == 0) $this->pdfRect(108.2, 121.0, 1.5, $pdf, 1.5);
if ($i == 0) $this->pdfRect(108.2, 124.1, 1.5, $pdf, 1.5);
	
}

if ($pdfTemplate == project::gti('nuern-zep80-zv')) 
{
// Frage 9
if ($i == 0) $this->pdfRect(108.2, 138.3, 1.5, $pdf, 1.5);
if ($i == 0) $this->pdfRect(108.2, 141.2, 1.5, $pdf, 1.5);

// Frage 10
if ($i == 0) $this->pdfRect(108.2, 155.5, 1.5, $pdf, 1.5);
if ($i == 0) $this->pdfRect(108.2, 158.2, 1.5, $pdf, 1.5);
	
}

$pdf->Text(32.2, 223.1, '1');
if ($i == 0) $this->pdfRect(21.0, 225.4, 50, $pdf, 2.5);


#$this->pdfClean(6, 290, 40, $pdf, 255, 255, 255, 8, 'F');


$tplidx = $pdf->importPage(5, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 208);

if ($i == 0) $this->pdfRect(21.1, 142.8, 1.5, $pdf, 1.5);
if ($i == 0) $this->pdfRect(21.1, 172.6, 1.5, $pdf, 1.5);

$pdf->Text(21.2, 217.5, 'X');



for($p=6;$p<=14;$p++)
{
    $tplidx = $pdf->importPage($p, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 208);

    if($p == 7) {
        if ($i == 0) $this->pdfRect(26.5, 90.4, 40, $pdf, 3);
        if ($i == 0) $this->pdfRect(108.9, 90.4, 40, $pdf, 3);
        if ($i == 0) $this->pdfRect(83.9, 90.4, 14, $pdf, 3);

        if ($i == 0) $this->pdfRect(26.5, 122.9, 40, $pdf, 3);
        if ($i == 0) $this->pdfRect(108.9, 122.9, 40, $pdf, 3);
        if ($i == 0) $this->pdfRect(83.9, 122.9, 14, $pdf, 3);

        $pdf->Text(20.1, 137.2, 'x');
        $pdf->Text(23.3, 141.3, 'x');
        $pdf->Text(83.3, 141.3, 'x');

        $pdf->Text(30, 183.1, 'Versicherungsmakler Experten GmbH, Feursstr. 56 / RGB, 82140 Olching');
        $pdf->Text(30, 190.3, 'allgemein@vm-experten.de, 08142 651 39 28');
    }

}

?>
