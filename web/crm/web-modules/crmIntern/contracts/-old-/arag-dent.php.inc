<?

// File for generating Contract PDF for ARAGz90
        if ($i==0) {
            $pagecount = $pdf->setSourceFile($path.'files/arag/antrag/2018/ARAG.Dent.Antrag.NEU.05-2018.pdf');
        } else {
            $pagecount = $pdf->setSourceFile($path.'files/arag/antrag/2018/ARAG.Dent.Antrag.NEU.05-2018.pdf');
        }
        $tplidx = $pdf->importPage(1, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        $fontSize = 8;
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $fontSize+3, 0, 2);
        $pdf->Text(168.0, 4.5, 'Insuro Partner 11580');

        if($i==0) $this->pdfRect(29, 1.0, 68, $pdf);
        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $fontSize+5, 0, 2);
        $pdf->Text(29.0, 5.0, $titles[$i]);

        $pdf->Text(41, 155.4, $data['begin']);

    // reset font
        $pdfCfg['fontSize'] = $fontSize;
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        // Makler Daten schreiben
        #$pdf->Text(40.5, 24.5, "5");
        #$pdf->Text(44.5, 24.5, "1");
        #$pdf->Text(49.0, 24.5, "7");
        #$pdf->Text(53.5, 24.5, "3");
        #$pdf->Text(58.0, 24.5, "0");

        #$pdf->Text(68.5, 24.5, "6");
        #$pdf->Text(73.0, 24.5, "0");
        #$pdf->Text(77.0, 24.5, "0");

    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-2, 0, 2);

        $this->getCompanyStamp(116, 12, $pdf, $data['contractComplete']['sourcePage'], 8);

    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);

        // Neuantrag
        $pdf->Text(31.8, 60.7, 'X');

        // Fernabsatz
        $pdf->Text(89.8, 60.7, 'X');

        if($data['gender']=='male') {
            $pdf->Text(31.8, 73.7, 'X');
        } else {
            $pdf->Text(31.8, 77.2, 'X');
        }   

        $pdf->Text(44, 78, $data['name']);
        $pdf->Text(38, 87, $data['street']);
        $pdf->Text(147.6, 78.5, $data['birthdate']);

        if(empty($data['phone']))
        {
            if($i==0) $this->pdfRect(146, 92, 50, $pdf);
        } else
            $pdf->Text(145, 96.3, $data['phone']);

        if(empty($data['email']))
        {
            if($i==0) $this->pdfRect(32, 101, 68, $pdf);
        } else
            $pdf->Text(38, 105, $data['email']);

        $pdf->Text(38, 96, $data['postcode']);
        $pdf->Text(60, 96, $data['city']);

        if(empty($data['job']))
        {
            if($i==0) $this->pdfRect(58.0, 111, 68, $pdf);
        } else
            $pdf->Text(60, 114.3, $data['job']);

        //mark Angestellter / Selbständiger / Freiberufler
        if($i==0) $this->pdfRect(31.6, 107.8, 2, $pdf, 2);
        if($i==0) $this->pdfRect(31.6, 111.4, 2, $pdf, 2);
        if($i==0) $this->pdfRect(31.6, 114.4, 2, $pdf, 2);

        // Ich bin damit einverstanden....
        if($i==0) $this->pdfRect(31.6, 129, 2, $pdf, 2);
        

    //Zahlungsweise
        //jährlich (4% skonto)
        if($i==0) $this->pdfRect(31.7, 161.9, 2, $pdf, 2);

        //halbjährlich (2% skonto)
        if($i==0) $this->pdfRect(65.4, 161.9, 2, $pdf, 2);

        //vierteljährlich
        if($i==0) $this->pdfRect(102.4, 161.9, 2, $pdf, 2);

        //monatlich
        if($i==0) $this->pdfRect(123.7, 161.9, 2, $pdf, 2);

        //zum ersten eines Monats
        if($i==0) $this->pdfRect(102.4, 165.3, 2, $pdf, 2);

        //zum 15. eines Monats
        if($i==0) $this->pdfRect(131.5, 165.3, 2, $pdf, 2);

    //Zu versichernde Person 1
        // person insured
        $pdf->Text(35, 191, $data['street'].', '.
            $data['postcode'].' '.$data['city']);

        //mark Angestellter / Selbständiger / Freiberufler
        if($i==0) $this->pdfRect(31.7, 194.2, 2, $pdf, 2);
        if($i==0) $this->pdfRect(31.7, 197.1, 2, $pdf, 2);
        if($i==0) $this->pdfRect(31.7, 200.6, 2, $pdf, 2);

        $pdf->Text(44, 182.5, $data['name2b']);
        $pdf->Text(148, 182.5, $data['birthdate2']);

        if(empty($data['job2']))
        {
            if($i==0) $this->pdfRect(57.5, 196.4, 68, $pdf);
        } else
            $pdf->Text(60, 200.5, $data['job2']);


            //print_r($data);

        if($data['gender2']=='male')    {
            $pdf->Text(31.7, 177.9, 'X');
        } else {
            $pdf->Text(31.7, 181.4, 'X');
        }

        

    // page 2
        $tplidx = $pdf->importPage(2, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

       // tariff
        switch($pdfTemplate)
        {
            case project::gti('arag-dent70'):
                $pdf->Text(58.5, 13.8, "X");
                break;
            case project::gti('arag-dent90'):
                $pdf->Text(74.9, 13.8, "X");
                break;
            case project::gti('arag-dent90plus'):
                $pdf->Text(91, 13.8, "X");
                break;
            case project::gti('arag-dent100'):
                $pdf->Text(108.7, 13.8, "X");
                break;
        }

        // prices
        $pdf->Text(143, 18, $data['bonusbase']);
        $pdf->Text(180, 18, $data['bonus']);
        $pdf->Text(180, 50.5, $data['price']);
 



        // Person 1 2
        if($i==0) $this->pdfRect(58.6, 102.3, 2.6, $pdf, 2.6);
        if($i==0) $this->pdfRect(67.3, 102.3, 2.6, $pdf, 2.6);

        #if(empty($data['insurance2']))
        #{
        #   if($i==0) $this->pdfRect(58, 65, 86, $pdf);
        #} else
        #   $pdf->Text(60.0, 69.0, 'X'.$data['insurance2']);

        //2a, Person 1 94
        if($i==0) $this->pdfRect(133.2, 119, 2, $pdf, 2);
        if($i==0) $this->pdfRect(144.8, 119, 2, $pdf, 2);
        if($i==0) $this->pdfRect(133.2, 127, 2, $pdf, 2);
        if($i==0) $this->pdfRect(144.8, 127, 2, $pdf, 2);
        if($i==0) $this->pdfRect(133.2, 158.8, 2, $pdf, 2);
        if($i==0) $this->pdfRect(144.8, 158.8, 2, $pdf, 2);

    if($data['contractComplete']) {
        if($data['incare']) {
            $pdf->Text(133, 121.3, 'X');
        } else {
            $pdf->Text(144.8, 121.3, 'X');
        }

        // Paradontose Behandlung
        // "yes" 20140314
        if ($data['contractComplete']['periodontitis'] == 'yes') {
            $pdf->Text(133.1, 130.3, 'X');
        } else {
            $pdf->Text(144.8, 130.3, 'X');
        }
    }
        //2c
        if($i==0) $this->pdfRect(134.6, 140.9, 8, $pdf, 4);

    if($data['contractComplete']) {
        $tsum = $data['t1'] + $data['t2'];
        if ($tsum == 0) {
            $pdf->Text(144.8, 145.1, 'X');
        } 

        $pdf->Text(137.2, 144, $tsum);

        if ($data['contractComplete']['periodontitis'] != 'yes' && ( ! $data['incare']) && $tsum > 0) {
            $pdf->Text(133.5, 160.2, 'X'); // b, insure toothes, yes
        } else {
            $pdf->Text(145.1, 160.2, 'X'); // b, insure toothes, no
        }
    }


    // page 3
        $tplidx = $pdf->importPage(3, '/MediaBox');
        $pdf->addPage('P', array(210, 320));
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

    //Unterschriften
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $fontSize+5, 0, 2);

        //Ort,Datum
        if($i==0) $this->pdfRect(49, 120.6, 65, $pdf, 6);

        //Antragsteller
        if($i==0) $this->pdfRect(126, 120.6, 65, $pdf, 6);

        //1. zu versichernde volljährige Person
        if($i==0) $this->pdfRect(49, 131, 65, $pdf, 6);


            $pdf->Text(49.5, 125.1, 'X');
            $pdf->Text(125.5, 125.1, 'X');
            $pdf->Text(49.5, 135, 'X');

        $agePersonInsured = actionHelper::getAge($data['personInsured']['birthdate']);

        #if($agePersonInsured >= 16 && $agePersonInsured < 18) {
            if($i==0) $this->pdfRect(126, 131, 65, $pdf, 6);
            $pdf->Text(125.5, 135, 'X');
        #}


        // Empfangsbestätigung
        //Ort,Datum
        if($i==0) $this->pdfRect(49, 199.1, 65, $pdf, 5);

        //Antragsteller
        if($i==0) $this->pdfRect(125, 199.1, 65, $pdf, 5);

        //1. zu versichernde volljährige Person
        if($i==0) $this->pdfRect(49, 216.4, 65, $pdf, 5);


            $pdf->Text(49.5, 203.1, 'X');
            $pdf->Text(125.5, 203.1, 'X');
            $pdf->Text(49.5, 220.4, 'X');

        $agePersonInsured = actionHelper::getAge($data['personInsured']['birthdate']);

        #if($agePersonInsured >= 16 && $agePersonInsured < 18) {
            if($i==0) $this->pdfRect(125, 216.4, 65, $pdf, 5);
            $pdf->Text(125.5, 220.4, 'X');
        #}

        $pdf->SetFont($pdfCfg['fontFamily'], '', $fontSize+3, 0, 2);

    // page 4 
        $tplidx = $pdf->importPage(4, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        $pdf->Text(10.4, 48.6, 'x');
        $pdf->Text(40, 54, $data['signupDate'][0]);
        $pdf->Text(43, 54, $data['signupDate'][1]);
        $pdf->Text(48.0, 54, $data['signupDate'][3]);
        $pdf->Text(51, 54, $data['signupDate'][4]);
        $pdf->Text(63, 54, $data['signupDate'][8]);
        $pdf->Text(66, 54, $data['signupDate'][9]);

        if($data['gender']=='male') {
            $pdf->Text(31.6, 82.9, 'X');
        } else {
            $pdf->Text(31.6, 85.9, 'X');
        }   

        $pdf->Text(46, 87, $data['name']);
        $pdf->Text(46, 96, $data['street']);
        $pdf->Text(46, 105, $data['postcode']);
        $pdf->Text(70, 105, $data['city']);

        if($i==0) $this->pdfRect(34, 144, 90, $pdf, 5);
        if($i==0) $this->pdfRect(34, 153, 90, $pdf, 5);
        if($i==0) $this->pdfRect(34, 162, 40, $pdf, 5);
        
        if($i==0) $this->pdfRect(34, 189, 77, $pdf, 8);
        if($i==0) $this->pdfRect(119, 189, 77, $pdf, 8);

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $fontSize+4, 0, 2);
        $pdf->Text(34, 195, 'X');
        $pdf->Text(122, 195, 'X');

    // page 5
        $tplidx = $pdf->importPage(5, '/MediaBox'); 
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 9, -1, 193);
    // page 6
        $tplidx = $pdf->importPage(6, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
    // page 7 SEPA
        $tplidx = $pdf->importPage(7, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
    // page 8
        $tplidx = $pdf->importPage(8, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
    // page 9
        $tplidx = $pdf->importPage(9, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
    // page 10
        $tplidx = $pdf->importPage(10, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
    // page 11
        $tplidx = $pdf->importPage(11, '/MediaBox');
        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

    if($complete)
        $pdf->addPage('P');
?>
