<?php
// File for generating Contract PDF for Signal Iduna Kompakt (Start & Plus & Top)

        /*
         * Constants
         */
        // SQUARE XY
        define('X_Y', 1.5);

        // RECT Y
        define('Y', 2.7);

        $data['person']['age'] = actionHelper::getAge($data['contractComplete']['birthdate']);

        if ($i!=1) {
            $pagecount = $pdf->setSourceFile($path.'files/signal-iduna/antrag/2020/ge-serie.pdf');
        } else {
            $pagecount = $pdf->setSourceFile($path.'files/signal-iduna/antrag/2020ge-serie.pdf');
        }

        $tplidx = $pdf->importPage(1, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
        $pdf->SetMargins(0, 0, 0);

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);

        if($i==0) $this->pdfRect(75.0, 3.0, 70, $pdf);
        $pdf->Text(75.0, 7.0, $titles[$i]);

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-1, 0, 2);

        // -> Seite 2
        $pdf->Text(90, 14.0, "Versicherungsmakler Experten GmbH");

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-3, 0, 2);

        //mark section
        //Selbstst�ndiger/Firma/Verein
        
        if($i==0) {
            $this->pdfRect(28.5, 64, X_Y, $pdf, X_Y);
            $this->pdfRect(47, 64, X_Y, $pdf, X_Y);
            $this->pdfRect(62.6, 64, X_Y, $pdf, X_Y);
            $this->pdfRect(74.9, 64, X_Y, $pdf, X_Y);
            $this->pdfRect(88, 64, X_Y, $pdf, X_Y);
        }
        

        //freiwillig versichert?
        if($i==0) {
            $this->pdfRect(131.7, 73.3, X_Y, $pdf, X_Y);
            $this->pdfRect(150.6, 73.3, X_Y, $pdf, X_Y);
            $this->pdfRect(150.6, 76.1, X_Y, $pdf, X_Y);
            $this->pdfRect(162.6, 76.1, X_Y, $pdf, X_Y);
        }

        //versichert seit...
        //if($i==0) $this->pdfRect(162, 71.2, 27.5, $pdf, Y);


        //mark section end
        $pdf->Text(158, 65.3, 'X'); // Beihilfe
        $pdf->Text(185.4, 65.3, 'X'); // Heilfuersorge

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);    

        if($data['gender']=='male') {
            $pdf->Text(16.4, 47.8, 'X');
        } else {
            $pdf->Text(16.4, 51.0, 'X');
        }

        if($data['widowed']) {
            $pdf->Text(16.4, 63.8, 'X');
        } else if($data['married']) {
            $pdf->Text(16.4, 60.2, 'X');
        } else {
            $pdf->Text(16.4, 57.4, 'X');
        }

        // Bereits Kunde?
        $pdf->Text(146.0, 37.6, 'X');
        
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
        $pdf->Text(30, 48.0, $data['name']);
        $pdf->Text(143.8, 48.0, $data['birthdate']);

        if($data['nation']=='DEU') {
            $pdf->Text(189.5, 44.6, 'x');
        } elseif(empty($data['nation'])) {
            $this->pdfRect(167.5, 44.6, 30, $pdf, 3.5);
        } else {
            $pdf->Text(168, 48.0, $data['nation']);
        }
 

        $pdf->Text(29.0, 55.0, $data['street']);

        $pdf->Text(116, 55.0, $data['postcode'].' '.$data['city']);

        if( ! empty($data['job'])) {
            $pdf->Text(29.0, 61.2, $data['job']);
        } else {
           if($i==0) 
                $this->pdfRect(28.5, 58.0, 30, $pdf, 4);
        }

        //Telefonnummer
        if(!empty($data['phone']) || $data['phone']=='')
        {
            if($i==0) $this->pdfRect(15.8, 82, 39, $pdf, 3);
        } else
            $pdf->Text(16.5, 84.4, $data['phone']);

        //eMail
        if(!empty($data['email']) || $data['email']=='')
        {
            if($i==0) $this->pdfRect(102, 82, 53, $pdf, 3);
        }
        else 
            $pdf->Text(105.0, 84.4, $data['email']);

        $pdf->Text(162, 80.4, $data['mobile']);

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);    


        if(!empty($data['insurance'])) {
            $pdf->Text(29.0, 78.5, $data['insurance']);
        } else {
            if($i==0) $this->pdfRect(28.5, 75.5, 62, $pdf, 3.5);
        }

        // Insured since
        if($i==0) $this->pdfRect(180, 75.4, 20, $pdf, 3.5);






    // 3 Beitragszahlung

        //mark section

        //Zahlungsweise
        if($i==0) {
            $this->pdfRect(36.0, 113.1, X_Y, $pdf, X_Y);
            $this->pdfRect(50.3, 113.1, X_Y, $pdf, X_Y);
            $this->pdfRect(66.7, 113.1, X_Y, $pdf, X_Y);
            $this->pdfRect(83.6, 113.1, X_Y, $pdf, X_Y);
        }

        //Konto
        if($i==0) {
            $this->pdfRect(17.5, 144.4, 100, $pdf, Y);
            $this->pdfRect(156.0, 144.4, 38, $pdf, Y);
            $this->pdfRect(17.5, 152.7, 89, $pdf, Y);
            $this->pdfRect(117.0, 152.7, 24, $pdf, Y);
            $this->pdfRect(149.5, 152.7, 47, $pdf, Y);
        }
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
        if($i==0) $pdf->Text(149, 155, 'X');

        // check for child
        if($data['name2b'] != $data['name']) {
            // setze marker bei Antragsteller / VN
            $pdf->Text(116.1, 41.2, 'X');            
            $data['personInsured']['age'] = actionHelper::getAge($data['personInsured']['birthdate']);

        //mark section
        //Selbstst�ndiger/Firma/Verein

            if($i==0) {
                $this->pdfRect(28.9, 199.7, X_Y, $pdf, X_Y);
                $this->pdfRect(47.4, 199.7, X_Y, $pdf, X_Y);
                $this->pdfRect(63, 199.7, X_Y, $pdf, X_Y);
                $this->pdfRect(75.3, 199.7, X_Y, $pdf, X_Y);
                $this->pdfRect(88, 199.7, X_Y, $pdf, X_Y);
            }
            

            //freiwillig versichert?
            if($i==0) {
                $this->pdfRect(131.9, 209.9, X_Y, $pdf, X_Y);
                $this->pdfRect(150.6, 209.9, X_Y, $pdf, X_Y);
                $this->pdfRect(150.6, 212.4, X_Y, $pdf, X_Y);
                $this->pdfRect(162.7, 212.4, X_Y, $pdf, X_Y);
            }
                        
                //mark section end


                
                $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);

                $pdf->Text(29.0, 177, $data['name2b']);
                $pdf->Text(29.0, 183, $data['street'].', '.$data['postcode'].' '.$data['city']);
                $pdf->Text(148.0, 177, $data['birthdate2']);
                if($data['gender2']=='male')    {
                    $pdf->Text(16.5, 180.6, 'X');
                } else {
                    $pdf->Text(16.5, 183.9, 'X');
                }

                if($data['widowed2']) {
                    $pdf->Text(16.5, 196.5, 'X');
                } else if($data['married2']) {
                    $pdf->Text(16.5, 193.2, 'X');
                } else {
                    $pdf->Text(16.5, 190.0, 'X');
                }

                $pdf->Text(157.8, 201.5, 'X'); // Beihilfe
                $pdf->Text(185.3, 201.5, 'X'); // Heilfuersorge
                
                if($data['nation2']=='DEU') {
                    $pdf->Text(189.5, 173.5, 'x');
                } elseif(empty($data['nation2'])) {
                    $this->pdfRect(167.5, 173.8, 30, $pdf, 3.5);
                } else {
                    $pdf->Text(168, 177, $data['nation2']);
                }

                if(!empty($data['job2'])) {
                    $pdf->Text(29.0, 190.5, $data['job2']);
                } else {
                    if($i==0) $this->pdfRect(28.5, 188, 80, $pdf, 3);
                }

                if(!empty($data['insurance2'])) {
                    $pdf->Text(29.0, 214, $data['insurance2']);
                } else {
                    if($i==0) $this->pdfRect(28.5, 211.2, 80, $pdf, 3);
                }

                // Insured since
                if($i==0) $this->pdfRect(180, 211.2, 20, $pdf, 3.5);

                #if($i==0) $this->pdfRect(163, 186.0, 26, $pdf, 3);
                $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        }
        // end child
        else {
            // setze marker bei Antragsteller / VN
            $pdf->Text(70.1, 41.2, 'X');
        }


// SEITE 2
        $tplidx = $pdf->importPage(2, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);


    // 4 Versicherungsbeginn und beantragter Versicherungsschutz
        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);


        if($data['name2b'] == $data['name']) {
            $this->pdfClean(49.3, 67.8, 10, $pdf, 255, 255, 255, 3.7);
            $pdf->Text(50.4, 70.9, $data['price']);
        }
        else {
            $this->pdfClean(99.9, 67.8, 10, $pdf, 255, 255, 255, 3.7);
            $pdf->Text(101.0, 70.9, $data['price']);
        }
        $this->pdfClean(177.8, 67.8, 10, $pdf, 255, 255, 255, 3.7);
        $pdf->Text(178.8, 70.9, $data['price']);

        $pdf->Text(170, 29.8, '01.'. $data['begin']);


        // choose tariff
        if($pdfTemplate == project::gti('sigidu-komplus')) {
            if($data['name2b'] == $data['name']) {
                $pdf->Text(18.7, 48.7, 'X');
                $pdf->Text(18.7, 53.6, 'X');
                $pdf->Text(18.7, 63.7, 'X');
            } else {
                $pdf->Text(68.6, 48.7, 'X');
                $pdf->Text(68.6, 53.6, 'X');
                $pdf->Text(68.6, 63.7, 'X');
            }
        }
        else if($pdfTemplate == project::gti('sigidu-komstart')){
            if($data['name2b'] == $data['name'])
            {
                $pdf->Text(18.7, 48.7, 'X');
                $pdf->Text(18.7, 63.7, 'X');
            } else {
                $pdf->Text(68.6, 48.7, 'X');
                $pdf->Text(68.6, 63.7, 'X');
            }
        }
        else if($pdfTemplate == project::gti('sigidu-komtop')){
            if($data['name2b'] == $data['name'])
            {
                $pdf->Text(18.7, 48.7, 'X');
                $pdf->Text(18.7, 58.7, 'X');
                $pdf->Text(18.7, 63.7, 'X');
            } else {
                $pdf->Text(68.6, 48.7, 'X');
                $pdf->Text(68.6, 58.7, 'X');
                $pdf->Text(68.6, 63.7, 'X');
            }
        }

        $this->getCompanyStamp(100.0, 5, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize'], 'horizontal', 'wide');

        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        $pdf->Text(33.0, 7.1, $data['name']);

        $pdf->Text(16.3, 144.3, 'X');

        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+20, 0, 2);
        $pdf->SetTextColor(35,122,78);

           $pdf->Text(9, 118.4, '!');
           $pdf->Text(203.6, 118.4, '!');

        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
        $pdf->SetTextColor(0,0,0);


        // Fragen an die zu versichernde Person
        
        //ja / nein
        if($data['name2b'] != $data['name'])
        {  //mark section
            if($i==0) {
                    $this->pdfRect(171.6, 111.9, X_Y, $pdf, X_Y);
                    $this->pdfRect(179.8, 111.9, X_Y, $pdf, X_Y);

                    $this->pdfRect(171.6, 114.7, X_Y, $pdf, X_Y);
                    $this->pdfRect(179.8, 114.7, X_Y, $pdf, X_Y);

                    $this->pdfRect(171.6, 119.9, X_Y, $pdf, X_Y);
                    $this->pdfRect(179.8, 119.9, X_Y, $pdf, X_Y);
            }
        } else {
                //mark section
                if($i==0) {
                    $this->pdfRect(156.6, 111.9, X_Y, $pdf, X_Y);
                    $this->pdfRect(164.7, 111.9, X_Y, $pdf, X_Y);

                    $this->pdfRect(156.6, 114.7, X_Y, $pdf, X_Y);
                    $this->pdfRect(164.7, 114.7, X_Y, $pdf, X_Y);

                    $this->pdfRect(156.6, 119.9, X_Y, $pdf, X_Y);
                    $this->pdfRect(164.7, 119.9, X_Y, $pdf, X_Y);
                }
        }
        

// SEITE 3
        $tplidx = $pdf->importPage(3, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        $this->getCompanyStamp(100.0, 5, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize'], 'horizontal', 'wide');

        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);

        $pdf->Text(33, 7.8, $data['name']);

        // Unterschriften
        
        // Antragsteller
        if($i==0) $this->pdfRect(18.5, 194.5, 28.0, $pdf, Y);
        if($i==0) $this->pdfRect(52.0, 194.5, 44.0, $pdf, Y);
        $pdf->Text(52, 197.2, 'X');



        // Unterschrift der mitzuversichernden Person ab 16 Jahre
        if(isset($data['personInsured']['age']))
        {
            if($data['personInsured']['age'] >= 16)
            {
                if($i==0) $this->pdfRect(100.5, 194.5, 44.0, $pdf, Y);
                $pdf->Text(102, 197.2, 'X');
            }
        }


        // Antragsteller minderj�hrig oder versicherte Person minderj�hrig
        if($data['person']['age'] <= 18 || ($data['personInsured']['age'] <=18) && (isset($data['personInsured']['age'])))
        {
            if($i==0) $this->pdfRect(152, 194.5, 44.0, $pdf, Y);
            $pdf->Text(153, 197.2, 'X');


            if($data['person']['age'] <= 18)
            {
                $this->pdfRect(113.0, 219.3, 82.0, $pdf, 3.2);
                $pdf->Text(115.0, 222, 'X');
            }
        }

        // Empfangsbest�tigung
        if($i==0) {
            $this->pdfRect(17.5, 219.3, 82.0, $pdf, 3.2);
            $pdf->Text(18.5, 222, 'X');
        }

        // Reset
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
        // Makler
        $pdf->Text(148.4, 235.2, 'X');
        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
        // Vmtl-Nr
        $pdf->Text(18.5, 243, '226 / 1500');


// SEITE 4
        $tplidx = $pdf->importPage(4, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);


// SEITE 5
        if($data['contractComplete']['reha'] == 'yes')
        {
            $pdf->setSourceFile($path.'/files/1200802_kurtagegeld.pdf');
            $tplidx = $pdf->importPage(1, '/MediaBox');

            $pdf->addPage('P');
            $pdf->useTemplate($tplidx, 0, 10, 200);

            $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);
            $pdf->Text(20, 69, $data['signupDate']);
            $pdf->Text(123, 69, $data['namec']);
            if($data['name2b'] != $data['name']) {
                $pdf->Text(20, 88, $data['name2']);
                $pdf->Text(123, 88, $data['birthdate2']);
            }
            else {
                $pdf->Text(20, 88, $data['namec']);
                $pdf->Text(123, 88, $data['birthdate']);
            }

            if($i==0) $this->pdfRect(16.5, 262, 38, $pdf, 6);
            if($i==0) $this->pdfRect(59.2, 262, 110, $pdf, 6);

            $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+2, 0, 2);
            $pdf->Text(18, 267, 'X');
            $pdf->Text(61, 267, 'X');


         $pdf->addPage('P');
        }

/*
        $pdf->Text(15, 160, 'Hinweis :');
        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
        if($data['name2b'] != $data['name']) {
            $pdf->Text(15, 164, 'Anzahl ben�tigter Unterschriften bei Versicherung von Kindern: 6 St�ck (2 x links Vorderseite, 2 x rechts Vorderseite,');
            $pdf->Text(15, 167, '1 x links auf der R�ckseite, 1 x rechts auf der R�ckseite = 6 Unterschriften!!!)');
        } else {
            $pdf->Text(15, 164, 'Anzahl ben�tigter Unterschriften bei Versicherung Erwachsener: 3 St�ck (2 x links Vorderseite, 1 x links auf der R�ckseite = 3 Unterschriften)');
        }
*/

?>