<?
// File for generating Contract PDF for Universa Dent Privat

    if($i==0) {
        $pagecount = $pdf->setSourceFile($path.'/files/universa/antrag/2020/KVA-903-prospektantrag_zahnzusatzversicherung-uni_dent_komfort_privat_11_2019.pdf');
    } else {
        $pagecount = $pdf->setSourceFile($path.'/files/universa/antrag/2020/KVA-903-prospektantrag_zahnzusatzversicherung-uni_dent_komfort_privat_11_2019.pdf');
    }
        $pdf->addPage('L');
        $tplidx = $pdf->importPage(1, '/MediaBox');
        $pdf->useTemplate($tplidx, 1.7, 1, 295.4);
           $pdf->SetMargins(0, 0, 0);

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+2, 0, 2);

        if($i==0) $this->pdfRect(86.0, 0, 62, $pdf);
        $pdf->Text(87.0, 4.0, $titles[$i]);


        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

        // $this->pdfClean(145, 20.8, 20, $pdf, 255, 255, 255, 4, 'F');
        // $this->pdfClean(170, 20.8, 20, $pdf, 255, 255, 255, 4, 'F');

        // AV-Nummer
        $pdf->Text(35.5, 14.7, "54920"); 

        // Antragsteller
        $pdf->Text(24, 42, $data['person']['surname']);
        $pdf->Text(89, 42, $data['person']['forename']);

        if($data['gender']=="female")
            $pdf->Text(14.2, 43.1, "x");
        else
            $pdf->Text(14.2, 40.7, "x");

        $pdf->Text(157, 42, $data['birthdate']);

        $pdf->Text(16, 50, $data['street']);

        $pdf->Text(104, 50, $data['postcode']);
        $pdf->Text(125, 50, $data['city']);



        // zu versichernde Person
        $pdf->Text(22, 63.8, $data['personInsured']['surname']);
        $pdf->Text(49, 63.8, $data['personInsured']['forename']);
        $pdf->Text(85, 63.8, $data['birthdate2']);
        if($data['gender2']=="female")
            $pdf->Text(14, 65, "x");
        else
            $pdf->Text(14, 62.6, "x");



        // Begin
        //$this->pdfClean(155.3, 64.0, 34, $pdf, 255,255,255, 5.2);
        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+2, 0, 2);

        $pdf->Text(152, 21, substr($data['begin'], 0, 2));
        $pdf->Text(163, 21, substr($data['begin'], 5, 2));

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-1, 0, 2);

        // Welcher Tarif?
        $pdfTemplate = isset($pdfTemplate)?$pdfTemplate:$data['idt'];
        switch($pdfTemplate) {
            case project::gti('universa-uni-dent-privat'):
                // Person 1 bei uni-dent|Privat
                $pdf->Text(120, 116.4, "X");

                // uni-dent|Komfort durchstreichen
                $pdf->SetLineWidth(3.0);
                $pdf->SetDrawColor(246,246,246);
                $pdf->Line(12, 125, 80, 81);
                break;
            case project::gti('universa-uni-dent-komfort'):
                // Person 1 bei uni-dent|Komfort
                $pdf->Text(53, 116.4, "X");

                // uni-dent|Privat durchstreichen
                $pdf->SetLineWidth(3.0);
                $pdf->SetDrawColor(246,246,246);
                $pdf->Line(119, 125, 179, 81);
                break;
            }


    // Seite 2
        $pdf->addPage('L');
        $tplidx = $pdf->importPage(2, '/MediaBox');
        //$pdf->useTemplate($tplidx, 6.5, 6, 283);
        $pdf->useTemplate($tplidx, 0.8, 1, 295.4);


        //Eigene Anschrift einblenden
        $this->pdfClean(210, 23, 40, $pdf, 255,1005, 255, 70);

        $this->getCompanyStamp(204.0, 40, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']+2);
        
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

        // Vor- und Zuname Kontoinhaber
        if($i==0) $this->pdfRect(101.7, 72.1, 90, $pdf);
        // Adresse Kontoinhaber
        if($i==0) $this->pdfRect(101.7, 80.6, 90, $pdf);
        // Geldinstitut
        if($i==0) $this->pdfRect(101.7, 89.6, 90, $pdf);
        // IBAN
        if($i==0) $this->pdfRect(110.7, 100.6, 81, $pdf);

        // Ort und Datum
        if($i==0) $this->pdfRect(101.7, 112.6, 90, $pdf, 4);
        // Kontoinhaber
        if($i==0) $this->pdfRect(101.7, 122.7, 90, $pdf, 3.5);

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);
        if($i==0) $pdf->Text(104.7, 125.7, "X");


        //Unterschrift
        if($i==0) $this->pdfRect(101.7, 155.7, 90, $pdf);
        if($i==0) $this->pdfRect(101.7, 167.2, 90, $pdf);
        if($i==0) $this->pdfRect(101.7, 177.5, 90, $pdf);
        if($i==0) $pdf->Text(102.2, 170.7, "X");
        if($i==0) $pdf->Text(102.2, 181, "X");

        if (project::gti('universa-uni-dent-privat'))
        {
            #$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);
            #if($i==0) $pdf->Text(196, 158.5, '*');
            #$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
            #if($i==0) $pdf->Text(100, 195, '* auf die Beitragsanpassung zum 1.1.2018 wurde ich hingewiesen!');
        }


        $this->pdfClean(200, 150, 60, $pdf, 255, 255, 255, 50, 'F');

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

?>