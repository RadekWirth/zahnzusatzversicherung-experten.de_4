<?
// File for generating Contract PDF for Janitos JA Dental +

        if($i==0) {
            $pagecount = $pdf->setSourceFile($path.'/files/janitos/antrag/2017/Janitos.Dental.Max.Plus.Antrag.NEU.06-2017.pdf');
        } else {
            $pagecount = $pdf->setSourceFile($path.'/files/janitos/antrag/2017/Janitos.Dental.Max.Plus.Antrag.NEU.06-2017.pdf');
        }

        $tplidx = $pdf->importPage(1, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
        $pdf->SetMargins(0, 0, 0);

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);

        //gelbe markierungen nur auf r�ckantwort drucken!
        if($i==0) $this->pdfRect(75, 1, 68, $pdf);

        $pdf->Text(75.0, 5.0, $titles[$i]);

        $this->getCompanyStamp(108.0, 14, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']-2, 'horizontal', 'wide');

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-3, 0, 2);

        // Antragsteller
        if($data['gender']=='female')   {
            $pdf->Text(187.9, 31.3, 'X');
        }
        else {
            $pdf->Text(193.4, 31.3, 'X');
        }

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

        $pdf->Text(11.5, 32.5, $data['name']);
        $pdf->Text(11.5, 39.5, $data['street']);
        $pdf->Text(11.5, 46.5, $data['postcode']." , ".$data['city']);


        $pdf->Text(154.0, 54.0, $data['birthdate']);
        if(empty($data['job']))
        {
            if($i==0) $this->pdfRect(107.0, 58.5, 50, $pdf, 3.0);
        } else
            $pdf->Text(107.0, 61.0, $data['job']);

        if(empty($data['phone']))
        {
        	if($i==0) $this->pdfRect(11.0, 51.5, 50, $pdf, 3.0);
        } else
        	$pdf->Text(11.5, 54.0, $data['phone']);

        if(empty($data['email']))
        {
            if($i==0) $this->pdfRect(11, 58.5, 50, $pdf, 3.0);
        } else
        $pdf->Text(11.5, 61.0, $data['email']);

        if($data['person']['nationCode']=='000') {
            $pdf->Text(107.0, 54.0, $data['nation']);
        } else $pdf->Text(107.0, 54.0, 'deutsch');



    // ZU VERSICHERNDE PERSON
        if($data['personInsured'])
            {
            $pdf->Text(11.5, 77.5, $data['name2b']);
            $pdf->Text(11.5, 84.5, $data['street']);
            $pdf->Text(11.5, 91.5, $data['postcode']." , ".$data['city']);

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-3, 0, 2);

            if($data['gender2']=='female')  {
                $pdf->Text(187.9, 76.4, 'X');
            } else
                $pdf->Text(193.6, 76.4, 'X');

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

        if(empty($data['job2']))
        {
            if($i==0) $this->pdfRect(11.5, 95.8, 50, $pdf, 3.0);
        } else
            $pdf->Text(11.5, 99.0, $data['job2']);


            $pdf->Text(107.0, 99.0, $data['birthdate2']);

        } // Ende Person1
        else
        {
            $pdf->Text(11.5, 76.0, $data['name']);
            $pdf->Text(11.5, 83.0, $data['street']);
            $pdf->Text(11.5, 90.0, $data['postcode']." , ".$data['city']);

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-3, 0, 2);

            if($data['gender']=='female')   {
                $pdf->Text(187.9, 74.9, 'X');
            } else
                $pdf->Text(193.6, 74.9, 'X');

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

        if(empty($data['job']))
        {
            if($i==0) $this->pdfRect(11.5, 94.3, 50, $pdf, 3.0);
        } else
            $pdf->Text(11.5, 97.5, $data['job']);
            $pdf->Text(107.0, 97.5, $data['birthdate']);
        }


    // VERMITTLER
	 $pdf->Text(11.5, 114.7, 'Versicherungsmakler Experten GmbH');
        $pdf->Text(11.5, 122.0, 'Maximilian Waizmann');
        $pdf->Text(11.5, 129.5, '17340001');

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-3, 0, 2);

    // TARIFAUSWAHL ZAHNZUSATZVERSICHERUNG
        $pdf->Text(11.5, 145.5, '01.'.$data['begin']);

	 // Welcher Tarif?
	 $pdfTemplate = isset($pdfTemplate)?$pdfTemplate:$data['idt'];
	 switch($pdfTemplate) {
		case project::gti('janitos-dentmax'):
			$pdf->Text(10.6, 152.8, "X");
			break;
	 	case project::gti('janitos-ja-dental-plus'):
			$pdf->Text(10.6, 155.3, "X");
			break;
		case project::gti('janitos-dental'):
			$pdf->Text(10.6, 157.8, "X");
			break;
	 }

        // Neuvertrag
        $pdf->Text(10.5, 163.2, 'X');

    // ZAHLWEISE
        if($i==0) $this->pdfRect(108.0, 149.7, 1.8, $pdf, 1.8);
        if($i==0) $this->pdfRect(108.0, 152.5, 1.8, $pdf, 1.8);
        if($i==0) $this->pdfRect(127.0, 149.7, 1.8, $pdf, 1.8);
        if($i==0) $this->pdfRect(127.0, 152.5, 1.8, $pdf, 1.8);


    // reset font
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

    // VERSICHERUNGSPR�MIE/BESONDERE VEREINBARUNG
        $pdf->Text(11.5, 189.7, project::gtfn($pdfTemplate));
        $pdf->Text(117.0, 189.7, $data['price']);

    // reset font
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-3, 0, 2);

    //Pagination raus
    $this->pdfClean(6, 290, 50, $pdf, 233, 233, 233, 6, 'F');

// PAGE 2
        $tplidx = $pdf->importPage(2, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);


    // ZAHNZUSATZVERSICHERUNG
        if($i==0) $this->pdfRect(102.0, 28.8, 2.0, $pdf, 2.0);
        if($i==0) $this->pdfRect(111.7, 28.8, 2.0, $pdf, 2.0);

        // Anzahl fehlender Z�hne
        if($i==0) $this->pdfRect(110, 37.5, 20, $pdf, 3.5);

        if($i==0) $this->pdfRect(101.5, 75.8, 2.5, $pdf, 2.5);
        if($i==0) $this->pdfRect(111.6, 75.8, 2.5, $pdf, 2.5);


        // Frage 1
	if($data['contractComplete']) {
        if($data['t2']>0) {
            $pdf->Text(111.8, 30.6, 'X');
        } else
            $pdf->Text(102.3, 30.6, 'X');
	}

        // Frage 2
        if($data['t1']>=0) {
            $pdf->Text(111.8, 40.5, $data['t1']==1?$data['t1'].' Zahn':$data['t1'].' Z�hne');
            if($data['t1'] == 2 || $data['t1'] == 3) {
                // Ich bin mit der ge�nderten L...
                if($i==0) $this->pdfRect(102.0, 47.7, 2.0, $pdf, 2.0);
                $pdf->Text(102.5, 49.6, 'X');
            }
        }


        // Frage 3
	if($data['contractComplete']) {
        if($data['contractComplete']['incare']=='yes') {
            $pdf->Text(111.9, 77.5, 'X');
        } else
            $pdf->Text(102.2, 77.5, 'X');
	}

        // Frage 4
        if(actionHelper::getAge($data['personInsured']['birthdate'])>=5 && actionHelper::getAge($data['personInsured']['birthdate'])<18) {
            #if($i==0) $this->pdfRect(100.0, 80.8, 90, $pdf, 6.0);
            if($i==0) $this->pdfRect(101.5, 83.3, 2.5, $pdf, 2.5);
            if($i==0) $this->pdfRect(111.6, 83.3, 2.5, $pdf, 2.5);
            if($data['contractComplete']['problem']==2 && $data['contractComplete']['incare']=='yes')
            {
                $pdf->Text(112.0, 85.5, 'X');
            }
            else {
	if($data['contractComplete']) {
                $pdf->Text(102.4, 85.5, 'X');
	}
            }

        }



    // WIR NEHMEN IHNEN ARBEIT AB
        if(empty($data['phone']))
        {
        if($i==0) $this->pdfRect(72.5, 106.7, 50, $pdf, 3.0);
        } else
        $pdf->Text(72.0, 108.7, $data['phone']);

        if(empty($data['email']))
        {
            if($i==0) $this->pdfRect(11, 106.7, 50, $pdf, 3.0);
        } else
        $pdf->Text(11.5, 108.7, $data['email']);

    //Pagination raus
    $this->pdfClean(6, 290, 50, $pdf, 232, 232, 232, 6, 'F');


    // PAGE 3
        $tplidx = $pdf->importPage(3, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);


    //Pagination raus
    $this->pdfClean(6, 290, 50, $pdf, 232, 232, 232, 6, 'F');

    // PAGE 4
        $tplidx = $pdf->importPage(4, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        if($i==0) $this->pdfRect(11.0, 53.5, 90, $pdf);
        if($i==0) $this->pdfRect(11.0, 64.5, 90, $pdf);
        if($i==0) $this->pdfRect(11.0, 78.7, 90, $pdf);
        if($i==0) $this->pdfRect(108.0, 53.5, 90, $pdf);
        if($i==0) $this->pdfRect(108.0, 64.5, 90, $pdf);
        if($i==0) $this->pdfRect(108.0, 78.7, 90, $pdf);

        if ($i==0) {
            $pdf->Text(11.5, 57.5, 'X');
            $pdf->Text(11.5, 68.5, 'X');
            $pdf->Text(11.5, 82.7, 'X');
            $pdf->Text(108.0, 57.5, 'X');
            $pdf->Text(108.0, 68.5, 'X');
            $pdf->Text(108.0, 82.7, 'X');
        }

    // reset font
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-3, 0, 2);


        $pdf->Text(11.5, 93.5, 'X');
        $pdf->Text(41.1, 93.5, 'X');
        $pdf->Text(65.8, 93.5, 'X');



    // reset font
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

    //Rote Faxnummer raus
    $this->pdfClean(6, 98.5, 124, $pdf, 232, 232, 232, 6, 'F');

    //Pagination raus
    $this->pdfClean(6, 290, 50, $pdf, 232, 232, 232, 6, 'F');


    // PAGE 6
        $tplidx = $pdf->importPage(6, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);


        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

        $pdf->Text(33.5, 71.5, $data['name']);
        $pdf->Text(33.5, 82.5, $data['street']);

        if($data['person']['nationCode']=='000') {
            $pdf->Text(33.5, 93.5, $data['nation']);
        } else $pdf->Text(33.5, 93.5, 'DE');

        $pdf->Text(52.5, 93.5, $data['postcode']);
        $pdf->Text(78.5, 93.5, $data['city']);



    // SEPA-LASTSCHRIFTMANDAT
        if($i==0) $this->pdfRect(33.0, 101.0, 92, $pdf, 4.0);
        if($i==0) $this->pdfRect(33.0, 112.0, 50, $pdf, 4.0);
        if($i==0) $this->pdfRect(33.0, 123.5, 92, $pdf, 4.0);

        if($i==0) $this->pdfRect(33.0, 134.5, 38, $pdf, 4.0);
        if($i==0) $this->pdfRect(75.0, 134.5, 34, $pdf, 4.0);
        if($i==0) $this->pdfRect(115.0, 134.5, 40, $pdf, 4.0);
        if($i==0) $this->pdfRect(158.0, 134.5, 40, $pdf, 4.0);
	 $pdf->Text(33.5, 152.5, $data['name']);


    //Pagination raus
    $this->pdfClean(6, 290, 50, $pdf, 232, 232, 232, 6, 'F');

	if($complete)
		$pdf->addPage('P');
