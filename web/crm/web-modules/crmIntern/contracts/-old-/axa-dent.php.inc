<?
    // Prüfung, ob Datei vorhanden ist...
    if ($i==0) {
        $pagecount = $pdf->setSourceFile($path.'/files/axa/antrag/Final_21009608_AXA_Zahnvorsorge.pdf');
    } else {
        $pagecount = $pdf->setSourceFile($path.'/files/axa/antrag/Final_21009608_AXA_Zahnvorsorge.pdf');
    }
    $tplidx = $pdf->importPage(2, '/MediaBox');

// SEITE 1
    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 1, 209);
    $pdf->SetMargins(0, 0, 0);

        $b1 = explode(".", $data['birthdate']);
        $b2 = explode(".", $data['birthdate2']);
        $wb = explode("-", $data['wishedBegin']);

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);

        if($i==0) $this->pdfRect(83.0, 2.0, 54, $pdf, 4.0);
            $pdf->Text(84.0, 5.0, $titles[$i]);

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-4, 0, 2);

        #$this->getCompanyLogo(39.0, 0.5, $pdf, $data['contractComplete']['sourcePage'], $i, 32);
        $this->getCompanyStamp(147, 1, $pdf, $data['contractComplete']['sourcePage'], 6.5);

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);


        // Orga-Nr
        // 8834003258 (Gueltig ab sofort oder ab 1.3.2015)
        $pdf->Text(41.2, 31.5, "8 8 3 4");
        $pdf->Text(57, 31.5, "3 2 5 8");

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);


        if($data['gender']=='female') {
            $pdf->Text(41.3, 42.5, "x");
        } else {
            $pdf->Text(41.3, 39.4, "x");
        }

        $pdf->Text(87, 42.5, $data['person']['forename']);
        $pdf->Text(157, 42.5, $data['person']['surname']);

        $pdf->Text(62, 49.5, $data['street']);
        $pdf->Text(135, 49.5, $data['postcode'].'       '.$data['city']);

        $pdf->Text(175, 56.5, $b1[0].'.'.$b1[1].'.'.$b1[2]);
        $pdf->Text(135, 56.5, $data['nation']);

        if (empty($data['job'])) {
            if ($i == 0) $this->pdfRect(42, 53.8, 36, $pdf, 3.0);
        } else {
            $pdf->Text(50, 56.4, $data['job']);
        }

        if(empty($data['phone'])) {
        if($i==0) $this->pdfRect(42, 60.7, 36, $pdf, 3.0);
        } else
            $pdf->Text(50, 63.2, $data['phone']);

        if(empty($data['email'])) {
        if($i==0) $this->pdfRect(126, 60.7, 70, $pdf, 3.0);
        } else
            $pdf->Text(135, 63.2, $data['email']);

        // Privatwirtschaft & öffentlicher Dienst verdecken
        if($i==0) {
        #    $this->pdfClean(40, 64.4, 30, $pdf, 242, 244, 250, 6.5);
        #} else {
        #    $this->pdfClean(40, 64.4, 30, $pdf, 246, 246, 246, 6.5);
        }

        // Berufsart markieren
        if($i==0) $this->pdfRect(41.0, 67.0, 2.7, $pdf, 6.0);
        if($i==0) $this->pdfRect(69.0, 67.0, 2.7, $pdf, 6.0);
        if($i==0) $this->pdfRect(102.2, 67.0, 2.7, $pdf, 6.0);
        if($i==0) $this->pdfRect(120.6, 70.0, 2.7, $pdf, 3.0);
        if($i==0) $this->pdfRect(137.9, 70.0, 2.7, $pdf, 3.0);
        if($i==0) $this->pdfRect(159.0, 67.0, 2.7, $pdf, 3.0);
        if($i==0) $this->pdfRect(176.9, 70.0, 2.7, $pdf, 3.0);


        // zu versichernde Person
        if($data['gender2']=='female') {
            $pdf->Text(41.4, 76.8, "x");
        } else {
            $pdf->Text(41.4, 80.1, "x");
        }

        $pdf->Text(65, 79.6, $data['name2b']);
        $pdf->Text(174.4, 79.6, $b2[0].'     '.$b2[1].'       '.$b2[2]);

        if(empty($data['job2'])) {
            $this->pdfRect(41, 84.1, 80, $pdf, 3.0);
        } else {
            $pdf->Text(65, 86.8, $data['job2']);
        }
        
        if($i==0) $this->pdfRect(186.3, 83.7, 2.7, $pdf, 2.7);
        if($i==0) $this->pdfRect(194.7, 83.7, 2.7, $pdf, 2.7);

        if($i==0) $this->pdfRect(159.9, 88.3, 2.7, $pdf, 2.7);
        if($i==0) $this->pdfRect(167.9, 88.3, 2.7, $pdf, 2.7);

        // Welcher Tarif?
        $pdfTemplate = isset($pdfTemplate)?$pdfTemplate:$data['idt'];
        switch($pdfTemplate) {
            case project::gti('axa-dent-premium'):
                $pdf->Text(41.3, 118.8, "x");
                $isAxaDentPremium = true;
                break;
            case project::gti('axa-dent-komfort'):
                $pdf->Text(82.3, 118.8, "x");

                break;
            case project::gti('axa-dent'):
		  $pdf->Text(41.3, 122.4, "x");
                break;
            case project::gti('axa-dent-smile-inlay'):
		  $pdf->Text(41.3, 122.4, "x");
                $pdf->Text(41.3, 125.9, "x");
                $pdf->Text(41.3, 129.5, "x");
                break;
            }


        $pdf->Text(99, 129.0, $data['price'] . ' EUR');
        $pdf->Text(47, 136.8, $data['price'] . ' EUR');

        // Versicherungsbeginn / zahlweise
        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);

	 $pdf->Text(42, 143.8, date("d.m.Y", strtotime($data['wishedBegin'])));

	 
        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        // Zahlweise markieren - monatliche zahlweise
        if($i==0) $this->pdfRect(139.6, 138.8, 2.7, $pdf, 2.7);
        if($i==0) $this->pdfRect(139.6, 142.2, 2.7, $pdf, 2.7);

        if($i==0) $this->pdfRect(166.0, 138.8, 2.7, $pdf, 2.7);
        if($i==0) $this->pdfRect(166.0, 142.2, 2.7, $pdf, 2.7);

        $pdf->Text(140.2, 141.2, "X");

        // SEPA
        if($i==0) {
            #$this->pdfClean(129, 147.8, 58, $pdf, 255, 255, 255, 3, 'F');
            #$this->pdfRect(129, 147.8, 58, $pdf, 3.0);
            $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
            $pdf->Text(45.6, 152.3, 'Bitte anhängendes SEPA-Lastschriftmandat ausfüllen  -> letzte Seite');
            $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
        }

        $pdf->Text(41.3, 156.7, "x");

        // Angaben zum Gesundheitszustand
        if($i==0) $this->pdfClean(14, 175.2, 191.2, $pdf, 37, 159, 37, 43.2, 'L');

        if($i==0) $this->pdfRect(129.6, 198.2, 17, $pdf, 3.0);

        if($i==0) $this->pdfRect(149.8, 198.3, 2.6, $pdf, 2.6);
        if($i==0) $this->pdfRect(158.2, 198.3, 2.6, $pdf, 2.6);
        if($i==0) $this->pdfRect(149.8, 209.2, 2.6, $pdf, 2.6);
        if($i==0) $this->pdfRect(158.2, 209.2, 2.6, $pdf, 2.6);
        if($i==0) $this->pdfRect(149.8, 214.6, 2.6, $pdf, 2.6);
        if($i==0) $this->pdfRect(158.2, 214.6, 2.6, $pdf, 2.6);

        // Frage 1
/*
        if($data['contractComplete']['tooth1'] || $data['contractComplete']['incare'] || $data['contractComplete']['biteSplint']) {

            if($data['contractComplete']['tooth1'] > 0) {
                $pdf->Text(150.3, 207.2, "x");
                $pdf->Text(140.6, 207.5, $data['contractComplete']['tooth1']);
            } else {    
                $pdf->Text(158.9, 207.2, "x");
            }

            // Frage 2
            if($data['contractComplete']['incare'] == 'yes' || $data['contractComplete']['biteSplint'] == 'yes') {
                $pdf->Text(150.3, 212.8, "x");
            } else {    
                $pdf->Text(158.9, 212.8, "x");
            }
        
            // Frage 3
            $pdf->Text(158.9, 218.3, "x");
        }
*/
        
        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);


        if($data['contractComplete']['biteSplint'] == 'yes') {
            $pdf->Text(109, 237.6, '*');
            $pdf->Text(44, 238, '1');
            $pdf->Text(54, 238, '2a.');
            $pdf->Text(64, 238, 'Aufbissschiene / Knirscherschiene');
            if ($i == 0) $this->pdfRect(117, 235.8, 12, $pdf, 3);
            if ($i == 0) $this->pdfRect(131.5, 235.2, 2.5, $pdf, 2.5);
            if ($i == 0) $this->pdfRect(138.6, 235.2, 2.5, $pdf, 2.5);
            if ($i == 0) $this->pdfRect(143, 235.8, 24, $pdf, 3);
            if ($i == 0) $this->pdfRect(170, 235.8, 33.5, $pdf, 3);

            #$this->pdfClean(40.7, 256.5, 163.5, $pdf, 255, 255, 255, 13.7, 'F');
            #$pdf->Text(42.0, 260.4, '* Leistungen für Aufbiss-/Kirscherschienen sind vom Versicherungsschutz ausgeschlossen.');
            #$pdf->Text(42.0, 264.4, 'Ich erkläre mich mit diesem Leistungsausschluss einverstanden.');
            #$pdf->Text(42.0, 270.0, 'Datum, Unterschrift __________________________________');

            // gelbe Markierung
            #if($i==0) $this->pdfRect(67, 266.6, 50.0, $pdf, 4.0);
            # $pdf->SetFont($pdfCfg['fontFamily'], 'B', 14, 0, 2);
            #if($i==0) $pdf->Text(68.5, 270.0, "x");
        }

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);


// SEITE 2
    $tplidx = $pdf->importPage(3, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);


// SEITE 3
    $tplidx = $pdf->importPage(4, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);

	//Datum
	$pdf->Text(41.0, 245.9, "x");
	if($i==0) $this->pdfRect(70, 246.2, 30.0, $pdf, 4.0);

	//Unterschriften
	if($i==0) $this->pdfRect(42, 262.6, 50.0, $pdf, 4.0);
	if($i==0) $this->pdfRect(42, 275.6, 50.0, $pdf, 4.0);

// SEITE 4
    $tplidx = $pdf->importPage(5, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 0, 209);


/*
        if ($data['contractComplete']['tooth1'] == 2 || $data['contractComplete']['tooth1'] == 3) {
            if($i==0) $this->pdfClean(2, 205, 203, $pdf, 48, 159, 48, 17, 'L');
        }

        // Unterschriften
        if($i==0) $this->pdfRect(30, 237.2, 85, $pdf, 5);
        if($i==0) $this->pdfRect(30, 248, 85, $pdf, 5);
        if($i==0) $this->pdfRect(30, 262, 85, $pdf, 5);

        //Hinzufügen von X
        if($i==0) $pdf->Text(31.2, 232.8, 'X');
        
        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 13, 0, 2);
        if($i==0) $pdf->Text(32, 240.8, 'X');
        if($i==0) $pdf->Text(32, 251.6, 'X');
        if($i==0) $pdf->Text(32, 265.7, 'X');

        // Wenn z.V.p unter 17 Jahre
        if(isset($data['personInsured']['birthdate']) && actionHelper::getAge($data['personInsured']['birthdate']) < 18) {
            if($i==0) $this->pdfRect(30, 274.8, 85, $pdf, 5);
            if($i==0) $pdf->Text(32, 278.7, 'X');
        }


        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-3, 0, 2);

        //Stempel
        // Maximilian Waizmann, Begonienstr 1 ...
        $pdf->Image($path.'/files/logo.png', 132, 246, 32.0);

        $pdf->Text(132, 256, "Versicherungsmakler Experten GmbH");
        $pdf->Text(132, 259, "Feursstr. 56 / RGB");
        $pdf->Text(132, 262, "82140 Olching");
        $pdf->Text(132, 265, "Tel.: 08142 - 651 39 28");
        $pdf->Text(132, 268, "info@zahnzusatzversicherung-experten.de");

        #if ($isAxaDentPremium) {
        #   $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+3, 0, 2);
        #   $pdf->Text(32, 290, '*');
        #   $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
        #   $pdf->Text(35.4, 290, 'Auf die Beitragsanpassung zum 1.1.2017 wurde ich hingewiesen!');
        #}

*/

// SEITE 5
    $tplidx = $pdf->importPage(6, '/MediaBox');
    
    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 1, 209);

// SEITE 
    $tplidx = $pdf->importPage(7, '/MediaBox');

    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 1, 209);


/*
*/


// SEITE 7
    $tplidx = $pdf->importPage(8, '/MediaBox');
    
    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 1, 209);

// SEITE 8
    $tplidx = $pdf->importPage(9, '/MediaBox');
    
    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 1, 209);

// SEITE 9
    $tplidx = $pdf->importPage(10, '/MediaBox');
    
    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 1, 209);

// SEITE 10
    $tplidx = $pdf->importPage(12, '/MediaBox');
    
    $pdf->addPage('P');
    $pdf->useTemplate($tplidx, 0, 1, 209);

    // set title and font to big and bold!
    $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);

    $this->pdfClean(69, 121.4, 12, $pdf, 255, 255, 255, 4, 'F');
    if($data['gender']=='female') {
        $pdf->Text(70, 125.4, 'Frau');
    } else {
        $pdf->Text(70, 125.4, 'Herr');
    }

    $this->pdfClean(69, 127.4, strlen($data['person']['forename']) * 2.5, $pdf, 255, 255, 255, 4, 'F');
    $this->pdfClean(69, 133.4, strlen($data['person']['surname']) * 2.5, $pdf, 255, 255, 255, 4, 'F');
    $pdf->Text(70, 130.7, $data['person']['forename']);
    $pdf->Text(70, 136.4, $data['person']['surname']);

    $this->pdfClean(93, 139.2, 10, $pdf, 255, 255, 255, 4, 'F');
    $pdf->Text(94, 142.4, (substr($data['person']['birthdate'], 0, 4)));
    $this->pdfClean(82, 139.2, 6, $pdf, 255, 255, 255, 4, 'F');
    $pdf->Text(82.8, 142.4, (substr($data['person']['birthdate'], 5, 2)));    
    $this->pdfClean(70, 139.2, 6, $pdf, 255, 255, 255, 4, 'F');
    $pdf->Text(70.5, 142.4, (substr($data['person']['birthdate'], 8, 2)));    
    $this->pdfClean(70, 145.2, strlen($data['street']) * 2.5, $pdf, 255, 255, 255, 4, 'F');
    $pdf->Text(70.5, 148.6, $data['street']);
    $this->pdfClean(70, 151.2, strlen($data['postcode']) * 2.5, $pdf, 255, 255, 255, 4, 'F');
    $pdf->Text(70.5, 154.6, $data['postcode']);     
    $this->pdfClean(90, 151.2, strlen($data['city']) * 2.5, $pdf, 255, 255, 255, 4, 'F');
    $pdf->Text(90.5, 154.6, $data['city']);     
    $this->pdfClean(70, 157, 23, $pdf, 255, 255, 255, 4, 'F');
    $pdf->Text(70.5, 160.4, 'Deutschland');       

    if($i==0) $this->pdfRect(69, 164.4, 80, $pdf, 3.0);   
    if($i==0) $this->pdfRect(69, 170, 80, $pdf, 3.0);
    if($i==0) $this->pdfRect(69, 175.8, 43, $pdf, 3.0);
    if($i==0) $this->pdfRect(69, 187.6, 62, $pdf, 3.0); 
    if($i==0) $this->pdfRect(69, 193.6, 30, $pdf, 3.0); 
    if($i==0) $this->pdfRect(69, 207.8, 80, $pdf, 3.0); 
    if($i==0) $this->pdfRect(69, 214.4, 80, $pdf, 3.0);       

    if($i==0) $pdf->Text(70, 216.9, 'X');
