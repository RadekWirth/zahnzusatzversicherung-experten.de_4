<?
		$fontSize = 8;
// File for generating Contract PDF for Barmenia
        if($i==0) {
            $pagecount = $pdf->setSourceFile($path.'/files/barmenia/antrag/contract_barmenia_antrag_2014.pdf');
        } else {
            $pagecount = $pdf->setSourceFile($path.'/files/barmenia/antrag/contract_barmenia_antrag_2014_bw.pdf');
        }
		$tplidx = $pdf->importPage(1, '/MediaBox');
		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);


		$this->pdfClean(50, 6.5, 95, $pdf, 255, 255, 255, 10.5);
	// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-2, 0, 2);

   		$this->getCompanyStamp(50, 9, $pdf, $data['contractComplete']['sourcePage'], 7.5, 'horizontal', 'wide');

	// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		// set title and font to big and bold!
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $fontSize+5, 0, 2);

		if($i==0) $this->pdfRect(50.0, 1, 68, $pdf);
		$pdf->Text(50.0, 5.0, $titles[$i]);

		// Vermittlernummer
		$this->pdfClean(167, 16.8, 20, $pdf, 255, 255, 255, 2.5, 'F');
		$pdf->SetFont($pdfCfg['fontFamily'], '', $fontSize-1, 0, 2);
		$pdf->Text(176, 9.7, '0010 / 3545');

		// reset font
		//$pdfCfg['fontSize'] = $fontSize;
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		// private Telefonnummer
		#if($i==0) $this->pdfRect(109.7, 41.5, 42, $pdf, 3.2);
		// Mobil Telefon
		#if($i==0) $this->pdfRect(109.7, 48.8, 42, $pdf, 3.2);



		// Neuantrag
		$pdf->Text(103.3, 27.9, 'X');

 		if($data['gender']=='male')	{
			$pdf->Text(12.7, 35.0, 'X');
		} else {
			$pdf->Text(20.2, 35.0, 'X');
		}

		$pdf->Text(27.0, 34.8, $data['name']);
 		$pdf->Text(13.5, 43.0, $data['street']);
 		$pdf->Text(13.5, 51.2, $data['postcode']);
 		$pdf->Text(37.5, 51.1, $data['city']);

		$pdf->Text(112.0, 34.8, $data['birthdate']);

		if(empty($data['phone'])) {
			if($i==0) $this->pdfRect(110.0, 40.4, 44, $pdf, 3.2);
		} else
			$pdf->Text(111.0, 43.0, $data['phone']);

		if(empty($data['mobile'])) {
			if($i==0) $this->pdfRect(110.0, 48.2, 44, $pdf, 3.2);
		} else
			$pdf->Text(111.0, 51.0, $data['mobile']);

 		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
		if(empty($data['email'])) {
			if($i==0) $this->pdfRect(159.0, 48.3, 44, $pdf, 3.2);
		} else
 			$pdf->Text(160.0, 51.0, $data['email']);

		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		if(empty($data['job']))
		{
			if($i==0) $this->pdfRect(13.0, 56.5, 44, $pdf, 3.2);
		} else
 		 	$pdf->Text(13.5, 59.5, $data['job']);


		//Berufsstellung
		if($i==0) $this->pdfRect(28.7, 62.9, 2.5, $pdf, 2.5);



		$pdf->Text(160.0, 34.8, $data['nation']);

		if($data['widowed']) {
			$pdf->Text(200.8, 34.8, 'X');
		}else if($data['married']) {
			$pdf->Text(187.3, 34.8, 'X');
		}else
			$pdf->Text(180.5, 34.8, 'X');


	// Zu versichernde Person
		if($i==0) $this->pdfRect(199.5, 76.0, 3, $pdf, 3);
		// child
		$pdf->Text(42.0, 78.5, $data['name2b']);
		if($data['gender2']=='male') {
			$pdf->Text(110.8, 78.6, 'X');
		}else
			$pdf->Text(118.5, 78.6, 'X');

		$pdf->Text(128.0, 78.5, $data['birthdate2']);

		if(empty($data['job2'])) {
			if($i==0) $this->pdfRect(154.5, 75.8, 40, $pdf, 3.2);
		} else
 			$pdf->Text(156.0, 78.5, $data['job2']);


	// Beantragte Versicherung
		// tariff
		#if($pdfTemplate==project::gti('barzg')) {
		#	$pdf->Text(10.0, 103.5, L::_(387));
		#}
		#elseif($pdfTemplate==project::gti('barzgplus')) {
		#	$pdf->Text(10.0, 103.5, L::_(388));
		#}

		$pdf->Text(76.5, 98.5, $data['begin']);
		$pdf->Text(122.0, 98.5, $data['price']);

	// Sonstige Krankenversicherungen

		$pdf->Text(13.5, 115.7, '1');
		$pdf->Text(23.8, 115.7, 'X');

		if(empty($data['insurance2'])) {
			if($i==0) $this->pdfRect(29.0, 112.9, 40, $pdf, 3.2);
		} else
 			$pdf->Text(30.0, 115.7, $data['insurance2']);

		if($data['insuredSince2']!="0000" && $data['insuredSince2']!="0001")
			$pdf->Text(73.0, 115.7, $data['insuredSince2']);
		if($data['insuredFamily2'] ||
			$data['insuredStrict2']) {
			$pdf->Text(102.7, 115.7, 'X'); // strict yes
		} else {
			$pdf->Text(97.0, 115.7, 'X'); // strict no
		}


	// Gesundheitsfragen an die zu versichernde Person
		if($i==0) $this->pdfRect(118.3, 139.8, 3.5, $pdf, 3.5);
		if($i==0) $this->pdfRect(130.2, 139.8, 3.5, $pdf, 3.5);
		if($i==0) $this->pdfRect(118.3, 148.9, 3.5, $pdf, 3.5);
		if($i==0) $this->pdfRect(130.2, 148.9, 3.5, $pdf, 3.5);
		if($i==0) $this->pdfRect(130.1, 153.7, 5.8, $pdf, 3.1);

		if($data['incare']) {
			$pdf->Text(131.0, 142.5, 'X');
		}else {
	if($data['contractComplete'])
			$pdf->Text(119.0, 142.5, 'X');
		}

		$pdf->Text(132.5, 156.2, $data['t1']);
		if($data['t1'] > 0) {
			$pdf->Text(131.0, 151.6, 'X');
		}else {
	if($data['contractComplete'])
			$pdf->Text(119.0, 151.6, 'X');
		}

	// Beitragsabruf
		//Bank / Sparkasse
		if($i==0) $this->pdfRect(20.9, 220.7, 82.5, $pdf, 3.2);
		//BIC
		if($i==0) $this->pdfRect(107.7, 220.7, 43.7, $pdf, 3.2);
		//Name des Kreditinstitutes
		if($i==0) $this->pdfRect(155.7, 220.7, 47.2, $pdf, 3.2);
		//Kontonummer
		if($i==0) $this->pdfRect(13.0, 229.9, 138, $pdf, 2.8);
		//Unterschrift d Kontoinhaber
		if($i==0) $this->pdfRect(156, 229.9, 46, $pdf, 2.8);
		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);
		if($i==0) $pdf->Text(156, 232.8, 'X');

		//Datum
		if($i==0) $this->pdfRect(13.0, 273.5, 21, $pdf, 3.2);
		//Antragsteller
		if($i==0) $this->pdfRect(39.5, 273.5, 46, $pdf, 3.2);
		if($i==0) $pdf->Text(40, 276.6, 'X');
		if ($data['personInsured']['forename'] != $data['person']['forename'] && $data['person']['forename'] != $data['person']['surname']) {
			//Zu versich. Person
			if($i==0) $this->pdfRect(92.5, 273.5, 46, $pdf, 3.2);	
			if($i==0) $pdf->Text(93, 276.6, 'X');				
		}
		if (actionHelper::getAge($data['person']['birthdate']) < 18) {
			//ges. Vertreter
			if($i==0) $this->pdfRect(144.9, 273.5, 46, $pdf, 3.2);
			if($i==0) $pdf->Text(145.4, 276.6, 'X');	
		}

		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

		$pdf->Text(49.0, 285.9, $data['namec']);
		$pdf->Text(110.0, 285.9, $data['signupDate']);

	// page 2
		$tplidx = $pdf->importPage(2, '/MediaBox');
		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);

		$pdf->Text(49.0, 9.5, $data['namec']);
		#$pdf->Text(110.0, 9.5, $data['signupDate']);

		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);
		//Datum
		if($i==0) $this->pdfRect(13.0, 41.2, 21, $pdf, 3.2);
		//Antragsteller
		if($i==0) $this->pdfRect(40.5, 41.2, 46, $pdf, 3.2);
		if($i==0) $pdf->Text(41, 44.2, 'X');
		if ($data['personInsured']['forename'] != $data['person']['forename'] && $data['person']['forename'] != $data['person']['surname']) {
			//Zu versich. Person
			if($i==0) $this->pdfRect(95.5, 41.2, 48, $pdf, 3.2);	
			if($i==0) $pdf->Text(96, 44.2, 'X');				
		}
		if (actionHelper::getAge($data['person']['birthdate']) < 18) {
			//ges. Vertreter
			if($i==0) $this->pdfRect(150.5, 41.2, 45, $pdf, 3.2);
			if($i==0) $pdf->Text(151.4, 44.2, 'X');	
		}
		// reset font
		$pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

	// page 3
		$tplidx = $pdf->importPage(3, '/MediaBox');
		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);

		$pdf->Text(49.0, 9.5, $data['namec']);
		#$pdf->Text(110.0, 9.5, $data['signupDate']);

	// page 4
		$tplidx = $pdf->importPage(4, '/MediaBox');
		$pdf->addPage('P');
		$pdf->useTemplate($tplidx, 0, 0.1, 209.9);

		$pdf->Text(49.0, 9.5, $data['namec']);
		#$pdf->Text(110.0, 9.5, $data['signupDate']);


	// page 5
		$pdf->addPage('P');
		$pdf->Image($path.'/files/contract-barmenia-page3.png', 15, null, 160);

		// Produktinformationsblatt
		$pdf->Text(68.5, 114.5, '+ Antrag');

		// Allgemeine Kundeninformationen
		$pdf->Text(70.5, 123.0, '(K6045)');

		// gew�hlter Tarif
		$pdf->Text(28.5, 133.0, 'Tarif ZGU+ (K4639)');

		// clean
		$this->pdfClean(27.0, 137.0, 136, $pdf, 255,255,255, 7.2);
		$this->pdfClean(27.0, 145.5, 138, $pdf, 255,255,255, 7);

		$pdf->Text(28.5, 140.0, 'Allg. Versicherungsbedingungen f�r die Krankenheitskosten- und Krankenhaustagegeld-');
		$pdf->Text(28.5, 143.5, 'versicherung MB/KK09 und TB/KK13 (K4601)');
		$pdf->Text(28.5, 148.5, 'Merkblatt zur Datenverarbeitung (B 3997, Ausgabe 04/2011)');

		//Datum
		if($i==0) $this->pdfRect(25.0, 214.5, 32, $pdf, 6.7);
		//Unterschrift
		if($i==0) $this->pdfRect(67.0, 214.5, 54, $pdf, 6.7);

		$pdf->SetFont($pdfCfg['fontFamily'], 'B', $fontSize+1, 0, 2);
		$pdf->Text(45.0, 67.0, $data['name']);
 		$pdf->Text(45.0, 73.0, $data['street']);
 		$pdf->Text(45.0, 79.2, $data['postcode'].' '.$data['city']);


	if($complete==2) { $pdf->AddPage('P'); }

?>