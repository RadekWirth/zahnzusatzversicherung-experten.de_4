<?
// File for generating Contract PDF for Hanse Merkur

    if($i==0) {
        $pagecount = $pdf->setSourceFile($path.'/files/hanse-merkur/antrag/2018/HanseMerkur.EZK.EZL.Zahntarife.Antrag.04-2018.pdf');
    } else {
        $pagecount = $pdf->setSourceFile($path.'files/hanse-merkur/antrag/2018/HanseMerkur.EZK.EZL.Zahntarife.Antrag.04-2018_bw.pdf');
    }
        $tplidx = $pdf->importPage(1, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
        $pdf->SetMargins(0, 0, 0);

        if($i==0) $this->pdfRect(74, 0, 68, $pdf);
        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);

        $pdf->Text(74.0, 4.0, $titles[$i]);

        $my = explode(".", $data['begin']);
        // monat
        $pdf->Text(169.6, 23.0, substr($my[0],0,1));
        $pdf->Text(174.6, 23.0, substr($my[0],1,1));
        // Jahr
        $pdf->Text(190.1, 23.0, substr($my[1], 2,1));
        $pdf->Text(195.5, 23.0, substr($my[1], 3,1));
        unset($my);

    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);
        //$pdf->Text(171.0, 8.0, "PP-Nr. 14754");
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

        // Makler Daten schreiben

        $pdf->Text(82.0, 35.1, "3     2     1    9     4    0    9");

        $this->getCompanyStamp(72.0, 9.5, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize']-2.5, 'horizontal', 'wide');

    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

        if($data['gender']=='male') {
            $pdf->Text(25.5, 85.5, 'X');
        } else {
            $pdf->Text(25.5, 89.5, 'X');
        }

        $pdf->Text(63.0, 71, $data['person']['surname']);
        $pdf->Text(139.0, 71, $data['person']['forename']);

        $pdf->Text(63.0, 76.6, $data['street']);
        $pdf->Text(173.0, 76.6, $data['birthdate']);
        $pdf->Text(63.0, 81.9, $data['postcode'].' '.$data['city']);

        if(empty($data['phone']))
        {
            if($i==0) $this->pdfRect(60.7, 90.0, 50, $pdf, 4.0);
        } else {
            $pdf->Text(63.0, 92.6, $data['phone']);
        }

        if(empty($data['email']))
        {
            if($i==0) $this->pdfRect(61, 95.8, 50.0, $pdf, 4.0);
        } else {
            $pdf->Text(63.0, 98.5, $data['email']);
        }


        if(empty($data['nation'])) {
            if($i==0) $this->pdfRect(61, 89.2, 50.0, $pdf, 4.0);
        } elseif($data['nation']=='DEU') {
            $pdf->Text(63, 87, 'deutsch');
        } else {
            $pdf->Text(63, 87, $data['nation']);
        }
/*
    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);


        switch($data['person']['familyStatusLid']) {
            case 37:
                // ledig
                $pdf->Text(60, 87, 'X');
            break;

            case 38:
                // verheiratet
                $pdf->Text(74.5, 87, 'X');
            break;

            case 40:
                // geschieden
                $pdf->Text(164.7, 87, 'X');
            break;

            case 39:
                // verwitwet
                $pdf->Text(185.5, 87, 'X');
            break;
        }
*/

    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

    //Beitragszahlung
        // IBAN
	 if($i==0) $this->pdfRect(60.4, 118.4, 89.8, $pdf, 4.0);
/*
        $x = 66.4;
        for($c=0; $c<2; $c++) {
            if($i==0) $this->pdfRect($x += 3.8, 112.3, 3.4, $pdf, 3.2);
        }
*/
/*
        // BLZ
        $x = 80;
        for($c=0; $c<4; $c++) {
            if($i==0) $this->pdfRect($x += 3.8, 112.3, 3.4, $pdf, 3.2);
        }
        $x += 0.7;
        for($c=0; $c<4; $c++) {
            if($i==0) $this->pdfRect($x += 3.8, 112.3, 3.4, $pdf, 3.2);
        }
*/
/*
        // Kontonummer
        $x += 8.9;
        for($c=0; $c<4; $c++) {
            if($i==0) $this->pdfRect($x += 3.8, 112.3, 3.4, $pdf, 3.2);
        }
        $x += 0.7;
        for($c=0; $c<4; $c++) {
            if($i==0) $this->pdfRect($x += 3.8, 112.3, 3.4, $pdf, 3.2);
        }
        $x += 0.7;
        for($c=0; $c<2; $c++) {
            if($i==0) $this->pdfRect($x += 3.8, 112.3, 3.4, $pdf, 3.2);
        }
*/
        // DATUM
        if($i==0) $this->pdfRect(61, 129.7, 26, $pdf, 4.0);

        //Unterschrift des Kontoinhabers
        if($i==0) $this->pdfRect(117, 129.7, 83, $pdf, 4.0);

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+1, 0, 2);
        $pdf->Text(118, 133.1, 'X');
        $pdf->Text(61, 133.1, 'X');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

    //Zahlungsweise
        //monatlich
        if($i==0) $this->pdfRect(60.1, 139.6, 2, $pdf, 2);

        //vierteljährlich
        if($i==0) $this->pdfRect(81.4, 139.6, 2, $pdf, 2);

        //halbjährlich
        if($i==0) $this->pdfRect(103.4, 139.6, 2, $pdf, 2);

        //jährlich
        if($i==0) $this->pdfRect(125.4, 139.6, 2, $pdf, 2);

        //firmenabrechnung
        if($i==0) $this->pdfRect(147.4, 139.6, 2, $pdf, 2);

    //Zu versichernde Person 1
        // person insured
        if(isset($data['personInsured']))
        {
            // Uebernahme antragsteller
            $pdf->Text(37.0, 156.6, $data['name2']);
            $pdf->Text(37.0, 164.6, $data['birthdate2']);

            #$pdf->Text(37.0, 143.5, $data['name2']);
            #$pdf->Text(37.0, 151.6, $data['birthdate2']);

            // Bei Auszubildenden
            //if($i==0) $this->pdfRect(34, 165.5, 80, $pdf, 4.0);

    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
            if($data['gender2']=='male')    {
                $pdf->Text(107.5, 154.9, 'X');
            } else {
                $pdf->Text(107.5, 157.4, 'X');
            }
    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

            if($data['nation2']=='DEU') {
                $pdf->Text(64.0, 164.6, 'deutsch');
            } else {
                $pdf->Text(64.0, 164.6, $data['nation2']);
            }

            if ($data['personInsured']['insurance']) {
                $pdf->Text(37, 173.8, $data['personInsured']['insurance']);
            }

        } // Ende Person1
        else {
            // Uebernahme antragsteller
            $pdf->Text(37.0, 143.5, $data['nameb']);
            $pdf->Text(37.0, 151.6, $data['birthdate']);

            // Bei Auszubildenden
            //if($i==0) $this->pdfRect(34, 165.5, 80, $pdf, 4.0);

    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
            if($data['gender']=='male') {
                $pdf->Text(107.5, 141.2, 'X');
            }
            else {
                $pdf->Text(107.5, 143.7, 'X');
            }
    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);

            if($data['nation']=='DEU') {
                $pdf->Text(64.0, 151.6, 'deutsch');
            } else {
                $pdf->Text(64.0, 151.6, $data['nation2']);
            }

            if ($data['personInsured']['insurance']) {
                $pdf->Text(37, 160.8, $data['personInsured']['insurance']);
            }

        }

        switch($pdfTemplate)
	 {
		case project::gti('hansemerkur-ezk'):

			// EZK
	              $pdf->Text(51.7, 207.8, 'X');
		        // Betrag
		        $pdf->Text(64.3, 207.6, $data['price']);
		break;
		case project::gti('hansemerkur-ezl'):


			// EZL
	              $pdf->Text(51.7, 222.2, 'X');
		        // Betrag
		        $pdf->Text(64.3, 221.7, $data['price']);
		break;
	 }





    // page 2
        $tplidx = $pdf->importPage(2, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        // Angaben zum Zahnstatus
        // erst markierungen

	// Frage 1
        if($i==0) $this->pdfRect(148.5, 40.0 , 2, $pdf, 2);
        if($i==0) $this->pdfRect(148.5, 43.4 , 2, $pdf, 2);
	
	// Frage 2
        if($i==0) $this->pdfRect(148.4, 56.8 , 2, $pdf, 2);
        if($i==0) $this->pdfRect(162.5, 56.8 , 2, $pdf, 2);

	// Frage 3
        if($i==0) $this->pdfRect(148.4, 71.5 , 2, $pdf, 2);
        if($i==0) $this->pdfRect(162.5, 71.5 , 2, $pdf, 2);

	// Frage 4
        if($i==0) $this->pdfRect(148.4, 86.6 , 2, $pdf, 2);
        if($i==0) $this->pdfRect(162.5, 86.6 , 2, $pdf, 2);
/*
if(!$reset) {
        if($data['t1'] > 0)
        {
            $pdf->Text(148.5, 38.9, 'X');
            $pdf->Text(164.0, 38.6, $data['contractComplete']['tooth1']);
        } else {
            $pdf->Text(148.5, 42.6, 'X');
        }

        // incare?
        if($data['incare'])
        {
            $pdf->Text(148.5, 55, 'X');
            $pdf->Text(148.5, 69, 'X');
        } else  {
            $pdf->Text(164.6, 55, 'X');
            $pdf->Text(164.6, 69, 'X');
        }
    }
*/
/*
        // Original-Datum verdecken
        if ($i == 0) $this->pdfClean(165.3, 83.3, 12, $pdf, 217, 238, 230, 2, 'F');

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', 6, 0, 2);
        $pdf->Text(165, 85, 'Januar 2016');
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);
*/

        // Zahn Ergaenzung erhalten
        $pdf->Text(50, 112.5, $data['signupDate']);
        $pdf->Text(128.0, 117.5, 'X');
        $pdf->Text(153, 118.0, 'KK 390 01.18');



        // Unterschrift

    // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+1, 0, 2);
        if($i==0) $this->pdfRect(130, 130.9, 64, $pdf, 4.0);
        if($i==0) $this->pdfRect(15.0, 166.2, 32, $pdf, 4.0);
        if($i==0) $this->pdfRect(50.0, 166.2, 45, $pdf, 4.0);
        if($i==0) $this->pdfRect(99.0, 166.2, 45, $pdf, 4.0);

        // Makler Daten schreiben
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-1, 0, 2);
        $pdf->Text(61.3, 215.5, "X");
        $pdf->Text(168, 219.5, "3 2 1 9 4 0 9");


    // page 3
        $tplidx = $pdf->importPage(3, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

    // page 4
        $tplidx = $pdf->importPage(4, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

?>
