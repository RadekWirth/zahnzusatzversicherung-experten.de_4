<?php
// File for generating Contract PDF for Signal Iduna Kompakt (Start & Plus & Top)

        /*
         * Constants
         */
        // SQUARE XY
        define('X_Y', 1.5);

        // RECT Y
        define('Y', 2.7);

        $data['person']['age'] = actionHelper::getAge($data['contractComplete']['birthdate']);

        //$pagecount = $pdf->setSourceFile($path.'/files/contract-sigidu.pdf');

        if ($i!=1) {
            $pagecount = $pdf->setSourceFile($path.'files/signal-iduna/antrag/2016/Signal.Antrag.GE.GePlus.GeTop.Z50-3.01-2016.pdf');
        } else {
            $pagecount = $pdf->setSourceFile($path.'files/signal-iduna/antrag/2016/Signal.Antrag.GE.GePlus.GeTop.Z50-3.01-2016-bw.pdf');
        }

        $tplidx = $pdf->importPage(1, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);
        $pdf->SetMargins(0, 0, 0);

        // set title and font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+4, 0, 2);

        if($i==0) $this->pdfRect(75.0, 3.0, 70, $pdf);
        $pdf->Text(75.0, 7.0, $titles[$i]);

        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-1, 0, 2);

        // -> Seite 2
        $pdf->Text(75.0, 14.0, "Versicherungsmakler Experten GmbH");

        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-3, 0, 2);

        //mark section
        //Selbstst�ndiger/Firma/Verein
        
        if($i==0) {
            $this->pdfRect(114.5, 57.2, X_Y, $pdf, X_Y);
            $this->pdfRect(133.0, 57.2, X_Y, $pdf, X_Y);
            $this->pdfRect(148.6, 57.2, X_Y, $pdf, X_Y);
            $this->pdfRect(160.9, 57.2, X_Y, $pdf, X_Y);
            $this->pdfRect(114.5, 60.2, X_Y, $pdf, X_Y);
        }
        

        //freiwillig versichert?
        if($i==0) {
            $this->pdfRect(131.7, 70.1, X_Y, $pdf, X_Y);
            $this->pdfRect(150.6, 70.1, X_Y, $pdf, X_Y);
            $this->pdfRect(150.6, 72.9, X_Y, $pdf, X_Y);
            $this->pdfRect(162.6, 72.9, X_Y, $pdf, X_Y);
        }

        //versichert seit...
        //if($i==0) $this->pdfRect(162, 71.2, 27.5, $pdf, Y);


        //mark section end
        $pdf->Text(182.3, 58.4, 'X'); // Beihilfe
        $pdf->Text(186.7, 61.6, 'X'); // Heilfuersorge

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);    

        if($data['gender']=='male') {
            $pdf->Text(16.7, 45.3, 'X');
        } else {
            $pdf->Text(16.7, 47.9, 'X');
        }

        if($data['widowed']) {
            $pdf->Text(16.7, 62.6, 'X');
        } else if($data['married']) {
            $pdf->Text(16.7, 59.5, 'X');
        } else {
            $pdf->Text(16.7, 56.4, 'X');
        }

        // Bereits Kunde?
        $pdf->Text(146.0, 37.6, 'X');
        
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
        $pdf->Text(29.0, 48.0, $data['name']);
        $pdf->Text(143.8, 48.0, $data['birthdate']);

        if($data['nation']=='DEU') {
            $pdf->Text(168, 48.0, 'deutsch');
        } else {
            $pdf->Text(168, 48.0, $data['nation']);
        }

        $pdf->Text(29.0, 55.0, $data['street'].
            ', '.$data['postcode'].' '.$data['city']);

        if(isset($data['job'])) {
            $pdf->Text(29.0, 61.2, $data['job']);
        } else {
           if($i==0) 
                $this->pdfRect(28.5, 58.0, 30, $pdf, 4);
        }

        //Telefonnummer
        if(!empty($data['phone']) || $data['phone']=='')
        {
            if($i==0) $this->pdfRect(15.8, 78, 39, $pdf, 3);
        } else
            $pdf->Text(16.5, 80.4, $data['phone']);

        //eMail
        if(!empty($data['email']) || $data['email']=='')
        {
            if($i==0) $this->pdfRect(102, 78, 53, $pdf, 3);
        }
        else 
            $pdf->Text(105.0, 80.4, $data['email']);

        $pdf->Text(162, 80.4, $data['mobile']);

        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);    


        if(isset($data['insurance'])) {
            $pdf->Text(29.0, 74.5, $data['insurance']);
        } else {
            if($i==0) $this->pdfRect(28.5, 71.5, 62, $pdf, 3.5);
        }

        // Insured since
        if($i==0) $this->pdfRect(180, 71.8, 20, $pdf, 3.5);



    // 3 Beitragszahlung

        //mark section

        //Zahlungsweise
        if($i==0) {
            $this->pdfRect(36.0, 108.1, X_Y, $pdf, X_Y);
            $this->pdfRect(50.3, 108.1, X_Y, $pdf, X_Y);
            $this->pdfRect(66.7, 108.1, X_Y, $pdf, X_Y);
            $this->pdfRect(83.6, 108.1, X_Y, $pdf, X_Y);
        }

        //Konto
        if($i==0) {
            $this->pdfRect(17.5, 139.4, 100, $pdf, Y);
            $this->pdfRect(156.0, 139.4, 38, $pdf, Y);
            $this->pdfRect(17.5, 147.7, 89, $pdf, Y);
            $this->pdfRect(117.0, 147.7, 24, $pdf, Y);
            $this->pdfRect(149.5, 147.7, 47, $pdf, Y);
        }
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
        if($i==0) $pdf->Text(149, 150, 'X');


// 2 Zu versichernde Personen
        

        // check for child
        if($data['name2b'] != $data['name']) {
            // setze marker bei Antragsteller / VN
            $pdf->Text(109.6, 41.2, 'X');            
            $data['personInsured']['age'] = actionHelper::getAge($data['personInsured']['birthdate']);

        //mark section
        //Selbstst�ndiger/Firma/Verein

            if($i==0) {
                $this->pdfRect(114.5, 180.2, X_Y, $pdf, X_Y);
                $this->pdfRect(133.0, 180.2, X_Y, $pdf, X_Y);
                $this->pdfRect(148.6, 180.2, X_Y, $pdf, X_Y);
                $this->pdfRect(160.9, 180.2, X_Y, $pdf, X_Y);
                $this->pdfRect(114.5, 183.1, X_Y, $pdf, X_Y);
            }
            

            //freiwillig versichert?
            if($i==0) {
                $this->pdfRect(131.9, 193.3, X_Y, $pdf, X_Y);
                $this->pdfRect(150.6, 193.3, X_Y, $pdf, X_Y);
                $this->pdfRect(150.6, 196.4, X_Y, $pdf, X_Y);
                $this->pdfRect(162.7, 196.4, X_Y, $pdf, X_Y);
            }
                        
                //mark section end


                
                $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);

                $pdf->Text(29.0, 172.0, $data['name2b']);
                $pdf->Text(29.0, 177.0, $data['street'].', '.$data['postcode'].' '.$data['city']);
                $pdf->Text(148.0, 172.0, $data['birthdate2']);
                if($data['gender2']=='male')    {
                    $pdf->Text(16.7, 171.9, 'X');
                }
                else {
                    $pdf->Text(16.7, 174.5, 'X');
                }

                if($data['widowed2']) {
                    $pdf->Text(16.7, 189.0, 'X');
                }
                else if($data['married2']) {
                    $pdf->Text(16.7, 185.9, 'X');
                }
                else {
                    $pdf->Text(16.7, 182.8, 'X');
                }

                $pdf->Text(182.4, 182.0, 'X'); // Beihilfe
                $pdf->Text(186.7, 185.1, 'X'); // Heilfuersorge
                
                if($data['nation2']=='DEU') {
                    $pdf->Text(169, 172.0, 'deutsch');
                } else {
                    $pdf->Text(169.0, 172.0, $data['nation2']);
                }

                if(isset($data['job'])) {
                    $pdf->Text(29.0, 184.5, $data['job2']);
                } else {
                    if($i==0) $this->pdfRect(28.5, 182.0, 80, $pdf, 3);
                }

                if(isset($data['insurance'])) {
                    $pdf->Text(29.0, 197.5, $data['insurance2']);
                } else {
                    if($i==0) $this->pdfRect(28.5, 194.7, 80, $pdf, 3);
                }
                // Insured since
                if($i==0) $this->pdfRect(180, 194.7, 20, $pdf, 3.5);

                #if($i==0) $this->pdfRect(163, 186.0, 26, $pdf, 3);
                $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        }
        // end child
        else {
            // setze marker bei Antragsteller / VN
            $pdf->Text(70.1, 41.2, 'X');
        }



    // 4 Versicherungsbeginn und beantragter Versicherungsschutz
        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);


        if($data['name2b'] == $data['name']) {
            $this->pdfClean(49.3, 279.8, 10, $pdf, 255, 255, 255, 4.0);
            $pdf->Text(50.4, 282.9, $data['price']);
        }
        else {
            $this->pdfClean(99.9, 279.8, 10, $pdf, 255, 255, 255, 4.0);
            $pdf->Text(101.0, 282.9, $data['price']);
        }
        $this->pdfClean(177.8, 279.8, 10, $pdf, 255, 255, 255, 4.0);
        $pdf->Text(178.8, 282.9, $data['price']);

        $pdf->Text(170, 243.0, '01.'. $data['begin']);


        // choose tariff
        if($pdfTemplate == project::gti('sigidu-komplus')) {
            if($data['name2b'] == $data['name']) {
                $pdf->Text(18.7, 261.9, 'X');
                $pdf->Text(18.7, 266.8, 'X');
                $pdf->Text(18.7, 276.9, 'X');
            } else {
                $pdf->Text(68.6, 261.9, 'X');
                $pdf->Text(68.6, 266.8, 'X');
                $pdf->Text(68.6, 276.9, 'X');
            }
        }
        else if($pdfTemplate == project::gti('sigidu-komstart')){
            if($data['name2b'] == $data['name'])
            {
                $pdf->Text(18.7, 261.9, 'X');
                $pdf->Text(18.7, 276.9, 'X');
            } else {
                $pdf->Text(68.6, 261.9, 'X');
                $pdf->Text(68.6, 276.9, 'X');
            }
        }
        else if($pdfTemplate == project::gti('sigidu-komtop')){
            if($data['name2b'] == $data['name'])
            {
                $pdf->Text(18.7, 261.9, 'X');
                $pdf->Text(18.7, 271.9, 'X');
                $pdf->Text(18.7, 276.9, 'X');
            } else {
                $pdf->Text(68.6, 261.9, 'X');
                $pdf->Text(68.6, 271.9, 'X');
                $pdf->Text(68.6, 276.9, 'X');
            }
        }


// SEITE 2
        $tplidx = $pdf->importPage(2, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);


        $this->getCompanyStamp(100.0, 5, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize'], 'horizontal', 'wide');

        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);

        $pdf->Text(33.0, 7.1, $data['name']);


        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+20, 0, 2);
        $pdf->SetTextColor(80,0,0);

           $pdf->Text(200.6, 56.4, '!');

        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize'], 0, 2);
        $pdf->SetTextColor(0,0,0);


        // Fragen an die zu versichernde Person
        
        //ja / nein
        if($data['name2b'] != $data['name'])
        {  //mark section
            if($i==0) {
                    $this->pdfRect(171.6, 46.6, X_Y, $pdf, X_Y);
                    $this->pdfRect(179.9, 46.6, X_Y, $pdf, X_Y);

                    $this->pdfRect(171.6, 49.4, X_Y, $pdf, X_Y);
                    $this->pdfRect(179.9, 49.4, X_Y, $pdf, X_Y);

                    $this->pdfRect(171.6, 58.2, X_Y, $pdf, X_Y);
                    $this->pdfRect(179.8, 58.2, X_Y, $pdf, X_Y);
                }
            } 
            else {
                //mark section
                if($i==0) {
                    $this->pdfRect(156.6, 46.5, X_Y, $pdf, X_Y);
                    $this->pdfRect(164.7, 46.5, X_Y, $pdf, X_Y);

                    $this->pdfRect(156.6, 49.3, X_Y, $pdf, X_Y);
                    $this->pdfRect(164.7, 49.3, X_Y, $pdf, X_Y);

                    $this->pdfRect(156.6, 58.2, X_Y, $pdf, X_Y);
                    $this->pdfRect(164.7, 58.2, X_Y, $pdf, X_Y);
                }
        }
        

// SEITE 3
        $tplidx = $pdf->importPage(3, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

        $this->getCompanyStamp(100.0, 5, $pdf, $data['contractComplete']['sourcePage'], $pdfCfg['fontSize'], 'horizontal', 'wide');

        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);

        $pdf->Text(33, 5.5, $data['name']);

        // Unterschriften
        
        // Antragsteller
        if($i==0) $this->pdfRect(18.5, 100.7, 28.0, $pdf, Y);
        if($i==0) $this->pdfRect(52.0, 100.7, 44.0, $pdf, Y);
        $pdf->Text(52, 103.4, 'X');

        // Unterschrift der mitzuversichernden Person ab 16 Jahre
        if(isset($data['personInsured']['age']))
        {
            if($data['personInsured']['age'] >= 16)
            {
                if($i==0) $this->pdfRect(103.5, 100.7, 44.0, $pdf, Y);
                $pdf->Text(105, 103.4, 'X');
            }
        }

        // Antragsteller minderj�hrig oder versicherte Person minderj�hrig
        if($data['person']['age'] <= 18 || ($data['personInsured']['age'] <=18) && (isset($data['personInsured']['age'])))
        {
            if($i==0) $this->pdfRect(152, 100.7, 44.0, $pdf, Y);
            $pdf->Text(153, 103.4, 'X');

            if($data['person']['age'] <= 18)
            {
                $this->pdfRect(113.0, 125.5, 82.0, $pdf, 3.2);
                $pdf->Text(115.0, 128.2, 'X');
            }
        }

        // Empfangsbest�tigung
        if($i==0) {
            $this->pdfRect(17.5, 125.5, 82.0, $pdf, 3.2);
            $pdf->Text(18.5, 128.2, 'X');
        }

        // Reset
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
        // Makler
        $pdf->Text(140.5, 139.5, 'X');
        // set font to big and bold!
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize'], 0, 2);
        // Vmtl-Nr
        $pdf->Text(18.5, 145.7, '226 / 1500');


// SEITE 4
        $tplidx = $pdf->importPage(4, '/MediaBox');

        $pdf->addPage('P');
        $pdf->useTemplate($tplidx, 0, 0.1, 209.9);

/*
        $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']-2, 0, 2);

        // Unterschriften
        
        // Antragsteller
        if($i==0) $this->pdfRect(17.5, 188, 28.0, $pdf, Y);
        if($i==0) $this->pdfRect(49.0, 188, 44.0, $pdf, Y);
        if($i==0) $pdf->Text(49.0, 190.5, 'X');

        // Unterschrift der mitzuversichernden Person ab 16 Jahre
        if(isset($data['personInsured']['age']))
        {
            if($data['personInsured']['age'] >= 16)
            {
                if($i==0) $this->pdfRect(96.5, 188, 44.0, $pdf, Y);
                $pdf->Text(97, 190.5, 'X');
            }
        }

        // Antragsteller minderj�hrig oder versicherte Person minderj�hrig
        if($data['person']['age'] <= 18 || ($data['personInsured']['age'] <=18) && (isset($data['personInsured']['age'])))
        {
            if($i==0) $this->pdfRect(144, 188, 44.0, $pdf, Y);
            $pdf->Text(144, 190.5, 'X');
        }
*/

// SEITE 5
        if($data['contractComplete']['reha'] == 'yes')
        {
            $pdf->setSourceFile($path.'/files/1200802_kurtagegeld.pdf');
            $tplidx = $pdf->importPage(1, '/MediaBox');

            $pdf->addPage('P');
            $pdf->useTemplate($tplidx, 0, 10, 200);

            $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']+2, 0, 2);
            $pdf->Text(20, 69, $data['signupDate']);
            $pdf->Text(123, 69, $data['namec']);
            if($data['name2b'] != $data['name']) {
                $pdf->Text(20, 88, $data['name2']);
                $pdf->Text(123, 88, $data['birthdate2']);
            }
            else {
                $pdf->Text(20, 88, $data['namec']);
                $pdf->Text(123, 88, $data['birthdate']);
            }

            if($i==0) $this->pdfRect(16.5, 262, 38, $pdf, 6);
            if($i==0) $this->pdfRect(59.2, 262, 110, $pdf, 6);

            $pdf->SetFont($pdfCfg['fontFamily'], 'B', $pdfCfg['fontSize']+2, 0, 2);
            $pdf->Text(18, 267, 'X');
            $pdf->Text(61, 267, 'X');


         $pdf->addPage('P');
        }

/*
        $pdf->Text(15, 160, 'Hinweis :');
        // reset font
        $pdf->SetFont($pdfCfg['fontFamily'], '', $pdfCfg['fontSize']-2, 0, 2);
        if($data['name2b'] != $data['name']) {
            $pdf->Text(15, 164, 'Anzahl ben�tigter Unterschriften bei Versicherung von Kindern: 6 St�ck (2 x links Vorderseite, 2 x rechts Vorderseite,');
            $pdf->Text(15, 167, '1 x links auf der R�ckseite, 1 x rechts auf der R�ckseite = 6 Unterschriften!!!)');
        } else {
            $pdf->Text(15, 164, 'Anzahl ben�tigter Unterschriften bei Versicherung Erwachsener: 3 St�ck (2 x links Vorderseite, 1 x links auf der R�ckseite = 3 Unterschriften)');
        }
*/

?>