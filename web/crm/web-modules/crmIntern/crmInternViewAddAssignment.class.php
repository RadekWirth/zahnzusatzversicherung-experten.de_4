<?php

class crmInternViewAddAssignment
	extends myInternView
{

function __construct($dataArray=null) {
	parent::__construct();
	$this->dataArray = $dataArray;
}

function processData() {
// get data for prefilling fields
$data = (empty($this->dataArray['data']))?array():$this->dataArray['data'];

$label = new label();
$crm = new crm();

$type = $this->dataArray['type'];
$id = $this->dataArray['id'];

$amid = null;
if($amid = $this->dataArray['amid']) {
	$data = $crm->getAssignment($amid);
}

$replCode = '';
$possibilites = '';

$replCode .= $this->geth1(L::_(103));

// generate options for assignmentStatus
$assignmentStatusRs = $label->getLabels('assignmentStatus', 'lid');
$assignmentStatusOptions = array(0 => L::_(25));
while($row = $assignmentStatusRs->fetch()) {
	$assignmentStatusOptions[$row['lid']] = $row['name'];
}

if(empty($data['tax'])) {
	$data['tax'] = 19;
}

if(empty($data['period']['period'])) {
	$data['period'] = 0;
}

// create field sets
$fieldsets =
array(
	array(
		'legend' => L::_(104),
		'group' => 'assignment',
		'fields' => array(
			array(
			'label' => L::_(105),
			'name' => 'caption',
			'type' => 'text',
			'size' => 'big',
			'maxlength' => 255,
			'value' => $data['caption'],
			'orientation' => 'vertical'
			),
			array(
			'label' => L::_(106),
			'name' => 'description',
			'type' => 'textarea',
			'size' => 'big',
			'value' => $data['description'],
			'orientation' => 'vertical'
			),
			array(
			'label' => L::_(122),
			'name' => 'price',
			'type' => 'text',
			'size' => 'medium',
			'maxlength' => 255,
			'value' => $data['price'],
			),
			array(
			'label' => L::_(123),
			'name' => 'tax',
			'type' => 'text',
			'size' => 'small',
			'maxlength' => 2,
			'value' => $data['tax'],
			),
			array(
			'label' => L::_(129),
			'name' => 'assignmentStatusLid',
			'type' => 'select',
			'size' => 'big',
			'options' => $assignmentStatusOptions,
			'selected' => $data['assignmentStatusLid']
			)
		)
	),
	array(
		'legend' => L::_(128),
		'group' => 'assignment',
		'fields' => array(
			array(
			'label' => L::_(125),
 			'name' => 'period',
			'type' => 'text',
			'size' => 'small',
			'maxlength' => 5,
			'value' => $data['period'],
			),
			array(
			'name' => 'periodBillAhead',
			'type' => 'checkbox',
			'options' => array('1' => L::_(130)),
			'class' => 'checkbox',
			'params' => 'hideLabel',
			'selected' => $data['periodBillAhead']
			),
			array(
			'label' => L::_(120),
			'name' => 'periodStartDate',
			'type' => 'date',
			'selected' => stringHelper::dateToArray($data['periodStartDate'])
			),
			array(
			'label' => L::_(121),
			'name' => 'periodEndDate',
			'type' => 'date',
			'selected' => stringHelper::dateToArray($data['periodEndDate'])
			)
		)
	),
	array(
		'params' => 'disableGroups',
		'fields' => array(
			array(
			'value' => L::_(43),
			'name' => 'submit',
			'type' => 'submit',
			'class' => 'submit3',
			'params' => 'hideLabel'
			)
		)
	)
);

$formEngine = new coreFormEngine();
$replCode .= $formEngine->requestForm($fieldsets, 'crmIntern',
	'addAssignment', array('type' => $type, 'id' => $id, 'amid' => $amid));

// finish
$this->replace('content', $replCode);

}

} // end class

?>