<?php

class crmInternViewQuestion
	extends crmInternView
{

function __construct($dataArray=null) {
	parent::__construct();
	$this->dataArray = $dataArray;
}

function processData() {

$dataArray = &$this->dataArray;
$uid = $dataArray['uid'];

$replCode = $this->geth1($dataArray['caption']);
$replCode .= $this->getp($dataArray['text']);

$replCode .= $this->getYesNoMenu($dataArray['linkYes'], $dataArray['linkNo']);

// finish
$this->replace('content', $replCode);

}

} // end class

?>