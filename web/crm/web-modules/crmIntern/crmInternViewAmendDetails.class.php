<?php

class crmInternViewAmendDetails
	extends crmInternView
{

protected $wzm;
protected $dataArray;
public $idt;
public $type;

	public function __construct() {
		$this->wzm = new wzm();
		$this->dataArray = $dataArray;
		$this->idt = $_GET['idt'];
		$this->type = $_GET['type'];
		parent::__construct(null, 'templates/indexWmDetails.tpl.html');
	}


	public function processData() {

		if(!isset($this->idt)) 
		{
			$ids = project::gta();
			foreach($ids as $idn => $idt)
			{

				$tariff[$idt] = $this->wzm->getTariff($idt);
				$details[$idt] = $this->wzm->getDetails($idt);

				$header[$idt] = $details[$idt]['header'];
				$data[$idt] = $details[$idt]['data'];

				/*
				** Trennung short und long in $data
				*/
				if(is_array($data[$idt]))
				{
					foreach($data[$idt] as $id => $content)
					{
						if($content['type'] == 'short')
							$tdshort[$idt][] = $content;
				
						if($content['type'] == 'long')
							$tdlong[$idt][] = $content;
					}
				}

				$instbl .= '<th>'.$tariff[$idt]['name'].'</th>';
#print $idt.' -> '.$instbl.'<br />';
			}

			/*
			** IDT ist nicht vorgegeben -> Ansicht nach Type (short / long)
			*/

			switch($this->type) {
				case 'short':
					// check
					if (count($header[$this->idt]) != count($tdshort[$this->idt]))
					{
						// Fehler - die ermittelten Daten sind nicht komplett!
						die('Fehler in der Ermittlung der Daten('.$header[$this->idt].') - die Headerdaten passen nicht zu den Zeilendaten zusammen!');
					}

					$bodytbl = '<h2>Kurzübersicht</h2>';
					$bodytbl .= '<div class="table">';
					$bodytbl .= '<table id="benefits" class="'.$this->type.' standard">';
					$bodytbl .= '<th>Versicherung</th>';

					$bodytbl .= $instbl;


				break;
				case 'long':
					// check
					if (count($header[$this->idt]) != count($tdlong[$this->idt]))
					{
						// Fehler - die ermittelten Daten sind nicht komplett!
						die('Fehler in der Ermittlung der Daten('.$header[$this->idt].') - die Headerdaten passen nicht zu den Zeilendaten zusammen!');
					}

					$bodytbl = '<h2>Langübersicht</h2>';
					$bodytbl .= '<div class="table">';
					$bodytbl .= '<table id="benefits" class="'.$this->type.' standard">';
					$bodytbl .= '<th>Versicherung</th>';

					$bodytbl .= $instbl;
				break;
			}
		}
		else
		{
			// IDT ist vorgegeben -> Ansicht nach Versicherung (short und long)
			$tariff[$this->idt] = $this->wzm->getTariff($this->idt);
			$details[$this->idt] = $this->wzm->getDetails($this->idt);

			$header[$this->idt] = $details[$this->idt]['header'];
			$data[$this->idt] = $details[$this->idt]['data'];

			// Trennung short und long in $data
			foreach($data[$this->idt] as $id => $content)
			{
				if($content['type'] == 'short')
					$tdshort[$this->idt][] = $content;
			
				if($content['type'] == 'long')
					$tdlong[$this->idt][] = $content;
			}

			// check
			if (count($header[$this->idt]) != count($tdshort[$this->idt]) || count($header[$this->idt]) != count($tdlong[$this->idt]))
			{
				// Fehler - die ermittelten Daten sind nicht komplett!
				die('Fehler in der Ermittlung der Daten('.$header[$this->idt].') - die Headerdaten passen nicht zu den Zeilendaten zusammen!');
			}

			$bodytbl = '<h2>'.$tariff[$this->idt]['name'].'</h2>';
			$bodytbl .= '<div class="table">';
			$bodytbl .= '<table id="benefits" class="'.$type.' standard">';
			$bodytbl .= '<thead><th>Beschreibung</th><th>Kurzübersicht</th><th>Langübersicht</th></thead><tbody>';
		}

if(count($header)>1)
	{
		$this->idt = 1;
	}
//print_r($header);
		for($i=1; $i<count($header[$this->idt]); $i++) {
			
			if(isset($header[$this->idt][$i]['type']) && $header[$this->idt][$i]['type'] == 'hl') {
				// we have a header

				if($i>3) { $bodytbl .= '</tbody>'; }

				$addClass = $header[$this->idt][$i]['type'] == 'hl'?'NoEdit':'';
				$thcolspan = 'colspan="'.(count($header) + 1).'"';

				$bodytbl .= '<thead>
				<tr class="'. $this->zebra($z++) .' '.$addClass.'">
					<th '.$thcolspan.'><h2 class="'.$addClass.'">'.$header[$this->idt][$i]['text'].' </h2></th>
				</tr>
				</thead>';
				
				if($i<count($header[$this->idt])) { $bodytbl .= '<tbody>'; }

			} else {
				// we have a normal line
				$bodytbl .= '
				<tr class="'. $this->zebra($z++) .'">
				<th>'.$header[$this->idt][$i]['text'].'</th>';

				if(count($header)==1)
				{
				$bodytbl .= '
					<td idt="'.$this->idt.'" cnt="'.$i.'" id="'.$tdshort[$this->idt][$i]['id'].'" >'.html_entity_decode($tdshort[$this->idt][$i]['text'], ENT_QUOTES | ENT_XML1, 'UTF-8').'</td>
					<td idt="'.$this->idt.'" cnt="'.$i.'" id="'.$tdlong[$this->idt][$i]['id'].'" >'.html_entity_decode($tdlong[$this->idt][$i]['text'], ENT_QUOTES | ENT_XML1, 'UTF-8').'</td>
					';
				} else {
					foreach($header as $idt => $idtContent)
						{
							if($this->type == 'short')
								$bodytbl .= '<td idt="'.$idt.'" cnt="'.$i.'" id="'.$tdshort[$idt][$i]['id'].'" >'.html_entity_decode($tdshort[$idt][$i]['text'], ENT_QUOTES | ENT_XML1, 'UTF-8').'</td>';
							else
								$bodytbl .= '<td idt="'.$idt.'" cnt="'.$i.'" id="'.$tdlong[$idt][$i]['id'].'" >'.html_entity_decode($tdlong[$idt][$i]['text'], ENT_QUOTES | ENT_XML1, 'UTF-8').'</td>';
						}
				}
				
				$bodytbl .= '</tr>';
			}
		}

		$bodytbl .= '</tbody></table></div>';



		$replCode = $bodytbl;

		// finish
		$this->replace('title', $tariff['name'] .' - '. $th['name']);
		$this->replace('robots', 'index, follow');
		$this->replace('content', $replCode);
}


}
?>
