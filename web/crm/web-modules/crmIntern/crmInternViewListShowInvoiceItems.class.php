<?php

class crmInternViewListShowInvoiceItems
	extends crmInternView
{

private $counter;

function __construct($dataArray=null) {
	parent::__construct();
	$this->dataArray = $dataArray;
}

function processData() {
// get data for prefilling fields
$data = (empty($this->dataArray['data']))?array():$this->dataArray['data'];

$replCode = '';


$label = new label();
$crm = new crm();
$company = new company();
$person = new person();
$invoice = new invoice();

// invoices
$replCode .= $this->geth1(L::_(132));
$replCode .= crmInternView::getInvoiceItemMenu();

// COMPANIES
$replCode .= $this->geth2(L::_(52));
$replCode .= "\n".'<div class="dataTable">';
$iiidRs = $invoice->getInvoiceItemsRs('cid');
$this->counter = 0;

while($item = $iiidRs->fetch()) {
	$com = $company->get($item['cid']);
	$tableRowData = $item;
	$tableRowData['firstcell'] = urlHelper::makeLink('crmIntern',
		'showCompany', $this->html($com['name']), array('cid' => $com['cid']));

	$replCode .= $this->getTableRow(&$tableRowData);
	$this->counter++;
}
$replCode .= "\n".'</table>';
$replCode .= "\n".'</div>';

// no results
if($this->counter == 0) {
	$replCode .= $this->getp(L::_(69));
}

// PERSONS
$replCode .= $this->geth2(L::_(53));
$replCode .= "\n".'<div class="dataTable">';
$iiidRs = $invoice->getInvoiceItemsRs('pid');
$this->counter = 0;

while($item = $iiidRs->fetch()) {
	$per = $person->get($item['pid']);
	$tableRowData = $item;
	$tableRowData['firstcell'] = urlHelper::makeLink('crmIntern',
		'showPerson', $this->html($per['surname'].", ".$per['forename']),
		array('pid' => $per['pid']));

	$replCode .= $this->getTableRow(&$tableRowData);
	$this->counter++;
}
$replCode .= "\n".'</table>';
$replCode .= "\n".'</div>';

// no results
if($this->counter == 0) {
	$replCode .= $this->getp(L::_(69));
}


// finish
$this->replace('content', $replCode);

}


private function getTableRow($data) {
	$rowClass = ($this->counter % 2)?'rowOdd':'rowEven';
	$data['price'] = number_format($data['price'], 2, ',', '.')." &euro;";

	return "\n".'
	<div class="'.$rowClass.'" >
	<div class="name"><span class="company">'.$data['firstcell'].'</span></div>
	<div class="caption"><span class="company">'.$this->html($data['caption'],1).'</span></div>
	<div class="from"><span class="company">'.$this->html($data['billingFrom'],1).'</span></div>
	<div class="till"><span class="company">'.$this->html($data['billingTill'],1).'</span></div>
	<div class="price"><span class="company">'.$data['price'].'</span></div>
	<div class="delivered"><span class="company">'.$this->html($data['deliveryDate'],1).'</span></div>
	<div class="created"><span class="company">'.$this->html($data['creationDate'],1).'</span></div>
	<div class="clearBoth"></div>
	</div>
	<!-- end row -->
	';
}


} // end class

?>