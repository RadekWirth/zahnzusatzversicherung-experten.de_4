<?php

class crmInternViewSpecialInsurance
	extends crmInternView
{

	function __construct($dataArray=null) {
		parent::__construct();
		$this->dataArray = $dataArray;
	}

	function processData() {


	$left = $right = '';


	// Initiate needed Classes
	$contract = new contract();

	$age = $this->dataArray;
	// Initiate END


	$rs = $contract->getSpecialTarifCustomers($age);

	$left .= "<ul>";

	foreach($rs as $id => $val)
	{
	$left .= "<li style='list-style: decimal-leading-zero;'>".$val['forename'].' '.$val['surname']." <div style='float: right; margin-right: 60%;'><a href='https://www.zahnzusatzversicherung-experten.de/crm/controller.php?cm=crmIntern&event=genSpecialContractPdfByImage&pid=".$val['pid']."&idco=".$val['idco']."&pdfTemplate=279' target='_new'>Antrag abrufen</a></div></li>";
	}

	$left .= "</ul>";

	// finish
	$replCode = crmInternView::couple($left, $right);
	$this->replace('content', $replCode);

	}

} // end class

?>