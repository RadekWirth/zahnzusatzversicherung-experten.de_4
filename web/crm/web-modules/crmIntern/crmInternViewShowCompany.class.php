<?php

class crmInternViewShowCompany
	extends myInternView
{

function __construct($dataArray=null) {
	parent::__construct();
	$this->dataArray = $dataArray;
}

function processData() {
$cid = $this->dataArray['cid'];
if(empty($cid)) {
	$this->replace('content', L::_(69));
}

$replCode = '';
$possibilites = '';
$groupName = '';

$label = new label();
$crm = new crm();
$company = new company();
$person = new person();


// COMPANY
$com = $company->get($cid, true);
$groupName = $crm->getGroupName('cid', $cid);

$com['companySizeLid'] = $this->html($label->getLabelName(
	$com['companySizeLid']));
$com['branchId'] = $this->html($crm->getBranchName(trim($com['branchId'])));
$com['note'] = nl2br($this->html(trim($com['note'])));
if(!empty($com['salesUnit'])) {
	$com['sales'] .= ' '.$this->html($label->getLabelName($com['salesUnit']));
}
unset($com['salesUnit']);
$addrc = $crm->getAddressPrimary('cid', $cid, true);
$addrc['stateId'] = $crm->getStateName($addrc['stateId']);
$addrc['country'] = $crm->getCountry($addrc['country']);
$contc = $crm->getContactPrimary('cid', $cid, true);

// tmp copy for skip at person hp/email, when equal
$contc_homepage = $contc['homepage'];
$contc_email = $contc['email'];

$contc['email'] = urlHelper::makeLink('crmIntern', 'sendMail',
	$contc['email'], array('type' => 'cid', 'id' => $cid,
	'email' => $contc['email']));

$contc['homepage'] = '<a href="go.php?www='.$contc['homepage'].'" target="_blank">'.
	$contc['homepage'].'</a>';

$replCode .= $this->geth1($com['name']);
if(!empty($groupName)) {
	$replCode .= $this->getp(L::_(137).': '.$groupName);
}

// international activities
$intActivities = $crm->getIntActivitiesFormated('cid', $cid);
if(count($intActivities)) {
$intActivitiesActions = array();
$intActivitiesActions[] = array('ids' => -1, 'cm' => 'crmIntern',
	'event' => 'deleteIntActivity', 'caption' => L::_(167));
}

$left  = viewHelper::genEasyTableWithTextids(L::_(58), $com,   3, '', true);
if(count($intActivities)) {
$left .= viewHelper::generateTableExtended(L::_(163), $intActivities,
	$intActivitiesActions, 3, '', array('type' => 'cid', 'id' => $cid));
}
$left .= viewHelper::genEasyTableWithTextids(L::_(31), $addrc, 3, '', true);
$left .= viewHelper::genEasyTableWithTextids(L::_(36), $contc, 3, '', true);


$right  = crmInternView::getCompanyMenu($cid);
$right .= crmInternView::getAssignments('cid', $cid);
$right .= crmInternView::getNotes('cid', $cid, 60);

$replCode .= crmInternView::couple($left, $right);


// PERSONS
$personRs = $person->getByCompanyRs($cid);
if($personRs->numRows()) {
	$left .= $this->geth1(L::_(53));
}
while($row = $personRs->fetch()) {
	$left = $right = '';
	$pid = $row['pid'];
	unset($row['cid']);
	unset($row['pid']);
	unset($row['uid']);

	$row['salutationLid'] = $label->getLabelName($row['salutationLid']);
	$row['titleLid'] = $label->getLabelName($row['titleLid']);
	$row['jobPositionId'] = $crm->getJobPositionName($row['jobPositionId']);

	if($row['birthdate'] == '0000-00-00') {
		unset($row['birthdate']);
	}

	$addrp = $crm->getAddressPrimary('pid', $pid, true);
	$addrp['stateId'] = $crm->getStateName($addrp['stateId']);
	$addrp['country'] = $crm->getCountry($addrp['country']);
	$contp = $crm->getContactPrimary('pid', $pid, true);

	if($contp['homepage'] == $contc_homepage) {
		unset($contp['homepage']);
	}
	else {
		if(!empty($contp['homepage'])) {
		$contp['homepage'] = '<a href="'.$contp['homepage'].'" target="_blank">'.
			$contp['homepage'].'</a>';
		}
	}

	if($contp['email'] == $contc_email) {
		unset($contp['email']);
	}
	else {
		if(!empty($contp['email'])) {
		$contp['email'] = urlHelper::makeLink('crmIntern', 'sendMail',
			$contp['email'], array('type' => 'pid', 'id' => $pid,
			'email' => $contp['email']));
		}
	}

	// ank
	$left .= "\n".'<a name="pid-'.$pid.'"></a>';

	$left .= $this->geth2($this->html($row['forename'].' '.$row['surname']));
 	$left .= viewHelper::genEasyTableWithTextids(L::_(59), $row, 3, '', 1);
	$left .= viewHelper::genEasyTableWithTextids(L::_(31), $addrp, 3, '', 1);
	$left .= viewHelper::genEasyTableWithTextids(L::_(36), $contp, 3, '', 1);

	$right .= crmInternView::getPersonMenu($pid, $cid);
	$right .= crmInternView::getNotes('pid', $pid);
	$replCode .= crmInternView::couple($left, $right);
}

// finish
$this->replace('content', $replCode);

}

} // end class

?>
