<?php

class crmInternViewHome
	extends crmInternView
{

function __construct($dataArray=null) {
	parent::__construct();
	$this->dataArray = $dataArray;
}

function processData() {

// get data for prefilling fields
$data = (empty($this->dataArray['data']))?array():$this->dataArray['data'];

$left = $right = '';
$possibilites = '';
$antrag_image = array(0 => 'byPost', 1 => 'byPost', 2 => 'byMail', 3 => 'byMailPost');


// Initiate needed Classes
	$contract = new contract();
	$crm = new crm();
	$wzm = new wzm();
	$reminder = new reminder(project::getProjectRoot());
	$person = new person();
	$entries = array();
// Initiate END


$filterLimit = isset($_GET['page']) ? $_GET['page'] : 0;


// FilterDate used?
if($_GET['vonDateD'] && $_GET['vonDateM'] && $_GET['vonDateY'] && $_GET['bisDateD'] && $_GET['bisDateM'] && $_GET['bisDateY'])
	{
		//Filter Datum gesetzt
		$filterDate = array();
		$filterDate['von'] = $_GET['vonDateY'].'-'.$_GET['vonDateM'].'-'.$_GET['vonDateD'];
		$filterDate['bis'] = $_GET['bisDateY'].'-'.$_GET['bisDateM'].'-'.$_GET['bisDateD'];
		

		if($_GET['telefon'] == 'on' || $_GET['telefon'] == '1')
		{
			$contract->setShowJustPhone(1);
		} else $contract->setShowJustPhone(0);

		$rs = $contract->getFromDate($filterDate, $filterLimit);

		$rscount = $contract->getCountFromDate($filterDate);
			$count = $rscount->fetch();
		$rscount = $contract->getRealCountFromDate($filterDate);
			$realCount = $rscount->fetch();

		$linksLeiste = $this->generateLinkLine($filterDate, $count['result']); 

/*
** 46 = Antrag noch nicht rausgeschickt
** 48 = Antrag rausgeschickt
** 49 = Antrag zurück
** 50 = Antrag policiert
*/

		$rscount = $contract->getCountFromDate($filterDate, 49);
			$returnedCount = $rscount->fetch();
		$rscount = $contract->getCountFromDate($filterDate, 50);
			$returnedCountPol = $rscount->fetch();

		$left .= $this->geth1('Gewähltes Datum: '.$_GET['vonDateD'].'.'.$_GET['vonDateM'].'.'.$_GET['vonDateY'].' - '.$_GET['bisDateD'].'.'.$_GET['bisDateM'].'.'.$_GET['bisDateY']);
		$left .= $this->geth3('Anzahl Datensätze: '.$count['result'].' (real: '.$realCount['result'].') - davon zurückgekommen: '.($returnedCount['result'] + $returnedCountPol['result']).' ('.round(($returnedCount['result'] + $returnedCountPol['result']) / $realCount['result'] * 100, 2).' %)');
		$left .= $linksLeiste;
	}
else 
	{
		$left .= $this->geth1(L::_(114));
		$rs = $contract->getLatest(50);
	}


while($row = $rs->fetch()) {
	$p = $person->get($row['pid']);
		$forename = $p['forename'];
		$surname = $p['surname'];
	$address[$row['pid']] = $crm->getAddressPrimary('pid', $row['pid']); 
	$s = $wzm->getSupportMessages($row['pid']);
		$SupportMessage = $s->fetch();
	
	
	
	$c = $crm->getContactPrimary('pid', $row['pid']);
		$phone = empty($c['mobile'])?"(".$c['phone'].")":"(".$c['mobile'].")";
		if ($phone=="()") unset($phone);

	switch($row['browserDevice'])
	{
		case 'desktop':
			$source = 'desktop';
			break;
		case 'mobile':
			$source = 'mobile';
			break;
	}

	$tariff = $wzm->getTariff($row['idt']);
	
	$date_row[$row['pid']] = '<span class="status">_ _</span>';


if(isset($SupportMessage['message']))
{
	$e = str_split($SupportMessage['message']);
	$new_string = '';
	foreach ($e as $pos => $var)
	{

		if(ord($var) == 10 || ord($var) == 13)
		{
			$new_string .= '<br />';
		} else 
			$new_string .= $var;

	}
	$new_string = htmlspecialchars($new_string);
}

	$note = isset($SupportMessage['message'])?'<a href="javascript:void(0);" onmouseover="return overlib(\''.$new_string.'\', AUTOSTATUS, WRAP);" onmouseout="nd();"><img src="img/note.png" width="10" /></a>':'';
	$entries[$row['pid']] = '<span class="insurance-'.$source.'" /><span class="status"><span class="s'.
		$row['contractStatusLid'].'">&nbsp;&nbsp;</span></span>&nbsp;'.
		stringHelper::makeShortDateFromSignUp($row['signupTime']).'&nbsp;
		<span class="image">'.$note.'</span>&nbsp;<span class="insurance">['.$tariff['shortname'].']</span>
		<span class="phone" style="float:right; margin-right:5px;">';
	if($row['recallIsDesired']==1)
			$entries[$row['pid']] .= '<img src="img/phone.png" width="12" /> -> ';
		$entries[$row['pid']] .= isset($phone)?$phone:"".'</span>';
		#<span class="gkv"> - GKV : '.$p[insurance].'</span>&nbsp';



	$insurance[$row['pid']] = $p['insurance'];
	$actions[] = array('ids' => array($row['pid'] => $row['pid']), 'cm' => 'crmIntern',
		'event' => 'showPerson', 'caption' => $surname.', '.
		$forename);
	unset($n);
}



$left .= viewHelper::generateTableExtendedPlus(L::_(344), $entries, $insurance, $address, $actions,
	2, 'b');



// Actions
$actions = $this->geth2(L::_(71)).'<ul>';
$actions .= '<li>'.urlHelper::makeLink('crmIntern', 'genContractPdfByImage', L::_(588), array('idco'=>0, 'complete'=>2, 'count'=>5)).'</li>';
$actions .= '<li>'.urlHelper::makeLink('crmIntern', 'genContractPdfByImage', L::_(590), array('idco'=>0, 'complete'=>2, 'reset'=>1, 'count'=>5)).'</li>';
$actions .= '<li>'.urlHelper::makeLink('crmIntern', 'genContractPdfByImage', L::_(589), array('idco'=>0, 'complete'=>2, 'count'=>1, 'statusChange'=>1)).'</li>';
$actions .= '</ul>';

$right .= $actions;


// stats contract
$stats = $contract->getStats();
$statsCode = $this->geth2(L::_(361)).'<ul>';
$sum=0;
foreach($stats as $stat) {

$statLink = $stat['count']>0?urlHelper::makeLink('crmIntern', 'showPersonsByStatus', $stat['name'], array('status' => (isset($stat['lid'])?$stat['lid']:"") )):$stat['name'];
$statsCode .= '<li><span class="status"><span class="s'.(isset($stat['lid'])?$stat['lid']:"").
	'">&nbsp;&nbsp;</span></span>
	&nbsp;'.$statLink.': <b>'.
	$stat['count'].'</b></li>';

$sum = $sum + $stat['count'];
}
$statsCode .= '</ul>';
$statsCode .= $this->getp(L::_(362).': <b>'.$sum.'</b>');
$right .= $statsCode ;

// Anträge Statistik
	$statsCode = "<div id=\"extra_stats\"><ul>";
	$contractData = $contract->getStats('extra_stats');

	if(isset($contractData)) {
	foreach($contractData as $no)
	{
		if(isset($no['senttext']) && $no['senttext']>0) $statsCode .= "<li>".L::_($no['senttext'])." : <b>".$no['sent']."</b></li>";
		if(isset($no['rettext']) && $no['rettext']>0) $statsCode .= "<li>".L::_($no['rettext'])." : <b>".$no['ret']."</b></li>";
	}}

	$statsCode .= "</ul></div>";
	$right .= $statsCode;


// stats lead
	$wzm = new wzm();
	$stats = $wzm->getLeadStats();
	$statsCode = $this->geth2(L::_(259)).'<ul>';
	$sum=0;
	foreach($stats as $stat) {
		$statsCode .= '<li>'.$stat['name'].': <b>'.$stat['count'].'</b></li>';
		$sum += $stat['count'];
	}
	$statsCode .= '</ul>';
	$statsCode .= $this->getp(L::_(362).': <b>'.$sum.'</b>');

	$right .= $statsCode ;

// Antragserinnerung
	$remLinks = $reminder->showDataLines();
	$remindCode = "<div id=\"antragserinnerung\">".$this->geth2('Antragserinnerung')."<ul>";
	$remindCode .= $remLinks;
	$remindCode .="</ul></div>";
	$remindCode .= $this->getp('Email-Erinnerungen seit 28.09.2011');
	$remindCode .= $this->getp('Erinnerungsschreiben seit 01.06.2011');	
	$right .= $remindCode;


// Graph
	$statsCode = "<div id=\"zahn_graph\">";
	if(file_exists('graph.php')) $statsCode .= "<img src=\"graph.php\" />";
	$statsCode .= "</div>";
	$right .= $statsCode;



// finish
$replCode = crmInternView::couple($left, $right);
$this->replace('content', $replCode);

}

} // end class

?>

