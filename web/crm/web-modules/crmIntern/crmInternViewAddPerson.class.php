<?php

class crmInternViewAddPerson
	extends crmInternView
{

function __construct($dataArray=null) {
	parent::__construct();
	$this->dataArray = $dataArray;
}

function processData() {
// get data for prefilling fields
$data = (empty($this->dataArray['data']))?array():$this->dataArray['data'];

$replCode = '';

$label = new label();
$crm = new crm();
$person = new person();
$db = project::getDb();

$pid = null;
if($pid = $this->dataArray['pid']) {
	$data['person'] = $person->get($pid);
	$data['contact'] = $crm->getContactPrimary('pid', $pid);
	$data['address'] = $crm->getAddressPrimary('pid', $pid);
	$replCode .= $this->geth1(L::_(109));
}
else {
	$replCode .= $this->geth1(L::_(44));
}

$cid = null;
if($cid = $this->dataArray['cid']) {
	$data['mapping'] = $person->getMapping($pid, $cid);
}


// generate options for salutation
$salutationRs = $label->getLabels('salutation');
$salutationOptions = array(0 => L::_(25));
if(!empty($salutationRs))
while($row = $db->fetchPDO($salutationRs)) {
	$salutationOptions[$row['lid']] = $row['name'];
}

// generate options for states
$stateRs = $crm->getStatesRs();
$stateOptions = array(0 => L::_(25));
if($stateRs)
while($row = $db->fetchPDO($stateRs)) {
	$stateOptions[$row['sid']] = $row['name'];
}

// generate options for title
$titleRs = $label->getLabels('title');
$titleOptions = array(0 => L::_(25));
if(!empty($titleRs))
while($row = $db->fetchPDO($titleRs)) {
	$titleOptions[$row['lid']] = $row['name'];
}

// generate options for country
$countryRs = $crm->getCountries();
$countryOptions = array('0' => L::_(25));
if(!empty($countryRs))
while($row = $db->fetchPDO($countryRs)) {
	$countryOptions[$row['code']] = $row['value'];
}

// generate options for insuranceStatus
$insuranceStatusRs = $label->getLabels('insuranceStatus');
$insuranceStatusOptions = array(0 => L::_(25));
if(!empty($insuranceStatusRs))
while($row = $db->fetchPDO($insuranceStatusRs)) {
	$insuranceStatusOptions[$row['lid']] = $row['name'];
}

// generate options for familyStatus
$familyStatusRs = $label->getLabels('familyStatus');
$familyStatusOptions = array(0 => L::_(25));
if(!empty($familyStatusRs))
while($row = $db->fetchPDO($familyStatusRs)) {
	$familyStatusOptions[$row['lid']] = $row['name'];
}

if(empty($data['address']['country'])) {
	$data['address']['country'] = 'DEU';
}

// person mapping to company
$personMappingArray = null;
if(!empty($cid)) {
	// generate options for jobPosition
	$jobPositionRs = $crm->getJobPositionsRs();
	$jobPositionOptions = array('0' => L::_(25));
	while($row = $db->fetchPDO($rs)) {
		$jobPositionOptions[$row['jpid']] = $row['name'];
	}

	$personMappingArray =
	array(
		'legend' => L::_(110),
		'group' => 'mapping',
		'fields' => array(
			array(
			'label' => L::_(54),
			'name' => 'department',
			'type' => 'text',
			'size' => 'big',
			'maxlength' => 255,
			'value' => $data['mapping']['department']
			),
			array(
			'label' => L::_(55),
			'name' => 'jobPositionId',
			'type' => 'select',
			'size' => 'big',
			'options' => $jobPositionOptions,
			'selected' => $data['mapping']['jobPositionId']
			)
		)
	);
}


// create field sets
$fieldsets =
array(
	array(
		'legend' => L::_(46),
 		'group' => 'person',
		'fields' => array(
			array(
			'label' => L::_(45),
			'name' => 'salutationLid',
			'type' => 'select',
			'size' => 'big',
			'options' => $salutationOptions,
			'selected' => $data['person']['salutationLid']
			),
			array(
			'label' => L::_(47),
			'name' => 'titleLid',
			'type' => 'select',
			'size' => 'big',
			'options' => $titleOptions,
			'selected' => $data['person']['titleLid']
			),
			array(
			'label' => L::_(48),
			'name' => 'forename',
			'type' => 'text',
			'size' => 'big',
			'maxlength' => 100,
			'value' => $data['person']['forename']
			),
			array(
			'label' => L::_(49),
			'name' => 'surname',
			'type' => 'text',
			'size' => 'big',
			'maxlength' => 100,
			'value' => $data['person']['surname']
			),
			array(
			'label' => 'Geburtsname',
			'name' => 'birthname',
			'type' => 'text',
			'size' => 'big',
			'maxlength' => 100,
			'value' => $data['person']['birthname']
			),
			array(
			'label' => L::_(50),
			'name' => 'birthdate',
			'type' => 'date',
			'selected' => stringHelper::dateToArray(
				$data['person']['birthdate'])
			),
			array(
			'label' => L::_(211),
			'name' => 'nationCode',
			'type' => 'radio',
			'options' => array(
				'DEU' => L::_(317),
				'000' => L::_(318)),
			'selected' => $data['person']['nationCode']
			),
			array(
			'label' => L::_(319),
			'name' => 'nationString',
			'type' => 'text',
			'size' => 'big',
			'maxlength' => 100,
			'value' => $data['person']['nationString']
			),
			array(
			'label' => L::_(258),
			'name' => 'familyStatusLid',
			'type' => 'select',
			'size' => 'big',
			'options' => $familyStatusOptions,
			'selected' => $data['person']['familyStatusLid']
			),
			array(
			'label' => L::_(213),
			'name' => 'job',
			'type' => 'text',
			'size' => 'big',
			'maxlength' => 100,
			'value' => $data['person']['job']
			),
			array(
			'label' => L::_(214),
			'name' => 'branch',
			'type' => 'text',
			'size' => 'big',
			'maxlength' => 100,
			'value' => $data['person']['branch']
			),
			array(
			'label' => L::_(215),
			'name' => 'insurance',
			'type' => 'text',
			'size' => 'big',
			'maxlength' => 100,
			'value' => $data['person']['insurance']
			),
			array(
			'label' => L::_(216),
			'name' => 'insuredSince',
			'type' => 'date',
			'selected' => stringHelper::dateToArray(
				$data['person']['insuredSince']),
			'params' => 'hideDay,hideMonth',
			'defaultCaptions' => array('D' => L::_(278), 'M' => L::_(279),
				'Y' => L::_(280))
			),
			array(
			'label' => L::_(217),
			'name' => 'insuranceStatusLid',
			'type' => 'select',
			'size' => 'big',
			'options' => $insuranceStatusOptions,
			'selected' => $data['person']['insuranceStatusLid']
			)
		)
	),
	array(
		'legend' => L::_(31),
		'group' => 'address',
		'fields' => array(
			array(
			'label' => L::_(32),
			'name' => 'street',
			'type' => 'text',
			'size' => 'big',
			'maxlength' => 255,
			'value' => $data['address']['street']
			),
			array(
			'label' => L::_(33),
			'name' => 'postcode',
			'type' => 'text',
			'size' => 'small',
			'maxlength' => 10,
			'value' => $data['address']['postcode']
			),
			array(
			'label' => L::_(34),
			'name' => 'city',
			'type' => 'text',
			'size' => 'big',
			'maxlength' => 100,
			'value' => $data['address']['city']
			),
			array(
			'label' => L::_(35),
			'name' => 'country',
			'type' => 'select',
			'size' => 'big',
			'options' => $countryOptions,
			'selected' => $data['address']['country']
			)
		)
	),
	array(
		'legend' => L::_(36),
		'group' => 'contact',
		'fields' => array(
			array(
			'label' => L::_(37),
			'name' => 'phone',
			'type' => 'text',
			'size' => 'medium-big',
			'maxlength' => 50,
			'value' => $data['contact']['phone']
			),
			array(
			'label' => L::_(39),
			'name' => 'mobile',
			'type' => 'text',
			'size' => 'medium-big',
			'maxlength' => 50,
			'value' => $data['contact']['mobile']
			),
			array(
			'label' => L::_(40),
			'name' => 'fax',
			'type' => 'text',
			'size' => 'medium-big',
			'maxlength' => 50,
			'value' => $data['contact']['fax']
			),
			array(
			'label' => L::_(41),
			'name' => 'email',
			'type' => 'text',
			'size' => 'medium-big',
			'maxlength' => 255,
			'value' => $data['contact']['email']
			),
			array(
			'label' => L::_(42),
			'name' => 'homepage',
			'type' => 'text',
			'size' => 'medium-big',
			'maxlength' => 150,
			'value' => $data['contact']['homepage']
			)
		)
	),

	$personMappingArray,

	array(
		'params' => 'disableGroups',
		'fields' => array(
			array(
			'value' => L::_(43),
			'name' => 'submit',
			'type' => 'submit',
			'class' => 'submit3',
			'params' => 'hideLabel'
			)
		)
	)
);

$formEngine = new coreFormEngine();
$replCode .= $formEngine->requestForm($fieldsets, 'crmIntern',
	'addPerson', array('pid' => $pid, 'cid' => $cid));

// finish
$this->replace('content', html_entity_decode($replCode));

}

} // end class

?>