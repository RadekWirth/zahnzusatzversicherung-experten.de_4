<?php

class crmInternViewListShowInvoices
	extends crmInternView
{

private $counter;

function __construct($dataArray=null) {
	parent::__construct();
	$this->dataArray = $dataArray;
}

function processData() {
$replCode = '';

// determine status for viewing invoices
if(empty($this->dataArray['viewStatus'])) {
	$viewStatus = array(135 => 'wait', 147 => 'sent');
}
elseif($this->dataArray['viewStatus']=='payed') {
	$viewStatus = array(155 => 'payed');
}

$label = new label();
$crm = new crm();
$company = new company();
$person = new person();
$invoice = new invoice();

// invoices
$replCode .= $this->geth1(L::_(131));
$replCode .= crmInternView::getInvoiceMenu();


foreach($viewStatus as $lvs => $vs) {
	$replCode .= $this->geth2(L::_($lvs));
	$replCode .= "\n".'<div class="dataTable">';
	$invoiceRs = $invoice->getInvoicesByStatusRs($vs);
	$this->counter = 0;
	$sum = 0;
	while($inv = $invoiceRs->fetch()) {
		$sum += $inv['price'];
		$info = $crm->getInfo(&$inv);
		$tableRowData = array_merge($inv, $info);
		$tableRowData['firstcell'] = $info['showLink'];
		$tableRowData['items'] = $invoice->getInvoiceItemsByIid($inv['iid']);
		$replCode .= $this->getTableRow(&$tableRowData);
		$this->counter++;
	}
	// $replCode .= "\n".'</table>';
	$replCode .= "\n".'</div>';

	// no results
	if($this->counter == 0) {
		$replCode .= $this->getp(L::_(69));
	}else {
		$replCode .= $this->getp(L::_(136).': '.enviro::formatPrice($sum));
	}

} // end foreach

// finish
$this->replace('content', $replCode);

}


private function getTableRow($data) {
	$rowClass = ($this->counter % 2)?'rowOdd':'rowEven';
	$data['price'] = enviro::formatPrice($data['price']);
	$link = urlHelper::makeLink('crmIntern', 'getInvoicePdf',
		$data['numberText'], array('iid' => $data['iid']));

	$payedLink = '';
	if($data['status'] == 'sent') {
		$payedLink = urlHelper::makeLink('crmIntern', 'setInvoiceStatus',
			L::_(154), array('iid' => $data['iid'], 'status' => 'payed'));
	}
	elseif($data['status'] == 'payed') {
		$payedLink = urlHelper::makeLink('crmIntern', 'setInvoiceStatus',
			L::_(156), array('iid' => $data['iid'], 'status' => 'sent'));
	}


	$typeLink = '';
	if($data['status'] == 'wait') {
		if(empty($data['caption']) || $data['caption'] == L::_(124)) {
			$typeLink = urlHelper::makeLink('crmIntern', 'setInvoiceType',
				L::_(124), array('iid' => $data['iid'], 'type' => 1));
		}
		elseif($data['caption'] == L::_(157)) {
			$typeLink = urlHelper::makeLink('crmIntern', 'setInvoiceType',
				L::_(157), array('iid' => $data['iid'], 'type' => 0));
		}
	}

	return "\n".'
	<div class="'.$rowClass.'" >
	<div class="name"><span class="company">'.$data['firstcell'].'</span></div>
	<div class="caption"><span class="company">'.$link.'</span></div>
	<div class="price"><span class="company">'.$data['price'].'</span></div>
	<div class="till"><span class="company">'.$this->html($data['billingDate'],1).'</span></div>
	<div class="till"><span class="company">'.$this->html(substr($data['sendDate'], 0, 10), 1).'</span></div>
	<div class="till"><span class="company">'.$payedLink.'</span></div>
	<div class="till"><span class="company">'.$typeLink.'</span></div>
	<div class="clearBoth"></div>
	<div>
	'.$this->getTableInvoiceItemsRows($data['items']).'
	</div>
	</div>
	<!-- end row -->
	';
}


private function getTableInvoiceItemsRows($items) {
	$ret = '';
	if(count($items))
	foreach($items as $data) {

	$rowClass = ($this->counter % 2)?'rowOdd':'rowEven';
	$data['price'] = enviro::formatPrice($data['price']);

	$ret .= "\n".'
	<div class="'.$rowClass.'" >
	<div class="name"><span class="company">&nbsp;</span></div>
	<div class="caption"><span class="grey">'.$this->html($data['caption'],1).'</span></div>
	<div class="price"><span class="grey">'.$data['price'].'</span></div>
	<div class="from"><span class="grey">'.$this->html($data['billingFrom'],1).'</span></div>
	<div class="till"><span class="grey">'.$this->html($data['billingTill'],1).'</span></div>
	<div class="clearBoth"></div>
	</div>
	<!-- end row -->
	';

	} // end foreach

	return $ret;
}


} // end class

?>