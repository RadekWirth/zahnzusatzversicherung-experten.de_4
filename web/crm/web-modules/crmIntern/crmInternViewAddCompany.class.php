<?php

class crmInternViewAddCompany
	extends myInternView
{

function __construct($dataArray=null) {
	parent::__construct();
	$this->dataArray = $dataArray;
}

function processData() {
// get data for prefilling fields
$data = (empty($this->dataArray['data']))?array():$this->dataArray['data'];

$replCode = '';

$label = new label();
$crm = new crm();
$company = new company();

$cid = null;
if($cid = $this->dataArray['cid']) {
	$data['company'] = $company->get($cid);
	$data['contact'] = $crm->getContactPrimary('cid', $cid);
	$data['address'] = $crm->getAddressPrimary('cid', $cid);
	$replCode .= $this->geth1(L::_(70));
}
else {
	$replCode .= $this->geth1(L::_(8));
}

// generate options for companySize
$companySizeRs = $label->getLabels('companySize', 'lid');
$companySizOptions = array(0 => L::_(25));
if($companySizeRs)
while($row = $companySizeRs->fetch()) {
	$companySizOptions[$row['lid']] = $row['name'];
}

// generate options for salesUnit
$salesUnitRs = $label->getLabels('money', 'lid');
$salesUnitOptions = array(0 => L::_(117));
while($row = $salesUnitRs->fetch()) {
	$salesUnitOptions[$row['lid']] = $row['name'];
}

// generate options for branches
$branchesRs = $crm->getBranchesRs();
$branchOptions = array(0 => L::_(25));
if($branchesRs)
while($row = $branchesRs->fetch()) {
	$branchOptions[$row['bid']] = $row['name'];
}


// generate options for states
$stateRs = $crm->getStatesRs();
$stateOptions = array(0 => L::_(25));
if($stateRs)
while($row = $stateRs->fetch()) {
	$stateOptions[$row['sid']] = $row['name'];
}

// generate options for country
$countryRs = $crm->getCountries();
$countryOptions = array('0' => L::_(25));
if(!empty($countryRs))
while($row = $countryRs->fetch()) {
	$countryOptions[$row['code']] = $row['value'];
}

if(empty($data['address']['country'])) {
	$data['address']['country'] = 'DEU';
}


// create field sets
$fieldsets =
array(
	array(
		'legend' => L::_(9),
		'group' => 'company',
		'fields' => array(
			array(
			'label' => L::_(10),
			'name' => 'name',
			'type' => 'text',
			'size' => 'big',
			'maxlength' => 255,
			'value' => $data['company']['name']
			),
			array(
			'label' => L::_(11),
			'name' => 'subtitle',
			'type' => 'text',
			'size' => 'big',
			'maxlength' => 255,
			'value' => $data['company']['subtitle']
			),
			array(
			'label' => L::_(13),
			'name' => 'companySizeLid',
			'type' => 'select',
			'size' => 'big',
			'options' => $companySizOptions,
			'selected' => $data['company']['companySizeLid']
			),
			array(
			'params' => 'appendToNextEntry',
			'name' => 'salesUnit',
			'type' => 'select',
			'size' => 'small',
			'options' => $salesUnitOptions,
			'selected' => $data['company']['salesUnit']
			),
			array(
			'label' => L::_(14),
			'name' => 'sales',
			'type' => 'text',
			'size' => 'medium',
			'maxlength' => 20,
			'value' => $data['company']['sales']
			),
			array(
			'label' => L::_(16),
			'name' => 'foundation',
			'type' => 'text',
			'size' => 'small',
			'maxlength' => 4,
			'value' => $data['company']['foundation']
			),
			array(
			'label' => L::_(17),
			'name' => 'branchId',
			'type' => 'select',
			'size' => 'big',
			'options' => $branchOptions,
			'selected' => $data['company']['branchId']
			),
			array(
			'label' => L::_(18),
			'name' => 'note',
			'type' => 'textarea',
			'size' => 'big',
			'value' => $data['company']['note']
			)
		)
	),
	array(
		'legend' => L::_(31),
		'group' => 'address',
		'fields' => array(
			array(
			'label' => L::_(32),
			'name' => 'street',
			'type' => 'text',
			'size' => 'big',
			'maxlength' => 255,
			'value' => $data['address']['street']
			),
			array(
			'label' => L::_(33),
			'name' => 'postcode',
			'type' => 'text',
			'size' => 'small',
			'maxlength' => 10,
			'value' => $data['address']['postcode']
			),
			array(
			'label' => L::_(34),
			'name' => 'city',
			'type' => 'text',
			'size' => 'big',
			'maxlength' => 100,
			'value' => $data['address']['city']
			),
			array(
			'label' => L::_(19),
			'name' => 'stateId',
			'type' => 'select',
			'size' => 'big',
			'options' => $stateOptions,
			'selected' => $data['address']['statdeId']
			),
			array(
			'label' => L::_(35),
			'name' => 'country',
			'type' => 'select',
			'size' => 'big',
			'options' => $countryOptions,
			'selected' => $data['address']['country']
			)
		)
	),
	array(
		'legend' => L::_(36),
		'group' => 'contact',
		'fields' => array(
			array(
			'label' => L::_(37),
			'name' => 'phone',
			'type' => 'text',
			'size' => 'medium-big',
			'maxlength' => 50,
			'value' => $data['contact']['phone']
			),
			array(
			'label' => L::_(39),
			'name' => 'mobile',
			'type' => 'text',
			'size' => 'medium-big',
			'maxlength' => 50,
			'value' => $data['contact']['mobile']
			),
			array(
			'label' => L::_(40),
			'name' => 'fax',
			'type' => 'text',
			'size' => 'medium-big',
			'maxlength' => 50,
			'value' => $data['contact']['fax']
			),
			array(
			'label' => L::_(41),
			'name' => 'email',
			'type' => 'text',
			'size' => 'medium-big',
			'maxlength' => 255,
			'value' => $data['contact']['email']
			),
			array(
			'label' => L::_(42),
			'name' => 'homepage',
			'type' => 'text',
			'size' => 'medium-big',
			'maxlength' => 150,
			'value' => $data['contact']['homepage']
			)
		)
	),
	array(
		'params' => 'disableGroups',
		'fields' => array(
			array(
			'value' => L::_(43),
			'name' => 'submit',
			'type' => 'submit',
			'class' => 'submit3',
			'params' => 'hideLabel'
			)
		)
	)
);

$formEngine = new coreFormEngine();
$replCode .= $formEngine->requestForm($fieldsets, 'crmIntern',
	'addCompany', array('cid' => $cid));

// finish
$this->replace('content', $replCode);

}

} // end class

?>