<?php

class crmInternViewSendMail
	extends crmInternView
{

function __construct($dataArray=null) {
	parent::__construct();
	$this->dataArray = $dataArray;
}

function processData() {
// get data for prefilling fields
$data = (empty($this->dataArray['data']))?array():$this->dataArray['data'];

$dataArray = &$this->dataArray;
$type = $dataArray['type'];
$id = $dataArray['id'];
$email = $dataArray['email'];

$replCode = $this->geth1(L::_(63));

if(empty($data['recipient'])) {
	$data['recipient'] = $email;
}

if(empty($email)) {
	$email = $data['recipient'];
}

if(empty($data['subject'])) {
	$data['subject'] = L::_(62);
}

if(empty($data['message'])) {
	$data['message'] = pref::get('emailSignatur');
}

// create field sets
$fieldsets =
array(
	array(
		'params' => 'noGroup',
		'fields' => array(
			array(
			'label' => L::_(64),
			'name' => 'recipient',
			'type' => 'text',
			'size' => 'big',
			'orientation' => 'vertical',
			'value' => $data['recipient'],
			'readonly' => 'readonly',
			'class' => 'readonly'
			),
			array(
			'label' => L::_(65),
			'name' => 'subject',
			'type' => 'text',
			'size' => 'big',
			'orientation' => 'vertical',
			'value' => $data['subject']
			),
			array(
			'label' => L::_(66),
			'name' => 'message',
			'type' => 'textarea',
			'size' => 'big',
			'orientation' => 'vertical',
			'value' => $data['message']
			),
			array(
			'value' => L::_(67),
			'name' => 'submit',
			'type' => 'submit',
			'class' => 'submit3',
			'params' => 'hideLabel',
			'orientation' => 'vertical',
			'size' => 'small'
			),

		)
	)
);

$formEngine = new coreFormEngine();
$replCode .= $formEngine->requestForm($fieldsets, 'crmIntern',
	'sendMail', array('type' => $type, 'id' => $id));

// finish
$this->replace('content', $replCode);

}

} // end class

?>