<?php

class crmInternViewListShowAllPersons
	extends crmInternView
{

function __construct($dataArray=null) {
	parent::__construct();
	$this->dataArray = $dataArray;
}

function processData() {
// get data for prefilling fields
$data = (empty($this->dataArray['data']))?array():$this->dataArray['data'];

$replCode = '';
$possibilites = '';


$label = new label();
$crm = new crm();
$person = new person();
$contract = new contract();
$wzm = new wzm();

$firstLetter = null;
if(!empty($this->dataArray['firstLetter'])) {
	$firstLetter = $this->dataArray['firstLetter'];
}

if(!empty($this->dataArray['personIds'])) {
	$page = $this->dataArray['personIds']['page'];
	$count = $this->dataArray['personIds']['count'];
	unset($this->dataArray['personIds']['page']);
	unset($this->dataArray['personIds']['count']);
	$personRs = $person->selectByIds($this->dataArray['personIds'], 'DESC');
}
else {
	$personRs = $person->getSinglesRs($firstLetter);
}


// persons
$replCode .= $this->geth1(L::_(53));


// 50 entries should be shown on each page
$showPages = intval($count/50);
if($count%50>0) $showPages++;

if($showPages>1) {
$htmlset = "<div id='selection'>Seite ";
	for($i=1; $i<=$showPages; $i++)
	{
		if($showPages<=10)
		{
			$htmlset .= urlHelper::makeLink('crmIntern', 'showPersonsByStatus', $i, array('status'=>$_GET['status'],'page'=>$i));
			$htmlset .= "&nbsp;";
		} else {
			if($i>2 && $i<$showPages-1 && ($page==($i-3) || $page==($i+3))) $htmlset .= "...&nbsp;";
			if($i <= 2 || $i >= $showPages-1 || ($page>=$i-2 && $page<=$i+2)) {
			$htmlset .= urlHelper::makeLink('crmIntern', 'showPersonsByStatus', $i, array('status'=>$_GET['status'],'page'=>$i));
			$htmlset .= "&nbsp;";
			}
		}

	}
$htmlset .= "</div>";
}

if(isset($page)) $replCode .= $htmlset;
$replCode .= "\n".'<div class="dataTable">';

$this->counter = 0;
while($per = $personRs->fetch()) {

	$c = $contract->getByPid($per['pid']);
	$t = $wzm->getTariff($c['idt']);
	$contp = $crm->getContactPrimary('pid', $per['pid']);
	

	$addrp = $crm->getAddressPrimary('pid', $per['pid']);

	$tableRowData = array('firstcell' => $per['surname'].', '.$per['forename'],
		'pid' => $per['pid'],
		'birthdate' => $per['birthdate'],

		'street' => $addrp['street'],
		'postcode' => $addrp['postcode'],
		'city' => $addrp['city'],

		'phone' => $contp['phone'],
		'fax' => $contp['fax'],
		'email' => $contp['email'],
		'homepage' => $contp['homepage'],

		'idco' => $c['idco'], 

		'tariff' => $t['name'],

		'status' => $c['contractStatusLid']

	);
	$replCode .= $this->getTableRow($tableRowData);
	$this->counter++;
}
$replCode .= "\n".'</div>';

// no results
if($this->counter == 0) {
	$replCode .= $this->getp(L::_(69));
}

// finish
$this->replace('content', $replCode);

}


private function getTableRow($data) {
	$rowClass = ($this->counter % 2)?'rowOdd':'rowEven';

	$jsTarget = urlHelper::makeCoreUrl('crmIntern',	'showPerson',
		array('pid' => $data['pid']));

	return "\n".'
	<div class="'.$rowClass.'" onClick="location.href = \''.$jsTarget.'\';">
	<div class="status"><span class="s'.$data['status'].'">&nbsp;</span></div>
	<div class="name"><span class="company">'.urlHelper::makeLink('crmIntern', 'showPerson', $this->html($data['firstcell'],1), array('pid' => $data['pid'])).'</span></div>
<!--	<div class="birthdate"><span class="company">'.$this->html($data['birthdate'],1).'</span></div> -->
	<div class="street"><span class="company">'.$this->html($data['street'],1).'</span></div>
	<div class="postcode"><span class="company">'.$this->html($data['postcode'],1).'</span></div>
	<div class="city"><span class="company">'.$this->html($data['city'],1).'</span></div>
	<div class="phone"><span class="company">'.$this->html($data['phone'],1).'</span></div>
	<div class="postcode"><span class="company">'.$this->html('#'.$data['idco'],1).'</span></div>
	<div class="group"><span class="company">'.$this->html($data['tariff'],1).'</span></div>
	<div class="clearBoth"></div>
	</div>
	<!-- end row -->
	';
}

} // end class

?>