<?php

class crmInternViewSaveFile
	extends myInternView
{

function __construct($dataArray=null) {
	parent::__construct();
	$this->dataArray = $dataArray;
}

function processData() {

$file = $this->dataArray['file'];
$comment = $this->dataArray['comment'];

$replCode = '';
$possibilites = '';

// text
$replCode .= $this->geth1(L::_(142));
$replCode .= $this->getp(L::_(143));

if(!empty($comment)) {
	$replCode .= $this->getp($comment);
}

$replCode .= $this->getp('<a href="../temp/'.$file.'">'.$file.'</a>');

// finish
$this->replace('content', $replCode);

}

} // end class

?>