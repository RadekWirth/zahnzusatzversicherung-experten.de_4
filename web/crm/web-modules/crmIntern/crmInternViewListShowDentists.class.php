<?php

class crmInternViewListShowDentists
	extends crmInternView
{

function __construct($dataArray=null) {
	parent::__construct();
	$this->dataArray = $dataArray;
}

function processData() {
$replCode = '';
$possibilites = '';

// text
$replCode .= $this->geth1(L::_(354));
$replCode .= $this->getp(L::_(355));

// data
$wzm = new wzm();
$person = new person();
$rs = $wzm->getDentists();

$replCode .= "\n".'<div class="dataTable">';
$this->counter=0;
while($row = $rs->fetch()) {
	$p = $person->get($row['pid']);
	$data = array('firstcell' => $row['name'], 'street' => $row['street'],
		'postcode' => $row['postcode'], 'city' => $row['city'],
		'person' => $p['forename'].' '.$p['surname'], 'pid' => $p['pid'],
		'idd' => $row['idd'], 'comment' => $row['comment']);
	if(empty($data['comment'])) {
		$data['comment'] = L::_(356);
	}
	$replCode .= $this->getTableRow($data);
	$this->counter++;
}
$replCode .= "\n".'</div>';

// finish
$this->replace('content', $replCode);

}

private function getTableRow($data) {
	$rowClass = ($this->counter % 2)?'rowOdd':'rowEven';

	return "\n".'
	<div class="'.$rowClass.'">
	<div class="name" onclick="javascript:showHide(\'comment'.$data['idd'].'\')"><a class="showHide">'.$this->html($data['firstcell'],1).'</a></div>
	<div class="street"><span class="company">'.$this->html($data['street'],1).'</span></div>
	<div class="postcode"><span class="company">'.$this->html($data['postcode'],1).'</span></div>
	<div class="city"><span class="company">'.$this->html($data['city'],1).'</span></div>
	<div class="name"><span class="company">'.urlHelper::makeLink('crmIntern', 'showPerson', $this->html($data['person'],1), array('pid' => $data['pid'])).'</span></div>
	<div class="clearBoth"></div>
	<p style="display:none" id="comment'.$data['idd'].'">'.$this->html($data['comment']).'</p>
	</div>
	<!-- end row -->
	';
}

} // end class

?>