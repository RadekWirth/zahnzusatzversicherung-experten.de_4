<?php

class crmInternViewAddIntActivity
	extends myInternView
{

function __construct($dataArray=null) {
	parent::__construct();
	$this->dataArray = $dataArray;
}

function processData() {
// get data for prefilling fields
$data = (empty($this->dataArray['data']))?array():$this->dataArray['data'];

$label = new label();
$crm = new crm();

$replCode = '';
$possibilites = '';
$type = $this->dataArray['type'];
$id = $this->dataArray['id'];

$replCode .= $this->geth1(L::_(163));

// generate options for assignmentStatus
$intActivityOptionsRs = $label->getLabels('intActivity');
$intActivityOptions = array();
while($row = $intActivityOptionsRs->fetch()) {
	$intActivityOptions[$row['lid']] = $row['name'];
}


$continentOptions = array();
$continents = array('africa', 'america', 'asia','australia', 'europe');
foreach($continents as $c) {
	$continentOptions[$c] = L::_($c);
}

// create field sets
$fieldsets =
array(
	array(
		'legend' => L::_(164),
		'group' => 'intActivity',
		'fields' => array(
			array(
			'name' => 'continent',
			'label' => L::_(165),
			'type' => 'select',
			'size' => 'medium',
			'options' => $continentOptions,
			'selected' => $data['intActivity']['continent']
			),
			array(
			'name' => 'intActivity',
			'label' => L::_(166),
			'type' => 'select',
			'size' => 'medium',
			'options' => $intActivityOptions,
			'selected' => $data['intActivity']['intActivity']
			)
		)
	),
	array(
		'params' => 'disableGroups',
		'fields' => array(
			array(
			'value' => L::_(43),
			'name' => 'submit',
			'type' => 'submit',
			'class' => 'submit3',
			'params' => 'hideLabel'
			)
		)
	)
);

$formEngine = new coreFormEngine();
$replCode .= $formEngine->requestForm($fieldsets, 'crmIntern',
	'addIntActivity', array('type' => $type, 'id' => $id));

// finish
$this->replace('content', $replCode);

}

} // end class

?>
