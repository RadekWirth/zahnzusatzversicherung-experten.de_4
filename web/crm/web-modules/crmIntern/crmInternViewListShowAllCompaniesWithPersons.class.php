<?php

class crmInternViewListShowAllCompaniesWithPersons
	extends crmInternView
{

private $counter;

function __construct($dataArray=null) {
	parent::__construct();
	$this->dataArray = $dataArray;
}

function processData() {
// get data for prefilling fields
$data = (empty($this->dataArray['data']))?array():$this->dataArray['data'];

$replCode = '';
$possibilites = '';
$firstLetter = null;
if(!empty($this->dataArray['firstLetter'])) {
	$firstLetter = $this->dataArray['firstLetter'];
}

$label = new label();
$crm = new crm();
$company = new company();
$person = new person();

// companies
$replCode .= $this->geth1(L::_(52));
$replCode .= "\n".'<div class="dataTable">';
// $replCode .= "\n".'<table cellpadding="0" cellspacing="0" class="dataTable">';
$companyRs = $company->getAllRs($firstLetter);
$this->counter = 0;
while($com = $companyRs->fetch()) {
	$addrp = $crm->getAddressPrimary('cid', $com['cid']);
	$contp = $crm->getContactPrimary('cid', $com['cid']);
	$tableRowData = array('name' => $com['name'], 'street' => $addrp['street'],
		'postcode' => $addrp['postcode'], 'city' => $addrp['city'],
		'phone' => $contp['phone'], 'fax' => $contp['fax'],
		'email' => $contp['email'], 'homepage' => $contp['homepage'],
		'group' => $crm->getGroupName('cid', $com['cid']),
		'cid' => $com['cid']);
	$tableRowData['firstcell'] = urlHelper::makeLink('crmIntern',
		'showCompany', $this->html($com['name']), array('cid' => $com['cid']));

 	$tableRowData['importantNotice'] = $crm->hasImportantNotes('cid',
 		$com['cid']);

	$tableRowData['assignments'] = $crm->hasAssignments('cid', $com['cid']);
	$tableRowData['persons'] = $person->getByCompanyFullArray($com['cid']);
	$replCode .= $this->getTableRow(&$tableRowData);
	$this->counter++;
}
// $replCode .= "\n".'</table>';
$replCode .= "\n".'</div>';

// no results
if($this->counter == 0) {
	$replCode .= $this->getp(L::_(69));
}

// finish
$this->replace('content', $replCode);

}


private function getTableRow($data) {
	$rowClass = ($this->counter % 2)?'rowOdd':'rowEven';

	return "\n".'
	<div class="'.$rowClass.'" >
	<div class="name"><span class="company">'.$data['firstcell'].'</span></div>
	<div class="importantNotice"><span class="company">'.(($data['importantNotice'])?'<img src="img/notice.png" alt="notice" />':'&nbsp;').'</span></div>
	<div class="assignments"><span class="company">'.(($data['assignments'])?'<img src="img/icons/copy.gif" alt="assignments" />':'&nbsp;').'</span></div>
	<div class="street"><span class="company">'.$this->html($data['street'],1).'</span></div>
	<div class="postcode"><span class="company">'.$this->html($data['postcode'],1).'</span></div>
	<div class="city"><span class="company">'.$this->html($data['city'],1).'</span></div>
	<div class="phone"><span class="company">'.$this->html($data['phone'],1).'</span></div>
	<div class="group"><span class="company">'.$this->html($data['group'],1).'</span></div>
	<div class="clearBoth"></div>
	<div>
	'.$this->getTablePersonRows($data['persons'], $data['cid']).'
	</div>
	</div>
	<!-- end row -->
	';
}


private function getTablePersonRows($persons, $cid) {
	$ret = '';
	$crm = new crm();
	$user = new user();

	if(count($persons))
	foreach($persons as $p) {
	$jsTarget = urlHelper::makeCoreUrl('crmIntern',	'showCompany',
		array('cid' => $cid), 'pid-'.$p['person']['pid']);

	$noteText = '';
	$note = $crm->getLatestNote('pid', $p['person']['pid']);
	if(!empty($note)) {
		$noteText = $note['creationDate'].' - '.
			$user->getNameByUid($note['uid']);
	}

	if(!empty($p['contact']['email'])) {
		$max = 24;
		$mailText = $p['contact']['email'];
		if(strlen($mailText) > $max && (strlen($mailText)-$max)>3 ) {
			$mailText = substr($mailText, 0, $max).'...';
		}
		$p['contact']['email'] = urlHelper::makeLink('crmIntern', 'sendMail',
			$mailText, array('type' => 'pid', 'id' => $p['person']['pid'],
			'email' => $p['contact']['email']));
	}else {
		$p['contact']['email'] = '&nbsp';
	}

 	$hasNotes = $crm->hasImportantNotes('pid', $p['person']['pid']);

	$ret .= '
	<div class="rowPerson" onClick="location.href = \''.$jsTarget.'\';">
	<div class="name"><span class="person">'.$this->html($p['person']['surname'].', '.$p['person']['forename'],1).'</span></div>
	<div class="importantNotice"><span class="company">'.(($hasNotes)?'<img src="img/notice.png" alt="notice" />':'&nbsp;').'</span></div>
	<div class="assignments"><span class="company">'.(($data['assignments'])?'<img src="img/icons/copy.gif" alt="assignments" />':'&nbsp;').'</span></div>
	<div class="streetpost"><span class="person">'.$this->html($noteText,1).'</span></div>
	<div class="city"><span class="person">'.$p['contact']['email'].'</a></span></div>
	<div class="phone"><span class="person">'.$this->html($p['contact']['phone'],1).'</span></div>
 	<div class="group"><span class="person">'.$this->html($p['person']['jobPositionId'],1).'</span></div>
	<div class="clearBoth"></div>
	</div>
	<!-- end person -->
	';
	}
	return $ret;
}

} // end class

?>