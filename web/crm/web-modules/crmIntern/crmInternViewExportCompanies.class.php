<?php

class crmInternViewExportCompanies
	extends myInternView
{

function __construct($dataArray=null) {
	parent::__construct();
	$this->dataArray = $dataArray;
}

function processData() {
// get data for prefilling fields
$data = (empty($this->dataArray['data']))?array():$this->dataArray['data'];

$label = new label();
$crm = new crm();

$replCode = '';

$replCode .= $this->geth1(L::_(138));

if(isset($this->dataArray['emptyResult'])) {
	$replCode .= $this->getp(L::_(144));
}

// generate options for companySize
$companySizeRs = $label->getLabels('companySize', 'lid');
$companySizOptions = array(0 => L::_(25));
if($companySizeRs)
while($row = $companySizeRs->fetch()) {
	$companySizOptions[$row['lid']] = $row['name'];
}

// generate options for branches
$branchesRs = $crm->getBranchesRs();
$branchOptions = array(0 => L::_(25));
if($branchesRs)
while($row = $branchesRs->fetch()) {
	$branchOptions[$row['bid']] = $row['name'];
}

// generate options for states
$stateRs = $crm->getStatesRs();
$stateOptions = array(0 => L::_(25));
if($stateRs)
while($row = $stateRs->fetch()) {
	$stateOptions[$row['sid']] = $row['name'];
}

// generate options for country
$countryRs = $crm->getCountries();
$countryOptions = array('0' => L::_(25));
if(!empty($countryRs))
while($row = $countryRs->fetch()) {
	$countryOptions[$row['code']] = $row['value'];
}

if(empty($data['address']['country'])) {
	$data['country'] = 'DEU';
}

// continents
$continentOptions = array(0 => L::_(25));
$continents = array('northamerica','southamerica','europe','asia','australia');
foreach($continents as $c) {
	$continentOptions[$c] = L::_($c);
}


$data['fieldsCompany']['name'] = 1;
$data['fieldsCompany']['address'] = 1;
$data['fieldsCompany']['contact'] = 1;
$data['fieldsCompany']['companySize'] = 1;
$data['fieldsCompany']['branch'] = 1;

$data['fieldsPerson']['name'] = 1;
$data['fieldsPerson']['address'] = 1;
$data['fieldsPerson']['contact'] = 1;
$data['fieldsPerson']['mapping'] = 1;

$data['export']['format'] = 'csv';

// create field sets
$fieldsets =
array(
	array(
		'legend' => L::_(139),
		'group' => 'selection',
		'fields' => array(
			array(
			'name' => 'justIntActivities',
			'type' => 'checkbox',
			'label' => L::_(168),
			'options' => array('1' => ''),
			'class' => 'checkbox',
			'selected' => $data['selection']['justIntActivities']
			),
			array(
			'name' => 'continent',
			'label' => L::_(165),
			'type' => 'select',
			'size' => 'big',
			'options' => $continentOptions,
			'selected' => $data['selection']['continent']
			),
			array(
			'label' => L::_(141),
			'name' => 'keywords',
			'type' => 'text',
			'size' => 'big',
			'value' => $data['keywords']
			),
			array(
			'label' => L::_(13),
			'name' => 'companySizeLid',
			'type' => 'select',
			'size' => 'big',
			'options' => $companySizOptions,
			'selected' => $data['companySizeLid']
			),
			array(
			'label' => L::_(17),
			'name' => 'branchId',
			'type' => 'select',
			'size' => 'big',
			'options' => $branchOptions,
			'selected' => $data['branchId']
			),
			array(
			'label' => L::_(33),
			'name' => 'postcode',
			'type' => 'text',
			'size' => 'medium-big',
			'maxlength' => 10,
			'value' => $data['postcode']
			),
			array(
			'label' => L::_(19),
			'name' => 'stateId',
			'type' => 'select',
			'size' => 'big',
			'options' => $stateOptions,
			'selected' => $data['statdeId']
			),
			array(
			'label' => L::_(35),
			'name' => 'country',
			'type' => 'select',
			'size' => 'big',
			'options' => $countryOptions,
			'selected' => $data['country']
			)
		)
	),
	array(
		'legend' => L::_(148),
		'group' => 'fieldsCompany',
		'fields' => array(
			array(
			'name' => 'name',
			'type' => 'checkbox',
			'options' => array('1' => L::_(10)),
			'class' => 'checkbox',
			'params' => 'hideLabel',
			'selected' => $data['fieldsCompany']['name']
			),
			array(
			'name' => 'address',
			'type' => 'checkbox',
			'options' => array('1' => L::_(31)),
			'class' => 'checkbox',
			'params' => 'hideLabel',
			'selected' => $data['fieldsCompany']['address']
			),
			array(
			'name' => 'contact',
			'type' => 'checkbox',
			'options' => array('1' => L::_(36)),
			'class' => 'checkbox',
			'params' => 'hideLabel',
			'selected' => $data['fieldsCompany']['contact']
			),
			array(
			'name' => 'companySize',
			'type' => 'checkbox',
			'options' => array('1' => L::_(13)),
			'class' => 'checkbox',
			'params' => 'hideLabel',
			'selected' => $data['fieldsCompany']['companySize']
			),
			array(
			'name' => 'branch',
			'type' => 'checkbox',
			'options' => array('1' => L::_(17)),
			'class' => 'checkbox',
			'params' => 'hideLabel',
			'selected' => $data['fieldsCompany']['branch']
			)
		)
	),
	array(
		'legend' => L::_(149),
		'group' => 'fieldsPerson',
		'fields' => array(
			array(
			'name' => 'name',
			'type' => 'checkbox',
			'options' => array('1' => L::_(10)),
			'class' => 'checkbox',
			'params' => 'hideLabel',
			'selected' => $data['fieldsPerson']['name']
			),
			array(
			'name' => 'address',
			'type' => 'checkbox',
			'options' => array('1' => L::_(31)),
			'class' => 'checkbox',
			'params' => 'hideLabel',
			'selected' => $data['fieldsPerson']['address']
			),
			array(
			'name' => 'contact',
			'type' => 'checkbox',
			'options' => array('1' => L::_(36)),
			'class' => 'checkbox',
			'params' => 'hideLabel',
			'selected' => $data['fieldsPerson']['contact']
			),
			array(
			'name' => 'mapping',
			'type' => 'checkbox',
			'options' => array('1' => L::_(110)),
			'class' => 'checkbox',
			'params' => 'hideLabel',
			'selected' => $data['fieldsPerson']['mapping']
			)
		),
	),
	array(
		'legend' => L::_(151),
		'group' => 'export',
		'fields' => array(
			array(
			'name' => 'format',
			'type' => 'radio',
			'options' => array('pdf' => L::_(152), 'csv' => L::_(153)),
			'class' => 'checkbox',
			'params' => 'hideLabel',
			'selected' => $data['export']['format']
			)
		)
	),
	array(
		'params' => 'disableGroups',
		'fields' => array(
			array(
			'value' => L::_(140),
			'name' => 'submit',
			'type' => 'submit',
			'class' => 'submit3',
			'params' => 'hideLabel',
			'onclick' => 'javascript:handleExportClick()',
			)
		)
	)
);

$formEngine = new coreFormEngine();
$replCode .= $formEngine->requestForm($fieldsets, 'crmIntern',
	'exportCompanies', array());

// finish
$this->replace('content', $replCode);

}

} // end class

?>