<?php

class crmInternController {

private $login;
private $path;

// -----------------------------------------------------------------------------
public function __construct($path_to_live=null) {
	$this->path = $path_to_live;
}
// -----------------------------------------------------------------------------
public function setFakeUser()
{
	// needed for jobs 
	$this->login = 'fake';
}
// -----------------------------------------------------------------------------
public function handleEvent($event) {

if(!$this->login)
{
    // check Login
    $user = new user();

    if(!$user->checkLogin()) {
      	 return array('forward' => array('classModule' => 'crmLogin',
            'event' => 'login'));
      
      	 return array('err' => 3, 'errmsg' => 'login session expired',
            'errclass' => 'intern');
    }
}


    // get Login user data
    $this->login = S::get('user', 'login');

// echo "asd $event"; exit;
    switch($event) {

        case 'home':
            $ret = $this->home();
            break;

        case 'logout':
            $ret = $this->logout();
            break;

        case 'search':
            $ret = $this->search($_POST['search']);
            break;

        case 'filterDate':
            //print_r($_POST);
            $ret = $this->filterDate($_POST);
            break;

        case 'editContract':
            $ret = $this->editContract($_GET['idco']);
            break;

        case 'editContractTeethData':
            $ret = $this->editContract($_GET['idco'], $type='teeth');
            break;

        case 'createAdviceProtocol':
            $ret = $this->createAdviceProtocol($_GET['pid']);
            break;

        case 'createCoverLetter':
            $ret = $this->createCoverLetter($_GET['pid']);
            break;

        case 'addIntActivity':
            $ret = $this->addIntActivity($_GET['type'], $_GET['id']);
            break;

        case 'deleteIntActivity':
            $ret = $this->deleteIntActivity($_GET['type'], $_GET['id'],
                $_GET['entryId']);
            break;

        case 'listShowAssignments':
            $ret = $this->listShowAssignments();
            break;

        case 'listShowImportancies':
            $ret = $this->listShowImportancies();
            break;

	 case 'amendDetails':
	     $ret = $this->amendDetails();
	     break;

	 case 'amendDetails2':
	     $ret = $this->amendDetails2();
	     break;

        case 'sendWaitingInvoices':
            $ret = $this->sendWaitingInvoices();
            break;
	
        case 'listShowInvoices':
            $ret = $this->listShowInvoices($_GET['viewStatus']);
            break;

        case 'listShowInvoiceItems':
            $ret = $this->listShowInvoiceItems();
            break;

        case 'generateInvoices':
            $ret = $this->generateInvoices();
            break;

        case 'setInvoiceStatus':
            $ret = $this->setInvoiceStatus($_GET['iid'], $_GET['status']);
            break;

        case 'setInvoiceType':
            $ret = $this->setInvoiceType($_GET['iid'], $_GET['type']);
            break;

        case 'getInvoicePdf':
            $iid = $_REQUEST['iid'];
            $ret = $this->getInvoicePdf($iid);
            break;

        case 'importNewData':
            $ret = $this->importNewData();
            break;

        case 'addAssignment':
            $type = $_REQUEST['type'];
            $id = $_REQUEST['id'];
            $amid = $_REQUEST['amid'];
            $ret = $this->addAssignment($type, $id, $amid);
            break;

        case 'delAssignment':
            $type = $_REQUEST['type'];
            $id = $_REQUEST['id'];
            $amid = $_REQUEST['amid'];
            $ret = $this->delAssignment($type, $id, $amid);
            break;

        case 'setAssignmentStatus':
            $pid = $_REQUEST['pid'];
            $cid = $_REQUEST['cid'];
            $amid = $_REQUEST['amid'];
            $status = $_REQUEST['status'];
            $ret = $this->setAssignmentStatus($pid, $cid, $amid, $status);
            break;

        case 'generatePeriodicalItems':
            $amid = $_REQUEST['amid'];
            $ret = $this->generatePeriodicalItems($amid);
            break;

        case 'generateReminderLetters':
            $date = $_REQUEST['date'];
            $ret = $this->generateReminderLetters($date);
            break;

        case 'editCompany':
        case 'addCompany':
            $cid = $_REQUEST['cid'];
            $ret = $this->addCompany($cid);
            break;

        case 'deleteCompany':
            $ret = $this->deleteCompany($_REQUEST['cid'],
                $_REQUEST['sure']);
            break;


        case 'editPerson':
        case 'addPerson':
            $pid = $_REQUEST['pid'];
            $cid = $_REQUEST['cid'];
            $ret = $this->addPerson($pid, $cid);
            break;

        case 'deletePerson':
            $ret = $this->deletePerson($_REQUEST['pid'],
                $_REQUEST['sure']);
            break;

        case 'showPerson':
            $pid = $_REQUEST['pid'];
	     $idco = $_REQUEST['idco'];
            if(empty($pid)) {
                $pid = $_REQUEST['entryId'];
            }
	     if($_REQUEST['idco'])
	     {
		$ret = $this->showPersonByIdco($idco); break;
	     } else
              $ret = $this->showPerson($pid); break;


        case 'listShowAll':
            $ret = $this->listShowAll(); break;

        case 'listShowAllCompanies':
            $ret = $this->listShowAllCompanies(); break;

        case 'listShowAllCompaniesWithPersons':
            $ret = $this->listShowAllCompaniesWithPersons(
                $_REQUEST['letter']); break;

        case 'listShowAllPersons':
            $ret = $this->listShowAllPersons(
                $_REQUEST['letter']); break;

        case 'showPersonsByStatus':
            $ret = $this->showPersonsByStatus(
                $_REQUEST['status'], $_REQUEST['page']); break;

        case 'showCompany':
            $cid = $_REQUEST['cid'];
            $ret = $this->showCompany($cid); break;


        case 'addNote':
            $ret = $this->addNote($_REQUEST['type'], $_REQUEST['id'],
                $_REQUEST['text'], $_REQUEST['importance']);
            break;

        case 'delNote':
            $ret = $this->delNote($_REQUEST['nid'], $_REQUEST['type'],
                $_REQUEST['id']);
            break;

        case 'newContract':
            if($_REQUEST['idt']>0 || $_REQUEST['idtNotPossible']>0)
                $ret = $this->addNewContractWithMail($_REQUEST['pid'], $_REQUEST['idco'],
                    $_REQUEST['idt'], $_REQUEST['idtNotPossible'], $_REQUEST['mail']);
            break;

        case 'newContractFromMiddleware':
            $ret = $this->addNewContractFromMiddleware();
            break;

        case 'exportCompanies':
            $ret = $this->exportCompanies();
            break;

        case 'sendMail':
            $ret = $this->sendMail($_REQUEST['type'], $_REQUEST['id'],
                $_REQUEST['email']);
            break;

        case 'ajaxGetPersonsByCompany':
            $this->ajaxGetPersonsByCompany($_REQUEST['cid']);
        break;

        case 'logout':
            $ret = $user->logout();
            return array('view' => new crmInternViewLogout());
            break;

        case 'getContractPdf':
            $ret = $this->getContractPdf($_GET['idco'], $_GET['pid']);
            break;

        case 'genContractPdfByImage':
            if($_GET['option'] && $_GET['option'] == 'amde')
            {
                $_GET['idt'] = $_GET['pdfTemplate'];
                $ret = $this->amendDetails();
            } else
                $ret = $this->genContractPdfByImage($_GET['idco'], $_GET['pid'],
                     $_GET['pdfTemplate'], $_GET['complete'], $_GET['reset'], $_GET['count'], $_GET['statusChange'], $_GET['benefitDescription']);
            break;

        case 'genContractPdf':
            $ret = $this->genContractPdf($_GET);
            break;

	    case 'genRefundApplication':
		    $ret = $this->genRefundApplication($_GET['idco'], $_GET['prefilled']);
		    break;

        case 'genBenefitsLongPdf':
            $ret = $this->genBenefitsLongPdf($_GET['pid']);
            break;

        case 'createCssChangeLetters':
            $ret = $this->createCssChangeLetters($limit=25);
            break;    

        case 'setStatus':
            $ret = $this->setStatus($_GET['statusLabelName'],
                $_GET['entityId'], $_POST['status'], $_POST['date'], $_POST['mail'], $_GET['pid']);
            break;

        case 'printStatus':
		print_r($_REQUEST);
            break;

        case 'showDentists':
		return array('view' => new crmInternViewListShowDentists());
		break;

	 case 'addTariffDetails':
		$ret = $this->duplicateDetails($_GET['duplicateDetailsSource'], $_GET['duplicateDetailsAim'], $_GET['returnToPid']);
	 	break;

        case 'gothaer-station':
		$ret = $this->showGothaerStationaerData($_POST['age']);
		break;

	 case 'genSpecialContractPdfByImage':
		if($_GET['pid'] && $_GET['idco'] && $_GET['pdfTemplate'])
		{
			$this->genSpecialContractPdfByImage(array('pid' => $_GET[pid], 'idco' => $_GET[idco], 'pdfTemplate' => $_GET[pdfTemplate]));
		}
		break;

	 case 'newMailForUnsentContracts':
	     $ret = $this->newMailForUnsentContracts();
	     break;

        default:
            return array('err' => 3, 'errmsg' => 'event could not be handled',
                'errclass' => 'crmInternController', 'errevent' => $event);
    }

    return $ret;
}
// -----------------------------------------------------------------------------
private function home($datum=null) {
    $view = new crmInternViewHome($datum);
    return array('view' => $view);
}
// -----------------------------------------------------------------------------
private function logout() {
    $user = new user();
    $user->logout();
    return array('forward' => array('classModule' => 'crmIntern',
        'event' => 'login'));
}
// -----------------------------------------------------------------------------
private function showPersonsByStatus($status, $page) {
    $contract = new contract();
    return array('view' => new crmInternViewListShowAllPersons(
        array('personIds' => $contract->getPidsByStatus($status, $page))));
}
// -----------------------------------------------------------------------------
private function search($search) {
    $crmIntern = new crmIntern();
    $result = $crmIntern->search($search);
    if(empty($result)) {
        return array('view' => new crmInternViewMessage(array('caption' =>
            L::_(359), 'text' => L::_(144))));
    }
    if(count($result)==1) {
        return array('forward' => array('classModule' => 'crmIntern',
            'event' => 'showPerson', 'parameter' =>array('pid' => $result[0])));
    }
    return array('view' => new crmInternViewListShowAllPersons(
        array('personIds' => $result)));
}

private function duplicateDetails($duplicateDetailsSource, $duplicateDetailsAim, $returnToPid) {
	
	if(isset($duplicateDetailsSource) && isset($duplicateDetailsAim)) {
		$wzm = new wzm();
		$status = $wzm->duplicateDetails($duplicateDetailsSource, $duplicateDetailsAim);
		#if($status) print $status;
	}
	
	if(isset($returnToPid)) {
		return  array('forward' => array('classModule' => 'crmIntern',
            'event' => 'showPerson', 'parameter' => array('pid' => $returnToPid)));
	} else return $this->home();
}


// -----------------------------------------------------------------------------
private function filterDate($date) {
    //$crmIntern = new crmIntern();

    if(is_array($date))
    {
        return array('forward' => array('classModule' => 'crmIntern',
            'event' => 'home', 'parameter' => $date));

    } else return array('view' => new crmInternViewListShowAllPersons(
            array('personIds' => $result)));
}

// -----------------------------------------------------------------------------
private function setStatus($statusLabelName, $entityId, $status, $date, $mail, $pid) {
    if($statusLabelName=='contractStatus') {
        $contract = new contract();
	 $crmIntern = new crmIntern();

        $contract->setStatus($entityId, $status, $date);
        // Antrag zurueck
        if($status == 49 && $mail == 'mail') {
            $ret = $crmIntern->sendMailContractArrived($pid);
            if(is_array($ret)) {
                //print "Error - notification mail not sent!";
                //print "Error: ".$ret[1];
            }
        }
	if($pid == 44424)
		$ret = $crmIntern->sendMailContractArrived($pid);
    }
    return array('forward' => array('classModule' => 'crmIntern',
        'event' => 'showPerson', 'parameter' => array('pid' => $pid)));
}

private function editContract($idco, $type=null) {
    if(empty($idco)) {
        return null;
    }
    // get data delivered by formEngine
    $data = coreFormEngine::getReceivedData();
    $contract = new contract();
    if(empty($data)) {
        return array('view' => new crmInternViewEditContract(array('data' =>
            array('idco' => $idco, 'type' => $type, 'contract' => $contract->get($idco)))));
    }
    else {

        // update contract
        $contract->set($idco, $data['contract']);

        // forward
        $c = $contract->get($idco);

        return array('forward' => array('classModule' => 'crmIntern',
        'event' => 'showPerson', 'parameter' => array('pid' => $c['pid'])));
    }
}
// -----------------------------------------------------------------------------
private function listShowAll() {
    $view = new crmInternViewListShowAll();
    return array('view' => $view);
}
private function listShowAllCompanies() {
    $view = new crmInternViewListShowAllCompanies();
    return array('view' => $view);
}
private function listShowAllCompaniesWithPersons($letter) {
    $view = new crmInternViewListShowAllCompaniesWithPersons(array(
        'firstLetter' => $letter));
    return array('view' => $view);
}
private function listShowAllPersons($letter) {
    $view = new crmInternViewListShowAllPersons(array(
        'firstLetter' => $letter));
    return array('view' => $view);
}
// -----------------------------------------------------------------------------
private function showCompany($cid) {
    $view = new crmInternViewShowCompany(array('cid' => $cid));
    return array('view' => $view);
}

private function showPerson($pid) {
    $view = new crmInternViewShowPerson(array('pid' => $pid));
    return array('view' => $view);
}

private function showPersonByIdco($idco) {
    $c = new contract();
    $con = $c->get($idco);
    $view = new crmInternViewShowPerson(array('pid' => $con['pid']));
    return array('view' => $view);
}

// -----------------------------------------------------------------------------
private function listShowAssignments() {
    $view = new crmInternViewListShowAssignments();
    return array('view' => $view);
}
// -----------------------------------------------------------------------------
private function listShowImportancies() {
    $view = new crmInternViewListShowImportancies();
    return array('view' => $view);
}
// -----------------------------------------------------------------------------
private function amendDetails () {
    $view = new crmInternViewAmendDetails();
    return array('view' => $view);
}
// -----------------------------------------------------------------------------
private function amendDetails2 () {
    $view = new crmInternViewAmendDetails2();
    return array('view' => $view);
}
private function showGothaerStationaerData($age) {
    $view = new crmInternViewSpecialInsurance($age);
    return array('view' => $view);

}

private function genSpecialContractPdfByImage($data)
{
	/* update data set */
	$c = new contract();
	$c->addSpecialOffer($data);
	
	header('Location: controller.php?cm=crmIntern&event=genContractPdfByImage&pid='.$data[pid].'&idco='.$data[idco].'&pdfTemplate=279');
}
// -----------------------------------------------------------------------------
// CONTRACT PDF EVENTS
// -----------------------------------------------------------------------------
private function genContractPdf($get)
{

    $idco = $get['idco'];
    $pid = $get['pid'];
    $sourcePage = $get['sourcePage'];
    $pdfTemplate = null;
        $split = explode('_' , $get['complete']);
        $benefitDescription = $split[0];
        $complete = $split[1];
        $reset = $split[2];

    $crmIntern = new crmIntern();

    #$array = array('idco' => $idco, 'pid' => $pid, 'pdfTemplate' => $pdfTemplate, 'complete' => $complete, 'reset' => $reset, 'count' => 1, 'statusChange' => null, 'benefitDescription' => $benefitDescription);
    #print_r($array);

    #$this->genContractPdfByImage($idco, $pid, $pdfTemplate, $complete, $reset, 1, null, $benefitDescription); 
    
    $pdf = $crmIntern->genContractPdfByImage($idco, $pdfTemplate, $complete, $reset, null, null, $benefitDescription, $sourcePage);
    if(!empty($pdf))
    {
        $crmIntern->outputPDF($pdf);
    } else print "crmInternController.class.php::genContractPdf says no data! <br /><a href='javascript:history.back()'>back</a>";
}



private function genContractPdfByImage($idco, $pid=null, $pdfTemplate=null, $complete=null, $reset=null, $count=1, $statusChange=null, $benefitDescription=null) {

    $crmIntern = new crmIntern();
    $contract = new contract();
    $person = new person();
    $pdf = null;

    if(!isset($pid)) {
        $contractData = $contract->get($idco);
        $pid = $contractData['pid'];
    }

    $crmIntern = new crmIntern();

    if($count>=1)
    {
        $openIdco = $contract->getOpenIdco($count);
        if($openIdco) {
            foreach ($openIdco as $key=>$idco)
            {
            $pdf = $crmIntern->genContractPdfByImage($idco, $pdfTemplate, $complete, $reset, $pdf);
            $crmIntern->outputPDF($pdf);
            $pdf = null;
            if($statusChange==1)
                $contract->setStatus($idco, 48);
        }}
    } else
        $pdf = $crmIntern->genContractPdfByImage($idco, $pdfTemplate, $complete, $reset, null, null, $benefitDescription);
    if(!empty($pdf))
    {
        $crmIntern->outputPDF($pdf);
    } else print "crmInternController.class.php::genContractPdfByImage says no data! <br /><a href='javascript:history.back()'>back</a>";
}

// -----------------------------------------------------------------------------
private function genRefundApplication($idco, $prefilled) {
	$crmIntern = new crmIntern();

	$pdf = $crmIntern->genRefundApplication($idco, $prefilled);
	$crmIntern->outputPDF($pdf);
}

// -----------------------------------------------------------------------------
private function importNewData() {
    $io = new ioHelper();
    return $io->getData();
}

// -----------------------------------------------------------------------------
private function generateReminderLetters($date) {
    $reminder = new reminder();

    $ret = $reminder->getData($date);

    $crmIntern = new crmIntern();
    $pdf = $crmIntern->genAnschreibenPDF($ret);
    $crmIntern->OutputPDF($pdf);
}

private function genBenefitsLongPdf($pid=null) {
    $crmIntern = new crmIntern();
    $contract = new contract();

    $contractData = $contract->getByPid($pid);
    $data['tariffId'] = $contractData['idt'];
    $data['series'] = $contractData['series'];
    $data['sourcePage'] = $contractData['sourcePage'];
   
    return $crmIntern->getBenefitsLongPdf($data);
}

private function getContractPdf($idco, $pid=null) {
    $crmIntern = new crmIntern();
    $contract = new contract();
    $person = new person();

    if(!empty($pid)) {
        $contractData = $contract->getByPid($pid);
        $idco = $contractData['idco'];
    }
    else {
        $contractData = $contract->get($idco);
        $pid = $contractData['pid'];
    }

    $personData = $person->get($pid);
    $fileName = $personData['surname'].'-'.$personData['forename'].'.pdf';
    $data = $crmIntern->getContractPdfData($idco);
    $file = $contract->generatePdf($data, $contractData['idt']);
    if($file!==false) {
        actionHelper::startDownloadDialog($file, $fileName);
    }
    else {
        return $this->showPerson($pid);
    }
}


// ------------------
// MIDDLEWARE 
// ------------------

private function addNewContractFromMiddleware() {
	/* get array with data from middleware */
	$c = new contract();
	
	echo "checking table middleware...";

	$middleware = $c->getOpenContractsFromMiddleware(1);

	if(!empty($middleware) && count($middleware)>0)
	{
		echo "Go and Import Data - we have ".count($middleware)." entries...";
		
		foreach((array)$middleware as $key => $value) {

			echo "\r\nKey : ".$key;
			echo "\r\nValue : ".print_r($value, true);

			$idco = $c->add($value);
			if(isset($idco) && $idco>0) {
				/* update the middleware tbl */
				$c->setMiddlewareToTransferred($value['id']);

				/* send out Mail | complete = 2, reset = 1 */
				$c->sendContractMail($idco, 2, 0);
			}
		}
	} else {
		echo "no open middleware orders - abborting...";
	}

}

private function newMailForUnsentContracts() {

$c = new contract();
$wzm = new wzm();
$crm = new crm();
$p = new person();
$internhandle = new crmIntern();

if(!$this->path)
	$this->path = project::getProjectRoot();


// nur zzv.com
$result = $c->getAllContractsNotSent('zzv-neu');

if(count($result)>0) {
	echo "it seems we have " . count($result) . " contract to send mails to. Let's go!";
	emailHelper::sendMail('info@zahnzusatzversicherung-experten.de','radek.wirth@gmail.com', 'Job - Sende zzv.com Mails', 'Es sind '.count($result).' Anträge für den Versand vorgesehen');
	
	foreach($result as $id => $idco)
	{
		echo 'Nächste idco : '.$idco.'<br />';
		$pdf = ""; 
		// try - catch
		try {
			$pdf = $internhandle->genContractPdfByImage($idco, null, 'yes', null, null, $this->path);


			$t = $wzm->getContract($idco);

			$tariff = $c->getTariff($t['idt']);
			$tariffName = $tariff['name'];
			$person = $p->get($t['pid']);
			$contact = $crm->getContactPrimary('pid', $t['pid']);

		         // send user info mail
	       	 $recipient = $contact['email'];
		        $salutation = ($person['salutationLid']==9)?
				'Sehr geehrter Herr '.$person['surname']:
				'Sehr geehrte Frau '.$person['surname'];

		        $message = str_replace('{*ANREDE_NAME*}', $salutation, file_get_contents($this->path.'/templates/mail-request-template-zzvcom.tpl'));
       		 $subject = "Ihr persönliches Angebot für eine ".$tariffName." - Zahnzusatzversicherung";

	       	 $message = str_replace("{*ADD_LIST_ITEMS*}", '<br /><h3>Im Anhang dieser Mail finden Sie folgende Dokumente:</h3><ol>'.$tariff['mail_message'].'</ol>', $message);
	
		 	 if($tariff['onlineLink'])
			 {
				$message = str_replace('{*ONLINE_SIGNUP_PARTIAL*}', file_get_contents($this->path.'/templates/mail-request-online-signup-template.tpl'), $message);
				$message = str_replace('{*ONLINE_SIGNUP_LINK*}', $tariff['onlineContractUrl'], $message);
			 } else
			 {
				$message = str_replace('{*ONLINE_SIGNUP_LINK*}', '', $message);
				$message = str_replace('{*ONLINE_SIGNUP_PARTIAL*}', '', $message);
			 }
		        $message = str_replace("{*INSURANCE_NAME*}", $tariffName, $message);
		        $message = str_replace("{*SIGNATURE*}", project::gts('html'), $message);
	       	 $message = str_replace("{*CSS_OPTION*}", "", $message);

	        
			 $internhandle->outputPDF($pdf, $idco);	        

			 $file = $this->path.'/files/pdf/Antrag - '.$idco.'.pdf';
			 emailHelper::sendFileMail(null, $recipient, $subject, $message, $file, $t['idt'], true, 'utf-8', null, $this->path);	

			 $c->setMailSentDate($idco, time());
		}
		catch(Exception $e) {
			$errors = array_merge($errors, array('idco' => $idco, 'error' => $e->getMessage()));
		}
		
	}
	if(isset($errors) && count($errors)>0)
	{
		emailHelper::sendMail('info@zahnzusatzversicherung-experten.de','radek.wirth@gmail.com', 'Job - Sende zzv.com Mails beendet mit Fehlern...', 'Es sind '.count($errors).' Fehler aufgetreten<br />'.print_r($errors, true));
		emailHelper::sendMail('info@zahnzusatzversicherung-experten.de','allgemein@vm-experten.de', 'Job - Sende zzv.com Mails beendet mit Fehlern...', 'Es sind '.count($errors).' Fehler aufgetreten<br />'.print_r($errors, true));
	} else
	{
		emailHelper::sendMail('info@zahnzusatzversicherung-experten.de','radek.wirth@gmail.com', 'Job - Sende zzv.com Mails beendet fehlerfrei...', 'Keine Daten');
	}

} else {
	echo "$c->getAllContractsNotSent retrieved no data, abborting";
}

}


// -----------------------------------------------------------------------------
// INT ACTIVITY EVENTS
// -----------------------------------------------------------------------------
private function addIntActivity($type, $id) {
    // get data delivered by formEngine
    $data = coreFormEngine::getReceivedData();
    if(empty($data)) {
        $view = new crmInternViewAddIntActivity(array('type' => $type,
            'id' => $id));
        return array('view' => $view);
    }
    else {
        $crm = new crm();
        $crm->addIntActivity($type, $id, $data['intActivity']['continent'],
            $data['intActivity']['intActivity']);
        return $this->itemHome($type, $id);
    }
}

private function deleteIntActivity($type, $id, $iaid) {
    $crm = new crm();
    $crm->deleteIntActivity($iaid);
    return $this->itemHome($type, $id);
}
// -----------------------------------------------------------------------------
// INVOICE EVENTS
// -----------------------------------------------------------------------------
private function generatePeriodicalItems($amid) {
    $invoice = new invoice();
    $invoice->generatePeriodicalItems($amid);
}
private function listShowInvoices($viewStatus=null) {
    $view = new crmInternViewListShowInvoices(
        array('viewStatus' => $viewStatus));
    return array('view' => $view);
}

private function listShowInvoiceItems() {
    $invoice = new invoice();
    $invoice->generatePeriodicalItems();
    $invoice->generateOnetimeItems();
    $view = new crmInternViewListShowInvoiceItems();
    return array('view' => $view);
}

private function generateInvoices() {
    $invoice = new invoice();
    $invoice->generateInvoices();
    $view = new crmInternViewListShowInvoices();
    return array('view' => $view);
}

private function generateInvoicePdfs($iid) {
    $invoice = new invoice();
    $invoice->generateInvoicePdfs();
    $view = new crmInternViewListShowInvoices();
    return array('view' => $view);
}

private function getInvoicePdf($iid) {
    $invoice = new invoice();
    $invoice->getInvoicePdf($iid);
}

private function sendWaitingInvoices() {
    $invoice = new invoice();
    $invoice->sendWaitingInvoices();
    $view = new crmInternViewListShowInvoices();
    return array('view' => $view);
}

private function setInvoiceStatus($iid, $status) {
    $invoice = new invoice();
    $invoice->setInvoiceStatus($iid, $status);
    return $this->listShowInvoices();
}

private function setInvoiceType($iid, $type) {
    $types = array(L::_(124), L::_(157));
    $caption = $types[$type];
    if(!empty($caption)) {
        $invoice = new invoice();
        $invoice->setInvoiceCaption($iid, $caption);
    }
    return $this->listShowInvoices();
}
// -----------------------------------------------------------------------------
// ASSIGNMENT EVENTS
// -----------------------------------------------------------------------------
private function addAssignment($type, $id, $amid) {
    // get data delivered by formEngine
    $receivedData = coreFormEngine::getReceivedData();

    if(empty($receivedData)) {
        $view = new crmInternViewAddAssignment(array('type' => $type,
            'id' => $id, 'amid' => $amid));
        return array('view' => $view);
    }
    else {
        $crm = new crm();

        stringHelper::arrayDateToString($receivedData['assignment'],
            'periodStartDate');
        stringHelper::arrayDateToString($receivedData['assignment'],
            'periodEndDate');

        if(!isset($receivedData['assignment']['periodBillAhead'])) {
            $receivedData['assignment']['periodBillAhead'] = 0;
        }

        if(empty($amid)) {
            //insert
            $amid = $crm->addAssignment($type, $id,
                $receivedData['assignment']);
        }
        else {
            // update
            $crm->setAssignment($amid, $receivedData['assignment']);
        }

        if($type == 'cid') {
        return array('forward' => array('classModule' => 'crmIntern',
            'event' => 'showCompany', 'parameter' => array('cid' => $id)));
        }
        else {
        return array('forward' => array('classModule' => 'crmIntern',
            'event' => 'showPerson', 'parameter' => array('pid' => $id)));
        }
    }
}

private function delAssignment($type, $id, $amid) {
    $crm = new crm();
    $crm->delAssignment($amid);
    $cid = $id;
    if($type == 'pid') {
        $person = new person();
        $cid = $person->getCidByPid($id);
    }
    if(empty($cid)) {
    return array('forward' => array('classModule' => 'crmIntern',
        'event' => 'showPerson', 'parameter' => array('pid' => $id)));
    }
    else {
    return array('forward' => array('classModule' => 'crmIntern',
        'event' => 'showCompany', 'parameter' => array('cid' => $cid),
        'ank' => $type.'-'.$id));
    }
}

private function setAssignmentStatus($pid, $cid, $amid, $status) {
    $crm = new crm();
    $crm->setAssignmentStatus($amid, $status);
    return array('forward' => array('classModule' => 'crmIntern',
        'event' => 'showCompany', 'parameter' => array('cid' => $cid)));
}
// -----------------------------------------------------------------------------
// COMPANY EVENTS
// -----------------------------------------------------------------------------
private function addCompany($cid=0) {
    // get data delivered by formEngine
    $receivedData = coreFormEngine::getReceivedData();

    if(empty($receivedData)) {
        $view = new crmInternViewAddCompany(array('cid' => $cid));
        return array('view' => $view);
    }
    else {
        //$checkErrors =myOnlineSignup::checkCompany($receivedData);
        $checkErrors=null;
        if(count($checkErrors)) {
            $view = new crmInternViewComEditCompany(
                array('data' => $receivedData));

            $view->addToDataArray(array('errors' => $checkErrors,
                'data' => $receivedData));

            return array('view' => $view);
        }
        else {
            $receivedData['company'] = str_replace(',', '.',
                $receivedData['company']);

            $company = new company();
            $crm = new crm();
            if(empty($cid)) {
                $cid = $company->add($receivedData['company']);
                $crm->addContact($receivedData['contact'], 'cid', $cid,
                    'primary');
                $crm->addAddress($receivedData['address'], 'cid', $cid,
                    'primary');
            }
            else {
                $ret = $company->set($cid, $receivedData['company']);
                $crm->setContact($receivedData['contact'], 'cid', $cid,
                    'primary');
                $crm->setAddress($receivedData['address'], 'cid', $cid,
                    'primary');

            }
        }
        return array('forward' => array('classModule' => 'crmIntern',
            'event' => 'showCompany', 'parameter' => array('cid' => $cid)));

    }
}

private function deleteCompany($cid, $sure=false) {
    if($sure && $cid) {
        $company = new company();
        $company->del($cid);
        return array('forward' => array('classModule' => 'crmIntern',
            'event' => 'home'));
    }
    else {
        return array('view' => new crmInternViewQuestion(array(
            'caption' => L::_(101),
            'text' => L::_(102),
            'linkYes' =>
                urlHelper::makeCoreURL('crmIntern', 'deleteCompany',
                array('cid' => $cid, 'sure' => 1)),
            'linkNo' =>
                urlHelper::makeCoreURL('crmIntern', 'showCompany',
                array('cid' => $cid))
            )));

    }
    return $view;
}
// -----------------------------------------------------------------------------
// PERSON EVENTS
// -----------------------------------------------------------------------------
private function addPerson($pid=null, $cid=null) {
    // get data delivered by formEngine
    $receivedData = coreFormEngine::getReceivedData();

    if(empty($receivedData)) {
        $view = new crmInternViewAddPerson(array('pid' => $pid,
            'cid' => $cid));
        return array('view' => $view);
    }
    else {
#        $checkErrors=null;
#        if(count($checkErrors)) {
#        }
#        else {
            stringHelper::arrayDateToString($receivedData['person'],
                'birthdate');
            stringHelper::arrayDateToString($receivedData['person'],
                'insuredSince');

            $person = new person();
            $crm = new crm();
            if(empty($pid)) {
                $pid = $person->add($receivedData['person']);
                if(!empty($cid)) {
                    $person->addMapping($pid, $cid, $receivedData['mapping']);
                }
                $crm->addContact($receivedData['contact'], 'pid', $pid,
                    'primary');
                $crm->addAddress($receivedData['address'], 'pid', $pid,
                    'primary');
            }
            else {
                $ret = $person->set($pid, $receivedData['person']);
                if(!empty($cid)) {
                    $ret = $person->setMapping($pid, $cid,
                        $receivedData['mapping']);
                }
                $crm->setContact($receivedData['contact'], 'pid', $pid,
                    'primary');
                $crm->setAddress($receivedData['address'], 'pid', $pid,
                    'primary');
            }
#        }
    }

    if(empty($cid)) {
    return array('forward' => array('classModule' => 'crmIntern',
        'event' => 'showPerson', 'parameter' => array('pid' => $pid)));
    }
    else {
    return array('forward' => array('classModule' => 'crmIntern',
        'event' => 'showCompany', 'parameter' => array('cid' => $cid),
        'ank' => 'pid-'.$pid));
    }
}

private function deletePerson($pid, $sure=false) {
    if($sure && $pid) {
        $person = new person();
        $contract = new contract();
        $p = $person->get($pid);
        $person->del($pid);
        if(!empty($p['isFatherOfPid'])) {
            $person->del($p['isFatherOfPid']);
        }
        $contract->deleteByPid($pid);
        return array('forward' => array('classModule' => 'crmIntern',
            'event' => 'home'));

    }
    else {
        return array('view' => new crmInternViewQuestion(array(
            'caption' => L::_(112),
            'text' => L::_(113),
            'linkYes' =>
                urlHelper::makeCoreURL('crmIntern', 'deletePerson',
                array('pid' => $pid, 'sure' => 1)),
            'linkNo' =>
                urlHelper::makeCoreURL('crmIntern', 'showPerson',
                array('pid' => $pid))
            )));

    }
    return $view;
}
// -----------------------------------------------------------------------------
private function createAdviceProtocol($pid) {
    	$crmIntern = new crmIntern();
	$crmIntern->createAdviceProtocol($pid);
}

private function createCoverLetter($pid) {
    	$crmIntern = new crmIntern();
	$crmIntern->createCoverLetter($pid);
}
// -----------------------------------------------------------------------------
private function addNewContract($pid, $idco, $idt, $mail, $clear, $wishedBegin)
{

    if(!isset($idco) || !isset($idt))
    {
        die();
        //break;
    }

    // Add new Contract
    $c = new contract();
    $cData = $c->addContractFromIdco($idco, $idt, $mail, $clear, $wishedBegin);

    return $cData;
}


private function addNewContractWithMail($pid, $idco, $idt, $idtNotPossible, $mail)
{
    // check if $idt is filled otherwise check $idtNotPossible
    if($idt == 0 || !isset($idt))
    {
	if($idtNotPossible >0 && isset($idtNotPossible)) 
	{
		$idt = $idtNotPossible;
		unset($idtNotPossible);
	}
    }

    // Add new Contract
    $c = new contract();
    //$cData = $c->addContractFromIdco($idco, $idt, $mail, false);


    /* 
    ** $idco = contract to copy from
    ** $idt  = idt to copy to
    ** $mail = mail options
    ** $clear = create empty contract - default false - just for specific contracts
    ** $wishedBegin = amend the wishedBeginn date - not the case here, the wishedbegin from $idco will be used
    */

    $cData = $c->addContractFromCalc($idco, $idt, $mail, false, null);

    $crm = new crm();
    $internhandle = new crmIntern();

    $tariff = $c->getTariff($idt);
    $tariff['onlineContractUrl'] = $c->getOnlineContractURL($idt);

    $tariffName = $tariff['name'];

    $person = $cData['person'];
    $address = $cData['address'];
    $contract = $cData['contract'];
    $contact = $cData['contact'];




    if(isset($mail) && $mail >0)
    {
         // send user info mail
        $recipient = $contact['email'];
        $salutation = ($person['salutationLid']==9)?
            'Sehr geehrter Herr '.$person['surname']:
            'Sehr geehrte Frau '.$person['surname'];

    /*
    ** Duplizieren normal / normal leer
    */
    if($mail==1 || $mail==3) {
        $message = str_replace('{*ANREDE_NAME*}', $salutation, file_get_contents(project::getProjectRoot().'/templates/mail-request-template.tpl'));
        $subject = 'Ihr persönliches Angebot für eine '.$tariffName.' - Zahnzusatzversicherung';
    }


    /*
    ** Duplizieren alternativer Angebotstext / Alternativ leer
    */
    if($mail==2 || $mail==4) {
        $message = str_replace('{*ANREDE_NAME*}', $salutation, file_get_contents(project::getProjectRoot().'/templates/mail-request-template-duplicate.tpl'));
        $subject = 'Alternativangebot zu Ihrer Anfrage - Angebot für eine '.$tariffName.' - Zahnzusatzversicherung';
    }


	if($tariff['onlineContractUrl'])
	{
		$message = str_replace('{*ONLINE_SIGNUP_PARTIAL*}', file_get_contents(project::getProjectRoot().'/templates/mail-request-online-signup-template.tpl'), $message);
		$message = str_replace('{*ONLINE_SIGNUP_LINK*}', $tariff['onlineContractUrl'], $message);
	} else
	{
		$message = str_replace('{*ONLINE_SIGNUP_PARTIAL*}', '', $message);
	}


	if($mail==3 || $mail==4) {
		$reset = 1;
	} else 
		$reset = null;


        $message = str_replace("{*INSURANCE_NAME*}", $tariffName, $message);
        $message = str_replace("{*ADD_LIST_ITEMS*}", $contract['mail_message'], $message);
        $message = str_replace("{*SIGNATURE*}", project::gts('html'), $message);
        $message = str_replace("{*CSS_OPTION*}", "", $message);


        //$cData['idco'] ist nun die zuletzt eingefügte ID

        $pdf = $internhandle->genContractPdfByImage($cData['idco'], null, 'yes', $reset);
        $internhandle->outputPDF($pdf, $cData['idco']);

        #$file = project::getProjectRoot().'/pdf/Antrag - '.$cData['idco'].'.pdf';

	 $f = $c->getMailerFiles($cData['idco'], $cData['contract']);

        emailHelper::sendMailWithFiles($recipient, $subject, $message, $f, $cData['contract']['idt']);
	 errorHandler::throwErrorDb('mail sent out for idt '.$cData['contract']['idt'], $cData['idco'].'-'.print_r($status,true));
    }

        // send admin notification email
        $mailBody =
            "Absende-Zeit: ".date("d.m.Y H:i:s").
            "\n\nTarif: ".project::gtn($idt).
            "\n\nPreis: ".$contract['price'].
            "\n\n".'Frage/Bemerkung: '."\n".$contract['addNotes'].
            "\n\n".'Vertragsdaten: '."\n\n".
            print_r($contract, true);

        $ret = $crm->sendMail('allgemein@vm-experten.de', 'DUPLIZIERT: Online Rechner Antrag',
            $mailBody);

        return array('forward' => array('classModule' => 'crmIntern',
        'event' => 'showPerson', 'parameter' => array('pid' => $pid)));

}



// -----------------------------------------------------------------------------
// NOTE EVENTS
// -----------------------------------------------------------------------------
private function addNote($type, $id, $text, $importance) {
    $crm = new crm();
    $crm->addNote($type, $id, $text, $importance);
    $cid = $id;
    if($type == 'pid') {
        $person = new person();
        $cid = $person->getCidByPid($id);
    }

    if(empty($cid)) {
    return array('forward' => array('classModule' => 'crmIntern',
        'event' => 'showPerson', 'parameter' => array('pid' => $id)));
    }
    else {
    return array('forward' => array('classModule' => 'crmIntern',
        'event' => 'showCompany', 'parameter' => array('cid' => $cid),
        'ank' => $type.'-'.$id));
    }
}

private function delNote($nid, $type, $id) {
    $crm = new crm();
    $crm->delNote($nid);
    $cid = $id;
    if($type == 'pid') {
        $person = new person();
        $cid = $person->getCidByPid($id);
    }
    if(empty($cid)) {
    return array('forward' => array('classModule' => 'crmIntern',
        'event' => 'showPerson', 'parameter' => array('pid' => $id)));
    }
    else {
    return array('forward' => array('classModule' => 'crmIntern',
        'event' => 'showCompany', 'parameter' => array('cid' => $cid),
        'ank' => $type.'-'.$id));
    }
}

private function createCssChangeLetters($limit)
{
    $contract = new contract();
    $wzm = new wzm();

    $cData = $contract->getCssSwitchIdco($limit);

    while($row = $cData->fetch())
    {
        // 1. Send out the first mail
        $ret = $contract->sendTemplateByMail('CssSwitchInfoMail', 'Ihr CSS Zahntarif - erneute BeitragserhÃ¶hung fÃ¼r 2015 - Jetzt Wechsel-Optionen prÃ¼fen - www.zahnzusatzversicherung-experten.de', $row['idco'], null, 'files/CSS.FAQ.aktuelle.Entwicklungen.BAP.Ãœbernahme.HM.1311-2014.pdf', false, 'css-wechsel@zahnzusatzversicherung-experten.de');
        
        
        // 2. Send out the Insurance - mail DieBay
        $ret = $this->addNewContract($row['pid'], $row['idco'], 60, false, true, '2015-01-01');
		$ih = new crmIntern();
		$pdf = &$ih->genContractPdfByImage($ret['idco'], null, 'yes', 1);
		$ih->outputPDF($pdf, $ret['idco']);
		$file = '../files/pdf/Antrag - '.$ret['idco'].'.pdf';
	 $contract->setStatus($ret['idco'], 48, date("d.m.Y", time()));
        $ret = $contract->sendTemplateByMail('CssSwitchContractDieBay', 'CSS-Wechsel - Vorteilsangebot 1: Die Bayerische VIP dental plus - keine Wartezeit & Summenstaffel', $row['idco'], 60, $file, true, 'css-wechsel@zahnzusatzversicherung-experten.de');


        // 3. Send out the Insurance - mail AXA
        $ret = $this->addNewContract($row['pid'], $row['idco'], 23, false, true, '2015-01-01');
        	$ih = new crmIntern();
		$pdf = &$ih->genContractPdfByImage($ret['idco'], null, 'yes', 1);
		$ih->outputPDF($pdf, $ret['idco']);
		$file = '../files/pdf/Antrag - '.$ret['idco'].'.pdf';
	 $contract->setStatus($ret['idco'], 48, date("d.m.Y", time()));
	 $ret = $contract->sendTemplateByMail('CssSwitchContractAxa', 'CSS-Wechsel - Vorteilsangebot 2: AXA Dent Premium U - keine Wartezeit', $row['idco'], 23, $file, true, 'css-wechsel@zahnzusatzversicherung-experten.de');
       
	 // everything went fine? save it in the switchTable
        $wzm->insertData('tariff_switch', array('idco'=>$row['idco'], 'pid'=>$row['pid'], 'idt'=>$row['idt'], 'letterSent'=>date("Y-m-d", time())));
    }

}


// -----------------------------------------------------------------------------
// EXPORT EVENTS
// -----------------------------------------------------------------------------
private function exportCompanies() {
    // get data delivered by formEngine
    $receivedData = coreFormEngine::getReceivedData();

    if(empty($receivedData)) {
        $view = new crmInternViewExportCompanies();
        return array('view' => $view);
    }
    else {
        $crmIntern = new crmIntern();
        if($receivedData['export']['format'] == 'pdf') {
            return $crmIntern->exportCompanies($receivedData);
        }
        else {
            return $crmIntern->exportCompaniesAsCsv($receivedData);
        }
    }
}
// -----------------------------------------------------------------------------
private function sendMail($type, $id, $email) {
    // get data delivered by formEngine
    $mail = coreFormEngine::getReceivedData();
    if(empty($mail)) {
        return array('view' => new crmInternViewSendMail(array('type' => $type,
            'id' => $id, 'email' => $email)));
    }
    else {
        if(empty($mail['subject'])) {
            //$mail['subject'] = L::_(62);
        }

        // log
        $crm = new crm();
        $crm->logSendMail($type, $id, $mail['subject'], $mail['message']);

        $ret = crm::sendMail($mail['recipient'], $mail['subject'],
            $mail['message']);

        if($ret == true) {
            return array('view' => new crmInternViewConfirmation(
                array('caption' => L::_(68), 'text' => '')));
        }
        else {
            return array('err' => 3, 'errmsg' => 'sendmail failed',
                'errclass' => 'crmIntern', 'errevent' => 'sendMail');
        }
    }
}
// -----------------------------------------------------------------------------
private function ajaxGetPersonsByCompany($cid) {
    if(empty($cid)) {
        echo "no cid given";
        exit;
    }
    $person = new person();
    $crm = new crm();
    $p = $person->getByCompanyRs($cid);
    while($row = $p->fetch()) {
        $row = label::resolveLabels($row);
        $row['jobPositionId'] = $crm->getJobPositionName($row['jobPositionId']);
        echo "\n".implode(';',$row);
    }
    exit;
}
// -----------------------------------------------------------------------------
private function itemHome($type, $id) {
    if($type == 'cid') {
    return array('forward' => array('classModule' => 'crmIntern',
        'event' => 'showCompany', 'parameter' => array('cid' => $id)));
    }
    else {
    return array('forward' => array('classModule' => 'crmIntern',
        'event' => 'showPerson', 'parameter' => array('pid' => $id)));
    }
}

} // end class

?>
