<?php

class errorHandlerView extends view {

private $dataArray;

function __construct($dataArray) {
	parent::__construct();
	$this->dataArray = $dataArray;
	$this->processData();
}

function processData() {

	$replCode  = '<div class="error">'.
	$replCode .= '<p>'.L::errMsg($this->dataArray['errclass'],
		$this->dataArray['errmsg']).'</p>';

	if(!empty($this->dataArray['errclass']) &&
		!empty($this->dataArray['errevent'])) {

		$replCode .= '<p>'.L::errMsg($this->dataArray['errclass'],
			'forwardMessage').'</p>';

		$nextLink = '<a href="'.urlHelper::makeCoreURL(
			$this->dataArray['errclass'], $this->dataArray['errevent'])
			.'">'.L::errMsg($this->dataArray['errclass'], 'next').'</a>';

		$replCode = str_replace('#NEXT#', $nextLink, $replCode);
	}

	$replCode .= '</div>';


	// finish
// 	$this->pageId = 41;
	$this->replace('content', $replCode);
}


/*
	if(empty($replCode)) {
	$replCode = 'ErrorId: '.$this->dataArray['errmsg'];
	}

	if(empty($replCode)) {
	$replCode = errorHandler::genMessage($this->dataArray);
	}

*/



} // end class

?>