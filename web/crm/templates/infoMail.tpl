<html>
<body>

<div id="header">
	<div id="logo">
		<img src="https://www.zahnzusatzversicherung-experten.de/files/cto_layout/themedesigner/uploads/logo.svg" width="380px" />
	</div>
<font face="Arial, sans-serif">
<h3 style="color: blue;">
<a href="https://www.zahnzusatzversicherung-experten.de" target="_new">www.zahnzusatzversicherung-experten.de</a> - Ihr Spezialist rund um die Zahnzusatzversicherung
</h3>
<div>
<br /><p></p><br />
<div id="content">
<h2>Verehrte Kunden,</h2>

<p>
bereits vor einiger Zeit haben Sie über unser Vergleichsportal <a href="https://www.zahnzusatzversicherung-experten.de" target="_new">www.zahnzusatzversicherung-experten.de</a> eine hochwertige Zahnzusatzversicherung bei der <strong>CSS Versicherung AG</strong> abgeschlossen.
</p>

<p>
Sicherlich wird Ihnen nicht entgangen sein, dass sich die Beiträge im Tarif CSS.flexi in den vergangenen Jahren drastisch erhöht haben. Zudem kamen in den letzten Wochen Gerüchte auf, wonach die CSS angeblich pleite oder verkauft worden sein sollte.
</p>

<p>
Im Anhang dieser Mail finden Sie ein <strong>pdf-Dokument</strong>, in dem die wichtigsten Fragen rund um die aktuellen Entwicklungen bei der CSS beantwortet werden. Darin finden Sie Informationen zur Problematik der Beitragsentwicklung und zum Gerücht, die CSS sei pleite oder verkauft worden!
</p>

<p>
Nachdem die CSS Versicherung vor wenigen Tagen eine <strong>erneute Anpassung im Tarif CSS.flexi zum 1.1.2015 
angekündigt hat</strong> (offizielle Informationen dazu erhalten Sie im Laufe des Novembers per Post von CSS!), sehen 
wir es als unsere Pflicht an, Ihnen beratend zur Seite zu stehen und mögliche Optionen für die Zukunft aufzuzeigen.
</p>

<br /><br />
<h3>Ihre möglichen Optionen für die Zukunft:</h3>

<br />
<p><strong>
1. Wechsel zu einem anderen Anbieter
</strong></p>

<p>
Für CSS-Kunden mit derzeit gutem Zahnzustand ist der Anbieterwechsel in aller Regel erste Wahl, da hier
das größte Potential zur Optimierung besteht.
</p>

<p>
Wichtig ist hierbei natürlich auch Ihr Zahnzustand - ein Wechsel bringt unter Umständen zunächst
Nachteile mit sich, da neue Anbieter i.d.R. zunächst Wartezeiten und Begrenzungen, so dass die vollen
tariflichen Leistungen meist erst nach einiger Zeit zur Verfügung stehen.
</p>

<p>
<strong>
Unser TIPP:</strong> Mit seperater E-Mail erhalten Sie automatisch einen Wechsel-Vorschlag für ein Angebot der Bayerischen, wo 
auf Wartezeit & anfängliche Leistungsstaffel verzichtet wird! Dieser Tarif ist nur für Personen mit Nachweis einer Vorverischerung
bei CSS abschließbar!
</p>


<br /><br />
<p><strong>
2. Wechsel des Tarifes innerhalb der CSS
</strong></p>

<p>
Um den monatlich zu zahlenden Beitrag zu reduzieren, besteht evtl. auch die Möglichkeit, innerhalb des Tarifspektrums
der CSS zu wechseln. 
</p>

<p>
Das Problem hierbei ist allerdings, dass die CSS nur Varianten mit ähnlich hoher Leistung wie im bestehenden
Tarif anbietet, sich hierbei allerdings der Beitrag nur leicht senken lässt. Auf der anderen Seite bietet die CSS auch
deutlich günstigere Tarifvarianten, wo dann jedoch die Leistungen deutlich (!) niedriger ausfallen.
</p>

<p>
Leider gibt es derzeit im Tarifspektrum der CSS keinen "Mittelweg", welcher annehmbaren Beitrag und annehmbare
Leistungen verbindet!
</p>


<br /><br />
<p><strong>
3. Verbleib im bestehenden Tarif
</strong></p>


<p>
Viele von Ihnen werden die Versicherung nicht umsonst abgeschlossen haben, sondern weil sie mit vermehrtem
zahnärztlichem Behandlungsbedarf  in Zukunft rechneten.
</p>

<p>
Sofern Sie sich derzeit in einer laufenden zahnärztlichen Behandlung befinden oder in naher Zukunft mit 
umfangreichem zahnärztlichen Behandlungsbedarf rechnen (müssen), macht ein Tarif- oder Anbieterwechsel
wenig Sinn, da Sie dann ja auf die Leistungen angewiesen sind bzw. die bereits bekannten Diagnosen 
bei einem neuen Versicherer nicht mehr versicherbar wären.
</p>

<p><strong>
In diesem Fall raten wir von einem Wechsel - sowohl innerhalb CSS als auch zu einem gänzlich neuen 
Anbieter - grundsätzlich ab!
</strong></p>

<p>
Ein Wechsel macht erst dann Sinn, wenn Ihr Zahnzustand vollständig als "saniert" einzustufen ist und Sie
auf Sicht mehrerer Jahre davon ausgehen können, keine gröberen Zahnprobleme zu bekommen.
</p>

<p>
Sie haben Fragen oder benötigen Beratung? Wir sind gerne für Sie da. Schreiben Sie uns einfach ein E-Mail
oder rufen Sie uns unter 089 - 327 085 58 an (Mo-Do 9-17 Uhr, Fr 9-15 Uhr).
</p>
<br /><br />

<p>
<div id="signature">
Viele Grüße aus Olching<br /><br />
<strong>Maximilian Waizmann</strong><br />
<i>Versicherungsmakler Experten GmbH</i><br />
<br />

<p>
<strong>PS:</strong> sollten Sie Ihren Vertrag mit der CSS Versicherung zwischenzeitlich bereits gekündigt haben, dann stehen 
wir Ihnen selbstverständlich trotzdem gerne als Ansprechpartner zur Verfügung, sofern Sie erneut Interesse am 
Abschluss einer Zahnzusatzversicherung haben sollten.
</p>

</div> 
 
{*SIGNATURE*}
</p>
</font>
</div> 
</body>
</html>