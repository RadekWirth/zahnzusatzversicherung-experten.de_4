<p>
<br />
<strong>Tipp für Schnellentschlossene - Online-Direktabschluss</strong>
<br /><br />
Die {*INSURANCE_NAME*}-Versicherung bietet auch einen Online-Direktabschluss an - wenn Sie die Versicherung sofort online
abschließen möchten, klicken Sie bitte folgenden Link und füllen die nachfolgende Online-Strecke aus:

<a href="{*ONLINE_SIGNUP_LINK*}">Versicherung {*INSURANCE_NAME*} - jetzt direkt online abschließen</a>
<br />
</p>
