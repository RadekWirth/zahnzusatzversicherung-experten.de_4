<html>
<body>

<div id="header">
	<div id="logo">
<img width="381px" src="https://www.zahnzusatzversicherung-experten.de/files/cto_layout/themedesigner/uploads/logo.svg" />
	</div>
<font face="Verdana, Arial, sans-serif">
	<p><b>www.zahnzusatzversicherung-experten.de - Ihr Spezialist rund um die Zahnzusatzversicherung</b></p>
<div>
<br />
<div id="content">
<h2>Unser Angebot für eine Top Zahnzusatzversicherung <br /></h2>

<p><br />
{*ANREDE_NAME*},
</p>

<p>
vor einer Woche haben Sie über unser Vergleichsportal ein Angebot für eine
Zahnzusatzversicherung angefordert.
</p>
<p>
Wir hoffen, dass Sie zwischenzeitlich die Zeit gefunden haben, sich mit den
Angebotsunterlagen zu befassen.
</p>
<p>
Sollten Sie noch nicht restlos überzeugt sein, würden wir uns sehr über eine
Rückmeldung von Ihnen freuen:
</p>
<ul>
	<li>Sind Ihnen einzelne Details der Leistungen noch unklar?
	<li>Entsprechen die Leistungen der Versicherung nicht Ihren Vorstellungen?
	<li>Können Sie sich zwischen verschiedenen Angeboten nicht entscheiden?
</ul>

<p>
Sprechen Sie uns an! Wir stehen Ihnen jederzeit gerne für ein Beratungsgespräch
zur Verfügung, um den passenden Zahntarif für Sie zu finden.
</p>

<h3>
Rufen Sie einfach an unter 08142 - 651 39 28!
</h3>

<p>
Wir würden uns sehr freuen, wenn Sie uns Ihr Vertrauen schenken und den
Abschluss der Zahnversicherung über uns realisieren.
</p>

<p> 
Wenn Sie mit unserem Angebotsservice und unserer Beratung zufrieden waren, würden wir uns sehr freuen, wenn Sie uns online bewerten unter: 
</p> 

<a href="https://www.zahnzusatzversicherung-experten.de/bewertung-abgeben.html">Bewertung abgeben.</a>
&nbsp;(Die Abgabe der Bewertung dauert maximal 1-2 Minuten!)<br />


<p><br />

<div id="signature">
Mit freundlichen Grüßen<br />
<br /> 
	<img src="http://www.zahnzusatzversicherung-experten.de/tl_files/bilder/sign_mw.png" />
<p>
<i>Maximilian Waizmann</i><br />
</div> 

<p><br />

<p>
<strong>PS:</strong> Reagieren Sie rechtzeitig! Schieben Sie den Abschluss einer Zahnversicherung
nicht auf die lange Bank! Wenn es erst einmal so weit ist, dass der
Zahnarzt Behandlungsbedarf diagnostiziert hat, ist es für eine Versicherung
leider zu spät.
 
{*SIGNATURE*}

</font></font>
</div> 
</body>

</html>
