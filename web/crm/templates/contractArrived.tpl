<html>
<body>

<div id="header">
	<div id="logo">
		<img src="https://www.zahnzusatzversicherung-experten.de/tl_files/bilder/Zahnzusatzversicherung-Experten.png" />
	</div>
<font face="Verdana, Arial, sans-serif">
	<p><b>www.zahnzusatzversicherung-experten.de - Ihr Spezialist rund um die Zahnzusatzversicherung</b></p>
<div>
<br />
<div id="content">
<h2>{*ANREDE_NAME*},</h2>

<p> 
vielen Dank für Ihr Vertrauen.
</p>
<p>
Ihren Antrag haben wir heute erhalten und an die Versicherungsgesellschaft weitergeleitet. Je nach Gesellschaft dauert es nun im Normalfall ca. 1-3 Wochen bis Sie den Versicherungsschein erhalten.
</p>
 
<p> 
Sollten Sie zukünftig Fragen zu Ihrem Versicherungsschutz haben, dürfen Sie sich selbstverständlich jederzeit gerne an uns wenden.
</p> 

<p> 
Wenn Sie mit unserem Angebotsservice und unserer Beratung zufrieden waren, würden wir uns sehr freuen, wenn Sie uns online bewerten unter: 
</p> 

<a href="https://www.zahnzusatzversicherung-experten.de/bewertung-abgeben.html">Bewertung abgeben.</a>
&nbsp;(Die Abgabe der Bewertung dauert maximal 1-2 Minuten!)<br />

<p>
<div id="signature">
Mit freundlichen Grüßen<br />
<br /> 
<i>Maximilian Waizmann</i><br />
</div> 
 
{*SIGNATURE*}
</p>
</font>
</div> 
</body>

</html>
