<html>
<body>

<div id="header">
	<div id="logo">
		<img src="http://www.zahnzusatzversicherung-experten.de/tl_files/bilder/Zahnzusatzversicherung-Experten.png" />
	</div>
<font face="Verdana, Arial, sans-serif">
<h3 style="color: blue;">
<a href="http://www.zahnzusatzversicherung-experten.de" target="_new">www.zahnzusatzversicherung-experten.de</a> - Ihr Spezialist rund um die Zahnzusatzversicherung
</h3>
<div>
<br />
<div id="content">
<h2>{*ANREDE_NAME*},</h2>

<p>
mit einem seperaten E-Mail informierten wir Sie parallel über die aktuell geplante Beitragserhöhung bei der CSS-Versicherung, wo seit einiger Zeit eine Zahnzusatzversicherung für Sie besteht.
</p>
 
<p> 
Anbei erhalten Sie einen Vorschlag für ein besonderes Wechsel-Angebot, welches ausschließlich Kunden mit einer hochwertigen Vorversicherung bei CSS vorbehalten ist:
</p> 

<p>Die Bayerische - VIP dental plus 
</p> 
 
<p> 
Das besondere: bei nahtlosem Versicherungsübergang verzichtet die Bayerische auf die ansonsten üblichen Wartezeiten und anfänglichen Summenbegrenzungen. Es besteht damit sofort voller Versicherungsschutz - ein Vorteil, den sonst kein anderes Angebot am Markt bieten kann!
</p>
 
<h3>
Im Anhang dieser Mail finden Sie folgende Dokumente:
</h3> 

<p><ol style=number>
<li>
Angebotsunterlagen (Anschreiben, Antrag, Beratungsdokumentation)
</li><li>
Offizielle Tarifbedingungen und Verbraucherinformationen (das "Kleingedruckte")
</li><li>
Ausführliche Leistungsbeschreibung (mit weitergehenden Erläuterungen zu den Tarifleistungen)
</li></ol></p>

<p style="color: blue;">
Bitte drucken Sie sich den Antrag und die Beratungsdokumentation aus und ergänzen die notwendigen Angaben an den gelb markierten Stellen. Sobald der Antrag vollständig ausgefüllt und unterschrieben ist, können Sie ihn uns per Post, E-Mail oder Fax zurücksenden.
</p>

<p>
Falls Sie eine der Fragen zum Zahnzustand mit JA beantworten müssen, kontaktieren Sie uns bitte, damit wir mögliche Alternativen mit Ihnen erörtern können!
</p>

<p>
Falls Sie keinen Drucker zur Verfügung haben, informieren Sie uns bitte - wir können Ihnen die Angebotsunterlagen selbstverständlich gerne auch per Post zusenden!
</p>

<p>
Sollten Sie Fragen zum Ausfüllen des Antrages oder den Leistungen haben, stehen wir Ihnen gerne zur Verfügung.
</p>

<p style="font-weight:bold;">
Schicken Sie uns Ihre Frage einfach per E-Mail und Sie erhalten innerhalb von 24 Stunden eine aussagekräftige Antwort von uns. Gerne stehen wir Ihnen unter 089 - 327 085 58 auch telefonisch für ein Beratungsgespräch zur Verfügung (Mo-Do 9-17 Uhr, Fr 9-15 Uhr).
</p>

<p>
<div id="signature">
Viele Grüße aus Olching<br />
<br /> 
<i>Maximilian Waizmann</i><br />
</div> 
 
{*SIGNATURE*}
</p>
</font>
</div> 
</body>
</html>
