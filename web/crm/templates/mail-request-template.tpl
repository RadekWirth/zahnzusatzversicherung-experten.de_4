<!DOCTYPE html>
<body>

<div id="header">
<!--	<div id="logo"><object data="https://www.zahnzusatzversicherung-experten.de/files/cto_layout/themedesigner/uploads/logo.svg" type="image/svg+xml" width="381" ></object>
	</div>-->
<font face="Verdana, Arial, sans-serif">
	<p><b>www.zahnzusatzversicherung-experten.de - Ihr Spezialist rund um die Zahnzusatzversicherung</b></p>
<div>
<br />
<div id="content">
<h2>{*ANREDE_NAME*}, </h2>

<p> 
vielen Dank für Ihre Anfrage auf <a href="https://www.zahnzusatzversicherung-experten.de" target="_new">www.zahnzusatzversicherung-experten.de</a> und das uns entgegengebrachte Vertrauen.<br />
</p>
<p>
Wie gewünscht erhalten Sie anbei das Angebot der {*INSURANCE_NAME*} Zahnzusatzversicherung per E-Mail. <font color="red">Zusätzlich erhalten Sie die Unterlagen in den nächsten 2-3 Tagen postalisch zugeschickt.</font><br />
</p>
 
<h3>Im Anhang dieser Mail finden Sie folgende Dokumente:</h3>
<ol>
	{*ADD_LIST_ITEMS*}
</ol>

<br />
{*ONLINE_SIGNUP_PARTIAL*}
<br />

<p> 
Bitte drucken Sie sich den Antrag und die Beratungsdokumentation aus und ergänzen die notwendigen Angaben (beachten Sie hierzu bitte die Hinweise zum Ausfüllen des Antrags). Sobald der Antrag vollständig ausgefüllt und unterschrieben ist, können Sie ihn uns per Post, E-Mail oder Fax zurücksenden.<br />
</p> 
{*CSS_OPTION*} 
 
<h3>Sollten Sie Fragen zum Ausfüllen des Antrages oder den Leistungen der Versicherung haben, stehen wir Ihnen gerne zur Verfügung:</h3>
<p>
Schicken Sie uns Ihre Frage einfach per E-Mail und Sie erhalten innerhalb von 24 Stunden eine aussagekräftige Antwort von uns. Gerne stehen wir Ihnen unter 08142 - 651 39 28 auch telefonisch für ein Beratungsgespräch zur Verfügung.<br />
</p>

<p> 
Wenn Sie mit unserem Angebotsservice und unserer Beratung zufrieden waren, würden wir uns sehr freuen, wenn Sie uns online bewerten unter: 
</p> 

<a href="https://www.zahnzusatzversicherung-experten.de/bewertung-abgeben.html">Bewertung abgeben.</a>
&nbsp;(Die Abgabe der Bewertung dauert maximal 1-2 Minuten!)<br />


<div id="signature">
Mit freundlichen Grüßen<br />
<br /> 
<i>Maximilian Waizmann</i><br />
</div> 
 
<p>
-------------------------------------------------------------------------------------------
</p>

<p>
Anschrift und Kontakt:<br />
Versicherungsmakler Experten GmbH<br />
Feursstr. 56 / RGB<br />
82140 Olching
</p>
<p>
Geschäftsführer: Maximilian Waizmann, Versicherungsfachmann BWV<br />
Eingetragen im Handelsregister des Amtsgericht München unter HRB 196198
</p>
<p>
Telefon  08142 - 651 39 28<br />
Fax   08142 - 651 39 29<br />
E-Mail:  info@zahnzusatzversicherung-experten.de<br />
Internet:  www.zahnzusatzversicherung-experten.de
</p>
<p>
Vermittlerstatus<br />
Die Versicherungsmakler Experten GmbH ist Versicherungsmakler gem. §93 HGB
</p>
<p>
IHK-Registrierung<br />
Eingetragen im Vermittlerregister der IHK-München unter der Nummer D-YIRL-NDZ7C-33
</p>
<p>
Unternehmensbeteiligungen<br />
Das Unternehmen besitzt keine direkten oder indirekten Beteiligungen oder Stimmrechte am Kapital eines Versicherungsunternehmens. Es besitzt auch kein Versicherungsunternehmen direkte oder indirekte Stimmrechte oder Beteiligungen am Kapital dieses Unternehmens.
</p>
<p>
Vermögensschadenhaftpflichtversicherung<br />
Eine den Bestimmungen der EU-Vermittlerrichtlinie entsprechende Berufshaftpflichtversicherung / Vermögensschadenhaftpflichtversicherung liegt vor (über HDI ehem. Nassau Versicherungen).<br />
</p>
<p>
Steuernummer<br />
143/190/00969
</p>
<p>
Umsatzsteuer-ID<br />
DE281397704
</p>
<p>
Zuständige Aufsichtsbehörde<br />
Industrie- und Handelskammer für München und Oberbayern<br />
Balanstr. 55-59<br />
81541 München<br />
Tel:  089 5116 - 0<br />
Fax:  089 5116 - 1306<br />
Mail:  ihkmail@muenchen.ihk.de<br />
web:  www.muenchen.ihk.de
</p>
<p>
Anschrift der Schlichtungsstellen: <br />
Bundesanstalt für Finanzdienstleistungsaufsicht (BAFin)<br />
Graurheindorfer Straße 108<br />
53117 Bonn<br />
Tel:  0228 4108 - 0<br />
Fax:  0228 4108 - 1550<br />
Mail: poststelle@bafin.de<br />
web:  www.bafin.de
</p>
<p>
Versicherungsombudsmann e.V.<br />
Postfach 08 06 22<br />
10006 Berlin<br />
Tel:  030 206058 - 0<br />
Fax:  030 206058 - 58<br />
Mail:  info@versicherungsombudsmann.de<br />
web:  www.versicherungsombudsmann.de
</p>
<p>
Ombudsmann für die private Kranken- und Pflegeversicherung<br />
Postfach 06 02 22<br />
10052 Berlin<br />
Tel:  01802 55 04 44 (6ct pro Anruf aus dem Dt. Festnetz)<br />
Fax: 030 20458931<br />
web: www.pkv-ombudsmann.de
</p>
</font></font>
</div> 
</body>
</html>
