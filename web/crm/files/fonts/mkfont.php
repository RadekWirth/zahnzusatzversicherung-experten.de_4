<?php
/*
 * mkfont.php - A PHP file for providing a command line access to makefont.php
 */
require_once('../../libs/fpdf16/font/makefont/makefont.php');
$afm_files = glob($argv[1]);
function ttf_file($f) {
  $ttf_name = basename($f, '.afm') . '.ttf';
  return file_exists($ttf_name) ? $ttf_name : '';
}
foreach($afm_files as $f) {
  MakeFont(ttf_file($f), $f);
}
?>