<?php
include_once('libs/phpgraphlib_v2.21/phpgraphlib.php');
include_once('classes/wzm/wzm.class.php');
include_once('classes/wzm/contract.class.php');
include_once('web-modules/project/project.class.php');
include_once('web-modules/database/database.class.php');
include_once('web-modules/database/databaseResultSet.class.php');

$contract = new contract();
$stats = $contract->getStats('in_out');

$graph = new PHPGraphLib(450,330);
if(is_array($stats))
{
	$graph->addData($stats[ret], $stats[sent]);
}
$graph->setupXAxis(20, "blue");
$graph->setTitle("Erhaltene / Verschickte Antr�ge");
$graph->setTitleColor("black");
$graph->setGridColor("153,204,255");
$graph->setTitleLocation("left");
$graph->setLegend(true);
$graph->setLegendTitle("zur�ck", "erhalten");
$graph->setBarColor("208,0,0", "102,208,0");
$graph->setDataValues(false);
$graph->createGraph();
?>