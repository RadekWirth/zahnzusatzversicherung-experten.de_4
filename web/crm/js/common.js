$(document).ready(function() {

	$("#antragserinnerung h2").click(function(){
		$("div#hid").toggle();
	});

	$("select#vonDateD").change(function(){
		$("select#bisDateD").val( $(this).val() );
	});

	$("select#vonDateM").change(function(){
		$("select#bisDateM").val( $(this).val() );
	});

	$("select#vonDateY").change(function(){
		$("select#bisDateY").val( $(this).val() );
	});

});

function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

function resetFormQuestions (oForm) {
	frm_elements = oForm.elements;


	var input = confirm("Die Daten werden zur�ckgesetzt - m�chten Sie das?");
	if(input==true)
	{
		for (i = 0; i < frm_elements.length; i++)
		{
	    	field_type = frm_elements[i].type.toLowerCase();
		    switch (field_type)
		    {
		    case "text":
		    case "password":
		    case "textarea":
		    case "hidden":
		        /* frm_elements[i].value = ""; */
	       	 break;
		    case "radio":
		    case "checkbox":
		        if (frm_elements[i].checked)
		        {
		            frm_elements[i].checked = false;
		        }
		        break;
		    case "select-one":
		    case "select-multi":
		        frm_elements[i].selectedIndex = -1;
		        break;
		    default:
		    break;
    		    }
	       }
	} else { return false; }
}

function showHide(shID) {
	if(document.getElementById(shID)) {
		if(document.getElementById(shID).style.display  == 'none') {
			document.getElementById(shID).style.display = 'block';
		}
		else {
			document.getElementById(shID).style.display = 'none';
		}
	}
}

function show(shID) {
	if(document.getElementById(shID)) {
		document.getElementById(shID).style.display = 'block';
	}
}

function hide(shID) {
	if(document.getElementById(shID)) {
		document.getElementById(shID).style.display = 'none';
	}
}
function handleExportClick() {
	$target= 'controller.php?cm=crmIntern&event=exportCompanies';
	$link = '<a href=\"'+$target+'\">Einen weiteren Export starten</a>';
	document.getElementById('hiddenForwarder').innerHTML = $link;
	document.getElementById('content').style.display = 'none';
	document.getElementById('hiddenForwarder').style.display = 'block';
}

function showPopbox(textToShow) {
	var elm = document.getElementById('popboxText');
	elm.innerHTML = textToShow;
	document.getElementById('popbox').style.top = 50+f_scrollTop()+'px';
	document.getElementById('popbox').style.display = 'block';
}

function f_scrollTop() {
	return f_filterResults (
		window.pageYOffset ? window.pageYOffset : 0,
		document.documentElement ? document.documentElement.scrollTop : 0,
		document.body ? document.body.scrollTop : 0
	);
}

function submitForm() {
	return true;
}

function f_filterResults(n_win, n_docel, n_body) {
	var n_result = n_win ? n_win : 0;
	if (n_docel && (!n_result || (n_result > n_docel)))
		n_result = n_docel;
	return n_body && (!n_result || (n_result > n_body)) ? n_body : n_result;
}


function checkToothSelect() {
	var t1 = document.getElementById('IDcontractTooth1').value;
	var t2 = document.getElementById('IDcontractTooth2').value;
	var i, c=0, max=parseInt(t1)+parseInt(t2);
	var e;
	for(i=0;i<32;i++) {
		e = document.getElementById("ts"+i);
		if(i==0 || i==15 || i==16 || i==31) {
			if(e.checked==true) {
				document.getElementById('toothSelectNotice').style.display =
					'block';
			}
			continue;
		}
		if(e.checked==true) {
			c++;
		}
 		if(c > max) return false;
	}
	return true;
}


function checkInput(entryId, elementId, type, minlen, maxlen) {
	if(minlen == null) {
		minlen = 3;
	}
	var entry = document.getElementById(entryId);
	var e = document.getElementById(elementId);
	switch(type) {
	case 'text':
		if(maxlen && e.value.length > maxlen) {
			setElementInputStatus(entry, 'nok');
			return;
		}
		if(e.value.length >= minlen) setElementInputStatus(entry, 'ok');
		else setElementInputStatus(entry, 'nok');
		break;
	case 'number':
		if(maxlen && e.value.length > maxlen) {
			setElementInputStatus(entry, 'nok');
			return;
		}
		if(isInt(e.value) && e.value.length >= minlen)
			setElementInputStatus(entry, 'ok');
		else setElementInputStatus(entry, 'nok');
		break;
	case 'select':
		if(e.value != 0) setElementInputStatus(entry, 'ok');
		else setElementInputStatus(entry, 'nok');
		break;
	case 'date':
		if(document.getElementById(elementId+'D').value!=0 &&
			document.getElementById(elementId+'M').value!=0 &&
			document.getElementById(elementId+'Y').value!=0)
			setElementInputStatus(entry, 'ok');
		else setElementInputStatus(entry, 'nok');
		break;
	case 'email':
		if(isEmailValid(e.value)) setElementInputStatus(entry, 'ok');
		else setElementInputStatus(entry, 'nok');
		break;
	}


}

function showMultiple(shIDs) {
    var id = shIDs.split(',');
    for (var i = 0; i < id.length; i++) {
        $("#" + id[i]).show(200);
    }
}
function hideMultiple(shIDs) {
    var id = shIDs.split(',');
    for (var i = 0; i < id.length; i++) {
        $("#" + id[i]).hide();
    }
}

function setElementInputStatus(e, status) {
	var bgX = 460;
	var bgY = 0;

	if(getInternetExplorerVersion()!=false &&
		getInternetExplorerVersion() < 7) {
		bgY = -30;
	}

	switch(status) {
	case 'ok':
	e.style.background= 'url("tl_files/formengine/ok.gif") no-repeat '+bgX+'px '+bgY+'px';
	break;

	case 'nok':
	e.style.background= 'url("tl_files/formengine/nok.gif") no-repeat '+bgX+'px '+bgY+'px';
	break;
	}
}


function getInternetExplorerVersion()
// Returns the version of Internet Explorer or a -1
// (indicating the use of another browser).
{
  var rv = false; // Return value assumes failure.
  if (navigator.appName == 'Microsoft Internet Explorer')
  {
    var ua = navigator.userAgent;
    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
    if (re.exec(ua) != null)
      rv = parseFloat( RegExp.$1 );
  }
  return rv;
}

function isEmailValid(str) {
	var at="@";
	var dot=".";
	var lat=str.indexOf(at);
	var lstr=str.length;
	var ldot=str.lastIndexOf(dot);
	if((lstr-ldot)<3 || (ldot-lat)<4) {
		return false;
	}
	if (str.indexOf(at)==-1) return false;
	if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		return false;
	}
	if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		return false;
	}
	if (str.indexOf(at,(lat+1))!=-1){
	return false;
	}
	if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
	return false;
	}
	if (str.indexOf(dot,(lat+2))==-1){
	return false;
	}
	if (str.indexOf(" ")!=-1){
	return false;
	}
	return true;
}

 function isInt(x) {
   var y=parseInt(x);
   if (isNaN(y)) return false;
   return x==y && x.toString()==y.toString();
 }

// e = mouse event
/*
function getCursorPosition(e) {
    e = e || window.event;
    var cursor = {x:0, y:0};
    if (e.pageX || e.pageY) {
        cursor.x = e.pageX;
        cursor.y = e.pageY;
    }
    else {
        var de = document.documentElement;
        var b = document.body;
        cursor.x = e.clientX +
            (de.scrollLeft || b.scrollLeft) - (de.clientLeft || 0);
        cursor.y = e.clientY +
            (de.scrollTop || b.scrollTop) - (de.clientTop || 0);
    }
    return cursor;
}
*/
