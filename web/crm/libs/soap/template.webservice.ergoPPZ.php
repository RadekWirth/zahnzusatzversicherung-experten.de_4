<?
	// Webserviceeinbindung
	// Christian Berlage 01.08.2011

	require_once('/libs/soap/nusoap.php');					      // hier sind alle wichtigen Funktionen drinnen

	// �bergabe der Variablen zur Antragsgenerierung
	$Eingabe=array();
	// -> [person][birthdate] oder [personInsured][birthdate]
	$Eingabe['EingabeDaten']['Person']['GeburtsDatum']	="1975-08-15";      // Geburtsdatum der versicherten Person
	// -> [begin]
	$Eingabe['EingabeDaten']['VertragsBeginn']		="2011-11-01";      // Beginn (immer der Monatserste)
	$Eingabe['EingabeDaten']['Zahlungsweise']			="monatlich";	      // immer monatlich
	$Eingabe['EingabeDaten']['VermittlungsID']		="WAIZMANN_MAX";    // Ihre ID f�r Firma Maximilian Waizmann;
	$Eingabe['EingabeDaten']['Tarife']				="ZAB;ZAE;ZBB;ZBE"; // Tarifkombination
		
	// URL f�r den Webservicezugriff
	$ServiceURL	= "https://ergodirekt.tough-werbeagentur.de/vertrieb/_Test/TarifPPZWebservice/ppzangebot.php?wsdl";
	
	// Webserviceverbindung herstellen
	$Client 	= new nusoap_client($ServiceURL, true);			

	// Testen auf Fehler bei der Verbindung
	$Err = $Client->getError();
	if ($Err) {	echo '<h2>Constructor error</h2><pre>' . $Err . '</pre>'; 		die;	}
	
	// Die Methode (hier "AntragPPZ") aufrufen (hier passiert das Wesentliche)
	$Result = $Client->call('AntragPPZ', $Eingabe);
	
	// Testen auf Fehler
	$Err = $Client->getError();
	if ($Err) {		echo '<h2>Error</h2><pre>' . $Err . '</pre>'; die;	}
	
	// ----------------------------------------------------------------------------------
	// -- Geschafft: hier sind wir jetzt schon fertig (alle Daten stehen in $Result)	-
	// ----------------------------------------------------------------------------------
	// 
	// so sieht das zur�ckgegeben Array nun aus:
	//
	// $Result['AusgabeDaten']['Beitrag']				Beitrag des Angebots, z.B. 26.7
	// $Result['AusgabeDaten']['Ablaufdatum']			*leer*
	// $Result['AusgabeDaten']['Fehler']				Wenn Fehler, dann ist hier die Fehlermeldung drin
	// $Result['AusgabeDaten']['PDFAngebot']			base64-codiertes PDF - Decodierung z.B. $PDF=base64_decode($Result['AusgabeDaten']['PDFAngebot'])

	$handle = fopen ("soap_ergo_premium.pdf", "wb");
	fwrite($handle, base64_decode($Result['AusgabeDaten']['PDFAngebot']));
	fclose($handle);
?>
