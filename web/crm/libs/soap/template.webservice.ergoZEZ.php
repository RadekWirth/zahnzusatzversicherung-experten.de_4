<?
	// Webserviceeinbindung ZEZ
	// Christian Berlage 25.06.2013, christian.berlage@berlage.eu

	require_once('nusoap.php');					// hier sind alle wichtigen Funktionen drinnen
						
	// �bergabe der Variablen zur Antragsgenerierung
	$Eingabe	=array();
	$Eingabe['EingabeDaten']['Person']['GeburtsDatum']	= "1975-08-15";			// Geburtsdatum der versicherten Person (Format JJJJ-MM-TT)
	$Eingabe['EingabeDaten']['VertragsBeginn']			= "2013-08-01";			// Beginn (immer der Monatserste, Format JJJJ-MM-01), max. 6 Monate in der Zukunft
	$Eingabe['EingabeDaten']['Zahlungsweise']			= "monatlich";			// immer monatlich
	$Eingabe['EingabeDaten']['VermittlungsID']			= "WAIZMANN_MAX";		// Ihre VermittlungsID (immer verwenden);
	$Eingabe['EingabeDaten']['Tarife']					= "ZEZ";				// Tarifkombination (hier nur 'ZEZ' m�glich)
		
	// URL f�r den Webservicezugriff
	$ServiceURL	= "https://ergodirekt.tough-werbeagentur.de/vertrieb/ZahnzusatzWebservice2013/zzangebot.php?wsdl";
	
	// Webserviceverbindung herstellen
	$Client 	= new nusoap_client($ServiceURL, true);			

	// Testen auf Fehler bei der Verbindung
	$Err = $Client->getError();
	if ($Err) {	echo '<h2>Constructor error</h2><pre>' . $Err . '</pre>'; 		die;	}
	
	// Die Methode (hier "AntragPPZ") aufrufen (hier passiert das Wesentliche)
	$Result = $Client->call('AntragPPZ', $Eingabe);
	
	// Testen auf Fehler
	$Err = $Client->getError();
	if ($Err) {		
		echo '<h2>Error</h2><pre>' . $Err . '</pre>'; 
		echo '<h2>Response</h2>.<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
	}
	
	// ----------------------------------------------------------------------------------
	// -- Geschafft: hier sind wir jetzt schon fertig (alle Daten stehen in $Result)	-
	// ----------------------------------------------------------------------------------
	// 
	// so sieht das zur�ckgegeben Array nun aus:
	//
	// $Result['AusgabeDaten']['Beitrag']				Beitrag des Angebots, z.B. 26.7
	// $Result['AusgabeDaten']['Ablaufdatum']			*leer*
	// $Result['AusgabeDaten']['Fehler']				Wenn Fehler, dann ist hier die Fehlermeldung drin
	// $Result['AusgabeDaten']['Tarif'] 				die gew�hlte TArifkombination, z.B. 'ZAB,ZAE,ZBB,ZBE'
    // $Result['AusgabeDaten']['Dokumentinfo'] 			f�r interner Zwecke, Aufbau des Dokuments, z.B. '15 Seiten|Antrag-1-2|PIB-3-4|VI-5-7|AVB-8-13|DV-14-15'
	// $Result['AusgabeDaten']['PDFAngebot']			base64-codiertes PDF - Decodierung z.B. $PDF=base64_decode($Result['AusgabeDaten']['PDFAngebot'])

	
	// im Folgenden zwei Beispiele 
	

	// Ausgabe aller R�ckgabedaten
	echo '<h2>Result</h2><pre>';
	print_r($Result);
	echo '</pre>';
	
	
	// Speichern des erhaltenen PDF-Antrags im aktuellen Verzeichnis
	$file	= 'zez_angebot.pdf';
	if (!($bytes = file_put_contents($file, base64_decode($Result['AusgabeDaten']['PDFAngebot'])))){
		echo "<br>Fehler beim Schreiben der Datei '$file'.";
	}else{
		echo "<br><a href='$file'>$file</a><br>";
	}

?>
