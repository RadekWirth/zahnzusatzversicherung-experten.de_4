<?php

/*
** This file shall be called from crm from link controller.php?cm=crmIntern&event=manedDetails
*/

if(strpos($_SERVER['HTTP_REFERER'], 'https://www.zahnzusatzversicherung-experten.de/') === false)
	die('Fehler! Aufruf nur von der Seite www.zahnzusatzversicherung-experten.de m�glich! -> '.$_SERVER['HTTP_REFERER']);

include_once('../../classes/wzm/wzm.class.php');
include_once('../../web-modules/project/project.class.php');
include_once('../../web-modules/database/database.class.php');
include_once('../../web-modules/database/databaseResultSet.class.php');
include_once('../../web-modules/helperClasses/stringHelper.class.php');

$wzm = new wzm();

$postHTML = $_POST['info'];
$postCNT = $_POST['cnt'];
$postIDT = $_POST['idt'];
$postID = $_POST['id'];

if(!isset($postHTML) || !isset($postCNT) || !isset($postIDT))
	die('Fehler! Variablen nicht komplett gesetzt!');

//$postHTML = str_replace("\", null, $postHTML);

//$sql = "UPDATE wzm_details SET text = '".htmlspecialchars($postHTML)."' WHERE 1=1 AND id = ".$postID;
//echo $sql;

$wzm->amendDetails(array('text' => htmlentities($postHTML, ENT_QUOTES, "UTF-8")), 'id', $postID);

?>