<?php

class company extends crm {

// -----------------------------------------------------------------------------
public function __construct() {

}
// -----------------------------------------------------------------------------
public function add($data) {
	$db = $this->getDb();
	$data = stringHelper::arrayUcFirst($data);
	$data = array_merge($data, array('uid' => user::getUid()));
	$valuesSql = stringHelper::arrayToSql($data);
	$db->query("INSERT INTO ".$this->table('company')." SET ".$valuesSql);
	return $db->insertId();
}

public function get($cid, $removeIds=false) {
	$db = $this->getDb();
	$rs = $db->query("SELECT * FROM ".$this->table('company').
		" WHERE cid='".$cid."'");
	$row = $rs->fetch();
	if($removeIds) {
		unset($row['cid']);
		unset($row['uid']);
	}
	return $row;
}

public function set($cid, $data) {
	$db = $this->getDb();
	$valuesSql = stringHelper::arrayToSql($data);
	$rs = $db->query("UPDATE ".$this->table('company')." SET ".$valuesSql.
		" WHERE cid='".$cid."'");
	return $db->affectedRows();
}

public function getAllRs($firstLetter=null) {
	$db = $this->getDb();
	$sqlWhere = '';
	if(!empty($firstLetter)) {
		if($firstLetter==1) {
			$sqlWhere = " WHERE name REGEXP '^[[:digit:]]+'";
		}else {
			$sqlWhere = " WHERE name LIKE '".$firstLetter."%'";
		}
	}
	$rs = $db->query("SELECT * FROM ".$this->table('company')
		.$sqlWhere." ORDER BY name");
	return $rs;
}

public function getAllWithAssignmentsRs() {
	$db = $this->getDb();
	$rs = $db->query("SELECT DISTINCT c.* FROM ".$this->table('company').
		"  AS c INNER JOIN ".$this->table('assignment')." AS a".
		" ON a.cid = c.cid".
		" WHERE a.assignmentStatusLid != '29'".
		" ORDER BY creationDate DESC");
	return $rs;
}

public function getAllWithImportanciesRs() {
	$db = $this->getDb();
	$rs = $db->query("SELECT DISTINCT c.* FROM ".$this->table('company').
		" AS c INNER JOIN ".$this->table('notes')." AS n".
		" ON n.cid = c.cid".
		" WHERE n.importance > 0".
		" ORDER BY n.creationDate DESC");
	return $rs;
}


public function del($cid) {
	$db = $this->getDb();
	$rs = $db->query("DELETE FROM ".$this->table('company').
		" WHERE cid='".$cid."'");

	$crm = new crm();

	// del notes contact & co
	$crm->delNotes('cid', $cid);
	$crm->deleteAddressAll('cid', $cid);
	$crm->deleteContactAll('cid', $cid);

	// delete persons
	$person = new person();
	$rs = $person->getByCompanyRs($cid);
	while($row = $rs->fetch()) {
		$person->del($row['pid']);
	}

}

public function search($selection) {
	$db = $this->getDb();
	$sqlWhere = '';

	if(count($selection['keywords'])) {
		$sqlWhere .= ' ('.stringHelper::makeSqlWhere(
			array('c.name', 'c.subtitle', 'c.note'),
			$selection['keywords'], 'OR', true).')';
	}
	unset($selection['keywords']);

	if(!empty($selection['postcode'])) {
		if(!empty($sqlWhere)) $sqlWhere .= " AND";
		$sqlWhere .= " a.postcode='".$selection['postcode']."'";
	}
	unset($selection['postcode']);

	if(!empty($selection['stateId'])) {
	if(!empty($sqlWhere)) $sqlWhere .= " AND";
		$sqlWhere .= " a.stateId='".$selection['stateId']."'";
	}
	unset($selection['stateId']);

	if(!empty($selection['country'])) {
		if(!empty($sqlWhere)) $sqlWhere .= " AND";
		$sqlWhere .= " a.country='".$selection['country']."'";
	}
	unset($selection['country']);

	if(!empty($selection['justIntActivities'])) {
		if(!empty($sqlWhere)) $sqlWhere .= " AND";
		$sqlWhere .= " ia.iaid IS NOT NULL";
	}
	unset($selection['justIntActivities']);

	if(!empty($selection['continent'])) {
		if(!empty($sqlWhere)) $sqlWhere .= " AND";
		$sqlWhere .= " ia.continent='".$selection['continent']."'";
	}
	unset($selection['continent']);

	$sqlWhere2 = stringHelper::makeSqlWhereByArray($selection, 'AND',
		false, 'c.', true);

	if(!empty($sqlWhere2)) $sqlWhere2 .= " AND";
	$sqlWhere2 .= $sqlWhere;


	$rs = $db->query(
		"SELECT DISTINCT c.* FROM ".$this->table('company')." AS c".
		" INNER JOIN ".$this->table('addressMap')." AS amap".
		" ON amap.cid = c.cid".
		" INNER JOIN ".$this->table('address')." AS a".
		" ON amap.adid = a.adid".
		" LEFT OUTER JOIN ".$this->table('intActivity')." AS ia".
		" ON ia.cid = c.cid".
		" WHERE ".$sqlWhere2.
		" ORDER BY c.name");
	return $rs;
}
// -----------------------------------------------------------------------------


} // end class

