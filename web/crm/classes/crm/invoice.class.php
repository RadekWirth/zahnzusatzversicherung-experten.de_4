<?php

class invoice extends crm {

// -----------------------------------------------------------------------------
public $oneMonthInSec;
// -----------------------------------------------------------------------------
public function __construct() {
	$this->oneMonthInSec = 30 * 24 * 60 * 60;
}
// -----------------------------------------------------------------------------
public function generateOnetimeItems() {
	$db = $this->getDb();

	$today = date('Y-m-d');
	$genStartDate = null;

	$ars = $db->query("SELECT a.* FROM ".$this->table('assignment')." AS a".
		" LEFT OUTER JOIN ".$this->table('invoiceItem')." AS ii".
		" ON a.amid=ii.amid".
		" WHERE period=0 AND deleted=0 AND ii.iid IS NULL");

	while($arow = $ars->fetch()) {
		$iiData = array(
			'amid' => $arow['amid'],
			'caption' => $arow['caption'],
			'description' => $arow['description'],
			'deliveryDate' => $arow['creationDate'],
			'price' => $arow['price'],
			'tax' => $arow['tax'],
		);
		$this->createInvoiceItem($iiData);
	}
}

public function generatePeriodicalItems() {
	$db = $this->getDb();

	$today = date('Y-m-d');
	$genStartDate = null;

	$ars = $db->query("SELECT * FROM ".$this->table('assignment').
		" WHERE period>0 AND deleted=0");

	while($arow = $ars->fetch()) {
		// set initial generate start date
		$genStartDate = $arow['periodStartDate'];

		// get latest invoice item to eventually correct $genStartDate
		$iirs = $db->query(
			"SELECT billingTill FROM ".$this->table('invoiceItem').
			" WHERE amid='".$arow['amid']."'".
			" ORDER BY billingTill DESC LIMIT 1");

		if($iirs->numRows()) {
			$iirow = $iirs->fetch();
			$genStartDate = $iirow['billingTill'];
		}

		// set initial generate end date
		$genEndDate = stringHelper::calcDate($genStartDate, $arow['period']);

		// set initial end time for next while-loop
		$loopEndTime = time() + $this->oneMonthInSec;

		if($arow['periodEndDate'] != '0000-00-00' &&
			strtotime($genEndDate) > strtotime($arow['periodEndDate'])) {
			$genEndDate = $arow['periodEndDate'];
		}

		// CREATE INVOICE ITEMS IN PAST
		while(strtotime($genStartDate) < strtotime($genEndDate) &&
			strtotime($genEndDate) < $loopEndTime) {

			// check if genEndDate is larger than periodEndDate
			if($arow['periodEndDate'] != '0000-00-00' &&
				strtotime($genEndDate) > strtotime($arow['periodEndDate'])) {
				$genEndDate = $arow['periodEndDate'];
				// last round
				$loopEndTime = 0;
			}


			$iiData = array(
				'amid' => $arow['amid'],
				'caption' => $arow['caption'],
				'description' => $arow['description'],
				'billingFrom' => $genStartDate,
				'billingTill' => $genEndDate,
				'price' => $arow['price'],
				'tax' => $arow['tax'],
			);

// 			echo "<br />gen past: ".$genStartDate." - ".$genEndDate;
			$this->createInvoiceItem($iiData);
			$genStartDate = $genEndDate;
			$genEndDate = stringHelper::calcDate($genStartDate,
				$arow['period']);
		}

		// CREATE INVOICE ITEM AHEAD (FUTURE)
		if(!empty($arow['periodBillAhead']) &&
			strtotime($genStartDate) < strtotime($genEndDate) &&
			strtotime($genEndDate) <
				time() + (($arow['period'] + 1) * $this->oneMonthInSec)) {

			// check if genEndDate is larger than periodEndDate
			if($arow['periodEndDate'] != '0000-00-00' &&
				strtotime($genEndDate) > strtotime($arow['periodEndDate'])) {
				$genEndDate = $arow['periodEndDate'];
			}

			$iiData = array(
				'amid' => $arow['amid'],
				'caption' => $arow['caption'],
				'description' => $arow['description'],
				'billingFrom' => $genStartDate,
				'billingTill' => $genEndDate,
				'price' => $arow['price'],
				'tax' => $arow['tax'],
			);
// 			echo "<br />gen ahead: ".$genStartDate." - ".$genEndDate;
 			$this->createInvoiceItem($iiData);
		}
	}

}

private function createInvoiceItem($data) {
	$db = $this->getDb();
	$date = date("Y-m-d H:i:s");
	$valuesSql = stringHelper::arrayToSql($data);
	$rs = $db->query("INSERT INTO ".$this->table('invoiceItem').
	" SET ".$valuesSql.", uid='".user::getUid().
	"', creationDate='".$date."'");
	return $db->insertId();
}

public function getInvoiceItemsRs($idCol) {
	$db = $this->getDb();
	$rs = $db->query("SELECT ii.*, a.".$idCol." FROM ".
		$this->table('invoiceItem')." AS ii".
		" INNER JOIN ".$this->table('assignment')." AS a".
		" ON a.amid = ii.amid".
		" WHERE ii.iid=0 AND a.".$idCol." !=0".
		" ORDER BY cid, pid, creationDate DESC");
	return $rs;
}

public function getInvoiceItemsByIid($iid) {
	$db = $this->getDb();
	$ret = array();
	$rs = $db->query("SELECT * FROM ".$this->table('invoiceItem').
		" WHERE iid='".$iid."' ORDER BY billingFrom");
	while($row = $rs->fetch()) {
		$ret[] = $row;
	}
	return $ret;
}

public function generateInvoices() {
	$db = $this->getDb();
	$iids = array();
	$newInvoices = false;

	$iirs = $db->query("SELECT ii.iiid, ii.price, ii.tax, a.cid, a.pid FROM ".
		$this->table('invoiceItem')." AS ii".
		" INNER JOIN ".$this->table('assignment')." AS a".
		" ON a.amid = ii.amid".
		" WHERE ii.iid=0 ORDER BY a.cid, a.pid");

	while($item = $iirs->fetch()) {
		$newInvoices = true;
		$iid = $this->addItemToInvoice($item);
		if(array_search($iid, $iids) === FALSE) {
			$iids[] = $iid;
		}
	}

	// close generated invoices by setting the status from 'generate' to 'wait'
	if($newInvoices) {
	$db->query("UPDATE ".$this->table('invoice')." SET status='wait'".
		" WHERE ".stringHelper::makeSqlWhere('iid', $iids));
	}
}

private function addItemToInvoice($item) {
	$db = $this->getDb();
	$iid = null;

	// detect owner data
	$idCol = (empty($item['pid']))?'cid':'pid';
	$id = $item[$idCol];

	// look for invoice to add item
	$irs = $db->query("SELECT iid FROM ".$this->table('invoice').
		" WHERE ".$idCol."='".$id."' AND status='generate'".
		" ORDER BY creationDate DESC LIMIT 1");

	// ok i found a invoice, which is open for adding items
	if($irow = $irs->fetch()) {
		$iid = $irow['iid'];
	}
	// no generating invoice existing, create one
	else {
		$iid = $this->createInvoice($idCol, $id);
	}

	// connect item with invoice
	$db->query("UPDATE ".$this->table('invoiceItem').
		" SET iid='".$iid."'".
		" WHERE iiid='".$item['iiid']."' LIMIT 1");

	// add item price to invoice price sum
	$db->query("UPDATE ".$this->table('invoice').
		" SET price=price+".$item['price'].
		" WHERE iid='".$iid."' LIMIT 1");

	return $iid;
}

private function createInvoice($idCol, $id) {
	$db = $this->getDb();
	$data['number'] = $this->getNewInvoiceNr();
	$data['numberText']='R0'.date('y').sprintf('%04s',
		$this->getNewInvoiceNr());
	$data['billingDate'] = date('Y-m-d');
	$data[$idCol] = $id;

	$valuesSql = stringHelper::arrayToSql($data);
	$rs = $db->query("INSERT INTO ".$this->table('invoice').
	" SET ".$valuesSql.", uid='".user::getUid().
	"', creationDate='".date('Y-m-d H:i:s')."'");
	return $db->insertId();
}

public function getInvoicesByStatusRs($status='wait') {
	$db = $this->getDb();
	$rs = $db->query("SELECT * FROM ".$this->table('invoice').
		" WHERE status='".$status."'".
		" ORDER BY iid DESC");
	return $rs;
}

public function getInvoice($iid) {
	$db = $this->getDb();
	$rs = $db->query("SELECT * FROM ".$this->table('invoice').
		" WHERE iid='".$iid."'");
	return $rs->fetch();
}

private function getNewInvoiceNr() {
	$db = $this->getDb();
	$rs = $db->query("SELECT MAX(number) AS max FROM ".
		$this->table('invoice'));
	$row = $rs->fetch();
	return ++$row['max'];
}
// -----------------------------------------------------------------------------
public function setInvoiceStatus($iid, $status) {
	$db = $this->getDb();
	$paySql = '';
	if($status == 'payed') {
		$paySql = ", payDate='".date('Y-m-d')."'";
	}
	if($status == 'sent') {
		$paySql = ", payDate='0000-00-00'";
	}
	$db->query("UPDATE ".$this->table('invoice').
		" SET status='".$status."'".$paySql." WHERE iid='".$iid."'");
	return $db->affectedRows();
}

public function setInvoiceCaption($iid, $caption) {
	$db = $this->getDb();
	$db->query("UPDATE ".$this->table('invoice').
		" SET caption='".$caption."' WHERE iid='".$iid."'");
	return $db->affectedRows();
}
// -----------------------------------------------------------------------------
public function generateInvoicePdfs() {
	$rs = $this->getInvoicesByStatusRs('wait');
	while($row = $rs->fetch()) {
		$this->createPdf($row['iid']);
	}
}

public function getInvoicePdf($iid) {
	$inv = $this->getInvoice($iid);
	$outpath = '../files/'.user::getProject().'/invoices/';
	$outfile = $inv['numberText'].'.pdf';
	$file = $outpath.$outfile;

	//if(!file_exists($file)) {
		$this->createPdf($iid);
	//}

	// send file to browser
	if(headers_sent() || ob_get_length()) {
		$this->Error('Some data has already been output, can\'t send PDF file');
	}
	header('Content-Type: application/pdf');
	header('Content-Length: '.filesize($file));
	header('Content-Disposition: attachment; filename="'.$outfile.'"');
	header('Cache-Control: private, max-age=0, must-revalidate');
	header('Pragma: public');

	$fp = fopen($file,"r") ;
	while(!feof($fp)) {
		$buff = fread($fp,4096);
		print $buff;
	}
}

public function createPdf($iid) {
	if(empty($iid)) {
	echo "createPdf: no $iid";
	exit;
	}
	$crm = new crm();
	$inv = $this->getInvoice($iid);
	$item = $crm->getInfo($inv);
	$recipient =
		$item['nameFull']."\n".
		$item['address']['street']."\n".
		$item['address']['postcode']." ".
		$item['address']['city'];

	$fontSize = 10;
	$fontSizeCaption = 15;
	$fontFamily = 'Arial';
	$fontLineHeight = 5;
	$fontColor = 65;
	$fontColorGrey = 155;
	$fontColorGreyLight = 181;
	$border = 0;
	$tableWidthCol1 = 80;
	$tableWidthCol2 = 40;
	$tableWidthCol3 = 35;

	$tablePaddingCol1 = 10;

	$widthSum = $tableWidthCol1 +
		$tableWidthCol2 +
		$tableWidthCol3 +
		$tablePaddingCol1;

	$tableSumWidthCol = $tableWidthCol1+$tablePaddingCol1+$tableWidthCol2;

	$leftMargin = 30;

	$outpath = '../files/'.user::getProject().'/invoices/';
	$outfile = $inv['numberText'].'.pdf';
	if(!file_exists($outpath)) {
		mkdir($outpath, 0777, true);
	}

	// caption
	$caption = "Rechnung";
	if(!empty($inv['caption'])) {
		$caption = $inv['caption'];
	}

	$pdf=new FPDF();
	$pdf->AddPage();

	// logo
	$pdf->Image('files/invoice-templates/standard/logo1_3.gif',
		$leftMargin+$tableWidthCol1+$tablePaddingCol1, 35, 45);

 	// top margin hack
 	$pdf->Cell(0, 20, '', 0, 2);

	$pdf->SetLeftMargin($leftMargin);

	// recipient
	$pdf->SetTextColor($fontColor);
	$pdf->SetFont($fontFamily, '', $fontSize, 0, 0);
	$pdf->Cell(0, 30, '', 0, 2);
	$pdf->MultiCell(50, $fontLineHeight, $recipient);

	// caption
	$pdf->Cell(0, 20, '', 0, 2);
	$pdf->SetFont($fontFamily, '', $fontSizeCaption, 0, 2);
	$pdf->SetTextColor($fontColorGrey);
	$pdf->Cell(40,10, $caption, $border, 2);

	// text
	$text = 'Rechnung '.$inv['numberText'].' vom '.
		stringHelper::makeGermanDate($inv['billingDate']);
	$pdf->SetFont($fontFamily, '', $fontSize, 0, 2);
	$pdf->SetTextColor($fontColor);
	$pdf->Cell(100, $fontLineHeight, $text, $border, 2);

	// print table head row
	$pdf->SetTextColor($fontColorGrey);
	$pdf->Cell(0, 8, '', 0, 2);
	$pdf->Cell($tableWidthCol1, $fontLineHeight, 'Leistung', '');
	$pdf->Cell($tablePaddingCol1, $fontLineHeight, '', '');
	$pdf->Cell($tableWidthCol2, $fontLineHeight, 'Zeitraum', '');
	$pdf->Cell($tableWidthCol3, $fontLineHeight, 'Nettobetrag', '', 1, 'R');
	$pdf->SetLineWidth(0.2);
	$pdf->SetDrawColor($fontColorGreyLight);
	$pdf->Line($leftMargin, $pdf->GetY()+1, $leftMargin+$widthSum,
		$pdf->GetY()+1);

	// print items
	$pdf->Cell(0, 2, '', 0, 2);
	$pdf->SetTextColor($fontColor);
	$iarray = $this->getInvoiceItemsByIid($inv['iid']);
	$taxTypes = 0;
	$taxTemp = -1;
	if(count($iarray))
	foreach($iarray as $row) {
	if($row['tax'] != $taxTemp) {
		$taxTypes++;
		$taxTemp = $row['tax'];
	}
	$text = $row['caption'].': '.$row['description'];
	$textFromTill = ($row['billingFrom']=='0000-00-00')?
		stringHelper::makeGermanDate($row['deliveryDate']):
		stringHelper::makeGermanDate($row['billingFrom']).' - '.
		stringHelper::makeGermanDate($row['billingTill']);
	$textPrice = enviro::formatPrice($row['price']);
	$pdf->Cell(1, 2, '', 0, 1);
	$pdf->MultiCell($tableWidthCol1, $fontLineHeight, $text, 0, 'L');
	$pdf->SetXY($tableWidthCol1+$leftMargin+$tablePaddingCol1,
		$pdf->GetY()-$fontLineHeight);
	$pdf->Cell($tableWidthCol2, $fontLineHeight, $textFromTill , 0);
	$pdf->Cell($tableWidthCol3, $fontLineHeight, $textPrice, 0, 1, 'R');
	}

	// CHECK TAX, i can only handle items with same tax
	if($taxTypes != 1) {
		echo "tax error: i can only handle items with same tax.";
		echo "<br />types: $taxTypes; last type: $taxTemp";
		exit;
	}

	// print sum
	$textPrice = enviro::formatPrice($inv['price']);
	$textTax = enviro::formatPrice($inv['price'] * $taxTemp / 100);
	$textSum = enviro::formatPrice($inv['price'] * (1+($taxTemp / 100)));

	$pdf->Line($leftMargin, $pdf->GetY()+2, $leftMargin+$widthSum,
		$pdf->GetY()+2);
	$pdf->Cell(0, 5, '', 0, 2);

	$pdf->Cell($tableSumWidthCol, $fontLineHeight, 'Summe', 0, 0, 'R');
	$pdf->Cell($tableWidthCol3, $fontLineHeight, $textPrice, 0, 1, 'R');

	$pdf->Cell($tableSumWidthCol, $fontLineHeight,
		'MwSt '.$taxTemp.' %', 0, 0, 'R');
	$pdf->Cell($tableWidthCol3, $fontLineHeight, $textTax, 0, 1, 'R');

	$pdf->SetFont($fontFamily, 'B');
	$pdf->Cell($tableSumWidthCol, $fontLineHeight, 'Gesamtsumme', 0, 0, 'R');
	$pdf->Cell($tableWidthCol3, $fontLineHeight, $textSum, 0, 1, 'R');
	$pdf->SetFont($fontFamily, '');

	// footer
	$pdf->SetY(-50);
	$pdf->Image('files/invoice-templates/standard/footer.png',
		$leftMargin, null, $widthSum+10);

	// save file
	$pdf->Output($outpath.$outfile);
}
// -----------------------------------------------------------------------------
public function sendWaitingInvoices() {
	$db = $this->getDb();
	$crm = new crm();
	$this->generateInvoicePdfs();
	$invoiceMailText = pref::get('invoiceMailText', user::getUid());
	$emailSignature = pref::get('emailSignature', user::getUid());
	$ids = array();
	$rs = $this->getInvoicesByStatusRs('wait');
	while($row = $rs->fetch()) {
		$outpath = '../files/'.user::getProject().'/invoices/';
		$outfile = $row['numberText'].'.pdf';
		$file = array('path' => $outpath.$outfile,
			'type' => 'application/pdf',
			'name' => $outfile);
		$info = $crm->getInfo($row);
		$mailText = str_replace('#greeting#', $info['greeting'],
			$invoiceMailText);
		$mailText .= $emailSignature;

		//$to = 'ef@pagery.de';
		$to = $info['contact']['email'];
		if(!empty($to)) {
			$crm->sendMailWithFile($to, 'Rechnung '.$row['numberText'],
				$mailText, $file, true);
			$ids[] = $row['iid'];
		}
		else {
			echo "<br />Not sent, no Email: ".$row['numberText'].": ".$to;
		}
	}

	// update status to sent
	if(count($ids)) {
	$sqlWhere = stringHelper::makeSqlWhere('iid', $ids, 'OR');
	$db->query("UPDATE ".$this->table('invoice').
		" SET status='sent', sendDate='".date('Y-m-d H:i:s')."'".
 		" WHERE ".$sqlWhere);
	}
}
// -----------------------------------------------------------------------------

} // end class


?>
