<?php

class user extends crm {

// -----------------------------------------------------------------------------
public function __construct() {
}
// -----------------------------------------------------------------------------
public function add($data) {
	$ret = $this->checkName($data['name']);
	if($ret['err'] > 0) return 0;

	if(!$this->isNameFree($data['name'])) {
	return 0;
	}

	$ret = $this->checkPassword($data['password']);
	if($ret['err'] > 0) return 0;

	$db = self::getDb();
	$data['password'] = sha1($data['password']);
	$valuesSql = stringHelper::arrayToSql($data);
	$db->queryPDO("INSERT INTO ".$this->table('user')." SET ".$valuesSql);
	return $db->getLastInsertedId();
}

// -----------------------------------------------------------------------------
public function setPassword($password) {
	$db = self::getDb();
	$password = sha1($password);
	$db->queryPDO("UPDATE ".$this->table('user').
		" SET password='".$password."' WHERE uid='".$this->getUid()."'");
	return $db->getAffectedRows();
}

public function setEmail($email) {
	$db = self::getDb();
	$db->queryPDO("UPDATE ".$this->table('user').
		" SET email='".$email."' WHERE uid='".$this->getUid()."'");
	return $db->getAffectedRows();
}
// -----------------------------------------------------------------------------
public function masterAdd($data) {
	// NO INPUT CHECKS
	// USE THIS FUNCTION WITH CARE
	// used by importFromStm class
	$db = $this->getDb();
	$data['password'] = sha1($data['password']);
	$valuesSql = stringHelper::arrayToSql($data);
	$db->queryPDO("INSERT INTO ".$this->table('user')." SET ".$valuesSql);
	return $db->getLastInsertedId();
}
// -----------------------------------------------------------------------------
public function login($project, $name, $password) {
	$db = self::getDb();
	#$password = sha1($password);

	// get project db data
	$rs = $db->queryPDO("SELECT pid FROM project WHERE name='".
		$project."' LIMIT 1");
	$pid = 1;
	$row = $db->fetchPDO($rs);
	if($row)
		$pid = $row['pid'];

	$rs = $db->queryPDO("SELECT * FROM project_dbdata WHERE pid='".
	$pid."' LIMIT 1");

	// save db data in session
	$dbData = $db->fetchPDO($rs);
	S::set('user', 'dbData', $dbData);
	

	$rs = $db->queryPDO("SELECT uid FROM ".
		$this->table('user')." WHERE name='".$name."' AND password='".
		sha1($password)."' LIMIT 1");
	$row = $db->fetchPDO($rs);	


	if(!$row) {
		return false;
	}

	// save login status in user table
	$rs = $db->queryPDO("UPDATE ".$this->table('user').
		" SET loginCount=loginCount+1, lastLoginTime=loginTime,".
		" loginTime='".date("Y-m-d H:i:s")."', logoutTime='0',".
		" ip='".getenv('REMOTE_ADDR')."', lastActionTime='".time()."'".
		" WHERE uid='".$row['uid']."' LIMIT 1");

	if(!$db->getAffectedRows()) {
		print $db->getQuery()."no affected rows";		
		return false;
	}

	// save login status in session
	S::set('user', 'login', array('uid' => $row['uid'],
		'project' => $project));

	return true;
}


// -----------------------------------------------------------------------------
public function logout($uid=0) {
	$db = self::getDb();
	if(empty($uid)) {
		$uid = user::getUid();
	}

	// save logout status in user table
	$rs = $db->queryPDO("UPDATE ".$this->table('user').
		" SET logoutTime='".date("Y-m-d H:i:s")."'".
		" , lastActionTime=0 WHERE uid='".$uid."' LIMIT 1");

	S::set('user', 'login', null);

	if(!$db->getAffectedRows()) {
		return false;
	}

	return true;
}


public function checkLogin($uid=0) {
	$db = self::getDb();

	if(empty($uid)) {
		$login = S::get('user', 'login');
		if(empty($login)) {
// 			echo "session cookie empty"; exit;
			return false;
		}
		$uid = $login['uid'];
	}

	$rs = $db->queryPDO("SELECT ip, logoutTime, lastActionTime FROM ".
		$this->table('user')." WHERE uid='".$uid."' LIMIT 1");
	$row = $db->fetchPDO($rs);

	$diff = (time() - $row['lastActionTime']) / 60;
	if($diff > 240) {
		$this->logout($uid);
// 		echo "time exceeded: ".$diff." min"; exit;
		return false;
	}

	if($row['ip'] != getenv('REMOTE_ADDR')) {
		$this->logout($uid);
// 		echo "ip changed: ".$row['ip']." != ".getenv('REMOTE_ADDR'); exit;
		return false;
	}

	$rs = $db->queryPDO("UPDATE ".$this->table('user').
		" SET lastActionTime='".time()."'".
		" WHERE uid='".$uid."' LIMIT 1");

	return true;
}


public function isUserLoggedIn($uid) {
	$db = self::getDb();

	$rs = $db->queryPDO("SELECT ip, lastActionTime, adminLoginTime FROM ".
		$this->table('user')." WHERE uid='".$uid."' LIMIT 1");
	$row = $db->fetchPDO($rs);

	if(!empty($row['adminLoginTime'][0])) {
		return false;
	}

	if(empty($row['lastActionTime'])) {
		return false;
	}

	$diff = (time() - $row['lastActionTime']) / 60;
	if($diff > 360) {
		$this->logout($uid);
		return false;
	}

	return true;
}
// -----------------------------------------------------------------------------
public static function checkPassword($password) {
	if(strlen($password)<5) {
	return array('err' => 1, 'errmsg' => 'password too short',
		'errclass' => 'user');
	}

	if(strlen($password)>50) {
	return array('err' => 1, 'errmsg' => 'password too long',
		'errclass' => 'user');
	}

	$variety=0;
	$s = stringHelper::analyzeString($password);
	if($s['lower']>0)	$variety += (0.5 * $s['lower']);
	if($s['numeric']>0)	$variety += (0.75 * $s['numeric']);
	if($s['upper']>0)	$variety += (0.75 * $s['upper']);
	if($s['misc']>0)	$variety += (1.5 * $s['misc']);

	// enough security for 5 chars is min variety of 3.5
	if($variety < 3.5) {
	return array('err' => 1, 'errmsg' => 'password too easy',
		'errclass' => 'user');
	}

	return array('err' => 0);
}

public static function checkName($name) {
	if(strlen($name)<5) {
	return array('err' => 1, 'errmsg' => 'username too short',
		'errclass' => 'user');
	}

	if(strlen($name)>25) {
	return array('err' => 2, 'errmsg' => 'username too long',
		'errclass' => 'user');
	}

	if($name == 'admin' || $name == 'root' || $name == 'adminstrator') {
	return array('err' => 3, 'errmsg' => 'username not valid',
		'errclass' => 'user');
	}

	if(!stringHelper::isVeryCleanString($name)) {
	return array('err' => 4, 'errmsg' => 'username not valid',
		'errclass' => 'user');
	}

	return array('err' => 0);
}

public function isNamefree($name) {
	$db = self::getDb();
	$rs = $db->queryPDO("SELECT uid FROM ".$this->table('user').
		" WHERE name = '".$name."' LIMIT 1");
	return ($db->getNumRows()>0)?false:true;
}
// -----------------------------------------------------------------------------
public static function getUid() {
	$login = S::get('user', 'login');
	return $login['uid'];
}

public static function getProject() {
	$login = S::get('user', 'login');
	return $login['project'];
}
// -----------------------------------------------------------------------------
public function getUserInfo($uid) {
	$db = self::getDb();
	$rs = $db->queryPDO("SELECT * FROM ".
		$this->table('user')." WHERE uid='".$uid."' LIMIT 1");
	$row = $db->fetchPDO($rs);
	return $row;
}

public function getUserType($uid) {
	$db = self::getDb();
	$rs = $db->queryPDO("SELECT type FROM ".$this->table('user').
		" WHERE uid='".$uid."' LIMIT 1");
	$row = $db->fetchPDO($rs);
	return $row['type'];
}

public function getUidByName($username) {
	$db = self::getDb();
	$rs = $db->queryPDO("SELECT uid FROM ".$this->table('user').
		" WHERE name='".$username."' LIMIT 1");
	$row = $db->fetchPDO($rs);
	return $row['uid'];
}

public function getNameByUid($uid) {
	$db = self::getDb();
	$rs = $db->queryPDO("SELECT name FROM ".$this->table('user').
		" WHERE uid='".$uid."' LIMIT 1");
	$row = $db->fetchPDO($rs);
	return $row['name'];
}
// -----------------------------------------------------------------------------
} // end class

