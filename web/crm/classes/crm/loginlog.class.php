<?php

class loginlog extends crm {

// -----------------------------------------------------------------------------
public function __construct() {

}
// -----------------------------------------------------------------------------
public function isIpBlocked($ip) {
	$this->cleanUp();
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT tries FROM ".$this->table('loginlog').
		" WHERE ip='".$ip."' LIMIT 1");
	$row = $db->fetchPDO($rs);
	if($row && $row['tries'] >= 3) {
		return true;
	}
	return false;
}

public function logSuspiciousIp($ip) {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT tries FROM ".$this->table('loginlog').
		" WHERE ip='".$ip."' LIMIT 1");
	if($db->getNumRows($rs)) {
		$rs = $db->queryPDO("UPDATE ".$this->table('loginlog').
			" SET tries=tries+1, loginTryTime='".date("Y-m-d H:i:s")."', ".
			" loginTryTimestamp='".time()."' WHERE ip='".$ip."' LIMIT 1");
	}
	else {
		$rs = $db->queryPDO("INSERT INTO ".$this->table('loginlog').
			" SET tries=1, loginTryTime='".date("Y-m-d H:i:s")."', ".
			" loginTryTimestamp='".time()."', ip='".$ip."'");
	}
}
// -----------------------------------------------------------------------------
private function cleanUp() {
	// delete all tries older than 5 minutes
	$db = $this->getDb();
	$rs = $db->queryPDO("DELETE FROM ".$this->table('loginlog').
		" WHERE loginTryTimestamp < '".(time()-300)."'");
}

// -----------------------------------------------------------------------------
}

?>