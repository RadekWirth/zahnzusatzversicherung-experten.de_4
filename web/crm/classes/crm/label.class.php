<?php

class label extends crm {

// -----------------------------------------------------------------------------
public function __construct() {
}


public function getLabels($type, $orderBy=null, $currentStatus=null) {
	$orderBy = "";
	$where = "";

	if(empty($orderBy)) {
		$orderBy = 'name';
	}
	if(isset($currentStatus))
	{
		if($type == 'contractStatus' && $currentStatus < 48)
		{
			$where = " AND lid <> 49 AND lid <> 50 ";
		}
	}
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT lid, name FROM ".$this->table('label').
		" WHERE type= '".$type."' ".$where."  ORDER BY ".$orderBy);
	return $rs;
}


public function getLabelName($lid) {
	if(empty($lid)) return '';
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT name FROM ".$this->table('label').
		" WHERE lid= '".$lid."'");
	$row = $db->fetchPDO($rs);
	return $row['name'];
}

public function getLabelId($name) {
	if(empty($name)) return 0;
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT lid FROM ".$this->table('label').
		" WHERE name= '".$name."'");
	$row = $db->fetchPDO($rs);
	return $row['lid'];
}

public function getYesNoName($bool) {
	if($bool) {
		return L::_(100);
	}else {
		return L::_(99);
	}
}

public function resolveLabels($array) {
	$label = new label();
	if(count($array)) {
		foreach($array as $key => &$value) {
			if(strstr($key, 'Lid')) {
				$value = $label->getLabelName($value);
			}
		}
	}
	return $array;
}

// -----------------------------------------------------------------------------

} // end class
