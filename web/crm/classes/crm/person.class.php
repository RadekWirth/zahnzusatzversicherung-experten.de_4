<?php

class person extends crm {

// -----------------------------------------------------------------------------
public function __construct() {

}
// -----------------------------------------------------------------------------
public function add($data) {
	$db = $this->getDb();
	$data = stringHelper::arrayUcFirst($data);
	$valuesSql = stringHelper::arrayToSql($data);
	if(!empty($valuesSql)) {
		$db->queryPDO("INSERT INTO ".$this->table('person')." SET ".$valuesSql);
		return $db->getLastInsertedId();
	}
	return 0;
}

public function set($pid, $data) {
	$db = $this->getDb();
	$data = stringHelper::arrayUcFirst($data);
	$valuesSql = stringHelper::arrayToSql($data);
	$db->queryPDO("UPDATE ".$this->table('person')." SET ".$valuesSql.
		" WHERE pid='".$pid."'");
	return $db->getAffectedRows();
}

public function del($pid) {
	$db = $this->getDb();
	// delete person
	$db->queryPDO("DELETE FROM ".$this->table('person').
		" WHERE pid='".$pid."'");
	// delete mapping if exists
	$db->queryPDO("DELETE FROM ".$this->table('personMap').
		" WHERE pid='".$pid."'");


	// delete contact and adresses
	$crm = new crm();
	$crm->deleteAddressAll('pid', $pid);
	$crm->deleteContactAll('pid', $pid);
	// delete notes
	$crm->delNotes('pid', $pid);
	return $db->getAffectedRows();
}

public function addMapping($pid, $cid, $data) {
	$db = $this->getDb();
	$valuesSql = stringHelper::arrayToSql($data);
	$db->queryPDO("INSERT INTO ".$this->table('personMap')
		." SET ".$valuesSql.", pid='".$pid."', cid='".$cid."'");
}

public function getMapping($pid, $cid) {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT * FROM ".$this->table('personMap')
		." WHERE pid='".$pid."' AND cid='".$cid."'");
	return $db->fetchPDO($rs);
}

public function setMapping($pid, $cid, $data) {
	$db = $this->getDb();
	$data = stringHelper::arrayUcFirst($data);
	$valuesSql = stringHelper::arrayToSql($data);
	$db->queryPDO("UPDATE ".$this->table('personMap')." SET ".$valuesSql.
		" WHERE pid='".$pid."' AND cid='".$cid."'");
	return $db->getAffectedRows();
}


public function get($pid, $removeIds=false) {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT * FROM ".$this->table('person').
		" WHERE pid='".$pid."'");
	$row = $db->fetchPDO($rs);
	if($removeIds) {
		unset($row['cid']);
		unset($row['uid']);
	}
	return $row;
}


public function getCidByPid($pid) {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT cid FROM ".$this->table('personMap').
		" WHERE pid='".$pid."'");
	$row = $db->fetchPDO($rs);
	return $row['cid'];
}

private function updatePersonHashes()
{
	$db = $this->getDb();

	$sql = "UPDATE crm_person SET `hash` = md5(CONCAT(forename, surname, cast(birthdate as char(10)))) where (hash is null or hash = '');";
	$db->queryPDO($sql);
}

/* used for creating reminders */
public function getPersonsByPeriod($endDate, $startDate=null) {
	$now = time();

	if(!isset($startDate)) $startDate = $endDate; 	
	$sql_css_aktion = 'CSS-Aktion vom 01.12.2014';	

	/* first update hashes */
	$this->updatePersonHashes();

	$db = $this->getDb();

	$sqltemp = 'DROP TEMPORARY TABLE IF EXISTS tmp_allPersonsHaveInsurance;';
	$rstemp = $db->queryPDO($sqltemp);

	$sqltemp = '	CREATE TEMPORARY TABLE tmp_allPersonsHaveInsurance
			SELECT person.forename, person.surname, person.birthdate, md5(CONCAT(person.forename, person.surname, cast(person.birthdate as char(10)))) as hash FROM crm_person as person
			INNER JOIN wzm_contract ON (person.pid = wzm_contract.pid and (wzm_contract.contractStatusLid = 49 or wzm_contract.contractStatusLid = 50 or (wzm_contract.contractStatusLid = 48 and wzm_contract.addNotes = \''.$sql_css_aktion.'\')))
			WHERE 1=1
			GROUP BY person.forename, person.surname, person.birthdate;';
	$rstemp = $db->queryPDO($sqltemp);
	
	
	$sqltemp = '  CREATE INDEX tmp_hash on tmp_allPersonsHaveInsurance (hash) USING BTREE;';
	$rstemp = $db->queryPDO($sqltemp);


	$sql ='	SELECT 
			min( person.pid ) as pid, contract.idco, contract.dateSent, person.forename, person.surname, person.birthdate, person.hash
			FROM wzm_contract AS contract
			INNER JOIN crm_person AS person ON contract.pid = person.pid
			LEFT JOIN wzm_reminder as reminder on reminder.hash = person.hash
			WHERE contract.dateSent between \''.$startDate.'\' AND \''.$endDate.'\'
			AND contract.contractStatusLid = 48
			AND reminder.idco is null
			AND contract.pid not in (SELECT cp.pid FROM crm_person cp INNER JOIN tmp_allPersonsHaveInsurance tmp ON cp.`hash` = tmp.`hash` )
			group by person.forename, person.surname, person.birthdate, contract.idco, contract.dateSent;';



	$rs = $db->queryPDO($sql);
	while($row = $db->fetchPDO($rs)) {
		$ret['result'][] = array("pid"=>$row['pid'], "idco"=>$row['idco'], "dateSent"=>$row['dateSent'], "forename"=>$row['forename'], "surname"=>$row['surname'], "birthdate"=>$row['birthdate'], "hash"=>$row['hash']);
	}
	return $ret;
}


public function getPidByName ($forename, $surename, $birthdate) {
	/* function needed 'cause people are not tracked by name, one customer can have multiple pid's */

	$sql = 'SELECT pid FROM '.$this->table('person').' WHERE forename = '.$forename.' AND surname = '.$surname.' AND birthdate = '.$birthdate;
	
	/* Not Needed for now */
}


/*
	the ordering of the result columns should not be changed
	because for ajax, the data is viewed as csv
	eg see myInternController::ajaxGetPersonsByCompany
*/
public function getByCompanyRs($cid, $removeIds=false) {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT * FROM ".$this->table('person')." AS p"
		." INNER JOIN ".$this->table('personMap')." AS map"
		." ON p.pid = map.pid"
		." WHERE map.cid='".$cid."'");
	return $rs;
}

public function getByCompanyFullArray($cid, $removeIds=false) {
	$db = $this->getDb();
	$crm = new crm();
	$ret = array();
	$rs = $db->queryPDO("SELECT * FROM ".$this->table('person')." AS p"
		." INNER JOIN ".$this->table('personMap')." AS map"
		." ON p.pid = map.pid"
		." WHERE map.cid='".$cid."'");
	while($row = $db->fetchPDO($rs)) {
		$p = array();
		$p['person'] = $this->resolve(label::resolveLabels($row));
		$p['contact'] = $crm->getContactPrimary('pid', $row['pid'], true);
		$p['address'] = $crm->resolveAddress($crm->getAddressPrimary('pid',
			$row['pid'], true));
		$ret[] = $p;
	}
	return $ret;
}

public function getSinglesRs($firstLetter=null, $removeIds=false) {
	$db = $this->getDb();
	$sqlWhere2 = '';
	if(!empty($firstLetter)) {
		if($firstLetter==1) {
			$sqlWhere2 = " AND p.surname REGEXP '^[[:digit:]]+'";
		}else {
			$sqlWhere2 = " AND p.surname LIKE '".$firstLetter."%'";
		}
		$where2 = " AND p.surname LIKE '".$firstLetter."%'";
	}
	$rs = $db->queryPDO("SELECT p.* FROM ".$this->table('person')." AS p"
		." WHERE 1=1 ".$sqlWhere2
		." ORDER BY p.surname ASC"
		." LIMIT 0, 150");
	return $rs;
}

public function getAllWithAssignmentsRs() {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT DISTINCT p.* FROM ".$this->table('person').
		"  AS p INNER JOIN ".$this->table('assignment')." AS a".
		" ON p.pid = a.pid".
		" WHERE a.assignmentStatusLid != '29'".
		" ORDER BY creationDate DESC");
	return $rs;
}

public function getAllWithImportanciesRs() {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT DISTINCT p.* FROM ".$this->table('person').
		" AS p INNER JOIN ".$this->table('notes')." AS n".
		" ON p.pid = n.pid".
		" WHERE n.importance > 0".
		" ORDER BY n.creationDate DESC");
	return $rs;
}

public function resolve($person) {
	$crm = new crm();
	$person['jobPositionId'] =
		$crm->getJobPositionName($person['jobPositionId']);
	return $person;
}

public function getAllRs() {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT * FROM ".$this->table('person')
		." ORDER BY surname");
	return $rs;
}

public function selectByIds($ids, $order='ASC') {
	$db = $this->getDb();
	$where = stringHelper::makeSqlWhereMany('pid', $ids);
	$rs = $db->queryPDO("SELECT * FROM ".$this->table('person')
		." WHERE ".$where." ORDER BY pid ".$order);
	return $rs;
}

public function getPidByMapping($type, $typeId) {
	$table = 'addressMap';
	$id = 'adid';
	if($type=='contact') {
		$table = 'contactMap';
		$id = 'ctid';
	}
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT * FROM ".$this->table($table)
		." WHERE ".$id."='".$typeId."'");
	$row = $rs->fetch();
	return $row['pid'];
}

// -----------------------------------------------------------------------------
public function search($keywords) {
	$db = $this->getDb();
	$resultPersons = array();

	$cols = array('p.forename', 'p.surname', 'p.nationString',
		'p.job', 'p.branch');
	$sqlWhere = stringHelper::makeSqlWhere($cols, $keywords, 'OR', true);
	$rs = $db->queryPDO("SELECT p.pid FROM ".$this->table('person')." AS p".
		" WHERE ".$sqlWhere." ORDER BY p.pid DESC LIMIT 1000");

	while($row = $db->fetchPDO($rs)) {
		$resultPersons[] = $row['pid'];
	}


	$cols = array('a.street', 'a.postcode', 'a.city');
	$sqlWhere = stringHelper::makeSqlWhere($cols, $keywords, 'OR', true);
	$rs = $db->queryPDO("SELECT a.adid FROM ".$this->table('address')." AS a".
		" WHERE ".$sqlWhere." ORDER BY a.adid DESC LIMIT 500");

	while($row = $db->fetchPDO($rs)) {
		$resultPersons[] = $this->getPidByMapping('address', $row['adid']);
	}


	$cols = array('c.phone', 'c.mobile', 'c.fax', 'c.email', 'c.homepage');
	$sqlWhere = stringHelper::makeSqlWhere($cols, $keywords, 'OR', true);
	$rs = $db->queryPDO("SELECT c.ctid FROM ".$this->table('contact')." AS c".
		" WHERE ".$sqlWhere." ORDER BY c.ctid DESC LIMIT 500");

	while($row = $db->fetchPDO($rs)) {
		$resultPersons[] = $this->getPidByMapping('contact', $row['ctid']);
	}


	return $resultPersons;
}
// ----------------------------------------------------------------------------
public function getFatherPid($childPid) {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT pid FROM ".$this->table('person').
		" WHERE isFatherOfPid='".$childPid."'");
	$row = $db->fetchPDO($rs);
	if(!empty($row['pid'])) {
		return $row['pid'];
	}
	else {
		return null;
	}
}
// ----------------------------------------------------------------------------


} // end class

