<?php

class crm {


protected function getDb() {
	return project::getDb();
}

protected function getMainDb() {
	return project::getDb();
}

protected function table($table) {
	$tables = array(
	'address' => 'crm_address',
	'addressMap' => 'crm_address_map',
	'assignment' => 'crm_assignment',
	'branches' => 'crm_branches',
	'comments' => 'crm_comments',
	'company' => 'crm_company',
	'contact' => 'crm_contact',
	'contactMap' => 'crm_contact_map',
	'countries' => 'crm_countries',
	'groups' => 'crm_groups',
	'groupMap' => 'crm_group_map',
	'intActivity' => 'crm_int_activity',
	'invoice' => 'crm_invoice',
	'invoiceItem' => 'crm_invoice_item',
	'jobpositions' => 'crm_jobpositions',
	'label' => 'crm_label',
	'loginlog' => 'loginlog',
	'notes' => 'crm_notes',
	'person' => 'crm_person',
	'personMap' => 'crm_person_map',
	'sendmail' => 'crm_sendmail',
	'states' => 'crm_states',
	'user' => 'crm_user',
	'userPrefs' => 'crm_user_prefs'
	);

	if(empty($tables[$table])) {
		return '**TABLE NOT FOUND**';
	}else {
		return $tables[$table];
	}
}
// -----------------------------------------------------------------------------
public function clearTables() {
	$dbc = $this->getDb();

// 	$dbc->queryPDO("TRUNCATE TABLE ".$this->table('user'));
// 	$dbc->queryPDO("TRUNCATE TABLE ".$this->table('address'));
// 	$dbc->queryPDO("TRUNCATE TABLE ".$this->table('addressMap'));
// 	$dbc->queryPDO("TRUNCATE TABLE ".$this->table('branches'));
// 	$dbc->queryPDO("TRUNCATE TABLE ".$this->table('company'));
// 	$dbc->queryPDO("TRUNCATE TABLE ".$this->table('contact'));
// 	$dbc->queryPDO("TRUNCATE TABLE ".$this->table('contactMap'));
// 	$dbc->queryPDO("TRUNCATE TABLE ".$this->table('groupMap'));
// 	$dbc->queryPDO("TRUNCATE TABLE ".$this->table('person'));
// 	$dbc->queryPDO("TRUNCATE TABLE ".$this->table('personMap'));
// 	$dbc->queryPDO("TRUNCATE TABLE ".$this->table('states'));
// 	$dbc->queryPDO("TRUNCATE TABLE ".$this->table('jobpositions'));
// 	$dbc->queryPDO("TRUNCATE TABLE ".$this->table('notes'));
}

// -----------------------------------------------------------------------------
// ADDRESSES
// -----------------------------------------------------------------------------
public function addAddress($data, $idCol, $id, $type='secondary') {
	$db = $this->getDb();
	$data = stringHelper::arrayUcFirst($data);
	$valuesSql = stringHelper::arrayToSql($data);
	
	if(!empty($valuesSql)) {
		$db->queryPDO("INSERT INTO ".$this->table('address')." SET ".$valuesSql);
		$adid = $db->getLastInsertedId();

		// add mapping
		$db->queryPDO("INSERT INTO ".$this->table('addressMap')
			." SET adid='".$adid."', ".$idCol."='".$id."', type='".$type."'");

		return $adid;
	}
	return 0;
}

public function getAddressPrimary($idCol, $id, $removeIds=false) {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT a.* FROM ".$this->table('address')." AS a"
		." INNER JOIN ".$this->table('addressMap')." AS map"
		." ON a.adid = map.adid"
		." WHERE map.".$idCol."='".$id."' AND map.type='primary'");
	$row = $db->fetchPDO($rs);
	if($removeIds) {
		unset($row['adid']);
		unset($row['cid']);
		unset($row['pid']);
		unset($row['type']);
	}
	return $row;
}

public function setAddress($data, $idCol, $id, $type) {
	$db = $this->getDb();
	$data = stringHelper::arrayUcFirst($data);
	$valuesSql = stringHelper::arrayToSql($data);
	$adid = $this->getAddressId($idCol, $id, $type);
	$db->queryPDO("UPDATE ".$this->table('address')." SET ".$valuesSql.
		" WHERE adid='".$adid."' LIMIT 1");
	return $db->getAffectedRows();
}


public function getAddressId($idCol, $id, $type=null) {
	$db = $this->getDb();
	$whereSql = '';
	if(!empty($type)) {
		 $whereSql= " AND type='".$type."'";
	}
	$rs = $db->queryPDO("SELECT adid FROM ".$this->table('addressMap')
		." WHERE ".$idCol."='".$id."'".$whereSql);
	$row = $db->fetchPDO($rs);
	return $row['adid'];
}

public function getAddressIdsRs($idCol, $id) {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT adid FROM ".$this->table('addressMap')
		." WHERE ".$idCol."='".$id."'");
	return $rs;
}


public function deleteAddressAll($idCol, $id) {
	$db = $this->getDb();
	$rs = $db->queryPDO("DELETE a, map"
		." FROM ".$this->table('address')." AS a"
		." INNER JOIN ".$this->table('addressMap')." AS map"
		." ON a.adid = map.adid"
		." WHERE map.".$idCol."='".$id."'");
	return $db->getAffectedRows();
}

public function resolveAddress($address) {
	$crm = new crm();
	$address['stateId'] = $crm->getStateName($address['stateId']);
	$address['country'] = $crm->getCountry($address['country']);
	return $address;
}


// -----------------------------------------------------------------------------
// CONTACT
// -----------------------------------------------------------------------------
public function addContact($data, $idCol, $id, $type='secondary') {
	$db = $this->getDb();
	$valuesSql = stringHelper::arrayToSql($data);
	if(!empty($valuesSql)) {
		$db->queryPDO("INSERT INTO ".$this->table('contact')." SET ".$valuesSql);
		$ctid = $db->getLastInsertedId();

		// add mapping
		$db->queryPDO("INSERT INTO ".$this->table('contactMap')
			." SET ctid='".$ctid."', ".$idCol."='".$id."', type='".$type."'");

		return $ctid;
	}
	return 0;
}

public function getContactPrimary($idCol, $id, $removeIds=false) {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT * FROM ".$this->table('contact')." AS c"
		." INNER JOIN ".$this->table('contactMap')." AS map"
		." ON c.ctid = map.ctid"
		." WHERE map.".$idCol."='".$id."' AND map.type='primary'");
	$row = $db->fetchPDO($rs);
	if($removeIds) {
		unset($row['ctid']);
		unset($row['cid']);
		unset($row['pid']);
		unset($row['type']);
	}
	return $row;
}


public function setContact($data, $idCol, $id, $type) {
	$db = $this->getDb();
	$valuesSql = stringHelper::arrayToSql($data);
	$ctid = $this->getContactId($idCol, $id, $type);
	$db->queryPDO("UPDATE ".$this->table('contact')." SET ".$valuesSql.
		" WHERE ctid='".$ctid."' LIMIT 1");
	return $db->getAffectedRows();
}


public function getContactId($idCol, $id, $type) {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT ctid FROM ".$this->table('contactMap')
		." WHERE ".$idCol."='".$id."' AND type='".$type."'");
	$row = $db->fetchPDO($rs);
	return $row['ctid'];
}

public function deleteContactAll($idCol, $id) {
	$db = $this->getDb();
	$rs = $db->queryPDO("DELETE c, map"
		." FROM ".$this->table('contact')." AS c"
		." INNER JOIN ".$this->table('contactMap')." AS map"
		." ON c.ctid = map.ctid"
		." WHERE map.".$idCol."='".$id."'");
	return $db->getAffectedRows();
}


// -----------------------------------------------------------------------------
// NOTE
// -----------------------------------------------------------------------------
public function addNote($idCol, $id, $text, $importance=0, $date=null) {
	if(empty($id) || empty($text)) {
		return -1;
	}
	$db = $this->getDb();
	if(empty($date)) {
		$date = date("Y-m-d H:i:s");
	}
	$db->queryPDO("INSERT INTO ".$this->table('notes').
		" SET ".$idCol."='".$id."', uid='".user::getUid()."', text='".$text."'".
		", importance='".$importance."', creationDate='".$date."'");
	return $db->getLastInsertedId();
}

public function duplicateNoteByPid($pid, $new_pid)
{
	$db = $this->getDb();
	$rs = $this->getNotes('pid', $pid);
	
	while($row = $db->fetchPDO($rs))
	{
		$this->addNote('pid', $new_pid, $row['text'], $row['importance'], $row['creationDate']);
	}
}

public function getLatestNote($idCol, $id) {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT * FROM ".$this->table('notes').
		" WHERE ".$idCol."='".$id."' ORDER BY creationDate DESC LIMIT 1");
	return $db->fetchPDO($rs);
}

public function getImportantNotesCount($idCol, $id) {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT COUNT('A') cnt FROM ".$this->table('notes').
		" WHERE ".$idCol."='".$id."' AND importance=1");
	return $rs;
}

public function getNotes($idCol, $id) {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT * FROM ".$this->table('notes').
		" WHERE ".$idCol."='".$id."' ORDER BY creationDate DESC");
	return $rs;
}

public function delNote($nid) {
	$db = $this->getDb();
	$rs = $db->queryPDO("DELETE FROM ".$this->table('notes').
		" WHERE nid='".$nid."' LIMIT 1");
	return $db->getAffectedRows();
}

public function delNotes($idCol, $id) {
	$db = $this->getDb();
	$rs = $db->queryPDO("DELETE FROM ".$this->table('notes').
		" WHERE ".$idCol."='".$id."'");
	return $db->getAffectedRows();
}

public function hasImportantNotes($idCol, $id) {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT nid FROM ".$this->table('notes').
		" WHERE ".$idCol."='".$id."' AND importance='1' LIMIT 1");
	return $db->getAffectedRows();
}


// UNUSED STUFF
// -----------------------------------------------------------------------------
// MAIL FUNC.
// -----------------------------------------------------------------------------
public function logSendMail($type, $id, $subject, $message) {
	$db = $this->getDb();
	$ids = ';'.$id.';';

	if($type=='cid') $idCol = 'recCids';
	else $idCol = 'recPids';

	$db->queryPDO("INSERT INTO ".$this->table('sendmail')
		." SET uid='".user::getUid()."'"
		.", ".$idCol."='".$ids."', subject='".$subject."'"
		.", message='".$message."', senddate='".date('Y-m-d H:i:s')."'");
}

public function sendMail($recipient, $subject, $message) {

	$pref = new pref();
	$user = new user();

	$fromString = $from = $pref->get('emailFrom', $user->getUid());
	$name = $pref->get('name', $user->getUid());

	if(!empty($name)) {
		$fromString = '"'.$name.'" <'.$from.'>';
	}
	$headers = "From: $fromString\n" .
			"Reply-To: $from\n".
			"MIME-Version: 1.0\n".
			"Content-Type: text/plain;\n\t charset=\"ISO-8859-15\"\n".
			"Content-Transfer-Encoding: 8bit\n".
			"X-Sender: ".$from."\n".
			"X-Mailer: PHP\n".
			"X-Priority: 3\n";
	$parameter = '-f '.$from;
	return mail($recipient, $subject, $message, $headers, $parameter);
}

public function sendMailWithFile($recipient, $subject, $message, $file,
	$readReceipt=false) {

	$pref = new pref();
	$user = new user();

	$fromString = $from = $pref->get('emailFrom', $user->getUid());
	$name = $pref->get('name', $user->getUid());

	if(!empty($name)) {
		$fromString = '"'.$name.'" <'.$from.'>';
	}

	$headers = "From: $fromString\n" .
			"Reply-To: $from\n".
			"X-Sender: ".$from."\n".
			"X-Mailer: PHP\n".
			"X-Priority: 3\n".
			"X-MS-Has-Attach: yes\n";

	if($readReceipt) {
	$headers .=
		"X-Confirm-Reading-To: $fromString\n".
		"Disposition-Notification-To: $fromString\n";
	}

	$fileatt = $file['path'];			//"/public_html/pdfs/mypdf.pdf";
	$fileatttype = $file['type'];		//"application/pdf";
	$fileattname = $file['name'];		//"newname.pdf";

	// read file
	$file = fopen( $fileatt, 'rb' );
	$data = fread( $file, filesize( $fileatt ) );
	fclose( $file );

	// Add the MIME content
	$semi_rand = md5( time() );
	$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";

	$headers .= "MIME-Version: 1.0\n" .
		"Content-Type: multipart/mixed;\n" .
		"\tboundary=\"{$mime_boundary}\"";

	$message =
		"boundary=\"$mime_boundary\"\n\n".
		"This is a multi-part message in MIME format.\n\n".
		"--$mime_boundary\n" .
		"Content-Type: text/plain; charset=\"iso-8859-1\"\n".
		// "Content-Transfer-Encoding: 7bit\n\n" .
		"Content-Transfer-Encoding: quoted-printable\n\n".
		$message . "\n\n"; /* html message */
		"--$mime_boundary\n" .
		"Content-Type: text/plain; charset=\"iso-8859-1\"\n".
		"Content-Transfer-Encoding: quoted-printable\n\n".
		$message . "\n\n";
		"--$mime_boundary--";

	$data = chunk_split( base64_encode( $data ) );

	$message .= "--{$mime_boundary}\n".
			"Content-Description: {$fileattname}\n".
			"Content-Transfer-Encoding: base64\n".
			"Content-Type: {$fileatttype};\n".
			"\tname=\"{$fileattname}\"\n".
			"Content-Disposition: attachment;\n" .
			"\tfilename=\"{$fileattname}\"\n\n".
			$data."\n\n".
			"--{$mime_boundary}--\n";

	$parameter = '-f '.$from;
	return mail($recipient, $subject, $message, $headers, $parameter);
}
// -----------------------------------------------------------------------------
public function getInfo($item) {
	// detect owner data
	$idCol = (empty($item['pid']))?'cid':'pid';
	$id = $item[$idCol];
	$ret = array('idCol' => $idCol, 'id' => $id);
	$label = new label();

	switch($idCol) {
		case 'cid':
			$company = new company();
			$com = $company->get($id);
			$ret['greeting'] = L::_(146);
			$ret['name'] = $com['name'];
			$ret['nameFull'] = $com['name'];
			if(!empty($com['subtitle'])) {
				$ret['nameFull'] = $com['name'].' '.$com['subtitle'];
			}
			$ret['showLink'] = urlHelper::makeLink('crmIntern',
				'showCompany', crmInternView::html($ret['name']),
				array('cid' => $com['cid']));
			break;

		case 'pid':
			$person = new person();
			$per = $person->get($id);

			// create greeting, eg for mails
			$ret['greeting'] = L::_(146);
			if(!empty($per['salutationLid'])) {
				$ret['greeting'] .= ' '.
					$label->getLabelName($per['salutationLid']);
			}
			$ret['greeting'] .= ' '.$per['surname'];

			$ret['name'] = $per['surname'].', '.$per['forename'];
			$ret['nameFull'] = $per['forename'].' '.$per['surname'];
			$ret['showLink'] = urlHelper::makeLink('crmIntern',
				'showPerson', crmInternView::html($ret['name']),
				array('pid' => $per['pid']));
			break;
	}

	$ret['address'] = $this->getAddressPrimary($idCol, $id);
	$ret['contact'] = $this->getContactPrimary($idCol, $id);

	return $ret;
}

// -----------------------------------------------------------------------------
public function addAssignment($idCol, $id, $data) {
	$db = $this->getDb();
	$date = date("Y-m-d H:i:s");
	$valuesSql = stringHelper::arrayToSql($data);
	$db->queryPDO("INSERT INTO ".$this->table('assignment').
		" SET ".$valuesSql.", ".$idCol."='".$id."', uid='".user::getUid().
		"', modifyDate='".$date."', creationDate=modifyDate");
	return $db->getLastInsertedId();
}

public function setAssignment($amid, $data) {
	$db = $this->getDb();
	$date = date("Y-m-d H:i:s");
	$valuesSql = stringHelper::arrayToSql($data);
	$db->queryPDO("UPDATE ".$this->table('assignment').
		" SET ".$valuesSql.", modifyDate='".$date."' WHERE amid='".$amid."'");
	return $db->getAffectedRows();
}


public function delAssignment($amid) {
	$db = $this->getDb();
	$db->queryPDO("UPDATE ".$this->table('assignment').
		" SET deleted=1 WHERE amid='".$amid."' LIMIT 1");
	return $db->getAffectedRows();
}

public function getAssignmentsRs($idCol, $id) {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT * FROM ".$this->table('assignment').
		" WHERE ".$idCol."='".$id."' AND deleted=0 ORDER BY creationDate DESC");
	return $rs;
}

public function getAssignment($amid) {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT * FROM ".$this->table('assignment').
		" WHERE amid='".$amid."'");
	return $db->fetchPDO($rs);
}

public function getAssignmentsAllRs($idCol='cid', $status, $statusNot) {
	$db = $this->getDb();
	$where = ' WHERE deleted=0';
	if(!empty($status)) {
		$where = " AND status='".$status."'";
	}elseif(!empty($statusNot)) {
		$where = " AND status != '".$statusNot."'";
	}
	$rs = $db->queryPDO("SELECT * FROM ".$this->table('assignment').
		" INNER JOIN ".$this->table('company')." USING ".$idCol.
		$where." ORDER BY creationDate DESC");
	return $rs;
}

// -----------------------------------------------------------------------------
public function hasAssignments($idCol, $id) {
	$db = $this->getDb();
	$db->query("SELECT amid FROM ".$this->table('assignment').
		" WHERE ".$idCol."='".$id."' AND deleted=0".
		" AND assignmentStatusLid != 29 LIMIT 1");
	return $db->getAffectedRows();
}

// -----------------------------------------------------------------------------
public function getCountries() {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT code, de AS value FROM ".
		$this->table('countries')." ORDER BY de");
	return $rs;
}

// -----------------------------------------------------------------------------
public function getCountry($code, $lang='de') {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT ".$lang." FROM ".$this->table('countries').
		" WHERE code='".$code."'");
	$row = $db->fetchPDO($rs);
	return $row[$lang];
}

// -----------------------------------------------------------------------------
public function addBranch($name) {
	if(empty($name)) {
		return null;
	}
	$db = $this->getDb();
	// check if name exists
	$rs = $db->query("SELECT bid FROM ".$this->table('branches')
		." WHERE name='".$name."'");
	if($row = $rs->fetch()) {
		return $row['bid'];
	}
	else {
		$db->query("INSERT INTO ".$this->table('branches')
			." SET name='".$name."'");
		return $db->getLastInsertedId();
	}
}

// -----------------------------------------------------------------------------
public function getBranchName($bid) {
	$db = $this->getDb();
	$rs = $db->query("SELECT name FROM ".$this->table('branches')
		." WHERE bid='".$bid."'");
	$row = $db->fetchPDO($rs);
	return $row['name'];
}

// -----------------------------------------------------------------------------
public function getBranchesRs() {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT * FROM ".$this->table('branches')
		." ORDER BY name");
	return $rs;
}

// -----------------------------------------------------------------------------
public function addJobPosition($name) {
	if(empty($name)) {
		return null;
	}
	$db = $this->getDb();
	// check if name exists
	$rs = $db->queryPDO("SELECT jpid FROM ".$this->table('jobpositions')
		." WHERE name='".$name."'");
	if($row = $db->fetchPDO($rs)) {
		return $row['jpid'];
	}
	else {
		$db->queryPDO("INSERT INTO ".$this->table('jobpositions')
			." SET name='".$name."'");
		return $db->getLastInsertedId();
	}
}

// -----------------------------------------------------------------------------
public function getJobPositionName($jpid) {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT name FROM ".$this->table('jobpositions')
		." WHERE jpid='".$jpid."'");
	$row = $db->fetchPDO($rs);
	return $row['name'];
}

// -----------------------------------------------------------------------------
public function getJobPositionsRs() {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT * FROM ".$this->table('jobpositions')
		." ORDER BY name");
	return $rs;
}

// -----------------------------------------------------------------------------
public function addState($name) {
	if(empty($name)) {
		return null;
	}
	$db = $this->getDb();
	// check if name exists
	$rs = $db->queryPDO("SELECT sid FROM ".$this->table('states')
		." WHERE name='".$name."'");
	if($row = $db->fetchPDO($rs)) {
		return $row['sid'];
	}
	else {
		$db->queryPDO("INSERT INTO ".$this->table('states')
			." SET name='".$name."'");
		return $db->getLastInsertedId();
	}
}

// -----------------------------------------------------------------------------
public function getStateName($sid) {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT name FROM ".$this->table('states')
		." WHERE sid='".$sid."'");
	$row = $db->fetchPDO($rs);
	return $row['name'];
}

// -----------------------------------------------------------------------------
public function getStatesRs() {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT * FROM ".$this->table('states')
		." ORDER BY name");
	return $rs;
}

// -----------------------------------------------------------------------------
public function addGroup($name) {
	if(empty($name)) {
		return null;
	}
	$db = $this->getDb();
	// check if name exists
	$rs = $db->queryPDO("SELECT gid FROM ".$this->table('groups')
		." WHERE name='".$name."'");
	if($row = $db->fetchPDO($rs)) {
		return $row['gid'];
	}
	else {
		$db->queryPDO("INSERT INTO ".$this->table('groups')
			." SET name='".$name."'");
		return $db->getLastInsertedId();
	}
}

// -----------------------------------------------------------------------------
public function addGroupMapping($gid, $idCol, $id) {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT gid FROM ".$this->table('groupMap')
		." WHERE gid='".$gid."' AND ".$idCol."='".$id."'");

	if(!$db->getNumRows()) {
		// add mapping
		$db->queryPDO("INSERT INTO ".$this->table('groupMap')
			." SET gid='".$gid."', ".$idCol."='".$id."'");
		return $db->getLastInsertedId();
	}
	else {
		return $gid;
	}
}

// -----------------------------------------------------------------------------
public function getGroupsRs() {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT * FROM ".$this->table('groups')
		." ORDER BY name");
	return $rs;
}

// -----------------------------------------------------------------------------
public function getGroupName($idCol, $id) {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT name FROM ".$this->table('groups')." AS g"
		." INNER JOIN ".$this->table('groupMap')." AS map"
		." ON g.gid=map.gid WHERE map.".$idCol."='".$id."'");
	$row = $db->fetchPDO($rs);
	return $row['name'];
}
// -----------------------------------------------------------------------------
public function addIntActivity($idCol, $id, $continent, $activityLid) {
	$db = $this->getDb();
	$db->query("INSERT INTO ".$this->table('intActivity').
	" SET ".$idCol."='".$id."', continent='".$continent."',".
	" intActivityLid='".$activityLid."'");
	return $db->insertId();
}

public function getIntActivityRs($idCol, $id) {
	$db = $this->getDb();
	$rs = $db->query("SELECT * FROM ".$this->table('intActivity').
		" WHERE ".$idCol."='".$id."' ORDER BY continent");
	return $rs;
}

public function getIntActivitiesFormated($idCol, $id) {
	$ret = array();
	$label = new label();
	$rs = $this->getIntActivityRs($idCol, $id);
	while($row = $rs->fetch()) {
		$ret[$row['iaid']] = L::_($row['continent']).': '.
			$label->getLabelName($row['intActivityLid']);
	}
	return $ret;
}

public function deleteIntActivity($iaid) {
	$db = $this->getDb();
	$rs = $db->query("DELETE FROM ".$this->table('intActivity').
		" WHERE iaid='".$iaid."'");
	return $db->affectedRows();
}
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
} // end class

