<?php

// pref = preference

class pref extends crm {

// -----------------------------------------------------------------------------
public function __construct() {
}
// -----------------------------------------------------------------------------

public function get($pref, $uid=null) {
	if($uid === null) {
		$uid = user::getUid();
	}

	$db = project::getDb();

	$rs = $db->queryPDO("SELECT text FROM crm_user_prefs".
		" WHERE uid='".$uid."' AND pref='".$pref."' LIMIT 1");
	$row = $db->fetchPDO($rs);
	return $row['text'];
}
// -----------------------------------------------------------------------------


// -----------------------------------------------------------------------------
} // end class
