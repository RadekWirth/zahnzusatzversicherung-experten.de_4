<?php

class benefits extends wzm
{

protected $tariffBenefitsLimitation;
protected $tariffBenefits;
protected $series;


	public function __construct($series)
	{
		$this->series = $series;
		/* 
			areLimitationsDurable
			limitationsDurableAmount
			limitationsDurableAmountPeriod
		*/
	}


	private function convert($string, $from='UTF-8', $to='Windows-1252')
	{
		#return iconv($from, $to, $string);
		return mb_convert_encoding($string, $to, $from);
		#return utf8mb4_encode($string);
	}

	private function checkMonthWaitingTime($var)
	{
		if(!isset($var))
			$ret = 'kein Wert';
		elseif(is_numeric($var))
		{
			if($var == 0)
			{
				$ret = 'keine Wartezeit';
			} else
			{
				$ret = $var.' Monat';
				if($var > 1)
					$ret .= 'e';
			}
		}

	return $ret;
	}



	private function getYearMultiple($val, $endingSpace=false)
	{
		$Space = '';
		if($endingSpace)
			$Space = ' ';

		if($val == 1)
		{	return ' Jahr'.$Space;
		}
		else   return ' Jahren'.$Space;
	}

	private function getMonthMultiple($val, $endingSpace=false)
	{
		$Space = '';
		if($endingSpace)
			$Space = ' ';

		if($val == 1)
		{	return ' Monat'.$Space;
		}
		else   return ' Monaten'.$Space;
	}

	private function checkIdentical($ary)
	{
		// Prüfen der Werte im Array 
		// - Rückgabe 1 Wert, wenn alle Werte identisch sind
		// - Rückgabe array mit min / max der Werte, wenn Unterschiede festgestellt werden

		if(is_array($ary))
		{
			if(count(array_unique( $ary )) == 1)
			{
				return $ary[0];
			}
			elseif(count(array_unique( $ary )) > 1)
			{
				return array(min($ary), max($ary));
			}
			else return null;		
		}
		else
		{
			return null;
		}
	}

	private function getMoneyFormat($val, $currency_sign=false, $percent_sign=false, $limits=0)
	{
		$ret = number_format($val, $limits, ',', '.');

		if($currency_sign)
			$ret.= "€";
		if($percent_sign)
			$ret.= "%";

		return $ret;
	}

	private function getHTMLTableFromArray($array, $class="none")
	{
		$html = "<div class='".$class."'><table><tbody>"; 

		if (is_array($array) === true)
		{
			foreach($array as $id => $val)
			{
				$html .= "<tr><td>".$id."</td><td>".$val."</td></tr>";
			}			
		}
		$html .= "</tbody></table></div>";

		return $html;
	}

	public function getInsuranceYearsTable($lb, $data, $split=false)
	{
		$r = array('1. Kalenderjahr' => null, 'unbegrenzte Leistung' => null, 'Leistung gesamt' => null);
		if($lb == "0")
		{
					$months[] = isset($data['months12'])?$data['months12']:0;
					$months_ary;
					if (isset($data['months24']))
					{
						$months[] = $data['months24'];
					}
					if (isset($data['months36']))
					{
						$months[] = $data['months36'];
					}
					if (isset($data['months48']))
					{
						$months[] = $data['months48'];
					}
					if (isset($data['months60']))
					{
						$months[] = $data['months60'];
					}
					if (isset($data['months72']))
					{
						$months[] = $data['months72'];
					}
					if (isset($data['months84']))
					{
						$months[] = $data['months84'];
					}
					if (isset($data['months96']))
					{
						$months[] = $data['months96'];
					}





					foreach($months as $id => $val)
					{
						if(isset($val))
						{
							$tmp = array(($id+1).'. Versicherungsjahr' => $this->getMoneyFormat($months[$id], true));
							
							if($id == 0)
								$r['1. Kalenderjahr'] = $this->getMoneyFormat($months[$id], true);
						}
						if($id > 0)
						{
							$months_ary = array_merge($months_ary, $tmp);
						}
						else   $months_ary = $tmp;
					}

					$r['unbegrenzte Leistung'] = 'ab '.(count($months)+1).'. Jahr';
					$r['Leistung gesamt'] = $this->getMoneyFormat(array_sum($months), true);
					$ret[] = array('checkmark' => "", 'content' => $this->getHTMLTableFromArray($months_ary, ''));
					$ret[] = array('checkmark' => "", 'content' => 'Während der ersten <strong>'.count($months).' Jahre</strong> stehen insgesamt somit bis zu <strong>'.$this->getMoneyFormat(array_sum($months), true).'</strong> an Leistung zur Verfügung, sofern die maximal möglichen Leistungen jedes Jahr voll ausgeschöpft werden!');



		}
		if($lb == "1")
		{
			$months[12] = isset($data['months12'])?$data['months12']:0;
			$months[24] = isset($data['months24'])?$data['months24']:0;
			$months[36] = isset($data['months36'])?$data['months36']:0;
			$months[48] = isset($data['months48'])?$data['months48']:0;
			$months[60] = isset($data['months60'])?$data['months60']:0;
			$months[72] = isset($data['months72'])?$data['months72']:0;
			$months[84] = isset($data['months84'])?$data['months84']:0;
			$months[96] = isset($data['months96'])?$data['months96']:0;

			$months_ary = array();
			foreach( $months as $id => $val )
			{
				$year = $id / 12;
				if($val > 0)
				{
					if($year == 1)
					{
						$tmp = array($year.'. Versicherungsjahr' => $this->getMoneyFormat($months[$id], true));
						$r['1. Kalenderjahr'] = $this->getMoneyFormat($months[$id], true);
					}
					else
						$tmp = array('1. - '.$year.'. Versicherungsjahr' => $this->getMoneyFormat($months[$id], true));


					if($id > 12)
					{
						$months_ary = array_merge($months_ary, $tmp);
					}
					else   $months_ary = $tmp;


				}
				else
					unset($months[$id]);
	

			}
			$r['unbegrenzte Leistung'] = 'ab '.(count($months)+1).'. Jahr';
			$r['Leistung gesamt'] = $this->getMoneyFormat($months[(count($months)-1)*12], true);
			$ret[] = array('checkmark' => "", 'content' => $this->getHTMLTableFromArray($months_ary, ''));


		}

		if($split === true)
		{
			return $r;
		}

		return $ret;
	}

	public function getInsuranceYearsTableNum($idt)
	{

/* ???? */

		$lb = $this->getTariffBenefitsLimitationsInline($idt);
		$lim = $this->getLimitations($lb);
		$tbl = $this->getInsuranceYearsTable(1, $lim['globalSeries0'], 1);
		print_r($tbl);
		$tbl = $this->getInsuranceYearsTable(0, $lim['globalSeries0'], 1);
		print_r($tbl);

	}



	

	private function setInternalVars($idt)
	{
		if(!isset($this->tariffBenefitsLimitation))
		{
			$this->tariffBenefitsLimitation = $this->getTariffBenefitsLimitationsInline($idt);
		}

		if(!isset($this->tariffBenefits))
		{
			$this->tariffBenefits = $this->getTariffBenefits($idt);
		}
	}


	public function getTariffLimitations($idt)
	{
		if(!isset($this->tariffBenefitsLimitation))
			$this->setInternalVars($idt);

		if(!$this->series)
			$this->series = 0;

	// wenn limitationsAreGlobal = 1
	// dann gilt nur Leistungsbegrenzung global

	// wenn limitationsAreGlobal != 1
	// dann prüfe limitationsAppliesToDentures (Zahnersatz)
	// dann prüfe limitationsAppliesToDentalTreatment (Zahnbehandlung)
	// dann prüfe limitationsAppliesToProphylaxis (Prophylaxe)
	// dann prüfe limitationsAppliesToOrthodontics (KFO)

	// wenn nichts aktiv, dann gelten lokale Begrenzungen



		foreach($this->tariffBenefitsLimitation as $id => $value)
		{
			if($value['isSeries0'])
				$l[0]['global'] = $value;

			if($value['isSeries1'])
				$l[1]['global'] = $value;

			if($value['isSeries2'])
				$l[2]['global'] = $value;

			if($value['isSeries0onlyDentures'])
				$l[0]['dentures'] = $value;

			if($value['isSeries1onlyDentures'])
				$l[1]['dentures'] = $value;

			if($value['isSeries2onlyDentures'])
				$l[2]['dentures'] = $value;

			if($value['isOnlyDentalTreatment'])
				$l[0]['dentaltreatment'] = $l[1]['dentaltreatment'] =
				$l[2]['dentaltreatment'] = $value;

			if($value['isOnlyOrthodontics'])
				$l[0]['orthodontics'] = $l[1]['orthodontics'] =
				$l[2]['orthodontics'] = $value;
		}

		$lim = array('limglobal' => array(), 'dentures' => array(), 'dentaltreatment' => array(), 'prophylaxis' => array(), 'orthodontics' => array());

		if($this->tariffBenefits['limitationsAreGlobal'] == 1)
		{
			// dann gilt nur Leistungsbegrenzung global
			foreach($lim as $name => $value)
			{
				$lim[$name] = $l[$this->series]['global'];
				array_push($lim['limglobal'], $name); 
			}
		}

		else 
		{
			if($this->tariffBenefits['limitationsAppliesToDentures'] == 1)
			{
				// global für Zahnersatz
				$lim['dentures'] = $l[$this->series]['global'];
				array_push($lim['limglobal'], 'dentures'); 
			} else {
				// nicht global -> prüfe Zahnersatz (lokal)
				$lim['dentures'] = $l[$this->series]['dentures'];
			}
			if($this->tariffBenefits['limitationsAppliesToDentalTreatment'] == 1)
			{
				// global für Zahnbehandlung
				$lim['dentaltreatment'] = $l[$this->series]['global'];
				array_push($lim['limglobal'], 'dentaltreatment'); 
			} else {
				// nicht global -> prüfe Zahnbehandlung (lokal)
				$lim['dentaltreatment'] = $l[$this->series]['dentaltreatment'];
			}
			if($this->tariffBenefits['limitationsAppliesToProphylaxis'] == 1)
			{
				// global für Prophylaxe
				$lim['prophylaxis'] = $l[$this->series]['global'];
				array_push($lim['limglobal'], 'prophylaxis');
			} else {
				// Prophylaxe lokal gibt es nicht
			}
			if($this->tariffBenefits['limitationsAppliesToOrthodontics'] == 1)
			{
				// global für KFO
				$lim['orthodontics'] = $l[$this->series]['global'];
				array_push($lim['limglobal'], 'orthodontics');
			} else {
				// nicht global -> prüfe KFO lokal
				$lim['orthodontics'] = $l[$this->series]['orthodontics'];
			}
		}


		foreach ($lim as $id => $val)
		{
			if(!isset($val) || count($val) == 0)
			{
				unset($lim[$id]);
			}
			elseif($val['months12'] == null && $val['months24'] == null && $id != 'limglobal')
			{
				unset($lim[$id]);
			}
		}

		return $lim;
		
	}



	public function getTariffLimitationsTest($idt)
	{
		if(!isset($this->tariffBenefitsLimitation))
			$this->setInternalVars($idt);

		if(!$this->series)
			$this->series = 0;

	// wenn limitationsAreGlobal = 1
	// dann gilt nur Leistungsbegrenzung global

	// wenn limitationsAreGlobal != 1
	// dann prüfe limitationsAppliesToDentures (Zahnersatz)
	// dann prüfe limitationsAppliesToDentalTreatment (Zahnbehandlung)
	// dann prüfe limitationsAppliesToProphylaxis (Prophylaxe)
	// dann prüfe limitationsAppliesToOrthodontics (KFO)

	// wenn nichts aktiv, dann gelten lokale Begrenzungen



		foreach($this->tariffBenefitsLimitation as $id => $value)
		{
			if($value['isSeries0'])
				$l[0]['global'] = $value;

			if($value['isSeries1'])
				$l[1]['global'] = $value;

			if($value['isSeries2'])
				$l[2]['global'] = $value;

			if($value['isSeries0onlyDentures'])
				$l[0]['dentures'] = $value;

			if($value['isSeries1onlyDentures'])
				$l[1]['dentures'] = $value;

			if($value['isSeries2onlyDentures'])
				$l[2]['dentures'] = $value;

			if($value['isOnlyDentalTreatment'])
				$l[0]['dentaltreatment'] = $l[1]['dentaltreatment'] =
				$l[2]['dentaltreatment'] = $value;

			if($value['isOnlyOrthodontics'])
				$l[0]['orthodontics'] = $l[1]['orthodontics'] =
				$l[2]['orthodontics'] = $value;
		}

		$lim = array('limglobal' => array(), 'dentures' => array(), 'dentaltreatment' => array(), 'prophylaxis' => array(), 'orthodontics' => array());

		if($this->tariffBenefits['limitationsAreGlobal'] == 1)
		{
			// dann gilt nur Leistungsbegrenzung global
			#foreach($lim as $name => $value)
			#{
			#	$lim[$name] = $l[$this->series]['global'];
			#	#array_push($lim['limglobal'], $name); 
			#}
			$lim['limglobal'] = $l[$this->series]['global'];
		}

		else 
		{
			if($this->tariffBenefits['limitationsAppliesToDentures'] == 1)
			{
				// global für Zahnersatz
				$lim['dentures'] = $l[$this->series]['global'];
				array_push($lim['limglobal'], 'dentures'); 
				$lim['limglobal']['global'] = $l[$this->series]['global'];
			}

			// prüfe Zahnersatz (lokal) egal ob global oder nicht
			$lim['dentures'] = $l[$this->series]['dentures'];

	
			if($this->tariffBenefits['limitationsAppliesToDentalTreatment'] == 1)
			{
				// global für Zahnbehandlung
				$lim['dentaltreatment'] = $l[$this->series]['global'];
				array_push($lim['limglobal'], 'dentaltreatment'); 
				$lim['limglobal']['global'] = $l[$this->series]['global'];
			} 

			// prüfe Zahnbehandlung (lokal) egal ob global oder nicht
			$lim['dentaltreatment'] = $l[$this->series]['dentaltreatment'];
			
			
			if($this->tariffBenefits['limitationsAppliesToProphylaxis'] == 1)
			{
				// global für Prophylaxe
				$lim['prophylaxis'] = $l[$this->series]['global'];
				array_push($lim['limglobal'], 'prophylaxis');
				$lim['limglobal']['global'] = $l[$this->series]['global'];
			}

			// Prophylaxe gibt es lokal nicht.

			if($this->tariffBenefits['limitationsAppliesToOrthodontics'] == 1)
			{
				// global für KFO
				$lim['orthodontics'] = $l[$this->series]['global'];
				array_push($lim['limglobal'], 'orthodontics');
				$lim['limglobal']['global'] = $l[$this->series]['global'];
			}
			
			// prüfe KFO lokal egal ob global oder nicht
			$lim['orthodontics'] = $l[$this->series]['orthodontics'];
			
		}

		foreach ($lim as $id => $val)
		{
			if(!isset($val) || count($val) == 0)
			{
				unset($lim[$id]);
			}
			elseif($val['months12'] == null && $val['months24'] == null && $id != 'limglobal')
			{
				unset($lim[$id]);
			}
		}

		return $lim;
		
	}



	public function getLimitations()
	{
		if(isset($this->tariffBenefitsLimitation))
			{
				foreach ($this->tariffBenefitsLimitation as $id => $val)
				{

					if(isset($val['isSeries0onlyDentures']) && $val['isSeries0onlyDentures'] == 1)
					{
						$lim['0']['Zahnersatz'] = $val;
					}
					if(isset($val['isSeries1onlyDentures']) && $val['isSeries1onlyDentures'] == 1)
					{
						$lim['1']['Zahnersatz'] = $val;
					}
					if(isset($val['isSeries2onlyDentures']) && $val['isSeries2onlyDentures'] == 1)
					{
						$lim['2']['Zahnersatz'] = $val;
					}
					if(isset($val['isOnlyDentalTreatment']) && $val['isOnlyDentalTreatment'] == 1)
					{
						$lim['0']['Zahnbehandlung'] = $val;
						$lim['1']['Zahnbehandlung'] = $val;
						$lim['2']['Zahnbehandlung'] = $val;
					}
					if(isset($val['isOnlyOrthodontics']) && $val['isOnlyOrthodontics'] == 1)
					{
						$lim['0']['KFO'] = $val;
						$lim['1']['KFO'] = $val;
						$lim['2']['KFO'] = $val;
					}

					/* check global values */
					if(isset($val['isSeries0']) && $val['isSeries0'] == 1)
					{
						$lim['0']['globalSeries'] = $val;
					}
					if(isset($val['isSeries1']) && $val['isSeries1'] == 1)
					{
						$lim['1']['globalSeries'] = $val;
					}
					if(isset($val['isSeries2']) && $val['isSeries2'] == 1)
					{
						$lim['2']['globalSeries'] = $val;
					}

					/* override */
					if($this->tariffBenefits['limitationsAreGlobal'] == 1)
					{
						$lim['0']['Zahnersatz'] = $lim['0']['globalSeries'];
						$lim['1']['Zahnersatz'] = $lim['1']['globalSeries'];
						$lim['2']['Zahnersatz'] = $lim['2']['globalSeries'];

						$lim['0']['Zahnbehandlung'] = $lim['0']['globalSeries'];
						$lim['1']['Zahnbehandlung'] = $lim['1']['globalSeries'];
						$lim['2']['Zahnbehandlung'] = $lim['2']['globalSeries'];

						$lim['0']['KFO'] = $lim['0']['globalSeries'];
						$lim['1']['KFO'] = $lim['1']['globalSeries'];
						$lim['2']['KFO'] = $lim['2']['globalSeries'];

					}
					
					if($this->tariffBenefits['limitationsAppliesToDentures'] == 1)
					{
						$lim['0']['Zahnersatz'] = $lim['0']['globalSeries'];
						$lim['1']['Zahnersatz'] = $lim['1']['globalSeries'];
						$lim['2']['Zahnersatz'] = $lim['2']['globalSeries'];
					}

					if($this->tariffBenefits['limitationsAppliesToDentalTreatment'] == 1)
					{
						$lim['0']['Zahnbehandlung'] = $lim['0']['globalSeries'];
						$lim['1']['Zahnbehandlung'] = $lim['1']['globalSeries'];
						$lim['2']['Zahnbehandlung'] = $lim['2']['globalSeries'];
					}

					if($this->tariffBenefits['limitationsAppliesToOrthodontics'] == 1)
					{
						$lim['0']['KFO'] = $lim['0']['globalSeries'];
						$lim['1']['KFO'] = $lim['1']['globalSeries'];
						$lim['2']['KFO'] = $lim['2']['globalSeries'];
					}



				}
			}
		return $lim[$this->series];

	}


	public function getPerformanceScale($idt)
	{
		$lb = $this->getLongBenefits($idt);

		if($lb['Allgemeines'][7]['A'])
		{
			foreach($lb['Allgemeines'][7]['A'] as $id => $val)
			{
				if($val['content'])
					$content .= $val['content']."<p></p>";

				if($val['content-table'])
				{
					$content .= "<table>";
					foreach($val['content-table'] as $id2 => $val2)
					{
						$content .= "<tr><td>".$id2."</td><td>".$val2."</td></tr>";
					}
					$content .= "</table>";
				}
					
			}
		}

			return $content;
	}


	public function getShortBenefits($idt)
	{
		$t = $this->getLongBenefits($idt, true);

		return $t;
	}


	public function getLongBenefits($idt, $short=false)
	{
		$t1 = $this->tariffBenefits = $this->getTariffBenefits($idt);

		

		#$this->tariffBenefits = $t1;

		$in = '';

		if(isset($t1['insurance_id']))
			$in = $this->getTariffInsurance($t1['insurance_id']);
		if(isset($idt))
			$this->tariffBenefitsLimitation = $this->getTariffBenefitsLimitationsInline($idt);

		$tt = $this->getTooltips();

		$lim = $this->getTariffLimitationsTest($idt); 
		$limLabel = array('dentures' => 'Zahnersatz', 'dentaltreatment' => 'Zahnbehandlung', 'prophylaxis' => 'Prophylaxe', 'orthodontics' => 'KFO');


		$args = ['idt' => $idt];
		$m = new modules($args);
		$bonusBase = $m->getBonusBase();

		$t['Kurz'] = array('Übersicht' => null, 'Zahnersatz' => null, 'Prophylaxe' => null, 'Zahnbehandlung' => null, 'Kieferorthopädie' => null, 'Leistungsstaffel' => null, 'Beiträge' => null, 'Sonstiges' => null);


		// 90
		$ze_fest = $t1['denturesFixedAllowanceInPercent'];
		// 100 - 110
		$ze_implantate = $this->checkIdentical(array($t1['implantsWithoutBonusInPercent'], $t1['implantsWith5YearsBonusInPercent'], $t1['implantsWith10YearsBonusInPercent']));
		// 175 - 185
		$ze_inlays = $this->checkIdentical(array($t1['inlaysWithoutBonusInPercent'], $t1['inlaysWith5YearsBonusInPercent'], $t1['inlaysWith10YearsBonusInPercent']));
		// 235 - 245
		$ze_prothesen = $this->checkIdentical(array($t1['crownsBridgesWithoutBonusInPercent'], $t1['crownsBridgesWith5YearsBonusInPercent'], $t1['crownsBridgesWith10YearsBonusInPercent']));
		// 280 - 290
		$ze_regelversorgung = $this->checkIdentical(array($t1['regularCareWithoutBonusInPercent'], $t1['regularCareWith5YearsBonusInPercent'], $t1['regularCareWith10YearsBonusInPercent']));
		// 300 - 310
		$zb_fuellungen = $this->checkIdentical(array($t1['syntheticFillingsWithoutBonusInPercent'], $t1['syntheticFillingsWith5YearsBonusInPercent'], $t1['syntheticFillingsWith10YearsBonusInPercent']));
		// 355 - 365
		$zb_wurzel_ohne_gkv = $this->checkIdentical(array($t1['rootTreatmentWithoutBonusInPercentWithoutGKVPayment'], $t1['rootTreatmentWith5YearsBonusInPercentWithoutGKVPayment'], $t1['rootTreatmentWith10YearsBonusInPercentWithoutGKVPayment']));
		// 395 - 405
		$zb_wurzel_mit_gkv = $this->checkIdentical(array($t1['rootTreatmentWithoutBonusInPercentWithGKVPayment'], $t1['rootTreatmentWith5YearsBonusInPercentWithGKVPayment'], $t1['rootTreatmentWith10YearsBonusInPercentWithGKVPayment']));
		// 425 - 435
		$zb_periodent_ohne_gkv = $this->checkIdentical(array($t1['periodontitisTreatmentWithoutBonusInPercentWithoutGKVPayment'], $t1['periodontitisWith5YearsBonusInPercentWithoutGKVPayment'], $t1['periodontitisWith10YearsBonusInPercentWithoutGKVPayment']));
		// 470 - 480
		$zb_periodent_mit_gkv = $this->checkIdentical(array($t1['periodontitisWithoutBonusInPercentWithGKVPayment'], $t1['periodontitisWith5YearsBonusInPercentWithGKVPayment'], $t1['periodontitisWith10YearsBonusInPercentWithGKVPayment']));
		// 510 - 520
		$zb_zahnreinigung = $this->checkIdentical(array($t1['dentalCleaningMaxAmountPerYear'], $t1['dentalCleaningMaxAmountPerTwoYears'], $t1['dentalCleaningMaxAmountPerTreatment']));
		// 545 - 555
		$zb_fissurenversiegelung = $this->checkIdentical(array($t1['fissuresMaxAmountPerYear'], $t1['fissuresMaxAmountPerTwoYears'], $t1['fissuresMaxAmountPerTreatment']));


	// Bis zu welchem Gebührensatz wird das zahnärztliche Honorar erstattet?
		$t['Allgemeines'][0]['Q'] = 'Bis zu welchem Gebührensatz wird das zahnärztliche Honorar erstattet?';
		
		$t['Allgemeines'][0]['A'] = array();


		if(isset($t1['denturesFixedAllowanceInPercent']) && $t1['denturesFixedAllowanceInPercent'] >= 1)
		{
			$t['Allgemeines'][0]['A'][] = array('checkmark' => "yellow", 'content' => "Der Tarif <strong>".$t1['name']."</strong> sieht keine Leistung auf Basis der privatärztlichen GOZ-Abrechnung vor, sondern die Leistung ist abhängig vom Festzuschuss der gesetzlichen Krankenkasse.");
		}

		if(isset($t1['gozChargeRestrictedUntilLid']))
		{
			switch ($t1['gozChargeRestrictedUntilLid'])
				{
					case 2:
						$t['Allgemeines'][0]['A'][] = array('checkmark' => "green", 'content' => "Der Tarif <strong>".$t1['name']."</strong> leistet im allgemeinen bis zum <strong>Höchstsatz<strong> der GOZ (3,5facher Satz).");
						break;
					case 1:
						$t['Allgemeines'][0]['A'][] = array('checkmark' => "black", 'content' => "Der Tarif <strong>".$t1['name']."</strong> leistet im allgemeinen bis zum <strong>Regelhöchstsatz</strong> der GOZ (2,3facher Satz).");
						break;
					case 0:
						$t['Allgemeines'][0]['A'][] = array('checkmark' => "green", 'content' => "Der Tarif <strong>".$t1['name']."</strong> sieht <strong>keine Begrenzung auf die Höchstsätze</strong> der GOZ vor, d.h. im Einzelfall sind auch Erstattungen oberhalb des 3,5fachen Gebührensatzes möglich. Dies setzt allerdings eine <strong>gültige Honorarvereinbarung</strong> mit einer medizinisch nachvollziehbaren Begründung für die Überschreitung des Höchstsatzes voraus. Die Honorarvereinbarung ist dem Versicherer zwingend vor Beginn der Behandlung zur Prüfung und Genehmigung vorzulegen.");
						break;

				}
			
			$t['Allgemeines'][0]['A'][]['content-expert'] = "<div class='headline'>Experten-Erklärung</div><div class='content'><p>Der Zahnarzt berechnet das <strong>Honorar für eine privatzahnärztliche Behandlung</strong> nach der <strong>GOZ (Gebührenordnung für Zahnärzte)</strong>. Bei der Bemessung der Gebührenhöhe hat der Zahnarzt einen gewissen Spielraum. Er kann die Höhe der einzelnen Gebührenpositionen anhand von sogenannten \"Steigerungsfaktoren\" anpassen.</p><p>In der Gebührenordnung ist jede Position mit dem sog. <strong>Einfachsatz (1facher Gebührensatz)</strong> angegeben. Die GOZ sieht vor, dass Behandlungen vom Zahnarzt ohne weitere Begründung bis zum <strong>2,3fachen Satz (dem sog. \"Regelhöchstsatz\")</strong> berechnet werden können. Eine Überschreitung des 2,3fachen Satzes ist mit gesonderter Begründung auf der Rechnung bis zum <strong>3,5fachen Satz (Höchstsatz der GOZ)</strong> möglich.</p> <p>In seltenen Einzelfällen hat der Zahnarzt darüber hinaus die Möglichkeit, den <strong>Höchstsatz der GOZ zu überschreiten</strong> - dies muss der Zahnarzt jedoch nach den Vorgaben der GOZ zwingend <strong>schriftlich vor Beginn</strong> der Behandlung mit dem Patienten vereinbaren (in der Praxis rechnen die meisten Zahnärzte im Rahmen des Höchstsatzes bis 3,5fach ab - eine <strong>Überschreitung des Höchstsatzes</strong> ist in der Praxis sehr <strong>selten</strong>).</p></div>";
		}


	// Sieht der Tarif Wartezeiten vor?
		$t['Allgemeines'][1]['Q'] = 'Sieht der Tarif Wartezeiten vor?';		

		$t['Allgemeines'][1]['A'] = array();


		// Keine Prüfung - es kommt immer der gleiche Satz
			$t['Allgemeines'][1]['A'][] = array('checkmark' => "", 'content' => "Der Tarif <strong>".$t1['name']."</strong> sieht folgende Wartezeiten vor:");
		
			$ary = array();
			if($this->checkMonthWaitingTime($t1['waitingTimeImplantsInMonths']) != 'kein Wert')
			{
				$ary["Zahnersatz"] = $this->checkMonthWaitingTime($t1['waitingTimeImplantsInMonths']);
			}
			if($this->checkMonthWaitingTime($t1['waitingTimeSyntheticFillingsInMonths']) != 'kein Wert')
			{
				$ary["Zahnbehandlung"] = $this->checkMonthWaitingTime($t1['waitingTimeSyntheticFillingsInMonths']);
			}
			if($this->checkMonthWaitingTime($t1['waitingTimeDentalCleaningInMonths']) != 'kein Wert')
			{
				$ary["Prophylaxe"] = $this->checkMonthWaitingTime($t1['waitingTimeDentalCleaningInMonths']);
			}
			if($this->checkMonthWaitingTime($t1['waitingTimeOrthodonticsAdultsInMonths']) != 'kein Wert')
			{
				$ary["Kieferorthopädie"] = $this->checkMonthWaitingTime($t1['waitingTimeOrthodonticsAdultsInMonths']);
			} elseif($this->checkMonthWaitingTime($t1['waitingTimeOrthodonticsKig35InMonths']) != 'kein Wert')
			{
				$ary["Kieferorthopädie"] = $this->checkMonthWaitingTime($t1['waitingTimeOrthodonticsKig35InMonths']);
			}
			
			foreach($ary as $id => $val)
			{
				$checkmark = 'green';

				if($val != 'keine Wartezeit')
					$checkmark = 'yellow';


				$t['Allgemeines'][1]['A'][] = array('checkmark' => $checkmark, 'content' => $id.": ".$val);
			}

			
			
	// Verzichtet der Versicherer auf das ordentliche Kündigungsrecht?
		$t['Allgemeines'][2]['Q'] = 'Verzichtet der Versicherer auf das ordentliche Kündigungsrecht?';
		
		$t['Allgemeines'][2]['A'] = array();

		if(isset($t1['isOrdinaryRightOfTerminationWaived']))
		{
			if($t1['isOrdinaryRightOfTerminationWaived'] == 1)
			{
				$t['Allgemeines'][2]['A'][] = array('checkmark' => "green", 'content' => "<strong>Ja</strong>, die Versicherung verzichtet auf das Recht der ordentlichen Kündigung. Das bedeutet, der Versicherer kann den Vertrag mit Ihnen <strong>nicht</strong> einfach so ohne Angabe von Gründen kündigen.");
				$t['Kurz']['Sonstiges']['Verzicht ordentliches Kündigungsrecht'] = array('checkmark' => "green", 'content' => "Ja, verzichtet auf die Anwendung des ordentlichen Kündigungsrechts");
			}
	
			if($t1['isOrdinaryRightOfTerminationWaived'] == 0)
			{
				$t['Allgemeines'][2]['A'][] = array('checkmark' => "black", 'content' => "Nein, die Versicherung verzichtet nicht auf das Recht der ordentlichen Kündigung. Der Vertrag kann damit seitens der Versicherungsgesellschaft während der ersten 3 Jahre nach Versicherungsbeginn <strong>ohne Angabe von Gründen gekündigt</strong> werden.");
				$t['Kurz']['Sonstiges']['Verzicht ordentliches Kündigungsrecht'] = array('checkmark' => "black", 'content' => "Nein, die Versicherung verzichtet nicht auf das Recht der ordentlichen Kündigung");
			}

			if($t1['isOrdinaryRightOfTerminationWaived'] == 2)
			{
				$t['Allgemeines'][2]['A'][] = "individueller Text";
			}
		}



	// Sieht der Tarif altersbedingte Beitragserhöhungen vor (Kalkulation mit oder ohne Alterungsrückstellungen)?
		$t['Allgemeines'][3]['Q'] = 'Sieht der Tarif altersbedingte Beitragserhöhungen vor (Kalkulation mit oder ohne Alterungsrückstellungen)?';
		
		$t['Allgemeines'][3]['A'] = array();		


		if(isset($t1['hasAgeningReserves']))
		{
			if($t1['hasAgeningReserves'] == 0)
			{
				$t['Allgemeines'][3]['A'][] = array('checkmark' => "yellow", 'content' => "<p>Ja, der Tarif ".$this->convert($t1['name'])." ist <strong>ohne Alterungsrückstellungen</strong> kalkuliert. Die Beiträge <strong>erhöhen</strong> sich demnach automatisch beim Erreichen der im Tarif definierten Altersgrenzen.</p><p>Den <strong>voraussichtlichen Verlauf</strong> der Beiträge können Sie dem abgebildeten Diagramm entnehmen.</p><p>Darüber hinaus besteht bei allen Tarifen das <strong>allgemeine Risiko von Beitragsanpassungen</strong> aufgrund veränderter Rahmenbedingungen!</p>");
				$t['Kurz']['Sonstiges']['Beitragsverlauf'] = array('checkmark' => "yellow", 'content' => "steigende Beiträge");
			}

			if($t1['hasAgeningReserves'] == 1)
			{
				$t['Allgemeines'][3]['A'][] = array('checkmark' => "green", 'content' => "<p><strong>Nein</strong>, der Tarif ".$this->convert($t1['name'])." ist <strong>mit Alterungsrückstellungen</strong> kalkuliert. In den Beiträgen ist ein „Sparanteil“ enthalten, welcher das Ansteigen der Beiträge mit zunehmendem Alter (wo üblicherweise vermehrt Leistungsfälle auftreten) verhindern soll. Die Beiträge verlaufen hinsichtlich des Alters <strong>konstant</strong>.</p><p>Davon abgesehen besteht bei allen Tarifen das allgemeine Risiko von Beitragsanpassungen aufgrund veränderter Rahmenbedingungen!</p>");
				$t['Kurz']['Sonstiges']['Beitragsverlauf'] = array('checkmark' => "green", 'content' => "Beiträge konstant");
			}

		}


	// Sieht der Vertrag eine Mindestvertragsdauer vor? Wie ist die Kündigung geregelt?
		$t['Allgemeines'][4]['Q'] = 'Sieht der Vertrag eine Mindestvertragsdauer vor? Wie ist die Kündigung geregelt?';
		
		$t['Allgemeines'][4]['A'] = array();

		if(isset($t1['minContractDurationInYears']))
		{
			if($t1['minContractDurationInYears'] == 1 || $t1['minContractDurationInYears'] == 2)
			{
				if(isset($t1['timeUnitMinContractForDurationLid']) && $t1['timeUnitMinContractForDurationLid'] == 4)
				{
					$t['Allgemeines'][4]['A'][] = array('checkmark' => "black", 'content' => "<strong>Ja</strong>, es besteht eine anfängliche <strong>Mindestvertragslaufzeit</strong> von ".$t1['minContractDurationInYears'].$this->getYearMultiple($t1['minContractDurationInYears']).". Das erste Versicherungsjahr endet in dieser Hinsicht am 31.12. des Beginnjahres (sog. „Rumpfjahr“).");
					$t['Kurz']['Sonstiges']['Mindestvertragsdauer & Kündigung'] = array('checkmark' => "red", 'content' => $t1['minContractDurationInYears'].$this->getYearMultiple($t1['minContractDurationInYears'])." Mindestvertragslaufzeit ");
				} 
				if(isset($t1['timeUnitMinContractForDurationLid']) && $t1['timeUnitMinContractForDurationLid'] == 3)
				{
					$t['Allgemeines'][4]['A'][] = array('checkmark' => "black", 'content' => "<strong>Ja</strong>, es besteht eine anfängliche <strong>Mindestvertragslaufzeit</strong> von ".$t1['minContractDurationInYears'].$this->getYearMultiple($t1['minContractDurationInYears']).". Die Mindestvertragslaufzeit wird ausgehend vom Versicherungsbeginn exakt berechnet.");
					$t['Kurz']['Sonstiges']['Mindestvertragsdauer & Kündigung'] = array('checkmark' => "red", 'content' => $t1['minContractDurationInYears'].$this->getYearMultiple($t1['minContractDurationInYears'])." Mindestvertragslaufzeit ");
				} 
				
			}
			elseif($t1['minContractDurationInYears'] == 0)
			{
				$t['Allgemeines'][4]['A'][] = array('checkmark' => "green", 'content' => "<strong>Nein</strong>, es besteht keine anfängliche Mindestvertragsdauer.");
				$t['Kurz']['Sonstiges']['Mindestvertragsdauer & Kündigung'] = array('checkmark' => "green", 'content' => "Keine Mindestvertragslaufzeit");
			}

		}

		if(isset($t1['terminationPeriodInMonths']))
		{
			if($t1['terminationPeriodInMonths'] == 1 || $t1['terminationPeriodInMonths'] == 2 | $t1['terminationPeriodInMonths'] == 3)
			{
				if($t1['minContractDurationInYears'] == 1 || $t1['minContractDurationInYears'] == 2)
				{
					if(isset($t1['timeUnitMinContractAfterDurationLid']) && $t1['timeUnitMinContractAfterDurationLid'] == 4)
					{
						$t['Allgemeines'][4]['A'][]['content'] .= "<p>Die <strong>Kündigung</strong> ist nach Ablauf der Mindestvertragslaufzeit jeweils mit einer Fristeinhaltung von ".$t1['terminationPeriodInMonths'].$this->getMonthMultiple($t1['terminationPeriodInMonths'])." zum Ende jedes <strong>Kalenderjahres</strong> möglich.</p>";
						$t['Kurz']['Sonstiges']['Mindestvertragsdauer & Kündigung']['content'] .= "/ Kündigungsfrist von ".$t1['terminationPeriodInMonths'].$this->getMonthMultiple($t1['terminationPeriodInMonths'])." zum Ende jedes Kalenderjahres";
					}

					if(isset($t1['timeUnitMinContractAfterDurationLid']) && $t1['timeUnitMinContractAfterDurationLid'] == 3)
					{
						$t['Allgemeines'][4]['A'][]['content'] .= "<p>Die <strong>Kündigung</strong> ist nach Ablauf der Mindestvertragslaufzeit jeweils mit einer Fristeinhaltung von ".$t1['terminationPeriodInMonths'].$this->getMonthMultiple($t1['terminationPeriodInMonths'])." zum Ende jedes <strong>Versicherungsjahres</strong> möglich. Das Versicherungsjahr wird ausgehend vom Versicherungsbeginn <strong>exakt</strong> berechnet.</p>";
						$t['Kurz']['Sonstiges']['Mindestvertragsdauer & Kündigung']['content'] .= "/ Kündigungsfrist von ".$t1['terminationPeriodInMonths'].$this->getMonthMultiple($t1['terminationPeriodInMonths'])." zum Ende jedes Kalenderjahres";
					}
				}
				else
				{
					if(isset($t1['timeUnitMinContractAfterDurationLid']) && $t1['timeUnitMinContractAfterDurationLid'] == 4)
					{
						$t['Allgemeines'][4]['A'][]['content'] .= "<p>Die Kündigung ist jeweils mit einer Fristeinhaltung von ".$t1['terminationPeriodInMonths'].$this->getMonthMultiple($t1['terminationPeriodInMonths'])." zum Ende jedes Kalenderjahres möglich.</p>";
						$t['Kurz']['Sonstiges']['Mindestvertragsdauer & Kündigung']['content'] .= "/ Kündigungsfrist von ".$t1['terminationPeriodInMonths'].$this->getMonthMultiple($t1['terminationPeriodInMonths'])." zum Ende jedes Kalenderjahres";
					}

					if(isset($t1['timeUnitMinContractAfterDurationLid']) && $t1['timeUnitMinContractAfterDurationLid'] == 3)
					{
						$t['Allgemeines'][4]['A'][]['content'] .= "<p>Die Kündigung ist jeweils mit einer Fristeinhaltung von ".$t1['terminationPeriodInMonths'].$this->getMonthMultiple($t1['terminationPeriodInMonths'])." zum Ende jedes Versicherungsjahres möglich. Das Versicherungsjahr wird ausgehend vom Versicherungsbeginn exakt berechnet.</p>";
						$t['Kurz']['Sonstiges']['Mindestvertragsdauer & Kündigung']['content'] .= "/ Kündigungsfrist von ".$t1['terminationPeriodInMonths'].$this->getMonthMultiple($t1['terminationPeriodInMonths'])." zum Ende jedes Kalenderjahres";
					}
				}

			} elseif ($t1['terminationPeriodInMonths'] == 0)
			{
				if($t1['minContractDurationInYears'] == 1 || $t1['minContractDurationInYears'] == 2)
				{
					$t['Allgemeines'][4]['A'][]['content'] .= "<p>Die <strong>Kündigung</strong> ist nach Ablauf der Mindestvertragsdauer <strong>ohne Einhaltung einer Kündigungsfrist</strong> möglich.</p>";
				}
				if($t1['minContractDurationInYears'] == 0)
				{
					$t['Allgemeines'][4]['A'][]['content'] .= "<p>Die <strong>Kündigung</strong> ist jederzeit <strong>ohne Einhaltung einer Kündigungsfrist</strong> möglich</p>";
				}

			}

		}

		
	

	// Muss vor Beginn einer Behandlung ein HKP zur Prüfung eingereicht werden?
		$t['Allgemeines'][5]['Q'] = 'Muss vor Beginn einer Behandlung ein HKP zur Prüfung eingereicht werden?';
		
		$t['Allgemeines'][5]['A'];

		if(isset($t1['planAndCostRequired']))
		{
			if($t1['planAndCostRequired'] == 0)
			{
				$t['Allgemeines'][5]['A'][] = array('checkmark' => "green", 'content' => "<p><strong>Nein</strong>, die vorherige Einreichung eines <strong>Heil- und Kostenplanes (HKP)</strong> ist im Leistungsfall <strong>nicht zwingend vorgeschrieben</strong>. Es erfolgt keine Leistungskürzung nur aufgrund eines vorab nicht geprüften Heil- und Kostenplanes.</p>");

				$t['Allgemeines'][5]['A'][] = array('checkmark' => "", 'content-expert' => "<div class='headline'>Experten-TIPP:</div><div class='content'>Die Einreichung eines Heil- und Kostenplanes vor Beginn einer Behandlung dient allerdings <strong>Ihrer eigenen Sicherheit</strong>. Wir empfehlen daher die vorherige Einreichung eines Heil- und Kostenplanes ab einer zu erwartenden Gesamtrechnung in Höhe von ca. <strong>1.000€</strong> (auch wenn die Versicherungsbedingungen keine Verpflichtung dazu vorsehen).</div>");
			}
			if($t1['planAndCostRequired'] == 1)
			{
				$t['Allgemeines'][5]['A'][] = array('checkmark' => "black", 'content' => "<strong>Ja</strong>, im Leistungsfall muss zwingend vor Behandlungsbeginn ein <strong>Heil- und Kostenplan</strong> eingereicht und dessen Prüfung abgewartet werden, sofern der Rechnungsbetrag die Gesamtsumme von ".$t1['planAndCostRequiredAmount']." € überschreitet.");

				if(isset($t1['noPlanAndCostReductions']) && $t1['noPlanAndCostReductions'] > 0)
				{
					$t['Allgemeines'][5]['A'][]['content'] .= "Bei <strong>Nichtbeachtung</strong> dieser vertraglichen Verpflichtung (Obliegenheit) <strong>kürzt</strong> der Versicherer die Erstattungsleistungen für den Betrag, der die Grenze von ".$t1['planAndCostRequiredAmount']." € überschreitet um ".$t1['noPlanAndCostReductions']."%.";
				}
				
				if(isset($t1['noPlanAndCostReductions']) && $t1['noPlanAndCostReductions'] == 0)
				{
					$t['Allgemeines'][5]['A'][]['content'] .= "Die Versicherungsbedingungen sehen im Falle eines nicht rechtzeitig eingereichten Heil- und Kostenplanes allerdings <strong>keine Kürzung</strong> der Leistung vor.<p><strong>Hinweis:<</strong> die Einreichung eines Heil- und Kostenplanes vor Beginn einer Behandlung dient allerdings <strong>Ihrer eigenen Sicherheit</strong>. Wir empfehlen daher ebenfalls die vorherige Einreichung eines Heil- und Kostenplanes, auch wenn die Versicherungsbedingungen Leistungskürzung bei Nichtbeachtung vorsehen.</p>";
				}
			}

		}
	

	// Sieht der Tarif ein Preis- und Leistungsverzeichnis für Material- und Laborkosten vor?
		$t['Allgemeines'][6]['Q'] = 'Sieht der Tarif ein Preis- und Leistungsverzeichnis für Material- und Laborkosten vor?';
		
		$t['Allgemeines'][6]['A'];

		if(isset($t1['denturesFixedAllowanceInPercent']) && $t1['denturesFixedAllowanceInPercent'] >= 1)
		{
			$t['Allgemeines'][6]['A'][] = array('checkmark' => "yellow", 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> sieht keine prozentuale Leistung auf Basis der Rechnungssumme vor, sondern die Leistung ist abhängig vom Festzuschuss der gesetzlichen Krankenkasse.");
		}

		if(isset($t1['hasMaterialCostRepository']))
		{
			if($t1['hasMaterialCostRepository'] == 0)
			{
				$t['Allgemeines'][6]['A'][] = array('checkmark' => "green", 'content' => "<strong>Nein</strong>, die Versicherungsbedingungen sehen <strong>kein festes Preis- und Leistungsverzeichnis</strong> vor. Im Tarif ".$this->convert($t1['name'])." werden Material- und Laborkosten daher grundsätzlich in angemessenem Rahmen <strong>tariflich voll erstattet</strong>.");
				$t['Kurz']['Sonstiges']['Preis- und Leistungsverzeichnis Materialkosten'] = array('checkmark' => "green", 'content' => "kein Preisverzeichnis");
			}
			elseif($t1['hasMaterialCostRepository'] == 1)
			{
				$t['Allgemeines'][6]['A'][] = array('checkmark' => "yellow", 'content' => "<strong>Ja</strong>, für den Tarif ".$this->convert($t1['name'])." hat der Versicherer ein <strong>festes Preis- und Leistungsverzeichnis</strong> in den Versicherungsbedingungen hinterlegt. Material- und Laborkosten werden maximal nach den dort festgelegten Höchstpreisen erstattet.");
				$t['Kurz']['Sonstiges']['Preis- und Leistungsverzeichnis Materialkosten'] = array('checkmark' => "yellow", 'content' => "festes Preis- und Leistungsverzeichnis");
			}
		}



	// Sind die tariflichen Leistungen während der ersten Jahre in der Höhe tariflich begrenzt?
		$t['Allgemeines'][7]['Q'] = 'Sind die tariflichen Leistungen während der ersten Jahre in der Höhe tariflich begrenzt?';
		
		$t['Allgemeines'][7]['A'];

		//ID702 = JA
		if(isset($t1['areLimitationsDurable']) && $t1['areLimitationsDurable'] == 1)
		{
			$t['Allgemeines'][7]['A'][] = array('checkmark' => "yellow", 'content' => "Achtung: die Leistungen im Tarif ".$this->convert($t1['name'])." sind dauerhaft auf ".$this->getMoneyFormat($t1['limitationsDurableAmount'], 1, 0)." je ".$t1['limitationsDurableAmountPeriod']." Jahr begrenzt. Es können also auf Dauer – auch nach vielen Jahren Vertragszugehörigkeit - nur Leistungen in maximal dieser Höhe in Anspruch genommen werden!");
		}


		// ID715 = gleich „Alles“
		if(isset($t1['limitationsAreGlobal']) && $t1['limitationsAreGlobal'] == 1)
		{
			if(isset($t1['hasNoLimitationsSeries0']))
			{
				// 1
				if($t1['hasNoLimitationsSeries0'] == 1)
				{
					$t['Allgemeines'][7]['A'][] = array('checkmark' => "green", 'content' => "<strong>Nein</strong>, die tariflichen Leistungen stehen direkt nach Abschluss der Versicherung in <strong>voller tariflicher Höhe</strong> zur Verfügung  – es gibt <strong>keine besondere „Zahnstaffel“ oder Summenbegrenzung</strong> während der ersten Versicherungsjahre.");

					
				}
				elseif($t1['hasNoLimitationsSeries0'] == 0)
				{
					$t['Allgemeines'][7]['A'][] = array('checkmark' => "yellow", 'content' => "<strong>Ja</strong>, die Leistungen im Tarif ".$this->convert($t1['name'])." sind während der ersten Jahre nach Abschluss in der <strong>Höhe eingeschränkt</strong> (Summenstaffel).");
				}
			}


			// 2
			if(isset($t1['limitationsAmountTypeLid']))
			{
				if($t1['limitationsAmountTypeLid'] == 5)
				{
					$t['Allgemeines'][7]['A'][] = array('checkmark' => "yellow", 'content' => "Die <strong>maximalen Leistungen</strong> des Tarifes sind anfänglich begrenzt auf:");
				}

				if($t1['limitationsAmountTypeLid'] == 6)
				{
					$t['Allgemeines'][7]['A'][] = array('checkmark' => "yellow", 'content' => "Der <strong>maximal erstattungsfähige Rechnungsbetrag</strong> für die Leistungen des Tarifes ist anfänglich begrenzt auf:");
				}
					#$firstEntryInArray = reset($lim['limglobal']);
					$tbl = $this->getInsuranceYearsTable($t1['areLimitationsCumulated'], $lim['limglobal']);
						
					if(is_array($tbl))
					{
						foreach($tbl as $id => $val)
						{
							$t['Allgemeines'][7]['A'][] = $val;
						}
					}


			}
		}

		// ID715 = ungleich „Alles“
		else
		{
			$t['Allgemeines'][7]['A'][] = array('checkmark' => "yellow", 'content' => "<strong>Ja</strong>, die Leistungen im Tarif ".$this->convert($t1['name'])." sind während der ersten Jahre nach Abschluss in der <strong>Höhe eingeschränkt</strong> (Summenstaffel).");


			/*
			$lim['Zahnersatz']['show'] = 0; 

			$lim['Zahnbehandlung']['show'] = 0; 

			$lim['Prophylaxe']['show'] = 0; 

			$lim['KFO']['show'] = 0;


			if($t1['limitationsAppliesToDentures']) 
			{
				$lim['Zahnersatz']['show'] = 1;
			}
			if($t1['limitationsAppliesToDentalTreatment'])
			{
				$lim['Zahnbehandlung']['show'] = 1;
			}
			if($t1['limitationsAppliesToProphylaxis'])
			{
				$lim['Prophylaxe']['show'] = 1;
			}
			if($t1['limitationsAppliesToOrthodontics'])
			{
				$lim['KFO']['show'] = 1;
			}
			foreach($lim as $id => $val) { 
				if($val['show']==1) {
					if(strlen($lim_text) > 0) 
						$lim_text .= ", ";
					$lim_text .= $id;
				}
			}
			*/

			if(isset($lim['limglobal']) && count($lim['limglobal']) >= 1)
			{
				foreach($lim['limglobal'] as $id => $val)
				{
					if(strlen($lim_text) > 0 && substr($lim_text, -2) != ', ') 
						$lim_text .= ", ";
					if(is_numeric($id))
						$lim_text .= $limLabel[$val];
				}
			}

			if(isset($t1['hasNoLimitationsSeries0']))
			{
				// 1
				if($t1['hasNoLimitationsSeries0'] == 1)
				{
					$t['Allgemeines'][7]['A'][] = array('checkmark' => "green", 'content' => "<strong>".$lim_text."</strong>:<p>Für die Leistungsbereiche ".$lim_text." gilt <strong>keine anfängliche Begrenzung</strong>. Diese Leistungen stehen direkt <strong>nach Abschluss</strong> der Versicherung in <strong>voller tariflicher Höhe</strong> zur Verfügung!</p>");
				}
				elseif($t1['hasNoLimitationsSeries0'] == 0)
				{
					if(isset($t1['limitationsAmountTypeLid']))
					{
						if($t1['limitationsAmountTypeLid'] == 5)
						{
							$t['Allgemeines'][7]['A'][] = array('checkmark' => "yellow", 'content' => "<strong>".$lim_text."</strong>:<p>Für die Leistungsbereiche ".$lim_text." sind die <strong>maximalen Leistungen</strong> anfänglich begrenzt auf:</p>");
						}
						if($t1['limitationsAmountTypeLid'] == 6)
						{
							$t['Allgemeines'][7]['A'][] = array('checkmark' => "yellow", 'content' => "<strong>".$lim_text."</strong>:<p>Für die Leistungsbereiche ".$lim_text." ist der <strong>maximal erstattungsfähige Rechnungsbetrag</strong> anfänglich begrenzt auf:</p>");
						}
					}
						
						$tbl = $this->getInsuranceYearsTable($t1['areLimitationsCumulated'], $lim['limglobal']['global']);
						
						if(is_array($tbl))
						{
							foreach($tbl as $id => $val)
							{
								$t['Allgemeines'][7]['A'][] = $val;
							}
						}


				
				}
			}
		}


			if(isset($t1['hasNoLimitationsDenturesSeries0']))
			{
				if($t1['hasNoLimitationsDenturesSeries0'] == 1)
				{
					$t['Allgemeines'][7]['A'][] = array('checkmark' => "green", 'content' => "<strong>Zahnersatz:</strong><p>Für <strong>Zahnersatz-Leistungen</strong> (z.B. Kronen, Brücken oder Implantate) gilt <strong>keine anfängliche Begrenzung</strong>. Diese Leistungen stehen direkt <strong>nach Abschluss</strong> der Versicherung in <strong>voller tariflicher Höhe</strong> zur Verfügung!</p>");
				}
				elseif($t1['hasNoLimitationsDenturesSeries0'] == 0)
				{
					if(isset($t1['limitationsAmountTypeLid']))
					{
						if($t1['limitationsAmountTypeLid'] == 5)
						{
							$t['Allgemeines'][7]['A'][] = array('checkmark' => "yellow", 'content' => "<strong>Zahnersatz:</strong><p>Die <strong>maximalen Leistungen</strong> im Bereich <strong>Zahnersatz</strong> (z.B. Kronen, Brücken oder Implantate) sind anfänglich begrenzt auf:</p>");
						}
						if($t1['limitationsAmountTypeLid'] == 6)
						{
							$t['Allgemeines'][7]['A'][] = array('checkmark' => "yellow", 'content' => "<strong>Zahnersatz:</strong><p>Der <strong>maximal erstattungsfähige Rechnungsbetrag</strong> für <strong>Zahnersatz</strong> (z.B. Kronen, Brücken oder Implantate) ist anfänglich begrenzt auf:</p>");
						}

					}


					if($lim['dentures'])
					{
						$tbl = $this->getInsuranceYearsTable($t1['areLimitationsCumulated'], $lim['dentures']);

						if(is_array($tbl))
						{
							foreach($tbl as $id => $val)
							{
								$t['Allgemeines'][7]['A'][] = $val;
							}
						}
					}
				
				}
			}
			if(isset($t1['hasNoLimitationsDentalTreatment']))
			{
				if($t1['hasNoLimitationsDentalTreatment'] == 1)
				{
					$t['Allgemeines'][7]['A'][] = array('checkmark' => "green", 'content' => "<strong>Zahnbehandlung:</strong><p>Für <strong>Zahnbehandlungs-Leistungen</strong> (z.B. Prophylaxe, Füllungen oder Wurzelbehandlung) gilt <strong>keine anfängliche Begrenzung</strong>. Diese Leistungen stehen direkt <strong>nach Abschluss</strong> der Versicherung in <strong>voller tariflicher Höhe</strong> zur Verfügung!</p>");
				}
				elseif($t1['hasNoLimitationsDentalTreatment'] == 0)
				{
					if(isset($t1['limitationsAmountTypeLid']))
					{
						if($t1['limitationsAmountTypeLid'] == 5)
						{
							$t['Allgemeines'][7]['A'][] = array('checkmark' => "yellow", 'content' => "<strong>Zahnbehandlung:</strong><p>Die <strong>maximalen Leistungen</strong> im Bereich <strong>Zahnbehandlung</strong> (z.B. Prophylaxe, Füllungen oder Wurzelbehandlung) sind begrenzt auf:</p>");
						}
						if($t1['limitationsAmountTypeLid'] == 6)
						{
							$t['Allgemeines'][7]['A'][] = array('checkmark' => "yellow", 'content' => "<strong>Zahnbehandlung:</strong><p>Der <strong>maximal erstattungsfähige Rechnungsbetrag</strong> für <strong>Zahnbehandlung</strong> (z.B. Prophylaxe, Füllungen oder Wurzelbehandlung) ist begrenzt auf.</p>");
						}

					}

					if($lim['dentaltreatment'])
					{
						$tbl = $this->getInsuranceYearsTable($t1['areLimitationsCumulated'], $lim['dentaltreatment']);

						if(is_array($tbl))
						{
							foreach($tbl as $id => $val)
							{
								$t['Allgemeines'][7]['A'][] = $val;
							}
						}

					}
				
				}
			}
			if(isset($t1['hasNoLimitationsOrthodontics']))
			{
				if($t1['hasNoLimitationsOrthodontics'] == 1)
				{
					$t['Allgemeines'][7]['A'][] = array('checkmark' => "green", 'content' => "<strong>Kieferorthopädie:</strong><p>Für <strong>Kieferorthopädie-Leistungen</strong> (z.B. Zahnspangen) gilt <strong>keine anfängliche Begrenzung</strong>. Diese Leistungen stehen direkt <strong>nach Abschluss</strong> der Versicherung in <strong>voller tariflicher Höhe</strong> zur Verfügung!</p>");
				}
				elseif($t1['hasNoLimitationsOrthodontics'] == 0)
				{
					if(isset($t1['limitationsAmountTypeLid']))
					{
						if($t1['limitationsAmountTypeLid'] == 5)
						{
							$t['Allgemeines'][7]['A'][] = array('checkmark' => "yellow", 'content' => "<strong>Kieferorthopädie:</strong><p>Die <strong>maximalen Leistungen</strong> für <strong>Kieferorthopädie</strong> (Zahnspangen) sind anfänglich begrenzt auf:</p>");
						}
						if($t1['limitationsAmountTypeLid'] == 6)
						{
							$t['Allgemeines'][7]['A'][] = array('checkmark' => "yellow", 'content' => "<strong>Kieferorthopädie:</strong><p>Der <strong>maximal erstattungsfähige Rechnungsbetrag</strong> für <strong>Kieferorthopädie</strong> ist anfänglich begrenzt auf:</p>");
						}

					}

					if($lim['orthodontics'])
					{
						$tbl = $this->getInsuranceYearsTable($t1['areLimitationsCumulated'], $lim['orthodontics']);

						if(is_array($tbl))
						{
							foreach($tbl as $id => $val)
							{
								$t['Allgemeines'][7]['A'][] = $val;
							}
						}

					}
				
				}
			}
			

			// 4
			if(isset($t1['timeUnitLimitationsLid']))
			{
				if($t1['timeUnitLimitationsLid'] == 3)
				{
					$t['Allgemeines'][7]['A'][] = array('content' => 'Das <strong>Versicherungsjahr</strong> berechnet sich mit jeweils 12 Monaten <strong>exakt</strong> ab Versicherungsjahr.<p><strong>Beispiel:</strong></p><p>Wenn Ihr Vertrag am <strong>1.6. eines Jahres</strong> begonnen hat, läuft das Versicherungsjahr hinsichtlich der Leistungsbegrenzungen immer vom <strong>1.6. bis zum 31.5. des Folgejahres</strong>.</p>');
				}
				if($t1['timeUnitLimitationsLid'] == 4)
				{
					$t['Allgemeines'][7]['A'][] = array('content' => 'Als <strong>Versicherungsjahr</strong> für die Begrenzungen gilt das <strong>Kalenderjahr</strong>. Das bedeutet, das 1. Versicherungsjahr beginnt mit dem in der Police angegebenen Versicherungsbeginn und endet dann am <strong>31.12.</strong> des laufenden Jahres (das 1. Versicherungsjahr ist somit kürzer als ein volles Jahr, sofern der Versicherungsbeginn unterjährig war).');
				}
			}

			// 5
			if(isset($t1['limitationsAmountTypeLid']))
			{
				if($t1['limitationsAmountTypeLid'] == 6)
				{
					$t['Allgemeines'][7]['A'][] = array('checkmark' => "yellow", 'content' => 'Die oben genannten Beträge stellen nicht die maximal mögliche Leistung des Tarifes dar, sondern den maximal erstattungsfähigen Rechnungsbetrag – aus diesen Beträgen – multipliziert mit dem Prozentsatz der jeweiligen Leistungsart – ergibt sich die maximal mögliche anfängliche Leistung.');
				}
			}

			// 6
			if(isset($t1['noLimitationsByAccident']))
			{
				if($t1['noLimitationsByAccident'] == 1)
				{

					$t['Allgemeines'][7]['A'][] = array('content' => 'Sofern Behandlungen auf einen nachweislich nach Abschluss der Versicherung eingetretenen <strong>Unfall</strong> zurückzuführen sind, <strong>entfällt die Begrenzung</strong>!');
	
				}
			}

		$t['Kurz']['Leistungsstaffel'] = $t['Allgemeines'][7]['A'];
		unset ($tbl);

		
	// Gibt es Rabatt, wenn man den Beitrag jährlich, halbjährlich oder vierteljährlich bezahlt?
		$t['Allgemeines'][8]['Q'] = 'Gibt es Rabatt, wenn man den Beitrag jährlich, halbjährlich oder vierteljährlich bezahlt?';
		
		$t['Allgemeines'][8]['A'];

		if(isset($t1['discountIfPaymentInAdvance']))
		{
			if($t1['discountIfPaymentInAdvance'] == 1)
			{
				$t['Allgemeines'][8]['A'][] = array('checkmark' => "green", 'content' => "<strong>Ja</strong>, wenn Sie den Beitrag im voraus bezahlen, erhalten Sie <strong>folgenden Rabatt</strong>:");
				$t['Allgemeines'][8]['A'][]['content'] = $this->getHTMLTableFromArray(array('jährlich' => $this->getMoneyFormat($t1['discountAnnualPayment'], 0, 1), 'halbjährlich' => $this->getMoneyFormat($t1['discountSemiAnnualPayment'], 0, 1), 'vierteljährlich' => $this->getMoneyFormat($t1['discountQuarterlyPayment'], 0, 1)), 'discountIfPaymentInAdvance');
			}

			if($t1['discountIfPaymentInAdvance'] == 0)
			{
				$t['Allgemeines'][8]['A'][] = array('checkmark' => "black", 'content' => "<strong>Nein</strong>, es gibt <strong>keinen Rabatt</strong>, wenn Sie die Beiträge jährlich, halbjährlich oder vierteljährlich bezahlen.");
			}
		}

	// Wer kann die Versicherung abschließen (Annahmerichtlinien)?
		$t['Annahmerichtlinien'][0]['Q'] = 'Wer kann die Versicherung abschließen (Annahmerichtlinien)?';
		
		$t['Annahmerichtlinien'][0]['A'];


		$t['Annahmerichtlinien'][0]['A'][] = array('checkmark' => "green", 'content' => "Im Tarif ".$this->convert($t1['name'])." können sich Personen versichern, die bei einer <strong>deutschen gesetzlichen Krankenkasse</strong> versichert sind (dabei ist unerheblich, ob die Person als Hauptmitglied selbst pflichtversichert, freiwillig versichert oder familienversichert ist).");

		if(isset($t1['medicalCare']) && $t1['medicalCare'] == 1)
		{
			$t['Annahmerichtlinien'][0]['A'][] = array('checkmark' => "green", 'content' => "Darüber hinaus ist dieser Tarif auch für Personen abschließbar, die (z.B. als Polizisten) über einen Träger der <strong>„freien Heilfürsorge“</strong> abgesichert sind.");
		}

		if(isset($t1['pkvBasisTariff']) && $t1['pkvBasisTariff'] == 1)
		{
			$t['Annahmerichtlinien'][0]['A'][] = array('checkmark' => "green", 'content' => "In diesem Tarif können sich auch Personen versichern, die im sog <strong>„Basistarif“</strong> der privaten Krankenversicherung versichert sind.");
		}

		if(isset($t1['maxAge']) && $t1['maxAge'] > 0)
		{
			$t['Annahmerichtlinien'][0]['A'][] = array('checkmark' => "yellow", 'content' => "Der Abschluss des Tarifes Tarifname ist gemäß Annahmerichtlinien der Versicherung nur bis zu einem Alter von <strong>".($t1['maxAge']-1)." Jahren</strong> möglich (Höchstaufnahmealter).");
		}

		if(isset($t1['dentalSituationAboveAge']) && $t1['dentalSituationAboveAge'] > 0)
		{
			$t['Annahmerichtlinien'][0]['A'][] = array('checkmark' => "yellow", 'content' => "Ab einem <strong>Alter von ".$t1['dentalSituationAboveAge']."</strong> muss zusätzlich zum Antrag ein ausführlicher <strong>Befundbericht</strong> vom Zahnarzt beim Versicherer eingereicht werden.");
		}


	// Führt der Versicherer bei Abschluss eine Gesundheitsprüfung durch?
		$t['Annahmerichtlinien'][1]['Q'] = 'Führt der Versicherer bei Abschluss eine Gesundheitsprüfung durch?';
		
		$t['Annahmerichtlinien'][1]['A'];

		if(isset($t1['hasHealthCheck']))
		{
			if($t1['hasHealthCheck'] == 0)
			{
				$t['Annahmerichtlinien'][1]['A'][] = array('checkmark' => 'green', 'content' => "Nein, der Tarif ".$this->convert($t1['name'])." wird ohne Gesundheitsprüfung angeboten. Bei Abschluss sind also keine Angaben über den Zahnzustand notwendig.");

				$t['Kurz']['Sonstiges']['Annahmerichtlinien'] = array('checkmark' => 'green', 'content' => "Ohne Gesundheitsfragen");

				if(isset($t1['ongoingTreatmentInsured']) && $t1['ongoingTreatmentInsured'] == 0)
				{
					$t['Annahmerichtlinien'][1]['A'][] = array('checkmark' => 'yellow', 'content' => "<strong>Ausgeschlossen</strong> vom Versicherungsschutz sind jedoch <strong>alle vor Abschluss</strong> der Versicherung eingetretenen <strong>„Versicherungsfälle“</strong>, also Behandlungen, die bereits vor Abschluss vom Zahnarzt angeraten oder geplant worden sind.");
				}

				if(isset($t1['missingTeethInsuranceableLimit']) && $t1['missingTeethInsuranceableLimit'] == 0)
				{
					$t['Annahmerichtlinien'][1]['A'][] = array('checkmark' => 'yellow', 'content' => "Ausgeschlossen sind darüber hinaus <strong>fehlende Zähne</strong>, die bereits <strong>vor Abschluss</strong> der Versicherung <strong>gefehlt hatten</strong> und <strong>nicht ersetzt waren</strong> (also bei Abschluss echte vorhandene Zahnlücken / Freiendlücken).");
				}

			}
			elseif($t1['hasHealthCheck'] == 1)
			{
				$t['Annahmerichtlinien'][1]['A'][] = array('checkmark' => 'yellow', 'content' => "<strong>Ja, die Versicherung führt bei Abschluss eine Gesundheitsprüfung durch</strong>. <!-- Im Antrag werden die folgenden Gesundheitsfragen gestellt:<p>Gesundheitsfragen der Versicherung 1:1 gemäß Antragsformular – Datenbank?<p>Annahmebedingungen – wann eine Annahme erfolgen kann – wann ein Antrag abgelehnt wird → muss für jeden Tarif individuell geschrieben werden! --!>");

				$t['Kurz']['Sonstiges']['Annahmerichtlinien'] = array('checkmark' => 'yellow', 'content' => "Mit Gesundheitsfragen");
			}
		}


	// Können fehlende Zähne (unversorgte Zahnlücken) mit versichert werden?
		$t['Annahmerichtlinien'][2]['Q'] = 'Können fehlende Zähne (unversorgte Zahnlücken) mit versichert werden?';
		
		$t['Annahmerichtlinien'][2]['A'];

		if(isset($t1['missingTeethInsuranceableLimit']))
		{
			if($t1['missingTeethInsuranceableLimit'] > 0)
			{
				$t['Annahmerichtlinien'][2]['A'][] = array('checkmark' => 'green', 'content' => "<strong>Ja</strong>, es können insgesamt bis zu <strong>".$this->getMoneyFormat($t1['missingTeethInsuranceableLimit'])." fehlende Zähne</strong> für die zukünftige Versorgung mit Zahnersatz mit versichert werden.");
				$t['Kurz']['Sonstiges']['Zahnlücken versicherbar?'] = array('checkmark' => 'green', 'content' => "bis zu ".$this->getMoneyFormat($t1['missingTeethInsuranceableLimit'])." fehlende Zähne können mit versichert werden");

				if(isset($t1['riskPremiumMissingTeethPercentage']) && $t1['riskPremiumMissingTeethPercentage'] > 0)
				{
					$t['Annahmerichtlinien'][2]['A'][] = array('checkmark' => 'green', 'content' => "Je fehlendem Zahn wird ein Risikozuschlag in Höhe von <strong>".$this->getMoneyFormat($t1['riskPremiumMissingTeethPercentage'], 0, 1)."</strong> berechnet.");
				}

				if(isset($t1['riskPremiumMissingTeethAmount']) && $t1['riskPremiumMissingTeethAmount'] > 0)
				{
					$t['Annahmerichtlinien'][2]['A'][] = array('checkmark' => 'yellow', 'content' => "Je fehlendem Zahn wird ein Risikozuschlag in Höhe von <strong>".$this->getMoneyFormat($t1['riskPremiumMissingTeethAmount'], 1, 0, 2)."</strong> berechnet.");
				}

				
			}
			elseif($t1['missingTeethInsuranceableLimit'] == 0)
			{
				$t['Annahmerichtlinien'][2]['A'][] = array('checkmark' => 'yellow', 'content' => "<strong>Nein</strong> – alle Zähne, die bereits vor Abschluss der Versicherung fehlten und nicht ersetzt waren, sind <strong>vom Versicherungsschutz ausgeschlossen</strong>.");
				$t['Kurz']['Sonstiges']['Zahnlücken versicherbar?'] = array('checkmark' => 'yellow', 'content' => "Nein, leider nicht");
			}

			if(isset($t1['missingTeethText']) && $t1['missingTeethText'] > 0)
			{
				$t['Annahmerichtlinien'][2]['A'][] = array('checkmark' => 'yellow', 'content' => "individueller Text");
			}

			if(isset($t1['exclusionOfBenefitsMissingTeethPossible']) && $t1['exclusionOfBenefitsMissingTeethPossible'] == 1)
			{
				$t['Annahmerichtlinien'][2]['A'][] = array('checkmark' => 'green', 'content' => "Alternativ können fehlende Zähne auf Wunsch des Versicherungsnehmers <strong>vom Versicherungsschutz ausgeschlossen</strong> werden – in diesem Fall wird auf die Berechnung von Risikozuschlägen verzichtet.");
			}

		}

	// Können vorhandene herausnehmbare Prothesen mit versichert werden?
		$t['Annahmerichtlinien'][3]['Q'] = 'Können vorhandene herausnehmbare Prothesen mit versichert werden?';
		
		$t['Annahmerichtlinien'][3]['A'];

		if(isset($t1['removableDenturesLimit']))
		{
			if($t1['removableDenturesLimit'] == 28)
			{
				$t['Annahmerichtlinien'][3]['A'][] = array('checkmark' => 'green', 'content' => "<strong>Ja</strong>, eine bei Abschluss vorhandene herausnehmbare Prothese ist <strong>uneingeschränkt</strong> im tariflichen Umfang <strong>mit versichert</strong>, sofern diese voll intakt ist, es sich dabei um eine dauerhafte Zahnersatzversorgung handelt (kein Provisorium) und vom Zahnarzt keine Neuversorgung, Reparaturen o.Ä. angeraten waren.");
				$t['Kurz']['Sonstiges']['Vorhandene Prothesen versichert?'] = array('checkmark' => 'green', 'content' => "mit versicherbar");
			}
			elseif($t1['removableDenturesLimit'] > 0 && $t1['removableDenturesLimit'] < 28)
			{
				$t['Annahmerichtlinien'][3]['A'][] = array('checkmark' => 'green', 'content' => "<strong>Ja</strong>, eine bei Abschluss vorhandene <strong>herausnehmbare Prothese</strong> ist im tariflichen Umfang <strong>mit versichert</strong>, sofern mit dieser Prothese nicht mehr als <strong>".$this->getMoneyFormat($t1['removableDenturesLimit'])." Zähne</strong> ersetzt worden sind, die Prothese voll intakt ist, es sich dabei um eine dauerhafte Zahnersatzversorgung handelt (kein Provisorium) und vom Zahnarzt keine Neuversorgung, Reparaturen o.Ä. angeraten waren.");
				$t['Kurz']['Sonstiges']['Vorhandene Prothesen versichert?'] = array('checkmark' => 'green', 'content' => "mit versicherbar, sofern nicht mehr als ".$this->getMoneyFormat($t1['removableDenturesLimit'])." Zähne ersetzt worden sind");

			}
			elseif($t1['removableDenturesLimit'] == 0)
			{
				$t['Annahmerichtlinien'][3]['A'][] = array('checkmark' => 'yellow', 'content' => "<strong>Nein</strong> – Personen, die bei Antragstellung mit einer <strong>herausnehmbaren Voll- oder Teilprothese</strong> versorgt waren, sind in diesem Tarif <strong>nicht versicherbar</strong>.");
				$t['Kurz']['Sonstiges']['Vorhandene Prothesen versichert?'] = array('checkmark' => 'red', 'content' => "nicht versicherbar");
			}

			if($t1['removableDenturesLimit'] > 0)
			{
				if(isset($t1['riskPremiumRemovableDenturesPercentage']) && $t1['riskPremiumRemovableDenturesPercentage'] > 0)
				{
					$t['Annahmerichtlinien'][3]['A'][] = array('checkmark' => 'yellow', 'content' => "Je fehlendem Zahn, der mittels einer herausnehmbaren Prothese ersetzt ist, wird ein <strong>Risikozuschlag</strong> in Höhe von <strong>".$this->getMoneyFormat($t1['riskPremiumRemovableDenturesPercentage'], 0, 1)."</strong> berechnet.");
				}
				if(isset($t1['riskPremiumRemovableDenturesAmount']) && $t1['riskPremiumRemovableDenturesAmount'] > 0)
				{
					$t['Annahmerichtlinien'][3]['A'][] = array('checkmark' => 'yellow', 'content' => "Je fehlendem Zahn, der mittels einer herausnehmbaren Prothese ersetzt ist, wird ein <strong>Risikozuschlag</strong> in Höhe von <strong>".$this->getMoneyFormat($t1['riskPremiumRemovableDenturesAmount'], 1)."</strong> berechnet.");
				}
			}

		}

		if(isset($t1['removableDenturesText']))
		{
			$t['Annahmerichtlinien'][3]['A'][] = array('checkmark' => '', 'content' => "individueller Text");
		}

		if(isset($t1['optionalExclusionRemovableTeeth']) && $t1['optionalExclusionRemovableTeeth'] == 1)
		{
			$t['Annahmerichtlinien'][3]['A'][] = array('checkmark' => 'green', 'content' => "Alternativ können fehlende Zähne, die mittels herausnehmbarer Prothese ersetzt sind auf Wunsch des Versicherungsnehmers <strong>vom Versicherungsschutz ausgeschlossen werden</strong> – in diesem Fall wird auf die Berechnung von Risikozuschlägen verzichtet.");
		}
		


	// Kann vorhandener fester Zahnersatz (z.B. Kronen, Brücken) mit versichert werden?
		$t['Annahmerichtlinien'][4]['Q'] = 'Kann vorhandener fester Zahnersatz (z.B. Kronen, Brücken) mit versichert werden?';
		
		$t['Annahmerichtlinien'][4]['A'];

		if(isset($t1['fixedDenturesLimit']))
		{
			if($t1['fixedDenturesLimit'] == 28)
			{
				$t['Annahmerichtlinien'][4]['A'][] = array('checkmark' => 'green', 'content' => "<strong>Ja</strong>, bei Abschluss vorhandener <strong>festsitzender Zahnersatz</strong> ist <strong>uneingeschränkt</strong> im tariflichen Umfang <strong>mit versichert</strong>, sofern dieser voll intakt war und vom Zahnarzt keine Neuversorgung, Reparaturen o.Ä. angeraten waren.");
				$t['Kurz']['Sonstiges']['Vorhandene Kronen, Brücken & Füllungen versichert?'] = array('checkmark' => 'green', 'content' => "mit versicherbar");
			}
			elseif($t1['fixedDenturesLimit'] > 0 && $t1['fixedDenturesLimit'] < 28)
			{
				$t['Annahmerichtlinien'][4]['A'][] = array('checkmark' => 'green', 'content' => "<strong>Ja</strong>, bei Abschluss vorhandener <strong>festsitzender Zahnersatz</strong> ist im tariflichen Umfang mit versichert, sofern insgesamt nicht mehr als <strong>".$this->getMoneyFormat($t1['fixedDenturesLimit'])." Zähne</strong> ersetzt worden sind, der Zahnersatz voll intakt ist und vom Zahnarzt keine Neuversorgung, Reparaturen o.Ä. angeraten waren.");
				$t['Kurz']['Sonstiges']['Vorhandene Kronen, Brücken & Füllungen versichert?'] = array('checkmark' => 'green', 'content' => "mit versicherbar");

			}
			elseif($t1['fixedDenturesLimit'] == 0)
			{
				$t['Annahmerichtlinien'][4]['A'][] = array('checkmark' => 'yellow', 'content' => "<strong>Nein</strong>, bei Abschluss vorhandener <strong>festsitzender Zahnersatz</strong> ist <strong>nicht</strong> mit versichert.");
				$t['Kurz']['Sonstiges']['Vorhandene Kronen, Brücken & Füllungen versichert?'] = array('checkmark' => 'black', 'content' => "nicht mit versicherbar");

			}

			if($t1['fixedDenturesLimit'] > 0)
			{
				if(isset($t1['riskPremiumRemovableDenturesPercentage']) && $t1['riskPremiumFixedDenturesPercentage'] > 0)
				{
					$t['Annahmerichtlinien'][4]['A'][] = array('checkmark' => 'yellow', 'content' => "Je ersetztem Zahn wird ein <strong>Risikozuschlag</strong> in Höhe von <strong>".$this->getMoneyFormat($t1['riskPremiumFixedDenturesPercentage'], 0, 1)."</strong> berechnet.");
				}
				if(isset($t1['riskPremiumRemovableDenturesAmount']) && $t1['riskPremiumFixedDenturesAmount'] > 0)
				{
					$t['Annahmerichtlinien'][4]['A'][] = array('checkmark' => 'yellow', 'content' => "Je ersetztem Zahn wird ein <strong>Risikozuschlag</strong> in Höhe von <strong>".$this->getMoneyFormat($t1['riskPremiumFixedDenturesAmount'], 1)."</strong> berechnet.");
				}
			}

		}

		if(isset($t1['fixedDenturesText']))
		{
			$t['Annahmerichtlinien'][4]['A'][] = array('checkmark' => '', 'content' => "individueller Text");
		}

		if(isset($t1['exlusionOfBenefitsFixedDenturesPossible']) && $t1['exlusionOfBenefitsFixedDenturesPossible'] == 1)
		{
			$t['Annahmerichtlinien'][4]['A'][] = array('checkmark' => 'green', 'content' => "Alternativ können ersetzte Zähne auf Wunsch des Versicherungsnehmers vom Versicherungsschutz ausgeschlossen werden – in diesem Fall wird auf die Berechnung von Risikozuschlägen verzichtet.");
		}



	// Was versteht der Versicherer unter Zahnersatz?
	/*
		$t['Zahnersatz'][0]['Q'] = 'Was versteht der Versicherer unter Zahnersatz?';
		
		$t['Zahnersatz'][0]['A'];

		$t['Zahnersatz'][0]['A'][] = array('checkmark' => '', 'content' => "Gemäß Bedingungen des Tarifes ".$this->convert($t1['name'])." versteht man unter Zahnersatz:");
		
		$t['Zahnersatz'][0]['A'][] = array('content-list' => array('Kronen', 'Brücken', 'Prothesen', 'Implantate', 'Inlays (Einlagefüllungen)', 'usw. je nach Tarif'));

		$t['Zahnersatz'][0]['A'][] = array('checkmark' => '', 'content' => "Versichert ist grundsätzlich die medizinisch notwendige Versorgung mit Zahnersatz (sowohl Neuanfertigung als auch Reparaturen an vorhandenem Zahnersatz).");

		// keine Leistung
		$t['Zahnersatz'][0]['A'][] = array('checkmark' => 'yellow', 'content' => "Der Tarif sieht keinerlei Leistungen für Zahnersatz vor.");		
	*/


	// In welcher Höhe leistet der Tarif für hochwertigere Kronen, Brücken und Prothesen?
		$t['Kronen, Brücken und Prothesen'][0]['Q'] = "In welcher Höhe leistet der Tarif für hochwertigere Kronen, Brücken und Prothesen?";
		
		$t['Kronen, Brücken und Prothesen'][0]['A'];


		if(isset($t1['denturesFixedAllowanceInPercent']) && $t1['denturesFixedAllowanceInPercent'] >= 1)
		{
			$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => "green", 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> sieht keine prozentuale Leistung auf Basis der privatzahnärztlichen GOZ-Abrechnung vor, sondern die Leistung ist abhängig vom Festzuschuss der gesetzlichen Krankenkasse. <p>Die Leistung beträgt <strong>".$this->getMoneyFormat($t1['denturesFixedAllowanceInPercent'], 0, 1)."</strong> des gesetzlichen Festzuschusses.</p>");
			
			switch($t1['denturesFixedAllowanceInPercent'])
			{
				case 100:
					$t['Kurz']['Zahnersatz']['Kronen, Brücken und Prothesen'] = array('checkmark' => 'green', 'content' => 'Verdopplung Festzuschuss');					
					break;
				case 200:
					$t['Kurz']['Zahnersatz']['Kronen, Brücken und Prothesen'] = array('checkmark' => 'green', 'content' => 'Verdreifachung Festzuschuss');					
					break;
				default:
					$t['Kurz']['Zahnersatz']['Kronen, Brücken und Prothesen'] = array('checkmark' => 'green', 'content' => "<strong>".$this->getMoneyFormat($t1['denturesFixedAllowanceInPercent'], 0, 1)."</strong> vom gesetzlichen Festzuschuss");									
					break;
			}
			if($t1['veneersInsured'] && $t1['veneersInsured'] == 1)
				$t['Kurz']['Zahnersatz']['Veneers lt. AVB versichert'] = array('checkmark' => 'green', 'content' => '<strong>ja</strong>, versichert');
			$t['Kurz']['Zahnersatz']['Keramikverblendungen'] = array('checkmark' => 'red', 'content' => 'nicht versichert');
		}

		if(isset($t1['hasHealthCareIncludedInBenefitsPercentDentures']))
		{
			switch($t1['hasHealthCareIncludedInBenefitsPercentDentures'])
			{
				case 2:
					#Restkostenerstattung
					if(is_array($ze_prothesen))
					{
						$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für hochwertige <strong>Kronen, Brücken und Prothesen</strong>:");
						$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => '', 'content-list' => array('ohne Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['crownsBridgesWithoutBonusInPercent'], 0, 1)."</strong> nach GKV", '5 Jahre Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['crownsBridgesWith5YearsBonusInPercent'], 0, 1)."</strong> nach GKV", '10 Jahre Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['crownsBridgesWith10YearsBonusInPercent'], 0, 1)."</strong> nach GKV"));
						$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => '', 'content' => "„nach GKV“ bedeutet, dass zuerst eine etwaige Vorleistung der gesetzlichen Krankenkasse abgezogen wird – der Prozentsatz bezieht sich auf den verbleibenden Eigenanteil.“<p>Keramische Verblendungen an Kronen oder Brücken werden bis einschließlich <strong>Zahn Nr. ".$this->getMoneyFormat($t1['crownsBridgesDeduction'])."</strong> erstattet.</p>");

						$t['Kurz']['Zahnersatz']['Kronen, Brücken und Prothesen'] = array('checkmark' => 'green', 'content' => "<strong>".$this->getMoneyFormat($t1['crownsBridgesWithoutBonusInPercent'])." - ".$this->getMoneyFormat($t1['crownsBridgesWith10YearsBonusInPercent'], 0, 1)."</strong> <span class='description'>nach. GKV</span>");
						if($t1['veneersInsured'] && $t1['veneersInsured'] == 1)
							$t['Kurz']['Zahnersatz']['Veneers lt. AVB versichert'] = array('checkmark' => 'green', 'content' => '<strong>Ja</strong>, versichert');
	
						$t['Kurz']['Zahnersatz']['Keramikverblendungen'] = array('checkmark' => 'green', 'content' => "<strong>ja</strong>, bis Zahn ".$this->getMoneyFormat($t1['crownsBridgesDeduction'])." (inkl.)");
					} 
					elseif(isset($ze_prothesen))
					{
						$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für hochwertige <strong>Kronen, Brücken und Prothesen</strong>:");
						$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => '','content-list' => array('unabhängig vom Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['crownsBridgesWithoutBonusInPercent'], 0, 1)."</strong> <span class='description'>nach. Vorleistung der GKV</span>"));
						$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => '', 'content' => "„nach GKV“ bedeutet, dass zuerst eine etwaige Vorleistung der gesetzlichen Krankenkasse abgezogen wird – der Prozentsatz bezieht sich auf den verbleibenden Eigenanteil.“<p>Keramische Verblendungen an Kronen oder Brücken werden bis einschließlich <strong>Zahn Nr. ".$this->getMoneyFormat($t1['crownsBridgesDeduction'])."</strong> erstattet.");

						$t['Kurz']['Zahnersatz']['Kronen, Brücken und Prothesen'] = array('checkmark' => 'green', 'content' => "<strong>".$this->getMoneyFormat($t1['crownsBridgesWithoutBonusInPercent'], 0, 1)."</strong> <span class='description'>nach. GKV</span>");
						if($t1['veneersInsured'] && $t1['veneersInsured'] == 1)
							$t['Kurz']['Zahnersatz']['Veneers lt. AVB versichert'] = array('checkmark' => 'green', 'content' => '<strong>Ja</strong>, versichert');

						$t['Kurz']['Zahnersatz']['Keramikverblendungen'] = array('checkmark' => 'green', 'content' => "<strong>ja</strong>, bis Zahn ".$this->getMoneyFormat($t1['crownsBridgesDeduction'])." (inkl.)");
					}
				break;

				case 1:
					#Ja
					if(is_array($ze_prothesen))
					{
						$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für hochwertige <strong>Kronen, Brücken und Prothesen</strong>:");
						$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => '', 'content-list' => array('ohne Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['crownsBridgesWithoutBonusInPercent'], 0, 1)."</strong> inkl. Vorleistung der GKV", '5 Jahre Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['crownsBridgesWith5YearsBonusInPercent'], 0, 1)."</strong> inkl. Vorleistung der GKV", '10 Jahre Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['crownsBridgesWith10YearsBonusInPercent'], 0, 1)."</strong> inkl. Vorleistung der GKV"));
						$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => '', 'content' => "„Inkl. GKV“ bedeutet, dass in den genannten Prozentsätzen bereits etwaige Leistungen Ihrer gesetzlichen Krankenversicherung berücksichtigt sind.<p>Keramische Verblendungen an Kronen oder Brücken werden bis einschließlich <strong>Zahn Nr. ".$this->getMoneyFormat($t1['crownsBridgesDeduction'])."</strong> erstattet.</p>");

						$t['Kurz']['Zahnersatz']['Kronen, Brücken und Prothesen'] = array('checkmark' => 'green', 'content' => "<strong>".$this->getMoneyFormat($t1['crownsBridgesWithoutBonusInPercent'])." - ".$this->getMoneyFormat($t1['crownsBridgesWith10YearsBonusInPercent'], 0, 1)."</strong> <span class='description'>inkl. GKV</span>");
						if($t1['veneersInsured'] && $t1['veneersInsured'] == 1)
							$t['Kurz']['Zahnersatz']['Veneers lt. AVB versichert'] = array('checkmark' => 'green', 'content' => '<strong>ja</strong>, versichert');

						$t['Kurz']['Zahnersatz']['Keramikverblendungen'] = array('checkmark' => 'green', 'content' => "<strong>ja</strong>, bis Zahn ".$this->getMoneyFormat($t1['crownsBridgesDeduction'])." (inkl.)");
					} 
					elseif(isset($ze_prothesen))
					{
						$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für hochwertige <strong>Kronen, Brücken und Prothesen</strong>:");
						$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => '','content-list' => array('unabhängig vom Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['crownsBridgesWithoutBonusInPercent'], 0, 1)."</strong> <span class='description'>inkl. Vorleistung der GKV</span>"));
						$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => '', 'content' => "„Inkl. GKV“ bedeutet, dass in den genannten Prozentsätzen bereits etwaige Leistungen Ihrer gesetzlichen Krankenversicherung berücksichtigt sind.<p>Keramische Verblendungen an Kronen oder Brücken werden bis einschließlich <strong>Zahn Nr. ".$this->getMoneyFormat($t1['crownsBridgesDeduction'])."</strong> erstattet.");

						$t['Kurz']['Zahnersatz']['Kronen, Brücken und Prothesen'] = array('checkmark' => 'green', 'content' => "<strong>".$this->getMoneyFormat($t1['crownsBridgesWithoutBonusInPercent'], 0, 1)."</strong> <span class='description'>inkl. GKV</span>");
						if($t1['veneersInsured'] && $t1['veneersInsured'] == 1)
							$t['Kurz']['Zahnersatz']['Veneers lt. AVB versichert'] = array('checkmark' => 'green', 'content' => '<strong>ja</strong>, versichert');

						$t['Kurz']['Zahnersatz']['Keramikverblendungen'] = array('checkmark' => 'green', 'content' => "<strong>ja</strong>, bis Zahn ".$this->getMoneyFormat($t1['crownsBridgesDeduction'])." (inkl.)");
					}
				break;
				case 0:
					#Nein
					if(is_array($ze_prothesen))
					{
						$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für hochwertige <strong>Kronen, Brücken und Prothesen</strong>:");
						$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => '','content-list' => array('ohne Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['crownsBridgesWithoutBonusInPercent'], 0, 1)."</strong> + Festzuschuss der GKV", '5 Jahre Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['crownsBridgesWith5YearsBonusInPercent'], 0, 1)."</strong> + Festzuschuss der GKV", '10 Jahre Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['crownsBridgesWith10YearsBonusInPercent'], 0, 1)."</strong> + Festzuschuss der GKV"));
						$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => '', 'content' => "Zusammen mit den Vorleistungen Ihrer GKV dürfen insgesamt ".$this->getMoneyFormat($t1['maxReimbursementInclHealthCareInPercent'], 1)." der erstattungsfähigen Aufwendungen nicht überschritten werden (maximale Gesamterstattung inkl. GKV-Festzuschuss).</p><p>Keramische Verblendungen an Kronen oder Brücken werden bis einschließlich <strong>Zahn Nr. ".$this->getMoneyFormat($t1['crownsBridgesDeduction'])."</strong> erstattet.</p>");

						$t['Kurz']['Zahnersatz']['Kronen, Brücken und Prothesen'] = array('checkmark' => 'green', 'content' => "<strong>".$this->getMoneyFormat($t1['crownsBridgesWithoutBonusInPercent'])." - ".$this->getMoneyFormat($t1['crownsBridgesWith10YearsBonusInPercent'], 0, 1)."</strong> <span class='description'>+ Festzuschuss der GKV</span>");
						if($t1['veneersInsured'] && $t1['veneersInsured'] == 1)
							$t['Kurz']['Zahnersatz']['Veneers lt. AVB versichert'] = array('checkmark' => 'green', 'content' => '<strong>ja</strong>, versichert');

						$t['Kurz']['Zahnersatz']['Keramikverblendungen'] = array('checkmark' => 'green', 'content' => "<strong>ja</strong>, bis Zahn ".$this->getMoneyFormat($t1['crownsBridgesDeduction'])." (inkl.)");
					}
					elseif(isset($ze_prothesen))
					{
						$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für hochwertige <strong>Kronen, Brücken und Prothesen</strong>:");
						$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => '','content-list' => array('unabhängig vom Bonusheft:' => "<strong>".$t1['crownsBridgesWithoutBonusInPercent']."</strong>% + Festzuschuss der GKV"));
						$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => '', 'content' => "Zusammen mit den Vorleistungen Ihrer GKV dürfen insgesamt ".$t1['maxReimbursementInclHealthCareInPercent']."% der erstattungsfähigen Aufwendungen nicht überschritten werden (maximale Gesamterstattung inkl. GKV-Festzuschuss).<p>Keramische Verblendungen an Kronen oder Brücken werden bis einschließlich <strong>Zahn Nr. ".$this->getMoneyFormat($t1['crownsBridgesDeduction'])."</strong> erstattet.");
				
						$t['Kurz']['Zahnersatz']['Kronen, Brücken und Prothesen'] = array('checkmark' => 'green', 'content' => "<strong>".$this->getMoneyFormat($t1['crownsBridgesWithoutBonusInPercent'], 0, 1)."</strong> <span class='description'>+ Festzuschuss der GKV</span>");
						if($t1['veneersInsured'] && $t1['veneersInsured'] == 1)
							$t['Kurz']['Zahnersatz']['Veneers lt. AVB versichert'] = array('checkmark' => 'green', 'content' => '<strong>ja</strong>, versichert');

						$t['Kurz']['Zahnersatz']['Keramikverblendungen'] = array('checkmark' => 'green', 'content' => "<strong>ja</strong>, bis Zahn ".$this->getMoneyFormat($t1['crownsBridgesDeduction'])." (inkl.)");
					}
				break;
			}
		}


		if(isset($t1['denturesFixedAllowanceHardshipConsidered']) && $t1['denturesFixedAllowanceHardshipConsidered'] == "1")
		{
			$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => "", 'content' => "Dabei wird auch ein erhöhter Zuschuss aufgrund der sogenannten Härtefallregelung berücksichtigt.");
		}


		if($ze_prothesen == "0")
		{

			$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => 'red', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet nicht für Kronen, Brücken oder Prothesen.");

			$t['Kurz']['Zahnersatz']['Kronen, Brücken und Prothesen'] = array('checkmark' => 'yellow', 'content' => "keine Leistung");	
		}
		
		if(isset($t1['crownsBridgesLimit']))
		{
			if($t1['crownsBridgesLimit'] == 0)
			{
				$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => '', 'content' => "Verblendungen werden damit ohne Einschränkungen für alle Zähne übernommen.");	
			}
			elseif($t1['crownsBridgesLimit'] == 1)
			{
				$t['Kronen, Brücken und Prothesen'][0]['A'][] = array('checkmark' => '', 'content' => "Für die dahinter liegenden Backenzähne sind Kronen oder Brücken zwar prinzipiell auch versichert, aber nur ohne Ausführung einer gesondert berechneten Keramikverblendung.");	
			}
		}

		if(!isset($t['Kurz']['Zahnersatz']['Kronen, Brücken und Prothesen']))
			$t['Kurz']['Zahnersatz']['Kronen, Brücken und Prothesen'] = array('checkmark' => 'red', 'content' => $ze_prothesen."keine Leistung");

		if(!isset($t['Kurz']['Zahnersatz']['Veneers lt. AVB versichert']))
			$t['Kurz']['Zahnersatz']['Veneers lt. AVB versichert'] = array('checkmark' => 'red', 'content' => "</strong>Nein</strong>, nicht versichert");

		if(!isset($t['Kurz']['Zahnersatz']['Keramikverblendungen']))
			$t['Kurz']['Zahnersatz']['Keramikverblendungen'] = array('checkmark' => 'red', 'content' => "keine Leistung");



	// Leistet der Versicherer für Kronen, Brücken oder Prothesen auch ohne Leistung der GKV?
		$t['Kronen, Brücken und Prothesen'][1]['Q'] = "Leistet der Versicherer für Kronen, Brücken oder Prothesen auch ohne Leistung der GKV?";
		
		$t['Kronen, Brücken und Prothesen'][1]['A'];


		if(isset($t1['denturesFixedAllowanceInPercent']) && $t1['denturesFixedAllowanceInPercent'] >= 1)
		{
			$t['Kronen, Brücken und Prothesen'][1]['A'][] = array('checkmark' => "red", 'content' => "Die Leistung des Tarifes ist direkt abhängig vom Festzuschuss ihrer gesetzlichen Krankenversicherung. Erbringt die GKV keinen Zuschuss für Zahnersatz, so erhalten Sie keine Leistung aus Tarif <strong>".$this->convert($t1['name'])."</strong>");
		}			


		if(isset($ze_prothesen) && ((is_array($ze_prothesen) && $ze_prothesen[0] > 0) || (!is_array($ze_prothesen) && $ze_prothesen > 0)) )
		{
			if(isset($t1['crownsBridgesDeductionWithoutHealthCareNotSelfinflicted']))
			{
					if($t1['crownsBridgesDeductionWithoutHealthCareNotSelfinflicted'] == 0)
					{
						$t['Kronen, Brücken und Prothesen'][1]['A'][] = array('checkmark' => 'green', 'content' => "Steht dem Versicherten seitens der gesetzlichen Krankenkasse <strong>kein Festzuschuss</strong> für eine <strong>medizinisch notwendige Zahnersatzbehandlung</strong> zu, ohne dass der Versicherte dies selbst verschuldet hat, erhalten Sie aus Tarif ".$this->convert($t1['name'])." dennoch die <strong>vollen tariflichen Leistungen</strong>!");	
					}

					if(isset($t1['crownsBridgesDeductionWithoutHealthCareInPercentNotSelfInflicted']))
					{

						if($t1['crownsBridgesDeductionWithoutHealthCareNotSelfinflicted'] == 1 && $t1['crownsBridgesDeductionWithoutHealthCareInPercentNotSelfInflicted'] == 0)
						{
							$t['Kronen, Brücken und Prothesen'][1]['A'][] = array('checkmark' => 'black', 'content' => "Steht dem Versicherten seitens der gesetzlichen Krankenkasse <strong>kein Festzuschuss</strong> für eine <strong>medizinisch notwendige Zahnersatzbehandlung</strong> zu, ohne dass der Versicherte dies selbst verschuldet hat, <strong>kürzt</strong> der Versicherer die tarifliche Erstattungsleistung <strong>um eine fiktive GKV-Vorleistung –</strong> die Höhe dieser fiktiven GKV-Vorleistung richtet sich danach, wie viel die gesetzliche Krankenkasse im individuellen Leistungsfall konkret erstattet hätte!");	
						}

						if($t1['crownsBridgesDeductionWithoutHealthCareNotSelfinflicted'] == 1 && $t1['crownsBridgesDeductionWithoutHealthCareInPercentNotSelfInflicted'] > 0 && $t1['crownsBridgesDeductionWithoutHealthCareInPercentNotSelfInflicted'] < 100)
						{
							$t['Kronen, Brücken und Prothesen'][1]['A'][] = array('checkmark' => 'black', 'content' => "Steht dem Versicherten seitens der gesetzlichen Krankenkasse <strong>kein Festzuschuss</strong> für eine <strong>medizinisch notwendige Zahnersatzbehandlung</strong> zu, ohne dass der Versicherte dies selbst verschuldet hat, <strong>kürzt</strong> der Versicherer die tarifliche Erstattungsleistung um <strong>".$this->getMoneyFormat($t1['crownsBridgesDeductionWithoutHealthCareInPercentNotSelfInflicted'], 0, 1)."</strong>!");	
						}

						if($t1['crownsBridgesDeductionWithoutHealthCareNotSelfinflicted'] == 1 && $t1['crownsBridgesDeductionWithoutHealthCareInPercentNotSelfInflicted'] == 100)
						{
							$t['Kronen, Brücken und Prothesen'][1]['A'][] = array('checkmark' => 'red', 'content' => "Steht dem Versicherten seitens der gesetzlichen Krankenkasse kein Festzuschuss für eine medizinisch notwendige Zahnersatzbehandlung zu, ohne dass der Versicherte dies selbst verschuldet hat, erhalten Sie keine Leistung aus Tarif ".$this->convert($t1['name'])."! Die tarifliche Erstattung ist somit vollständig abhängig von einer Vorleistung der gesetzlichen Krankenversicherung.");	
						}
					}
			}

		
		}
		if($ze_prothesen == "0")
		{
			$t['Kronen, Brücken und Prothesen'][1]['A'][] = array('checkmark' => 'yellow', 'content' => "Der Tarif ".$this->convert($t1['name'])." leistet <string>nicht</string> für Kronen, Brücken oder Prothesen.");	
		}



		
	// In welcher Höhe leistet der Tarif bei Durchführung der Regelversorgung?
		$t['Kronen, Brücken und Prothesen'][2]['Q'] = "In welcher Höhe leistet der Tarif bei Durchführung der Regelversorgung?";
		
		$t['Kronen, Brücken und Prothesen'][2]['A'];

		$showExpertText = 'false';
		if(is_array($ze_regelversorgung))
		{
			$t['Kronen, Brücken und Prothesen'][2]['A'][] = array('checkmark' => '', 'content' => "Der Tarif ".$this->convert($t1['name'])." leistet im Rahmen der gesetzlichen Regelversorgung:");
			$t['Kronen, Brücken und Prothesen'][2]['A']['content'] = $this->getHTMLTableFromArray(array('ohne Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['regularCareWithoutBonusInPercent'], 0, 1).'</strong> inkl. Vorleistung GKV', '5 Jahre Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['regularCareWith5YearsBonusInPercent'], 0, 1).'</strong> inkl. Vorleistung GKV', '10 Jahre Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['regularCareWith10YearsBonusInPercent'], 0, 1).'</strong> inkl. Vorleistung GKV'), 'ze_regelversorgung');

			if($t1['regularCareWithoutBonusInPercent'] > 0)
				$showExpertText = 'true';
		}
		else
		{
			if($ze_regelversorgung == 0)
			{
				$t['Kronen, Brücken und Prothesen'][2]['A'][] = array('checkmark' => 'yellow', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet <strong>nicht</strong> Zahnersatz (auch nicht im Rahmen der gesetzlichen Regelversorgung)");	

			}
			elseif($ze_regelversorgung > 0)
			{
				$t['Kronen, Brücken und Prothesen'][2]['A'][] = array('checkmark' => '', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet im Rahmen der gesetzlichen Regelversorgung:<p><strong>".$this->getMoneyFormat($t1['regularCareWithoutBonusInPercent'], 0, 1)."</string> inkl. Vorleistung GKV</p>");	

				$showExpertText = 'true';
			}
		}


		if($showExpertText == 'true')
		{
			$t['Kronen, Brücken und Prothesen'][2]['A'][] = array('checkmark' => '', 'content-expert' => "<div class='headline'>Experten-Erklärung</div><div class='content'><p>Dieser Punkt dürfte für die <strong>Praxis eine eher untergeordnete Rolle</strong> spielen. Unter <strong>gesetzlicher Regelversorgung</strong> versteht man eine</p><ul><li>ausreichende</li><li>wirtschaftliche</li><li>zweckmäßige</li></ul><p>Versorgung mit Zahnersatz. Es handelt sich dabei quasi um die <strong>günstigste Möglichkeit</strong>, Ihr Gebiss mit <strong>Zahnersatz</strong> zu versorgen.</p><p>Die gesetzliche Krankenkasse erstattet Ihnen als <strong>Festzuschuss 50%</strong> der Kosten für die gesetzliche Regelversorgung. Durch regelmäßige zahnärztliche Vorsorge können Sie diesen Zuschuss auf <strong>60% (5 Jahre Bonusheft Nachweis)</strong> bzw. <strong>65% (10 Jahre Nachweis über Bonusheft)</strong> erhöhen.</p><p>Wenn Sie eine Zahnersatzversorgung <strong>ausschließlich im Rahmen der gesetzlichen Regelversorgung</strong> durchführen lassen und keinerlei private Vergütungsanteile gemäß GOZ (Gebührenordnung für Zahnärzte) berechnet werden, entstehen in der Praxis keine besonders hohen Kosten.</p><p>In diesem <strong>Sonderfall</strong> erstatten viele Versicherungsgesellschaften höhere Prozentsätze als für privatzahnärztliche Behandlungen. In der Praxis dürfte dieser Fall allerdings <strong>eher selten auftreten</strong> (dafür schließt man ja eigentlich eine Zahnzusatzversicherung ab, um die Zähne im Bedarfsfall mit hochwertigem Zahnersatz versorgen lassen zu können), daher sollten sie ihm keine besondere Bedeutung zukommen lassen.</p></div>");
		}
		
		
		

		

	// In welcher Höhe leistet der Versicherer für Implantate?
		$t['Implantate'][0]['Q'] = "In welcher Höhe leistet der Versicherer für Implantate?";
		
		$t['Implantate'][0]['A'];

		$showExpertText = 'false';	


		if(isset($t1['denturesFixedAllowanceInPercent']) && $t1['denturesFixedAllowanceInPercent'] >= 1)
		{
			$t['Implantate'][0]['A'][] = array('checkmark' => "green", 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> sieht keine prozentuale Leistung auf Basis der privatzahnärztlichen GOZ-Abrechnung vor, sondern die Leistung ist abhängig vom Festzuschuss der gesetzlichen Krankenkasse. Für das Implantat selbst erfolgt also keine Leistung aus diesem Tarif. <p>Sie erhalten allerdings in vielen Fällen einen Festzuschuss für den Zahnersatz, der auf das Implantat aufgesetzt wird (sogenannte Suprakonstruktion).</p><p>Die Leistung beträgt <strong>".$this->getMoneyFormat($t1['denturesFixedAllowanceInPercent'], 0, 1)."</strong> des gesetzlichen Festzuschusses.</p>");

			switch($t1['denturesFixedAllowanceInPercent'])
			{
				case 100:
					$t['Kurz']['Zahnersatz']['Implantate'] = array('checkmark' => 'green', 'content' => 'Verdopplung Festzuschuss');					
					break;
				case 200:
					$t['Kurz']['Zahnersatz']['Implantate'] = array('checkmark' => 'green', 'content' => 'Verdreifachung Festzuschuss');					
					break;
				default:
					$t['Kurz']['Zahnersatz']['Implantate'] = array('checkmark' => 'green', 'content' => "<strong>".$this->getMoneyFormat($t1['denturesFixedAllowanceInPercent'], 0, 1)."</strong> vom gesetzlichen Festzuschuss");									
					break;
			}
			$t['Kurz']['Zahnersatz']['Knochenaufbau Implantate'] = array('checkmark' => 'red', 'content' => 'nicht versichert');
			$t['Kurz']['Zahnersatz']['Implantate begrenzt'] = array('checkmark' => 'red', 'content' => 'nicht versichert');
			
		}

		if(isset($t1['denturesFixedAllowanceHardshipConsidered']) && $t1['denturesFixedAllowanceHardshipConsidered'] == "1")
		{
			$t['Implantate'][0]['A'][] = array('checkmark' => "", 'content' => "Dabei wird auch ein erhöhter Zuschuss aufgrund der sogenannten Härtefallregelung berücksichtigt.");
		}


		if(isset($ze_implantate))
		{
			if(is_array($ze_implantate))
			{
				if($t1['hasHealthCareIncludedInBenefitsPercentDentures'] == 2)
				{
					$t['Implantate'][0]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für Implantate:");
					$t['Implantate'][0]['A'][] = array('checkmark' => '', 'content' => $this->getHTMLTableFromArray(array('ohne Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['implantsWithoutBonusInPercent'], 0, 1)."</strong> nach. Vorleistung der GKV",'5 Jahre Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['implantsWith5YearsBonusInPercent'], 0, 1)."</strong> nach. Vorleistung der GKV",'10 Jahre Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['implantsWith10YearsBonusInPercent'], 0, 1)."</strong> nach. Vorleistung der GKV")), '');
					$t['Implantate'][0]['A'][] = array('checkmark' => '', 'content' => "„nach GKV“ bedeutet, dass zuerst eine etwaige Vorleistung der gesetzlichen Krankenkasse abgezogen wird – der Prozentsatz bezieht sich auf den verbleibenden Eigenanteil.");

					$t['Kurz']['Zahnersatz']['Implantate'] = array('checkmark' => 'green', 'content' => '<strong>'.$this->getMoneyFormat($t1['implantsWithoutBonusInPercent']).' - '.$this->getMoneyFormat($t1['implantsWith10YearsBonusInPercent'], 0, 1).'</strong> <span class="description">nach. GKV</span>');
				}

				if($t1['hasHealthCareIncludedInBenefitsPercentDentures'] == 1)
				{
					$t['Implantate'][0]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für Implantate:");
					$t['Implantate'][0]['A'][] = array('checkmark' => '', 'content' => $this->getHTMLTableFromArray(array('ohne Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['implantsWithoutBonusInPercent'], 0, 1)."</strong> inkl. Vorleistung der GKV",'5 Jahre Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['implantsWith5YearsBonusInPercent'], 0, 1)."</strong> inkl. Vorleistung der GKV",'10 Jahre Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['implantsWith10YearsBonusInPercent'], 0, 1)."</strong> inkl. Vorleistung der GKV")), '');
					$t['Implantate'][0]['A'][] = array('checkmark' => '', 'content' => "„inkl. GKV“ bedeutet, dass in den genannten Prozentsätzen bereits der Festzuschuss Ihrer gesetzlichen Krankenversicherung berücksichtigt ist.");

					$t['Kurz']['Zahnersatz']['Implantate'] = array('checkmark' => 'green', 'content' => '<strong>'.$this->getMoneyFormat($t1['implantsWithoutBonusInPercent']).' - '.$this->getMoneyFormat($t1['implantsWith10YearsBonusInPercent'], 0, 1).'</strong> <span class="description">inkl. GKV</span>');
				}

				if($t1['hasHealthCareIncludedInBenefitsPercentDentures'] == 0)
				{
					$t['Implantate'][0]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für Implantate:");
					$t['Implantate'][0]['A'][] = array('checkmark' => '', 'content' => $this->getHTMLTableFromArray(array('ohne Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['implantsWithoutBonusInPercent'], 0, 1)." + Festzuschuss der GKV",'5 Jahre Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['implantsWith5YearsBonusInPercent'], 0, 1)."</strong> + Festzuschuss der GKV",'10 Jahre Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['implantsWith10YearsBonusInPercent'], 0, 1)."</strong> + Festzuschuss der GKV")), '');
					$t['Implantate'][0]['A'][] = array('checkmark' => '', 'content' => "Zusammen mit den Vorleistungen Ihrer GKV dürfen insgesamt WertID60% der erstattungsfähigen Aufwendungen nicht überschritten werden (maximale Gesamterstattung inkl. GKV-Festzuschuss).");

					$t['Kurz']['Zahnersatz']['Implantate'] = array('checkmark' => 'green', 'content' => '<strong>'.$this->getMoneyFormat($t1['implantsWithoutBonusInPercent']).' - '.$this->getMoneyFormat($t1['implantsWith10YearsBonusInPercent'], 0, 1).'</strong> <span class="description">+ Festzuschuss der GKV</span>');
				}

				if($t1['implantsWithoutBonusInPercent'] > 0)
					$showExpertText = 'true';

			}
			else
			{
				if($ze_implantate == 0)
				{
					$t['Implantate'][0]['A'][] = array('checkmark' => 'yellow', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet <strong>nicht</strong> für Implantate.");
				}
				else
				{
					if($t1['hasHealthCareIncludedInBenefitsPercentDentures'] == 2)
					{
						$t['Implantate'][0]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für <strong>Implantate</strong>:");
						$t['Implantate'][0]['A'][] = array('checkmark' => '', 'content-list' => array('unabhängig vom Bonusheft:' => '<strong>'.$this->getMoneyFormat($ze_implantate, 0, 1).'</strong> nach. Vorleistung der GKV'));
						$t['Implantate'][0]['A'][] = array('checkmark' => '', 'content' => "„nach GKV“ bedeutet, dass zuerst eine etwaige Vorleistung der gesetzlichen Krankenkasse abgezogen wird – der Prozentsatz bezieht sich auf den verbleibenden Eigenanteil.");

						$t['Kurz']['Zahnersatz']['Implantate'] = array('checkmark' => 'green', 'content' => '<strong>'.$this->getMoneyFormat($ze_implantate, 0, 1).'</strong> <span class="description">nach. GKV</span>');
					}

					if($t1['hasHealthCareIncludedInBenefitsPercentDentures'] == 1)
					{
						$t['Implantate'][0]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für <strong>Implantate</strong>:");
						$t['Implantate'][0]['A'][] = array('checkmark' => '', 'content-list' => array('unabhängig vom Bonusheft:' => '<strong>'.$this->getMoneyFormat($ze_implantate, 0, 1).'</strong> inkl. Vorleistung der GKV'));
						$t['Implantate'][0]['A'][] = array('checkmark' => '', 'content' => "„inkl. GKV“ bedeutet, dass in den genannten Prozentsätzen bereits der Festzuschuss Ihrer gesetzlichen Krankenversicherung berücksichtigt ist.");

						$t['Kurz']['Zahnersatz']['Implantate'] = array('checkmark' => 'green', 'content' => '<strong>'.$this->getMoneyFormat($ze_implantate, 0, 1).'</strong> <span class="description">inkl. GKV</span>');
					}

					if($t1['hasHealthCareIncludedInBenefitsPercentDentures'] == 0)
					{
						$t['Implantate'][0]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für <strong>Implantate</strong>:");
						$t['Implantate'][0]['A'][] = array('checkmark' => '', 'content-list' => array('unabhängig vom Bonusheft:' => '<strong>'.$this->getMoneyFormat($ze_implantate, 0, 1).'</strong> + Festzuschuss der GKV'));
						$t['Implantate'][0]['A'][] = array('checkmark' => '', 'content' => "Zusammen mit den Vorleistungen Ihrer GKV dürfen insgesamt <strong>".$this->getMoneyFormat($t1['maxReimbursementInclHealthCareInPercent'], 0, 1)."</strong> der erstattungsfähigen Aufwendungen nicht überschritten werden (<strong>maximale Gesamterstattung inkl. GKV-Festzuschuss</strong>).");

						$t['Kurz']['Zahnersatz']['Implantate'] = array('checkmark' => 'green', 'content' => '<strong>'.$this->getMoneyFormat($ze_implantate, 0, 1).'</strong> <span class="description">+ Festzuschuss der GKV</span>');
					}

					$showExpertText = 'true';
				}
			}
		}

		if(!isset($t['Kurz']['Zahnersatz']['Implantate']))
			$t['Kurz']['Zahnersatz']['Implantate'] = array('checkmark' => 'black', 'content' => 'keine Leistung');


		if(isset($t1['hasHealthCareIncludedInBenefitsPercentDentures']))
		{
			if($t1['hasHealthCareIncludedInBenefitsPercentDentures'] == 1)
			{
				$t['Implantate'][0]['A'][] = array('checkmark' => 'green', 'content' => "Im Rahmen einer Implantation ist auch ein medizinisch notwendiger <strong>Knochenaufbau</strong> mit künstlichem oder körpereigenem Material (sogenannte „Augumentation“) <strong>mit versichert</strong>.");
			}
			elseif($t1['hasHealthCareIncludedInBenefitsPercentDentures'] == 0)
			{
				$t['Implantate'][0]['A'][] = array('checkmark' => 'red', 'content' => "Ein im Rahmen einer Implantation notwendiger <strong>Knochenaufbau</strong> mit künstlichem oder körpereigenem Material ist in diesem Tarif <strong>nicht mit versichert</strong>.");
			}
		}

		if(isset($t1['implantsLimitInUpperJaw']) && isset($t1['implantsLimitInLowerJaw']))
		{
			if($t1['implantsLimitInUpperJaw'] == 0 && $t1['implantsLimitInLowerJaw'] == 0)
			{
				$t['Implantate'][0]['A'][] = array('checkmark' => 'green', 'content' => "Die <strong>Anzahl</strong> erstattungsfähiger Implantate ist <strong>tariflich nicht begrenzt</strong>. Die Erstattung richtet sich nach den medizinischen Erfordernissen des Einzelfalles.");

				$t['Kurz']['Zahnersatz']['Knochenaufbau Implantate'] = array('checkmark' => 'green', 'content' => "<strong>Ja</strong> <span class='description'>versichert</span>");
			}
			elseif($t1['implantsLimitInUpperJaw'] > 0 || $t1['implantsLimitInLowerJaw'] > 0)
			{
				$t['Implantate'][0]['A'][] = array('checkmark' => 'yellow', 'content' => "Der Versicherer begrenzt die maximale Anzahl erstattungsfähiger Implantate tariflich auf:");
				$t['Implantate'][0]['A'][] = array('checkmark' => '', 'content-list' => array($t1['implantsLimitInUpperJaw']." im Oberkiefer", $t1['implantsLimitInLowerJaw']." im Unterkiefer"));

				$t['Kurz']['Zahnersatz']['Knochenaufbau Implantate'] = array('checkmark' => 'green', 'content' => "<strong>Ja</strong>, versichert");
			}
		}

		if(isset($t1['hasImplantsLimitAmount']) && isset($t1['hasMaterialCostRepository']))
		{
			if($t1['hasImplantsLimitAmount'] == 0 && $t1['hasMaterialCostRepository'] == 0)
			{
				$t['Implantate'][0]['A'][] = array('checkmark' => 'green', 'content' => "Die <strong>Erstattungsleistung</strong> für Implantate ist tariflich nicht auf einen bestimmten Höchstbetrag je Implantat begrenzt.");

				$t['Kurz']['Zahnersatz']['Implantate begrenzt'] = array('checkmark' => 'green', 'content' => "<strong>Keine</strong> tarifliche Begrenzung");

				
			}
			if($t1['hasImplantsLimitAmount'] == 0 && $t1['hasMaterialCostRepository'] == 1)
			{
				$t['Implantate'][0]['A'][] = array('checkmark' => 'yellow', 'content' => "Die <strong>Erstattungsleistung</strong> für Implantate ist tariflich nicht auf einen bestimmten Höchstbetrag je Implantat begrenzt.<p>Zu beachten ist jedoch das Preis- und Leistungsverzeichnis für Materialkosten. Hier sind Höchstbeträge für Material- und Laborkosten definiert, die auch für Implantate gelten.</p>");

				$t['Kurz']['Zahnersatz']['Implantate begrenzt'] = array('checkmark' => 'yellow', 'content' => "Begrenzung vorhanden");
			}
		} 

		if(isset($t1['hasImplantsLimitAmount']))
		{
			if($t1['hasImplantsLimitAmount'] == 1)
			{
				$t['Implantate'][0]['A'][] = array('checkmark' => 'yellow', 'content' => "Die Erstattungsleistung für Implantate ist tariflich in folgender Höhe <strong>begrenzt</strong>:");

				if(isset($t1['maxInvoiceAmountPerImplant']))
				{
					if($t1['maxInvoiceAmountPerImplant'] > 0)
					{
						$t['Implantate'][0]['A']['content-list'] = array("max. ".$this->getMoneyFormat($t1['maxInvoiceAmountPerImplant'], 1)." erstattungsfähiger Rechnungsbetrag je Implantat");
					}
				}

				if(isset($t1['maxAmountOfBenefitsPerImplant']))
				{
					if($t1['maxAmountOfBenefitsPerImplant'] > 0)
					{
						$t['Implantate'][0]['A']['content-list'] = array("max. ".$this->getMoneyFormat($t1['maxAmountOfBenefitsPerImplant'], 1)." Leistung je Implantat");
					}
				}

				if(isset($t1['maxInvoiceAmountPerImplantPerYear']))
				{
					if($t1['maxInvoiceAmountPerImplantPerYear'] > 0)
					{
						$t['Implantate'][0]['A']['content-list'] = array("max. ".$this->getMoneyFormat($t1['maxInvoiceAmountPerImplantPerYear'], 1)." erstattungsfähiger Rechnungsbetrag je Versicherungsjahr");
					}
				}

				if(isset($t1['maxAmountOfBenefitsPerImplantPerYear']))
				{
					if($t1['maxAmountOfBenefitsPerImplantPerYear'] > 0)
					{
						$t['Implantate'][0]['A']['content-list'] = array("max. ".$this->getMoneyFormat($t1['maxAmountOfBenefitsPerImplantPerYear'], 1)." Leistung je Versicherungsjahr");
					}
				}
			}
		}

		if($showExpertText == 'true')
		{
			$t['Implantate'][0]['A'][] = array('content-expert' => "<div class='headline'>Experten-Erklärung</div><div class='content'><p>Dieser Punkt dürfte für die <strong>Praxis eine eher untergeordnete Rolle</strong> spielen. Unter <strong>gesetzlicher Regelversorgung</strong> versteht man eine</p><ul><li>ausreichende</li><li>wirtschaftliche</li><li>zweckmäßige</li></ul><p>Versorgung mit Zahnersatz. Es handelt sich dabei quasi um die <strong>günstigste Möglichkeit</strong>, Ihr Gebiss mit <strong>Zahnersatz</strong> zu versorgen.</p><p>Die gesetzliche Krankenkasse erstattet Ihnen als <strong>Festzuschuss 50%</strong> der Kosten für die gesetzliche Regelversorgung. Durch regelmäßige zahnärztliche Vorsorge können Sie diesen Zuschuss auf <strong>60% (5 Jahre Bonusheft Nachweis)</strong> bzw. <strong>65% (10 Jahre Nachweis über Bonusheft)</strong> erhöhen.</p><p>Wenn Sie eine Zahnersatzversorgung <strong>ausschließlich im Rahmen der gesetzlichen Regelversorgung</strong> durchführen lassen und keinerlei private Vergütungsanteile gemäß GOZ (Gebührenordnung für Zahnärzte) berechnet werden, entstehen in der Praxis keine besonders hohen Kosten.</p><p>In diesem <strong>Sonderfall</strong> erstatten viele Versicherungsgesellschaften höhere Prozentsätze als für privatzahnärztliche Behandlungen. In der Praxis dürfte dieser Fall allerdings <strong>eher selten auftreten</strong> (dafür schließt man ja eigentlich eine Zahnzusatzversicherung ab, um die Zähne im Bedarfsfall mit hochwertigem Zahnersatz versorgen lassen zu können), daher sollten sie ihm keine besondere Bedeutung zukommen lassen.</p></div>");
		}

	// Leistet der Versicherer für Implantate auch ohne Vorleistung der GKV?
		$t['Implantate'][1]['Q'] = "Leistet der Versicherer für Implantate auch ohne Vorleistung der GKV?";
		
		$t['Implantate'][1]['A'];

		if(isset($ze_implantate) && isset($t1['implantsWithoutBonusInPercent']) &&  $t1['implantsWithoutBonusInPercent'] > 0)
		{
			if(isset($t1['implantsDeductionWithoutHealthCareNotSelfInflicted']) && $t1['implantsDeductionWithoutHealthCareNotSelfInflicted'] == 0)
			{
				$t['Implantate'][1]['A'][] = array('checkmark' => 'green', 'content' => "Steht dem Versicherten seitens der gesetzlichen Krankenkasse kein Festzuschuss für eine medizinisch notwendige Implantatbehandlung zu, ohne dass der Versicherte dies selbst verschuldet hat, erhalten Sie aus Tarif ".$this->convert($t1['name'])." dennoch die vollen tariflichen Leistungen!");
			}

			if(isset($t1['implantsDeductionInPercentNotSelfInflicted']))
			{
				if(isset($t1['implantsDeductionWithoutHealthCareNotSelfInflicted']) && $t1['implantsDeductionWithoutHealthCareNotSelfInflicted'] == 1)
				{
					if($t1['implantsDeductionInPercentNotSelfInflicted'] == 0)
					{
						$t['Implantate'][1]['A'][] = array('checkmark' => 'black', 'content' => "Steht dem Versicherten seitens der gesetzlichen Krankenkasse <strong>kein Festzuschuss</strong> für eine <strong>medizinisch notwendige Zahnersatzbehandlung</strong> zu, ohne dass der Versicherte dies selbst verschuldet hat, <strong>kürzt</strong> der Versicherer die tarifliche Erstattungsleistung <strong>um eine fiktive GKV-Vorleistung</strong> – die Höhe dieser fiktiven GKV-Vorleistung richtet sich danach, wie viel die gesetzliche Krankenkasse im individuellen Leistungsfall konkret erstattet hätte!");
					}

					if($t1['implantsDeductionInPercentNotSelfInflicted'] > 0 && $t1['implantsDeductionInPercentNotSelfInflicted'] < 100)
					{
						$t['Implantate'][1]['A'][] = array('checkmark' => 'black', 'content' => "Steht dem Versicherten seitens der gesetzlichen Krankenkasse <strong>kein Festzuschuss</strong> für eine <strong>medizinisch notwendige Zahnersatzbehandlung</strong> zu, ohne dass der Versicherte dies selbst verschuldet hat, <strong>kürzt</strong> der Versicherer die tarifliche Erstattungsleistung um <strong>".$this->getMoneyFormat($t1['implantsDeductionInPercentNotSelfInflicted'], 0, 1)."</strong>!");
					}

					if($t1['implantsDeductionInPercentNotSelfInflicted'] == 100)
					{
						$t['Implantate'][1]['A'][] = array('checkmark' => 'red', 'content' => "Steht dem Versicherten seitens der gesetzlichen Krankenkasse kein Festzuschuss für eine medizinisch notwendige Zahnersatzbehandlung zu, ohne dass der Versicherte dies selbst verschuldet hat, erhalten Sie keine Leistung aus Tarif ".$this->convert($t1['name'])."! Die tarifliche Erstattung ist somit vollständig abhängig von einer Vorleistung der gesetzlichen Krankenversicherung.");
					}
				}
			}

		}

		if(isset($ze_implantate) && $ze_implantate == 0)
		{
			$t['Implantate'][1]['A'][] = array('checkmark' => 'red', 'content' => "Der Tarif ".$this->convert($t1['name'])." leistet nicht für Implantate.");
		}

		if(isset($t1['denturesFixedAllowanceInPercent']) && $t1['denturesFixedAllowanceInPercent'] >= 1)
		{
			$t['Implantate'][1]['A'][] = array('checkmark' => "red", 'content' => "Die Leistung des Tarifes ist direkt abhängig vom Festzuschuss ihrer gesetzlichen Krankenversicherung. Erbringt die GKV keinen Zuschuss für Zahnersatz, so erhalten Sie keine Leistung aus Tarif <strong>".$this->convert($t1['name'])."</strong>.");
		}



	// In welcher Höhe leistet der Versicherer für Inlays?
		$t['Inlays'][0]['Q'] = "In welcher Höhe leistet der Versicherer für Inlays?";
		
		$t['Inlays'][0]['A'];

		$showExpertText = 'false';	
		if(isset($ze_inlays))
		{
			if(is_array($ze_inlays))
			{
				if($t1['hasHealthCareIncludedInBenefitsPercentDentures'] == 2)
				{
					$t['Inlays'][0]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für Inlays:");
					$t['Inlays'][0]['A'][] = array('checkmark' => '', 'content' => $this->getHTMLTableFromArray(array('ohne Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['inlaysWithoutBonusInPercent'], 0, 1)."</strong> nach. Vorleistung der GKV",'5 Jahre Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['inlaysWith5YearsBonusInPercent'], 0, 1)."</strong> nach. Vorleistung der GKV",'10 Jahre Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['inlaysWith10YearsBonusInPercent'], 0, 1)."</strong> nach. Vorleistung der GKV")), 'hasHealthCareIncludedInBenefitsPercentDentures');
					$t['Inlays'][0]['A'][] = array('checkmark' => '', 'content' => "„nach GKV“ bedeutet, dass zuerst eine etwaige Vorleistung der gesetzlichen Krankenkasse abgezogen wird – der Prozentsatz bezieht sich auf den verbleibenden Eigenanteil.");

					$t['Kurz']['Zahnersatz']['Inlays'] = array('checkmark' => 'green', 'content' => '<strong>'.$this->getMoneyFormat($t1['inlaysWithoutBonusInPercent']).' - '.$this->getMoneyFormat($t1['inlaysWith10YearsBonusInPercent'], 0, 1).'</strong> <span class="description">nach. GKV</span>');
				}

				if($t1['hasHealthCareIncludedInBenefitsPercentDentures'] == 1)
				{
					$t['Inlays'][0]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für Inlays:");
					$t['Inlays'][0]['A'][] = array('checkmark' => '', 'content' => $this->getHTMLTableFromArray(array('ohne Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['inlaysWithoutBonusInPercent'], 0, 1)."</strong> inkl. Vorleistung der GKV",'5 Jahre Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['inlaysWith5YearsBonusInPercent'], 0, 1)."</strong> inkl. Vorleistung der GKV",'10 Jahre Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['inlaysWith10YearsBonusInPercent'], 0, 1)."</strong> inkl. Vorleistung der GKV")), 'hasHealthCareIncludedInBenefitsPercentDentures');
					$t['Inlays'][0]['A'][] = array('checkmark' => '', 'content' => "„inkl. GKV“ bedeutet, dass in den genannten Prozentsätzen bereits der Festzuschuss Ihrer gesetzlichen Krankenversicherung berücksichtigt ist.");

					$t['Kurz']['Zahnersatz']['Inlays'] = array('checkmark' => 'green', 'content' => '<strong>'.$this->getMoneyFormat($t1['inlaysWithoutBonusInPercent']).' - '.$this->getMoneyFormat($t1['inlaysWith10YearsBonusInPercent'], 0, 1).'</strong> <span class="description">inkl. GKV</span>');
				}

				if($t1['hasHealthCareIncludedInBenefitsPercentDentures'] == 0)
				{
					$t['Inlays'][0]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für Inlays:");
					$t['Inlays'][0]['A'][] = array('checkmark' => '', 'content' => $this->getHTMLTableFromArray(array('ohne Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['inlaysWithoutBonusInPercent'], 0, 1)."</strong> + Festzuschuss der GKV",'5 Jahre Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['inlaysWith5YearsBonusInPercent'], 0, 1)."</strong> + Festzuschuss der GKV",'10 Jahre Bonusheft:' => "<strong>".$this->getMoneyFormat($t1['inlaysWith10YearsBonusInPercent'], 0, 1)."</strong> + Festzuschuss der GKV")), 'hasHealthCareIncludedInBenefitsPercentDentures');
					$t['Inlays'][0]['A'][] = array('checkmark' => '', 'content' => "Zusammen mit den Vorleistungen Ihrer GKV dürfen insgesamt ".$this->getMoneyFormat($t1['maxReimbursementInclHealthCareInPercent'], 0, 1)." der erstattungsfähigen Aufwendungen nicht überschritten werden (maximale Gesamterstattung inkl. GKV-Festzuschuss).");

					$t['Kurz']['Zahnersatz']['Inlays'] = array('checkmark' => 'green', 'content' => '<strong>'.$this->getMoneyFormat($t1['inlaysWithoutBonusInPercent']).' - '.$this->getMoneyFormat($t1['inlaysWith10YearsBonusInPercent'], 0, 1).'</strong> <span class="description">+ Festzuschuss der GKV</span>');
				}

				if($t1['implantsWithoutBonusInPercent'] > 0)
					$showExpertText = 'true';

			}
			else
			{
				if($ze_inlays == 0)
				{
					$t['Inlays'][0]['A'][] = array('checkmark' => 'red', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet <strong>nicht</strong> für Inlays.");
				}
				else
				{
					if($t1['hasHealthCareIncludedInBenefitsPercentDentures'] == 2)
					{
						$t['Inlays'][0]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für <strong>Inlays</strong>:");
						$t['Inlays'][0]['A'][] = array('checkmark' => '', 'content-list' => array('unabhängig vom Bonusheft:' => '<strong>'.$this->getMoneyFormat($ze_inlays, 0, 1).'</strong> nach. Vorleistung der GKV'));
						$t['Inlays'][0]['A'][] = array('checkmark' => '', 'content' => "„nach GKV“ bedeutet, dass zuerst eine etwaige Vorleistung der gesetzlichen Krankenkasse abgezogen wird – der Prozentsatz bezieht sich auf den verbleibenden Eigenanteil.");

						$t['Kurz']['Zahnersatz']['Inlays'] = array('checkmark' => 'green', 'content' => '<strong>'.$this->getMoneyFormat($ze_inlays, 0, 1).'</strong> <span class="description">nach. GKV</span>');
					}

					if($t1['hasHealthCareIncludedInBenefitsPercentDentures'] == 1)
					{
						$t['Inlays'][0]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für <strong>Inlays</strong>:");
						$t['Inlays'][0]['A'][] = array('checkmark' => '', 'content-list' => array('unabhängig vom Bonusheft:' => '<strong>'.$this->getMoneyFormat($ze_inlays, 0, 1).'</strong> inkl. Vorleistung der GKV'));
						$t['Inlays'][0]['A'][] = array('checkmark' => '', 'content' => "„inkl. GKV“ bedeutet, dass in den genannten Prozentsätzen bereits der Festzuschuss Ihrer gesetzlichen Krankenversicherung berücksichtigt ist.");

						$t['Kurz']['Zahnersatz']['Inlays'] = array('checkmark' => 'green', 'content' => '<strong>'.$this->getMoneyFormat($ze_inlays, 0, 1).'</strong> <span class="description">inkl. GKV</span>');
					}

					if($t1['hasHealthCareIncludedInBenefitsPercentDentures'] == 0)
					{
						$t['Inlays'][0]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für <strong>Implantate</strong>:");
						$t['Inlays'][0]['A'][] = array('checkmark' => '', 'content-list' => array('unabhängig vom Bonusheft:' => '<strong>'.$this->getMoneyFormat($ze_implantate, 0, 1).'</strong> + Festzuschuss der GKV'));
						$t['Inlays'][0]['A'][] = array('checkmark' => '', 'content' => "Zusammen mit den Vorleistungen Ihrer GKV dürfen insgesamt <strong>".$this->getMoneyFormat($t1['maxReimbursementInclHealthCareInPercent'], 0, 1)."</strong> der erstattungsfähigen Aufwendungen nicht überschritten werden (<strong>maximale Gesamterstattung inkl. GKV-Festzuschuss</strong>).");

						$t['Kurz']['Zahnersatz']['Inlays'] = array('checkmark' => 'green', 'content' => '<strong>'.$this->getMoneyFormat($ze_inlays, 0, 1).'</strong> <span class="description">+ Festzuschuss der GKV</span>');
					}

					$showExpertText = 'true';
				}
			}
		}

		if(!isset($t['Kurz']['Zahnersatz']['Inlays']))
		{
			$t['Kurz']['Zahnersatz']['Inlays'] = array('checkmark' => 'red', 'content' => 'keine Leistung');
			$t['Kurz']['Zahnersatz']['Inlays begrenzt'] = array('checkmark' => 'red', 'content' => 'keine Leistung');
		}

		if(isset($t1['inlaysLimit']))
		{
			if($t1['inlaysLimit'] == 0)
			{
				$t['Inlays'][0]['A'][] = array('checkmark' => 'green', 'content' => "Die <strong>Anzahl erstattungsfähiger Inlays</strong> ist tariflich <strong>nicht</strong> auf eine bestimmte Anzahl begrenzt.");

				$t['Kurz']['Zahnersatz']['Inlays begrenzt'] = array('checkmark' => 'green', 'content' => 'keine tarifliche Begrenzung');
			}

			if($t1['inlaysLimit'] > 0)
			{
				$t['Inlays'][0]['A'][] = array('checkmark' => 'yellow', 'content' => "Die <strong>Anzahl erstattungsfähiger Inlays</strong> ist auf insgesamt <strong>".$this->getMoneyFormat($t1['inlaysLimit'], 1)."</strong> begrenzt.");

				$t['Kurz']['Zahnersatz']['Inlays begrenzt'] = array('checkmark' => 'yellow', 'content' => 'Begrenzungen vorhanden');
			}
		}
	
		if(isset($t1['maxInvoiceAmountPerInlay']) && $t1['maxInvoiceAmountPerInlay'] > 0)
		{
			$t['Inlays'][0]['A'][] = array('checkmark' => 'yellow', 'content' => "<strong>Je Inlay</strong> wird maximal ein Rechnungsbetrag in Höhe von <strong>".$this->getMoneyFormat($t1['maxInvoiceAmountPerInlay'], 1)."</strong> als erstattungsfähig anerkannt.");
		}

		if(isset($t1['maxAmountOfBenefitsPerInlay']) && $t1['maxAmountOfBenefitsPerInlay'] > 0)
		{
			$t['Inlays'][0]['A'][] = array('checkmark' => 'yellow', 'content' => "<strong>Je Inlay</strong> wird maximal ein Betrag in Höhe von ".$this->getMoneyFormat($t1['maxInvoiceAmountPerInlay'], 1)." erstattet.");
		}

		if(isset($t1['maxInvoiceAmountPerInlayPerYear']) && $t1['maxInvoiceAmountPerInlayPerYear'] > 0)
		{
			$t['Inlays'][0]['A'][] = array('checkmark' => 'yellow', 'content' => "<strong>Pro Versicherungsjahr</strong> wird für Inlays insgesamt maximal ein Rechnungsbetrag in Höhe von <strong>".$this->getMoneyFormat($t1['maxInvoiceAmountPerInlayPerYear'], 1)."</strong> als erstattungsfähig anerkannt.");
		}

		if(isset($t1['maxAmountOfBenefitsPerInlayPerYear']) && $t1['maxAmountOfBenefitsPerInlayPerYear'] > 0)
		{
			$t['Inlays'][0]['A'][] = array('checkmark' => 'yellow', 'content' => "<strong>Pro Versicherungsjahr</strong> wird für Inlays insgesamt maximal ein Betrag in Höhe von ".$this->getMoneyFormat($t1['maxAmountOfBenefitsPerInlayPerYear'], 1)." erstattet.");
		}


	// Leistet der Versicherer für Inlays auch ohne Vorleistung der GKV?
		$t['Inlays'][1]['Q'] = "Leistet der Versicherer für Inlays auch ohne Vorleistung der GKV?";
		
		$t['Inlays'][1]['A'];


		if(isset($ze_inlays) && isset($t1['inlaysWithoutBonusInPercent']) &&  $t1['inlaysWithoutBonusInPercent'] > 0)
		{
			if(isset($t1['inlaysDeductionWithoutHealthCareNotSelfinflicted']) && $t1['inlaysDeductionWithoutHealthCareNotSelfinflicted'] == 0)
			{
				$t['Inlays'][1]['A'][] = array('checkmark' => 'green', 'content' => "Steht dem Versicherten seitens der gesetzlichen Krankenkasse <strong>kein Zuschuss</strong> für eine <strong>medizinisch notwendige Versorgung mit Inlays</strong> zu, ohne dass der Versicherte dies selbst verschuldet hat, erhalten Sie aus Tarif <strong>".$this->convert($t1['name'])."</strong> dennoch die <strong>vollen tariflichen Leistungen</strong>!");
			}

			if(isset($t1['inlaysDeductionPercentNotSelfInflicted']))
			{
				if(isset($t1['inlaysDeductionWithoutHealthCareNotSelfinflicted']) && $t1['inlaysDeductionWithoutHealthCareNotSelfinflicted'] == 1)
				{
					if($t1['inlaysDeductionPercentNotSelfInflicted'] == 0)
					{
						$t['Inlays'][1]['A'][] = array('checkmark' => 'black', 'content' => "Steht dem Versicherten seitens der gesetzlichen Krankenkasse <strong>kein Zuschuss</strong> für eine <strong>medizinisch notwendige Versorgung</strong> mit Inlays zu, ohne dass der Versicherte dies selbst verschuldet hat, <strong>kürzt</strong> der Versicherer die tarifliche Erstattungsleistung <strong>um eine fiktive GKV-Vorleistung</strong> – die Höhe dieser fiktiven GKV-Vorleistung richtet sich danach, wie viel die gesetzliche Krankenkasse im individuellen Leistungsfall konkret erstattet hätte!");
					}

					if($t1['inlaysDeductionPercentNotSelfInflicted'] > 0 && $t1['inlaysDeductionPercentNotSelfInflicted'] < 100)
					{
						$t['Inlays'][1]['A'][] = array('checkmark' => 'black', 'content' => "Steht dem Versicherten seitens der gesetzlichen Krankenkasse <strong>kein Zuschuss</strong> für eine <strong>medizinisch notwendige Versorgung</strong> mit Inlays zu, ohne dass der Versicherte dies selbst verschuldet hat, <strong>kürzt</strong> der Versicherer die tarifliche Erstattungsleistung um <strong>".$this->getMoneyFormat($t1['inlaysDeductionPercentNotSelfInflicted'], 0, 1)."</strong>!");
					}

					if($t1['inlaysDeductionPercentNotSelfInflicted'] == 100)
					{
						$t['Inlays'][1]['A'][] = array('checkmark' => 'red', 'content' => "Steht dem Versicherten seitens der gesetzlichen Krankenkasse <strong>kein Zuschuss</strong> für eine <strong>medizinisch notwendige Versorgung mit Inlays</strong> zu, ohne dass der Versicherte dies selbst verschuldet hat, erhalten Sie keine Leistung aus Tarif <strong>".$this->convert($t1['name'])."</strong>! Die tarifliche Erstattung ist somit vollständig abhängig von einer Vorleistung der gesetzlichen Krankenversicherung.");
					}
				}
			}

		}

		if(isset($ze_inlays) && $ze_inlays == 0)
		{
			$t['Inlays'][1]['A'][] = array('checkmark' => 'red', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet <strong>nicht</strong> für Inlays.");
		}
		if(isset($t1['denturesFixedAllowanceInPercent']) && $t1['denturesFixedAllowanceInPercent'] >= 1)
		{
			$t['Inlays'][1]['A'][] = array('checkmark' => "red", 'content' => "Die Leistung des Tarifes ist direkt abhängig vom Festzuschuss ihrer gesetzlichen Krankenversicherung. Erbringt die GKV keinen Zuschuss für Zahnersatz, so erhalten Sie keine Leistung aus Tarif <strong>".$this->convert($t1['name'])."</strong>.");
		}



	// Leistet der Tarif bei einer Zahnersatzmaßnahme auch für FAL/FT?
		$t['Inlays'][2]['Q'] = "Leistet der Tarif bei einer Zahnersatzmaßnahme auch für FAL/FT?";
		
		$t['Inlays'][2]['A'];

		if(isset($t1['isFunctionalTherapyInsuredInCaseOfDentures']))
		{
			if($t1['isFunctionalTherapyInsuredInCaseOfDentures'] == 1)
			{
				$t['Inlays'][2]['A'][] = array('checkmark' => 'green', 'content' => "<strong>Ja</strong>, der Tarif leistet im Zusammenhang mit Zahnersatzbehandlungen auch für <strong>funktionsanalytische / funktionstherapeutische Maßnahmen (FAL/FT)</strong>. Die Leistungen werden gemäß Abschnitt J. Der GOZ übernommen (GOZ 8000-8100).");
			}
			elseif($t1['isFunctionalTherapyInsuredInCaseOfDentures'] == 0)
			{
				$t['Inlays'][2]['A'][] = array('checkmark' => 'red', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet <strong>nicht</strong> für <strong>funktionsanalytische / funktionstherapeutische Maßnahmen</strong> in Zusammenhang mit Zahnersatzbehandlungen.");
			}
		}

		if(isset($t1['functionalTherapyRestricted']) && $t1['functionalTherapyRestricted'] > 1)
		{
			$t['Inlays'][2]['A'][] = array('checkmark' => 'yellow', 'content' => "Leistungen werden nur im Rahmen von umfangreichen Zahnersatzbehandlungen übernommen, bei denen mindestens ".$this->getMoneyFormat($t1['functionalTherapyRestricted'])." Zähne gleichzeitig mit Zahnersatz (z.B. Implantate, Kronen, o.Ä.) versorgt werden.");
		}

		$t['Inlays'][2]['A'][] = array('checkmark' => '', 'content-expert' => "<div class='headline'>Experten-Erklärung</h5></div><div class='content'><p>Im Rahmen einer Versorgung mit <strong>Zahnersatz</strong> ist es wichtig, dass Kiefergelenk und der einzubringende Zahnersatz optimal aufeinander abgestimmt werden. Ansonsten kann es unter Umständen zu <strong>gravierenden Kiefergelenksproblemen</strong> wie z.B. Zähneknirschen, Verspannungen o.Ä. kommen. Um solche Beschwerden zu vermeiden, hat der Zahnarzt die Möglichkeit, eine sogenannte <strong>Funktionsanalyse / Funktionstherapie</strong> durchzuführen. Die Kosten einer solchen Zusatzbehandlung liegen im Bereich <strong>von ca. 600 bis 900 €</strong> und werden <strong>nicht von den gesetzlichen Krankenkassen</strong> getragen.</p></div>");


	// Was versteht der Versicherer unter Zahnbehandlung?
		$t['Zahnbehandlung & Prophylaxe'][0]['Q'] = "Was versteht der Versicherer unter Zahnbehandlung?";
		
		$t['Zahnbehandlung & Prophylaxe'][0]['A'];

		if($zb_fuellungen == 0 &&
			$zb_wurzel_ohne_gkv == 0 &&
			$zb_wurzel_mit_gkv == 0 &&
			$zb_periodent_ohne_gkv == 0 &&
			$zb_periodent_mit_gkv == 0 &&
			isset($t1['dentalCleaningInPercent']) && $t1['dentalCleaningInPercent'] == 0 &&
			isset($t1['fissuresInPercent']) && $t1['fissuresInPercent'] == 0)
		{
			$t['Zahnbehandlung & Prophylaxe'][0]['A'][] = array('checkmark' => 'red', 'content' => "Der Tarif sieht keinerlei Leistungen für <strong>Zahnbehandlung</strong> vor.");
		}
		else
		{
			$t['Zahnbehandlung & Prophylaxe'][0]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif leistet für folgende Arten von <strong>Zahnbehandlung</strong>:");

			if(isset($t1['dentalCleaningInPercent']) && $t1['dentalCleaningInPercent'] > 0)
			{
				$list[] = "professionelle Zahnreinigung (PZR)";
			}
			if(isset($t1['fissuresInPercent']) && $t1['fissuresInPercent'] > 0)
			{
				$list[] = "Fissurenversiegelung";
			}
			if((is_array($zb_fuellungen) && $zb_fuellungen[0] > 0) || $zb_fuellungen > 0)
			{
				$list[] = "hochwertige Kunststofffüllungen";
			}
			if((isset($t1['hasRootTreatmentWithoutPayment']) && $t1['hasRootTreatmentWithoutPayment'] == 1) ||
				isset($t1['hasRootTreatmentWithPayment']) && $t1['hasRootTreatmentWithPayment'] == 1)
			{
				$list[] = "Wurzelkanalbehandlung";
			}
			if((isset($t1['hasPeriodontitisTreatmentWithoutPayment']) && $t1['hasPeriodontitisTreatmentWithoutPayment'] == 1) ||
				isset($t1['hasPeriodontitisTreatmentWithPayment']) && $t1['hasPeriodontitisTreatmentWithPayment'] == 1)
			{
				$list[] = "Parodontosebehandlung";
			}

			$t['Zahnbehandlung & Prophylaxe'][0]['A'][] = array('checkmark' => '', 'content-list' => $list);
		}

		if(is_array($ze_regelversorgung))
		{	if($ze_regelversorgung[0] != $ze_regelversorgung[2])
			{
				$t['Kurz']['Zahnersatz']['Regelversorgung'] = array('checkmark' => 'green', 'content' => '<strong>'.$this->getMoneyFormat($ze_regelversorgung[0], 0, 1).'</strong> - <strong>'.$this->getMoneyFormat($ze_regelversorgung[2], 0, 1).'</strong>');
			} else
			{
				$t['Kurz']['Zahnersatz']['Regelversorgung'] = array('checkmark' => 'green', 'content' => $this->getMoneyFormat($ze_regelversorgung, 0, 1));
			}
		}
		else
		{
			$t['Kurz']['Zahnersatz']['Regelversorgung'] = array('checkmark' => 'green', 'content' => $this->getMoneyFormat($ze_regelversorgung, 0, 1));
		}



		if($ary["Zahnersatz"] == 'keine Wartezeit')
		{
			$t['Kurz']['Zahnersatz']['Wartezeit'] = array('checkmark' => 'green', 'content' => $ary["Zahnersatz"]);
		}
		else
		{
			$t['Kurz']['Zahnersatz']['Wartezeit'] = array('checkmark' => 'yellow', 'content' => $ary["Zahnersatz"]);
		}


		if(!isset($t['Kurz']['Zahnersatz']['Regelversorgung']))
			$t['Kurz']['Zahnersatz']['Regelversorgung'] =  array('checkmark' => 'black', 'content' => 'keine Leistung');
		

	// In welcher Höhe leistet der Versicherer für professionelle Zahnreinigung (PZR)?
		$t['Zahnbehandlung & Prophylaxe'][1]['Q'] = "In welcher Höhe leistet der Versicherer für professionelle Zahnreinigung (PZR)?";
		
		$t['Zahnbehandlung & Prophylaxe'][1]['A'];

		if(isset($t1['dentalCleaningInPercent']) && $t1['dentalCleaningInPercent'] > 0)
		{
			$t['Zahnbehandlung & Prophylaxe'][1]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet für <strong>professionelle Zahnreinigung</strong> (PZR) ".$this->getMoneyFormat($t1['dentalCleaningInPercent'], 0, 1).".");

			$t['Kurz']['Prophylaxe']['Professionelle Zahnreinigung'] = array('checkmark' => 'green', 'content' => "<strong>".$this->getMoneyFormat($t1['dentalCleaningInPercent'], 0, 1)."</strong>", 'limit' => 'ohne Begrenzung');
		}
		elseif(isset($t1['dentalCleaningInPercent']) && $t1['dentalCleaningInPercent'] == 0)
		{
			$t['Zahnbehandlung & Prophylaxe'][1]['A'][] = array('checkmark' => 'red', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> sieht <strong>keine Leistung</strong> für professionelle Zahnreinigung vor.");

			$t['Kurz']['Prophylaxe']['Professionelle Zahnreinigung'] = array('checkmark' => 'red', 'content' => "keine Leistung");
		}

		if(isset($t1['dentalCleaningMaxAmountPerYear']) && $t1['dentalCleaningMaxAmountPerYear'] > 0)
		{
			$t['Zahnbehandlung & Prophylaxe'][1]['A'][] = array('checkmark' => '', 'content' => "<strong>Je Versicherungsjahr</strong> erstattet der Versicherer maximal <strong>".$this->getMoneyFormat($t1['dentalCleaningMaxAmountPerYear'], 1)."</strong> für die PZR.");

			$t['Kurz']['Prophylaxe']['Professionelle Zahnreinigung']['limit'] = " max. ".$this->getMoneyFormat($t1['dentalCleaningMaxAmountPerYear'], 1)." im Jahr";
		}

		if(isset($t1['dentalCleaningMaxAmountPerTwoYears']) && $t1['dentalCleaningMaxAmountPerTwoYears'] > 0)
		{
			$t['Zahnbehandlung & Prophylaxe'][1]['A'][] = array('checkmark' => '', 'content' => "<strong>Innerhalb von zwei aufeinanderfolgenden Versicherungsjahren</strong> erstattet der Versicherer maximal <strong>".$this->getMoneyFormat($t1['dentalCleaningMaxAmountPerTwoYears'], 1)."</strong> für die PZR.");

			$t['Kurz']['Prophylaxe']['Professionelle Zahnreinigung']['limit'] = " max. ".$this->getMoneyFormat($t1['dentalCleaningMaxAmountPerTwoYears'], 1)." in zwei Jahren";
		}

		if(isset($t1['dentalCleaningMaxTreatmentsPerYear']) && $t1['dentalCleaningMaxTreatmentsPerYear'] > 0)
		{
			$t['Zahnbehandlung & Prophylaxe'][1]['A'][] = array('checkmark' => '', 'content' => "Die professionelle Zahnreinigung wird <strong>maximal ".$this->getMoneyFormat($t1['dentalCleaningMaxTreatmentsPerYear'])."</strong> mal <strong>je Versicherungsjahr</strong> erstattet.");

			$t['Kurz']['Prophylaxe']['Professionelle Zahnreinigung']['limit'] = " / max. ".$this->getMoneyFormat($t1['dentalCleaningMaxTreatmentsPerYear'])."x pro Jahr";
		}

		if(isset($t1['dentalCleaningMaxAmountPerTreatment']) && $t1['dentalCleaningMaxAmountPerTreatment'] > 0)
		{
			$t['Zahnbehandlung & Prophylaxe'][1]['A'][] = array('checkmark' => '', 'content' => "Je Zahnreinigung erstattet der Versicherer <strong>maximal ".$this->getMoneyFormat($t1['dentalCleaningMaxAmountPerTreatment'], 1)."</strong>");

			$t['Kurz']['Prophylaxe']['Professionelle Zahnreinigung']['limit'] .= " (bis ".$this->getMoneyFormat($t1['dentalCleaningMaxAmountPerTreatment'], 1)." je Behandlung)";
		}

		if(isset($t1['dentalCleaningMaxTreatmentsPerYear']) && $t1['dentalCleaningMaxTreatmentsPerYear'] == 0)
		{
			$t['Zahnbehandlung & Prophylaxe'][1]['A'][] = array('checkmark' => '', 'content' => "Die <strong>Anzahl</strong> erstattungsfähiger PZR-Behandlungen ist <strong>tariflich nicht begrenzt</strong>.");

			if(!is_array($zb_zahnreinigung) && $zb_zahnreinigung == 0)
			{
				$t['Zahnbehandlung & Prophylaxe'][1]['A'][] = array('checkmark' => '', 'content' => "Die Leistung für PZR ist in der Höhe <strong>nicht</strong> tariflich begrenzt.");
			}
		}

		if(isset($t['Kurz']['Prophylaxe']['Professionelle Zahnreinigung']['limit']))
		{
			$t['Kurz']['Prophylaxe']['Professionelle Zahnreinigung']['content'] .= ' '.$t['Kurz']['Prophylaxe']['Professionelle Zahnreinigung']['limit'];
			unset($t['Kurz']['Prophylaxe']['Professionelle Zahnreinigung']['limit']);
		}

		if(!isset($t['Kurz']['Prophylaxe']['Professionelle Zahnreinigung']))
			$t['Kurz']['Prophylaxe']['Professionelle Zahnreinigung'] = array('checkmark' => 'red', 'content' => "keine Leistung");


		// Bleaching
		if (isset($t1['bleechingInPercent']) && $t1['bleechingInPercent'] > 0)
		{
			$t['Kurz']['Prophylaxe']['Bleaching'] = array('checkmark' => 'green', 'content' => "<strong>".$this->getMoneyFormat($t1['bleechingInPercent'], 0, 1)."</strong>", 'limit' => 'ohne Begrenzung');
		} elseif (isset($t1['bleechingInPercent']) && $t1['bleechingInPercent'] == 0) {
			$t['Kurz']['Prophylaxe']['Bleaching'] = array('checkmark' => 'red', 'content' => "keine Leistung");
		} else
			$t['Kurz']['Prophylaxe']['Bleaching'] = array('checkmark' => 'red', 'content' => "keine Leistung");

		if(isset($t1['bleechingMaxAmountPerYear']) && $t1['bleechingMaxAmountPerYear'] > 0)
		{
			$t['Kurz']['Prophylaxe']['Bleaching']['limit'] = " max. ".$this->getMoneyFormat($t1['bleechingMaxAmountPerYear'], 1)." im Jahr";
		}

		if(isset($t1['bleechingMaxAmountPerTwoYears']) && $t1['bleechingMaxAmountPerTwoYears'] > 0)
		{
			$t['Kurz']['Prophylaxe']['Bleaching']['limit'] = " max. ".$this->getMoneyFormat($t1['bleechingMaxAmountPerTwoYears'], 1)." in zwei Jahren";
		}

		if(isset($t1['bleechingMaxAmountPerThreeYears']) && $t1['bleechingMaxAmountPerThreeYears'] > 0)
		{
			$t['Kurz']['Prophylaxe']['Bleaching']['limit'] = " max. ".$this->getMoneyFormat($t1['bleechingMaxAmountPerThreeYears'], 1)." in drei Jahr";
		}

		if(isset($t1['bleechingMaxAmountPerFourYears']) && $t1['bleechingMaxAmountPerFourYears'] > 0)
		{
			$t['Kurz']['Prophylaxe']['Bleaching']['limit'] = " max. ".$this->getMoneyFormat($t1['bleechingMaxAmountPerFourYears'], 1)." in vier Jahren";
		}

		if(isset($t1['bleechingMaxAmountPerFiveYears']) && $t1['bleechingMaxAmountPerFiveYears'] > 0)
		{
			$t['Kurz']['Prophylaxe']['Bleaching']['limit'] = " max. ".$this->getMoneyFormat($t1['bleechingMaxAmountPerFiveYears'], 1)." in fünf Jahren";
		}

		if(isset($t['Kurz']['Prophylaxe']['Bleaching']['limit']))
		{
			$t['Kurz']['Prophylaxe']['Bleaching']['content'] .= ' '.$t['Kurz']['Prophylaxe']['Bleaching']['limit'];
			unset($t['Kurz']['Prophylaxe']['Bleaching']['limit']);
		}



		if(isset($t1['dentalCleaningMaxTreatmentsPerYear']) && $t1['dentalCleaningMaxTreatmentsPerYear'] > 0 &&
			isset($t1['dentalCleaningMaxAmountPerTreatment']) && $t1['dentalCleaningMaxAmountPerTreatment'] > 0)
		{
			$t['Zahnbehandlung & Prophylaxe'][1]['A'][] = array('checkmark' => '', 'content' => "<strong>Insgesamt</strong> werden somit je Versicherungsjahr maximal <strong>".$this->getMoneyFormat($t1['dentalCleaningMaxAmountPerTreatment'])." x ".$this->getMoneyFormat($t1['dentalCleaningMaxTreatmentsPerYear'], 1)."</strong> für professionelle Zahnreinigung erstattet.");			
		}

		#if(isset($t1['limitForDentalCleaningAndFissures']) && $t1['limitForDentalCleaningAndFissures'] == 1)
		#{
		#	$t['Zahnbehandlung & Prophylaxe'][1]['A'][] = array('checkmark' => '', 'content' => "Diese tarifliche Begrenzung gilt sowohl für die professionelle Zahnreinigung als auch für alle anderen prophylaktischen Leistungen, die in diesem Tarif erstattet werden (z.B. Fissurenversiegelung).");
		#}

		if(isset($t1['limitForDentalCleaning']) && $t1['limitForDentalCleaning'] == 1)
		{
			$tmplim = "Diese tarifliche Begrenzung gilt für die professionelle Zahnreinigung";
		}
		if(isset($t1['limitForFissures']) && $t1['limitForFissures'] == 1)
		{
			if(!$tmplim)
				$tmplim = "Diese tarifliche Begrenzung gilt für Fissurenversiegelung";

			$tmplim .= " , Fissurenversiegelung";
		}
		if(isset($t1['limitForBleaching']) && $t1['limitForBleaching'] == 1)
		{
			if(!$tmplim)
				$tmplim = "Diese tarifliche Begrenzung gilt für Bleaching";

			$tmplim .= " und kosmetisches Bleaching";
		}

		if($tmplim)
			$t['Zahnbehandlung & Prophylaxe'][1]['A'][] = array('checkmark' => '', 'content' => $tmplim);


		if(isset($t1['hasDentalCleaningForChilds']) && $t1['hasDentalCleaningForChilds'] == 1)
		{
			$t['Zahnbehandlung & Prophylaxe'][1]['A'][] = array('checkmark' => '', 'content' => "Die professionelle Zahnreinigung wird <strong>auch für Kinder</strong> im tariflichen Rahmen erstattet.");
		}
		elseif(isset($t1['hasDentalCleaningForChilds']) && $t1['hasDentalCleaningForChilds'] == 0)
		{
			$t['Zahnbehandlung & Prophylaxe'][1]['A'][] = array('checkmark' => 'yellow', 'content' => "Für <strong>Kinder</strong> erstattet der Versicherer eine professionelle Zahnreinigung <strong>nicht</strong>!");
		}

		$t['Zahnbehandlung & Prophylaxe'][1]['A'][] = array('checkmark' => '', 'content-expert' => "<div class='headline'>Experten-Erklärung</div><div class='content'><p>Die <strong>professionelle Zahnreinigung</strong> (abgekürzt <strong>PZR</strong>) ist eine umfassende <strong>Vorsorge</strong>-Maßnahme, welche nicht im Rahmen der kassenzahnärztlichen Versorgung übernommen wird. Bei einer PZR werden <strong>nicht nur oberflächliche Beläge</strong> entfernt (wie bei einer einfachen Zahnsteinentfernung), sondern auch <strong>Beläge unterhalb des Zahnfleisches</strong>. Zusätzlich werden auch die schwer zugänglichen <strong>Zahnzwischenräume</strong> gründlich gereinigt, die Oberflächen der Zähne <strong>poliert und fluoridiert</strong>. Die PZR wird nach der <strong>GOZ-Ziffer 1040</strong> berechnet - die Gebührenhöhe richtet sich nach der Anzahl behandelter Zähne und dem Schwierigkeitsgrad im Einzelfall. Im Schnitt liegen die <strong>Kosten einer professionellen Zahnreinigung</strong> etwa zwischen <strong>70 und 110 €</strong> je Behandlung.</p></div>");



	// Sieht der Tarif Leistungen für die Fissurenversiegelung vor?
		$t['Zahnbehandlung & Prophylaxe'][2]['Q'] = "Sieht der Tarif Leistungen für die Fissurenversiegelung vor?";
		
		$t['Zahnbehandlung & Prophylaxe'][2]['A'];

		if(isset($t1['fissuresInPercent']) && $t1['fissuresInPercent'] > 0)
		{
			$t['Zahnbehandlung & Prophylaxe'][2]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet für <strong>Fissurenversiegelung ".$this->getMoneyFormat($t1['fissuresInPercent'], 0, 1)."</strong>.");

			$t['Kurz']['Prophylaxe']['Fissurenversiegelung'] = array('checkmark' => 'green', 'content' => "<strong>".$this->getMoneyFormat($t1['fissuresInPercent'], 0, 1)."</strong>");
		}
		elseif(isset($t1['fissuresInPercent']) && $t1['fissuresInPercent'] == 0)
		{
			$t['Zahnbehandlung & Prophylaxe'][2]['A'][] = array('checkmark' => 'red', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> sieht <strong>keine Leistung</strong> für <strong>Fissurenversiegelung</strong> vor.");

			$t['Kurz']['Prophylaxe']['Fissurenversiegelung'] = array('checkmark' => 'red', 'content' => "keine Leistung");
			#$t['Kurz']['Prophylaxe']['Wartezeit'] = array('checkmark' => 'red', 'content' => "keine Leistung");
		}

		if(isset($t1['fissuresMaxAmountPerYear']) && $t1['fissuresMaxAmountPerYear'] > 0)
		{
			$t['Zahnbehandlung & Prophylaxe'][2]['A'][] = array('checkmark' => '', 'content' => "<strong>Je Versicherungsjahr</strong> erstattet der Versicherer maximal <strong>".$this->getMoneyFormat($t1['fissuresMaxAmountPerYear'], 1)."</strong> für die Fissurenversiegelung.");

			$t['Kurz']['Prophylaxe']['Fissurenversiegelung']['content'] .= " max. ".$this->getMoneyFormat($t1['fissuresMaxAmountPerYear'], 1)." im Jahr";
		}

		if(isset($t1['fissuresMaxAmountPerTwoYears']) && $t1['fissuresMaxAmountPerTwoYears'] > 0)
		{
			$t['Zahnbehandlung & Prophylaxe'][2]['A'][] = array('checkmark' => '', 'content' => "<strong>Innerhalb von zwei aufeinanderfolgenden Versicherungsjahren</strong> erstattet der Versicherer maximal <strong>".$this->getMoneyFormat($t1['fissuresMaxAmountPerTwoYears'], 1)."</strong> für die Fissurenversiegelung.");

			$t['Kurz']['Prophylaxe']['Fissurenversiegelung']['content'] .= " max. ".$this->getMoneyFormat($t1['fissuresMaxAmountPerTwoYears'], 1)." in zwei Jahren";
		}

		if(isset($t1['fissuresMaxAmountPerTreatment']) && $t1['fissuresMaxAmountPerTreatment'] > 0)
		{
			$t['Zahnbehandlung & Prophylaxe'][2]['A'][] = array('checkmark' => '', 'content' => "Je Fissurenversiegelung erstattet der Versicherer maximal <strong>".$this->getMoneyFormat($t1['fissuresMaxAmountPerTreatment'], 1)."</strong>.");

			$t['Kurz']['Prophylaxe']['Fissurenversiegelung']['content'] .= " max. ".$this->getMoneyFormat($t1['fissuresMaxAmountPerTreatment'], 1)." pro Anwendung";
		}

		if(isset($t1['fissuresMaxTreatmentsPerYear']) && $t1['fissuresMaxTreatmentsPerYear'] > 0)
		{
			$t['Zahnbehandlung & Prophylaxe'][2]['A'][] = array('checkmark' => '', 'content' => "Die Fissurenversiegelung wird <strong>maximal ".$this->getMoneyFormat($t1['fissuresMaxTreatmentsPerYear'])."</strong> mal <strong>je Versicherungsjahr</strong> erstattet.");

			$t['Kurz']['Prophylaxe']['Fissurenversiegelung']['content'] .= " max. ".$this->getMoneyFormat($t1['fissuresMaxTreatmentsPerYear'], 1)." Anwendungen pro Jahr";
		}
		elseif(isset($t1['fissuresMaxTreatmentsPerYear']) && $t1['fissuresMaxTreatmentsPerYear'] == 0)
		{
			$t['Zahnbehandlung & Prophylaxe'][2]['A'][] = array('checkmark' => '', 'content' => "Die <strong>Anzahl</strong> erstattungsfähiger Behandlungen ist <strong>tariflich nicht begrenzt</strong>.");

			if(!is_array($zb_zahnreinigung) && $zb_zahnreinigung == 0)
			{
				$t['Zahnbehandlung & Prophylaxe'][2]['A'][] = array('checkmark' => '', 'content' => "Die Leistung für Fissurenversiegelung ist in der Höhe <strong>nicht</strong> tariflich begrenzt.");
			}
		}
		
		if(!isset($t['Kurz']['Prophylaxe']['Fissurenversiegelung']))
		{
			$t['Kurz']['Prophylaxe']['Fissurenversiegelung'] = array('checkmark' => 'red', 'content' => "keine Leistung");
			#$t['Kurz']['Prophylaxe']['Wartezeit'] = array('checkmark' => 'red', 'content' => "keine Leistung");

		}

		if(isset($t1['fissuresMaxTreatmentsPerYear']) && $t1['fissuresMaxTreatmentsPerYear'] > 0 &&
			isset($t1['fissuresMaxAmountPerTreatment']) && $t1['fissuresMaxAmountPerTreatment'] > 0)
		{
			$t['Zahnbehandlung & Prophylaxe'][2]['A'][] = array('checkmark' => '', 'content' => "<strong>Insgesamt</strong> werden somit je Versicherungsjahr maximal ".$this->getMoneyFormat($t1['fissuresMaxAmountPerTreatment'])." x ".$this->getMoneyFormat($t1['fissuresMaxTreatmentsPerYear'], 1)." für die Versiegelung der Zähne erstattet.");			
		}

		if(isset($t1['limitForDentalCleaningAndFissures']) && $t1['limitForDentalCleaningAndFissures'] == 1)
		{
			$t['Zahnbehandlung & Prophylaxe'][2]['A'][] = array('checkmark' => '', 'content' => "Diese tarifliche Begrenzung gilt sowohl für die Fissurenversiegelung als auch für alle anderen prophylaktischen Leistungen, die in diesem Tarif erstattet werden (z.B. professionelle Zahnreinigung).");
		}

		if(isset($t1['fissuresOnMilkTeeth']) && $t1['fissuresOnMilkTeeth'] == 1)
		{
			$t['Zahnbehandlung & Prophylaxe'][2]['A'][] = array('checkmark' => '', 'content' => "Bei Kindern leistet der Versicherer auch für die Versiegelung kariesfreier Milchzähne.");
		}
		elseif(isset($t1['fissuresOnMilkTeeth']) && $t1['fissuresOnMilkTeeth'] == 0)
		{
			$t['Zahnbehandlung & Prophylaxe'][2]['A'][] = array('checkmark' => '', 'content' => "Die Fissurenversiegelung wird bei Kindern nur an bleibenden Zähnen erstattet (nicht an Milchzähnen)!");
		}

		$t['Zahnbehandlung & Prophylaxe'][2]['A'][] = array('checkmark' => '', 'content-expert' => "<div class='headline'>Experten-Erklärung</div><div class='content'><p>Die <strong>Kautäler</strong> zwischen den Höckern unserer <strong>Backenzähne</strong> bezeichnet man als <strong>\"Fissuren\"</strong>. Diese lassen sich mit einer <strong>dünnen Schicht Kunststoff versiegeln</strong>, so dass sich an diesen für Zahnbürsten schwer erreichbaren Stellen <strong>nicht so leicht Karies bilden</strong> kann. Die Fissurenversiegelung wird häufig bei <strong>Kindern und Jugendlichen</strong> empfohlen. Die <strong>gesetzlichen Kassen</strong> erstatten bei Kindern von 6 bis 18 Jahren die Fissurenversiegelung nur an den beiden hinteren bleibenden Backenzähnen (6er und 7er) - eine Versiegelung der davor liegenden kleinen Backenzähnen (4er und 5er) ist <strong>Privatleistung</strong> und wird nicht von der GKV getragen, genauso wie die Versiegelung von <strong>Milchzähnen</strong>. Die Kosten einer Fissurenversiegelung berechnen sich nach der <strong>GOZ Ziffer 2000</strong> und liegen je Zahn bei bis zu 17,72€ (3,5facher Satz GOZ 2000) - allein die Versiegelung der 4 kleinen Backenzähne kann somit insgesamt <strong>über 70€ kosten</strong>!</p></div>");

		if(!$t['Kurz']['Prophylaxe']['Wartezeit'])
		{
			if($ary["Prophylaxe"] == 'keine Wartezeit')
			{
				$t['Kurz']['Prophylaxe']['Wartezeit'] = array('checkmark' => 'green', 'content' => $ary["Prophylaxe"]);
			}
			elseif(!isset($ary["Prophylaxe"]))
			{
				$t['Kurz']['Prophylaxe']['Wartezeit'] = array('checkmark' => 'red', 'content' => 'keine Leistungen');
			}
			else
			{
				$t['Kurz']['Prophylaxe']['Wartezeit'] = array('checkmark' => 'yellow', 'content' => $ary["Prophylaxe"]);
			}

		}


	// In welcher Höhe leistet der Tarif für hochwertige Kunststofffüllungen?
		$t['Zahnbehandlung & Prophylaxe'][3]['Q'] = "In welcher Höhe leistet der Tarif für hochwertige Kunststofffüllungen?";
		
		$t['Zahnbehandlung & Prophylaxe'][3]['A'];

		if(is_array($zb_fuellungen) && $zb_fuellungen[0] > 0)
		{
			if(isset($t1['hasHealthCareIncludedInBenefitsPercentDentalTreatment']))
			{
			if($t1['hasHealthCareIncludedInBenefitsPercentDentalTreatment'] == 2)
			{
				$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für hochwertige Kunststofffüllungen:");
				$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => '', 'content-list' => array('ohne Bonusheft' => $this->getMoneyFormat($t1['syntheticFillingsWithoutBonusInPercent'], 0, 1)." nach. Vorleistung der GKV", '5 Jahre Bonusheft' => $this->getMoneyFormat($t1['syntheticFillingsWith5YearsBonusInPercent'], 0, 1)." nach. Vorleistung der GKV", '10 Jahre Bonusheft' => $this->getMoneyFormat($t1['syntheticFillingsWith10YearsBonusInPercent'], 0, 1)." nach. Vorleistung der GKV"));
				$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => '', 'content' => "„nach GKV“ bedeutet, dass zuerst eine etwaige Vorleistung der gesetzlichen Krankenkasse abgezogen wird – der Prozentsatz bezieht sich auf den verbleibenden Eigenanteil.");				

				$t['Kurz']['Zahnbehandlung']['Hochwertige Füllungen'] = array('checkmark' => 'green', 'content' => "<strong>".$this->getMoneyFormat($t1['syntheticFillingsWithoutBonusInPercent'])." - ".$this->getMoneyFormat($t1['syntheticFillingsWith10YearsBonusInPercent'], 0, 1)."</strong> <span class='description'>nach. GKV</span>");
			}

			if($t1['hasHealthCareIncludedInBenefitsPercentDentalTreatment'] == 1)
			{
				$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für hochwertige Kunststofffüllungen:");
				$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => '', 'content-list' => array('ohne Bonusheft' => $this->getMoneyFormat($t1['syntheticFillingsWithoutBonusInPercent'], 0, 1)." inkl. Vorleistung der GKV", '5 Jahre Bonusheft' => $this->getMoneyFormat($t1['syntheticFillingsWith5YearsBonusInPercent'], 0, 1)." inkl. Vorleistung der GKV", '10 Jahre Bonusheft' => $this->getMoneyFormat($t1['syntheticFillingsWith10YearsBonusInPercent'], 0, 1)." inkl. Vorleistung der GKV"));
				$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => '', 'content' => "„Inkl. GKV“ bedeutet, dass in den genannten Prozentsätzen bereits etwaige Leistungen Ihrer gesetzlichen Krankenversicherung berücksichtigt sind.");				

				$t['Kurz']['Zahnbehandlung']['Hochwertige Füllungen'] = array('checkmark' => 'green', 'content' => "<strong>".$this->getMoneyFormat($t1['syntheticFillingsWithoutBonusInPercent'])." - ".$this->getMoneyFormat($t1['syntheticFillingsWith10YearsBonusInPercent'], 0, 1)."</strong> <span class='description'>inkl. GKV</span>");
			}
			elseif($t1['hasHealthCareIncludedInBenefitsPercentDentalTreatment'] == 0)
			{
				$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für hochwertige Kunststofffüllungen:");
				$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => '', 'content-list' => array('ohne Bonusheft' => $this->getMoneyFormat($t1['syntheticFillingsWithoutBonusInPercent'], 0, 1)." + Zuschuss GKV", '5 Jahre Bonusheft' => $this->getMoneyFormat($t1['syntheticFillingsWith5YearsBonusInPercent'], 0, 1)." + Zuschuss GKV", '10 Jahre Bonusheft' => $this->getMoneyFormat($t1['syntheticFillingsWith10YearsBonusInPercent'], 0, 1)." + Zuschuss GKV"));
				$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => '', 'content' => "Der Versicherer erstattet den genannten Prozentsatz vom (erstattungsfähigen) Rechnungsbetrag. Mit der gesetzlichen Krankenkasse kann Ihr Zahnarzt zusätzlich einen Zuschuss in Höhe von ca. 30-60€ abrechnen (BEMA-Ziffer 13a-d). Dieser Zuschuss kommt zur Leistung Ihrer Zusatzversicherung hinzu und erhöht somit Ihre Gesamtleistung (maximal 100%).");				

				$t['Kurz']['Zahnbehandlung']['Hochwertige Füllungen'] = array('checkmark' => 'green', 'content' => "<strong>".$this->getMoneyFormat($t1['syntheticFillingsWithoutBonusInPercent'])." - ".$this->getMoneyFormat($t1['syntheticFillingsWith10YearsBonusInPercent'], 0, 1)."</strong> <span class='description'>+ Zuschuss GKV</span>");
			}
			}
		}
		elseif(!is_array($zb_fuellungen))
		{
			if($zb_fuellungen > 0)
			{
				if(isset($t1['hasHealthCareIncludedInBenefitsPercentDentalTreatment']))
				{
					if($t1['hasHealthCareIncludedInBenefitsPercentDentalTreatment'] == 2)
					{
						$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für hochwertige Kunststofffüllungen:");
						$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => '', 'content-list' => array('unabhängig vom Bonusheft' => $this->getMoneyFormat($t1['syntheticFillingsWithoutBonusInPercent'], 0, 1)." nach. Vorleistung der GKV"));
						$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => '', 'content' => "„nach GKV“ bedeutet, dass zuerst eine etwaige Vorleistung der gesetzlichen Krankenkasse abgezogen wird – der Prozentsatz bezieht sich auf den verbleibenden Eigenanteil.");				
	
						$t['Kurz']['Zahnbehandlung']['Hochwertige Füllungen'] = array('checkmark' => 'green', 'content' => $this->getMoneyFormat($t1['syntheticFillingsWithoutBonusInPercent'], 0, 1)." <span class='description'>nach. GKV</span>");
					}

					if($t1['hasHealthCareIncludedInBenefitsPercentDentalTreatment'] == 1)
					{
						$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für hochwertige Kunststofffüllungen:");
						$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => '', 'content-list' => array('unabhängig vom Bonusheft' => $this->getMoneyFormat($t1['syntheticFillingsWithoutBonusInPercent'], 0, 1)." inkl. Vorleistung der GKV"));
						$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => '', 'content' => "„Inkl. GKV“ bedeutet, dass in den genannten Prozentsätzen bereits etwaige Leistungen Ihrer gesetzlichen Krankenversicherung berücksichtigt sind.");				

						$t['Kurz']['Zahnbehandlung']['Hochwertige Füllungen'] = array('checkmark' => 'green', 'content' => $this->getMoneyFormat($t1['syntheticFillingsWithoutBonusInPercent'], 0, 1)." <span class='description'>inkl. GKV</span>");
					}
					elseif($t1['hasHealthCareIncludedInBenefitsPercentDentalTreatment'] == 0)
					{
						$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => 'green', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet in folgender Höhe für hochwertige Kunststofffüllungen:");
						$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => '', 'content-list' => array('unabhängig vom Bonusheft' => $this->getMoneyFormat($t1['syntheticFillingsWithoutBonusInPercent'], 0, 1)." + Zuschuss GKV"));
						$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => '', 'content' => "Der Versicherer erstattet den genannten Prozentsatz vom (erstattungsfähigen) Rechnungsbetrag. Mit der gesetzlichen Krankenkasse kann Ihr Zahnarzt zusätzlich einen Zuschuss in Höhe von ca. 30-60€ abrechnen (BEMA-Ziffer 13a-d). Dieser Zuschuss kommt zur Leistung Ihrer Zusatzversicherung hinzu und erhöht somit Ihre Gesamtleistung (maximal 100%).");				

						$t['Kurz']['Zahnbehandlung']['Hochwertige Füllungen'] = array('checkmark' => 'green', 'content' => $this->getMoneyFormat($t1['syntheticFillingsWithoutBonusInPercent'], 0, 1)." <span class='description'>+ Zuschuss GKV</span>");
					}
				}
			}
			if($zb_fuellungen == 0)
			{
				$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => 'red', 'content' => "Der Tarif sieht <strong>keine Leistungen für Kunststofffüllungen vor!</strong>");
			}
		}

		if(!isset($t['Kurz']['Zahnbehandlung']['Hochwertige Füllungen']))
			$t['Kurz']['Zahnbehandlung']['Hochwertige Füllungen'] =  array('checkmark' => 'red', 'content' => 'keine Leistung');

		
		if(isset($t1['syntheticFillingsDoubledSubsidy']) && $t1['syntheticFillingsDoubledSubsidy'] == 1)
		{
			$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => 'black', 'content' => "Der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet für <strong>Kunststofffüllungen</strong> in <strong>derselben Höhe</strong>, in der sich auch Ihre <strong>gesetzliche Krankenkasse</strong> (GKV) an der Behandlung beteiligt.</p><p>Mit der <strong>GKV</strong> kann Ihr Zahnarzt für Füllungen einen Zuschuss in Höhe von ca. <strong>30-60€</strong> abrechnen (BEMA-Ziffer 13a-d – Stand 08-2015).</p><p>Von Ihrer <strong>Zusatzversicherung</strong> erhalten Sie einen Zuschuss in <strong>genau derselben Höhe</strong>. Erhalten Sie von Ihrer gesetzlichen Krankenkasse – aus welchen Gründen auch immer – keinen Zuschuss für eine Füllung, so erhalten Sie auch <strong>aus diesem Tarif keine Leistung!</strong>");
		}

		if(isset($t1['syntheticFillingsDeductionWithoutHealthCareNotSelfinflicted']))
		{
			if($t1['syntheticFillingsDeductionWithoutHealthCareNotSelfinflicted'] == 0)
			{
				$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => '', 'content' => "Die Leistung erfolgt <strong>unabhängig von einer Vorleistung</strong> Ihrer gesetzlichen Kasse, d.h. Auch wenn sich die GKV nicht an den Kosten einer Zahnfüllung beteiligt, erhalten Sie die <strong>volle tarifliche Leistung</strong>.");
			}

			elseif($t1['syntheticFillingsDeductionWithoutHealthCareNotSelfinflicted'] == 1)
			{
				if(isset($t1['syntheticFillingsDeductionInPercentNotSelfInflicted']) && $t1['syntheticFillingsDeductionInPercentNotSelfInflicted'] > 0 && $t1['syntheticFillingsDeductionInPercentNotSelfInflicted'] < 100)
				{
					$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => 'yellow', 'content' => "Beteiligt sich die <strong>gesetzliche Krankenkasse nicht</strong> an den Kosten einer Füllungstherapie, erfolgt eine Leistung <strong>nicht</strong> in voller Höhe.<p>Der Versicherer nimmt in diesem Fall einen <strong>Abzug</strong> in Höhe von <strong>".$this->getMoneyFormat($t1['syntheticFillingsDeductionWithoutHealthCareNotSelfinflicted'], 0, 1)."</strong> der tariflichen Leistungen vor.</p>");
				}
				if(isset($t1['syntheticFillingsDeductionInPercentNotSelfInflicted']) && $t1['syntheticFillingsDeductionInPercentNotSelfInflicted'] == 100)
				{
					$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => 'yellow', 'content' => "<p>Die Leistung für Füllungen ist <strong>abhängig von einer Vorleistung</strong> Ihrer gesetzlichen Krankenkasse. Der Versicherer leistet <strong>nur dann</strong> in tariflicher Höhe für Kunststofffüllungen, sofern sich auch Ihre gesetzliche Krankenkasse an den Kosten der Füllungstherapie beteiligt.</p><p>Erhalten Sie von Ihrer <strong>gesetzlichen Krankenkasse</strong> – aus welchen Gründen auch immer – keinen Zuschuss für eine Füllung, so erhalten Sie <strong>auch aus diesem Tarif keine Leistung</strong>!</p>");
				}
			}
		}

		$t['Zahnbehandlung & Prophylaxe'][3]['A'][] = array('checkmark' => '', 'content-expert' => "<div class='headline'>Experten-Erklärung</div><div class='content'><p><strong>Karies</strong> ist eine weit verbreitete Volkskrankheit. Die Zahnmedizin hat sich in Sachen <strong>Füllungstherapie</strong> stetig weiterentwickelt. Arbeitete man früher häufig mit Amalgam oder einfachen Kunststofffüllungen, so versorgen <strong>die meisten Zahnärzte</strong> Ihre Patienten heutzutage mit <strong>hochwertigen Kunststofffüllungen in Mehrschichttechnik</strong>. Dabei werden nach Entfernung der Karies <strong>mehrere dünne Schichten Kunststoff</strong> nacheinander in den präparierten Zahn eingebracht und <strong>einzeln ausgehärtet</strong>. Dadurch entsteht eine sehr langlebige Füllung, die in vielen Fällen mitunter länger als 10 Jahre halten kann. Die <strong>Berechnung</strong> derartiger Kunststofffüllungen erfolgt nach <strong>GOZ (Ziffern 2060 / 2080 / 2100 / 2120)</strong>. Eine solche Füllung <strong>kostet</strong> je nach Größe und Schwierigkeit etwa <strong>zwischen 70 und 150 €</strong>.</p></div>");


	// Narkose (nur Short)
		if(isset($t1['anesthesia']) && $t1['anesthesia'] > 0)
		{
			$t['Kurz']['Zahnbehandlung']['Narkose'] = array('checkmark' => 'green', 'content' => "<strong>".$this->getMoneyFormat($t1['anesthesia'], 0, 1)."</strong>", 'limit' => 'ohne Begrenzung');
		}
		elseif(isset($t1['anesthesia']) && $t1['anesthesia'] == 0)
		{
			$t['Kurz']['Zahnbehandlung']['Narkose'] = array('checkmark' => 'red', 'content' => "keine Leistung");
		}

		if(isset($t1['anesthesiaAmountLimitationPerYear']) && $t1['anesthesiaAmountLimitationPerYear'] > 0)
		{
			$t['Kurz']['Zahnbehandlung']['Narkose']['limit'] = " max. ".$this->getMoneyFormat($t1['anesthesiaAmountLimitationPerYear'], 1)." im Jahr";
		}

		if(isset($t1['anesthesiaAmountLimitationPerTwoYears']) && $t1['anesthesiaAmountLimitationPerTwoYears'] > 0)
		{
			$t['Kurz']['Zahnbehandlung']['Narkose']['limit'] = " max. ".$this->getMoneyFormat($t1['anesthesiaAmountLimitationPerTwoYears'], 1)." in zwei Jahren";
		}

		if(isset($t['Kurz']['Zahnbehandlung']['Narkose']['limit']))
		{
			$t['Kurz']['Zahnbehandlung']['Narkose']['content'] .= ' '.$t['Kurz']['Zahnbehandlung']['Narkose']['limit'];
			unset($t['Kurz']['Zahnbehandlung']['Narkose']['limit']);
		}

		if(!isset($t['Kurz']['Zahnbehandlung']['Narkose']))
			$t['Kurz']['Zahnbehandlung']['Narkose'] = array('checkmark' => 'red', 'content' => "keine Leistung");
		


	// Leistet der Tarif für eine Wurzelkanalbehandlung?
		$t['Zahnbehandlung & Prophylaxe'][4]['Q'] = "Leistet der Tarif für eine Wurzelkanalbehandlung?";
		
		$t['Zahnbehandlung & Prophylaxe'][4]['A'];


		if((isset($t1['hasRootTreatmentWithoutPayment']) && $t1['hasRootTreatmentWithoutPayment'] == 1) ||
			(isset($t1['hasRootTreatmentWithPayment']) && $t1['hasRootTreatmentWithPayment'] == 1))
		{
			$t['Zahnbehandlung & Prophylaxe'][4]['A'][] = array('checkmark' => 'green', 'content' => "<strong>Ja</strong>, der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet auch für <strong>Wurzelkanalbehandlungen</strong>");
		}
		if((isset($t1['hasRootTreatmentWithoutPayment']) && $t1['hasRootTreatmentWithoutPayment'] == 0) &&
			(isset($t1['hasRootTreatmentWithPayment']) && $t1['hasRootTreatmentWithPayment'] == 0))
		{
			$t['Zahnbehandlung & Prophylaxe'][4]['A'][] = array('checkmark' => 'red', 'content' => "<strong>Nein</strong>, der Tarif sieht <strong>keine Leistungen für Wurzelkanalbehandlungen</strong> vor.");
		}



		if(isset($t1['hasRootTreatmentWithoutPayment']) && $t1['hasRootTreatmentWithoutPayment'] == 1)
		{
			if(is_array($zb_wurzel_ohne_gkv))
			{
				$t['Zahnbehandlung & Prophylaxe'][4]['A'][] = array('checkmark' => 'green', 'content' => "<strong>Wurzelbehandlung ohne Vorleistung der GKV:</strong><p>Fällt die Wurzelkanalbehandlung nicht in den Leistungsumfang der gesetzlichen Krankenversicherung (Fall 1), so sieht der Tarif <strong>".$this->convert($t1['name'])."</strong> dafür <strong>keine Leistung</strong> vor!</p><p>Die Kosten einer reinen Privatwurzelbehandlung können etwa zwischen <strong>800 und 1100€</strong> betragen!");

				$t['Kurz']['Zahnbehandlung']['Wurzelbehandlung ohne GKV-Vorleistung'] = array('checkmark' => 'green', 'content' => "<strong>".$this->getMoneyFormat($t1['rootTreatmentWithoutBonusInPercentWithoutGKVPayment'], 0, 1)." - ".$this->getMoneyFormat($t1['rootTreatmentWith10YearsBonusInPercentWithoutGKVPayment'], 0, 1)."</strong>");
			}
			else
			{
				$t['Zahnbehandlung & Prophylaxe'][4]['A'][] = array('checkmark' => 'green', 'content' => "<strong>Wurzelbehandlung ohne Vorleistung der GKV:</strong><p>Fällt die Wurzelkanalbehandlung nicht in den Leistungsumfang der gesetzlichen Krankenversicherung (Fall 1), so leistet der Tarif <strong>".$this->convert($t1['name'])." ".$this->getMoneyFormat($t1['rootTreatmentWithoutBonusInPercentWithoutGKVPayment'], 0, 1)."</strong> für eine vollständig privat berechnete Wurzelkanalbehandlung.</p><p>Die Kosten einer <strong>reinen Privatwurzelbehandlung</strong> können etwa zwischen <strong>800 und 1100€</strong> betragen!");

				$t['Kurz']['Zahnbehandlung']['Wurzelbehandlung ohne GKV-Vorleistung'] = array('checkmark' => 'green', 'content' => $this->getMoneyFormat($t1['rootTreatmentWithoutBonusInPercentWithoutGKVPayment'], 0, 1));
			}

			$t['Zahnbehandlung & Prophylaxe'][4]['A'][] = array('checkmark' => '', 'content' => "Im Rahmen einer <strong>reinen Privatbehandlung</strong> (ohne Vorleistung der GKV) leistet der Tarif für eine Behandlung mit hochmoderner Technik, beispielsweise die <strong>Anwendung eines Laser-Gerätes</strong> oder die Arbeit mit einem <strong>hochauflösenden Operationsmikroskops</strong>. Für diese Leistungen akzeptiert der Versicherer die Berechnung der gemäß GOZ vorgesehenen Zuschlagspositionen (Zuschlag Laser GOZ 0120, Zuschlag OP-Mikroskop GOZ 0110).");
		}
		else
		{
			$t['Zahnbehandlung & Prophylaxe'][4]['A'][] = array('checkmark' => 'yellow', 'content' => "<strong>Wurzelbehandlung ohne Vorleistung der GKV:</strong><p>Fällt die Wurzelkanalbehandlung nicht in den Leistungsumfang der gesetzlichen Krankenversicherung (Fall 1), so sieht der Tarif <strong>".$this->convert($t1['name'])."</strong> dafür keine Leistung vor!</p><p>Die Kosten einer reinen Privatwurzelbehandlung können etwa zwischen 800 und 1100€ betragen!");
		}

		
		if(isset($t1['hasRootTreatmentWithPayment']) && $t1['hasRootTreatmentWithPayment'] == 1)
		{
			if(is_array($zb_wurzel_mit_gkv))
			{
				$t['Zahnbehandlung & Prophylaxe'][4]['A'][] = array('checkmark' => 'green', 'content' => "<strong>Wurzelbehandlung mit Vorleistung der GKV:</strong><p>Fällt die Wurzelkanalbehandlung in den <strong>Leistungsumfang der GKV (Fall 2)</strong>, z.B. weil durch die Behandlung eine geschlossene Zahnreihe erhalten werden kann, leistet der Tarif <strong>".$this->convert($t1['name'])." ".$this->getMoneyFormat($t1['rootTreatmentWithoutBonusInPercentWithGKVPayment'], 1)." bis ".$this->getMoneyFormat($t1['rootTreatmentWith10YearsBonusInPercentWithGKVPayment'], 1)."</strong> (abhängig vom Bonusheft) für <strong>privat berechnete Mehrkosten</strong>:</p>");
				$t['Zahnbehandlung & Prophylaxe'][4]['A'][] = array('checkmark' => '', 'content-list' => array('ohne Bonusheft' => "<strong>".$this->getMoneyFormat($t1['rootTreatmentWithoutBonusInPercentWithGKVPayment'], 1)."</strong>", '5 Jahre Bonusheft' => "<strong>".$this->getMoneyFormat($t1['rootTreatmentWith5YearsBonusInPercentWithGKVPayment'], 1)."</strong>", '10 Jahre Bonusheft' => "<strong>".$this->getMoneyFormat($t1['rootTreatmentWith10YearsBonusInPercentWithGKVPayment'], 1)."</strong>"));
				$t['Zahnbehandlung & Prophylaxe'][4]['A'][] = array('checkmark' => '', 'content' => "Voraussetzung für die Berechnung der Mehrkosten ist, dass es sich um eigenständige Leistungen der GOZ handelt, die mit der Kassenleistung nicht abgegolten sind. Reine Verlangensleistungen gemäß §2.3 der GOZ werden vom Versicherer nicht erstattet.<p>.<strong>Beispiele</strong> für <strong>erstattungsfähige Mehrkosten</strong> sind:</p>");
				$t['Zahnbehandlung & Prophylaxe'][4]['A'][] = array('checkmark' => '', 'content-list' => array("elektrometische Längenbestimmung eines Wurzelkanals (GOZ 2400)", "Anwendung elektrophysikalisch-chemischer Methoden (GOZ 2420)", "zusätzliche medikamentöse Einlagen (GOZ2430)"));

				$t['Kurz']['Zahnbehandlung']['Wurzelbehandlung mit GKV-Vorleistung'] = array('checkmark' => 'green', 'content' => "<strong>".$this->getMoneyFormat($t1['rootTreatmentWithoutBonusInPercentWithGKVPayment'])." - ".$this->getMoneyFormat($t1['rootTreatmentWith10YearsBonusInPercentWithGKVPayment'], 0, 1)."</strong> nach GKV");
			}
			else
			{
				$t['Zahnbehandlung & Prophylaxe'][4]['A'][] = array('checkmark' => 'green', 'content' => "<strong>Wurzelbehandlung mit Vorleistung der GKV:</strong><p>Fällt die Wurzelkanalbehandlung in den <strong>Leistungsumfang der GKV (Fall 2)</strong>, z.B. weil durch die Behandlung eine geschlossene Zahnreihe erhalten werden kann, leistet der Tarif <strong>".$this->convert($t1['name'])." ".$this->getMoneyFormat($t1['rootTreatmentWithoutBonusInPercentWithGKVPayment'], 0, 1)." </strong> für <strong>privat berechnete Mehrkosten.</strong></p><p>Voraussetzung für die Berechnung der Mehrkosten ist, dass es sich um eigenständige Leistungen der GOZ handelt, die mit der Kassenleistung nicht abgegolten sind. Reine Verlangensleistungen gemäß §2.3 der GOZ werden vom Versicherer nicht erstattet.</p><p><strong>Beispiele</strong> für <strong>erstattungsfähige Mehrkosten</strong> sind:");
				$t['Zahnbehandlung & Prophylaxe'][4]['A'][] = array('checkmark' => '', 'content-list' => array("elektrometische Längenbestimmung eines Wurzelkanals (GOZ 2400)", "Anwendung elektrophysikalisch-chemischer Methoden (GOZ 2420)", "zusätzliche medikamentöse Einlagen (GOZ2430)"));

				$t['Kurz']['Zahnbehandlung']['Wurzelbehandlung mit GKV-Vorleistung'] = array('checkmark' => 'green', 'content' => $this->getMoneyFormat($t1['rootTreatmentWithoutBonusInPercentWithGKVPayment'], 0, 1).' nach GKV');
			}

			$t['Zahnbehandlung & Prophylaxe'][4]['A'][] = array('checkmark' => '', 'content' => "Im Rahmen einer <strong>reinen Privatbehandlung</strong> (ohne Vorleistung der GKV) leistet der Tarif für eine Behandlung mit hochmoderner Technik, beispielsweise die <strong>Anwendung eines Laser-Gerätes</strong> oder die Arbeit mit einem <strong>hochauflösenden Operationsmikroskops</strong>. Für diese Leistungen akzeptiert der Versicherer die Berechnung der gemäß GOZ vorgesehenen Zuschlagspositionen (Zuschlag Laser GOZ 0120, Zuschlag OP-Mikroskop GOZ 0110).");
		}
		else
		{
			$t['Zahnbehandlung & Prophylaxe'][4]['A'][] = array('checkmark' => 'yellow', 'content' => "<strong>Wurzelbehandlung mit Vorleistung der GKV:</strong><p>Fällt die Wurzelkanalbehandlung in den <strong>Leistungsumfang der GKV (Fall2)</strong>, z.B. weil durch die Behandlung eine geschlossene Zahnreihe erhalten werden kann, leistet der Tarif <strong>".$this->convert($t1['name'])." nicht</strong> für <strong>privat berechnete Mehrkosten</strong>, die für eine umfassendere Behandlung entstehen (z.B. elektrometische Längenbestimmung, Anwendung elektrophysikalisch-chemischer Methoden o.ä.)!</p>");
		}

		if(isset($t1['rootTreatmentGKVIncludedInPercent']) && $t1['rootTreatmentGKVIncludedInPercent'] == 1)
		{
			$t['Zahnbehandlung & Prophylaxe'][4]['A'][] = array('checkmark' => 'yellow', 'content' => "Der angegebene Prozentsatz beinhaltet bereits die Vorleistungen Ihrer gesetzlichen Krankenversicherung, d.h. die Leistungen der GKV werden angerechnet.");
		}

		$t['Zahnbehandlung & Prophylaxe'][4]['A'][] = array('checkmark' => '', 'content-expert' => "<div class='headline'>Experten-Erklärung</div><div class='content'><p>Die <strong>gesetzlichen Kassen</strong> übernehmen die Kosten einer Wurzelbehandlung im <strong>Backenzahnbereich</strong> nur dann, wenn dadurch</p><p><ul><li>eine geschlossene Zahnreihe erhalten bleibt</li><li>eine einseitige Freiendsituation vermieden wird</li><li>vorhandener intakter Zahnersatz erhalten wird</li></ul></p><p>Sind diese <strong>Kriterien nicht erfüllt</strong>, leistet Ihre gesetzliche Krankenkasse <strong>nicht</strong> für die Erhaltung eines Zahnes mittels einer Wurzelkanalbehandlung. Die <strong>Kassenleistung</strong> besteht dann darin, den an der Zahnwurzel erkrankten <strong>Zahn zu entfernen</strong> und im Rahmen der <strong>gesetzlichen Regelversorgung zu ersetzen</strong>.</p><p>Entscheiden Sie sich dennoch für den <strong>Erhalt Ihres Zahnes</strong>, ist die Wurzelbehandlung <strong>vollständig Privatleistung</strong>. Die <strong>Kosten</strong> einer solchen Privatbehandlung können je nach Aufwand und Verlauf in etwa zwischen <strong>800 und 1100€</strong> betragen (<strong>Fall 1: Wurzelbehandlung ohne GKV-Vorleistung</strong>).</p><p>Ist eines der oben genannten <strong>Kriterien erfüllt</strong>, übernimmt die GKV im Rahmen des <strong>Sachleistungsprinzips</strong> die Kosten der Wurzelbehandlung vollständig – allerdings nur im Rahmen einer einfachen <strong>Standardbehandlung</strong>, welche den Richtlinien des 5. Sozialgesetzbuches (SGB V) entspricht. Demnach muss die Behandlung der GKV</p><ul><li>ausreichend</li><li>wirtschaftlich</li><li>zweckmäßig</li></ul><p>sein.</p><p>Entscheiden Sie sich in Absprache mit Ihrem Zahnarzt für eine darüber hinausgehende <strong>hochwertigere Versorgung</strong> mit moderner zahnmedizinischer Technik, können ungeachtet der Kassenleistung darüber hinausgehende privat zu tragende <strong>Mehrkosten</strong> in Höhe von mehreren hundert € entstehen (<strong>Fall 2: Wurzelbehandlung mit GKV-Vorleistung</strong>), beispielsweise für das <strong>exakte Ausmessen der Wurzelkanäle</strong> mittels modernster Technik (GOZ 2400 – elektrometische Längenbestimmung).</p></div>");

		if(!isset($t['Kurz']['Zahnbehandlung']['Wurzelbehandlung mit GKV-Vorleistung']))
			$t['Kurz']['Zahnbehandlung']['Wurzelbehandlung mit GKV-Vorleistung'] = array('checkmark' => 'black', 'content' => "keine Leistung");
		if(!isset($t['Kurz']['Zahnbehandlung']['Wurzelbehandlung ohne GKV-Vorleistung']))
			$t['Kurz']['Zahnbehandlung']['Wurzelbehandlung ohne GKV-Vorleistung'] = array('checkmark' => 'black', 'content' => "keine Leistung");

	// Leistet der Tarif für eine Parodontosebehandlung?
		$t['Zahnbehandlung & Prophylaxe'][5]['Q'] = "Leistet der Tarif für eine Parodontosebehandlung?";
		
		$t['Zahnbehandlung & Prophylaxe'][5]['A'];

		if((isset($t1['hasPeriodontitisTreatmentWithoutPayment']) && $t1['hasPeriodontitisTreatmentWithoutPayment'] == 1) ||
			(isset($t1['hasPeriodontitisTreatmentWithPayment']) && $t1['hasPeriodontitisTreatmentWithPayment'] == 1))
		{
			$t['Zahnbehandlung & Prophylaxe'][5]['A'][] = array('checkmark' => 'green', 'content' => "<strong>Ja</strong>, der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet auch für <strong>Parodontosebehandlungen</strong>:");
		}
		if((isset($t1['hasPeriodontitisTreatmentWithoutPayment']) && $t1['hasPeriodontitisTreatmentWithoutPayment'] == 0) &&
			(isset($t1['hasPeriodontitisTreatmentWithPayment']) && $t1['hasPeriodontitisTreatmentWithPayment'] == 0))
		{
			$t['Zahnbehandlung & Prophylaxe'][5]['A'][] = array('checkmark' => 'red', 'content' => "<strong>Nein</strong>, der Tarif sieht <strong>keine Leistungen für Parodontosebehandlungen</strong> vor.");
		}

		

		if(isset($t1['hasPeriodontitisTreatmentWithoutPayment']) && $t1['hasPeriodontitisTreatmentWithoutPayment'] == 1)
		{
			if(is_array($zb_periodent_ohne_gkv))
			{
				$t['Zahnbehandlung & Prophylaxe'][5]['A'][] = array('checkmark' => 'green', 'content' => "<strong>Parodontosebehandlung ohne Vorleistung der GKV:</strong><p>Fällt die Parodontosebehandlung <strong>nicht in den Leistungsumfang der gesetzlichen Krankenversicherung (Fall 1)</strong>, so leistet der Tarif <strong>".$this->convert($t1['name'])." ".$this->getMoneyFormat($t1['periodontitisTreatmentWithoutBonusInPercentWithoutGKVPayment'], 1)."</strong> bis <strong>".$this->getMoneyFormat($t1['periodontitisWith10YearsBonusInPercentWithoutGKVPayment'], 0, 1)."</strong> (abhängig vom Bonusheft) für eine vollständig privat berechnete Parodontosebehandlung:</p>");
				$t['Zahnbehandlung & Prophylaxe'][5]['A'][] = array('checkmark' => '', 'content-list' => array('ohne Bonusheft' => "<strong>".$this->getMoneyFormat($t1['periodontitisTreatmentWithoutBonusInPercentWithoutGKVPayment'], 0, 1)."</strong>", '5 Jahre Bonusheft' => "<strong>".$this->getMoneyFormat($t1['periodontitisWith5YearsBonusInPercentWithoutGKVPayment'], 0, 1)."</strong>", '10 Jahre Bonusheft' => "<strong>".$this->getMoneyFormat($t1['periodontitisWith10YearsBonusInPercentWithoutGKVPayment'], 0, 1)."</strong>"));
				$t['Zahnbehandlung & Prophylaxe'][5]['A'][] = array('checkmark' => '', 'content' => "Die Kosten einer <strong>reinen Privatwurzelbehandlung</strong> können etwa zwischen <strong>800 und 1100€</strong> betragen!");

				$t['Kurz']['Zahnbehandlung']['Parodontosebehandlung ohne GKV-Vorleistung'] = array('checkmark' => 'green', 'content' => "<strong>".$this->getMoneyFormat($t1['periodontitisTreatmentWithoutBonusInPercentWithoutGKVPayment'], 0, 1)." - ".$this->getMoneyFormat($t1['periodontitisWith10YearsBonusInPercentWithoutGKVPayment'], 0, 1)."</strong>");
			}

			else
			{
				$t['Zahnbehandlung & Prophylaxe'][5]['A'][] = array('checkmark' => 'green', 'content' => "<strong>Parodontosebehandlung ohne Vorleistung der GKV:</strong><p>Fällt die Parodontosebehandlung <strong>nicht in den Leistungsumfang der gesetzlichen Krankenversicherung (Fall 1)</strong>, so leistet der Tarif <strong>".$this->convert($t1['name'])." ".$this->getMoneyFormat($t1['periodontitisTreatmentWithoutBonusInPercentWithoutGKVPayment'], 0, 1)."</strong> für eine vollständig privat berechnete Parodontosebehandlung.</p>");

				$t['Kurz']['Zahnbehandlung']['Parodontosebehandlung ohne GKV-Vorleistung'] = array('checkmark' => 'green', 'content' => $this->getMoneyFormat($t1['periodontitisTreatmentWithoutBonusInPercentWithoutGKVPayment'], 0, 1));
			}

			$t['Zahnbehandlung & Prophylaxe'][5]['A'][] = array('checkmark' => '', 'content' => "Im Rahmen einer <strong>reinen Privatbehandlung</strong> (ohne Vorleistung der GKV) leistet der Tarif für eine Behandlung mit hochmoderner Technik, beispielsweise die <strong>Anwendung eines Laser-Gerätes</strong>. Für die Abrechnung eines Lasers akzeptiert der Versicherer den Zuschlag nach GOZ 0120 (in Verbindung mit GOZ 4080, 4090, 4100, 4130 und 4133).");
		}
		else
		{
			$t['Zahnbehandlung & Prophylaxe'][5]['A'][] = array('checkmark' => 'yellow', 'content' => "<strong>Parodontosebehandlung ohne Vorleistung der GKV:</strong><p>Fällt die Parodontosebehandlung <strong>nicht in den Leistungsumfang der gesetzlichen Krankenversicherung (Fall 1)</strong>, so sieht der Tarif <strong>".$this->convert($t1['name'])."</strong> dafür <strong>keine Leistung</strong> vor!</p>");
		}

		
		if(isset($t1['hasPeriodontitisTreatmentWithPayment']) && $t1['hasPeriodontitisTreatmentWithPayment'] == 1)
		{
			if(is_array($zb_periodent_mit_gkv))
			{
				$t['Zahnbehandlung & Prophylaxe'][5]['A'][] = array('checkmark' => 'green', 'content' => "<strong>Parodontosebehandlung mit Vorleistung der GKV:</strong><p>Fällt die Parodontosebehandlung in den <strong>Leistungsumfang der GKV (Fall 2)</strong>, weil die Taschentiefe oberhalb von 3,5mm liegt, leistet der Tarif <strong>".$this->convert($t1['name'])." ".$this->getMoneyFormat($t1['periodontitisWithoutBonusInPercentWithGKVPayment'], 1)."</strong> bis <strong>".$this->getMoneyFormat($t1['periodontitisWith10YearsBonusInPercentWithGKVPayment'], 0, 1)."</strong> (abhängig vom Bonusheft) für <strong>privat berechnete Mehrkosten</strong></p>");
				$t['Zahnbehandlung & Prophylaxe'][5]['A'][] = array('checkmark' => '', 'content-list' => array('ohne Bonusheft' => $this->getMoneyFormat($t1['periodontitisWithoutBonusInPercentWithGKVPayment'], 0, 1), '5 Jahre Bonusheft' => $this->getMoneyFormat($t1['periodontitisWith5YearsBonusInPercentWithGKVPayment'], 0, 1), '10 Jahre Bonusheft' => $this->getMoneyFormat($t1['periodontitisWith10YearsBonusInPercentWithGKVPayment'], 0, 1)));
				$t['Zahnbehandlung & Prophylaxe'][5]['A'][] = array('checkmark' => '', 'content' => "Voraussetzung für die Berechnung der Mehrkosten ist, dass es sich um eigenständige Leistungen der GOZ handelt, die mit der Kassenleistung nicht abgegolten sind. Reine Verlangensleistungen gemäß §2.3 der GOZ werden vom Versicherer nicht erstattet.");

				$t['Kurz']['Zahnbehandlung']['Parodontosebehandlung mit GKV-Vorleistung'] = array('checkmark' => 'green', 'content' => '<strong>'.$this->getMoneyFormat($t1['periodontitisWithoutBonusInPercentWithGKVPayment'], 0).' - '.$this->getMoneyFormat($t1['periodontitisWith10YearsBonusInPercentWithGKVPayment'], 0, 1).'</strong> nach GKV');
			}
			else
			{
				$t['Zahnbehandlung & Prophylaxe'][5]['A'][] = array('checkmark' => 'green', 'content' => "<strong>Parodontosebehandlung mit Vorleistung der GKV:</strong></p><p>Fällt die Parodontosebehandlung in den <strong>Leistungsumfang der GKV (Fall 2)</strong>, weil die Taschentiefe oberhalb von 3,5mm liegt, leistet der Tarif <strong>".$this->convert($t1['name'])." ".$this->getMoneyFormat($t1['periodontitisWithoutBonusInPercentWithGKVPayment'], 0, 1)."</strong> für <strong>privat berechnete Mehrkosten</strong>.</p><p>Voraussetzung für die Berechnung der Mehrkosten ist, dass es sich um eigenständige Leistungen der GOZ handelt, die mit der Kassenleistung nicht abgegolten sind. Reine Verlangensleistungen gemäß §2.3 der GOZ werden vom Versicherer nicht erstattet.</p>");

				$t['Kurz']['Zahnbehandlung']['Parodontosebehandlung mit GKV-Vorleistung'] = array('checkmark' => 'green', 'content' => $this->getMoneyFormat($t1['periodontitisWithoutBonusInPercentWithGKVPayment'], 0, 1).' nach GKV');
			}

			$t['Zahnbehandlung & Prophylaxe'][5]['A'][] = array('checkmark' => '', 'content' => "Im Rahmen einer <strong>reinen Privatbehandlung</strong> (ohne Vorleistung der GKV) leistet der Tarif für eine Behandlung mit hochmoderner Technik, beispielsweise die <strong>Anwendung eines Laser-Gerätes</strong> oder die Arbeit mit einem <strong>hochauflösenden Operationsmikroskops</strong>. Für diese Leistungen akzeptiert der Versicherer die Berechnung der gemäß GOZ vorgesehenen Zuschlagspositionen (Zuschlag Laser GOZ 0120, Zuschlag OP-Mikroskop GOZ 0110).");
		}
		else
		{
			$t['Zahnbehandlung & Prophylaxe'][5]['A'][] = array('checkmark' => 'yellow', 'content' => "<strong>Parodontosebehandlung mit Vorleistung der GKV:</strong><p>Fällt die Parodontosebehandlung in den <strong>Leistungsumfang der GKV (Fall2)</strong>, weil die Taschentiefe oberhalb von 3,5mm liegt, leistet der Tarif <strong>".$this->convert($t1['name'])." nicht</strong> für <strong>privat berechnete Mehrkosten</strong>, die für eine umfassendere Behandlung entstehen!");
		}

		if(isset($t1['periodontitisTreatmentGKVIncludedInPercent']) && $t1['periodontitisTreatmentGKVIncludedInPercent'] == 1)
		{
			$t['Zahnbehandlung & Prophylaxe'][5]['A'][] = array('checkmark' => 'yellow', 'content' => "Der angegebene Prozentsatz beinhaltet bereits die Vorleistungen Ihrer gesetzlichen Krankenversicherung, d.h. die Leistungen der GKV werden angerechnet.");
		}

		$t['Zahnbehandlung & Prophylaxe'][5]['A'][] = array('checkmark' => '', 'content-expert' => "<div class='headline'>Experten-Erklärung</div><div class='content'><p>Bei einer <strong>Parodontose-Erkrankung</strong> bilden sich zwischen Zahn und Zahnfleisch sog. „Taschen“. In diesen <strong>Zahntaschen</strong> können sich Keime und Bakterien sammeln, die sich stark entzünden und eine <strong>Parodontitis</strong> hervorrufen können.</p><p>Die <strong>gesetzlichen Krankenkassen</strong> erstatten eine Parodontose-Behandlung erst ab einer <strong>Taschentiefe von 3,5mm</strong>. Um die Taschentiefe festzustellen, kann Ihr Zahnarzt alle 2 Jahre einen <strong>PSI-Status</strong> (parodontaler Screening Index) auf Kosten Ihrer gesetzlichen Krankenkasse erstellen.</p><p>Stellt Ihr Zahnarzt eine akute oder chronische Zahnfleischerkrankung fest, erstellt er einen <strong>Parodontalstatus</strong> und reicht diesen zusammen mit einem <strong>Behandlungsplan</strong> bei Ihrer <strong>gesetzlichen Krankenkasse</strong> ein. Zähne, an denen sich Zahntaschen von <strong>mehr als 3,5mm</strong> gebildet haben, können auf Kosten der gesetzlichen Krankenversicherung abgerechnet werden.</p><p><strong>Viele Zahnärzte empfehlen</strong> jedoch im Sinne einer Prophylaxe eine systematische Parodontose-Behandlung <strong>auch für Zähne mit niedrigeren Taschentiefen</strong>. Diese Behandlung ist vollständig Privatleistung (<strong>Fall 1: Parodontosebehandlung ohne GKV-Vorleistung</strong>).</p><p>Liegen die <strong>Taschentiefen bei über 3,5mm</strong>, übernimmt die GKV im Rahmen des <strong>Sachleistungsprinzips</strong> die Kosten der Parodontosebehandlung vollständig – allerdings nur im Rahmen einer einfachen <strong>Standardbehandlung</strong>, welche den Richtlinien des 5. Sozialgesetzbuches (SGB V) entspricht. Demnach muss die Behandlung der GKV</p><ul><li>ausreichend</li><li>wirtschaftlich</li><li cla>zweckmäßig</li></ul><p>sein.</p><p>Entscheiden Sie sich in Absprache mit Ihrem Zahnarzt für eine darüber hinausgehende <strong>hochwertigere Versorgung</strong> mit moderner zahnmedizinischer Technik, können ungeachtet der Kassenleistung darüber hinausgehende privat zu tragende <strong>Mehrkosten entstehen</strong>.</p></div>");

		if(!isset($t['Kurz']['Zahnbehandlung']['Parodontosebehandlung mit GKV-Vorleistung']))
			$t['Kurz']['Zahnbehandlung']['Parodontosebehandlung mit GKV-Vorleistung'] = array('checkmark' => 'red', 'content' => "keine Leistung");
		if(!isset($t['Kurz']['Zahnbehandlung']['Parodontosebehandlung ohne GKV-Vorleistung']))
			$t['Kurz']['Zahnbehandlung']['Parodontosebehandlung ohne GKV-Vorleistung'] = array('checkmark' => 'red', 'content' => "keine Leistung");

		

		if($ary["Zahnbehandlung"] == 'keine Wartezeit')
		{
			$t['Kurz']['Zahnbehandlung']['Wartezeit'] = array('checkmark' => 'green', 'content' => $ary["Zahnbehandlung"]);
		}
		elseif(!isset($ary["Zahnbehandlung"]))
		{
			$t['Kurz']['Zahnbehandlung']['Wartezeit'] = array('checkmark' => 'red', 'content' => 'keine Leistung');
		}
		else
			$t['Kurz']['Zahnbehandlung']['Wartezeit'] = array('checkmark' => 'yellow', 'content' => $ary["Zahnbehandlung"]);			



	// Leistet der Tarif für kieferorthopädische Behandlungen bei Erwachsenen?
		$t['Kieferorthopädie'][0]['Q'] = "Leistet der Tarif für kieferorthopädische Behandlungen bei Erwachsenen?";
		
		$t['Kieferorthopädie'][0]['A'];

		if(isset($t1['orthodonticsAdultsInPercent']) && $t1['orthodonticsAdultsInPercent'] == 0)
		{
			$t['Kieferorthopädie'][0]['A'][] = array('checkmark' => 'red', 'content' => "<strong>Nein</strong>, der Tarif sieht <strong>keine Leistungen</strong> für eine <strong>kieferorthopädische Behandlung bei Erwachsenen</strong> vor.");

			$t['Kurz']['Kieferorthopädie']['Erwachsene'] = array('checkmark' => 'red', 'content' => 'keine Leistungen');
		}
		if(isset($t1['orthodonticsAdultsInPercent']) && $t1['orthodonticsAdultsInPercent'] > 0)
		{
			$t['Kieferorthopädie'][0]['A'][] = array('checkmark' => 'green', 'content' => "<strong>Ja</strong>, der Tarif übernimmt die Kosten einer <strong>kieferorthopädischen Behandlung</strong> (Zahnspange) auch für <strong>Erwachsene</strong> in Höhe von <strong>".$this->getMoneyFormat($t1['orthodonticsAdultsInPercent'], 0, 1)."</strong>.");

			$t['Kurz']['Kieferorthopädie']['Erwachsene'] = array('checkmark' => 'green', 'content' => "<strong>".$this->getMoneyFormat($t1['orthodonticsAdultsInPercent'], 0, 1)."</strong>");
		}


		if(isset($t1['orthodonticsAdultsOnlyInCaseOfAccident']) && $t1['orthodonticsAdultsOnlyInCaseOfAccident'] == 1)
		{
			$t['Kieferorthopädie'][0]['A'][] = array('checkmark' => 'yellow', 'content' => "Der Versicherer übernimmt die Kosten einer Zahnkorrektur mittels <strong>Zahnspangen</strong> im genannten tariflichen Rahmen, sowohl wenn die Behandlungsbedürftigkeit auf einen <strong>Unfall</strong> zurückzuführen ist, der sich nach Abschluss der Versicherung ereignet hat, als auch bei Behandlungen, die <strong>nicht ursächlich auf einen Unfall</strong> zurückzuführen sind.<p>Erstattet werden ausschließlich <strong>medizinisch notwendige</strong> Korrekturen. Behandlungen, die rein aus <strong>kosmetischen Gründen</strong> durchgeführt werden sollen (die also medizinisch gesehen nicht notwendig wären), sind <strong>nicht erstattungsfähig</strong>.</p><p>Voraussetzung für eine Leistungspflicht ist darüber hinaus, dass sich die zu behandelnde Zahnfehlstellung nachweislich im Laufe der <strong>Versicherungszeit entwickelt</strong> hat. Die Korrektur einer Zahnfehlstellung, die bereits <strong>vor Abschluss</strong> der Versicherung bestand und diagnostiziert wurde, ist insofern vom Versicherungsschutz ausgeschlossen!</p>");
		}

		if(isset($t1['orthodonticsAdultsOnlyInCaseOfAccident']) && $t1['orthodonticsAdultsOnlyInCaseOfAccident'] == 0)
		{
			$t['Kieferorthopädie'][0]['A'][] = array('checkmark' => 'yellow', 'content' => "Der Versicherer übernimmt die Kosten einer Zahnkorrektur mittels <strong>Zahnspangen</strong> im genannten tariflichen Rahmen nur, wenn die Behandlungsbedürftigkeit auf einen <strong>Unfall</strong> zurückzuführen ist, der sich nachweislich <strong>nach Abschluss der Versicherung</strong> ereignet hat.<p>Erstattet werden ausschließlich <strong>medizinisch notwendige</strong> Korrekturen. Behandlungen, die rein aus <strong>kosmetischen Gründen</strong> durchgeführt werden sollen (die also medizinisch gesehen nicht notwendig wären), sind <strong>nicht erstattungsfähig</strong>.</p>");

			$t['Kurz']['Kieferorthopädie']['Erwachsene']['content'] .= ' nur bei Unfall';
		}

		if(isset($t1['orthodonticsAdultsAmountLimitPerYear']) && $t1['orthodonticsAdultsAmountLimitPerYear'] > 0)
		{
			$t['Kieferorthopädie'][0]['A'][] = array('checkmark' => 'yellow', 'content' => "Der Versicherer erstattet für kieferorthopädische Behandlungen bei Erwachsenen <strong>maximal</strong> bis zu <strong>".$this->getMoneyFormat($t1['orthodonticsAdultsAmountLimitPerYear'], 1)." je Versicherungsjahr</strong>.");

			$t['Kurz']['Kieferorthopädie']['Erwachsene']['content'] .= ' max. '.$this->getMoneyFormat($t1["orthodonticsAdultsAmountLimitPerYear"], 1).' je Versicherungsjahr';
		}

		if(isset($t1['orthodonticsAdultsAmountLimitationPerInsuredEvent']) && $t1['orthodonticsAdultsAmountLimitationPerInsuredEvent'] > 0)
		{
			$t['Kieferorthopädie'][0]['A'][] = array('checkmark' => 'yellow', 'content' => "Der Versicherer erstattet für kieferorthopädische Behandlungen bei Erwachsenen <strong>maximal</strong> bis zu <strong>".$this->getMoneyFormat($t1['orthodonticsAdultsAmountLimitationPerInsuredEvent'], 1)." je Versicherungsfall</strong>.");

			$t['Kurz']['Kieferorthopädie']['Erwachsene']['content'] .= ' max. '.$this->getMoneyFormat($t1["orthodonticsAdultsAmountLimitationPerInsuredEvent"], 1).' je Versicherungsfall';
		}

		if(isset($t1['orthodonticsAdultsAmountLimitationPerContractPeriod']) && $t1['orthodonticsAdultsAmountLimitationPerContractPeriod'] > 0)
		{
			$t['Kieferorthopädie'][0]['A'][] = array('checkmark' => 'yellow', 'content' => "Der Versicherer erstattet für kieferorthopädische Behandlungen bei Erwachsenen <strong>maximal</strong> bis zu <strong>".$this->getMoneyFormat($t1['orthodonticsAdultsAmountLimitationPerContractPeriod'], 1)." über die gesamte Vertragslaufzeit</strong>.");

			$t['Kurz']['Kieferorthopädie']['Erwachsene']['content'] .= ' max. '.$this->getMoneyFormat($t1["orthodonticsAdultsAmountLimitationPerContractPeriod"], 1).' über die gesamte Vertragslaufzeit';
		}

		if(isset($t1['orthodonticsAdultsAmountLimitPerJaw']) && $t1['orthodonticsAdultsAmountLimitPerJaw'] > 0)
		{
			$t['Kieferorthopädie'][0]['A'][] = array('checkmark' => 'yellow', 'content' => "Der Versicherer erstattet für kieferorthopädische Behandlungen bei Erwachsenen <strong>maximal</strong> bis zu <strong>".$this->getMoneyFormat($t1['orthodonticsAdultsAmountLimitPerJaw'], 1)." je behandeltem Kiefer</strong>.");

			$t['Kurz']['Kieferorthopädie']['Erwachsene']['content'] .= ' max. '.$this->getMoneyFormat($t1["orthodonticsAdultsAmountLimitPerJaw"], 1).' je behandeltem Kiefer';
		}



	// Leistet der Tarif für kieferorthopädische Behandlungen bei Kindern in KIG-2?
		$t['Kieferorthopädie'][1]['Q'] = "Leistet der Tarif für kieferorthopädische Behandlungen bei Kindern in KIG-2?";
		
		$t['Kieferorthopädie'][1]['A'];

		
		if(isset($t1['orthodonticsKig2InPercent']) )
		{
			if($t1['orthodonticsKig2InPercent'] == 0)
			{
				$t['Kieferorthopädie'][1]['A'][] = array('checkmark' => 'red', 'content' => "<strong>Nein</strong>, der Tarif sieht <strong>keine Leistungen</strong> für eine <strong>kieferorthopädische Behandlung bei Kindern in KIG 2</strong> vor.");

				$t['Kurz']['Kieferorthopädie']['Kinder - KIG 2'] = array('checkmark' => 'red', 'content' => 'keine Leistungen');
			}

			if($t1['orthodonticsKig2InPercent'] > 0)
			{

				$t['Kieferorthopädie'][1]['A'][] = array('checkmark' => 'green', 'content' => "Ja, der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet für medizinisch notwendige <strong>kieferorthopädische Behandlungen</strong> von Kindern, sofern <strong>keine Leistung der gesetzlichen Krankenkasse</strong> erbracht wird <strong>(KIG 2)</strong>.<p>Die Leistung beträgt <strong>".$this->getMoneyFormat($t1['orthodonticsKig2InPercent'], 0, 1)."</strong></p>");

				$t['Kurz']['Kieferorthopädie']['Kinder - KIG 2'] = array('checkmark' => 'green', 'content' => "<strong>".$this->getMoneyFormat($t1['orthodonticsKig2InPercent'], 0, 1)."</strong>");

				if(empty($t1['orthodonticsKig2AmountLimitationPerYear']) && 
				empty($t1['orthodonticsKig2AmountLimitationPerInsuredEvent']) &&
				empty($t1['orthodonticsKig2AmountLimitationPerContractPeriod']) &&
				empty($t1['orthodonticsKig2AmountLimitationPerJaw']))
				{
				$t['Kieferorthopädie'][1]['A'][] = array('checkmark' => '', 'content' => "Die Höhe der Leistungen bei KFO-Behandlungen <strong>ohne Vorleistung der GKV</strong> ist tariflich <strong>nicht</strong> auf einen bestimmten <strong>Höchstbetrag begrenzt</strong>!");
				}

			if(isset($t1['orthodonticsKig2AmountLimitationPerYear']) && $t1['orthodonticsKig2AmountLimitationPerYear'] > 0)
			{
				$t['Kieferorthopädie'][1]['A'][] = array('checkmark' => 'yellow', 'content' => "Der Versicherer erstattet für Behandlungen ohne Vorleistung der gesetzlichen Krankenkasse <strong>maximal</strong> bis zu <strong>".$this->getMoneyFormat($t1['orthodonticsKig2AmountLimitationPerYear'], 1)." je Versicherungsjahr</strong>.");

				$t['Kurz']['Kieferorthopädie']['Kinder - KIG 2']['content'] .= " max. ".$this->getMoneyFormat($t1['orthodonticsKig2AmountLimitationPerYear'], 1)." je Versicherungsjahr";
			}
			if(isset($t1['orthodonticsKig2AmountLimitationPerInsuredEvent']) && $t1['orthodonticsKig2AmountLimitationPerInsuredEvent'] > 0)
			{
				$t['Kieferorthopädie'][1]['A'][] = array('checkmark' => 'yellow', 'content' => "Der Versicherer erstattet für Behandlungen ohne Vorleistung der gesetzlichen Krankenkasse <strong>maximal</strong> bis zu <strong>".$this->getMoneyFormat($t1['orthodonticsKig2AmountLimitationPerInsuredEvent'], 1)." je Versicherungsfall</strong>.");

				$t['Kurz']['Kieferorthopädie']['Kinder - KIG 2']['content'] .= " max. ".$this->getMoneyFormat($t1['orthodonticsKig2AmountLimitationPerInsuredEvent'], 1)." je Versicherungsfall";
			}
			if(isset($t1['orthodonticsKig2AmountLimitationPerContractPeriod']) && $t1['orthodonticsKig2AmountLimitationPerContractPeriod'] > 0)
			{
				$t['Kieferorthopädie'][1]['A'][] = array('checkmark' => 'yellow', 'content' => "Der Versicherer erstattet für Behandlungen ohne Vorleistung der gesetzlichen Krankenkasse <strong>maximal</strong> bis zu <strong>".$this->getMoneyFormat($t1['orthodonticsKig2AmountLimitationPerContractPeriod'], 1)." über die gesamte Vertragslaufzeit</strong>.");

				$t['Kurz']['Kieferorthopädie']['Kinder - KIG 2']['content'] .= " max. ".$this->getMoneyFormat($t1['orthodonticsKig2AmountLimitationPerContractPeriod'], 1)." über die gesamte Vertragslaufzeit";
			}
			if(isset($t1['orthodonticsKig2AmountLimitationPerJaw']) && $t1['orthodonticsKig2AmountLimitationPerJaw'] > 0)
			{
				$t['Kieferorthopädie'][1]['A'][] = array('checkmark' => 'yellow', 'content' => "Der Versicherer erstattet für Behandlungen ohne Vorleistung der gesetzlichen Krankenkasse <strong>maximal</strong> bis zu <strong>".$this->getMoneyFormat($t1['orthodonticsKig2AmountLimitationPerJaw'], 1)." je behandeltem Kiefer</strong>.");

				$t['Kurz']['Kieferorthopädie']['Kinder - KIG 2']['content'] .= " max. ".$this->getMoneyFormat($t1['orthodonticsKig2AmountLimitationPerJaw'], 1)." je behandeltem Kiefer";
			}

			}
		}

		$t['Kieferorthopädie'][1]['A'][] = array('checkmark' => '', 'content-expert' => "<div class='headline'>Experten-Erklärung</div><div class='content'><p>Viele Kinder <strong>im Alter zwischen 8 und 12 Jahren</strong> benötigen eine <strong>Zahnspange</strong>. Die gesetzlichen Krankenkassen übernehmen die Korrektur allerdings nur in mittleren bis schweren Fällen.</p><p>Der Kieferorthopäde vermisst den Kiefer und trifft anhand der Ergebnisse eine Einstufung nach den sogenannten <strong>„kieferorthopädischen Indikationsgruppen“</strong>, abgekürzt „KIG“. Diese Skala zur Kategorisierung von Zahnfehlstellungen geht von <strong>KIG1</strong> (sehr leichte Fehlstellung) bis <strong>KIG5</strong> (schwerwiegende Fehlstellung). Die gesetzlichen Krankenkassen übernehmen die Kosten einer Korrektur erst ab KIG3 – 5.</p><p>Bei <strong>leichten Fehlstellungen</strong> vom Grad KIG 1-2 ist eine Korrektur <strong>keine Kassenleistung</strong>. Während eine Behandlung beim Grad von KIG 1 in den meisten Fällen noch <strong>nicht medizinisch notwendig</strong> ist, empfehlen die meisten Zahnärzte und Kieferorthopäden bei einer Fehlstellung vom Grad <strong>KIG 2 eine frühzeitige Behandlung</strong>, um eine weitere Verschiebung der Zähne zu verhindern und die Fehlstellung frühzeitig zu korrigieren.</p><p>Die <strong>Kosten</strong> einer solchen KFO-Behandlung können bis zu <strong>ca. 6.000€</strong> betragen und sollten bei Kindern <strong>frühzeitig versichert</strong> werden. Der Abschluss einer <strong>privaten Zahnzusatzversicherung für Kinder</strong> ist nur möglich, solange der Zahnarzt vor Abschluss keine Zahnfehlstellung diagnostiziert hat!</p></div>");


	// Leistet der Tarif für kieferorthopädische Behandlungen bei Kindern in KIG 3-5?
		$t['Kieferorthopädie'][2]['Q'] = "Leistet der Tarif für kieferorthopädische Behandlungen bei Kindern in KIG 3-5?";
		
		$t['Kieferorthopädie'][2]['A'];


		if(isset($t1['orthodonticsKig35InPercent']) && $t1['orthodonticsKig35InPercent'] == 0)
		{
			$t['Kieferorthopädie'][2]['A'][] = array('checkmark' => 'red', 'content' => "<strong>Nein</strong>, der Tarif sieht <strong>keine Leistungen</strong> für Mehrkosten einer <strong>kieferorthopädischen Behandlung bei Kindern in KIG 3-5</strong> vor.");

			$t['Kurz']['Kieferorthopädie']['Kinder - KIG 3-5'] = array('checkmark' => 'red', 'content' => "keine Leistungen");
		}

		if(isset($t1['orthodonticsKig35InPercent']) && $t1['orthodonticsKig35InPercent'] > 0)
		{
			$t['Kieferorthopädie'][2]['A'][] = array('checkmark' => 'green', 'content' => "Ja, der Tarif <strong>".$this->convert($t1['name'])."</strong> leistet für Mehrkosten einer <strong>kieferorthopädischen Behandlung</strong> bei Kindern, wenn die Leistung grundsätzlich in den Bereich der gesetzlichen Krankenkasse fällt <strong>(KIG 3-5)</strong>.<p>Die Leistung beträgt ".$this->getMoneyFormat($t1['orthodonticsKig35InPercent'], 0, 1).".</p>");

				$t['Kurz']['Kieferorthopädie']['Kinder - KIG 3-5'] = array('checkmark' => 'green', 'content' => "<strong>".$this->getMoneyFormat($t1['orthodonticsKig35InPercent'], 0, 1)."</strong>");
		

			if(isset($t1['orthodonticsKig35AestheticBenefitsExplicitInTermsAndConditions']) && $t1['orthodonticsKig35AestheticBenefitsExplicitInTermsAndConditions'] == 1)
			{
				$t['Kieferorthopädie'][2]['A'][] = array('checkmark' => 'yellow', 'content' => "Neben Zusatzleistungen, die aus Gründen der medizinischen Notwendigkeit erbracht werden, nennt der Versicherer in seinen Vertragsbedingungen explizit auch einige Zusatzbehandlungen, die rein kosmetisch sind:<p>Aufzählung ästhetische Mehrkosten mit Aufzählungszeichen → in DB speichern für entsprechende Tarife!!!</p>");
			}
			if(isset($t1['orthodonticsKig35AestheticBenefitsExplicitInTermsAndConditions']) && $t1['orthodonticsKig35AestheticBenefitsExplicitInTermsAndConditions'] == 0)
			{
				$t['Kieferorthopädie'][2]['A'][] = array('checkmark' => 'green', 'content' => "Erstattungsfähig sind ausschließlich Kosten für medizinisch notwendige Zusatzleistungen. Leistungen, die rein aus Gründen der Ästhetik erbracht werden, sind nicht erstattungsfähig!");
			}

			if(empty($t1['orthodonticsKig35AmountLimitationPerYear']) && 
				empty($t1['orthodonticsKig35AmountLimitationPerInsuredEvent']) &&
				empty($t1['orthodonticsKig35AmountLimitationPerContractPeriod']) &&
				empty($t1['orthodonticsKig35AmountLimitationPerJaw']))
			{
				$t['Kieferorthopädie'][2]['A'][] = array('checkmark' => '', 'content' => "Die Höhe der Leistungen bei KFO-Behandlungen <strong>ohne Vorleistung der GKV</strong> ist tariflich <strong>nicht</strong> auf einen bestimmten <strong>Höchstbetrag begrenzt</strong>!");
			}
		
			if(isset($t1['orthodonticsKig35AmountLimitationPerYear']) && $t1['orthodonticsKig35AmountLimitationPerYear'] > 0)
			{
				$t['Kieferorthopädie'][2]['A'][] = array('checkmark' => 'yellow', 'content' => "Der Versicherer erstattet für Behandlungen ohne Vorleistung der gesetzlichen Krankenkasse <strong>maximal</strong> bis zu <strong>".$this->getMoneyFormat($t1['orthodonticsKig35AmountLimitationPerYear'], 1)." je Versicherungsjahr</strong>.");

				$t['Kurz']['Kieferorthopädie']['Kinder - KIG 3-5']['content'] .= " max. ".$this->getMoneyFormat($t1['orthodonticsKig35AmountLimitationPerYear'], 1)." je Versicherungsjahr";
			}
			if(isset($t1['orthodonticsKig35AmountLimitationPerInsuredEvent']) && $t1['orthodonticsKig35AmountLimitationPerInsuredEvent'] > 0)
			{
				$t['Kieferorthopädie'][2]['A'][] = array('checkmark' => 'yellow', 'content' => "Der Versicherer erstattet für Behandlungen ohne Vorleistung der gesetzlichen Krankenkasse <strong>maximal</strong> bis zu <strong>".$this->getMoneyFormat($t1['orthodonticsKig35AmountLimitationPerInsuredEvent'], 1)." je Versicherungsfall</strong>.");

				$t['Kurz']['Kieferorthopädie']['Kinder - KIG 3-5']['content'] .= " max. ".$this->getMoneyFormat($t1['orthodonticsKig35AmountLimitationPerInsuredEvent'], 1)." je Versicherungsfall";
			}
			if(isset($t1['orthodonticsKig35AmountLimitationPerContractPeriod']) && $t1['orthodonticsKig35AmountLimitationPerContractPeriod'] > 0)
			{
				$t['Kieferorthopädie'][2]['A'][] = array('checkmark' => 'yellow', 'content' => "Der Versicherer erstattet für Behandlungen ohne Vorleistung der gesetzlichen Krankenkasse <strong>maximal</strong> bis zu <strong>".$this->getMoneyFormat($t1['orthodonticsKig35AmountLimitationPerContractPeriod'], 1)." über die gesamte Vertragslaufzeit</strong>.");

				$t['Kurz']['Kieferorthopädie']['Kinder - KIG 3-5']['content'] .= " max. ".$this->getMoneyFormat($t1['orthodonticsKig35AmountLimitationPerContractPeriod'], 1)." über die gesamte Vertragslaufzeit";
			}
			if(isset($t1['orthodonticsKig35AmountLimitationPerJaw']) && $t1['orthodonticsKig35AmountLimitationPerJaw'] > 0)
			{
				$t['Kieferorthopädie'][2]['A'][] = array('checkmark' => 'yellow', 'content' => "Der Versicherer erstattet für Behandlungen ohne Vorleistung der gesetzlichen Krankenkasse <strong>maximal</strong> bis zu <strong>".$this->getMoneyFormat($t1['orthodonticsKig35AmountLimitationPerJaw'], 1)." je behandeltem Kiefer</strong>.");

				$t['Kurz']['Kieferorthopädie']['Kinder - KIG 3-5']['content'] .= " max. ".$this->getMoneyFormat($t1['orthodonticsKig35AmountLimitationPerJaw'], 1)." je behandeltem Kiefer";
			}
		}

		$t['Kieferorthopädie'][2]['A'][] = array('checkmark' => '', 'content-expert' => "<div class='headline'>Experten-Erklärung</div><div class='content'><p>Wenn bei Ihrem Kind eine <strong>mittlere bis schwerwiegende</strong> Zahnfehlstellung im Grad <strong>KIG 3-5</strong> vorliegt, erstattet die gesetzliche Krankenkasse die Kosten einer <strong>Standard KFO-Behandlung</strong>. Die Eltern müssen in diesem Fall zunächst 10 bis 20% Selbstbehalt übernehmen – dieser wird seitens der GKV dann am Ende der Behandlung rückerstattet, wenn ein erfolgreicher Abschluss der Behandlung nachgewiesen werden kann – damit will man Eltern und Kinder motivieren, aktiv auf einen erfolgreichen Behandlungsverlauf mitzuwirken (z.B. indem herausnehmbare Zahnspangen regelmäßig getragen werden).</p><p>In diesen Fällen können – zusätzlich zur Leistung der gesetzlichen Krankenkasse – <strong>Mehrkosten für ergänzende Privatleistungen</strong> entstehen. Diese sind gesondert im Rahmen einer sogenannten <strong>Mehrkostenvereinbarung</strong> mit dem Kieferorthopäden zu vereinbaren. Darin können beispielsweise hochwertigere Brackets, Retainer oder andere Zusatzleistungen vereinbart werden. Die Kosten solcher Zusatzleistungen liegen in vielen Fällen zwischen <strong>ca. 1.000 – 2.000€</strong>.</p><p><strong>Private Zusatzversicherungen</strong> übernehmen teilweise auch derartige <strong>Mehrkosten</strong> in gewissem Umfang. Üblicherweise werden allerdings nur solche Mehrkosten erstattet, die für <strong>medizinisch notwendige Zusatzbehandlungen</strong> anfallen – es sei denn, die offiziellen Vertragsbedingungen (AVB) führen <strong>explizit</strong> auch Leistungen für rein kosmetische Zusatzleistungen auf (nur in diesem Fall wären auch kosmetische Zusatzleistungen vom Versicherungsschutz erfasst).</p></div>");


		if($ary["Kieferorthopädie"] == 'keine Wartezeit')
		{
			$t['Kurz']['Kieferorthopädie']['Wartezeit'] = array('checkmark' => 'green', 'content' => $ary["Kieferorthopädie"]);
		}
		elseif(!isset($ary["Kieferorthopädie"]))
		{
			$t['Kurz']['Kieferorthopädie']['Wartezeit'] = array('checkmark' => 'red', 'content' => 'keine Leistungen');
		}
		else
		{
			$t['Kurz']['Kieferorthopädie']['Wartezeit'] = array('checkmark' => 'yellow', 'content' => $ary["Kieferorthopädie"]);
		}



	// So gehen Sie im Leistungsfall vor
		$t['Sonstiges'][0]['Q'] = "So gehen Sie im Leistungsfall vor";
		
		$t['Sonstiges'][0]['A'];

		if(isset($t1['planAndCostRequired']) && $t1['planAndCostRequired'] == 0)
		{
			$t['Sonstiges'][0]['A'][] = array('checkmark' => '', 'content' => "<strong>Vor Beginn der Behandlung – Heil- und Kostenplan prüfen lassen</strong>:</p><p>Die Einreichung eines <strong>Heil- und Kostenplanes</strong> (HKP) vor Behandlungsbeginn ist im Tarif <strong>".$this->convert($t1['name'])." nicht zwingend vorgeschrieben</strong> – das bedeutet, dass die Erstattungsleistung nicht rein auf Basis des Umstandes gekürzt werden kann, dass vorab kein Heil- und Kostenplan eingereicht worden ist.</p><p>Generell <strong>empfehlen</strong> wir jedoch im eigenen Interesse <strong>umfangreichere zahnärztliche Behandlungen</strong> (insbesondere im Bereich Zahnersatz oder Kieferorthopädie) im Vorhinein mit dem Versicherer abzustimmen und <strong>rechtzeitig einen Kostenplan zur Prüfung</strong> einzureichen. Der Versicherer teilt Ihnen dann schon vorab mit, ob er die Kosten der geplanten Behandlung im vollen tariflichen Umfang übernehmen kann oder ob es Einschränkungen gibt.</p><p><strong>Hinweis für weniger umfangreiche Behandlungen</strong>:</p><p>Bei <strong>kleinen Behandlungen</strong> wie z.B. einer <strong>professionellen Zahnreinigung (PZR)</strong> oder einer <strong>Kunststofffüllung</strong> müssen Sie natürlich vorab <strong>keinen Kostenplan</strong> einreichen. Hier reicht es in der Tat aus, später die Rechnung einzureichen.</p>");
		}
		if(isset($t1['planAndCostRequired']) && $t1['planAndCostRequired'] == 1)
		{
			$t['Sonstiges'][0]['A'][] = array('checkmark' => 'yellow', 'content' => "<strong>Vor Beginn der Behandlung – Heil- und Kostenplan prüfen lassen</strong>:</p><p>Die Einreichung eines <strong>Heil- und Kostenplanes</strong> (HKP) vor Behandlungsbeginn ist im Tarif <strong>".$this->convert($t1['name'])."</strong> ab einem Rechnungsbetrag in Höhe von <strong>".$this->getMoneyFormat($t1['planAndCostRequiredAmount'], 1)."</strong> verpflichtend vorgeschrieben. Im Falle eines Versäumnis, kürzt der Versicherer den <strong>".$this->getMoneyFormat($t1['planAndCostRequiredAmount'], 1)."</strong> übersteigenden Rechnungsbetrag und somit die Erstattungsleistung um <strong>".$this->getMoneyFormat($t1['noPlanAndCostReductions'], 0, 1)."</strong>. Reichen Sie bitte rechtzeitig vor Behandlungsbeginn einen Kostenplan zur Prüfung ein, sofern zu erwarten ist, dass der o.g. Betrag überschritten wird.</p><p>Auch ohne tarifliche Verpflichtung empfehlen wir allerdings schon <strong>im eigenen Interesse</strong>, umfangreichere zahnärztliche Behandlungen im Vorhinein mit dem Versicherer abzustimmen. Der Versicherer teilt Ihnen dann schon vorab mit, ob er die Kosten der geplanten Behandlung im vollen tariflichen Umfang übernehmen kann oder ob es Einschränkungen gibt.</p><p><strong>Hinweis für weniger umfangreiche Behandlungen</strong>:</p><p>Bei <strong>kleinen Behandlungen</strong> wie z.B. einer <strong>professionellen Zahnreinigung (PZR)</strong> oder einer <strong>Kunststofffüllung</strong> müssen Sie natürlich vorab <strong>keinen Kostenplan</strong> einreichen. Hier reicht es in der Tat aus, später die <strong>Rechnung</strong> einzureichen.</p>");
		}
		$t['Sonstiges'][0]['A'][] = array('checkmark' => '', 'content' => "Ihren <strong>Heil- und Kostenplan</strong> können Sie dem Versicherer wahlweise per <strong>Post, Fax oder E-Mail</strong> senden:");

		$address = "Kontaktdaten Versicherer :<br />";

		if(isset($in['company']) && strlen($in['company']) > 0)
			$address .= "<p>Per Post:<br />".$in['company'];

		if(isset($in['post_box']) && strlen($in['post_box']) > 0)
			$address .= "<br />".$in['post_box'];

		if(isset($in['street']) && strlen($in['street']) > 0)
			$address .= "<br />".$in['street'];

		if(isset($in['zip']) && isset($in['city']))
			$address .= "<br />".$in['zip']." ".$in['city'];

		$address .= "</p>";

		if(isset($in['email']) && strlen($in['email']) > 0)
			$address .= "<p>Per E-Mail:<br />".$in['email']."</p>";

		if(isset($in['fax']) && strlen($in['fax']) > 0)
			$address .= "<p>Per Fax:<br />".$in['fax_prefix']." ".$in['fax']."</p>";


		


		$t['Sonstiges'][0]['A'][] = array('checkmark' => '', 'content' => $address);

		$t['Sonstiges'][0]['A'][] = array('checkmark' => '', 'content' => "<strong>Nach der Behandlung – Rechnung per Post einreichen:</strong><p>Nach Durchführung der Behandlung erhalten Sie von Ihrem Zahnarzt üblicherweise eine <strong>Rechnung in zweifacher Ausfertigung</strong> – Original und Duplikat.</p><p>Der Versicherer benötigt für die Erstattung das <strong>Original</strong>. Bitte schicken Sie die Original-Rechnung unbedingt <strong>per Post direkt an die Versicherung</strong> (Anschrift siehe oben).</p><p>Die Versicherung überweist seine Leistung nie an den Zahnarzt direkt, sondern immer <strong>auf Ihr Konto</strong>. Die Überweisung der Rechnung an Ihren Zahnarzt müssen Sie daher selbst veranlassen. Dazu gehen Sie entweder in Vorleistung oder warten den Geldeingang des Versicherers ab (bitte berücksichtigen Sie eventuell bestehende Zahlungsziele Ihres Zahnarztes – die rechtzeitige Begleichung der Rechnung liegt in Ihrem Verantwortungsbereich, unabhängig davon wie lange die Bearbeitung der Rechnungen durch den Versicherer dauert).</p>");
		$t['Sonstiges'][0]['A'][] = array('checkmark' => '', 'content-expert' => "<div class='headline'>Hinweis 1: Bearbeitungsdauer allgemein</div><div class='content'>
<p>Üblicherweise dauert die Bearbeitung von Rechnungen und Kostenplänen nicht länger als <strong>maximal 2-4 Wochen</strong>. Insbesondere Kleinschäden wie z.B. Zahnreinigung werden häufig auch deutlich schneller reguliert.
</p><p>In Einzelfällen kann die Bearbeitung länger dauern, beispielsweise wenn die Versicherung noch weitergehende Unterlagen zur Beurteilung der medizinischen Notwendigkeit o.Ä. benötigt.
</p></div>");
		$t['Sonstiges'][0]['A'][] = array('checkmark' => '', 'content-expert' => "<div class='headline'>Hinweis 2: Bearbeitungsdauer insbesondere beim 1. größeren Leistungsfall</div><div class='content'>
</p><p>Insbesondere die Prüfung des <strong>ersten größeren Leistungsfalles</strong> wird in den meisten Fällen <strong>etwas länger dauern</strong>, weil die Versicherung zu diesem Zeitpunkt üblicherweise prüft, ob die eingereichte Behandlung bereits <strong>vor Antragstellung vom Zahnarzt angeraten</strong> war. Aus diesem Grund fordert der Versicherer beim ersten Schadenfall üblicherweise Auskünfte Ihres Zahnarztes ein.
</p><p>Bitten Sie Ihren Zahnarzt in diesem Fall um seine zügige Mithilfe. Je zügiger Ihr Zahnarzt die benötigten Auskünfte erteilt, umso schneller kann das Versicherungsunternehmen Ihren Leistungsfall regulieren und das Geld auf Ihr Konto überweisen.</p></div>");

		// Source
		// $t['Source'] = $t1;

		$tf = $this->getTariffAVBFile($idt);
		if(isset($tf))
		{
			if(isset($tf['file']))
			{
				$t['Kurz']['Sonstiges']['Versicherungsbedingungen'] = array('checkmark' => '', 'content' => "<i class='pdfIcon'></i><a href='".$tf['file']."' target='_new'>AVB</a>");
			}
		}


		// Preis und Leistungsverzeichnis soll bei ZE und KFO auch stehen...
		$t['Kurz']['Kieferorthopädie']['Preis- und Leistungsverzeichnis Materialkosten'] = $t['Kurz']['Sonstiges']['Preis- und Leistungsverzeichnis Materialkosten'];
		// Preis und Leistungsverzeichnis soll bei ZE und KFO auch stehen...
		$t['Kurz']['Zahnersatz']['Preis- und Leistungsverzeichnis Materialkosten'] = $t['Kurz']['Sonstiges']['Preis- und Leistungsverzeichnis Materialkosten'];
	


		if($short) {
			// Erstelle Übersicht für Kurzansicht

			if($tt['benefits-implants'])
				$t['Kurz']['Zahnersatz']['Implantate']['tooltip'] = $tt['benefits-implants'];
			if($tt['benefits-implants-limitations'])
				$t['Kurz']['Zahnersatz']['Implantate begrenzt']['tooltip'] = $tt['benefits-implants-limitations'];
			if($tt['benefits-implants-bone-augmentation'])
				$t['Kurz']['Zahnersatz']['Knochenaufbau Implantate']['tooltip'] = $tt['benefits-implants-bone-augmentation'];
			if($tt['benefits-crowns-bridges'])
				$t['Kurz']['Zahnersatz']['Kronen, Brücken und Prothesen']['tooltip'] = $tt['benefits-crowns-bridges'];
			if($tt['benefits-veneering'])
				$t['Kurz']['Zahnersatz']['Keramikverblendungen']['tooltip'] = $tt['benefits-veneering'];
			if($tt['benefits-inlays'])
				$t['Kurz']['Zahnersatz']['Inlays']['tooltip'] = $tt['benefits-inlays'];
			if($tt['benefits-inlays-limitations'])
				$t['Kurz']['Zahnersatz']['Inlays begrenzt']['tooltip'] = $tt['benefits-inlays-limitations'];
			if($tt['benefits-regular-care'])
				$t['Kurz']['Zahnersatz']['Regelversorgung']['tooltip'] = $tt['benefits-regular-care'];
			if($tt['benefits-waiting-time-dentures'])
				$t['Kurz']['Zahnersatz']['Wartezeit']['tooltip'] = $tt['benefits-waiting-time-dentures'];
			if($tt['benefits-material-cost-repository'])
				$t['Kurz']['Zahnersatz']['Preis- und Leistungsverzeichnis Materialkosten']['tooltip'] = $tt['benefits-material-cost-repository'];


			if($tt['benefits-synthetic-fillings'])
				$t['Kurz']['Zahnbehandlung']['Hochwertige Füllungen']['tooltip'] = $tt['benefits-synthetic-fillings'];
			if($tt['benefits-root-treatment-with-gkv'])
				$t['Kurz']['Zahnbehandlung']['Wurzelbehandlung mit GKV-Vorleistung']['tooltip'] = $tt['benefits-root-treatment-with-gkv'];
			if($tt['benefits-root-treatment-without-gkv'])
				$t['Kurz']['Zahnbehandlung']['Wurzelbehandlung ohne GKV-Vorleistung']['tooltip'] = $tt['benefits-root-treatment-without-gkv'];
			if($tt['benefits-periodontitis-treatment'])
				$t['Kurz']['Zahnbehandlung']['Parodontosebehandlung mit GKV-Vorleistung']['tooltip'] = $tt['benefits-periodontitis-treatment'];
			if($tt['benefits-periodontitis-treatment'])
				$t['Kurz']['Zahnbehandlung']['Parodontosebehandlung ohne GKV-Vorleistung']['tooltip'] = $tt['benefits-periodontitis-treatment'];
			if($tt['benefits-waiting-time-dental-treatment'])
				$t['Kurz']['Zahnbehandlung']['Wartezeit']['tooltip'] = $tt['benefits-waiting-time-dental-treatment'];
			#if($tt['benefits-material-cost-repository'])
			#	$t['Kurz']['Zahnbehandlung']['Preis- und Leistungsverzeichnis Materialkosten']['tooltip'] = $tt['benefits-material-cost-repository'];


			if($tt['benefits-dental-cleaning'])
				$t['Kurz']['Prophylaxe']['Professionelle Zahnreinigung']['tooltip'] = $tt['benefits-dental-cleaning'];
			if($tt['benefits-dental-cleaning'])
				$t['Kurz']['Prophylaxe']['Fissurenversiegelung']['tooltip'] = $tt['benefits-fissures'];
			if($tt['benefits-waiting-time-dental-treatment'])
				$t['Kurz']['Prophylaxe']['Wartezeit']['tooltip'] = $tt['benefits-waiting-time-dental-treatment'];
			#if($tt['benefits-material-cost-repository'])
			#	$t['Kurz']['Prophylaxe']['Preis- und Leistungsverzeichnis Materialkosten']['tooltip'] = $tt['benefits-material-cost-repository'];


			if($tt['benefits-kig2'])
				$t['Kurz']['Kieferorthopädie']['Kinder - KIG 2']['tooltip'] = $tt['benefits-kig2'];
			if($tt['benefits-kig35'])
				$t['Kurz']['Kieferorthopädie']['Kinder - KIG 3-5']['tooltip'] = $tt['benefits-kig35'];
			if($tt['benefits-orthodontics-for-adults'])
				$t['Kurz']['Kieferorthopädie']['Erwachsene']['tooltip'] = $tt['benefits-orthodontics-for-adults'];
			if($tt['benefits-waiting-time-orthodontics'])
				$t['Kurz']['Kieferorthopädie']['Wartezeit']['tooltip'] = $tt['benefits-waiting-time-orthodontics'];
			if($tt['benefits-material-cost-repository'])
				$t['Kurz']['Kieferorthopädie']['Preis- und Leistungsverzeichnis Materialkosten']['tooltip'] = $tt['benefits-material-cost-repository'];


			if($tt['benefits-ordinary-right-of-termination'])
				$t['Kurz']['Sonstiges']['Verzicht ordentliches Kündigungsrecht']['tooltip'] = $tt['benefits-ordinary-right-of-termination'];
			if($tt['benefits-material-cost-repository'])
				$t['Kurz']['Sonstiges']['Preis- und Leistungsverzeichnis Materialkosten']['tooltip'] = $tt['benefits-material-cost-repository'];
			if($tt['benefits-termination-period-and-min-contract-duration'])
				$t['Kurz']['Sonstiges']['Mindestvertragsdauer & Kündigung']['tooltip'] = $tt['benefits-termination-period-and-min-contract-duration'];
			if($tt['benefits-missing-teeth'])
				$t['Kurz']['Sonstiges']['Zahnlücken versicherbar?']['tooltip'] = $tt['benefits-missing-teeth'];
			if($tt['benefits-removable-dentures'])
				$t['Kurz']['Sonstiges']['Vorhandene Prothesen versichert?']['tooltip'] = $tt['benefits-removable-dentures'];
			if($tt['benefits-crowns-and-bridges-insured'])
				$t['Kurz']['Sonstiges']['Vorhandene Kronen, Brücken & Füllungen versichert?']['tooltip'] = $tt['benefits-crowns-and-bridges-insured'];
			if($tt['benefits-health-check'])
				$t['Kurz']['Sonstiges']['Annahmerichtlinien']['tooltip'] = $tt['benefits-health-check'];
			if($tt['benefits-agening-reserves'])
				$t['Kurz']['Sonstiges']['Beitragsverlauf']['tooltip'] = $tt['benefits-agening-reserves'];





			$t['Kurz']['Übersicht']['Zahnersatz'] = array('Implantate' => $t['Kurz']['Zahnersatz']['Implantate'], 'Kronen & Brücken' => $t['Kurz']['Zahnersatz']['Kronen, Brücken und Prothesen'], 'Inlays' => $t['Kurz']['Zahnersatz']['Inlays'], 'Regelversorgung' => $t['Kurz']['Zahnersatz']['Regelversorgung']);
			$t['Kurz']['Übersicht']['Zahnbehandlung'] = array('Hochwertige Füllungen' => $t['Kurz']['Zahnbehandlung']['Hochwertige Füllungen'], 'Wurzelbehandlung' => $t['Kurz']['Zahnbehandlung']['Wurzelbehandlung mit GKV-Vorleistung'], 'Parodontosebehandlung' => $t['Kurz']['Zahnbehandlung']['Parodontosebehandlung mit GKV-Vorleistung']);
			$t['Kurz']['Übersicht']['Zahnprophylaxe'] = array('Zahnreinigung (PZR)' => $t['Kurz']['Prophylaxe']['Professionelle Zahnreinigung'], 'Bleaching' => $t['Kurz']['Prophylaxe']['Bleaching'], 'Fissurenversiegelung' => $t['Kurz']['Prophylaxe']['Fissurenversiegelung']);
			$t['Kurz']['Übersicht']['Kieferorthopädie'] = array('KFO - KIG 2' => $t['Kurz']['Kieferorthopädie']['Kinder - KIG 2'], 'KFO - KIG 3-5' => $t['Kurz']['Kieferorthopädie']['Kinder - KIG 3-5'], 'KFO - Erwachsene' => $t['Kurz']['Kieferorthopädie']['Erwachsene']);
			
			if($t1['hasAgeningReserves'] == 0)
			{
				$t['Kurz']['Beiträge']['Beiträge'][] = array('checkmark' => '', 'content' => 'Dieser Tarif ist ohne Alterungsrückstellungen kalkuliert - die Beiträge verändern sich automatisch bei Erreichen der jeweiligen Altersstufen');
				$t['Kurz']['Beiträge']['Beiträge'][] = array('checkmark' => '', 'content' => $bonusBase);
			} elseif($t1['hasAgeningReserves'] == 1)
			{
				$t['Kurz']['Beiträge']['Beiträge'][] = array('checkmark' => '', 'content' => 'Dieser Tarif ist mit Alterungsrückstellungen kalkuliert - der Beitrag wird auf Basis Ihres Eintrittsalters berechnet und erhöht sich nicht automatisch bei Erreichen eines bestimmten Alters');
			}
			
	
			//
			#if(!$tbl)
			$tbl = $this->getInsuranceYearsTable($t1['areLimitationsCumulated'], $lim['dentures'], true);

			if(!isset($tbl['1. Kalenderjahr']))
				$tbl = array('1. Kalenderjahr' => 'unbegrenzt', 'unbegrenzte Leistung' => 'sofort');

			$t['Kurz']['Übersicht']['Begrenzungen & Wartezeiten'] = array('Max. Zahnersatz 1. Jahr' => array('checkmark' => '', 'content' => $tbl['1. Kalenderjahr']), 'unbegrenzte Leistung' => array('checkmark' => '', 'content' => $tbl['unbegrenzte Leistung']), 'Wartezeit Zahnersatz' => $t['Kurz']['Zahnersatz']['Wartezeit'], 'Wartezeit Zahnreinigung' => $t['Kurz']['Prophylaxe']['Wartezeit'], 'Wartezeit Kieferorthopädie' => $t['Kurz']['Kieferorthopädie']['Wartezeit']);

			if($tt['benefits-max-reimbursement-first-year'])
				$t['Kurz']['Übersicht']['Begrenzungen & Wartezeiten']['Max. Zahnersatz 1. Jahr']['tooltip'] = $tt['benefits-max-reimbursement-first-year'];
			if($tt['benefits-max-reimbursement-fourth-year'])
				$t['Kurz']['Übersicht']['Begrenzungen & Wartezeiten']['unbegrenzte Leistung']['tooltip'] = $tt['benefits-max-reimbursement-fourth-year'];


			foreach($t['Kurz'] as $id => $val)
			{
				if($id != 'Übersicht')
				{
					$this->expandArray($t, $id);

					if($id == 'Leistungsstaffel')
						$this->expandArray($t, $id);
				}
			}

			$t = $t['Kurz'];
		} else 
			unset($t['Kurz']);

		return $t;
	}

	private function expandArray(&$t, $name)
	{
		$tmp = $t['Kurz'][$name]; unset($t['Kurz'][$name]);
		$t['Kurz'][$name][$name] = $tmp;
	}


}