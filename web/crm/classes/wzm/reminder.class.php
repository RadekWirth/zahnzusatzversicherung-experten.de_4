<?php


class reminder extends wzm
{

protected $templ;
protected $debug;

	public function __construct($path)
	{
		$this->templ = $path.'templates/mail-reminder-template.tpl';
		$this->debug = 'false';
	}

	public function checkData()
	{
		/* checks whether data was already stored */
		$today = date('Y-m-d', time());

		$db = $this->getDb();
		$sql = "SELECT count(id) as cnt FROM `".$this->table('reminder').
			"` WHERE `added_dte` = '".$today."'; ";
		$rs = $db->queryPDO($sql);
		$row = $db->fetchPDO($rs);

	return $row['cnt'];
	}

	public function getData($date, $id=0)
	{
		/* gets Data for reminder writing */
		$id = $id>0?" id = ".$id:" 1=1";
		$mydate = explode('-', $date);


		$db = $this->getDb();
		$sql ="SELECT `id`, `pid`, `status`, `added_dte` FROM `".$this->table('reminder').
			"` WHERE ".$id." AND `added_dte` = '".$date."' ORDER BY `id` DESC";
		$rs = $db->queryPDO($sql);
		$ret = '';

		if($db->getNumRows($rs)>0) {
			while($row = $db->fetchPDO($rs)) {
				$ret[] = $row;
			}
		} else
		{
			/* possible place to reload contractreminders */
		}

		return $ret;
	}


	public function showDataLines($days=30)
	{
		if(!is_numeric($days) || $days<=0) return;

		$day = 24*60*60; $t = ""; 
		$db = $this->getDb();

		/* Datum soll mit Anzahl der Tage vom frühesten zum Älteren dargestellt werden. */
		/* Angezeigt werden $days Tage, der Rest max 30 Tage klappt auf in einem div    */
		for($i=0; $i<$days; $i++)
		{
			$tmp = $sql = "";

			$date2show = date('Y-m-d', time()- $i*$day);

			$sql = "SELECT count(`count`) as cnt, `status`, `added_dte` FROM `".$this->table('reminder').
				"` WHERE `added_dte` = '".$date2show."' GROUP BY `added_dte`, `status` ;";
			$rs = $db->queryPDO($sql);
			while($row = $db->fetchPDO($rs))
			{
			$tmp = '<li class="'.$row['status'].'">'.urlHelper::makeLink('crmIntern', 'generateReminderLetters',
				$row['added_dte'], array('date' => $row['added_dte'])).' - Anzahl : <b>'.$row['cnt'].'</b></li>';
			}
			
			if(!empty($tmp)) { $t .= $tmp; } else {
				$t .= '<li class="notcounted">'.urlHelper::makeLink('crmIntern', 'generateReminderLetters',
				$date2show, array('date' => $date2show)).' - Anzahl : <b>nicht gezählt</b></li>';
			}

		if($i == 4) {
			$t .= '</ul></div><div id="hid"><ul>';
		}
			
		}
	if(!empty($t)) return $t;
	}


	public function setStatus($id, $pid=null) {
		/* sets the Status for Reminderlines - req. by mw 30.05.2011 */
		if(empty($id) && empty($pid)) return;
		$db = "";

		if(!empty($id)) {
			$sql = "UPDATE `".$this->table('reminder')."` SET `status` = 'printed', `status_date` = '".time().
				"' WHERE `status` = 'open' AND `id` = ".$id;
		} else
		{
			if(is_array($pid)) {
				$komma = "";
				foreach($pid as $p) {
					$in .= $komma.$p;
					$komma =", ";
				}
			$pid = $in;
			}

			$sql = "UPDATE `".$this->table('reminder')."` SET `status` = 'printed', `status_date` = '".time().
				"' WHERE `status` = 'open' AND `pid` IN (".$pid.") ";
		}

		if(!empty($sql)) {
			$db = $this->getDb();
			$rs = $db->queryPDO($sql);
		}

	return $db->getAffectedRows();
	}

	public function checkReminderLetters($data)
	{
		$sql = "SELECT * FROM `".$this->table('reminder')."` WHERE 1=1 ";
		$sql .= " AND `hash` = '".$data."' ";
		$sql .= " LIMIT 0, 1 ";

		$db = $this->getDb();

		$rs = $db->queryPDO($sql);

		while($row = $db->fetchPDO($rs)) {
			return 1;
		}
		return $sql;
	}

	public function addData($data)
	{
		/* adds Data for reminder writing */
		$now = time();
		$db = $this->getDb();

		if(is_array($data))
		{
			if($this->checkReminderLetters($data['hash'])!=1)
			{
				if($this->debug == 'false')
				{
					// send automatically via mail
					$this->sendReminderByMail($data['pid']);


					echo "Datensatz : ".$data['forename'].", ".$data['surname'];
					$sql ="INSERT INTO `".$this->table('reminder').
						"` (`count`, `pid`, `forename`, `surname`, `birthdate`, `idco`, `sentDate`, `added_dte`) VALUES (1, '".$data['pid']."', '".addslashes($data['forename'])."', '".addslashes($data['surname'])."', '".$data['birthdate']."', '".$data['idco']."', '".$data['dateSent']."', '".date('Y-m-d', $now)."')";
					$rs = $db->queryPDO($sql);

					$this->updateReminderHashes();
				} else {
					print_r($data);
				}
			}
			//else print "-:: dublette gefunden ::-";
		}
	}

	/* Ermitteln des letzten eingetragenen Datums */
	public function getLastDate()
	{
		$retDate = date('Y-m-d', time());

		$sql = "SELECT max(added_dte) as lastDate from wzm_reminder limit 0,1;";

		$db = $this->getDb();
		$rs = $db->queryPDO($sql);

		$row = $db->fetchPDO($rs);
			$retDate = $row['lastDate'];
		return $retDate;
	}


	public function calcRemainDays($refDate)
	{
		// calculates days between both dates - if $toDate is null $toDate is today.
		$today = date('Y-m-d', time()); $today = explode('-', $today);
		$today = mktime(0,0,0, $today[1], $today[2], $today[0]);
		
		$refDate = mktime(0,0,0, $refDate[1], $refDate[2], $refDate[0]);

		$diff = $today - $refDate;


		$fulldays = $diff / (24*60*60);
		return $fulldays;
	}


	public function sendReminderByMail($pid)
	{
		$crm = new crm();
		$person = new person();

		$data['contact'] = $crm->getContactPrimary('pid', $pid);
		$data['person'] = $person->get($pid);


		// send user info mail
		$sender = 'Zahnzusatzversicherung-Experten <angebot@zahnzusatzversicherung-experten.de>';
		$from = 'angebot@zahnzusatzversicherung-experten.de';
		$recipient = $data['contact']['email'];
		#$recipient = 'anfrage@consulting-wirth.de';


		$subject = L::_(679);

		$salutation = ($data['person']['salutationLid']==9)?
			'Sehr geehrter Herr '.$data['person']['surname']:
			'Sehr geehrte Frau '.$data['person']['surname'];

		$file = file_get_contents($this->templ);
		if(!$file) 
			die('error: file not found in reminder.class.php');

		$message = str_replace('{*ANREDE_NAME*}', $salutation, $file);
		$message = str_replace('{*SIGNATURE*}', project::gts('html'), $message);

		emailHelper::sendMail($sender, $recipient, $subject, $message, 'utf8');
	}

	private function updateReminderHashes()
	{
		$db = $this->getDb();		
		$sql = "UPDATE wzm_reminder 
			SET `hash` = md5(CONCAT(forename, surname, cast(birthdate as char(10))))
			where (hash is null or hash = '');";

		$db->queryPDO($sql);
	}

	public function genAnschreibenErinnerung()
	{
		$days = project::getReminderDays();
		$person = new person();
		$lastDate = date('Y-m-d', time() - (24*60*60*$days));

		/* first update hashes */
		$this->updateReminderHashes();
		
		// Check whether Reminder was stored today or on $refDate
		if ($this->checkData() >0) // --> 1 = data was already stored
		{
			$ret['errmsg'] = "Daten wurden bereits erfasst - keine weitere Erfassung notwendig";
		}
		else
		{
			// check the last data in table
			$startDate = $this->getLastDate();
			

			if(empty($startDate))
			{
				$startDate = stringHelper::parseNcalcDate($lastDate, -5, 0, 0); 
			} else {
				$startDate = stringHelper::parseNcalcDate($startDate, -$days+1, 0, 0);
			}
			// Data was not yet added
			$ret = $person->getPersonsByPeriod($lastDate, $startDate);
			if(is_array($ret['result'])) {
				foreach($ret['result'] as $id)
				{
					// Data is complete - lets save it
					/* see $person [getPersonsByPeriod]
						returns array(pid, idco, dataSent, forename, surname, birthdate, hash)
					*/
					$this->addData($id);
				}
			}
		}

	return $ret;
	}


/* TEST */

	public function debugReminder()
	{
		print "<h1>Debug Status: " .$this->debug. "</h1>";
		$file = isset($this->templ)?file_get_contents($this->templ):null;
		if($file)
			return $file;
	}

	public function setDebug($status)
	{
		$this->debug = $status;
	}

	public function genAnschreibenErinnerungTest()
	{

		$days = project::getReminderDays();
		$person = new person();
		$lastDate = date('Y-m-d', time() - (24*60*60*$days));
		
		// Check whether Reminder was stored today or on $refDate
		if ($this->checkData() >0) // --> 1 = data was already stored
		{
			$ret['errmsg'] = "Daten wurden bereits erfasst - keine weitere Erfassung notwendig";
		}
		else
		{
			// check the last data in table
			$startDate = $this->getLastDate();
			

			if(empty($startDate))
			{
				$startDate = stringHelper::parseNcalcDate($lastDate, -5, 0, 0); 
			} else {
				$startDate = stringHelper::parseNcalcDate($startDate, -$days+1, 0, 0);
			}
			// Data was not yet added
			$ret = $person->getPersonsByPeriod($lastDate, $startDate);
		}


		// send user info mail
		$sender = 'Zahnzusatzversicherung-Experten <angebot@zahnzusatzversicherung-experten.de>';
		$from = 'angebot@zahnzusatzversicherung-experten.de';
		$recipient = 'radek.wirth@consulting-wirth.de';

		$data['person']['surname'] = 'Wirth';
		$data['person']['salutationLid'] = 9;


		$subject = L::_(679);

		$salutation = ($data['person']['salutationLid']==9)?
			'Sehr geehrter Herr '.$data['person']['surname']:
			'Sehr geehrte Frau '.$data['person']['surname'];

		$file = file_get_contents($this->templ);
		if(!$file) 
			die('error: file not found in reminder.class.php');

		$message = str_replace('{*ANREDE_NAME*}', $salutation, $file);
		$message = str_replace('{*SIGNATURE*}', project::gts('html'), $message);
		$message .= print_r($ret, true);
		
		emailHelper::sendMail($sender, $recipient, $subject, $message, 'utf8');

	}

	public function checkHash() {
		echo $this->checkReminderLetters('9a673beba98ac8e38431d6a7514b1a3a');
	}

}
?>
