<?php

class wzm {

private $db;

protected static function getDb() {
	#if(!$this->db)
	#	$this->db = project::getDb();
	return project::getDb();
}

protected static function getMainDb() {
	#if(!$this->db)
	#	$this->db = project::getDb();
	return project::getDb();
}

protected static function table($table) {
	$tables = array(
	'usage'			  => 'usage',
	'bonusbase' 			  => 'wzm_bonusbase',
	'benefit' 			  => 'wzm_benefit',
	'contract' 			  => 'wzm_contract',
	'dentist'			  => 'wzm_dentist',
	'details' 			  => 'wzm_details',
	'lead' 			  => 'wzm_lead',
	'reminder'  			  => 'wzm_reminder',
	'tariff' 			  => 'wzm_tariff',
	'insurance'  			  => 'wzm_insurance',
	'support' 			  => 'wzm_support',
	'tariff_switch'		  => 'wzm_tariff_switch',
	'person'			  => 'crm_person',
	'middleware'			  => 'zzv_middleware',
	'guideline'			  => 'wzm_tariff_guidelines',
	'highlights'			  => 'wzm_tariff_highlights',
	'config'			  => 'wzm_tariff_config',
	'campaign'			  => 'wzm_tariff_campaign',
	'tariff-benefits'    	  => 'wzm_tariff_benefits',
	'tariff-benefits-limitations' => 'wzm_tariff_benefits_limitations',
	'tariff-rating'		  => 'wzm_tariff_rating',
	'tariff-rating-grades'	  => 'wzm_tariff_rating_grades',
	'tariff-rating-extern'	  => 'wzm_tariff_rating_extern',
	'tariff-special-offer'	  => 'wzm_tariff_special_offer',
	'seo-meta'	  		  => 'wzm_seo_meta',
	'language'			  => 'wzm_language'
	);

	if(empty($tables[$table])) {
		return '**TABLE NOT FOUND**';
	}else {
		return $tables[$table];
	}
}
// -----------------------------------------------------------------------------
protected function _insert($table, $data) {
	$db = $this->getDb();
	$valuesSql = stringHelper::arrayToSql($data);
	$rs = $db->queryPDO("INSERT INTO ".$this->table($table).
		" SET ".$valuesSql);
	return $db->getLastInsertedId();
}

protected function _update($table, $data, $colid, $id, $limit=1) {
	$db = $this->getDb();
	$valuesSql = stringHelper::arrayToSql($data);
	$sqlstr = "UPDATE ".$this->table($table).
		" SET ".$valuesSql." WHERE ".$colid."='".$id."' LIMIT ".$limit;

	$rs = $db->queryPDO($sqlstr);
	#return $sqlstr;
}

protected function _insertOrUpdate($table, $data/*, $colid, $id*/) {
	$db = $this->getDb();
	$valuesSql = stringHelper::arrayToSql($data);
	$rs = $db->queryPDO("INSERT INTO `".$this->table($table).
		"` SET ".$valuesSql.
		" ON DUPLICATE KEY UPDATE ".$valuesSql);
}

protected function _select($table, $colid=null, $id=null, $oderby=null,
	$limit=null, $filterDate=null, $offset=0) {

	$db = $this->getDb();
	$sql = "SELECT * FROM ".$this->table($table)." WHERE 1=1 ";
	if(!empty($colid) && !empty($id)) {
		$sql .= "AND ".$colid."='".$id."'";
	}
	if (is_array($filterDate)) {
		// $filterDate ist ein Array -> von & bis Datum
		$sql .= "AND signupTime BETWEEN '".$filterDate['von']." 00:00:00' AND '".$filterDate['bis']." 23:59:59' ";
	}
	if(!empty($oderby)) {
		$sql .= " ORDER BY ".$oderby;
	}
	if(!empty($limit)) {
		$sql .= " LIMIT ".$offset." , ".$limit;
	}
	if(empty($limit)) {
		$sql .= " LIMIT ".($offset*50).", 50";
	}
	return $db->queryPDO($sql);
}

public function addUsage($class, $function)
{
	#$this->_insertOrUpdate('usage', array('class' => $class, 'func' => $function));
}


public function getTooltips()
{
	#$this->addUsage(get_class($this), 'getTooltips');

	$sql = "SELECT ident, attr1 FROM `wzm_language` where type1 = 'tooltip' and type2 = 'benefits' LIMIT 0, 50 ";
	$db = $this->getDb();

	$rs = $db->queryPDO($sql);

	while($row = $db->fetchPDO($rs)) {
		$result[$row['ident']] = $row['attr1'];
	}
	return $result;
}

public function getInsuranceHighlightsHTML($idt) {
	#$this->addUsage(get_class($this), 'getInsuranceHighlightsHTML');

	$c = new contract();
	$highlights = '';

	$highlightsFromDb = $c->getInsuranceHighlights($idt);

	if(isset($highlightsFromDb))
	{
		for($i=0;$i<count($highlightsFromDb);$i++)
		{		
			$highlights .= '<li>'.$highlightsFromDb[$i]['highlight_text'].'</li>';
		}
	}
	return $highlights;
}

private function getMaxAgeByIdt ($idt)
{
	#$this->addUsage(get_class($this), 'getMaxAgeByIdt');

	// used to get the maximum age by idt chosen
	// private - used by getBonusBaseHtmlTable()

	$db = $this->getDb();
	if(!isset($idt)) return 0;
	$sql = "SELECT max( age ) AS max FROM `wzm_bonusbase` WHERE CURDATE() >= validFrom and bonus>0 AND idt = ".$idt." ORDER BY age";

	$rs = $db->queryPDO($sql);
	$row = $db->fetchPDO($rs);

	return $row['max'];
}


protected function _count($table, $col, $filterDate=null, $status=null) {
// 46 = Antrag noch nicht rausgeschickt
// 48 = Antrag rausgeschickt
// 49 = Antrag zurück

	$db = $this->getDb();
	$sql = "SELECT count(".$col.") as result FROM ".$this->table($table)." WHERE 1=1 ";
	if (is_array($filterDate)) {
		// $filterDate ist ein Array -> von & bis Datum
		$sql .= " AND signupTime BETWEEN '".$filterDate['von']." 00:00:00' AND '".$filterDate['bis']." 23:59:59' ";
	}
	if (!empty($status)) {
		// Hier können die versendeten und auch erhaltenen Anträge ausgewertet werden
		$sql .= " AND `contractStatusLid` = ".$status;
	}
	return $db->queryPDO($sql);
}

public function realCount ($table, $col, $filterDate=null, $status=null) {
// 46 = Antrag noch nicht rausgeschickt
// 48 = Antrag rausgeschickt
// 49 = Antrag zurück

	#$this->addUsage(get_class($this), 'realCount');

	if (!empty($status)) {
		// Hier können die versendeten und auch erhaltenen Anträge ausgewertet werden
		$sqlStatus = " AND `contractStatusLid` = ".$status;
	}
	if (is_array($filterDate)) {
		// $filterDate ist ein Array -> von & bis Datum
		$sqlFilter = " AND signupTime BETWEEN '".$filterDate['von']." 00:00:00' AND '".$filterDate['bis']." 23:59:59' ";
	}

	$db = $this->getDb();
	$sql = "SELECT count('A') as result FROM ( ".
		"SELECT p.forename, p.surname, p.birthdate from ".$this->table($table)." p ".
		" INNER JOIN wzm_contract c ON c.pid = p.pid ".$sqlFilter.
		$sqlStatus." GROUP BY p.forename, p.surname, p.birthdate ) TMP ";

	return $db->queryPDO($sql);
}

public function getContractsWithPhone($filterdate, $offset) {

	#$this->addUsage(get_class($this), 'getContractsWithPhone');

	$db = $this->getDb();
	
	$sql = "SELECT c.* FROM wzm_contract c ".
		" join crm_contact_map map on map.pid = c.pid ".
		" join crm_contact con on con.ctid = map.ctid ".
		" WHERE 1=1 and ((con.phone is not null and LENGTH(con.phone) >0) or (con.mobile is not null and LENGTH(con.mobile)>0))  ";

	if (is_array($filterdate)) {
		// $filterDate ist ein Array -> von & bis Datum
		$sql .= "AND c.signupTime BETWEEN '".$filterdate['von']." 00:00:00' AND '".$filterdate['bis']." 23:59:59' ";
	}

	$sql .= "ORDER BY signupTime DESC ";

	if(!empty($limit)) {
		$sql .= " LIMIT ".$offset." , ".$limit;
	}
	if(empty($limit)) {
		$sql .= " LIMIT ".($offset*50).", 50";
	}

	return $db->queryPDO($sql);
}



public function duplicateDetails($from, $to) {
	#$this->addUsage(get_class($this), 'duplicateDetails');

	$db = $this->getDb();

	$sql = "INSERT INTO wzm_details (`ordr`, `type`, `idt`, `text`, `status`) ".
		"SELECT `ordr`, `type`, ".$to.", `text`, `status` FROM wzm_details WHERE `idt` = ".$from;
	return $db->queryPDO($sql);
}



// -----------------------------------------------------------------------------
public static function calcPrice($base, $bonus=0) {
	#$this->addUsage(get_class($this), 'calcPrice');

	$euro=0;

	if(is_array($base))
		$base = $base['activeBonusBase'];

	// is bonus % or euro cash
	if($bonus < 1) {
		$euro = $base * (1 + $bonus);
	}
	else {
		$euro = $base + $bonus;
	}
	return $euro;
}
// -----------------------------------------------------------------------------
public function calcPriceFormated($base, $bonus=0, $format=2) {
	#$this->addUsage(get_class($this), 'calcPriceFormated');
	$euro = $this->calcPrice($base, $bonus);
	$euroF = number_format($euro, $format, ',', '.').' &#8364;';

	return $euroF;
}
// -----------------------------------------------------------------------------
public function addBonusBase($data) {
	#$this->addUsage(get_class($this), 'addBonusBase');

	$db = $this->getDb();
	$valuesSql = stringHelper::arrayToSql($data);
	$rs = $db->queryPDO("INSERT INTO ".$this->table('bonusbase').
		" SET ".$valuesSql);
	return $db->getLastInsertedId();
}


/* getTariffGuidelines
** Genutzt durch die rest-Schnittstelle /rest
*/

public function getTariffGuidelines($idt=null) {
	#$this->addUsage(get_class($this), 'getTariffGuidelines');
	
	$sqlWhere = "";
	if(isset($idt))
	{
		
		if(is_numeric($idt))
			$sqlWhere = " AND tariff_id = ".$idt;
	} 

	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT * FROM wzm_tariff_guidelines WHERE 1=1 ".$sqlWhere);
	
	if(isset($idt))
	{
		$ret = $db->fetchPDO($rs);
	}
	else
	{
		while($row = $db->fetchPDO($rs)) {
			$ret[] = $row;
		}
	}
	return $ret;
}

public function getTariffAVBFile($idt) {

/*
	MT: https://www.meistertask.com/app/task/dp46ULGF/avb-dokumente-download

	Type :: wzm_filestore_document_type
		
		1 = AVB
		2 = Versicherungsbedingungen
		3 = Leistungsbeschreibung
		4 = Werbeprospekt
		5 = Beitragstabelle
		6 = Annahmerichtlinien
		7 = Antrag
		8 = Preis und Leistungsverzeichnis
		9 = Sonstige

	Link :: https://rest.zahnzusatzversicherung-experten.com/filestore/documents/
*/

	$ret = $this->getTariffFiles($idt, 1);
	
	return $ret;

}

public function getTariffLeistungsbeschreibungFile($idt) {

/*
	MT: https://www.meistertask.com/app/task/91Vvv9yQ/mail-anhange-aus-admin-doks

	Type :: wzm_filestore_document_type
		
		1 = AVB
		2 = Versicherungsbedingungen
		3 = Leistungsbeschreibung
		4 = Werbeprospekt
		5 = Beitragstabelle
		6 = Annahmerichtlinien
		7 = Antrag
		8 = Preis und Leistungsverzeichnis
		9 = Sonstige

	Link :: https://rest.zahnzusatzversicherung-experten.com/filestore/documents/
*/

	$ret = $this->getTariffFiles($idt, 3);
	
	return $ret;

}


public function getTariffFiles($idt=null, $type=null) {
	
/*
	MT: https://www.meistertask.com/app/task/dp46ULGF/avb-dokumente-download

	Type :: wzm_filestore_document_type

		1 = AVB
		2 = Versicherungsbedingungen
		3 = Leistungsbeschreibung
		4 = Werbeprospekt
		5 = Beitragstabelle
		6 = Annahmerichtlinien
		7 = Antrag
		8 = Preis und Leistungsverzeichnis
		9 = Sonstige

	Link :: https://rest.zahnzusatzversicherung-experten.com/filestore/documents/
*/



	$sqlWhere = "";
	$filePath = "https://rest.zahnzusatzversicherung-experten.com/filestore/documents/";
	if(isset($idt))
	{
		if(is_numeric($idt))
			$sqlWhere .= " AND idt = ".$idt;
	} 
	if(isset($type))
	{
		if(is_numeric($type))
			$sqlWhere .= " AND document_type_id = ".$type;
	} 


	$db = $this->getDb();





	$rs = $db->queryPDO("
		SELECT filename, document_type_id from wzm_filestore_tariff t0
   		   JOIN wzm_filestore t1 ON t1.id = t0.filestore_id
		   JOIN wzm_tariff t2 ON t2.idt = t0.tariff_id ".$sqlWhere);

	if(isset($idt))
	{
		while($row = $db->fetchPDO($rs)) {
			$ret['document_type_id'][] = $row['document_type_id'];
			$ret['file'][] = $filePath.$row['filename'];
		}
	}



	$rs = $db->queryPDO("
		SELECT filename, document_type_id from wzm_filestore_insurance t0
		   JOIN wzm_filestore t1 ON t1.id = t0.filestore_id
		   JOIN wzm_tariff t2 ON t2.insurance_id = t0.insurance_id ".$sqlWhere);

	
	if(isset($idt))
	{
       	while($row = $db->fetchPDO($rs)) {
			$ret['document_type_id'][] = $row['document_type_id'];
			$ret['file'][] = $filePath.$row['filename'];
		}
	}



	return $ret;
}



/*
* 13.05.2014 RW
*
* getAge
* - enhanced as it calculates the real age or by year depending on insurance settings
* - targetdate is used when the wishedDate is in future to calculate the correct age of customer
*

public function getAgeByIdt($idt, $birthdate, $targetdate=null)
{
	$tariff = $this->getTariff($idt);
	#$g = $this->getTariffGuidelines($idt);

	if($tariff['ageByYear'] == 1) {
		return stringHelper::calcAgeByYear($birthdate, $targetdate);
	} else return stringHelper::calcAge($birthdate, $targetdate);
}
*/
public function getAgeByIdt($idt, $birthdate, $targetdate=null)
{
	#$tariff = $this->getTariff($idt);
	$g = $this->getTariffGuidelines($idt);

	#7 = Geburtsjahr
	#8 = Reales Alter
	#9 = Reales Alter +1

	if($g['calculationStartingAgeLid'])
	{
		if((int)$g['calculationStartingAgeLid'] == 7) 
			return stringHelper::calcAgeByYear($birthdate, $targetdate);
		if((int)$g['calculationStartingAgeLid'] == 8) 
			return stringHelper::calcAge($birthdate, $targetdate);
		if((int)$g['calculationStartingAgeLid'] == 9) 
			return stringHelper::calcAge($birthdate, $targetdate) + 1;
	} else return -1;	
}



/*
**
** --15.11.2013 RW
** --29.11.2014 Anpassung - birthdate statt age! $wishedBegin ist im format time()
**
*/
public function getBonusBaseWithDate($idt, $birthdate, $wishedBegin, $gender='male') {

	/* this function shall be used to find out which date is currently active and if there is a change in future */

	$db = $this->getDb();
	$c = new contract();
	
	// first check, if wishedBegin is possible, e.g. Ergo does not accept late contracts
	if(stringHelper::findString($wishedBegin, '-') == true)
	{
		/* Format nicht in time() Format -> konvertierung */
		$wishedBegin = stringHelper::parseNcalcDate($wishedBegin, 0, 0, 0, true);
	}

	$wishedBeginReq = $wishedBegin;
	$wishedBegin = $c->checkWishedBegin($idt, $wishedBegin);


	$wishedBeginSql = date("Y-m-d", $wishedBegin);
	
	$where = '';
	// if no birthdate was set, we expect a list
	if(isset($birthdate)) 
	{
		$age = $this->getAgeByIdt($idt, $birthdate, $wishedBegin);
		$where .= " AND age='".$age."' ";	
	}	

	$sql = "
		SELECT description, age, bonus, vDate FROM (
		(SELECT 'activeDate' as Description, age, bonus, validFrom as vDate FROM ".$this->table('bonusbase')." WHERE idt='".$idt."' ".$where." AND gender='".$gender."' AND validFrom <= '".$wishedBeginSql."' ORDER BY validFrom DESC LIMIT 1)

		UNION ALL

		(SELECT 'nextDate', age, bonus, validFrom as vDate FROM ".$this->table('bonusbase')." WHERE idt='".$idt."' ".$where." AND gender='".$gender."' AND validFrom > '".$wishedBeginSql."' ORDER BY validFrom LIMIT 1 )
		) TMP WHERE vDate IS NOT NULL
	";

try {
	$rs = $db->queryPDO($sql);

	#$ret['num'] = $db->getNumRows($rs);
	
	if($wishedBeginReq != $wishedBegin) // show only if a difference
		$ret['wishedBeginRequested'] = $wishedBeginReq;
	$ret['wishedBegin'] = $wishedBegin;
	while($row = $db->fetchPDO($rs))
	{
		if($row['description'] == 'activeDate') {
			if($age==null) {
				$ret[$row['age']]['activeBonusBase'] = $row['bonus'];
				$ret[$row['age']]['activeBonusDate'] = $row['vDate'];
			} else {
				$ret['activeBonusBase'] = $row['bonus'];
				$ret['activeBonusDate'] = $row['vDate'];
			}
		}

		if($row['description'] == 'nextDate') {
			if($age==null) {
				$ret[$row['age']]['nextBonusBase'] = $row['bonus'];
				$ret[$row['age']]['nextBonusDate'] = $row['vDate'];
			} else {
				$ret['nextBonusBase'] = $row['bonus'];
				$ret['nextBonusDate'] = $row['vDate'];
			}
		}
	}

	// check date!
	/*
	if($ret['num'] == 2)
	{
			$date1 = new DateTime("now");
			$date2 = new DateTime($ret['nextBonusDate']);
			$int = date_diff($date1, $date2);

			if(($int < 30 && $int > 0 && (date('j') >= $switchDate['D'])) || ($ret['activeBonusBase'] == $ret['nextBonusBase']))
			{
				$ret['activeBonusBase'] = $ret['nextBonusBase'];
				unset($ret['nextBonusBase']);
				$ret['activeBonusDate'] = $ret['nextBonusDate'];
				unset($ret['nextBonusDate']);
				$ret['num']=1;
			}


	} */

} catch(Exception $e) {

}

	return $ret;
}


// -----------------------------------------------------------------------------
/*
public function getBonusBase($idt, $age, $gender='male') {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT bonus FROM ".$this->table('bonusbase').
		" WHERE 1=1 AND idt='".$idt."' AND age='".$age."' AND gender='".$gender."'");
	$row = $db->fetchPDO($rs);
	return $row['bonus'];
}
*/

// -----------------------------------------------------------------------------
public function getCountByAge($idt, $gender='male') {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT count('A') as cnt FROM (SELECT count(age) as grpage, bonus FROM ".$this->table('bonusbase').
		" WHERE 1=1 AND idt='".$idt."' AND gender='".$gender."' group by bonus) TMP ");

	$row = $db->fetchPDO($rs);
	return $row['cnt'];
}

public function getBonusBase($idt, $wishedBegin, $age=null, $gender='male', $limit=100) {
	$db = $this->getDb();
	
	$activeDate = $this->getActiveDate($idt, $wishedBegin);

	$sqlFilterAge = "";
	if(isset($age) && $age>0) $sqlFilterAge = " AND age >= ".$age." ";

	// Gruppierte Ansicht
	$sql = "SELECT age, bonus FROM `wzm_bonusbase` WHERE bonus>0 AND validFrom = '".$activeDate."' AND idt = ".$idt.$sqlFilterAge.
			" ORDER BY age LIMIT ".$limit;


	$rs = $db->queryPDO($sql);

	while ($row = $db->fetchPDO($rs)) {
		$rows[$row['age']] = $row['bonus'];
	}
	return $rows;
}




public function getBonusBaseTable($idt, $wishedBegin, $age=null, $gender='male', $limit=100) {
	// In order to get the BonusBaseData in a grouped format this function shall be used.
	$db = $this->getDb();

	$activeDate = $this->getActiveDate($idt, $wishedBegin);
	#$idtCount = $this->getCountByIdt($idt, $activeDate);

	$sqlFilterAge = "";
	if(isset($age) && $age>0) $sqlFilterAge = " AND age >= ".$age." ";

	// Gruppierte Ansicht
	$sql = "SELECT min( age ) AS min, max( age ) AS max, gender, bonus FROM `wzm_bonusbase` WHERE bonus>0 AND validFrom = '".$activeDate."' AND idt = ".$idt.$sqlFilterAge.
			" GROUP BY gender, bonus ORDER BY age, gender LIMIT ".$limit;

	$rs = $db->queryPDO($sql);

	while ($row = $db->fetchPDO($rs)) {
		$rows[] = $row;
	}
	return $rows;
}

private function getActiveDate($idt, $wishedBegin)
{
	// first check, if wishedBegin is possible, e.g. Ergo does not accept late contracts
	if(stringHelper::findString($wishedBegin, '-') == true)
	{
		/* Format nicht in time() Format -> konvertierung */
		$wishedBegin = stringHelper::parseNcalcDate($wishedBegin, 0, 0, 0, true);
	}

	$wishedBeginReq = $wishedBegin;
	$wishedBegin = contract::checkWishedBegin($idt, $wishedBegin);

	$wishedBeginSql = date("Y-m-d", $wishedBegin);

	$db = $this->getDb();
	$sql = "SELECT validFrom FROM ".$this->table('bonusbase')." WHERE idt = '".$idt."' AND gender = 'male' AND validFrom <= '".$wishedBeginSql."' ORDER BY validFrom DESC LIMIT 1";

	$rs = $db->queryPDO($sql);
	$row = $db->fetchPDO($rs);
	return $row['validFrom'];
}

private function getAgeningReserves($id)
{
	if(is_numeric($id))
	{
		if($id == 1)
			return "stabil";
		if($id == 0)
			return "steigend";
	}
}

private function getSchoolMarks($id)
{
	if(is_numeric($id))
	{
		if($id == 0)
			return "k.A.";
		if($id < 1.5)
			return "<strong>Sehr gut</strong>";
		if($id < 2.5)
			return "Gut";
		if($id < 3.5)
			return "Befriedigend";
		if($id < 4.5)
			return "Ausreichend";
		if($id < 5.5)
			return "Mangelhaft";
		if($id < 6.0)
			return "Ungenügend";
		if($id = 6.0)
			return "keine Leistung";

	}
}

private function getGradeColors($id)
{
	if(is_numeric($id))
	{
		if($id == 0)
			return "<i class='fa fa-times-circle color-info'></i>";
		if($id < 1.5)
			return "<i class='fa fa-check-circle color-success'></i>";
		if($id < 2.5)
			return "<i class='fa fa-check-circle color-success'></i>";
		if($id < 3.5)
			return "<i class='fa fa-check-circle color-warning'></i>";
		if($id < 4.5)
			return "<i class='fa fa-check-circle color-warning'></i>";
		if($id < 5.5)
			return "<i class='fa fa-check-circle color-warning'></i>";
		if($id < 6.0)
			return "<i class='fa fa-check-circle color-warning'></i>";
		if($id == 6.0)
			return "<i class='fa fa-times-circle color-alert'></i>";
	}
	
}



public function getRating($idt, $series=0)
{
	$db = $this->getDb();
	$sql = "SELECT `status`, `type`, `hasAgeningReserves`, `denturesWithoutPayment`, `fixedAllowanceTariffFixedAllowanceBenefit`, `fixedAllowanceTariffInlayBenefit`, `bonusBenefitsInlays`, `boneBuilding`, `functionalTherapy`, `benefitsWithoutPayment`, `implantsLimit`, `deductionMissingBenefitImplant`, `veneersLimit`, `materialcostList`, `prophylaxisPotentialAmountPa`, `prophylaxisPotentialNumberPa`, `maxBenefitsPercent`, `syntheticFillings`, `fissures`, `fissuresLimit`, `childProphylaxis`, `childProphylaxisLimit`, `syntheticFillingsDeductionGKV`, `rootTreatmentWithPayment`, `rootTreatmentWithPaymentDeductionGKV`, `rootTreatmentWithoutPayment`, `periodontitisTreatmentWithPayment`, `periodontitisTreatmentDeductionGKV`, `periodontitisTreatmentWithoutPayment`, `noWaitingTime`, `limitDentures`, `kig2pilotCase`, `acceptanceForKig2`, `kig35additionalCostInTerms`, `kig35benefitsInPilotCase`, `kig35PriceList`, `waitingPeriodRules`, `orthodonticsLimits`, `deductionSeries1`, `deductionSeries2`, `sumDentures`, `sumProphylaxis`, `sumProphylaxisAndDentalTreatment`, `sumOrthodontics`, `sumDentalTreatment`, `sumWaitingPeriod`, `sumAdult`, `sumAdultSeries1`, `sumAdultSeries2`, `sumChild` 
			FROM ".$this->table('tariff-rating')." tr JOIN ".$this->table('tariff-benefits')." tb on tr.tariff_id = tb.tariff_id WHERE tr.tariff_id = '".$idt."' ";

	$rs = $db->queryPDO($sql);
	while ($row = $db->fetchPDO($rs)) {
		$rows['intern'][$row['type']] = $row;
	}

	$sql = "SELECT `tariff_id`, `status`, `financetest`, `imageName`, `oekotest`, `focusMoney`, `euro`, `morgenAndMorgen`, `disq`, `ralLicenseNumber`, `date`, `dateValidUntil`, `updated_at`, `created_at` FROM ".$this->table('tariff-rating-extern')." WHERE tariff_id = '".$idt."' ";
	$rs = $db->queryPDO($sql);
	while ($row = $db->fetchPDO($rs)) {
		$rows['extern'][] = $row;
	}
	
	if(isset($rows['intern']['adult']))
	{
		$internRatingFactors = array(
			'dentures' => 2,
			'dentalCleaning' => 1,
			'dentalTreatment' => 1,
			'waitingPeriod' => 1
		);

		$i = 0;
		
		switch($series)
		{
			case 0:
				$rows['intern']['adult']['sumAdultAll'] = $rows['intern']['adult']['sumAdult'];
				$rows['intern']['adult']['gradeAdultAll'] = sprintf("%01.1f", round($this->getRatingGrade($rows['intern']['adult']['sumAdultAll'] / 5), 1));
				$rows['intern']['adult']['limitDentures'] = $rows['intern']['adult']['limitDentures'];
				break;
			case 1: 
				$rows['intern']['adult']['sumAdultAll'] = $rows['intern']['adult']['sumAdultSeries1'];
				$rows['intern']['adult']['gradeAdultAll'] = sprintf("%01.1f", round($this->getRatingGrade($rows['intern']['adult']['sumAdultSeries1'] / 5), 1));
				$rows['intern']['adult']['limitDentures'] = $rows['intern']['adult']['limitDentures'] + $rows['intern']['adult']['deductionSeries1'];
				break;
			case 2:
				$rows['intern']['adult']['sumAdultAll'] = $rows['intern']['adult']['sumAdultSeries2'];
				$rows['intern']['adult']['gradeAdultAll'] = sprintf("%01.1f", round($this->getRatingGrade($rows['intern']['adult']['sumAdultSeries2'] / 5), 1));
				$rows['intern']['adult']['limitDentures'] = $rows['intern']['adult']['limitDentures'] + $rows['intern']['adult']['deductionSeries2'];
				break;
		}

		foreach(array('sumAdultSeries2', 'sumAdultSeries1', 'sumAdult', 'deductionSeries1', 'deductionSeries2') as $id => $val)
		{
			unset($rows['intern']['adult'][$val]);
		}

		if(isset($rows['intern']['adult']['sumDentures']))
		{
			$grades['dentures'] = sprintf("%01.1f", round($this->getRatingGrade($rows['intern']['adult']['sumDentures'] / $internRatingFactors['dentures']), 1));
			$i++; $i++;

			switch($series)
			{
				case 0:
					$rows['intern']['adult']['sumAdultDentures'] = $rows['intern']['adult']['sumDentures'] * 2 + $rows['intern']['adult']['sumWaitingPeriod'];
					break;
				case 1:
					$rows['intern']['adult']['sumAdultDentures'] = $rows['intern']['adult']['sumDentures'] * 2 + $rows['intern']['adult']['sumWaitingPeriod'] + $rows['intern']['adult']['deductionSeries1'];
					break;
				case 2:
					$rows['intern']['adult']['sumAdultDentures'] = $rows['intern']['adult']['sumDentures'] * 2 + $rows['intern']['adult']['sumWaitingPeriod'] + $rows['intern']['adult']['deductionSeries1'];
					break;
			}

			$rows['intern']['adult']['gradeAdultDentures'] = sprintf("%01.1f", round($this->getRatingGrade($rows['intern']['adult']['sumAdultDentures'] / 5), 1));
		}

		if(isset($rows['intern']['adult']['sumDentalTreatment']))
		{
			$grades['dentalTreatment'] = sprintf("%01.1f", round($this->getRatingGrade($rows['intern']['adult']['sumDentalTreatment'] / $internRatingFactors['dentalTreatment']), 1));
			$i++;
		}

		if(isset($rows['intern']['adult']['sumProphylaxis']))
		{
			$grades['dentalCleaning'] = sprintf("%01.1f", round($this->getRatingGrade($rows['intern']['adult']['sumProphylaxis'] / $internRatingFactors['dentalCleaning']), 1));
			$i++;
		}

		if(isset($rows['intern']['adult']['sumWaitingPeriod']))
		{
			$grades['waitingPeriod'] = sprintf("%01.1f", round($this->getRatingGrade($rows['intern']['adult']['sumWaitingPeriod'] / $internRatingFactors['waitingPeriod']), 1));
			$i++;
		}

		if(isset($grades))
		{
			$grades['overall'] = round(( ($grades['dentures'] * $internRatingFactors['dentures']) + ($grades['dentalTreatment'] * $internRatingFactors['dentalTreatment']) + ($grades['dentalCleaning'] * $internRatingFactors['dentalCleaning']) + ($grades['waitingPeriod'] * $internRatingFactors['waitingPeriod'])) / $i, 1);

			// hasAgeningReserves
			$grades['hasAgeningReserves'] = $rows['intern']['adult']['hasAgeningReserves'];
			$rows['intern']['adult']['grades'] = $grades;

			foreach($grades as $grade => $id)
			{
				if($grade == 'hasAgeningReserves')
				{
					$gradesText[$grade] = $this->getAgeningReserves($id);
				} else {
					$gradesText[$grade] = $this->getSchoolMarks($id);
				}
			
				$gradesIcon[$grade] = $this->getGradeColors($id);

				if(!isset($gradesIcon[$grade])) 
					$gradesIcon[$grade] = "<i class='fa fa-times-circle color-alert'></i>";
			}
			$rows['intern']['adult']['gradesText'] = $gradesText;
			$rows['intern']['adult']['gradesIcon'] = $gradesIcon;

		}
	}

	if(isset($rows['intern']['child']))
	{
/*
-sumDentures : 11.55 (max 12)
-sumProphylaxisAndDentalTreatment: 44 (max 48)
-sumProphylaxis : 21 (max 24)
-sumDentalTreatment : 22 (max 24)
-sumOrthodontics: 158 (max 168)
-sumWaitingPeriod: 12 (max 12)
*/
		unset($grades);
		$internRatingFactors = array(
			'dentures' => 1,
			'dentalTreatment' => 2,
			'prophylaxis' => 2,
			'orthodontics' => 14,
			'waitingPeriod' => 1
		);


		if($rows['intern']['child']['sumDentures'])
		{
			$grades['dentures'] = sprintf("%01.1f", round($this->getRatingGrade($rows['intern']['child']['sumDentures'] / $internRatingFactors['dentures']), 1));
		}

		if($rows['intern']['child']['sumDentalTreatment'])
		{
			$grades['dentalTreatment'] = sprintf("%01.1f", round($this->getRatingGrade($rows['intern']['child']['sumDentalTreatment'] / $internRatingFactors['dentalTreatment']), 1));
		}

		if($rows['intern']['child']['sumProphylaxis'])
		{
			$grades['dentalCleaning'] = sprintf("%01.1f", round($this->getRatingGrade($rows['intern']['child']['sumProphylaxis'] / $internRatingFactors['prophylaxis']), 1));
		}

		if($rows['intern']['child']['sumOrthodontics'])
		{
			$grades['orthodontics'] = sprintf("%01.1f", round($this->getRatingGrade($rows['intern']['child']['sumOrthodontics'] / $internRatingFactors['orthodontics']), 1));
		}

		if($rows['intern']['child']['sumWaitingPeriod'])
		{
			$grades['waitingPeriod'] = sprintf("%01.1f", round($this->getRatingGrade($rows['intern']['child']['sumWaitingPeriod'] / $internRatingFactors['waitingPeriod']), 1));
		}

		if(isset($grades))
		{
			$grades['overall'] = sprintf("%01.1f", round($this->getRatingGrade(($rows['intern']['child']['sumDentures'] + $rows['intern']['child']['sumProphylaxisAndDentalTreatment'] + $rows['intern']['child']['sumOrthodontics'] + $rows['intern']['child']['sumWaitingPeriod']) / array_sum($internRatingFactors)), 1));
			$rows['intern']['child']['grades'] = $grades;

			foreach($grades as $grade => $id)
			{
				$gradesText[$grade] = $this->getSchoolMarks($id);
				$gradesIcon[$grade] = $this->getGradeColors($id);

				if(!isset($gradesIcon[$grade])) 
					$gradesIcon[$grade] = "<i class='fa fa-times-circle color-alert'></i>";
			}
			$rows['intern']['child']['gradesText'] = $gradesText;
			$rows['intern']['child']['gradesIcon'] = $gradesIcon;

		}	
		
       }



	
	return $rows;
}


private function getRatingGrade($factor)
{
	$db = $this->getDb();
	$sql = "SELECT grade FROM ".$this->table('tariff-rating-grades')." where factor <= ". $factor ." order by grade ASC limit 1";
		$rs = $db->queryPDO($sql);
		$row = $db->fetchPDO($rs);
	
	
	return $row['grade'];
}


public function addSpecialOffer($data)
{
	if($data['idco'])
	{
		$db = $this->getDb();
		$sql = "INSERT INTO ".$this->table('tariff-special-offer')."  SET idco = ".$data['idco'].", offer = ".$data['pdfTemplate'];
		$rs = $db->queryPDO($sql);
	}
}

// -----------------------------------------------------------------------------
// obsolete
//

public function _getBonusBaseTable($idt, $age=null, $gender='male', $limit=100) {
	// In order to get the BonusBaseData in a grouped format this function shall be used.
	$db = $this->getDb();

	if(isset($age) && $age>0) $agE = " AND age >= ".$age." ";

	$sql = "SELECT min( age ) AS min, max( age ) AS max, gender, bonus FROM `wzm_bonusbase` WHERE 1=1 AND bonus>0 AND idt = ".$idt.$gen.$agE.
			" GROUP BY gender, bonus ORDER BY age, gender LIMIT ".$limit;

	if($this->getCountByAge($idt, $gender)>12);
	$sql = "SELECT age AS min, age AS max, gender, bonus FROM `wzm_bonusbase` WHERE 1=1 AND bonus>0 AND idt = ".$idt.$gen.$agE.
			" GROUP BY gender, bonus ORDER BY age, gender LIMIT ".$limit;

	$rs = $db->queryPDO($sql);

	while ($row = $db->fetchPDO($rs)) {
		$rows[] = $row;
	}
	return $rows;
}




// -----------------------------------------------------------------------------
// Shall use the function getBonusBaseWithDate to retrieve the correct data
//

public function getBonusBaseHtmlTable($idt, $wishedBegin, $age=null, $gender=null) {
	$data = $this->getBonusBaseTable($idt, $wishedBegin, null, $gender);
	$iCount = 0;

	$tblBreak = 17;

	//check how many fields we have
	#if(count($data)>30 && $age>0) {
	#	if(($this->getMaxAgeByIdt($idt)-$age) > ($tblBreak*2))
	#	{
	#		$data = $this->getBonusBaseTable($idt, $age, $gender, $tblBreak*2);
	#	}
	#}
	$css = ''; $mod = '';
	if($data) {
		if(count($data)/$tblBreak > 3) $css = "wide";
	}

	if($data) {
	$html = "<div class='bb_data $css'><table><tbody><th>Alter</th><th>Beitrag</th>";
		foreach($data as $line => $content)
		{
		$html .= "<tr>";
		$iCount++;
		#($iCount % 2==0)?$mod=' even':$mod='';

		$age = $content['max']-$content['min']==0?$content['max']:$content['min']."-".$content['max'];
		$html .= "<td class='age".$mod."'>".$age."</td><td>".number_format($content['bonus'], 2, ',', '.').' &euro;'."</td>";
		$html .= "</tr>";
		if(is_integer($iCount / $tblBreak) && $iCount!=count($data)) $html .= "</tbody><tbody><th>Alter</th><th>Beitrag</th>";
		}
	$html .= "</tbody></table></div>";
	}
	if($html) return $html;
}

public function isCalendarYearTariff($idt) {
	$db = $this->getDb();
	if(!isset($idt)) return 0;
	$sql = "SELECT timeUnitLimitationsLid FROM wzm_tariff_benefits WHERE tariff_id =". $idt;
	$rs = $db->queryPDO($sql);
	$row = $db->fetchPDO($rs);

	if ( ! empty($row['timeUnitLimitationsLid']))
	{
		// 4 = Kalenderjahr
		if ($row['timeUnitLimitationsLid'] == 4)
		{
			return true;
		} else {
			return false;
		}
	}
	return true;
}

public function isCalendarYearEndTariff($idt) {
	$db = $this->getDb();
	if(!isset($idt)) return 0;
	$sql = "SELECT idt FROM ".$this->table('tariff')." WHERE insurance_id = 6 /* Die Bayerische */ and idt =". $idt;
	$rs = $db->queryPDO($sql);
	$row = $db->fetchPDO($rs);

	if ( ! empty($row['idt']))
	{
		return true;
	}
	return false;
}

// -----------------------------------------------------------------------------
public static function old_getTariff($idt) {
	$db = self::getDb();
	$rs = $db->queryPDO("SELECT * FROM ".self::table('tariff')." WHERE idt='".$idt."'");
	return $db->fetchPDO($rs);
}

public static function getTariffBenefitsInline($idt) {
	$db = self::getDb();
	$rs = $db->queryPDO("
		SELECT b.* FROM ".self::table('tariff-benefits')." b where b.tariff_id = '".$idt."'
	");
return $db->fetchPDO($rs);
}

public static function getTariffBenefitsLimitationsInline($idt) {
	$db = self::getDb();
	$rs = $db->queryPDO("
		SELECT bl.* FROM ".self::table('tariff-benefits-limitations')." bl where bl.tariff_id = '".$idt."'
	");

	while($row = $db->fetchPDO($rs))
	{
		$ret[] = $row;
	} 
	return $ret;
}


public static function getTariffBenefits($idt) {
	$db = self::getDb();
	$rs = $db->queryPDO("SELECT i.ident as insurance, tre.financetest, t.*, b.*, bl.*, g.*, c.* FROM ".self::table('tariff'). 
		" t LEFT JOIN ".self::table('tariff-benefits').
		" b on b.tariff_id = t.idt LEFT JOIN ".self::table('tariff-benefits-limitations').
		" bl on bl.tariff_id = t.idt LEFT JOIN ".self::table('guideline').
		" g on g.tariff_id = t.idt LEFT JOIN ".self::table('config').
		" c on c.tariff_id = t.idt LEFT JOIN ".self::table('insurance').
		" i on i.id = t.insurance_id LEFT JOIN ".self::table('tariff-rating-extern').
		" tre on tre.tariff_id = t.idt and tre.financetest is not null WHERE idt='".$idt."'");

	return $db->fetchPDO($rs);
}


public static function getTariffInsurance($id) {
	$db = self::getDb();
	$rs = $db->queryPDO("SELECT * FROM ".self::table('insurance')." WHERE id ='".$id."'");

	return $db->fetchPDO($rs);
}


public static function getTariff($idt) {
	$db = self::getDb();
	$rs = $db->queryPDO("SELECT t.*, ca.campaign, ca.campaignHeader, ca.campaignHelpText, c.onlineContractUrl FROM ".self::table('tariff').
		" t left join ".self::table('config')." c on c.tariff_id = t.idt ".
		" left join ".self::table('campaign')." ca on ca.tariff_id = t.idt and ca.campaignStart <= now() and ca.campaignEnd >= now() and ca.campaign = 'Y' WHERE t.idt='".$idt."'");
	return $db->fetchPDO($rs);
}

public function getActiveCrmTariffs() {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT * FROM ".$this->table('tariff')." WHERE activeInCrm='Y' order by name");
	while($row = $db->fetchPDO($rs))
	{
		$ret[] = $row;
	} 
	return $ret;
}

public function getActiveTariffs() {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT idt, name, tariffShortName FROM ".$this->table('tariff')." WHERE isActive=1 order by name");
	while($row = $db->fetchPDO($rs))
	{
		$ret[] = $row;
	} 
	return $ret;
}

public function getTariffConfig($idt=null) {
	$db = $this->getDb();

	$sqladd = "";
	if($idt)
		$sqladd = " AND tariff_id = ".$idt;

	$rs = $db->queryPDO("SELECT * FROM ".$this->table('config')." WHERE 1=1 ".$sqladd." order by tariff_id");
	while($row = $db->fetchPDO($rs))
	{
		$ret[] = $row;
	} 
	return $ret;
}

public function getTariffsWithValidDetails() {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT idt FROM wzm_details group by idt having count(idt)=76 ");

	if($rs !== NULL) {
		while($row = $db->fetchPDO($rs))
		{
			$ret[] = $row;
		} 
	}
	return $ret;
}
// -----------------------------------------------------------------------------
public function getInsuranceId($idt) {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT insurance_id FROM ".$this->table('tariff')." WHERE idt='".$idt."'");
	$array = $db->fetchPDO($rs);
	return $array['insurance_id'];
}
// -----------------------------------------------------------------------------
public function getInsuranceByTariffId($idt) {
	$db = $this->getDb();
	$insuranceId = $this->getInsuranceId($idt);
	$rs = $db->queryPDO("SELECT * FROM ". $this->table('insurance') ." WHERE id='". $insuranceId ."'");
	return $db->fetchPDO($rs);
}
// -----------------------------------------------------------------------------
public function getBenefits($selTariffs, $limit) {
	$db = $this->getDb();
	$where = '';
	if(count($selTariffs))
	foreach($selTariffs as $idt) {
		if(!empty($where)) {
		$where .= ' OR ';
		}
		$where .= " idt='".$idt."'";
	}
	$ret = array();
	$rs = $db->query("SELECT * FROM ".$this->table('benefit')." WHERE".$where);
	while($row = $db->fetchPDO($rs)) {
		$ret[] = $row;
	}
	return $ret;
}
// -----------------------------------------------------------------------------
public function getDetails($idt) {
	$db = $this->getDb();
	$where = '';


	$sql = 'SELECT * FROM '.$this->table("details").' WHERE status="active" AND ';

	// first generate header data
	$hrs = $db->queryPDO($sql." type IN ('l', 'hl') ORDER BY ORDR");

	while($hrsdata = $db->fetchPDO($hrs)) {
		$header[] = $hrsdata;
	}

	if(is_array($idt)) {
		$where = stringHelper::arrayToSql($idt);
		$sql .= $where;
	} else $sql .= " idt = ".$idt;
	$sql .= " ORDER BY ORDR";

	// generate data
	$rs = $db->queryPDO($sql);
	while($rsdata = $db->fetchPDO($rs)) {
		$data[] = $rsdata;
	}

	return array('header' => $header, 'data' => $data);
}
// -----------------------------------------------------------------------------
public function addDentist($data) {
	$db = $this->getDb();
	$valuesSql = stringHelper::arrayToSql($data);
	$rs = $db->queryPDO("INSERT INTO ".$this->table('dentist').
		" SET ".$valuesSql);
	return $db->getLastInsertedId();
}


public function getDentists() {
	return $this->_select('dentist', null, null, 'idd');
}
// -----------------------------------------------------------------------------
public function addLead($data) {
	$db = $this->getDb();
	$valuesSql = stringHelper::arrayToSql($data);
	$rs = $db->queryPDO("INSERT INTO ".$this->table('lead').
		" SET ".$valuesSql);
	return $db->getLastInsertedId();
}

public function getLead($pid) {
	$db = $this->getDb();
	$rs = $this->_select('lead', 'pid', $pid);
	return $db->fetchPDO($rs);
}

public function getLeadStats() {
	$db = $this->getDb();
	$db2 = $this->getDb();
	$label = new label();
	$ret = array();
	$rs = $db->queryPDO("SELECT DISTINCT leadLid FROM ".$this->table('lead'));
	while($row = $db->fetchPDO($rs)) {
		$rs2 = $db2->queryPDO("SELECT COUNT(*) AS count FROM ".
			$this->table('lead')." WHERE leadLid='".$row['leadLid']."'");
		$row2 = $db->fetchPDO($rs2);
		if(empty($row['leadLid'])) {
			$name = L::_(356);
		}
		else {
			$name = $label->getLabelName($row['leadLid']);
		}
		$ret[] = array('leadLid' => $row['leadLid'],
			'name' => $name,
			'count' => $row2['count']);
	}
	return $ret;
}
// -----------------------------------------------------------------------------
public function addContract($data) {
	$db = $this->getDb();
	$data['signupTime'] = date('Y-m-d H:i:s');
	$valuesSql = stringHelper::arrayToSql($data);
	$rs = $db->queryPDO("INSERT INTO ".$this->table('contract').
		" SET ".$valuesSql);
	return $db->getLastInsertedId();
}

// -----------------------------------------------------------------------------
public function getContract($idco) {
	$db = $this->getDb();
	$rs = $this->_select('contract', 'idco', $idco);
	$ret = $db->fetchPDO($rs);
	return $ret;
}

// -----------------------------------------------------------------------------
public function addSupportMessage($message, $pid) {
	$db = $this->getDb();
	$data = array();
	$data['pid'] = $pid;
	$data['message'] = $message;
	$data['datetime'] = date('Y-m-d H:i:s');
	$valuesSql = stringHelper::arrayToSql($data);
	$rs = $db->queryPDO("INSERT INTO ".$this->table('support').
		" SET ".$valuesSql);
	return $db->getLastInsertedId();
}

public function duplicateSupportMessageByPid($pid, $new_pid)
{
	$db = $this->getDb();
	$rs = $this->getSupportMessages($pid);
	
	$data = array();

	while($row = $db->fetchPDO($rs))
	{
		$data = $row;
		$data['pid'] = $new_pid;
		unset($data['ids']);

		$this->insertData('support', $data);
	}
}


public function getSupportMessages($pid) {
	return $this->_select('support', 'pid', $pid);
}
// -----------------------------------------------------------------------------

public function amendDetails($text, $colid, $id)
{
	$this->_update('details', $text, $colid, $id);
}

// -----------------------------------------------------------------------------
public function getCssSwitchIdco($limit)
{
	$db = $this->getDb();

	$sql = "select c.*, p.salutationLid, p.forename, p.surname, p.birthdate as b2, p.isFatherOfPid from wzm_contract c
		inner join crm_person p on c.pid = p.pid
		where (c.idt = 1 or c.idt = 38) and (c.dateRec <='2014-01-01' or c.datePolice <= '2014-01-01') AND  c.contractStatusLid in (49, 50) and c.birthdate != '0000-00-00'
		and c.idco not in (SELECT DISTINCT idco FROM wzm_tariff_switch) LIMIT ".$limit;

	$rs = $db->queryPDO($sql);

	return $rs;
	
}

public function getBonus($idt, $tooth1, $tooth2=0, $tooth3=0, $tooth4=0, $tooth5=0) {
	$bonus = 0;
	return $bonus;
}

// -----------------------------------------------------------------------------
public function insertData($table, $data) {
	return $this->_insert($table, $data);
}
// -----------------------------------------------------------------------------
public function updateStatus($status, $where) {
	$this->_update('details', $status, 'status', $where, 5000);
}

} // end class

