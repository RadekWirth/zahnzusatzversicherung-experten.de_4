<?php

class modules {

protected $input;
protected $calculation;
protected $activeIdt;
protected $tariff;
protected $tariffRating;
protected $tariffBenefits;
protected $tariffGuideline;
protected $tariffConfig;
protected $age;
protected $bonusbase;
protected $surplus;
protected $surplusPercentage;
protected $surplusAmount;

public function __construct($args)
{
	if($args)
	{
		$this->input = $args;
		$this->surplusPercentage = 0;
	}
}


public function getBenefitsData($idt)
{
	$benefits = new benefits($input['series']);
	$b = $benefits->getTariffBenefitsInline($idt);

	$tl = $benefits->getTariffBenefitsLimitationsInline($idt);

	$b['limitations'] = $tl;
	return $b;
}


public function getTariffAVBFile($idt)
{
	$c = new contract();

	# TariffAVBDocuments
	return $c->getTariffAVBFile($idt);
	#return 1;
}


public function getComparisonData()
{
	$benefits = new benefits(0);
	$t1 = $benefits->getTariffBenefits($this->input['idt']);



	$comparison[$this->input['idt']] = 0;

	$onlineLinkUrl = $t1['isOnlineContractPossible']==1 && isset($t1['onlineContractUrl'])?$t1['onlineContractUrl']:'';
	$pathToLogo = '';

	if(isset($t1['financetest']) ) 
	{
		switch(round($t1['financetest']-0.01, 0))
		{
			case 0:
				$financetest_text = 'k.A.';
				break;
			case 1:
				$financetest_text = 'Sehr Gut';
				break;
			case 2:
				$financetest_text = 'Gut';
				break;
			case 3:
				$financetest_text = 'Befriedigend';
				break;
			case 4:
				$financetest_text = 'Ausreichend';
				break;
		}
		if($t1['financetest'] == 0.5)
			$financetest_text = 'Sehr Gut';
	}

	/* 1 */
	$c1 = array('idt' => $this->input['idt'], 'insurance' => utf8_encode($t1['insurance']), 'status' => ($t1['isActive']==1?'active':'not active'), 'tarif' => utf8_encode($t1['name']), 'tarif-logo' => $pathToLogo, 'tarif-name' => utf8_encode($t1['tariffName']), 'online-link' => $onlineLinkUrl, 'financetest' => $t1['financetest'], 'financetest-text' => $financetest_text, 'hasAgeningReserves' => $t1['hasAgeningReserves']);


	/* 2 */
	$implantants = 0; $crownsBridges = 0;
	if(isset($t1['implantsWithoutBonusInPercent']) && isset($t1['implantsWith10YearsBonusInPercent']))
		$implantants = $t1['implantsWithoutBonusInPercent'] == $t1['implantsWith10YearsBonusInPercent']?round($t1['implantsWithoutBonusInPercent'], 0):round($t1['implantsWithoutBonusInPercent'],0).' - '.round($t1['implantsWith10YearsBonusInPercent'],0); 

	if(isset($t1['inlaysWithoutBonusInPercent']) && isset($t1['inlaysWith10YearsBonusInPercent']))
		$inlays = $t1['inlaysWithoutBonusInPercent'] == $t1['inlaysWith10YearsBonusInPercent']?round($t1['inlaysWithoutBonusInPercent'],0):round($t1['inlaysWithoutBonusInPercent'],0).' - '.round($t1['inlaysWith10YearsBonusInPercent'],0);

	if(isset($t1['regularCareWithoutBonusInPercent']) && isset($t1['regularCareWith10YearsBonusInPercent']))
		$regularCare = $t1['regularCareWithoutBonusInPercent'] == $t1['regularCareWith10YearsBonusInPercent']?round($t1['regularCareWithoutBonusInPercent'],0):round($t1['regularCareWithoutBonusInPercent'],0).' - '.round($t1['regularCareWith10YearsBonusInPercent'],0);

	if(isset($t1['crownsBridgesDeduction']))
		$crownsBridges = round($t1['crownsBridgesDeduction'],0);

	$c2 = array('implantants' => $implantants, 'isFunctionalTherapyInsuredInCaseOfDentures' => $t1['isFunctionalTherapyInsuredInCaseOfDentures'], 'boneAugmentationImplants' => $t1['boneAugmentationImplants'], 'crownsBridgesDeduction' => $crownsBridges, 'inlays' => $inlays, 'regularCare' => $regularCare, 'syntheticFillings' => $syntheticFillings);


	/* 3 */
	if(isset($t1['dentalCleaningInPercent']))
	{
		$dentalCleaning = round($t1['dentalCleaningInPercent'],0);

		if(isset($t1['dentalCleaningMaxAmountPerYear']))
			$dentalCleaningText = ' max. '.round($t1['dentalCleaningMaxAmountPerYear'],0).'&euro; pro Jahr';
		if(isset($t1['dentalCleaningMaxAmountPerTwoYears']))
			$dentalCleaningText .= 'max. '.round($t1['dentalCleaningMaxAmountPerTwoYears'],0).'&euro; alle 2 Jahre';
		if(!isset($t1['dentalCleaningMaxTreatmentsPerYear']) && isset($t1['dentalCleaningMaxAmountPerTreatment']))
			$dentalCleaningText .= 'max '.round($t1['dentalCleaningMaxTreatmentsPerYear'],0).'x pro Jahr';
		
		if(isset($t1['dentalCleaningMaxTreatmentsPerYear']) && $t1['dentalCleaningMaxTreatmentsPerYear'] == 0 && isset($t1['dentalCleaningMaxAmountPerTreatment']) )
		{ 
			$dentalCleaningText .= ' max. '.round($t1['dentalCleaningMaxAmountPerTreatment'],0).'&euro; je PZR';
		} 
		elseif(isset($t1['dentalCleaningMaxTreatmentsPerYear']) && $t1['dentalCleaningMaxTreatmentsPerYear'] > 0)
		{
			$dentalCleaningText .= ' max. '.$t1['dentalCleaningMaxTreatmentsPerYear'].'x '.round($t1['dentalCleaningMaxAmountPerTreatment'],0).'&euro; pro Jahr';
		} 
	}

	/* PZR bei Kindern */
	if(isset($t1['hasDentalCleaningForChilds']) && $t1['hasDentalCleaningForChilds'] == 1)
	{
		$dentalCleaningForChilds = round($t1['fissuresInPercent'],0);

		if(isset($t1['fissuresMaxAmountPerYear']))
			$dentalCleaningForChildsText = ' max. '.round($t1['fissuresMaxAmountPerYear'],0).'&euro; pro Jahr';
		if(isset($t1['fissuresMaxAmountPerTwoYears']))
			$dentalCleaningForChildsText .= 'max. '.round($t1['fissuresMaxAmountPerTwoYears'],0).'&euro; alle 2 Jahre';
		if(!isset($t1['fissuresMaxTreatmentsPerYear']) && isset($t1['fissuresMaxAmountPerTreatment']))
			$dentalCleaningForChildsText .= 'max '.round($t1['fissuresMaxTreatmentsPerYear'],0).'x pro Jahr';
		
		if(isset($t1['fissuresMaxTreatmentsPerYear']) && $t1['fissuresMaxTreatmentsPerYear'] == 0 && isset($t1['fissuresMaxAmountPerTreatment']) )
		{ 
			$dentalCleaningForChildsText .= ' max. '.round($t1['fissuresMaxAmountPerTreatment'],0).'&euro; je PZR';
		} 
		elseif(isset($t1['fissuresMaxTreatmentsPerYear']) && $t1['fissuresMaxTreatmentsPerYear'] > 0)
		{
			$dentalCleaningForChildsText .= ' max. '.$t1['fissuresMaxTreatmentsPerYear'].'x '.round($t1['fissuresMaxAmountPerTreatment'],0).'&euro; pro Jahr';
		} 
	}


	if(isset($t1['syntheticFillingsWithoutBonusInPercent']) && isset($t1['syntheticFillingsWith10YearsBonusInPercent']))
		$syntheticFilling = $t1['syntheticFillingsWithoutBonusInPercent'] == $t1['syntheticFillingsWith10YearsBonusInPercent']?round($t1['syntheticFillingsWithoutBonusInPercent'],0):round($t1['syntheticFillingsWithoutBonusInPercent'],0).' - '.round($t1['syntheticFillingsWith10YearsBonusInPercent'],0);

	$rootTreatment = "";
	if(isset($t1['hasRootTreatmentWithoutPayment']))
	{
		if(isset($t1['rootTreatmentWithoutBonusInPercentWithoutGKVPayment']) && isset($t1['rootTreatmentWith10YearsBonusInPercentWithoutGKVPayment']))
			$rootTreatment = $t1['rootTreatmentWithoutBonusInPercentWithoutGKVPayment'] == $t1['rootTreatmentWith10YearsBonusInPercentWithoutGKVPayment']?round($t1['rootTreatmentWithoutBonusInPercentWithoutGKVPayment'],0):round($t1['rootTreatmentWithoutBonusInPercentWithoutGKVPayment'],0).' - '.round($t1['rootTreatmentWith10YearsBonusInPercentWithoutGKVPayment'],0);	
	}

	$periodentitis = "";
	if(isset($t1['hasPeriodontitisTreatmentWithoutPayment']))
	{
		if(isset($t1['periodontitisTreatmentWithoutBonusInPercentWithoutGKVPayment']) && isset($t1['periodontitisWith10YearsBonusInPercentWithoutGKVPayment']))
			$periodentitis = $t1['periodontitisTreatmentWithoutBonusInPercentWithoutGKVPayment'] == $t1['periodontitisWith10YearsBonusInPercentWithoutGKVPayment']?round($t1['periodontitisTreatmentWithoutBonusInPercentWithoutGKVPayment'],0):round($t1['periodontitisTreatmentWithoutBonusInPercentWithoutGKVPayment'], 0).' - '.round($t1['periodontitisWith10YearsBonusInPercentWithoutGKVPayment'], 0);	
	}

	$c3 = array('dentalCleaning' => $dentalCleaning, 'dentalCleaningText' => $dentalCleaningText, 'dentalCleaningForChilds' => $dentalCleaningForChilds, 'dentalCleaningForChildsText' => $dentalCleaningForChildsText, 'syntheticFilling' => $syntheticFilling, 'rootTreatment' => $rootTreatment, 'periodentitis' => $periodentitis);

	

	/* 4 */

	/* Zahnersatz */



	if(isset($t1['waitingTimeImplantsInMonths']) && isset($t1['waitingTimeInlaysInMonths']) && isset($t1['waitingTimeCrownsBridgesInMonths']))
	{
		if($t1['waitingTimeImplantsInMonths'] == $t1['waitingTimeInlaysInMonths'] && $t1['waitingTimeInlaysInMonths'] == $t1['waitingTimeCrownsBridgesInMonths'])
		{
		// Zusammenfassen!´
			$text = "Zahnersatz : ".($t1['waitingTimeImplantsInMonths']==0?0:$t1['waitingTimeImplantsInMonths']);
			$waitingTimeAry['Zahnersatz'] = $t1['waitingTimeImplantsInMonths']==0?0:$t1['waitingTimeImplantsInMonths'];
		} else {
		// nicht zusammenfassen !
		$text = "Kronen, Brücken, Prothesen : ".$t1['waitingTimeImplantsInMonths'];
		$text .= "<br />Implantate : ".$t1['waitingTimeInlaysInMonths'];
		$text .= "<br />Inlays : ".$t1['waitingTimeCrownsBridgesInMonths'];
		$waitingTimeAry['Zahnersatz'] = array('Kronen, Brücken, Prothesen' => $t1['waitingTimeImplantsInMonths'], 'Implantate' => $t1['waitingTimeInlaysInMonths'], 'Inlays' => $t1['waitingTimeCrownsBridgesInMonths']);
		}

		$text .= "<br />";
	} else {
		$text .= "Zahnersatz : 0<br/>";
		$waitingTimeAry['Zahnersatz'] = 0;
	}

	/* Zahnbehandlung */
	if((isset($t1['rootTreatmentWithoutBonusInPercentWithoutGKVPayment']) && $t1['rootTreatmentWithoutBonusInPercentWithoutGKVPayment'] == 0) || !isset($t1['rootTreatmentWithoutBonusInPercentWithoutGKVPayment']))
	{
		$text .= "Zahnbehandlung : <br/>";
		$waitingTimeAry['Zahnbehandlung'] = '';
	}
	else
	{
		if(isset($t1['waitingTimeSyntheticFillingsInMonths']) && isset($t1['waitingTimeRootTreatmentInMonths']) && isset($t1['waitingTimePeriodontitisTreatmentInMonths'])) 
		{
			if($t1['waitingTimeSyntheticFillingsInMonths'] == $t1['waitingTimeRootTreatmentInMonths'] && $t1['waitingTimeSyntheticFillingsInMonths'] == $t1['waitingTimePeriodontitisTreatmentInMonths'])
			{
			// Zusammenfassen!´
				$text .= "Zahnbehandlung : ".($t1['waitingTimeSyntheticFillingsInMonths']==0?0:$t1['waitingTimeSyntheticFillingsInMonths']);
				$waitingTimeAry['Zahnbehandlung'] = $t1['waitingTimeSyntheticFillingsInMonths']==0?0:$t1['waitingTimeSyntheticFillingsInMonths'];
			} else {
			// nicht zusammenfassen !
			$text .= "Kunststofffüllungen : ".$t1['waitingTimeSyntheticFillingsInMonths'];
			$text .= "<br />Wurzelbehandlung : ".$t1['waitingTimeRootTreatmentInMonths'];
			$text .= "<br />PA-Behandlung : ".$t1['waitingTimePeriodontitisTreatmentInMonths'];
			$waitingTimeAry['Zahnbehandlung'] = array($t1['waitingTimeSyntheticFillingsInMonths'], $t1['waitingTimeRootTreatmentInMonths'], $t1['waitingTimePeriodontitisTreatmentInMonths']);
			}

			$text .= "<br />";
		} else {
			$text .= "Zahnbehandlung : 0<br/>";
			$waitingTimeAry['Zahnbehandlung'] = 0;
		}
	}

	/* Prophylaxe */
	if((isset($t1['dentalCleaningInPercent']) && $t1['dentalCleaningInPercent'] == 0) || !isset($t1['dentalCleaningInPercent']))
	{
		$text .= "Prophylaxe : <br />";
		$waitingTimeAry['Prophylaxe'] = '';
	} else {
		if(isset($t1['waitingTimeDentalCleaningInMonths']) && isset($t1['waitingTimeFissuresInMonths']) ) 
		{
			if($t1['waitingTimeDentalCleaningInMonths'] == $t1['waitingTimeFissuresInMonths'] )
			{
			// Zusammenfassen!´
				$text .= "Prophylaxe : ".($t1['waitingTimeDentalCleaningInMonths']==0?0:$t1['waitingTimeDentalCleaningInMonths']);
				$waitingTimeAry['Prophylaxe'] = $t1['waitingTimeDentalCleaningInMonths']==0?0:$t1['waitingTimeDentalCleaningInMonths'];
			} else {
			// nicht zusammenfassen !
			$ary = array($t1['waitingTimeDentalCleaningInMonths'], $t1['waitingTimeFissuresInMonths']);
			asort($ary);

			$text .= "Prophylaxe : ";

			$i = 0;			
			foreach($ary as $key => $val)
			{
				if($i != 0) $text .= ' / ';
				$text .= $val; $i++;
			}

			$waitingTimeAry['Prophylaxe'] = $ary;
			}

			$text .= "<br />";
		} else {
			$text .= "Prophylaxe : 0<br/>";
			$waitingTimeAry['Prophylaxe'] = 0;
		}
	}



	/* Kieferorthopädie */
	if((isset($t1['orthodonticsAdultsInPercent']) && $t1['orthodonticsAdultsInPercent'] == 0) || !isset($t1['orthodonticsAdultsInPercent']))
	{
		$text .= "Kieferorthopädie : ";
		$waitingTimeAry['adult']['Kieferorthopädie'] = '';
	} else {
		$orthodontics['adult'] = round($t1['orthodonticsAdultsInPercent'],0);
		if(isset($t1['orthodonticsAdultsOnlyInCaseOfAccident']) && $t1['orthodonticsAdultsOnlyInCaseOfAccident'] == 0)
		{
			$orthodontics_text['adult'] = "nur bei Unfall";
		} else $orthodontics_text['adult'] = "";

		// Begrenzungen adult
		if(isset($t1['orthodonticsAdultsAmountLimitPerYear']) && $t1['orthodonticsAdultsAmountLimitPerYear'] > 0)
			$orthodontics_limitations['adult'] = "max. ".round($t1['orthodonticsAdultsAmountLimitPerYear'], 0)." &euro; p.a.";

		if(isset($t1['orthodonticsAdultsAmountLimitationPerInsuredEvent']) )
			$orthodontics_limitations['adult'] = "max. ".round($t1['orthodonticsAdultsAmountLimitationPerInsuredEvent'], 0)." &euro; je Versicherungsfall";

		if(isset($t1['orthodonticsAdultsAmountLimitationPerContractPeriod']) )
			$orthodontics_limitations['adult'] = "max. ".round($t1['orthodonticsAdultsAmountLimitationPerContractPeriod'], 0)." &euro; während der Vertragslaufzeit";

		if(isset($t1['orthodonticsAdultsAmountLimitPerJaw']) )
			$orthodontics_limitations['adult'] = "max. ".round($t1['orthodonticsAdultsAmountLimitPerJaw'], 0)." &euro; je Kiefer";

	}

	/* Kieferorthopädie Kinder 1-2 */
	if((isset($t1['orthodonticsKig2InPercent']) && $t1['orthodonticsKig2InPercent'] == 0) || !isset($t1['orthodonticsKig2InPercent']))
	{
		$text .= "Kieferorthopädie Kinder : ";
		$waitingTimeAry['child']['Kieferorthopädie2'] = '';
	} else {
		$orthodontics['child']['1-2'] = round($t1['orthodonticsKig2InPercent'],0);

		// Begrenzungen 
		if(isset($t1['orthodonticsKig2AmountLimitationPerYear']) && $t1['orthodonticsKig2AmountLimitationPerYear'] > 0)
			$orthodontics_limitations['child']['1-2'] = "max. ".round($t1['orthodonticsKig2AmountLimitationPerYear'],0)." &euro; p.a.";

		if(isset($t1['orthodonticsKig2AmountLimitationPerInsuredEvent']) )
			$orthodontics_limitations['child']['1-2'] = "max. ".round($t1['orthodonticsKig2AmountLimitationPerInsuredEvent'],0). " &euro; je Versicherungsfall";

		if(isset($t1['orthodonticsKig2AmountLimitationPerContractPeriod']) )
			$orthodontics_limitations['child']['1-2'] = "max. ".round($t1['orthodonticsKig2AmountLimitationPerContractPeriod'],0). " &euro; während der Vertragslaufzeit";

		if(isset($t1['orthodonticsKig2AmountLimitationPerJaw']) )
			$orthodontics_limitations['child']['1-2'] = "max. ".round($t1['orthodonticsKig2AmountLimitationPerJaw'],0). " &euro; je Kiefer";


	}

	/* Kieferorthopädie Kinder 3-5 */
	if((isset($t1['orthodonticsKig35InPercent']) && $t1['orthodonticsKig35InPercent'] == 0) || !isset($t1['orthodonticsKig35InPercent']))
	{
		$text .= "Kieferorthopädie Kinder : ";
		$waitingTimeAry['child']['Kieferorthopädie35'] = '';
	} else {
		$orthodontics['child']['3-5'] = round($t1['orthodonticsKig35InPercent'],0);

		// Begrenzungen
		if(isset($t1['orthodonticsKig35AmountLimitationPerYear']) && $t1['orthodonticsKig35AmountLimitationPerYear'] > 0)
			$orthodontics_limitations['child']['3-5'] = "max. ".round($t1['orthodonticsKig35AmountLimitationPerYear'],0)." &euro; p.a.";

		if(isset($t1['orthodonticsKig35AmountLimitationPerInsuredEvent']) )
			$orthodontics_limitations['child']['3-5'] = "max. ".round($t1['orthodonticsKig35AmountLimitationPerInsuredEvent'],0). " &euro; je Versicherungsfall";

		if(isset($t1['orthodonticsKig35AmountLimitationPerContractPeriod']) )
			$orthodontics_limitations['child']['3-5'] = "max. ".round($t1['orthodonticsKig35AmountLimitationPerContractPeriod'],0). " &euro; während der Vertragslaufzeit";

		if(isset($t1['orthodonticsKig35AmountLimitationPerJaw']) )
			$orthodontics_limitations['child']['3-5'] = "max. ".round($t1['orthodonticsKig35AmountLimitationPerJaw'],0). " &euro; je Kiefer";


	}

	/* Kieferorthopädie Wartezeit */
	if(isset($t1['waitingTimeOrthodonticsKig2InMonths']) || isset($t1['waitingTimeOrthodonticsKig35InMonths'])) 
	{
			if($t1['waitingTimeOrthodonticsKig2InMonths'] == $t1['waitingTimeOrthodonticsKig35InMonths'])
			{
			// Zusammenfassen!´
				$text .= "Kieferorthopädie : ".($t1['waitingTimeOrthodonticsKig2InMonths']==0?0:$t1['waitingTimeOrthodonticsKig2InMonths']);
				$waitingTimeAry['Kieferorthopädie'] = $t1['waitingTimeOrthodonticsKig2InMonths']==0?0:$t1['waitingTimeOrthodonticsKig2InMonths'];
			} else {
			// nicht zusammenfassen !
				$text .= "<br />KFO Kind Kig2 : ".$t1['waitingTimeOrthodonticsKig2InMonths'];
				$text .= "<br />KFO Kind Kig3-5 : ".$t1['waitingTimeOrthodonticsKig35InMonths'];
				$waitingTimeAry['Kieferorthopädie'] = array($t1['waitingTimeOrthodonticsKig2InMonths'], $t1['waitingTimeOrthodonticsKig35InMonths']);
			}

			$text .= "<br />";
	} else {
			$text .= "Kieferorthopädie : 0";
			$waitingTimeAry['Kieferorthopädie'] = 0;
	}


	$v = '';

	$waitingTime = $text;

	#$benefits = json_decode($t1['benefits_raw'], true);
	$performanceScale = $benefits->getPerformanceScale($this->input['idt']);

	#if(isset($benefits['periodsAndLimits']))
	#{
	#	$benefits = $benefits['periodsAndLimits'];
	#} else $benefits = "";

	$c4 = array('orthodontics' => $orthodontics, 'orthodontics_text' => $orthodontics_text, 'orthodontics_limitations' => $orthodontics_limitations, 'waitingTime' => $waitingTime, 'waitingTimeAry' => $waitingTimeAry, 'waitingTimeVal' => $v, 'materialCost' => $t1['hasMaterialCostRepository'], 'benefits' => $performanceScale);

	

	/* Leistungen in Kürze */
	$overview['übersicht'] = array('zahnersatz' =>'', 'prophylaxe' =>'', 'kieferorthopädie' =>'', 'begrenzungen' =>'');

	$overview['übersicht']['zahnersatz'] = array('Zahnersatz auf Privatniveau' => $this->bold($implantants, true, true), 
								'Inlays auf Privatniveau' => $this->bold($inlays, true, true), 
								'Regelversorgung' => $this->bold($regularCare, true, true)
	);
	$overview['übersicht']['prophylaxe'] = array('Zahnreinigung (PZR)' => $this->bold($c3['dentalCleaning'], true).$c3['dentalCleaningText'], 
								'Hochwertige Füllungen' => $this->bold($syntheticFilling, true, true), 
								'Wurzelbehandlung' => $this->bold($rootTreatment, true), 
								'Parodontosebehandlung' => $this->bold($periodentitis, true)
	);
	$overview['übersicht']['kieferorthopädie'] = array('KFO - KIG 2' => $this->bold($orthodontics['child']['1-2'], true) .($orthodontics_limitations['child']['1-2']?' '.$orthodontics_limitations['child']['1-2']:''), 
								'KFO - KIG 3-5' => $this->bold($orthodontics['child']['3-5'], true) .($orthodontics_limitations['child']['3-5']?' '.$orthodontics_limitations['child']['3-5']:''),
								'KFO Erwachsene' => $this->bold($orthodontics['adult'], true) .($orthodontics_text['adult']?' '.$orthodontics_text['adult']:'')
	);
/*
	if(isset($benefits['maxBenefits'][0]['maxBenefits'][0]))
	{
	$overview['übersicht']['begrenzungen'] = array('Max. Zahnersatz 1. Jahr' => $benefits['maxBenefits'][0]['maxBenefits'][0]. ' &euro;', 
								'Max. Zahnersatz 1. - 4. Jahr' => $benefits['maxBenefits'][0]['maxBenefits'][3]. ' &euro;', 
								'Wartezeit Zahnersatz' => $this->getValue($waitingTimeAry['Zahnersatz'], ' Monate Wartezeit'), 
								'Wartezeit Zahnreinigung' => $this->getValue($waitingTimeAry['Prophylaxe'], ' Monate Wartezeit')
	);
	}
*/
	$class = array('TarifKlasse' => array('Kindertarif (mit KFO-Leistungen)' => $t1['isChildTariffWithKfoPayment'], 'E – ZE + ZB + ZR' => $t1['showIfDenturesTreatmentProphylaxis'], 'E – ZE + ZB' => $t1['showIfDenturesTreatment'], 'E – ZE + ZR' => $t1['showIfDenturesProphylaxis'], 'E – ZB + ZR' => $t1['showIfTreatmentProphylaxis'], 'E – ZE' => $t1['showIfDentures'], 'E – ZB' => $t1['showIfTreatment'], 'E – ZR' => $t1['showIfProphylaxis'] ));

	$comparison = array_merge($c1, $c2, $c3, $c4, $overview, $class);


	return $comparison;

}

private function bold($text, $percentage=false, $gkv=false)
{
	if(!isset($text))
	{
	return 'Keine Leistung';
	}
	return '<strong>'.$text.($percentage?'%':'').'</strong>'.($gkv?'inkl. GKV':'');
}

private function getValue($val, $addText=null)
{

	if($val == null)
	{
		return 'Keine Leistung';
	}

	if(is_array($val))
	{
		return max($val).$addText;
	}
	else return $val.$addText;
}

public function getBonusBase($table=true)
{

	$c = new contract();

	if(!isset($this->input['idt']))
	{
		return null;
	}
	else
	{
		if(!isset($this->input['wishedBegin']))
		{
			$wishedBegin = mktime(0, 0, 0, date("m")+1, 1, date("Y"));
		} else {
			$wishedBegin = $this->input['wishedBegin'];
		}

		if($table)
		{
			return $c->getBonusBaseHtmlTable($this->input['idt'], $wishedBegin);
		} else {
			return $c->getBonusBaseTable($this->input['idt'], $wishedBegin);
		}
	}

}

private function determineStatus($val)
{	
	$ret = 0;

	foreach($val[0] as $filter)
	{
		if($this->tariffBenefits[$filter] >= 1 || is_null($this->tariffBenefits[$filter]))
			$ret = 1;
	}
	return $ret;
}


public function getRefundExamples()
{
	$wzm = new wzm();


	//$idt = 201;
	$bonus = 2;

	$t1 = $this->tariffBenefits = $wzm->getTariffBenefits($this->input['idt']);

	$categories = ['implantat', 'keramik_krone', 'keramik_inlay', 'kunststoff', 'wb_ohne_gkv', 'pzr'];

	$calc_ze_text['implantat']['text'] = 'Implantate';
	$calc_ze_text['implantat']['img'] = 'refundExamples_implantate';
	$calc_ze_text['implantat']['status'] = 0;

	$calc_ze_text['keramik_krone']['text'] = 'Vollkeramikkrone';
	$calc_ze_text['keramik_krone']['img'] = 'refundExamples_vollkeramikkrone';
	$calc_ze_text['keramik_krone']['status'] = 0;

	$calc_ze_text['keramik_inlay']['text'] = 'Keramikinlay';
	$calc_ze_text['keramik_inlay']['img'] = 'refundExamples_keramikinlay';
	$calc_ze_text['keramik_inlay']['status'] = 0;

	$calc_ze_text['kunststoff']['text'] = 'Hochwertige Kunststofffüllungen';
	$calc_ze_text['kunststoff']['img'] = 'refundExamples_kunststoff';
	$calc_ze_text['kunststoff']['status'] = 0;

	$calc_ze_text['wb_ohne_gkv']['text'] = 'Wurzelbehandlung ohne GKV';
	$calc_ze_text['wb_ohne_gkv']['img'] = 'refundExamples_wurzelbehandlung';
	$calc_ze_text['wb_ohne_gkv']['status'] = 0;

	$calc_ze_text['pzr']['text'] = 'Professionelle Zahnreinigung';
	$calc_ze_text['pzr']['img'] = 'refundExamples_pzr';
	$calc_ze_text['pzr']['status'] = 0;

	$calc_ze['header'] = $calc_ze_text;
	$calc_ze['insurance-info']['insurance'] = $t1['insurance'];
	$calc_ze['insurance-info']['tarif'] = $t1['highlightsName'];
	$calc_ze['insurance-info']['onlinelink'] = $t1['onlineLink'];


	$implantat[0] = array('implantsWithoutBonusInPercent', 'implantsWith5YearsBonusInPercent', 'implantsWith10YearsBonusInPercent');
	$implantat[1] = array('text' => "Kosten für ein Implantat mit Keramikkrone", 'amount' => 2300);
	$implantat[2] = array('text' => "Anteil der GKV", 'amount' => 400);
	$implantat[3] = array('text' => "Eigenanteil <strong>ohne Zahnzusatzversicherung</strong>", 'amount' => $implantat[1]['amount'] - $implantat[2]['amount']);

	$keramik_krone[0] = array('crownsBridgesWithoutBonusInPercent', 'crownsBridgesWith5YearsBonusInPercent', 'crownsBridgesWith10YearsBonusInPercent');
	$keramik_krone[1] = array('text' => "Kosten für eine Vollkeramikkrone", 'amount' => 800);
	$keramik_krone[2] = array('text' => "Anteil der GKV", 'amount' => 200);
	$keramik_krone[3] = array('text' => "Eigenanteil <strong>ohne Zahnzusatzversicherung</strong>", 'amount' => $keramik_krone[1]['amount'] - $keramik_krone[2]['amount']);

	$keramik_inlay[0] = array('inlaysWithoutBonusInPercent', 'inlaysWith5YearsBonusInPercent', 'inlaysWith10YearsBonusInPercent');
	$keramik_inlay[1] = array('text' => "Kosten für ein Keramikinlay", 'amount' => 800);
	$keramik_inlay[2] = array('text' => "Anteil der GKV", 'amount' => 50);
	$keramik_inlay[3] = array('text' => "Eigenanteil <strong>ohne Zahnzusatzversicherung</strong>", 'amount' => $keramik_inlay[1]['amount'] - $keramik_inlay[2]['amount']);

	$kunststoff[0] = array('syntheticFillingsWithoutBonusInPercent', 'syntheticFillingsWith5YearsBonusInPercent', 'syntheticFillingsWith10YearsBonusInPercent');
	$kunststoff[1] = array('text' => "Kosten für eine hochwertige Kunststofffüllung", 'amount' => 150);
	$kunststoff[2] = array('text' => "Anteil der GKV", 'amount' => 50);
	$kunststoff[3] = array('text' => "Eigenanteil <strong>ohne Zahnzusatzversicherung</strong>", 'amount' => $kunststoff[1]['amount'] - $kunststoff[2]['amount']);

	$wb_ohne_gkv[0] = array('rootTreatmentWithoutBonusInPercentWithoutGKVPayment', 'rootTreatmentWith5YearsBonusInPercentWithoutGKVPayment', 'rootTreatmentWith10YearsBonusInPercentWithoutGKVPayment');
	$wb_ohne_gkv[1] = array('text' => "Kosten für eine Wurzelbehandlung", 'amount' => 1000);
	$wb_ohne_gkv[2] = array('text' => "Anteil der GKV", 'amount' => 0);
	$wb_ohne_gkv[3] = array('text' => "Eigenanteil <strong>ohne Zahnzusatzversicherung</strong>", 'amount' => $wb_ohne_gkv[1]['amount'] - $wb_ohne_gkv[2]['amount']);

	$pzr[0] = '';
	$pzr[1] = array('text' => "Kosten für eine professionelle Zahnreinigung", 'amount' => 100);
	$pzr[2] = array('text' => "Anteil der GKV", 'amount' => 0);
	$pzr[3] = array('text' => "Eigenanteil <strong>ohne Zahnzusatzversicherung</strong>", 'amount' => $pzr[1]['amount'] - $pzr[2]['amount']);


	#$wb_mit_gkv[0] = array('inlaysWithoutBonusInPercent', 'inlaysWith5YearsBonusInPercent', 'inlaysWith10YearsBonusInPercent');
	#$wb_mit_gkv[1] = array('text' => "Kosten für eine Wurzelbehandlung", 'amount' => 200);
	#$wb_mit_gkv[2] = array('text' => "Anteil der GKV", 'amount' => 0);
	#$wb_mit_gkv[3] = array('text' => "Eigenanteil <strong>ohne Zahnzusatzversicherung</strong>", 'amount' => $wb_mit_gkv[1]['amount'] - $wb_mit_gkv[2]['amount']);


	$calc_ze['pzr'] = 0;

	$calc_ze['implantat'] = $implantat;
	$calc_ze['keramik_krone'] = $keramik_krone;
	$calc_ze['keramik_inlay'] = $keramik_inlay;
	$calc_ze['pzr'] = $pzr;
	$calc_ze['kunststoff'] = $kunststoff;
	$calc_ze['wb_ohne_gkv'] = $wb_ohne_gkv;
	


	if (isset($t1['denturesFixedAllowanceInPercent']) && $t1['denturesFixedAllowanceInPercent'] > 1)
	{
		if(is_null($t1['implantsWithoutBonusInPercent']) || $t1['implantsWithoutBonusInPercent'] != 0)
			$calc_ze['implantat'][4]['amount'] = $implantat[2]['amount'] * $t1['denturesFixedAllowanceInPercent'] / 100;
	
		if(is_null($t1['crownsBridgesWithoutBonusInPercent']) || $t1['crownsBridgesWithoutBonusInPercent'] != 0)
			$calc_ze['keramik_krone'][4]['amount'] = $keramik_krone[2]['amount'] * $t1['denturesFixedAllowanceInPercent'] / 100;

		if(is_null($t1['inlaysWithoutBonusInPercent']) || $t1['inlaysWithoutBonusInPercent'] != 0)
			$calc_ze['keramik_inlay'][4]['amount'] = $keramik_inlay[2]['amount'] * $t1['denturesFixedAllowanceInPercent'] / 100;
	} else 
	{
		switch($t1['hasHealthCareIncludedInBenefitsPercentDentures']) 
		{
			case 0:
				$calc_ze['implantat'][4]['amount'] = ($implantat[1]['amount'] * $t1[$implantat[0][$bonus]] / 100) /*- $implantat[2]['amount']*/;
				## max. Spalte C ##
				if($calc_ze['implantat'][4]['amount'] > ($implantat[1]['amount'] - $implantat[2]['amount']))
					$calc_ze['implantat'][4]['amount'] = ($implantat[1]['amount'] - $implantat[2]['amount']);

				## max Spalte B * ID60 ##
				if($calc_ze['implantat'][4]['amount'] > ($implantat[1]['amount'] * $t1['maxReimbursementInclHealthCareInPercent']) / 100)
					$calc_ze['implantat'][4]['amount'] = ($implantat[1]['amount'] * $t1['maxReimbursementInclHealthCareInPercent']) / 100;

				$calc_ze['keramik_krone'][4]['amount'] = ($keramik_krone[1]['amount'] * $t1[$keramik_krone[0][$bonus]] / 100) /*- $keramik_krone[2]['amount']*/;
				## max. Spalte C ##
				if($calc_ze['keramik_krone'][4]['amount'] > ($keramik_krone[1]['amount'] - $keramik_krone[2]['amount']))
					$calc_ze['keramik_krone'][4]['amount'] = ($keramik_krone[1]['amount'] - $keramik_krone[2]['amount']);

				## max Spalte B * ID60 ##
				if($calc_ze['keramik_krone'][4]['amount'] > ($keramik_krone[1]['amount'] * $t1['maxReimbursementInclHealthCareInPercent']) / 100)
					$calc_ze['keramik_krone'][4]['amount'] = ($keramik_krone[1]['amount'] * $t1['maxReimbursementInclHealthCareInPercent']) / 100;

				$calc_ze['keramik_inlay'][4]['amount'] = ($keramik_inlay[1]['amount'] * $t1[$keramik_inlay[0][$bonus]] / 100) /*- $keramik_inlay[2]['amount']*/;
				## max. Spalte C ##
				if($calc_ze['keramik_inlay'][4]['amount'] > ($keramik_inlay[1]['amount'] - $keramik_inlay[2]['amount']))
					$calc_ze['keramik_inlay'][4]['amount'] = ($keramik_inlay[1]['amount'] - $keramik_inlay[2]['amount']);

				## max Spalte B * ID60 ##
				if($calc_ze['keramik_inlay'][4]['amount'] > ($keramik_inlay[1]['amount'] * $t1['maxReimbursementInclHealthCareInPercent']) / 100)
					$calc_ze['keramik_inlay'][4]['amount'] = ($keramik_inlay[1]['amount'] * $t1['maxReimbursementInclHealthCareInPercent']) / 100;
			break;

			case 1:
				$calc_ze['implantat'][4]['amount'] = ($implantat[1]['amount'] * $t1[$implantat[0][$bonus]] / 100) - $implantat[2]['amount'];
				$calc_ze['keramik_krone'][4]['amount'] = ($keramik_krone[1]['amount'] * $t1[$keramik_krone[0][$bonus]] / 100) - $keramik_krone[2]['amount'];
				$calc_ze['keramik_inlay'][4]['amount'] = ($keramik_inlay[1]['amount'] * $t1[$keramik_inlay[0][$bonus]] / 100) - $keramik_inlay[2]['amount'];
			break;

			case 2:
				$calc_ze['implantat'][4]['amount'] = ($implantat[1]['amount'] - $implantat[2]['amount']) * $t1[$implantat[0][$bonus]] / 100;
				$calc_ze['keramik_krone'][4]['amount'] = ($keramik_krone[1]['amount'] - $keramik_krone[2]['amount']) * $t1[$keramik_krone[0][$bonus]] / 100;
				$calc_ze['keramik_inlay'][4]['amount'] = ($keramik_inlay[1]['amount'] - $keramik_inlay[2]['amount']) * $t1[$keramik_inlay[0][$bonus]] / 100;
			break;
		}
	}


	## PZR ##
	if (isset($t1['hasDentalCleaningLimit']) && $t1['hasDentalCleaningLimit'] == 0)
	{
		$calc_ze['pzr'][4]['amount'] = $calc_ze['pzr'][3]['amount'];
	}
	else
	{
		if(isset($t1['dentalCleaningInPercent']))
			$calc_ze['pzr'][4]['amount'] = $pzr[1]['amount'] * $t1['dentalCleaningInPercent'] / 100;

		if(isset($t1['dentalCleaningMaxAmountPerYear']) && $calc_ze['pzr'][4]['amount'] > $t1['dentalCleaningMaxAmountPerYear'])
			$calc_ze['pzr'][4]['amount'] = $t1['dentalCleaningMaxAmountPerYear'];

		if(isset($t1['dentalCleaningMaxAmountPerTwoYears']) && $calc_ze['pzr'][1]['amount'] > $t1['dentalCleaningMaxAmountPerTwoYears'])
			$calc_ze['pzr'][4]['amount'] = $t1['dentalCleaningMaxAmountPerTwoYears']; 

		if(isset($t1['dentalCleaningMaxAmountPerTreatment']) && $calc_ze['pzr'][4]['amount'] < $t1['dentalCleaningMaxAmountPerTreatment'])



			$calc_ze['pzr'][4]['amount'] = $t1['dentalCleaningMaxAmountPerTreatment'];
	}

	## Kunststofffüllung ##
	if (isset($t1['hasHealthCareIncludedInBenefitsPercentDentalTreatment']))
	{
		switch($t1['hasHealthCareIncludedInBenefitsPercentDentalTreatment'])
		{
			case 0:
				$calc_ze['kunststoff'][4]['amount'] = $kunststoff[1]['amount'] * $t1[$kunststoff[0][$bonus]] / 100;
			break;

			case 1:
				$calc_ze['kunststoff'][4]['amount'] = $kunststoff[1]['amount'] * $t1[$kunststoff[0][$bonus]] / 100 - $kunststoff[2]['amount'];
			break;

			case 2:
				$calc_ze['kunststoff'][4]['amount'] = ($kunststoff[1]['amount'] - $kunststoff[2]['amount']) * $t1[$kunststoff[0][$bonus]] / 100;
			break;
		}
		if(($calc_ze['kunststoff'][4]['amount'] + $calc_ze['kunststoff'][2]['amount']) > $calc_ze['kunststoff'][1]['amount'])
			$calc_ze['kunststoff'][4]['amount'] = $calc_ze['kunststoff'][1]['amount'] - $calc_ze['kunststoff'][2]['amount'];
	}


	## WB ohne GKV ##
	if (isset($t1['hasRootTreatmentWithoutPayment']) && $t1['hasRootTreatmentWithoutPayment'] == 0)
	{
		$calc_ze['wb_ohne_gkv'][4]['amount'] = 0;
	}
	else
	{
		$calc_ze['wb_ohne_gkv'][4]['amount'] = $wb_ohne_gkv[1]['amount'] * $t1[$wb_ohne_gkv[0][$bonus]] / 100;
	}



	$calc_ze['implantat'][5]['amount'] = $calc_ze['implantat'][4]['amount'];
	$calc_ze['keramik_krone'][5]['amount'] = $calc_ze['keramik_krone'][4]['amount'];
	$calc_ze['keramik_inlay'][5]['amount'] = $calc_ze['keramik_inlay'][4]['amount'];
	$calc_ze['kunststoff'][5]['amount'] = $calc_ze['kunststoff'][4]['amount'];
	$calc_ze['wb_ohne_gkv'][5]['amount'] = $calc_ze['wb_ohne_gkv'][4]['amount'];
	$calc_ze['pzr'][5]['amount'] = $calc_ze['pzr'][4]['amount'];

	$calc_ze['implantat'][4]['amount'] = $calc_ze['implantat'][3]['amount'] - $calc_ze['implantat'][5]['amount'];
	$calc_ze['keramik_krone'][4]['amount'] = $calc_ze['keramik_krone'][3]['amount'] - $calc_ze['keramik_krone'][5]['amount'];
	$calc_ze['keramik_inlay'][4]['amount'] = $calc_ze['keramik_inlay'][3]['amount'] - $calc_ze['keramik_inlay'][5]['amount'];
	$calc_ze['kunststoff'][4]['amount'] = $calc_ze['kunststoff'][3]['amount'] - $calc_ze['kunststoff'][5]['amount'];
	$calc_ze['wb_ohne_gkv'][4]['amount'] = $calc_ze['wb_ohne_gkv'][3]['amount'] - $calc_ze['wb_ohne_gkv'][5]['amount'];
	$calc_ze['pzr'][4]['amount'] = $calc_ze['pzr'][3]['amount'] - $calc_ze['pzr'][5]['amount'];

	foreach($calc_ze as $id => $value)
	{
		if($id != 'header' && $id != 'insurance-info')
		{
			$calc_ze[$id][4]['text'] = "Eigenanteil <strong>mit ".$t1['highlightsName']."</strong>";
			$calc_ze[$id][5]['text'] = 'Dieser Beitrag wird von diesem Tarif erstattet';
		}
	}


	/* Prüfung der Leistung */
	$calc_ze['header']['keramik_krone']['status'] = $this->determineStatus($keramik_krone);

	return $calc_ze;
	}

public function getRating()
	{
		$c = new contract();

		$rating = $c->getRating($this->input['idt']);

		return $rating;
	}

private function calculatePricePerformance()
	{
		$c = new contract();
		$bonusAmt = 0;
		$b = $this->tariffBenefits;

	if($this->input['type'] == 0)
	{
		// Preis Leistung
		$bonus = $c->getBonusBase($this->activeIdt, $this->input['wishedBegin'], $this->age, 'male', 20);

		if(is_array($bonus))
		{
			if($b['hasAgeningReserves'] == 1)
			{
				// mit Altersrücktellungen kann einfach der 20fache Satz des ersten Monats berechnet werden
				//$arrKeys = array_keys($bonus);
				$bonusAmt = array_shift(array_values($bonus)) * 12 * 20;
			}
			else
			{
				foreach($bonus as $id => $val)
				{
					$bonusAmt += ($val * 12);
				}
			}
		}
		else
		{
			// no bonus!
		}

		if($this->surplusPercentage)
		{
			$percentage = $this->surplusPercentage['missing-teeth']>0?$this->surplusPercentage['missing-teeth']:$this->surplusPercentage['replaced-teeth-removable'];

			$bonusAmt += $bonusAmt / 100 * $percentage;
		}

		if($this->surplus)
		{
			$bonusAmt += $this->surplus * 12 * 20;
		}
		
		if($bonusAmt) {
			$pricePerformance['bonusAmt'] = $bonusAmt;
			$pricePerformance['bonus'] = $bonus;
			if($this->tariffRating['intern']['adult'])
			{
				$aR = $this->tariffRating['intern']['adult'];
				if($aR['sumAdultAll'])
					$pricePerformance['pricePerformanceAll'] = round($bonusAmt / $aR['sumAdultAll'], 2);
				if($aR['sumAdultAllSeries1'])
					$pricePerformance['pricePerformanceAllSeries1'] = round($bonusAmt / $aR['sumAdultAllSeries1'], 2);
				if($aR['sumAdultAllSeries2'])
					$pricePerformance['pricePerformanceAllSeries2'] = round($bonusAmt / $aR['sumAdultAllSeries2'], 2);

				if($aR['sumAdultDentures'])
					$pricePerformance['pricePerformanceDenturesAll'] = round($bonusAmt / $aR['sumAdultDentures'], 2);
				if($aR['sumAdultDenturesSeries1'])
					$pricePerformance['pricePerformanceDenturesSeries1'] = round($bonusAmt / $aR['sumAdultDenturesSeries1'], 2);
				if($aR['sumAdultDenturesSeries2'])
					$pricePerformance['pricePerformanceDenturesSeries2'] = round($bonusAmt / $aR['sumAdultDenturesSeries2'], 2);


			}
			if($pricePerformance)
			{
				$this->addCalculation($pricePerformance);
			}

		}
	}
	elseif($this->input['type'] == 1)
	{
		/* kids */
		if($this->calculation[$this->activeIdt]['intern']['child'] && $this->calculation[$this->activeIdt]['bonusbase'])
		{
			if($this->calculation[$this->activeIdt]['bonusbase']['activeBonusBase'] > 0)
			{
				$pricePerformance['pricePerformanceKids'] = round($this->calculation[$this->activeIdt]['intern']['child']['sumChild'] / $this->calculation[$this->activeIdt]['bonusbase']['activeBonusBase'], 2);
				$this->addCalculation($pricePerformance);
			}
		}
	}

	}

public function getCalculation()
	{
			foreach($this->calculation as $idt => $val)
			{
				if($this->input['type'] == 0)
				{
					unset($val['intern']['child']);
				} elseif($this->input['type'] == 1)
				{
					unset($val['intern']['adult']);
				}

				$this->calculation[$idt] = $val;
			}

		return $this->calculation;
	}

private function startCalculation($idt)
	{
		$this->activeIdt = $idt;
		$this->calculation[$idt] = ['insurance' => utf8_encode($this->tariff['company']), 'tariffName' => $this->tariff['tariffName'], 'status' => 1, 'statusText' => 'Annahme', 'series' => 0];
	}

private function addCalculation($args)
	{
		if($this->activeIdt)
		{
			$status = $this->calculation[$this->activeIdt];
			
			if(is_array($args))
				$status = array_merge($status, $args);
		

			$this->calculation[$this->activeIdt] = $status;
		}
	}   

private function checkBirthdate($g)
	{
		if($this->age > $g['maxAge'])
		{
			$this->addCalculation(array('idt' => $this->idt, 'status' => 0, 'series' => 0, 'statusText' => 'Ablehnung, da maximales Alter Überschritten.'));
		}
	}


private function checkMissingTeeth($g)
	{
		$status = array('status' => 0, 'series' => 0, 'statusText' => 'Ablehnung, da zu viele fehlende Zähne!');

		if (isset($g['missingTeethLimitSeries2']))
		{
			if((int)$this->input['missing-teeth'] <= (int)$g['missingTeethLimitSeries2'])
				$status = array('idt' => $this->idt,'status' => 1, 'series' => 2, 'statusText' => 'Annahme, aber wegen fehlender Zähne gilt Leistungsstaffel 2!');
		}

		if (isset($g['missingTeethLimitSeries1']))
		{
			if((int)$this->input['missing-teeth'] <= (int)$g['missingTeethLimitSeries1'])
				$status = array('idt' => $this->idt,'status' => 1, 'series' => 1, 'statusText' => 'Annahme, aber wegen fehlender Zähne gilt Leistungsstaffel 1!');
		}

		if (isset($g['missingTeethLimit']))
		{
			if ((int)$this->input['missing-teeth'] <= (int)$g['missingTeethLimit'])
				unset($status);		
		}
		if(isset($status))
			$this->addCalculation($status);
	}
		


private function checkRemovableProstehes($g)
	{
		if ((int)$this->input['replaced-teeth-removable'] > (int)$g['removableDenturesLimit'])
		{
			$status = array('status' => 0, 'series' => 0, 'statusText' => 'Ablehnung, da zu viele fehlende herausnehmbare Zähne!');
		} else
		{			
			if ((int)$this->input['replaced-teeth-removable'] <= (int)$g['removableProsthesesLimitSeries2'])
			{
				$status = array('idt' => $this->idt, 'status' => 1, 'series' => 2, 'statusText' => 'Annahme, aber wegen fehlender herausnehmbarer Zähne gilt Leistungsstaffel 2!');				
			} 
			if ((int)$this->input['replaced-teeth-removable'] <= (int)$g['removableProsthesesLimitSeries1'])
			{
				$status = array('idt' => $this->idt, 'status' => 1, 'series' => 1, 'statusText' => 'Annahme, aber wegen fehlender herausnehmbarer Zähne gilt Leistungsstaffel 1!');
			} 
			if ((int)$this->input['replaced-teeth-removable'] <= (int)$g['removableProsthesesLimit'])
			{
				unset($status);		
			} 
		}

		if(isset($status)) 
			$this->addCalculation($status);
	}


private function checkFixedProstehes($g)
	{
			if ((int)$this->input['replaced-teeth-fix'] > (int)$g['fixedDenturesLimit'])
			{
				$status = array('status' => 0, 'series' => 0, 'statusText' => 'Ablehnung, da zu viele fehlende herausnehmbare Zähne!');
			} else
			{			
				if ((int)$this->input['replaced-teeth-fix'] <= (int)$g['fixProsthesesLimitSeries2'])
				{
					$status = array('idt' => $this->idt, 'status' => 1, 'series' => 2, 'statusText' => 'Annahme, aber wegen fehlender herausnehmbarer Zähne gilt Leistungsstaffel 2!');				
				} 
				if ((int)$this->input['replaced-teeth-fix'] <= (int)$g['fixProsthesesLimitSeries1'])
				{
					$status = array('idt' => $this->idt, 'status' => 1, 'series' => 1, 'statusText' => 'Annahme, aber wegen fehlender herausnehmbarer Zähne gilt Leistungsstaffel 1!');
				} 
				if ((int)$this->input['replaced-teeth-fix'] <= (int)$g['fixProsthesesLimit'])
				{
					unset($status);		
				} 
			}

		if(isset($status)) 
			$this->addCalculation($status);
		
	}

private function checkDenturesOlderBy10Years($g)
	{
		if(isset($g['denturesOlderThan10YearsLimit']))
		{
			if(isset($g['denturesOlderThan10YearsLimitSeries2']) && $this->input['replaced-teeth-older-10-years'] <= $g['denturesOlderThan10YearsLimitSeries2']) {
				$status = array('idt' => $this->idt,'status' => 1, 'series' => 2, 'statusText' => 'Annahme, aber wegen Zahnersatz älter als 10 Jahre gilt Leistungsstaffel 2!');
			}
			else if(isset($g['denturesOlderThan10YearsLimitSeries1']) && $this->input['replaced-teeth-older-10-years'] <= $g['denturesOlderThan10YearsLimitSeries1']) {
				$status = array('idt' => $this->idt,'status' => 1, 'series' => 1, 'statusText' => 'Annahme, aber wegen Zahnersatz älter als 10 Jahre gilt Leistungsstaffel 1!');
			}
			else if(isset($g['denturesOlderThan10YearsLimit']) && $this->input['replaced-teeth-older-10-years'] <= $g['denturesOlderThan10YearsLimit']) {
				unset($status);
			}
			else
				$status = array('status' => 0, 'series' => 0, 'statusText' => 'Ablehnung, da Zahnersatz zu alt!');
		}
		if(isset($status))
			$this->addCalculation($status);
	}

private function checkDentalTreatment($g)
	{

		if($this->input['type'] == 0)
		{
			if(isset($g['isPossibleIfOngoingTreatment']) && $g['isPossibleIfOngoingTreatment'] == 0)
			{
				$status = array('status' => 0, 'series' => 0, 'statusText' => 'Ablehnung, da in Behandlung!');
			} 
			else
			{
				#no message needed. accepted.
			}
		}

		if($this->input['type'] == 1)
		{
			if(isset($g['isPossibleWithOngoingDentalTreatmentChilds']) && $g['isPossibleWithOngoingDentalTreatmentChilds'] == 0)
			{
				$status = array('status' => 0, 'series' => 0, 'statusText' => 'Ablehnung, da in Behandlung!');
			} 
			else
			{
				#no message needed. accepted.
			}
		}

		if(isset($status))
			$this->addCalculation($status);
	}


private function checkOrthodonticsTreatment($g)
	{
		if($this->input['type'] == 1)
		{
			if(isset($g['isPossibleWithOngoingOrthodonticsTreatment']) && $g['isPossibleWithOngoingOrthodonticsTreatment'] == 0)
			{
				$status = array('status' => 0, 'series' => 0, 'statusText' => 'Ablehnung, da in kieferorthopädischer Behandlung!');
			} 
			else
			{
				#no message needed. accepted.
			}
		}

		if(isset($status))
			$this->addCalculation($status);
	}


private function checkPeriodontitis($g)
	{
		if((!isset($g['isPossibleIfPeriodontitisInPast']) || $g['isPossibleIfPeriodontitisInPast'] == 1) && (($g['isPossibleIfPeriodontitisHasHealed'] == 1 && $this->input['periodontitis-healed'] == 1) || !isset($g['isPossibleIfPeriodontitisHasHealed']))) 
		{
			$status = null;
		}
		if(!isset($g['isPossibleIfPeriodontitisInPast']) || $g['isPossibleIfPeriodontitisInPast'] == 1)
		{
			if(!isset($g['isPossibleIfPeriodontitisHasHealed']) || ($g['isPossibleIfPeriodontitisHasHealed'] == 1 && $this->input['periodontitis-healed'] == 1))
			{
				$status = null;
			}
			else 
			{
				$status = array('status' => 0, 'series' => 0, 'statusText' => 'Ablehnung, da Parodontose vorhanden und nicht ausgeheilt!');
			}
		}		
		else
		{
			$status = array('status' => 0, 'series' => 0, 'statusText' => 'Ablehnung, da Parodontose vorhanden und nicht ausgeheilt!');
		}

		if(isset($guidelineById['isPossibleIfPeriodontitisInPast']) && $guidelineById['isPossibleIfPeriodontitisInPast'] == 0)
		{
			$status = array('status' => 0, 'series' => 0, 'statusText' => 'Ablehnung, da Parodontose vorhanden!');
		}

		if(isset($status))
			$this->addCalculation($status);
	}

private function checkBiteSplint($g)
	{
		if(isset($g['isPossibleWithSplint']) && $g['isPossibleWithSplint'] == 0)
		{
			$status = array('status' => 0, 'series' => 0, 'statusText' => 'Ablehnung, da Aufbissschiene vorhanden!');
		}
		else
		{
			#no message needed. accepted.
		}
		if(isset($status))
			$this->addCalculation($status);		
	}


public function calculateTariffSurplus()
{

/* nutzen : ZE 
	$this->tariffGuideline['missingTeethInsuranceableLimit'];
	$this->tariffGuideline['riskPremiumMissingTeethPercentage'];
	$this->tariffGuideline['riskPremiumMissingTeethAmount'];
*/
	if(isset($this->input['missing-teeth']) && $this->input['missing-teeth'] > 0)
	{
		if(isset($this->tariffGuideline['riskPremiumMissingTeethPercentage']) && $this->tariffGuideline['riskPremiumMissingTeethPercentage'] > 0)
		{
			$this->surplus += round((double)$this->bonusbase['activeBonusBase'] / 100 * (double)$this->tariffGuideline['riskPremiumMissingTeethPercentage'] * (int)$this->input['missing-teeth'], 2);
			$this->surplusPercentage = array('missing-teeth' => (double)$this->tariffGuideline['riskPremiumMissingTeethPercentage']);
		}
		if(isset($this->tariffGuideline['riskPremiumMissingTeethAmount']) && $this->tariffGuideline['riskPremiumMissingTeethAmount'] > 0 )
		{
			$limit = $this->input['missing-teeth'];
			if($this->tariffGuideline['missingTeethInsuranceableLimit'] && $this->tariffGuideline['missingTeethInsuranceableLimit'] > 0 && $this->tariffGuideline['missingTeethInsuranceableLimit'] < $this->input['missing-teeth'])
				$limit = $this->tariffGuideline['missingTeethInsuranceableLimit'];
			$this->surplus += round($limit * $this->tariffGuideline['riskPremiumMissingTeethAmount'], 2);
			$this->surplusAmount = array('missing-teeth' => round($limit * $this->tariffGuideline['riskPremiumMissingTeethAmount'], 2));
		}
	}
	


/* nutzen : Zuschläge Herausnehmbarer ZE
	$this->tariffGuideline['removableDenturesLimit'];
	$this->tariffGuideline['riskPremiumRemovableDenturesPercentage'];
	$this->tariffGuideline['riskPremiumRemovableDenturesAmount'];
*/

	$limit = $this->input['replaced-teeth-removable'];
	if($this->tariffGuideline['removableDenturesLimit'] && $this->tariffGuideline['removableDenturesLimit'] > 0 && $this->tariffGuideline['removableDenturesLimit'] < $this->input['replaced-teeth-removable'])
		$limit = $this->tariffGuideline['removableDenturesLimit'];


	if(isset($this->tariffGuideline['riskPremiumRemovableDenturesPercentage']) && $this->tariffGuideline['riskPremiumRemovableDenturesPercentage'] > 0)
	{
		$this->surplus += round($limit * (double)$this->bonusbase['activeBonusBase'] / 100 * (double)$this->tariffGuideline['riskPremiumRemovableDenturesPercentage'], 2);
		#$this->surplusPercentage['replaced-teeth-removable'] = (double)$this->tariffGuideline['riskPremiumMissingTeethPercentage'];
	}
	if(isset($this->tariffGuideline['riskPremiumRemovableDenturesAmount']) && $this->tariffGuideline['riskPremiumRemovableDenturesAmount'] > 0 && 
		isset($this->input['replaced-teeth-removable']) && $this->input['replaced-teeth-removable'] > 0)
	{
		$this->surplus += round($limit * $this->tariffGuideline['riskPremiumRemovableDenturesAmount'], 2);
		$this->surplusAmount['replaced-teeth-removable'] = round($limit * $this->tariffGuideline['riskPremiumRemovableDenturesAmount'], 2);
	}


/* nicht nutzen : Zuschläge Festsitzender ZE
		

	$this->tariffGuideline['fixedDenturesLimit'];
	$this->tariffGuideline['riskPremiumFixedDenturesPercentage'];
	$this->tariffGuideline['riskPremiumFixedDenturesAmount'];


	if(isset($this->tariffGuideline['riskPremiumFixedDenturesPercentage']) && $this->tariffGuideline['riskPremiumFixedDenturesPercentage'] > 0)
	{
		$this->surplus += round((double)$this->bonusbase['activeBonusBase'] / 100 * (double)$this->tariffGuideline['riskPremiumFixedDenturesPercentage'], 2);
	}
	if(isset($this->tariffGuideline['riskPremiumFixedDenturesAmount']) && $this->tariffGuideline['riskPremiumFixedDenturesAmount'] > 0 && 
		isset($this->input['replaced-teeth-fix']) && $this->input['replaced-teeth-fix'] > 0)
	{
		$limit = $this->input['replaced-teeth-fix'];
		if($this->tariffGuideline['fixedDenturesLimit'] && $this->tariffGuideline['fixedDenturesLimit'] > 0 && $this->tariffGuideline['fixedDenturesLimit'] < $this->input['replaced-teeth-fix'])
			$limit = $this->tariffGuideline['fixedDenturesLimit'];
		$this->surplus += round($limit * $this->tariffGuideline['riskPremiumFixedDenturesAmount'], 2);
	}
*/


}


private function checkSpecials()
	{
		$missingTeeth = isset($this->input['missing-teeth']) ? (int)$this->input['missing-teeth'] : 0;
		$replacedTeethRemovable = isset($this->input['replaced-teeth-removable']) ? (int)$this->input['replaced-teeth-removable'] : 0;
		$replacedTeethFixed = isset($this->input['replaced-teeth-fix']) ? (int)$this->input['replaced-teeth-fix'] : 0;
		$replacedTeethOlder10Years = isset($this->input['replaced-teeth-older-10-years']) ? (int)$this->input['replaced-teeth-older-10-years'] : 0;
		$toothSum = $missingTeeth + $replacedTeethRemovable;
	
		switch($this->activeIdt)
		{
			//HALLESCHE
				case 227:
				case 228:
				case 229:
				case 230:
				case 231:
				case 232:
					if($toothSum == 0) {
						#no message needed.	accepted.
					} elseif($toothSum < 4) {
						$status = array('status' => 1, 'series' => 1, 'statusText' => '(SPECIAL) Annahme, aber wegen Zahnersatz oder fehlender Zähne gilt Leistungsstaffel 1!');							
					} else {
						$status = array('status' => 0, 'series' => 0, 'statusText' => '(SPECIAL) Ablehnung, da zu viele fehlende Zähne!');
					}
				break;
			//ARAG - combination missing teeth
				case 3:
				case 17:
				case 47:
				case 63:
			//CSS - combination missing teeth
				case 38:
				case 66:
				case 67:
				case 68:	
					if(($toothSum) <= $this->tariffGuideline['missingTeethLimit'])
						{
							#no message needed.	accepted.			
						} 
					else
						{
							$status = array('status' => 0, 'series' => 0, 'statusText' => '(SPECIAL) Ablehnung, da zu viele fehlende Zähne!');				
						}
				break;
			//SIGNAL - points per missing tooth. 40 per missing tooth, 20 per replaced one. 120 points max.
				case 32:
				case 33:
				case 52:
				case 53:
				case 80:
				case 81:
				case 82:
				case 123:
				case 124:
					$points = $missingTeeth * 40 + ($replacedTeethRemovable + $replacedTeethFixed) * 20;
					if ($points <= 120)
					{
						#no message needed.	accepted.			
					} 
					else
					{
						$status = array('status' => 0, 'series' => 0, 'statusText' => '(SPECIAL) Ablehnung, da zu viele fehlende Zähne!');				
					}
				break;
			// CONTINENTALE - Besonderheit ZE älter als 10 Jahre
				case 57:
				case 116:
					if($toothSum < 4 && $replacedTeethOlder10Years < 9)	
					{
						switch($toothSum)
						{
							case 3:
								if($replacedTeethOlder10Years < 6)
									$status = array('status' => 1, 'series' => 1, 'statusText' => '(SPECIAL) Annahme, aber wegen Zahnersatz oder fehlender Zähne gilt Leistungsstaffel 1!');	
								else $status = array('status' => 0, 'series' => 0, 'statusText' => '(SPECIAL) Ablehnung, da zu viele fehlende Zähne und / oder ZE zu alt!');
							break;
							case 2:
								if($replacedTeethOlder10Years < 7)
									$status = array('status' => 1, 'series' => 1, 'statusText' => '(SPECIAL) Annahme, aber wegen Zahnersatz oder fehlender Zähne gilt Leistungsstaffel 1!');								
								else $status = array('status' => 0, 'series' => 0, 'statusText' => '(SPECIAL) Ablehnung, da zu viele fehlende Zähne und / oder ZE zu alt!');
							break;
							case 1:
								if($replacedTeethOlder10Years > 2 && $replacedTeethOlder10Years < 8)
								{
									$status = array('status' => 1, 'series' => 1, 'statusText' => '(SPECIAL) Annahme, aber wegen Zahnersatz oder fehlender Zähne gilt Leistungsstaffel 1!');
								}
								elseif($replacedTeethOlder10Years <= 2)
								{
									/* Annahme */
								}
								else $status = array('status' => 0, 'series' => 0, 'statusText' => '(SPECIAL) Ablehnung, da zu viele fehlende Zähne und / oder ZE zu alt!');
							break;
							case 0:
								if($replacedTeethOlder10Years > 2 && $replacedTeethOlder10Years < 9)
								{
									$status = array('status' => 1, 'series' => 1, 'statusText' => '(SPECIAL) Annahme, aber wegen Zahnersatz oder fehlender Zähne gilt Leistungsstaffel 1!');
								}
								elseif($replacedTeethOlder10Years <= 2)
								{
									/* Annahme */
								}							
								else $status = array('status' => 0, 'series' => 0, 'statusText' => '(SPECIAL) Ablehnung, da zu viele fehlende Zähne und / oder ZE zu alt!');
							break;
						}
					}
					else
					{
						$status = array('status' => 0, 'series' => 0, 'statusText' => '(SPECIAL) Ablehnung, da zu viele fehlende Zähne und / oder ZE zu alt!');	
					}
				break;				
			// WÜRTTEMBERGISCHE - Besonderheit beim Abschluss
				case 25:
				case 26:
				case 27:
				case 28:
				case 29:
				case 30:
				case 233:
					if($toothSum < 5 && $replacedTeethFixed < 17)
					{
						switch($missingTeeth)
						{
							case 4:
								if($replacedTeethFixed<13) {
									$status = array('status' => 1, 'series' => 2, 'statusText' => '(SPECIAL) Annahme, aber wegen Zahnersatz oder fehlender Zähne gilt Leistungsstaffel 2!');
								}
							break;
							case 3:
								if($replacedTeethFixed>9 && $replacedTeethFixed<14) {
									$status = array('status' => 1, 'series' => 2, 'statusText' => '(SPECIAL) Annahme, aber wegen Zahnersatz oder fehlender Zähne gilt Leistungsstaffel 2!');
								} elseif($replacedTeethFixed<10) {
									$status = array('status' => 1, 'series' => 1, 'statusText' => '(SPECIAL) Annahme, aber wegen Zahnersatz oder fehlender Zähne gilt Leistungsstaffel 1!');
								}
							break;
							case 2:
								if($replacedTeethFixed>10 && $replacedTeethFixed<15) {
									$status = array('status' => 1, 'series' => 2, 'statusText' => '(SPECIAL) Annahme, aber wegen Zahnersatz oder fehlender Zähne gilt Leistungsstaffel 2!');
								} elseif($replacedTeethFixed<11) {
									$status = array('status' => 1, 'series' => 1, 'statusText' => '(SPECIAL) Annahme, aber wegen Zahnersatz oder fehlender Zähne gilt Leistungsstaffel 1!');
								}
							break;
							case 1:
								if($replacedTeethFixed>11 && $replacedTeethFixed<16) {
									$status = array('status' => 1, 'series' => 2, 'statusText' => '(SPECIAL) Annahme, aber wegen Zahnersatz oder fehlender Zähne gilt Leistungsstaffel 2!');
								} elseif($replacedTeethFixed<12) {
									$status = array('status' => 1, 'series' => 1, 'statusText' => '(SPECIAL) Annahme, aber wegen Zahnersatz oder fehlender Zähne gilt Leistungsstaffel 1!');
								}
							case 0:
								if($replacedTeethFixed>12 && $replacedTeethFixed<17) {
									$status = array('status' => 1, 'series' => 2, 'statusText' => '(SPECIAL) Annahme, aber wegen Zahnersatz oder fehlender Zähne gilt Leistungsstaffel 2!');
								} elseif($replacedTeethFixed<13) {
									$status = array('status' => 1, 'series' => 1, 'statusText' => '(SPECIAL) Annahme, aber wegen Zahnersatz oder fehlender Zähne gilt Leistungsstaffel 1!');
								}							
						}
						if($toothSum < 3 && $replacedTeethFixed < 9)
						{
							unset($status);
						}																	
					}
					else 
					{
						$status = array('status' => 0, 'series' => 0, 'statusText' => '(SPECIAL) Ablehnung, da zu viele fehlende und oder ersetzte Zähne!');
					}
				break;
		}
		if(isset($status)) {
			$this->addCalculation($status);
		}
}

private function get($var)
{
	if(isset($this->$var))
		return $this->$var;
}


public function doCalc()
	{

		$c = new contract();

		$tl = $c->getTariffList(null, 0); // $insurance, showInCalcResults

		$gl = $c->getTariffGuidelines();

		foreach($gl as $id => $val)
		{
			$g[$val['tariff_id']] = $val;
		}

		$calc = "";


		foreach($tl as $id => $val)
		{
			$this->surplus = 0;
			$this->bonusbase = "";

			#$this->tariffRating = $c->getTariffRating($val['idt']);
			$this->tariffBenefits = $c->getTariffBenefitsInline($val['idt']);
			$this->tariffConfig = $c->getTariffConfig($val['idt']);
			$this->tariffGuideline = $g[$val['idt']];

			$this->idt = $val['idt'];
			$this->tariff = $val;

			$this->age = $c->getAgeByIdt($val['idt'], $this->input['birthdate']); 

			/* calculation bonusbase */
			$this->bonusbase = $c->getBonusBaseWithDate($this->idt, $this->input['birthdate'], $this->input['wishedBegin']);
			
			/* calculation surplus */
			$this->calculateTariffSurplus();

			$this->startCalculation($val['idt']);

			
			if($g[$this->idt]['hasHealthCheck'] == 1)
			{
				if($this->input['type'] == 0)
				{
					if($this->calculation[$val['idt']]['status'] != 0 && isset($g[$this->idt]['maxAge']) && $g[$this->idt]['maxAge']>0)
					{
						$this->checkBirthdate($g[$this->idt]);					
					}

					if($this->calculation[$val['idt']]['status'] != 0 && isset($this->input['missing-teeth']) && $this->input['missing-teeth']<>0)
					{
						$this->checkMissingTeeth($g[$this->idt]);					
					}

					if(isset($this->input['replaced-teeth-removable']) && $this->input['replaced-teeth-removable']<>0)
						$this->checkRemovableProstehes($g[$this->idt]);
					if(isset($this->input['replaced-teeth-fix']) && $this->input['replaced-teeth-fix']<>0)
						$this->checkFixedProstehes($g[$this->idt]);
					if(isset($this->input['replaced-teeth-older-10-years']) && $this->input['replaced-teeth-older-10-years']<>2)	
						$this->checkDenturesOlderBy10Years($g[$this->idt]);

					if($this->calculation[$val['idt']]['status'] != 0 && isset($this->input['dental-treatment']) && $this->input['dental-treatment'] == 1)
					{
						$this->checkDentalTreatment($g[$this->idt]);
					}
					if($this->calculation[$val['idt']]['status'] != 0 && isset($this->input['periodontitis']) && $this->input['periodontitis'] == 1) 
					{
						$this->checkPeriodontitis($g[$this->idt]);
					}
					if($this->calculation[$val['idt']]['status'] != 0 && isset($this->input['bite-splint']) && $this->input['bite-splint']>0)
					{
						$this->checkBiteSplint($g[$this->idt]);
					}
					# now lets check whether we have an insurance with special guidelines.
					if($this->calculation[$val['idt']]['status'] != 0)
						$this->checkSpecials();
				}
				if($this->input['type'] == 1)
				{
					if($this->calculation[$val['idt']]['status'] != 0 && isset($this->input['dental-treatment']) && $this->input['dental-treatment'] == 1)
					{
						$this->checkDentalTreatment($g[$this->idt]);
					}
					if($this->calculation[$val['idt']]['status'] != 0 && isset($this->input['orthodontics-treatment']) && $this->input['orthodontics-treatment'] == 1)
					{
						$this->checkOrthodonticsTreatment($g[$this->idt]);
					}
				}
			

			}
			$this->addCalculation(array('idt' => $this->idt, 'ident' => $val['ident'], 'bonusbase' => $this->bonusbase, 'surplus' => $this->surplus, 'missingTeethInsuranceableLimit' => $this->tariffGuideline['missingTeethInsuranceableLimit'], 'age' => $this->age, 'maxAge' => $this->tariffGuideline['maxAge'], 'benefits' => $this->tariffBenefits));
			$this->addCalculation(array('calculationStartingAgeLid' => $this->get('tariffGuideline')['calculationStartingAgeLid'], 
							'showInCalcResults' => $this->get('tariffConfig')[0]['showInCalcResults'],
							'showFromInPrice' =>  $this->get('tariffConfig')[0]['showFromInPrice'],
							'isChildTariffWithKfoPayment' => $this->get('tariffConfig')[0]['isChildTariffWithKfoPayment'], 
							'showIfDenturesTreatmentProphylaxis' => $this->get('tariffConfig')[0]['showIfDenturesTreatmentProphylaxis'], 
							'onlineContractUrl' => $this->get('tariffConfig')[0]['onlineContractUrl'],
							'backDatingBeginUntilPossible' => $this->get('tariffGuideline')['backDatingBeginUntilPossible'])
			); 


			# TariffRating 
			if($this->calculation[$val['idt']])
				$this->tariffRating = $c->getTariffRating($val['idt'], $this->calculation[$val['idt']]['series']);
			$this->addCalculation($this->tariffRating);

			# TariffAVBDocuments
			#if($this->calculation[$val['idt']])
			#	$this->addCalculation(array('files' => $this->getTariffAVBFile($val['idt'])));

			# PricePerformance
			if($this->input['wishedBegin'] && $this->age)
				$this->calculatePricePerformance();

			# Add PerformanceValues
			$series = 0; $perfomance;
			if($this->calculation[$val['idt']]['series'])
				$series = $this->calculation[$val['idt']]['series'];

			$b = new benefits($series);

			#$b->getShortBenefits($val['idt']);
			$tlim = $b->getTariffLimitations($val['idt']);

			#$tlg = $b->getLimGlobalSeries();
			#$tlze = $b->getLimZahnersatz();

			if(isset($tlim['limglobal']) && is_array($tlim[$tlim['limglobal'][0]]))
			{
				$performance = $tlim[$tlim['limglobal'][0]];
			} elseif ($tlim['dentures']) {
				$performance = $tlim['dentures'];
			}


			$this->addCalculation(array('performance' => $performance));

			/* add campaign data if valid */
			if(($this->tariffConfig[0]['campaignActive'] == 1) 
			&& (
				((strtotime($this->tariffConfig[0]['campaignAvailableFrom']) < time() || !isset($this->tariffConfig[0]['campaignAvailableFrom']) ) && (date("d.m.Y", strtotime($this->tariffConfig[0]['campaignAvailableTill'])) >= date("d.m.Y", time()) || !isset($this->tariffConfig[0]['campaignAvailableTill']) ) )
				||
				($this->tariffConfig[0]['campaignAvailableFrom'] && strtotime($this->tariffConfig[0]['campaignAvailableFrom']) < time() && !isset($this->tariffConfig[0]['campaignAvailableTill']) ) 
				||
				($this->tariffConfig[0]['campaignAvailableTill'] && date("d.m.Y", strtotime($this->tariffConfig[0]['campaignAvailableTill'])) >= date("d.m.Y", time()) && !isset($this->tariffConfig[0]['campaignAvailableFrom']) ) 
				))
			{
				$this->addCalculation(array('campaignShortText' => $this->tariffConfig[0]['campaignShortText'], 'campaignLongText' => $this->tariffConfig[0]['campaignLongText'], 'campaignAvailableTill' => $this->tariffConfig[0]['campaignAvailableTill'], 'campaignActiveOnLandingpages' => $this->tariffConfig[0]['campaignActiveOnLandingpages']));
			}

			$class = array('K-KFO' => $val['isChildTariffWithKfoPayment'], 'ZE+ZB+ZR' => $val['showIfDenturesTreatmentProphylaxis'], 'ZE+ZB' => $val['showIfDenturesTreatment'], 'ZE+ZR' => $val['showIfDenturesProphylaxis'], 'ZB+ZR' => $val['showIfTreatmentProphylaxis'], 'ZE' => $val['showIfDentures'], 'ZB' => $val['showIfTreatment'], 'ZR' => $val['showIfProphylaxis'] );
			$this->addCalculation(array('class' => $class));
		}
	}



}