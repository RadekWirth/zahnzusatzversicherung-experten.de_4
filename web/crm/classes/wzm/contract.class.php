<?php

class contract extends wzm {

private $files;
private $fields;
private $showJustPhone;

// -----------------------------------------------------------------------------

public function __construct(){
	$this->showJustPhone = 0;
}



public function get($idco) {
	$db = $this->getDb();
	$rs = $this->_select('contract', 'idco', $idco);
	return $db->fetchPDO($rs);
}

public function getPidByContractNumber($contractnumber) {
	$db = $this->getDb();
	$rs = $this->_select('contract', 'contractNumber', trim($contractnumber));
	while($row = $db->fetchPDO($rs)) {
		$result[] = $row['pid'];
	}
	return $result;
}

public function set($idco, $data) {
	return $this->_update('contract', $data, 'idco', $idco);
}

public function getByPid($pid) {
	$db = $this->getDb();
	$rs = $this->_select('contract', 'pid', $pid);
	return $db->fetchPDO($rs);
}

public function getLatest($c=30) {
	return $this->_select('contract', null, null, 'signupTime DESC', $c);
}

public function getFromDate($date, $offset=0) {
	if($this->showJustPhone == 0)
	{
		return $this->_select('contract', null, null, 'signupTime DESC', null, $date, $offset);	
	} else
	{
		return $this->getContractsWithPhone($date, $offset);
	}
}

public function getCountFromDate($date, $status=null) {
		return $this->_count('contract', 'idco', $date, $status);	
}

public function getRealCountFromDate($date, $status=null) {
	return $this->realCount('person', 'pid', $date, $status);	
}

public function getIdtWithoutHealthCheck() {
	return $this->_select('guidelines', 'hasHealthCheck', 0);
}


public function deleteByPid($pid) {
	if(empty($pid)) {
		return false;
	}

	$db = $this->getDb();
	$c = $this->getByPid($pid);
	$db->queryPDO("DELETE FROM ".$this->table('contract').
		" WHERE pid='".$pid."'");
	$db->queryPDO("DELETE FROM ".$this->table('dentist').
		" WHERE pid='".$pid."'");
	$db->queryPDO("DELETE FROM ".$this->table('lead').
		" WHERE pid='".$pid."'");
	$db->queryPDO("DELETE FROM ".$this->table('support').
		" WHERE pid='".$pid."'");
	return true;
}

public function getOnlineContractURL($idt)
{
	$db = $this->getDb();
	$rs = $this->_select('config', 'tariff_id', $idt);
	$row = $db->fetchPDO($rs);

	if(isset($row['onlineContractUrl']))
		return $row['onlineContractUrl'];
}

public function getOnlineContractURLs($idt)
{
	$db = $this->getDb();
	$rs = $this->_select('config', 'tariff_id', $idt);
	$row = $db->fetchPDO($rs);

	if(isset($row['onlineContractUrl']) && isset($row['onlineContractUrlShort']))
		return array('onlineurl' => $row['onlineContractUrl'], 'shorturl' => $row['onlineContractUrlShort']);
}

public function getAllOnlineContractURLs()
{
	$db = $this->getDb();
	$rs = $this->_select('config', null, null, null, 1000);
	$row = $db->fetchPDO($rs);

	while(	$row = $db->fetchPDO($rs) )
	{
	if(isset($row['onlineContractUrl']))
		$ret[$row['tariff_id']] = $row['onlineContractUrl'];
	}
	if (!empty($ret))
		return $ret;
}


public function getInsuranceHighlights($idt) {

	$db = $this->getDb();
	$rs = $this->_select('highlights', 'tariff_id', $idt, 'visible_order');

	while(	$row = $db->fetchPDO($rs) )
	{
		$ret[] = $row;
	}

	if(isset($ret))
		return $ret;
}


public function getInsuranceIdent($idt) {
	$db = $this->getDb();

	$sql = "SELECT T0.ident FROM `wzm_insurance` T0 inner join `wzm_tariff` T1 on T1.insurance_id = T0.id AND T1.activeInCrm = 'Y' and T1.idt = '".$idt."'";

	$rs = $db->queryPDO($sql);
	$row = $db->fetchPDO($rs);

	if(isset($row))
		return $row['ident'];
}


/* add 
** Genutzt von (SignUp und LP), mobile Update, zzv-experten.de (neu)	
*/

public function add($data)
{
	$type = $data['source'];

	if(isset($type) && ($type=='mobile' || $type=='zzv-experten.de'))
	{
		/* $data enthält immer nur einen DS */
		$value = $data;
			$person = new person();
			$crm = new crm();
			
					if(isset($value['idt']))
					{
						/* rollback if issue somewhere? */


						/* other person insured? */
						if($value['insure_person_firstname'] && $value['insure_person_lastname'])
						{
							$value['insure_person'] = array('forename' => $value['insure_person_firstname'], 'surname' => $value['insure_person_lastname'], 'birthdate' => $value['insure_person_birthdate'], 'insurance' => $value['insure_person_gkv'], 'job' => $value['insure_person_job']);
							$value['insure_person']['salutationLid'] = $value['insure_person_gender']=='f'?10:9;
							$value['insure_person']['nationcode'] = $value['insure_person_nationality'] == 'german'?'DEU':'';
							$value['insure_person']['nationString'] = $value['insure_person_nationality'] != 'german'?$value['insure_person_nationality']:'';

							$insure_pid = $person->add(stringHelper::str_decode_utf8($value['insure_person']));
						}

						/* create array and add person */
						$value['person'] = array('forename' => $value['person_firstname'], 'surname' => $value['person_lastname'], 'birthdate' => $value['person_birthdate'], 'insurance' => $value['person_gkv'], 'job' => $value['person_job']);
						$value['person']['salutationLid'] = $value['person_gender']=='f'?10:9;
						$value['person']['nationcode'] = $value['person_nationality'] == 'german'?'DEU':'';
						$value['person']['nationString'] = $value['person_nationality'] != 'german'?$value['person_nationality']:'';

						if(isset($insure_pid))
							$value['person']['isFatherOfPid'] = $insure_pid;

						$pid = $person->add(stringHelper::str_decode_utf8($value['person']));

			
						/* add address and contact */
						$value['address'] = array('street' => $value['address_street'], 'postcode' => $value['address_postcode'], 'city' => $value['address_city']);
						$crm->addAddress(stringHelper::str_decode_utf8($value['address']), 'pid', $pid, 'primary');

						$value['contact'] = array('phone' => $value['contact_phone'], 'email' => $value['contact_email']);
						$crm->addContact(stringHelper::str_decode_utf8($value['contact']), 'pid', $pid, 'primary');

			
						/* add support message if set */
						if(!empty($value['contact_msg'])) {
							$this->addSupportMessage(stringHelper::str_decode_utf8($value['contact_msg']), $pid);
						}

						/* lets go and add the contract... */
						$value['contract'] = array(
							'middleware_id'	=> $value['id'],
							'idt' 			=> $value['idt'], 
							'pid' 			=> $pid, 
							'series'		=> $value['series'],
							'birthdate' 		=> $value['person_birthdate'],
							'gender' 		=> ($value['person_gender']=='f'?'female':'male'), 
							'wishedBegin' 	=> $value['wished_begin'],
							'contractType' 	=> ($value['child']==0?'adult':'kids'), 
							'bonusbase' 		=> $value['baseprice'],
							'bonus'		=> $value['surplus'],
							'price' 		=> $value['display_price'],
							'signupTime' 		=> $value['created_at'], 
							'sourcePath'		=> $value['sourcePath'],
							'angebot'		=> 3, 
							'browserAgent'	=> $value['browserAgent'], 
							'sourcePage'		=> $value['source']
						); 

						$insurance = array(1 => 'gkv', 2 => 'heil', 3 => 'pkv');

						
						/* check fields if they are set */
						if(isset($value['missingTeeth']))
							$value['contract']['tooth1'] = $value['missingTeeth'];
						if(isset($value['prosthesis']))
							$value['contract']['tooth3'] = $value['prosthesis'];
						if(isset($value['implantants']))
							$value['contract']['tooth4'] = $value['implantants'];
						if(isset($value['olderTenYrs']))
							$value['contract']['tooth5'] = $value['olderTenYrs'];
						if(isset($value['inCare']))
							$value['contract']['incare'] = ($value['inCare']==1?'yes':'no');
						if(isset($value['periodontitis']))
							$value['contract']['periodontitis'] = ($value['periodontitis']==1?'yes':'no');
						if(isset($value['periodontitisCured']))
							$value['contract']['periodontitisCured'] = ($value['periodontitisCured']==1?'yes':'no');
						if(isset($value['orthodonticsTreatment']))
							$value['contract']['orthodontics'] = ($value['orthodonticsTreatment']==1?'yes':'no');
						if(isset($value['dentalTreatment']))
							$value['contract']['incare'] = ($value['dentalTreatment']==1?'yes':'no');
						if(isset($value['biteSplint']))
							$value['contract']['biteSplint'] = ($value['biteSplint']==1?'yes':'no');



						if(isset($value['insurancetype']))
						{
							$value['contract']['insurance'] = $insurance[$value['insurancetype']];
						}

						/* lets go and add the contract... */

						$idco = $this->addContract(stringHelper::str_decode_utf8($value['contract']));
						$this->updateContractUA($idco);
						return $idco;
					}

			




	} else if (isset($type) && $type=='homepage') {
		return -1;
	}
	/* type nicht richtig, abbruch */
	return -1;
}


/* 
** sendContractMail - sends just the contract mail out
** @vars:
** $idco = idco that should be sent out
** $complete = 1 / 2 | 0 = off
** $reset = 1 -> empty form, $reset = 0 -> filled
*/

public function sendContractMail($idco, $complete=2, $reset=0)
{
	$crm = new crm();
	$db = $this->getDb();
	$p = new person();

	if(empty($idco)===true) {
		errorHandler::throwErrorDb('Variable $idco not set!', $idco);
	}

	$c = $this->getContract($idco);
	$tariff = $this->getTariff($c['idt']);
	$contact = $crm->getContactPrimary('pid', $c['pid'], true);
	$address = $crm->getAddressPrimary('pid', $c['pid'], true);
	$person = $p->get($c['pid'], true);

	if(isset($person['isFatherOfPid']))
	{
		$personInsured = $p->get($person['isFatherOfPid'], true);
	}

	$rs_support = $this->getSupportMessages( $c['pid'] );
	$support = $db->fetchPDO($rs_support);

	$ocURL = $this->getOnlineContractURL($c['idt']);

	$path = project::getProjectRoot();

	if(empty($path)===true) {
		errorHandler::throwErrorDb('Variable $path not set!', $idco);
		$path = PATH_TO_LIVE;
	}

	
	errorHandler::throwErrorDb('all steps successful - going on with sendMail', $idco.' '.$path);
	// send user info mail - Auslagern...
	$recipient = $contact['email'];

	$subject = 'Zahnzusatzversicherung-Experten - Vielen Dank für Ihre Online-Anforderung - ';
	$salutation = ($person['salutationLid']==9)?
		'Sehr geehrter Herr '.$person['surname']:
		'Sehr geehrte Frau '.$person['surname'];

	$insuredPerson ="";
	if(isset($person['isFatherOfPid']))
	{
		$insuredPerson = "\n\n"."Zu versichernde Person : "."\n\n".print_r(stringHelper::str_decode_utf8($personInsured), true);
	}
	

	$mailBody =
		"Absende-Zeit: ".date("d.m.Y H:i:s").
		"\n\n".
		"Tarif: ".$tariff['name'].
		"\n\n"."Beitrag : ".$c['price']."\n\n".
		"\n\n"."Support : "."\n\n".
		print_r(stringHelper::str_decode_utf8($support), true).
		"\n\n"."SourcePfad : ".$c['sourcePath']."\n\n".
		"\n\n"."Versicherungsnehmer : "."\n\n".
		print_r(stringHelper::str_decode_utf8($person), true).
		$insuredPerson.
		"\n\n"."Kontakt : "."\n\n".
		print_r(stringHelper::str_decode_utf8($address), true).
		print_r(stringHelper::str_decode_utf8($contact), true).

		"\n\n"."Vertragsdaten : "."\n\n".
		print_r(stringHelper::str_decode_utf8($c), true);


	if($person['salutationLid']==9) {
		$person['salutationText'] = 'Herr';
	} else $person['salutationText'] = 'Frau';

	if($personInsured['salutationLid']==9) {
		$personInsured['salutationText'] = 'Herr';
	} else $personInsured['salutationText'] = 'Frau';
	unset($c['browserDeviceDetail']);
	unset($c['browserAgent']);


	// data build for zoho
	$c['tariffName'] = $tariff['name'];
	$zoho = $this->getZohoData(array('support' => $support, 'person' => $person, 'personInsured' => $personInsured, 'address' => $address, 'contact' => $contact, 'contract' => $c));

/*
	$zoho = "Support:".stringHelper::str_decode_utf8($support['message'])."\n\n"; 
	$zoho .= "Person:".stringHelper::arrayToZoho($person)."\n\n"; 
	$zoho .= "PersonInsured:".stringHelper::arrayToZoho($personInsured)."\n\n"; 
	$zoho .= "Address:".stringHelper::arrayToZoho($address)."\n\n"; 
	$zoho .= "Contact:".stringHelper::arrayToZoho($contact)."\n\n"; 
	$zoho .= "Contract:".stringHelper::arrayToZoho($c);
*/	


	emailHelper::sendHTMLMail('allgemein@vm-experten.de', 'Online Rechner Antrag', $mailBody);
	emailHelper::sendHTMLMail('zzvantrageingang@vmexperten.zohodesk.eu', 'Online Rechner Antrag', $zoho);


	if(!file_exists($path.'/templates/mail-request-template.tpl')) {
		errorHandler::throwErrorDb($path.'/templates/mail-request-template.tpl not found', $idco);
		return -1;
	}
	$message = str_replace('{*ANREDE_NAME*}', $salutation, file_get_contents($path.'/templates/mail-request-template.tpl'));

	if($ocURL)
	{
		$message = str_replace('{*ONLINE_SIGNUP_PARTIAL*}', file_get_contents($path.'/templates/mail-request-online-signup-template.tpl'), $message);
		$message = str_replace('{*ONLINE_SIGNUP_LINK*}', $ocURL, $message);
	} else
	{
		$message = str_replace('{*ONLINE_SIGNUP_PARTIAL*}', '', $message);
	}

	$message = str_replace('{*ONLINE_SIGNUP_PARTIAL*}', '', $message);

	$message = str_replace("{*INSURANCE_NAME*}", $tariff['name'], $message);
	$message = str_replace("{*ADD_LIST_ITEMS*}", $tariff['mail_message'], $message);
	$message = str_replace("{*SIGNATURE*}", project::gts('html'), $message);
	$message = str_replace("{*CSS_OPTION*}", "", $message);

		if($c['price'] == 0)
		{
			eMailHelper::sendHTMLMail('anfrage@consulting-wirth.de', 'ZZV - Fehler in Antrag - Rechner = 0', print_r($c));
		}


	$subject = "Ihr persönliches Angebot für eine ".$tariff['name']." - Zahnzusatzversicherung";

	$internhandle = new crmIntern();
		errorHandler::throwErrorDb('(info) step 1 before pdf creation', $idco.' '.$path);
	$pdf = $internhandle->genContractPdfByImage($idco, null, $complete, $reset, null, $path);
		errorHandler::throwErrorDb('(info) step 2 after pdf creation', $idco.' '.$path);
	$internhandle->outputPDF($pdf, $idco);

	$f = $this->getMailerFiles($idco, $c);

	if(!file_exists($f[0])) {
		errorHandler::throwErrorDb('generated File not found - '.$f[0], $idco);
		errorHandler::throwErrorDb('mail was NOT send out for idt '.$idt, $idco);
	}
	else {
		$status = emailHelper::sendMailWithFiles($recipient, $subject, $message, $f, $c['idt']);
		errorHandler::throwErrorDb('mail sent out for idt '.$idt, $idco.'-'.print_r($status,true));
	}
	return $status;
}

public function getMailerFiles($idco, $c)
{
	// Antrag hinzufügen
	$path = project::getProjectRoot();

	if(empty($path)===true) {
		errorHandler::throwErrorDb('Variable $path not set!', $idco);
		$path = PATH_TO_LIVE;
	}


	$file = $path.'/pdf/Antrag - '.$idco.'.pdf';
	$f = array($file);

	// Erstinformationen hinzufügen
	$file = $path.'/tl_files/files/vmexperten.Erstinformation.pdf';
	array_push($f, $file);


	$files = $this->getTariffFiles($c['idt']);
	if(isset($files) && is_array($files['file']) && isset($files['file']))
	{
		if(count($files['file'])>=1)
		{
	
			foreach($files['file'] as $id=>$value)
			{
				array_push($f, $value);
			}

		} 
		else
		{
			array_push($f, $files['file']);
		}
		
		
		errorHandler::throwErrorDb('pdf files added ', print_r($files, true));
	} else {
		errorHandler::throwErrorDb('pdf files not added ', print_r($files, true).' '.$path);
	}


	errorHandler::throwErrorDb('pdfs to be sent out ', print_r($f, true));

	return $f;	
}


public function getZohoData($ary)
{
	if(is_array($ary) && $ary['contract'])
	{
		if($ary['support'])
			$zoho = "Support:".stringHelper::str_decode_utf8($support['message'])."\n\n";

		if($ary['person'])
			$zoho .= "Person:".stringHelper::arrayToZoho($person)."\n\n";

		if($ary['personInsured'])
			$zoho .= "PersonInsured:".stringHelper::arrayToZoho($personInsured)."\n\n"; 

		if($ary['address'])
			$zoho .= "Person:".stringHelper::arrayToZoho($address)."\n\n";

		if($ary['contact'])
			$zoho .= "Person:".stringHelper::arrayToZoho($contact)."\n\n";

		if($ary['contract'])
			$zoho .= "Person:".stringHelper::arrayToZoho($c)."\n\n";
	}

	return $zoho;
}


private function getTariffAVBDocument($idt)
{
	// returns link to AVB Document
	// type = 1 general
	// type = 2 tariff-spec

	$sql = "SELECT TOP 1 filename FROM wzm_filestore f JOIN wzm_filestore_tariff ft on ft.filestore_id = f.id WHERE ft.tariff_id = ". $idt ." AND f.document_type_id = 1";

	$db = $this->getDb();
	$rs = $db->queryPDO($sql);
	$ret = '';
	$row = $db->fetchPDO($rs);

	return $row['filename'];
}

public function getFinancetestRating($idt) {
	$tariff = $this->geTariff($idt);

	if($tariff['financeTest'])
		{
			$rating = $tariff['financeTest'];
			$html = '<h4>Finanztest 05-2018</h4>';
			$html .= '<p style="font-size: 15px; line-height: 1.3;">Testurteil &bdquo;'. self::translateFinancetestRatingIntoText($rating) .'&ldquo;<br>(<span class="financetest-rating">Note '. $rating .'</span>)</p><br>';
			$html .= '<p>Im Test: 220 Zahnzusatz-Tarife</p>';
		}
		elseif (project::hasFinancetestRatingText($idt)) {
			$html = '<p>'. self::getFinancetestRatingText($idt) .'</p>';
		}
		else {
			$html = '<p>keine Bewertung</p>';
		}

	return $html;
}




public function getOpenContractsFromMiddleware ($count=1) 
{
	$sql = "SELECT * FROM ".$this->table('middleware')." middle where middle.to_transfer = 1 and (middle.transferred is null or middle.transferred <>1) LIMIT ".$count;
	
	$db = $this->getDb();
	$rs = $db->queryPDO($sql);
	$ret = array();

	while($row = $db->fetchPDO($rs)) {
		$ret[] = $row;
	}
	return $ret;
}


public function setMiddlewareToTransferred ($id)
{
	if(isset($id) && is_numeric($id)===true) {
		$sql = "UPDATE ".$this->table('middleware')." SET transferred = 1 where id = ".$id;

		$db = $this->getDb();
		$db->queryPDO($sql);
	}
}	

public function addContractToMiddleware ($data)
{
	$db = $this->getDb();
	$date = date("Y-m-d H:i:s");

	$valuesSql = stringHelper::arrayToSql($data);

	#return "INSERT INTO ".$this->table('middleware')." SET ".$valuesSql;

	$rs = $db->queryPDO("INSERT INTO ".$this->table('middleware')." SET ".$valuesSql);
	return $db->getLastInsertedId();
}


public function getSpecialTarifCustomers($age)
{
	/* 
	** age :: 0 = 21 - 25
	** age :: 1 = 26 - 30
	** age :: 2 = 31 - 38
	*/

	switch($age)
	{
		case 2:
			$sql_add = "BETWEEN 31 and 38";
			break;
		case 1:
			$sql_add = "BETWEEN 26 and 30";
			break;
		case 0:
			$sql_add = "BETWEEN 21 and 25";
			break;
		default: 
			$sql_add = "= 0";
	}

	$sql = "SELECT c.idco, c.pid, c.birthdate, p.*, a.*  FROM wzm_contract c
	JOIN crm_person p ON p.pid = c.pid AND p.isFatherOfPid IS null
	JOIN crm_address_map am ON am.pid = p.pid
	JOIN crm_address a ON a.adid = am.adid
	WHERE c.contractStatusLid IN (49, 50) AND 
	DATEDIFF(CURRENT_DATE, STR_TO_DATE(c.birthdate, '%Y-%m-%d'))/365 ".$sql_add." AND
	c.idco not in (SELECT idco from wzm_tariff_special_offer) LIMIT 50; ";


	$db = $this->getDb();
	$rs = $db->queryPDO($sql);

	while($row = $db->fetchPDO($rs)) {
		$ret[] = $row;
	}
	return $ret;


	
}

/* getInsuranceList
** Genutzt durch die rest-Schnittstelle /rest
*/

public function getInsuranceList($insurance=null) {

	$sqlWhere = '';
	if(isset($insurance))
	{
		if(is_numeric($insurance))
			$sqlWhere = " AND wi.id = ".$insurance;
		else
			$sqlWhere = " AND wi.ident = '".$insurance."'";
	} 
	
	$db = $this->getDb();

	$sql = "SELECT * from ".$this->table('insurance')." wi WHERE 1=1  ".$sqlWhere;

	$rs = $db->queryPDO($sql);
	while($row = $db->fetchPDO($rs)) {
		$ret[] = $row;
	}
	return $ret;

}

private function strClean($strin) {
        $strout = null;

        for ($i = 0; $i < strlen($strin); $i++) {
                $ord = ord($strin[$i]);

                if (($ord > 0 && $ord < 32) || ($ord >= 127)) {
                        $strout .= "&amp;#{$ord};";
                }
                else {
                        switch ($strin[$i]) {
                                case '<':
                                        $strout .= '&lt;';
                                        break;
                                case '>':
                                        $strout .= '&gt;';
                                        break;
                                case '&':
                                        $strout .= '&amp;';
                                        break;
                                case '"':
                                        $strout .= '&quot;';
                                        break;
                                default:
                                        $strout .= $strin[$i];
                        }
                }
        }

        return $strout;
}


/* getTariffList
** Genutzt durch die rest-Schnittstelle /rest
*/

public function getTariffList($insurance=null, $showInCalcResults=0) {

	$sqlWhere = '';
	if(isset($insurance))
	{
		if(is_numeric($insurance))
			$sqlWhere = " AND wt.idt = ".$insurance;
		else
			$sqlWhere = " AND wt.url_alias = '".$insurance."' ";
	} 

	if($showInCalcResults == 0)
	{
		$sqlShowInCalcResults = "";
	}	
	else
	{
		$sqlShowInCalcResults = "and c.showInCalcResults = ".$showInCalcResults;		
	}

	$db = $this->getDb();

	$sql = "SELECT wt.idt, wt.insurance_id, wt.name as tariff, wt.tariffName, wt.rating, wt.kidsRating, wt.url_alias, wi.name, wi.short_name, wi.ident, wi.url_alias as insurance_url_alias, wi.company, wi.street, wi.zip, wi.post_box, wi.city, wi.email, wsm.title as seo_title, wsm.meta_description, wtb.*, c.* 
			from ".$this->table('tariff')." wt 
			inner join ".$this->table('insurance')." wi on wi.id = wt.insurance_id 
			inner join ".$this->table('tariff-benefits')." wtb on wtb.tariff_id = wt.idt
			inner join ".$this->table('config')." c on c.tariff_id = wt.idt ".$sqlShowInCalcResults." and c.isActiveInFrontend = 1
			left join ".$this->table('seo-meta')." wsm on wsm.id = wt.seo_meta_id WHERE 1=1  ".$sqlWhere;

	$rs = $db->queryPDO($sql);
	while($row = $db->fetchPDO($rs)) {
		if($row['campaignLongText'] || $row['campaignShortText'])
			{
				#$row['campaignLongText'] = htmlentities($row['campaignLongText']); 
				#$row['campaignShortText'] = htmlentities($this->strClean($row['campaignShortText']));
			}
		$ret[] = $row;
	}
	return $ret;
}


/* getTariffBenefitLimitation
** Genutzt durch die rest-Schnittstelle /rest
*/

public function getTariffBenefitLimitation($insurance=null) {


	if(isset($insurance))
	{
		if(is_numeric($insurance))
			$sqlWhere = " AND wt.idt = ".$insurance;
		else
			$sqlWhere = " AND wt.url_alias = '".$insurance."' ";
	} 
		

	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT wt.idt, wt.name as tariff, wt.tariffName, wtb.*, wtbl.*	from wzm_tariff wt inner join wzm_tariff_benefits wtb on wtb.tariff_id = wt.idt inner join wzm_tariff_benefits_limitations wtbl on wtbl.tariff_id = wtb.tariff_id WHERE 1=1 ".$sqlWhere);
	while($row = $db->fetchPDO($rs)) {
		$ret[] = $row;
	}
	return $ret;
}


/* getTariffRating
** Genutzt durch die rest-Schnittstelle /rest
*/

public function getTariffRating($idt=null, $series=0) {

	$ret = $this->getRating($idt, $series);
	return $ret;
}


/* getTariffMenu
** Genutzt durch die rest-Schnittstelle /rest
*/

public function getTariffMenu() {


	$db = $this->getDb();
	$rs = $db->queryPDO("
		SELECT wi.name, wi.url_alias insurance_url_alias, wt.idt, wt.url_alias, wt.tariffName FROM `wzm_insurance` wi INNER JOIN `wzm_tariff` wt ON wt.insurance_id = wi.id "
	);
	while($row = $db->fetchPDO($rs)) {
		$ret[] = $row;
	}
	return $ret;
}





public function setStatus($idco, $status, $date) {
// 46 = Antrag noch nicht rausgeschickt
// 48 = Antrag rausgeschickt
// 49 = Antrag zurÃ¼ck

	$pos = strpos($date, ".");
	if($pos === false) die('Datum nicht im richtigen Format!');

	$dat = explode(".", $date); $dat=$dat[2].'-'.$dat[1].'-'.$dat[0];
	$set = array('contractStatusLid' => $status);
	#if($status==48) $set['dateSent'] = date("Y-m-d");
	#if($status==49) $set['dateRec'] = date("Y-m-d");
	#if($status==50) $set['datePolice'] = date("Y-m-d");

	if($status==48) $set['dateSent'] = $dat;
	if($status==49) $set['dateRec'] = $dat;
	if($status==50) $set['datePolice'] = $dat;
	return $this->_update('contract', $set, 'idco', $idco);
}

private function getCountByStatus($status) {
	$db = $this->getDb();
	$rs = $db->queryPDO("SELECT COUNT(pid) AS NumCount FROM ".$this->table('contract')." WHERE contractStatusLid='".$status."'");
	$row = $db->fetchPDO($rs);
	return $row['NumCount'];
}

public function getPidsByStatus($status, $page) {
	$thisCount = $this->getCountByStatus($status);
	if(!isset($page) && $thisCount>50) $page=1;
	$limit = $order = "";
	if($status!="46") $order = " DESC ";
	if($page!=0) $limit = " LIMIT ".(($page*50)-50).", 50";
	$db = $this->getDb();
	$ret['page'] = $page;
	$ret['count'] = $thisCount;
	$rs = $db->queryPDO("SELECT pid FROM ".$this->table('contract')." WHERE contractStatusLid='".$status."' ORDER BY pid".$order.$limit);
	while($row = $db->fetchPDO($rs)) {
		$ret[] = $row['pid'];
	}
	return $ret;
}

public function getOpenIdco($count) {
	/*
	** Diese Funktion liefert ein Array der offenen Anträge - Anzahl steht in $count
	*/

	if($count<=0) $count = 1;

	$db = $this->getDb();
	$status = 46; // offen
	$sqlst = "SELECT idco FROM ".$this->table('contract')." WHERE contractStatusLid='".$status."' LIMIT 0, ".$count;

	$rs = $db->queryPDO($sqlst);

	while($row = $db->fetchPDO($rs))
	{
		$ret[] = $row['idco'];
	} 

	if(count($ret)>0)
		return $ret;
}


public function setMailSentDate($idco, $dat) {
	/*
	** Diese Funktion setzt ein Datum in wzm_contract.contractMailSent
	*/

	$e = $this->_update('contract', array('contractMailSent' => date("Ymd", $dat)) , 'idco', $idco);
	print $e;
}

public function getAllContractsNotSent($sourcePage='zzv-neu') {
	/*
	** Diese Funktion liefert alle Anträge, die noch nicht per Mail verschickt wurden
	*/

	$db = $this->getDb();
	$sqlst = "SELECT idco FROM ".$this->table('contract')." WHERE 1=1 AND sourcePage ='".$sourcePage."' AND (contractMailSent is NULL OR contractMailSent = '0000-00-00') AND idco >= 139492 ";

	$rs = $db->queryPDO($sqlst);

	while($row = $db->fetchPDO($rs))
	{
		$ret[] = $row['idco'];
	} 

	if(count($ret)>0)
		return $ret;	
}

public function updateContractUA($idco=null) 
{
	$ua = $this->getContractsWithUA();

	$bh = '';

	foreach($ua as $id => $val)
	{
		#print '<pre>'.$val['idco'].'</pre>';
		$bh = browserHelper::detect($val['browserAgent']);
		$ary = array('browserName' => $bh['name'], 'browserVersion' => $bh['version'], 'browserPlatform' => $bh['platform'], 'browserDevice' => $bh['device'], 'browserDeviceDetail' => $bh['device_detail']);
		$e = $this->_update('contract', $ary, 'idco', $val['idco']);

		if($e) print $e;
	}
}

public function getContractsWithUA()
{
	$db = $this->getDb();

	$sqlst = "SELECT idco, browserAgent FROM ".$this->table('contract')." WHERE 1=1 AND LENGTH(browserAgent)>0 AND (LENGTH(browserDevice) = 0 OR browserDeviceDetail = 'unknown') ";

	$rs = $db->queryPDO($sqlst);

	while($row = $db->fetchPDO($rs))
	{
		$ret[] = $row;
	} 

	if(count($ret)>0)
		return $ret;	
}

public static function checkWishedBegin($idt, $wishedBegin)
{
/*
** Ermittlung der richtigen Abschlusszeit
** Bei Tarifen, die nicht in der Vergangenheit abgeschlossen werden können, muss ein neues
** wishedBegin ermittelt werden
*/
	$wzm = new wzm();
	// returns new (or old) wishedBegin in Format time()
	$tariff = $wzm->getTariff($idt); 

	if($tariff['InsuranceBeginInPast'] == 'N' && time()>$wishedBegin)
		$wishedBegin = mktime(0,0,0, 1+ date("n", $wishedBegin), date("j", $wishedBegin), date("Y", $wishedBegin));

	return $wishedBegin;
}

public function getDbInsuranceOrder($age, $insuranceType, $start = 4) {
/*
** DB Umsetzung des Rechners
**
** $age = Alter
** $insuranceType = heil für heilfürsorge (noch nicht umgesetzt) / sonst gesetzlich versichert.
** 
*/

	$sqladd = "";

	/* mit diesem Zusatz wird die init. Anzeige im Rechner dargestellt für einen schnelleren Aufbau */
	if($start && ($start == 4 || $start == 8)) 
		$sqladd = " AND cssub.id = ".$start;

	$db = $this->getDb();

	$sqlst = 'SELECT cs.NAME headerText, cssub.NAME subText, tcc.id category_id, tcc.step_id, tcc.step_sub_id, tcc.text, tc.idt, tc.visOrder, tc.fromAge, tc.tillAge, t.name, t.rating, t.ageProvision, t.financeTest FROM wzm_tariff_calc_steps cs
	INNER JOIN wzm_tariff_calc_steps cssub ON cssub.parentId = cs.id '.$sqladd.'
	INNER JOIN wzm_tariff_calc_category tcc ON tcc.step_id = cs.id AND tcc.step_sub_id = cssub.id
	INNER JOIN wzm_tariff_calc tc ON tc.category_id = tcc.id
	INNER JOIN wzm_tariff t ON t.idt = tc.idt
	WHERE cs.parentId = 0 AND cssub.parentId = 1 AND tc.isActive = 1

	ORDER BY 3, 4, 5, 8; ';

	$rs = $db->queryPDO($sqlst);

	$i = 1; $j = 1;
	$header = "root"; $subHeader = "root"; $text = "root";

	while($row = $db->fetchPDO($rs))
	{
		if($header && $header != $row['headerText'])
			$header = utf8_encode($row['headerText']);

		if($subHeader && $subHeader != $row['subText'])
			$subHeader = utf8_encode($row['subText']);

		if($text && $text != $row['text'])
			$text = utf8_encode($row['text']);


		/* Ausnahme im Rechner aktuell nur für ERGO */
		if(isset($row['fromAge']) && isset($row['tillAge']) && $row['fromAge'] <= $age && $row['tillAge'] >= $age)
		{
			/* do not print line */
		} else {
			$ret[$header][$subHeader][$text][] = array('idt' => $row['idt'], 'name' => utf8_encode($row['name']), 'rating' => $row['rating'], 'ageProvision' => $row['ageProvision']);
		}
	} 

	return $ret;

}

public function getInsuranceOrder($age, $insuranceType) {
/*
** Erstmalige Übertragung der Order in contract - später in der DB.
*/
		$ids = project::gta();
/*
print "<pre>";
print_r($ids);
print "</pre>";
*/
		$steps[1][1] = array(
			"Top-Leistungen"=>array(
						$ids['bayerische-dentplus'],
						$ids['barmenia-zahn100-zv'],
						$ids['bayerische-zahn-prestige'],
						$ids['nuern-z100'],
						$ids['dfv-exklusiv'],
						$ids['hallesche-gigadent'],
						$ids['wuerttembergische-v1'],
						$ids['dkv-kdtp100-kdbe'],
						$ids['hansemerkur-ezl'],
						$ids['gothaer-mediz-duo'],
						$ids['barmenia-zahn90-zv'],
						$ids['allianz-dent-best'],
						$ids['ukv-zp-premium'],
						#$ids['wb-d1-ze90-zbe'],
						$ids['bayerische-zahn-komfort'],
						$ids['nuern-z90'],
						$ids['mv-571-572-573-574'],
						$ids['hallesche-megadent'],
						$ids['axa-dent-premium'],
						$ids['rv-premium-z1zv'],
						#$ids['sigidu-zahn-top-pur'],
						$ids['ergo-direkt-premium'],
						$ids['wuerttembergische-v2'],
						$ids['hansemerkur-ezk']
						),
			"Gute Alternativen"=>array(
						$ids['universa-uni-dent-privat'],
						$ids['barmenia-zahn80-zv'],
						$ids['inter-z90-zpro'],
						$ids['bayerische-zahn-smart'],
						$ids['conticezp-u'],
						$ids['nuern-z80'],
						$ids['ukv-zp-optimal'],
						$ids['hansemerkur-ez-ezt-ezp'],
						$ids['wuerttembergische-v3'],
						$ids['dkv-kdt85-kdbe'],
						#$ids['barzgplus'],
						$ids['janitos-ja-dental-plus'],						
						$ids['aragz90'],
						$ids['sigidu-komplus']
						),
			"G&uuml;nstige Basisabsicherung"=>array(
						$ids['allianz-dent-plus'],
						#$ids['wb-d2-ze70-zbe'],
						$ids['axa-dent-komfort'],
						$ids['rv-comfort-z2zv'],
						$ids['ukv-zp-kompakt'],
						$ids['sigidu-komstart']
						),
			"wichtige Leistungsvorgaben werden nicht erf&uuml;llt (keine Prophylaxe & ZB)"=>array(
						$ids['barmenia-zahn100'],
						$ids['dkv-kdtp100'],
						#$ids['hallesche-dentze100'],
						$ids['rv-premium-z1'],
						$ids['wuerttembergische-z1'],
						$ids['barmenia-zahn90'],
						#$ids['wb-p1-ze90'],
						#$ids['hallesche-dentze90'],
						#$ids['mv-571'],
						#$ids['inter-z90'],
						$ids['dkv-kdt85'],
						$ids['hansemerkur-ez-ezt'],
						$ids['barmenia-zahn80'],
						$ids['wuerttembergische-z2'],
						#$ids['wb-p2-ze70'],
						$ids['rv-comfort-z2'],
						$ids['wuerttembergische-z3']
						)
			);

	if($age<=30) {
		$steps[1][1] = array(
			"Top-Leistungen"=>array(
						$ids['bayerische-dentplus'],
						$ids['barmenia-zahn100-zv'],
						$ids['bayerische-zahn-prestige'],
						$ids['nuern-z100'],
						$ids['dfv-exklusiv'],
						$ids['hallesche-gigadent'],
						$ids['wuerttembergische-v1'],
						$ids['dkv-kdtp100-kdbe'],
						$ids['hansemerkur-ezl'],
						$ids['gothaer-mediz-duo'],
						$ids['barmenia-zahn90-zv'],
						$ids['allianz-dent-best'],
						$ids['ukv-zp-premium'],
						#$ids['wb-d1-ze90-zbe'],
						$ids['bayerische-zahn-komfort'],
						$ids['nuern-z90'],
						$ids['mv-571-572-573-574'],
						$ids['hallesche-megadent'],
						$ids['axa-dent-premium'],
						$ids['rv-premium-z1zv'],
						#$ids['sigidu-zahn-top-pur'],
						$ids['wuerttembergische-v2'],
						$ids['hansemerkur-ezk']
						),
			"Gute Alternativen"=>array(
						$ids['universa-uni-dent-privat'],
						$ids['barmenia-zahn80-zv'],
						$ids['inter-z90-zpro'],
						$ids['bayerische-zahn-smart'],
						$ids['conticezp-u'],
						$ids['nuern-z80'],
						$ids['ukv-zp-optimal'],
						$ids['hansemerkur-ez-ezt-ezp'],
						$ids['wuerttembergische-v3'],
						$ids['dkv-kdt85-kdbe'],
						#$ids['barzgplus'],
						$ids['janitos-ja-dental-plus'],						
						$ids['aragz90'],
						$ids['ergo-direkt-premium'],
						$ids['sigidu-komplus']
						),
			"G&uuml;nstige Basisabsicherung"=>array(
						$ids['allianz-dent-plus'],
						#$ids['wb-d2-ze70-zbe'],
						$ids['axa-dent-komfort'],
						$ids['rv-comfort-z2zv'],
						$ids['ukv-zp-kompakt'],
						$ids['sigidu-komstart']
						),
			"wichtige Leistungsvorgaben werden nicht erf&uuml;llt (keine Prophylaxe & ZB)"=>array(
						$ids['barmenia-zahn100'],
						$ids['dkv-kdtp100'],
						#$ids['hallesche-dentze100'],
						$ids['rv-premium-z1'],
						$ids['wuerttembergische-z1'],
						$ids['barmenia-zahn90'],
						#$ids['wb-p1-ze90'],
						#$ids['hallesche-dentze90'],
						#$ids['mv-571'],
						#$ids['inter-z90'],
						$ids['dkv-kdt85'],
						$ids['hansemerkur-ez-ezt'],
						$ids['barmenia-zahn80'],
						$ids['wuerttembergische-z2'],
						#$ids['wb-p2-ze70'],
						$ids['rv-comfort-z2'],
						$ids['wuerttembergische-z3']
						)
			);

	}

		$steps[1][] = array(
			"Die besten Tarife bei Stiftung Warentest (Note 0,5 - 1,3)"=>array(
						$ids['bayerische-zahn-prestige'],
						$ids['dfv-exklusiv'],
						$ids['hansemerkur-ezl'],
						$ids['dkv-kdtp100-kdbe'],
						$ids['hallesche-gigadent'],
						$ids['ukv-zp-premium'],
						$ids['mv-571-572-573-574'],
						$ids['ergo-direkt-premium'],
						$ids['rv-premium-z1zv'],
						$ids['allianz-dent-best'],
						#$ids['wb-d1-ze90-zbe'],
						$ids['hansemerkur-ezk'],
						$ids['hansemerkur-ez-ezt-ezp'],
						#$ids['sigidu-zahn-top-pur'],
						$ids['axa-dent-premium'],
						$ids['bayerische-zahn-komfort'],
						$ids['hallesche-megadent'],
						#$ids['barzgplus'],
						$ids['janitos-ja-dental-plus'],
						$ids['conticezp-u'],
						$ids['universa-uni-dent-privat'],
						$ids['dkv-kdt85-kdbe']
						),
			"Gute Alternativen (ab Note 1,4)"=>array(
						$ids['inter-z90-zpro'],
						$ids['aragz90'],
						$ids['bayerische-zahn-smart'],
						$ids['allianz-dent-plus'],
						$ids['ukv-zp-optimal'],
						$ids['axa-dent-komfort'],
						$ids['rv-comfort-z2zv']
						#$ids['wb-d2-ze70-zbe']
						),
			"Teilweise bewertete Tarife"=>array(),
			"Nicht bewertete Tarife"=>array(
						$ids['barmenia-zahn100-zv'],
						$ids['barmenia-zahn90-zv'],
						$ids['barmenia-zahn80-zv'],
						$ids['gothaer-mediz-duo'],
						$ids['bayerische-dentplus'],
						#$ids['bayerische-zahn-prestige'],
						#$ids['bayerische-zahn-komfort'],
						#$ids['bayerische-zahn-smart'],
						#$ids['allianz-dent-plus'],
						$ids['nuern-z100'],
						$ids['nuern-z90'],
						$ids['nuern-z80'],
						$ids['wuerttembergische-v1'],
						$ids['wuerttembergische-v2'],
						$ids['wuerttembergische-v3'],
						$ids['sigidu-komplus'],
						$ids['sigidu-komstart']
						),
			"wichtige Leistungsvorgaben werden nicht erf&uuml;llt (keine Prophylaxe & ZB)"=>array(
						$ids['barmenia-zahn100'],
						$ids['dkv-kdtp100'],
						#$ids['hallesche-dentze100'],
						$ids['rv-premium-z1'],
						$ids['wuerttembergische-z1'],
						$ids['barmenia-zahn90'],
						#$ids['hallesche-dentze90'],
						#$ids['mv-571'],
						#$ids['inter-z90'],
						$ids['dkv-kdt85'],
						$ids['hansemerkur-ez-ezt'],
						$ids['barmenia-zahn80'],
						$ids['wuerttembergische-z2'],
						$ids['rv-comfort-z2'],
						$ids['wuerttembergische-z3']
						)
			);

		$steps[1][] = array(
			"Top Anfangsleistungen"=>array(
						$ids['bayerische-dentplus'],
						$ids['barmenia-zahn100-zv'],
						$ids['ukv-zp-premium'],
						$ids['barmenia-zahn90-zv'],		
						$ids['rv-premium-z1zv'],
						$ids['bayerische-zahn-prestige'],
						$ids['dfv-exklusiv'],
						$ids['bayerische-zahn-komfort'],
						$ids['barmenia-zahn80-zv'],
						$ids['bayerische-zahn-smart'],
						$ids['gothaer-mediz-duo'],
						$ids['nuern-z100'],
						$ids['ukv-zp-optimal'],
						#$ids['bayerische-vdent-prestige'],
						$ids['axa-dent-premium'],
						$ids['hansemerkur-ezl']
						#$ids['wb-d1-ze90-zbe']
						),
			"Mittlere Anfangsleistungen"=>array(
						$ids['nuern-z90'],
						$ids['allianz-dent-best'],
						$ids['dkv-kdtp100-kdbe'],
						$ids['inter-z90-zpro'],
						$ids['hallesche-gigadent'],
						$ids['hallesche-megadent'],
						$ids['ukv-zp-kompakt'],
						$ids['wuerttembergische-v1'],
						$ids['dkv-kdt85-kdbe'],
						$ids['janitos-ja-dental-plus'],
						$ids['conticezp-u'],
						$ids['nuern-z80'],
						#$ids['barzgplus'],
						$ids['aragz90'],
						$ids['allianz-dent-plus'],
						$ids['sigidu-komplus'],
						$ids['sigidu-komstart'],
						$ids['wuerttembergische-v2'],
						$ids['axa-dent-komfort']
						),
			"Niedrige Anfangsleistungen"=>array(
						$ids['hansemerkur-ezk'],
						$ids['hansemerkur-ez-ezt-ezp'],
						$ids['ergo-direkt-premium'],
						$ids['mv-571-572-573-574'],
						$ids['universa-uni-dent-privat'],
						$ids['sigidu-zahn-top-pur'],
						$ids['wuerttembergische-v3']
						#$ids['wb-d2-ze70-zbe']
						),
			"wichtige Leistungsvorgaben werden nicht erf&uuml;llt (keine Prophylaxe & ZB)"=>array(
						$ids['dkv-kdtp100'],
						#$ids['hallesche-dentze100'],
						$ids['barmenia-zahn100'],
						$ids['rv-premium-z1'],
						$ids['wuerttembergische-z1'],
						#$ids['hallesche-dentze90'],
						$ids['barmenia-zahn90'],
						#$ids['mv-571'],
						#$ids['inter-z90'],
						$ids['dkv-kdt85-kdbe'],
						$ids['hansemerkur-ez-ezt'],
						$ids['wuerttembergische-z2'],
						$ids['barmenia-zahn80'],
						$ids['rv-comfort-z2'],
						$ids['wuerttembergische-z3']
						)
			);



		$steps[2][1] = array(
			"Top-Leistungen"=>array(
						$ids['barmenia-zahn100'],
						$ids['dkv-kdtp100'],
						#$ids['hallesche-dentze100'],
						$ids['wuerttembergische-z1'],
						$ids['barmenia-zahn90'],
						$ids['rv-premium-z1']
						#,$ids['hallesche-dentze90'],
						#$ids['mv-571'],
						#$ids['inter-z90']
						),
			"Gute Alternativen"=>array(
						$ids['barmenia-zahn80'],
						$ids['dkv-kdt85'],
						$ids['hansemerkur-ez-ezt'],
						$ids['wuerttembergische-z2']
						),
			"G&uuml;nstige Basisabsicherung"=>array(
						$ids['rv-comfort-z2'],
						$ids['wuerttembergische-z3']
						),
			"Tarife mit Prophylaxe und ZB (nicht erw&uuml;nscht)"=>array(
						$ids['bayerische-dentplus'],
						$ids['bayerische-zahn-prestige'],
						$ids['dfv-exklusiv'],
						$ids['hallesche-gigadent'],
						$ids['wuerttembergische-v1'],
						$ids['dkv-kdtp100-kdbe'],
						$ids['hansemerkur-ezl'],
						$ids['allianz-dent-best'],
						$ids['ukv-zp-premium'],
						#$ids['wb-d1-ze90-zbe'],
						$ids['mv-571-572-573-574'],
						$ids['hallesche-megadent'],
						$ids['bayerische-zahn-komfort'],
						$ids['axa-dent-premium'],
						$ids['rv-premium-z1zv'],
						$ids['ergo-direkt-premium'],
						$ids['wuerttembergische-v2'],
						$ids['hansemerkur-ezk'],
						$ids['universa-uni-dent-privat'],
						$ids['inter-z90-zpro'],
						$ids['bayerische-zahn-smart'],
						$ids['conticezp-u'],
						$ids['ukv-zp-optimal'],
						$ids['hansemerkur-ez-ezt-ezp'],
						$ids['wuerttembergische-v3'],
						$ids['dkv-kdt85-kdbe'],
						#$ids['barzgplus'],
						#$ids['advigon-ideal'],
						$ids['janitos-ja-dental-plus'],						
						$ids['aragz90'],
						$ids['sigidu-komplus'],
						$ids['allianz-dent-plus'],
						#$ids['wb-d2-ze70-zbe'],
						$ids['axa-dent-komfort'],
						$ids['rv-comfort-z2zv'],
						$ids['ukv-zp-kompakt'],
						$ids['sigidu-komstart']
						
						)
			);

		$steps[2][] = array(
			"Testsieger bei Stiftung Warentest (Note 1,0 - 1,3)"=>array(
						$ids['dkv-kdtp100'],
						#$ids['hallesche-dentze100'],
						$ids['rv-premium-z1'],
						$ids['hansemerkur-ez-ezt']
						#,$ids['hallesche-dentze90']
						),
			"Gute Alternativen (ab Note 1,4)"=>array(
						#$ids['inter-z90'],
						$ids['dkv-kdt85'],
						$ids['rv-comfort-z2']
						#,$ids['mv-571']
						),
			"Teilweise bewertete Tarife"=>array(),
			"Nicht bewertete Tarife"=>array(
						$ids['wuerttembergische-z1'],
						$ids['wuerttembergische-z2'],
						$ids['wuerttembergische-z3'],
						$ids['barmenia-zahn100'],
						$ids['barmenia-zahn90'],
						$ids['barmenia-zahn80']
						),
			"Tarife mit Prophylaxe und ZB (nicht erw&uuml;nscht)"=>array(
						$ids['bayerische-dentplus'],
						$ids['bayerische-zahn-prestige'],
						$ids['bayerische-zahn-komfort'],
						$ids['bayerische-zahn-smart'],
						$ids['dkv-kdtp100-kdbe'],
						$ids['hallesche-gigadent'],
						$ids['dfv-exklusiv'],
						$ids['allianz-dent-best'],
						$ids['allianz-dent-plus'],
						#$ids['bayerische-vdent-prestige'],
						$ids['rv-premium-z1zv'],
						$ids['axa-dent-premium'],
						$ids['hallesche-megadent'],
						$ids['ukv-zp-premium'],
						$ids['ukv-zp-optimal'],
						$ids['ukv-zp-kompakt'],
						$ids['inter-z90-zpro'],
						$ids['conticezp-u'],
						$ids['wuerttembergische-v1'],
						$ids['mv-571-572-573-574'],
						$ids['ergo-direkt-premium'],
						$ids['wuerttembergische-v2'],
						$ids['dkv-kdt85-kdbe'],
						$ids['janitos-ja-dental-plus'],
						$ids['advigon-ideal'],
						$ids['wuerttembergische-v3'],
						#$ids['barzgplus'],
						#$ids['aragz90'],
						$ids['universa-uni-dent-privat'],
						$ids['hansemerkur-ez-ezt-ezp'],
						$ids['axa-dent-komfort'],
						$ids['sigidu-komplus'],
						$ids['rv-comfort-z2zv'],
						$ids['sigidu-komstart'],
						$ids['nuern-z80']
						)
			);

		$steps[2][] = array(
			"Top Anfangsleistungen"=>array(
						$ids['barmenia-zahn100'],
						$ids['rv-premium-z1'],
						$ids['barmenia-zahn90'],
						$ids['barmenia-zahn80'],
						$ids['rv-comfort-z2']
						#,$ids['hallesche-dentze100'],
						#$ids['hallesche-dentze90']
						),
			"Mittlere Anfangsleistungen"=>array(
						#$ids['dkv-kdtp100'],
						$ids['wuerttembergische-z1'],
						#$ids['inter-z90'],
						$ids['dkv-kdt85'],
						$ids['wuerttembergische-z2'],
						$ids['hansemerkur-ez-ezt']
						),
			"Niedrige Anfangsleistungen"=>array(
						$ids['wuerttembergische-z3']
						#,$ids['mv-571']
						),
			"Tarife mit Prophylaxe und ZB (nicht erw&uuml;nscht)"=>array(
						$ids['bayerische-dentplus'],
						$ids['bayerische-zahn-prestige'],
						$ids['bayerische-zahn-komfort'],
						$ids['bayerische-zahn-smart'],
						$ids['dkv-kdtp100-kdbe'],
						$ids['hallesche-gigadent'],
						$ids['dfv-exklusiv'],
						$ids['allianz-dent-best'],
						$ids['allianz-dent-plus'],
						#$ids['bayerische-vdent-prestige'],
						$ids['rv-premium-z1zv'],
						$ids['axa-dent-premium'],
						$ids['hallesche-megadent'],
						$ids['ukv-zp-premium'],
						$ids['ukv-zp-optimal'],
						$ids['ukv-zp-kompakt'],
						$ids['inter-z90-zpro'],
						$ids['conticezp-u'],
						$ids['wuerttembergische-v1'],
						$ids['mv-571-572-573-574'],
						$ids['ergo-direkt-premium'],
						$ids['wuerttembergische-v2'],
						$ids['dkv-kdt85-kdbe'],
						$ids['janitos-ja-dental-plus'],
						$ids['advigon-ideal'],
						$ids['wuerttembergische-v3'],
						#$ids['barzgplus'],
						#$ids['aragz90'],
						$ids['universa-uni-dent-privat'],
						$ids['hansemerkur-ez-ezt-ezp'],
						$ids['axa-dent-komfort'],
						$ids['sigidu-komplus'],
						$ids['rv-comfort-z2zv'],
						$ids['sigidu-komstart']
						)
			);


	// I search: 3 - eine Versicherung, für Kinder
		$steps[3][1] = array(
			"Top-Schutz f&uuml;r Kinder"=>array(
						$ids['bayerische-dentplus'],
						$ids['ukv-zp-premium'],
						$ids['axa-dent-premium'],
						$ids['allianz-dent-best'],
						$ids['inter-z90-zpro'],
						$ids['mv-571-572-573-574'],
						#$ids['aragz90'],
						$ids['advigon-ideal'],
						$ids['rv-premium-z1zv']
						),
			"G&uuml;nstige Alternativen f&uuml;r Kinder"=>array(
						$ids['allianz-dent-plus'],
						$ids['axa-dent-komfort'],
						$ids['sigidu-komplus'],
						$ids['sigidu-komstart'],
						$ids['dkv-kdt85-kdbe'],
						$ids['janitos-ja-dental-plus'],
						$ids['universa-uni-dent-privat']
						)
			);


		$steps[3][2] = $steps[3][3] = $steps[3][1];


	/*
	**
	** HEILFÜRSORGE
	**
	*/


	if($insuranceType == 'heil')
	{
		$steps[1][1] = array(
			"Top-Leistungen"=>array(
						$ids['bayerische-dentplus'],
						$ids['barmenia-zahn100-zv'],
						$ids['bayerische-zahn-prestige'],
						$ids['barmenia-zahn90-zv'],
						$ids['nuern-z100'],
						$ids['mv-571-572-573-574'],
						$ids['bayerische-zahn-komfort'],
						$ids['nuern-z90'],
						$ids['axa-dent-premium'],
						$ids['inter-z90-zpro'],

						),
			"Gute Alternativen"=>array(
						$ids['barmenia-zahn80-zv'],
						$ids['bayerische-zahn-smart'],
						$ids['advigon-ideal'],
						$ids['aragz90'],
						$ids['sigidu-komplus'],
						$ids['nuern-z80']
						),
			"G&uuml;nstige Basisabsicherung"=>array(
						$ids['axa-dent-komfort'],
						$ids['sigidu-komstart']
						),
			"wichtige Leistungsvorgaben werden nicht erf&uuml;llt (keine Prophylaxe & ZB)"=>array(
						$ids['barmenia-zahn100'],
						$ids['barmenia-zahn90'],
						$ids['barmenia-zahn80'],
						$ids['wuerttembergische-z1'],
						#$ids['mv-571'],
						#$ids['inter-z90'],
						$ids['wuerttembergische-z2'],
						$ids['wuerttembergische-z3']
						)
			);

		$steps[1][2] = array(
			"Testsieger bei Stiftung Warentest (Note 1,0 - 1,3)"=>array(
						$ids['bayerische-zahn-prestige'],
						$ids['mv-571-572-573-574'],
						$ids['axa-dent-premium'],
						$ids['bayerische-zahn-komfort']
						),
			"Gute Alternativen (ab Note 1,4)"=>array(
						$ids['inter-z90-zpro'],
						$ids['bayerische-zahn-smart'],
						#$ids['advigon-ideal'],
						$ids['aragz90'],
						$ids['axa-dent-komfort']
						),
			"Teilweise bewertete Tarife"=>array(),
			"Nicht bewertete Tarife"=>array(
						$ids['bayerische-dentplus'],
						#$ids['bayerische-zahn-prestige'],
						#$ids['bayerische-zahn-komfort'],
						#$ids['bayerische-zahn-smart'],
						#$ids['wuerttembergische-v1'],
						#$ids['wuerttembergische-v2'],
						#$ids['wuerttembergische-v3'],
						#$ids['bayerische-vdent-prestige'],
						$ids['sigidu-komplus'],
						$ids['sigidu-komstart'],
						$ids['barmenia-zahn100-zv'],
						$ids['barmenia-zahn90-zv'],
						$ids['barmenia-zahn80-zv']
						),
			"wichtige Leistungsvorgaben werden nicht erf&uuml;llt (keine Prophylaxe & ZB)"=>array(
						$ids['barmenia-zahn100'],
						$ids['barmenia-zahn90'],
						$ids['barmenia-zahn80'],
						$ids['wuerttembergische-z1'],
						#$ids['mv-571'],
						#$ids['inter-z90'],
						$ids['wuerttembergische-z2'],
						$ids['wuerttembergische-z3']
						)
			);

		$steps[1][3] = array(
			"Top Anfangsleistungen"=>array(
						$ids['bayerische-dentplus'],
						$ids['barmenia-zahn100-zv'],
						$ids['bayerische-zahn-prestige'],
						$ids['barmenia-zahn90-zv'],
						$ids['bayerische-zahn-komfort'],
						$ids['barmenia-zahn80-zv'],
						$ids['bayerische-zahn-smart']
						),
			"Mittlere Anfangsleistungen"=>array(
						$ids['axa-dent-premium'],
						$ids['inter-z90-zpro'],
						$ids['aragz90'],
						$ids['sigidu-komplus'],
						$ids['sigidu-komstart'],
						$ids['axa-dent-komfort']
						),
			"Niedrige Anfangsleistungen"=>array(
						#$ids['advigon-ideal'],
						$ids['mv-571-572-573-574']
						#,$ids['wuerttembergische-v3']
						),
			"wichtige Leistungsvorgaben werden nicht erf&uuml;llt (keine Prophylaxe & ZB)"=>array(
						$ids['barmenia-zahn100'],
						$ids['barmenia-zahn90'],
						$ids['barmenia-zahn80'],
						$ids['wuerttembergische-z1'],
						#$ids['mv-571'],
						#$ids['inter-z90'],
						$ids['wuerttembergische-z2'],
						$ids['wuerttembergische-z3']
						)
			);



		$steps[2][1] = array(
			"Top-Leistungen"=>array(
						$ids['wuerttembergische-z1']
						),
			"Gute Alternativen"=>array(
						$ids['wuerttembergische-z2']
						),
			"G&uuml;nstige Basisabsicherung"=>array(
						$ids['wuerttembergische-z3']
						),
			"Tarife mit Prophylaxe und ZB (nicht erw&uuml;nscht)"=>array(
						$ids['bayerische-dentplus'],
						#$ids['bayerische-vdent-prestige'],
						$ids['axa-dent-premium'],
						$ids['inter-z90-zpro'],
						#$ids['wuerttembergische-v1'],
						$ids['mv-571-572-573-574'],
						#$ids['wuerttembergische-v2'],
						$ids['advigon-ideal'],
						#$ids['wuerttembergische-v3'],
						#$ids['aragz90'],
						$ids['sigidu-komplus'],
						$ids['sigidu-komstart'],
						$ids['axa-dent-komfort']
						)
			);

		$steps[2][2] = array(
			"Testsieger bei Stiftung Warentest (Note 1,0 - 1,3)"=>array(),
			"Gute Alternativen (ab Note 1,4)"=>array(),
			"Teilweise bewertete Tarife"=>array(),
			"Nicht bewertete Tarife"=>array(
						$ids['wuerttembergische-z1'],
						$ids['wuerttembergische-z2'],
						$ids['wuerttembergische-z3']
						),
			"Tarife mit Prophylaxe und ZB (nicht erw&uuml;nscht)"=>array(
						$ids['bayerische-dentplus'],
						$ids['axa-dent-premium'],
						$ids['inter-z90-zpro'],
						$ids['mv-571-572-573-574'],
						$ids['advigon-ideal'],
						$ids['sigidu-komplus'],
						$ids['sigidu-komstart'],
						$ids['axa-dent-komfort']
						)
			);

		$steps[2][3] = array(
			"Top Anfangsleistungen"=>array(),
			"Mittlere Anfangsleistungen"=>array(
						$ids['wuerttembergische-z1'],
						$ids['wuerttembergische-z2']
						),
			"Niedrige Anfangsleistungen"=>array(
						$ids['wuerttembergische-z3']
						),
			"Tarife mit Prophylaxe und ZB (nicht erw&uuml;nscht)"=>array(
						$ids['bayerische-dentplus'],
						$ids['axa-dent-premium'],
						$ids['inter-z90-zpro'],
						$ids['mv-571-572-573-574'],
						$ids['advigon-ideal'],
						$ids['sigidu-komplus'],
						$ids['sigidu-komstart'],
						$ids['axa-dent-komfort']
						)
			);
	}
	return $steps;
}


public function getCRMInsurances($idco) {
/* 
** Zentrale Funktion zur Ermittlung der Price / BonusBase bei Duplizierung im CRM
**
**			$idco (wird Übermittelt bei Duplizieren eines Antrages)
** return array
*/
$wzm = new wzm();
$person = new person();
$ids = $this->getActiveTariffs();

	foreach($ids as $key => $val)
	{
		$arr[$val['idt']] = $val['name'];
	}


	$c = contract::get($idco);
	$p = $person->get($c['pid']);
	$contractType = $c['contractType'];

	if(!$p['isFatherOfPid'])
	{
			$birthdate = $p['birthdate'];
			
	} else {
			$child = $person->get($p['isFatherOfPid']);
			$birthdate = $child['birthdate'];
	}

	
	$data = '0/'; // Erwachsener
	$data .= $birthdate.'/'; // GebDat
	$data .= $c['wishedBegin'].'/'; // wishedBegin
	$data .= $c['tooth1'].'/'; // missing-teeth
	$data .= $c['tooth3'].'/'; // replaced-teeth-removable
	$data .= ($c['incare']=='yes'?'1':'0').'/'; // dental-treatment
	$data .= $c['tooth5'].'/'; // replaced-teeth-fix
	$data .= $c['toothFixed'].'/'; // replaced-teeth-older-10-years
	$data .= ($c['periodontitis']=='yes'?'1':'0').'/'; // periodontitis
	$data .= ($c['periodontitisCured']=='yes'?'1':'0').'/'; // periodontitisCured	
	$data .= ($c['biteSplint']=='yes'?'1':'0'); // bite-splint

	if($contractType == 'kids')
	{	
		$data = '1/'; // Kind
		$data .= $birthdate.'/'; // GebDat
		$data .= $c['wishedBegin'].'/'; // wishedBegin
		$data .= ($c['incare']=='yes'?'1':'0').'/'; // dental-treatment
		$data .= ($c['orthodontics']=='yes'?'1':'0'); // orthodontics
	}

	$s = json_decode($this->getCalculatorData($data), true);

	$tariff_accepted = $tariff_denied = array();
	foreach($s as $key => $value) 
	{
		if($value['status'] == 1 && isset($value['bonusbase']['activeBonusBase']))
			$tariff_accepted[] = array("idt" => $key, "name" => strtoupper($value['ident']).' '.$value['tariffName'], "status" => $value['status'], "series" => $value['series'], "surplus" => $value['surplus'], "bonusbase" => $value['bonusbase']['activeBonusBase'], "wishedBegin" => $value['bonusbase']['wishedBegin']);
		if($value['status'] == 0 && isset($value['bonusbase']['activeBonusBase']))
			$tariff_denied[] = array("idt" => $key, "name" => strtoupper($value['ident']).' '.$value['tariffName'], "status" => $value['status'], "series" => $value['series'], "surplus" => $value['surplus'], "bonusbase" => $value['bonusbase']['activeBonusBase'], "wishedBegin" => $value['bonusbase']['wishedBegin']);
		unset($arr[$key]); 
	}


	usort($tariff_accepted, function($a, $b) {
		return strcmp($a['name'], $b['name']);
	});
	usort($tariff_denied, function($a, $b) {
		return strcmp($a['name'], $b['name']);
	});

	if(count($arr) > 0)
	{
		foreach($arr as $key => $val)
		{
			$bonusbase = $wzm->getBonusBaseWithDate($key, $c['birthdate'], $c['wishedBegin']);
			$tariff_denied[] = array("idt" => $key, "name" => strtoupper($val), "status" => 0, "series" => 0, "bonusbase" => $bonusbase['activeBonusBase']);
		}
	}
		



	return array("tariffs_accepted" => $tariff_accepted, "tariffs_denied" => $tariff_denied);

	
}


public function getInsurances($tariffType, $data, $idco=null, $wishedBegin=null) {
/* 
** Zentrale Funktion zur Ermittlung der Versicherungen basierend der Vorgaben des Antragstellers
**
** vars:	$tariffType ('adult', 'kids')
**			$data (data-array aus dem Rechner)
**			$idco (wird Übermittelt bei Duplizieren eines Antrages)
**			$wishedBegin (Antragsdatum, falls nicht aus Rechner)
** return array
*/


$wzm = new wzm();
$person = new person();
$ids = project::gta();

if($data['formData'][1] && $data['formData'][2]) {
	// Aufruf aus Rechner oder LandingPage

	$step1 = $data['formData'][1];
	$step2 = $data['formData'][2];

	$birthdate = $step1['birthdate']; //Format YYYY-MM-DD
} elseif(isset($idco)) {
	// Aufruf aus Backend - dupl. Fkt.

	$c = contract::get($idco);
	$p = $person->get($c['pid']);

	if(!$p['isFatherOfPid'])
	{
			$birthdate = $p['birthdate'];
			$tariffType = 'adult';
	} else {
			$child = $person->get($p['isFatherOfPid']);
			$birthdate = $child['birthdate'];
			$tariffType = 'kids';
	}

	$step2 = $c;

	// -1 fix
	$step2['tooth1'] = $step2['tooth1'] == -1 ? 0 : $step2['tooth1'];
	$step2['tooth2'] = $step2['tooth2'] == -1 ? 0 : $step2['tooth2'];
	$step2['tooth3'] = $step2['tooth3'] == -1 ? 0 : $step2['tooth3'];
	$step2['tooth4'] = $step2['tooth4'] == -1 ? 0 : $step2['tooth4'];
	$step2['tooth5'] = $step2['tooth5'] == -1 ? 0 : $step2['tooth5'];
	$step2['wishedBegin'] = stringHelper::parseNcalcDate($wishedBegin, 0, 0, 0, true);

} else { 
	$datar = print_r($data, true);
	error_log('Keine Daten Uebergeben in crm/contract/getInsurances. $data = '.$data.', $idco = '.$idco.', SERVER = '.print_r($_SERVER, true).'-'.$datar, 1, 'anfrage@consulting-wirth.de'); 
}



/* 
** Ermittlung Alter nach Versicherung
** Erstellen der benötigten Arrays und Berechnen der Bonusbase
*/
$bonus = $age = $tariff = array();

foreach($ids as $name => $idt)
{
	$age[$idt] = $wzm->getAgeByIdt($idt, $birthdate);
	$bonus[$idt] = 0;
	$tariff[$idt] = 0;
	$bonusbase[$idt] = $wzm->getBonusBaseWithDate($idt, $birthdate, $step2['wishedBegin']);
}



switch($tariffType) {
		case 'adult':
		$toothSum = $step2['tooth1']+$step2['tooth3'];


	/* ADVIGON ZE TOP + ZB
	** 
	** NEU
	*/

		if($toothSum <= 3 && $age[$ids['advigon-ideal']]<= 70 && $step2['tooth5']==0 && $step2['periodontitis']=='no' && $step2['incare']=='no' && $step2['biteSplint']=='no' && $step2['docControl']!='no')
		{
			$tariff[$ids['advigon-premium-zgp']] = 0;
			$tariff[$ids['advigon-ideal']] = 1;
		}


	/* SIGNAL IDUNA KOMPAKT-START & SIGNAL IDUNA KOMPAKT-PLUS
	**
	** NEU
	*/

		if($age[$ids['sigidu-komstart']]<= 60 && 
			($step2['sickness']=='no' || !isset($step2['sickness']))) {
			$tariff[$ids['sigidu-komstart']] = 1;
			$tariff[$ids['sigidu-komplus']] = 1;
		}
		#if($step2['ambulant']!='yes') {
		#	unset($tariff[$ids['sigidu-komstart']]);
		#	unset($tariff[$ids['sigidu-komplus']]);
		#}


	/* UNIVERSA
	**
	** NEU
	*/
 		if($age[$ids['universa-uni-dent-privat']]<= 80)
			$tariff[$ids['universa-uni-dent-privat']] = 1;


	/* HALLESCHE
	**
	** NEU
	*/
		if($step2['periodontitis']=='no' && $step2['biteSplint']=='no' && $step2['incare']=='no') 
		{
			if($toothSum == 0)
			{
				$tariff[$ids['hallesche-megadent']] = 1;
				$tariff[$ids['hallesche-gigadent']] = 1;
			}
			elseif ($toothSum <=3)
			{
				$tariff[$ids['hallesche-megadent']] = 2;
				$tariff[$ids['hallesche-gigadent']] = 2;
				$tariff['series'][$ids['hallesche-megadent']] = 1;
				$tariff['series'][$ids['hallesche-gigadent']] = 1;
				$tariff['helpText'][$ids['hallesche-megadent']] = L::_('minSummenstaffel');
				$tariff['helpText'][$ids['hallesche-gigadent']] = L::_('minSummenstaffel');
			}
		}

		
	/* UKV
	**
	** NEU
	*/
		if($step2['tooth1']<=3 && $step2['incare'] == 'no')
		{
			$tariff[$ids['ukv-zp-premium']] = 1;
			$bonus[$ids['ukv-zp-premium']] = $step2['tooth1'] * 8.6;

			$tariff[$ids['ukv-zp-optimal']] = 1;
			$bonus[$ids['ukv-zp-optimal']] = $step2['tooth1'] * 6.7;

			$tariff[$ids['ukv-zp-kompakt']] = 1;
			$bonus[$ids['ukv-zp-kompakt']] = $step2['tooth1'] * 4.8;
		}
		



	/* BARMENIA ZG PLUS
	**
	** NEU
	*/
		if($step2['tooth1']<=1 && $step2['incare']=='no' && ($step2['periodontitis']=='no' || $step2['periodontitisCured']!=1)) {
		#	$tariff[$ids['barzgplus']] = 1;
		}
		
		if($step2['tooth1']<=3 && $step2['incare']=='no' && $step2['periodontitisCured']!=0) {
		#	$tariff['helpText'][$ids['barzgplus']] = L::_('minSummenstaffel');
		#	$tariff['series'][$ids['barzgplus']] = 1;
		}


	/* BARMENIA MEHR ZAHN TARIFE
	**
	** NEU
	*/
		if($step2['tooth1']<=1 && $step2['incare']=='no' && ($step2['periodontitis']=='no' || $step2['periodontitisCured']!=1)) {
			$tariff[$ids['barmenia-zahn100']] = 1;
			$tariff[$ids['barmenia-zahn90']] = 1;
			$tariff[$ids['barmenia-zahn80']] = 1;
			$tariff[$ids['barmenia-zahn100-zv']] = 1;
			$tariff[$ids['barmenia-zahn90-zv']] = 1;
			$tariff[$ids['barmenia-zahn80-zv']] = 1;
		}
		
		if($step2['tooth1']<=3 && $step2['incare']=='no' && $step2['periodontitisCured']!=0) {
			$tariff['helpText'][$ids['barmenia-zahn100']] = L::_('minSummenstaffel');
			$tariff['series'][$ids['barmenia-zahn100']] = 1;
			$tariff['helpText'][$ids['barmenia-zahn90']] = L::_('minSummenstaffel');
			$tariff['series'][$ids['barmenia-zahn90']] = 1;
			$tariff['helpText'][$ids['barmenia-zahn80']] = L::_('minSummenstaffel');
			$tariff['series'][$ids['barmenia-zahn80']] = 1;
			$tariff['helpText'][$ids['barmenia-zahn100-zv']] = L::_('minSummenstaffel');
			$tariff['series'][$ids['barmenia-zahn100-zv']] = 1;
			$tariff['helpText'][$ids['barmenia-zahn90-zv']] = L::_('minSummenstaffel');
			$tariff['series'][$ids['barmenia-zahn90-zv']] = 1;
			$tariff['helpText'][$ids['barmenia-zahn80-zv']] = L::_('minSummenstaffel');
			$tariff['series'][$ids['barmenia-zahn80-zv']] = 1;
		}



	/* DEUTSCHE FAMILIEN VERSICHERUNG
	**
	** NEU
	*/
		$tariff[$ids['dfv-exklusiv']] = 1;


	/* ALLIANZ DENT BEST und DENT PLUS
	**
	** NEU
	*/
		if($age[$ids['allianz-dent-best']]<= 64 || $age[$ids['allianz-dent-plus']]<= 64) {
			if($step2['tooth1']<=3 && $step2['incare']=='no' && $step2['periodontitisCured']!=1) {
				$tariff[$ids['allianz-dent-best']] = 1;
				$tariff[$ids['allianz-dent-plus']] = 1;

				$bonus[$ids['allianz-dent-best']] = $step2['tooth1'] * 5.4;
				$bonus[$ids['allianz-dent-plus']] = $step2['tooth1'] * 3.4;
			}
		}



	/* MÜNCHNER VEREIN
	**
	** NEU
	*/
		$tariff[$ids['mv-571-572-573-574']] = 1;


	/* HANSE MERKUR EZK, EZL
	**
	** NEU
	*/
		if($age[$ids['hansemerkur-ezk']]< 70 && $step2['periodontitis']=='no' && $step2['tooth1'] <= 3) {
			$tariff[$ids['hansemerkur-ezk']] = 1;
			$bonus[$ids['hansemerkur-ezk']] = $step2['tooth1'] * 3;
		}
		if($age[$ids['hansemerkur-ezl']]< 70 && $step2['periodontitis']=='no' && $step2['tooth1'] <= 3) {	
			$tariff[$ids['hansemerkur-ezl']] = 1;
			$bonus[$ids['hansemerkur-ezl']] = $step2['tooth1'] * 6;
		}

	/* HANSE MERKUR EZ, EZT (, EZP)
	**
	** NEU
	*/
		if($age[$ids['hansemerkur-ez-ezt']]< 70 && $step2['tooth1'] <= 3) {
			$tariff[$ids['hansemerkur-ez-ezt']] = 1;
			$bonus[$ids['hansemerkur-ez-ezt']] = $step2['tooth1'] * 3;
		}
		if($age[$ids['hansemerkur-ez-ezt-ezp']]< 70 && $step2['tooth1'] <= 3) {	
			$tariff[$ids['hansemerkur-ez-ezt-ezp']] = 1;
			$bonus[$ids['hansemerkur-ez-ezt-ezp']] = $step2['tooth1'] * 3;
		}


	/* GOTHAER MEDIZ DUO
	**
	** NEU
	*/
		if($age[$ids['gothaer-mediz-duo']]< 70 && $step2['tooth1'] <= 3) {
			$tariff[$ids['gothaer-mediz-duo']] = 1;
			$tariff[$ids['gothaer-mediz-duo-80']] = 1;
			$tariff[$ids['gothaer-mediz-duo-90']] = 1;
			$tariff[$ids['gothaer-mediz-duo-100']] = 1;
		}


	/* ERGO DIREKT
	**
	** NEU
	*/
		if($age[$ids['ergo-direkt-premium']]<=80) {
			$tariff[$ids['ergo-direkt-premium']] = 1;
			$tariff[$ids['ergo-direkt-zez']] = 1;
		}


	/* SDK
	**
	** NEU
	*/
       if($step2['tooth1'] <= 3) {
            $tariff[$ids['sdk-zahn100-zp1']] = 1;
            $tariff[$ids['sdk-zahn90-zp9']] = 1;
            $tariff[$ids['sdk-zahn70-zp7']] = 1;

            $bonus[$ids['sdk-zahn100-zp1']] = $step2['tooth1'] * 0.2 * $bonusbase[($ids['sdk-zahn100-zp1'])]['activeBonusBase'];
            $bonus[$ids['sdk-zahn90-zp9']] = $step2['tooth1'] * 0.2 * $bonusbase[($ids['sdk-zahn90-zp9'])]['activeBonusBase'];
            $bonus[$ids['sdk-zahn70-zp7']] = $step2['tooth1'] * 0.2 * $bonusbase[($ids['sdk-zahn70-zp7'])]['activeBonusBase'];            
       }



	/* JANITOS
	**
	** NEU
	*/
		if($age[$ids['janitos-ja-dental-plus']]<=80 && $step2['tooth3'] == 0 && $step2['periodontitis'] == 'no')
		{
			if($step2['tooth1'] <= 1) {
				$tariff[$ids['janitos-ja-dental-plus']] = 1;
			} elseif($step2['tooth1']>1 && $step2['tooth1']<4) {
				$tariff['helpText'][$ids['janitos-ja-dental-plus']] = L::_('minSummenstaffel');
				$tariff['series'][$ids['janitos-ja-dental-plus']] = 1;
				$tariff[$ids['janitos-ja-dental-plus']] = 2;
			}
		}


	/* R + V
	**
	** NEU
	*/
		if($age[$ids['rv-premium-z1']]<=80) {
			$tariff[$ids['rv-premium-z1']] = 1;
			$tariff[$ids['rv-comfort-z2']] = 1;
			$tariff[$ids['rv-premium-z1zv']] = 1;
			$tariff[$ids['rv-comfort-z2zv']] = 1;
		}


	/* ARAG Z90
	**
	** NEU
	** 
	** bonus: eg 2 toothes = 0.2 = 20%
	*/
		#if($age[$ids['aragz90']]<=48 && $toothSum <= 3 && $step2['incare']=='no' && $step2['periodontitis']!='yes') {
		#	$tariff[$ids['aragz90']] = 1;
		#	$bonus[$ids['aragz90']] = sprintf('%.2f', $toothSum * 0.2);
		#}


	/* DKV - KDT85, KDT100
	**
	** NEU
	**
	** bonus 85: eg 2 toothes = 4€ p. tooth = 8€
	** bonus 100: eg 2 toothes = 7€ p. tooth = 14€
	*/
		if($step2['tooth1']<=3 && $step2['incare']=='no' && $step2['periodontitisCured']<>1 && $step2['biteSplint']!='yes') {
				$tariff[$ids['dkv-kdt85']] = 1;
				$tariff[$ids['dkv-kdt85-kdbe']] = 1;
				$tariff[$ids['dkv-kdtp100']] = 1;
				$tariff[$ids['dkv-kdtp100-kdbe']] = 1;
				$bonus[$ids['dkv-kdt85']] = $bonus[$ids['dkv-kdt85-kdbe']] = $step2['tooth1'] * 4;
				$bonus[$ids['dkv-kdtp100']] = $bonus[$ids['dkv-kdtp100-kdbe']] = $step2['tooth1'] * 7;
		}


	/* AXA
	**
	** NEU
	*/
		if($age[$ids['axa-dent-premium']]<=80) {
			if($step2['incare']!='yes' && $step2['periodontitis']!='yes' && $step2['tooth1']<=3) {
				$tariff[$ids['axa-dent-premium']] = 1;
				$tariff[$ids['axa-dent-komfort']] = 1;

				if($step2['tooth1']==1 || $step2['tooth1']==2 || $step2['tooth1']==3) {
					$tariff['helpText'][$ids['axa-dent-premium']] = $tariff['helpText'][$ids['axa-dent-komfort']] = L::_('Summenstaffel8');
					$tariff['series'][$ids['axa-dent-premium']] = $tariff['series'][$ids['axa-dent-komfort']] = 1;

					$tariff[$ids['axa-dent-premium']] = 2;
					$tariff[$ids['axa-dent-komfort']] = 2;
				}
			}
		}


	/* CONTINENTALE CEZP-U, CEZK-U, CEZE
	**
	** NEU
	*/
		if($step2['incare'] == 'no' && $step2['biteSplint'] == 'no' && $step2['periodontitisCured']<>1)
		{
			if($step2['tooth5'] <= 8 && $toothSum <= 4 && ($step2['tooth5'] + $toothSum <= 8))
			{
				$tariff[$ids['conticezp-u']] = 2;
				$tariff[$ids['conticezk-u']] = 2;
				$tariff[$ids['continentale-ceze']] = 2;

				$tariff['helpText'][$ids['conticezp-u']]  = L::_('minSummenstaffel');
				$tariff['series'][$ids['conticezp-u']]  = 1;
				$tariff['helpText'][$ids['conticezk-u']]  = L::_('minSummenstaffel');
				$tariff['series'][$ids['conticezk-u']]  = 1;
				$tariff['helpText'][$ids['continentale-ceze']]  = L::_('minSummenstaffel');
				$tariff['series'][$ids['continentale-ceze']]  = 1;				
			}

			if($step2['tooth5'] <= 2 && $toothSum <= 1)
			{
				$tariff[$ids['conticezp-u']] = 1;
				$tariff[$ids['conticezk-u']] = 1;
				$tariff[$ids['continentale-ceze']] = 1;

				unset($tariff['helpText'][$ids['conticezp-u']]);
				unset($tariff['series'][$ids['conticezp-u']]);
				unset($tariff['helpText'][$ids['conticezk-u']]);
				unset($tariff['series'][$ids['conticezk-u']]);
				unset($tariff['helpText'][$ids['continentale-ceze']]);
				unset($tariff['series'][$ids['continentale-ceze']]);
			}
		}

	/* INTER Z90 Zpro
	**
	** NEU
	*/

		if($step2['tooth1'] < 6 && ($step2['tooth3'] + $step['tooth4']) < 14 && $step2['tooth5'] < 6)
		{
			$tariff[$ids['inter-z90-zpro']] = 1;
		}


	/* NÜRNBERGER Z80, Z90, Z100
	**
	** NEU
	*/

		$tariff[$ids['nuern-z80']] = 1;
		$tariff[$ids['nuern-z90']] = 1;
		$tariff[$ids['nuern-z100']] = 1;



	/* DIE BAYERISCHE VIP DENT PLUS
	**
	** NEU
	
		if($step2['changeInsurance']!='no' && ($step2['previousTarif']<8 || $step2['previousTarifInsured']!=0) && $step2['incare']!='yes' && $step2['orthodontics']!='yes' && $step2['periodontitis']!='yes' && $step2['tooth1']<=3 && $step2['tooth3']==0 && $step2['tooth4']<=6) {
			$tariff[$ids['bayerische-dentplus']] = 1;
			$tariff['helpText'][$ids['bayerische-dentplus']] = L::_('Vorversicherung');
	}
	*/


	/* DIE BAYERISCHE ZAHN
	**
	** NEU
	*/
	
		if($step2['tooth1'] < 4 && $step2['tooth3']==0 && $step2['periodontitis']!='yes') {
				$tariff[$ids['bayerische-zahn-smart']] = 1;
				$tariff[$ids['bayerische-zahn-komfort']] = 1;
				$tariff[$ids['bayerische-zahn-prestige']] = 1;

				if($step2['tooth1'] > 1) {
					$tariff[$ids['bayerische-zahn-smart']] = 2;
					$tariff[$ids['bayerische-zahn-komfort']] = 2;
					$tariff[$ids['bayerische-zahn-prestige']] = 2;

					$tariff['series'][$ids['bayerische-zahn-smart']] = 1;
					$tariff['helpText'][$ids['bayerische-zahn-smart']] = L::_('minSummenstaffel');
					$tariff['series'][$ids['bayerische-zahn-komfort']] = 1;
					$tariff['helpText'][$ids['bayerische-zahn-komfort']] = L::_('minSummenstaffel');
					$tariff['series'][$ids['bayerische-zahn-prestige']] = 1;
					$tariff['helpText'][$ids['bayerische-zahn-prestige']] = L::_('minSummenstaffel');
				}
		}



	/* DIE BAYERISCHE VIP DENT PRESTIGE
	**
	** NEU
	*/

		if($age[$ids['bayerische-vdent-prestige']]<90 && $step2['incare']!='yes' && $step2['periodontitis']!='yes' && $step2['tooth1']<=1 && $step2['tooth3']==0) {
			$tariff[$ids['bayerische-vdent-prestige']] = 1;
/*
			switch($step2['tooth1']) {
				case 0:
					if($step2['tooth4']<=2) {
						// Annahme
						$tariff[$ids['bayerische-vdent-prestige']] = 1;
					} elseif($step2['tooth4']<=4) {
						// Leistungsstaffel 1
						$tariff[$ids['bayerische-vdent-prestige']] = 2;
						$tariff['helpText'][$ids['bayerische-vdent-prestige']] = L::_('minSummenstaffel');
						$tariff['series'][$ids['bayerische-vdent-prestige']] = 1;
						$tariff['addInfoInsurance'][$ids['bayerische-vdent-prestige']] = 1;
					} else {
						// Leistungsstaffel 2
						$tariff[$ids['bayerische-vdent-prestige']] = 0;
						//$tariff['helpText'][$ids['bayerische-vdent-prestige']] = 'Verminderte Leistungsstaffel in den ersten 4 Jahren!';
						//$tariff['series'][$ids['bayerische-vdent-prestige']] = 1;
						//$tariff['addInfoInsurance'][$ids['bayerische-vdent-prestige']] = 2;
					}
				break;
				case 1:
					if($step2['tooth4']==0) {
						// Annahme
						$tariff[$ids['bayerische-vdent-prestige']] = 1;
					} elseif($step2['tooth4']<=2) {
						// Leistungsstaffel 1
						$tariff[$ids['bayerische-vdent-prestige']] = 2;
						$tariff['helpText'][$ids['bayerische-vdent-prestige']] = L::_('minSummenstaffel');
						$tariff['series'][$ids['bayerische-vdent-prestige']] = 1;
						$tariff['addInfoInsurance'][$ids['bayerische-vdent-prestige']] = 1;
					} elseif($step2['tooth4']<=4) {
						// Leistungsstaffel 2
						$tariff[$ids['bayerische-vdent-prestige']] = 0;
						//$tariff['helpText'][$ids['bayerische-vdent-prestige']] = 'Verminderte Leistungsstaffel in den ersten 4 Jahren!';
						//$tariff['series'][$ids['bayerische-vdent-prestige']] = 1;
						//$tariff['addInfoInsurance'][$ids['bayerische-vdent-prestige']] = 2;
					}
				break;
				case 2:
					if($step2['tooth4']==0) {
						// Leistungsstaffel 1
						$tariff[$ids['bayerische-vdent-prestige']] = 2;
						$tariff['helpText'][$ids['bayerische-vdent-prestige']] = L::_('minSummenstaffel');
						$tariff['series'][$ids['bayerische-vdent-prestige']] = 1;
						$tariff['addInfoInsurance'][$ids['bayerische-vdent-prestige']] = 1;
					} elseif($step2['tooth4']<=2) {
						// Leistungsstaffel 2
						$tariff[$ids['bayerische-vdent-prestige']] = 0;
						//$tariff['helpText'][$ids['bayerische-vdent-prestige']] = 'Verminderte Leistungsstaffel in den ersten 4 Jahren!';
						//$tariff['series'][$ids['bayerische-vdent-prestige']] = 1;
						//$tariff['addInfoInsurance'][$ids['bayerische-vdent-prestige']] = 2;
					}
				break;
			}
*/

		}


	/* WÜRTTENMBERGISCHE
	**
	** NEU
	*/

		if($age[$ids['wuerttembergische-v1']]<=75 && $step2['periodontitis']!='yes') {
			if($toothSum<5 && $step2['tooth4']<17) {

				switch($step2['tooth1'])
				{

					case 4:
						if($step2['tooth4']<13) {
							$tariff['helpText'][$ids['wuerttembergische-v1']] = L::_('Summenstaffel8');
							$tariff['series'][$ids['wuerttembergische-v1']] = 2;
							$tariff[$ids['wuerttembergische-v1']] = 2;
						}
					break;
					case 3:
						if($step2['tooth4']>9 && $step2['tooth4']<14) {
							$tariff['helpText'][$ids['wuerttembergische-v1']] = L::_('Summenstaffel8');
							$tariff['series'][$ids['wuerttembergische-v1']] = 2;
							$tariff[$ids['wuerttembergische-v1']] = 2;
						} elseif($step2['tooth4']<10) {
							$tariff['helpText'][$ids['wuerttembergische-v1']] = L::_('Summenstaffel6');
							$tariff['series'][$ids['wuerttembergische-v1']] = 1;
							$tariff[$ids['wuerttembergische-v1']] = 1;
						}
					break;
					case 2:
						if($step2['tooth4']>10 && $step2['tooth4']<15) {
							$tariff['helpText'][$ids['wuerttembergische-v1']] = L::_('Summenstaffel8');
							$tariff['series'][$ids['wuerttembergische-v1']] = 2;
							$tariff[$ids['wuerttembergische-v1']] = 2;
						} elseif($step2['tooth4']<11) {
							$tariff['helpText'][$ids['wuerttembergische-v1']] = L::_('Summenstaffel6');
							$tariff['series'][$ids['wuerttembergische-v1']] = 1;
							$tariff[$ids['wuerttembergische-v1']] = 1;
						}
					break;
					case 1:
						if($step2['tooth4']>11 && $step2['tooth4']<16) {
							$tariff['helpText'][$ids['wuerttembergische-v1']] = L::_('Summenstaffel8');
							$tariff['series'][$ids['wuerttembergische-v1']] = 2;
							$tariff[$ids['wuerttembergische-v1']] = 2;
						} elseif($step2['tooth4']<12) {
							$tariff['helpText'][$ids['wuerttembergische-v1']] = L::_('Summenstaffel6');
							$tariff['series'][$ids['wuerttembergische-v1']] = 1;
							$tariff[$ids['wuerttembergische-v1']] = 1;
						}
					break;
					case 0:
						if($step2['tooth4']>12 && $step2['tooth4']<17) {
							$tariff['helpText'][$ids['wuerttembergische-v1']] = L::_('Summenstaffel8');
							$tariff['series'][$ids['wuerttembergische-v1']] = 2;
							$tariff[$ids['wuerttembergische-v1']] = 2;
						} elseif($step2['tooth4']<13) {
							$tariff['helpText'][$ids['wuerttembergische-v1']] = L::_('Summenstaffel6');
							$tariff['series'][$ids['wuerttembergische-v1']] = 1;
							$tariff[$ids['wuerttembergische-v1']] = 1;
						}
				}

				if($toothSum<3 && $step2['tooth4']<9) {
					unset($tariff['helpText'][$ids['wuerttembergische-v1']]);
					unset($tariff['series'][$ids['wuerttembergische-v1']]);
				}

				// Übertragen der Hinweise auf die anderen Versicherungen
				$tariff['helpText'][$ids['wuerttembergische-v2']] = $tariff['helpText'][$ids['wuerttembergische-v3']] = isset($tariff['helpText'][$ids['wuerttembergische-v1']])?$tariff['helpText'][$ids['wuerttembergische-v1']]:'';
				$tariff['helpText'][$ids['wuerttembergische-z1']] = $tariff['helpText'][$ids['wuerttembergische-z2']] = $tariff['helpText'][$ids['wuerttembergische-z3']] = isset($tariff['helpText'][$ids['wuerttembergische-v1']])?$tariff['helpText'][$ids['wuerttembergische-v1']]:'';

				$tariff['series'][$ids['wuerttembergische-v2']] = $tariff['series'][$ids['wuerttembergische-v3']] = isset($tariff['series'][$ids['wuerttembergische-v1']])?$tariff['series'][$ids['wuerttembergische-v1']]:0;
				$tariff['series'][$ids['wuerttembergische-z2']] = $tariff['series'][$ids['wuerttembergische-z3']] = $tariff['series'][$ids['wuerttembergische-z1']] = isset($tariff['series'][$ids['wuerttembergische-v1']])?$tariff['series'][$ids['wuerttembergische-v1']]:0;

				$tariff[$ids['wuerttembergische-v2']] = $tariff[$ids['wuerttembergische-v3']] = isset($tariff[$ids['wuerttembergische-v1']])?$tariff[$ids['wuerttembergische-v1']]:'';				
				$tariff[$ids['wuerttembergische-z2']] = $tariff[$ids['wuerttembergische-z3']] = $tariff[$ids['wuerttembergische-z1']] = isset($tariff[$ids['wuerttembergische-v1']])?$tariff[$ids['wuerttembergische-v1']]:'';
			}
		}

	break;
	case 'kids':
	/* AXA
	**
	** NEU
	*/

		if($age[$ids['axa-dent-premium']] <= 80) {
			if($step2['incare']!='yes' && $step2['incare']=='no' &&  $step2['orthodontics']=='no') {
				$tariff[$ids['axa-dent-premium']] = 1;
				$tariff[$ids['axa-dent-komfort']] = 1;
			}
		}

	/* ADVIGON ZE TOP + ZB
	**
	** NEU
	*/

		if($age[$ids['advigon-ideal']] <= 70 && $step2['incare']=='no' &&  $step2['orthodontics']=='no')
		{
			$tariff[$ids['advigon-premium-zgp']] = 0;
			$tariff[$ids['advigon-ideal']] = 1;
		}


	/* ALLIANZ DENT BEST und DENT PLUS
	**
	** NEU
	*/
		if($step2['orthodontics']=='no' && ($age[$ids['allianz-dent-best']]<= 64 || $age[$ids['allianz-dent-plus']]<= 64)) {
			if($step2['tooth1']<=3 && $step2['incare']=='no' && $step2['periodontitisCured']!=1) {
				$tariff[$ids['allianz-dent-best']] = 1;
				$tariff[$ids['allianz-dent-plus']] = 1;

				$bonus[$ids['allianz-dent-best']] = $step2['tooth1'] * 5.4;
				$bonus[$ids['allianz-dent-plus']] = $step2['tooth1'] * 3.4;
			}
		}


	/* JANITOS
	**
	** NEU
	*/
		if($age[$ids['janitos-ja-dental-plus']] <= 80 && $step2['orthodontics']=='no') {
			$tariff[$ids['janitos-ja-dental-plus']] = 1;
		}


	/* ARAG Z90
	**
	** NEU
	*/
		#if($age[$ids['aragz90']] <= 54 && $step2['incare']=='no' && $step2['orthodontics']=='no') {
		#	$tariff[$ids['aragz90']] = 1;
		#}

	/* UKV
	**
	** NEU
	*/
		if($step2['tooth1']<=3 && $step2['incare'] == 'no')
		{
			$tariff[$ids['ukv-zp-premium']] = 1;
			$bonus[$ids['ukv-zp-premium']] = $step2['tooth1'] * 8.6;
			
			if($step2['orthodontics']=='yes')	$tariff['helpText'][$ids['ukv-zp-premium']] = L::_('KfoNVersichert');
		}

	/* MÃœNCHNER VEREIN
	**
	** NEU
	*/
		if($age[$ids['mv-571-572-573-574']]<=7) {
			$tariff[$ids['mv-571-572-573-574']] = 1;
			if($step2['orthodontics']=='yes')	$tariff['helpText'][$ids['mv-571-572-573-574']] = L::_('KfoNVersichert');
		}


	/* DIE BAYERISCHE VIP DENT PLUS
	**
	** NEU
	
		if($step2['changeInsurance']!='no' && ($step2['previousTarif']<8 || $step2['previousTarifInsured']!=0) && $step2['incare']!='yes' && $step2['periodontitis']!='yes' && $step2['tooth1']<=3 && $step2['tooth3']==0 && $step2['tooth4']<=6) {
			$tariff[$ids['bayerische-dentplus']] = 1;
			if($step2['orthodontics']=='yes') $tariff['helpText'][$ids['bayerische-dentplus']] = L::_('KfoNVersichert');
	}
	*/

	/* R + V
	**
	** NEU
	*/
		if($age[$ids['rv-premium-z1zv']] <= 80) {
			$tariff[$ids['rv-premium-z1zv']] = 1;
			if($step2['orthodontics']=='yes') {
				$tariff['helpText'][$ids['rv-premium-z1zv']] = L::_('KfoNVersichert');
				$tariff['series'][$ids['rv-premium-z1zv']] = 1;
			}
		}

	/* INTER Z90 Zpro
	**
	** NEU
	*/

		if($step2['tooth1'] < 6 && ($step2['tooth3'] + $step['tooth5']) < 14 && $step2['tooth5'] < 6)
		{
			$tariff[$ids['inter-z90-zpro']] = 1;
		}

	/* SIGNAL IDUNA KOMFORT+ & SIGNAL IDUNA KOMFORT ZAHN
	**
	** NEU
	*/
		if($age[$ids['sigidu-komstart']]<=60 && $step2['orthodontics']!='yes') {
			$tariff[$ids['sigidu-komstart']] = 1;
			$tariff[$ids['sigidu-komplus']] = 1;
		}


	/* DKV - KDT85
	**
	** NEU
	**
	** bonus: eg 2 toothes = 4€ p. tooth = 8€
	*/
		if($step2['tooth1']<=3) {
			if($step2['incare']=='no' && $step2['orthodontics']=='no') {
				$tariff[$ids['dkv-kdt85-kdbe']] = 1;
		}}


	/* UNIVERSA
	**
	** NEU
	*/
		$tariff[$ids['universa']] = 1;
			if($step2['orthodontics']=='yes') {
				$tariff['helpText'][$ids['universa']] = L::_('KfoNVersichert');
				$tariff['series'][$ids['universa']] = 1;
			}

	break;
	}

	return array('step2' => $step2, 'age' => $age, 'tariff' => $tariff, 'bonus' => $bonus,
		'bonusbase' => $bonusbase, 'tariffType' => $tariffType);
}





public function getPossibleContract($idco, $wishedBegin) {
	/* 
	** Diese Funktion soll anhand der vorgegebenen idco weitere Versicherungen liefern, 
	** die im CRM bei einem bestehenden Vertrag zur Duplizierung der Daten genutzt werden können.
	*/

	$insurances = contract::getInsurances(null,null,$idco, $wishedBegin);

	/* neue Tarife */
	$insurances['tariff'][project::gti('mv-71-72')] = 1;
	$insurances['tariff'][project::gti('inter-z90')] = 1;
	$insurances['tariff'][project::gti('inter-z90-zpro')] = 1;

	return (array('step2' => $insurances['step2'], 'age' => $insurances['age'], 't'=>$insurances['tariff'], 'b'=>$insurances['bonusbase'], 'bonus'=>$insurances['bonus'], 'tariffType'=>$insurances['tariffType'], 'idco'=>$idco));
}


public function addContractFromCalc ($idco, $idt, $mail, $clear, $wishedBegin=null) {
/*
** $idco = Datensatz aus dem ein neuer Contract erstellt wird
** $idt  = in welchen Tarif soll Transferiert werden
** $mail = wie soll die E-Mail aufgebaut sein
**
**
*/

	$calc = S::get('CRM', 'posCon');

	if($calc['tariffs_accepted'] && is_array($calc))
	{
		foreach($calc['tariffs_accepted'] as $key => $value)
		{
			if($idt == $value['idt'])
			{
				$cond = $value;
				break;
			}
		}
	}

	if($calc['tariffs_denied'] && is_array($calc) && !$cond)
	{
		foreach($calc['tariffs_denied'] as $key => $value)
		{
			if($idt == $value['idt'])
				$cond = $value;
		}
	}


	$wzm = new wzm();
	$p = new person();
	$c = new crm();	

	$t = $wzm->getContract($idco);

	$person = $p->get($t['pid']);
	$pid = $person['pid'];

	$address = $c->getAddressPrimary('pid', $t['pid'], true);
	$contact = $c->getContactPrimary('pid', $t['pid'], true);

	if($person['isFatherOfPid']) {
		$child = $p->get($person['isFatherOfPid']);
		$child_pid = $child['pid'];
			unset($child['pid']);
		$new_child = $p->add($child);
		$person['isFatherOfPid'] = $new_child;
	}

	unset($person['pid']);
	$new_pid = $p->add($person);
	$new_adid = $c->addAddress($address, 'pid', $new_pid, 'primary');
	$new_contact = $c->addContact($contact, 'pid', $new_pid, 'primary');

	$c->duplicateNoteByPid($pid, $new_pid);
	$wzm->duplicateSupportMessageByPid($pid, $new_pid);

	if($wishedBegin)
	{
		$t['wishedBegin']=$wishedBegin;
	}

	$ary = $this->getPossibleContract($idco, $t['wishedBegin']);

	if($clear === true)
	{
		unset($t);

		$t['changeInsurance']='yes';
		$t['birthdate']=$person['birthdate'];
		$t['addNotes']='CSS-Aktion vom 01.12.2014';
		$t['addNotes2']='Erstellt von Vertragsnummer #'.$idco;
	} else {
		unset($t['idco']);
		unset($t['dateSent']);
		unset($t['dateRec']);
		unset($t['datePolice']);
		unset($t['dateEmail']);
		unset($t['addNotes']);
		unset($t['series']);

		$t['addNotes2']='Erstellt von Vertragsnummer #'.$idco;

		switch($mail)
		{
			case 1:
				$t['addNotes2'].=', mit normaler E-Mail';
				break;
			case 2:
				$t['addNotes2'].=', mit Alternativangebot';
				break;
			case 3:
				$t['addNotes2'].=', mit normaler E-Mail (leer)';
				break;
			case 4:
				$t['addNotes2'].=', mit Alternativangebot (leer)';
				break;
			default:
				$t['addNotes2'].=', ohne E-Mail';
		}
		if(isset($ary['t']['helpText'][$idt])) $t['addNotes'] = $ary['t']['helpText'][$idt];
		if(isset($ary['t']['series'][$idt])) $t['series'] = $ary['t']['series'][$idt];
	}



	$t['idt']=$idt;
	$t['pid']=$new_pid;
	$t['contractStatusLid']=46;

	$t['bonusbase'] = $cond['bonusbase'];
	$t['bonus'] = $cond['surplus'];
	$t['price'] = $cond['bonusbase'] + $cond['surplus'];
	$t['series'] = $cond['series'];
	
	
	$t['signupTime']=date('Y-m-d H:i', time());
	
	$ret = $wzm->_insert('contract', $t);

	if(isset($ret)) return array('idco'=>$ret, 'contract'=>$t, 'person'=>$person, 'address'=>$address, 'contact'=>$contact);

}



public function addContractFromIdco ($idco, $idt, $mail, $clear, $wishedBegin=null) {

	$wzm = new wzm();
	$p = new person();
	$c = new crm();	

	$t = $wzm->getContract($idco);

	$person = $p->get($t['pid']);
	$pid = $person['pid'];

	$address = $c->getAddressPrimary('pid', $t['pid'], true);
	$contact = $c->getContactPrimary('pid', $t['pid'], true);

	if($person['isFatherOfPid']) {
		$child = $p->get($person['isFatherOfPid']);
		$child_pid = $child['pid'];
			unset($child['pid']);
		$new_child = $p->add($child);
		$person['isFatherOfPid'] = $new_child;
	}

	unset($person['pid']);
	$new_pid = $p->add($person);
	$new_adid = $c->addAddress($address, 'pid', $new_pid, 'primary');
	$new_contact = $c->addContact($contact, 'pid', $new_pid, 'primary');

	$c->duplicateNoteByPid($pid, $new_pid);
	$wzm->duplicateSupportMessageByPid($pid, $new_pid);

	if($wishedBegin)
	{
		$t['wishedBegin']=$wishedBegin;
	}

	$ary = $this->getPossibleContract($idco, $t['wishedBegin']);

	if($clear === true)
	{
		unset($t);

		$t['changeInsurance']='yes';
		$t['birthdate']=$person['birthdate'];
		$t['addNotes']='CSS-Aktion vom 01.12.2014';
		$t['addNotes2']='Erstellt von Vertragsnummer #'.$idco;
	} else {
		unset($t['idco']);
		unset($t['dateSent']);
		unset($t['dateRec']);
		unset($t['datePolice']);
		unset($t['dateEmail']);
		unset($t['addNotes']);
		unset($t['series']);

		$t['addNotes2']='Erstellt von Vertragsnummer #'.$idco;

		switch($mail)
		{
			case 1:
				$t['addNotes2'].=', mit normaler E-Mail';
				break;
			case 2:
				$t['addNotes2'].=', mit Alternativangebot';
				break;
			case 3:
				$t['addNotes2'].=', mit normaler E-Mail (leer)';
				break;
			case 4:
				$t['addNotes2'].=', mit Alternativangebot (leer)';
				break;
			default:
				$t['addNotes2'].=', ohne E-Mail';
		}
		if(isset($ary['t']['helpText'][$idt])) $t['addNotes'] = $ary['t']['helpText'][$idt];
		if(isset($ary['t']['series'][$idt])) $t['series'] = $ary['t']['series'][$idt];
	}



	$t['idt']=$idt;
	$t['pid']=$new_pid;
	$t['contractStatusLid']=46;

	$t['bonusbase']=$ary['b'][$idt]['activeBonusBase'];
	$t['bonus']=$ary['bonus'][$idt];
	if($idt == 17) { // Arag Z90, hier wird pro fehlendem Zahn mit 20% Aufschlag gerechnet
		$t['price']=$t['bonusbase'] * (1+ $t['bonus']);
	} else 
		$t['price']=$t['bonusbase'] + $t['bonus'];
	
	$t['signupTime']=date('Y-m-d H:i', time());
	
	$ret = $wzm->_insert('contract', $t);

	if(isset($ret)) return array('idco'=>$ret, 'contract'=>$t, 'person'=>$person, 'address'=>$address, 'contact'=>$contact);

}



public function checkSentContract($forename, $surname, $birthdate)
{
	/*
	** Suche nach Antragstellern, die den Antrag zurÃƒÂ¼ckgeschickt haben.
	** Sobald ein Datensatz ÃƒÅ“bereinstimmt, wird eine 1 zurÃƒÂ¼ckgegeben, welche kein Schreiben bekommt. 
	*/

	$ret = 0;

	$sql = 'SELECT
	person.forename,
	person.surname,
	person.birthdate
	FROM crm_person as person
	INNER JOIN wzm_contract ON (person.pid = wzm_contract.pid and wzm_contract.contractStatusLid = 49)

	WHERE 1=1 AND
		person.forename = \''.urlencode($forename).'\' AND
		person.surname = \''.urlencode($surname).'\' AND
		person.birthdate = \''.urlencode($birthdate).'\'
	GROUP BY forename, surname, birthdate';



	$db = $this->getDb();
	$rs = $db->queryPDO($sql);

	while($row = $db->fetchPDO($rs)) {
		$ret = 1;
	}
	return $ret;

}

public function getContractByPeriod($daysagoStart=20, $daysPeriod=1) {
	$akt_time = time(); 
	//$time_start = $akt_time - ($daysagoStart*24*60*60);	
	
	$sql = 'SELECT 
		contract.idco,
		person.forename,
		person.surname, 
		person.birthdate
		 FROM '.$this->table(contract).' as contract 
		INNER JOIN crm_person as person ON contract.pid = person.pid	
		WHERE 
		contract.dateSent =  date_sub(CURDATE(), INTERVAL '.$daysagoStart.' DAY) AND
		contract.contractStatusLid = 48
		order by contract.idco asc limit 0,250';



	$sql2 = 'CREATE TEMPORARY TABLE tmp_allPersonsHaveInsurance
			SELECT person.forename, person.surname, person.birthdate, md5(CONCAT(person.forename, person.surname, cast(person.birthdate as char(10)))) as hash  FROM crm_person as person
			INNER JOIN wzm_contract ON (person.pid = wzm_contract.pid and (wzm_contract.contractStatusLid = 49 or wzm_contract.contractStatusLid = 50 or (wzm_contract.contractStatusLid = 48 and wzm_contract.addNotes = \'CSS-Aktion vom 01.12.2014\')))
			WHERE 1=1
			GROUP BY person.forename, person.surname, person.birthdate;
		CREATE INDEX tmp_hash on tmp_allPersonsHaveInsurance (hash) USING BTREE;
		SELECT 
			min(person.pid) as pid, person.forename, person.surname, person.birthdate
			FROM wzm_contract AS contract
			INNER JOIN crm_person AS person ON contract.pid = person.pid
			WHERE contract.dateSent = date_sub(CURDATE(), INTERVAL '.$daysagoStart.' DAY)
			AND contract.contractStatusLid = 48
			AND contract.pid not in (SELECT cp.pid FROM crm_person cp INNER JOIN tmp_allPersonsHaveInsurance tmp ON cp.`hash` = tmp.`hash`)
			group by person.forename, person.surname, person.birthdate;';
		


	$db = $this->getDb();
	$rs = $db->queryPDO($sql);

	$ret = array();	
	$ret['daysAgo'] = $daysagoStart;
	$ret['num'] = $rs->numRows();


	while($row = $db->fetchPDO($rs)) {
		$sendReminder = $this->checkSentContract($row['forename'], $row['surname'], $row['birthdate']);

		if ($sendReminder==0)
		{
			$ret['result'][] = $row['idco'];
		}
	}
	return $ret;
}


public function sendTemplateByMail($template, $subject, $idco, $idt=null, $files=null, $isContract=false, $recipientBCC=null)
	{

		$c = $this->get($idco);
		$person = new person();
		$contact = new crm();
	
		$data['person'] = $person->get($c['pid']);
		$data['contact'] = $contact->getContactPrimary('pid', $c['pid']);

		// send user info mail
		//$sender = 'Zahnzusatzversicherung-Experten <info@zahnzusatzversicherung-experten.de>';
		$from = 'info@zahnzusatzversicherung-experten.de';
		$recipient = $data['contact']['email'];
		// subject will be set automatically


		$salutation = ($data['person']['salutationLid']==9)?
			'Sehr geehrter Herr '.$data['person']['surname']:
			'Sehr geehrte Frau '.$data['person']['surname'];


		$message = str_replace('{*ANREDE_NAME*}', $salutation, file_get_contents('templates/'.$template.'.tpl'));
		$message = str_replace('{*SIGNATURE*}', project::gts('html'), $message);

		emailHelper::sendFileMail($from, $recipient, $subject, $message, $files, $idt, $isContract, 'utf-8', $recipientBCC);
	}



// -----------------------------------------------------------------------------
private function getStatsData($from_days, $to_days=null)
{
	$to_days_sql="";
	if(isset($to_days)) $to_days_sql=" AND dateSent < date_sub(CURDATE() , INTERVAL 0 DAY)";

	$db = $this->getDb();
	$sql = "SELECT count(s) as anz, s, d, m, y FROM 
		(select 'sent' AS s, day(signupTime) as d, month(signupTime) AS m, year(signupTime) as y from wzm_contract WHERE 
		signupTime >= date_sub(CURDATE() , INTERVAL ".$from_days." DAY)  AND addNotes <> 'ADVIGON-Aktion vom 01.12.2014' ".$to_days_sql."
		UNION ALL ";

	if(isset($to_days)) $to_days_sql=" AND dateRec < date_sub(CURDATE() , INTERVAL 0 DAY)";
	
	$sql .="select 'ret' AS s, day(dateRec) as d, month(dateRec) AS m, year(dateRec) as y from wzm_contract WHERE contractStatusLid in ('49')
		AND dateRec >= date_sub(CURDATE() , INTERVAL ".$from_days." DAY) ".$to_days_sql."
		) RESULT GROUP BY s, y, m, d ORDER BY y, m, d, s ";
	$rs = $db->queryPDO($sql);
	return $rs;
}

// -----------------------------------------------------------------------------
public function getStats($type='aufmerksam', $days=30) {
	switch($type)
	{
		case "aufmerksam":
			$label = new label();
			$db = $this->getDb();
			$rs = $label->getLabels('contractStatus', 'lid');

			$rs2 = $db->queryPDO("SELECT COUNT(*) AS count FROM ".
				$this->table('contract')." WHERE contractStatusLid=0");
			
			$row = $db->fetchPDO($rs2);

			// init return array with count of unset contracts
			$ret = array(array('name' => L::_(363), 'count' => $row['count']));

			while($row = $db->fetchPDO($rs)) {
				$rs2 = $db->queryPDO("SELECT COUNT(*) AS count FROM ".
					$this->table('contract')." WHERE contractStatusLid='".
				$row['lid']."'");
				$row2 = $db->fetchPDO($rs2);
				$row['count'] = $row2['count'];
				$row['lid'] = $row['lid'];
			$ret[] = $row;
			}
			return $ret;
			break;
		case "in_out":
			$db = $this->getDb();

			// init Array
			$oneday = 86400;
			$bars = array("sent","ret");

			for($b=0; $b<count($bars); $b++)
			{
				for($i=$days; $i>=0; $i--)
				{
					$tag = date('d.m.Y',time()-($i*$oneday));
					$graph[$bars[$b]][$tag] = 0;
				}
			}

			// generate Data
			$rs = $this->getStatsData($days);

			while($row = $rs->fetch())
			{
				if($row['anz']>0)
				{
				$day = $row[d]<10?"0".$row[d]:$row[d];
				$month = $row[m]<10?"0".$row[m]:$row[m];
				$graph[$row[s]][$day.".".$month.".".$row[y]] = $row['anz'];
				}
			}
			
			return $graph;
			break;

		case "extra_stats":
			// generateData
			$arr = array(0,1,7,30);
			$txt['sent'] = array(412,414,416,418);
			$txt['ret'] = array(413,415,417,419);
 
			$init = $count['ret'] = $count['sent'] = 0;

			for($i=0; $i<count($arr);$i++)
			{
				// Sollte nur Daten mit Werten angezeigt werden, dann Zeile ausdokumentieren
				//$report[$arr[$i]] = array('sent' =>0, 'senttext' =>$txt['sent'][$i], 'ret' =>0, 'rettext' =>$txt['ret'][$i]);

				// Fehler abfangen - Gestern / Zeitraum
				if($arr[$i]==1) $rs_data = $this->getStatsData($arr[$i], 'set');
				else $rs_data = $this->getStatsData($arr[$i]);

				while($rs = $rs_data->fetch())
				{
					if($init!=$arr[$i])
					{
						// Wechsel der Tage
						$count['ret'] = $count['sent'] = 0;
						$init = $arr[$i];
					}
					if($rs['s']=='sent')
					{
					$report[$arr[$i]][$rs['s']] = $count[$rs['s']] + $rs['anz'];
					$report[$arr[$i]][$rs['s'].'text'] = $txt[$rs['s']][$i];
					$count[$rs['s']] += $rs['anz'];
					}
					else 
					{
					$report[$arr[$i]][$rs['s']] = $count[$rs['s']] + $rs['anz'];
					$report[$arr[$i]][$rs['s'].'text'] = $txt[$rs['s']][$i];
					$count[$rs['s']] += $rs['anz'];
					}
				}
			}
			return $report;
			break;
	}
}

public function getCalculatorData($data)
{
	$PIDSERVICE_URL="https://s4.zahnzusatzversicherung-experten.de/getCalculator/".$data;
	//$PIDSERVICE_USER="pdf";
	//$PIDSERVICE_PASSWD="234klsl23r4_sdfo34(h2l34n";


	//url-ify the data for the GET
	// foreach ($data as $key => $value) 
	// { 
  	//  $fields_string .= '/' . $value; 
	// }
	// rtrim($fields_string, '&');

	// Get cURL resource
	$curl = curl_init();

	// Set the url to authenticate
	curl_setopt($curl,CURLOPT_URL,$PIDSERVICE_URL);

	// ...my my my
	// $fh = fopen ('files/benefits-long/local.pdf', 'w+');
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

	curl_setopt($curl, CURLOPT_TIMEOUT, 50);
	//curl_setopt($curl, CURLOPT_FILE, $fh);

	// Set the authentication options
	// curl_setopt($curl, CURLOPT_USERPWD, $PIDSERVICE_USER.":".$PIDSERVICE_PASSWD);
	// curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

	// Include header in result? (0 = no, 1 = yes)
	curl_setopt($curl, CURLOPT_HEADER, 0);

	curl_setopt($curl, CURLOPT_POST, 0);
	//curl_setopt($curl, CURLOPT_POSTFIELDS, $fields_string);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
	// Download the given URL
	$result = curl_exec($curl);
	curl_close($curl);
	//fclose($fh);

return $result;
}

public function getBenefitsLong($data)
{
	$PIDSERVICE_URL="https://rest.zahnzusatzversicherung-experten.com/api/benefits-long";
	$PIDSERVICE_USER="pdf";
	$PIDSERVICE_PASSWD="234klsl23r4_sdfo34(h2l34n";

	/*
	$fields = array(
  	'tariffId'                => 23,
 	'series'                   => 0,
 	'sourcePage'			   => 'zzv-neu'
	);
	*/


	//url-ify the data for the GET
	foreach ($data as $key => $value) 
	{ 
  	  $fields_string .= $key . '=' . $value . '&'; 
	}
	rtrim($fields_string, '&');

	// Get cURL resource
	$curl = curl_init();

	// Set the url to authenticate
	curl_setopt($curl,CURLOPT_URL,$PIDSERVICE_URL);

	// ...my my my
	$fh = fopen ('files/benefits-long/local.pdf', 'w+');
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

	curl_setopt($curl, CURLOPT_TIMEOUT, 50);
	curl_setopt($curl, CURLOPT_FILE, $fh);

	// Set the authentication options
	curl_setopt($curl, CURLOPT_USERPWD, $PIDSERVICE_USER.":".$PIDSERVICE_PASSWD);
	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

	// Include header in result? (0 = yes, 1 = no)
	curl_setopt($curl, CURLOPT_HEADER, 0);

	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $fields_string);

	// Download the given URL
	curl_exec($curl);
	curl_close($curl);
	fclose($fh);
}

public function getCoverSheet($data)
{
	$PIDSERVICE_URL="https://rest.zahnzusatzversicherung-experten.com/api/cover-sheet";
	$PIDSERVICE_USER="pdf";
	$PIDSERVICE_PASSWD="234klsl23r4_sdfo34(h2l34n";





	/*
	$fields = array(
  	'tariff-id'                => 23,
 	'first-name'               => 'Söder',
  	'last-name'                => 'Schultheiß',
  	'birthdate'                => '04.08.1981',
  	'insurance-begin-date'     => '01.01.2016',
  	'series'                   => 0,
  	'missing-teeth'            => 0,
  	'replaced-teeth-removable' => 0,
  	'replaced-teeth-fix'       => 0,
  	'uid'                      => 'test',
	'price'		      => 9.5,
	'path'			      => $path
	);
	*/

	$fields_string = $path = '';

	if(isset($data['path'])) 
		$path = $data['path'];

	//url-ify the data for the POST
	foreach ($data as $key => $value) 
	{ 
	  if($key != 'path')
	  	  $fields_string .= $key . '=' . $value . '&'; 
	}
	rtrim($fields_string, '&');


	#$file = $path.'save.cover-sheet.data'.date("dmY");
	#file_put_contents($file, serialize($data).'~', FILE_APPEND | LOCK_EX);


	// ...my my my
	#$path = project::getprojectRoot();
	#if(substr($path, -1) != '/')
	#	$path .= '/';
	


	// Get cURL resource
	$curl = curl_init();

	// Set the url to authenticate
	curl_setopt($curl,CURLOPT_URL,$PIDSERVICE_URL);


	if (file_exists($path.'files/deckblatt/local.pdf'))
	{
		unlink($path.'files/deckblatt/local.pdf');
	}

	$fh = fopen ($path.'files/deckblatt/local.pdf', 'w+');
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_TIMEOUT, 50);
		curl_setopt($curl, CURLOPT_FILE, $fh);

		#curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

		// Set the authentication options
		curl_setopt($curl, CURLOPT_USERPWD, $PIDSERVICE_USER.":".$PIDSERVICE_PASSWD);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

		// Include header in result? (0 = yes, 1 = no)
		curl_setopt($curl, CURLOPT_HEADER, 0);
		// Should cURL return or print out the data? (true = return, false = print)
		#curl_setopt($curl, CURLOPT_RETURNTRANSFER, false);

		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $fields_string);

		// Download the given URL
		curl_exec($curl);
		curl_close($curl);
		fclose($fh);
	
}



// -----------------------------------------------------------------------------

public function generatePdf($data, $tariff) {
	if(empty($tariff)) {
		return false;
	}

	if(empty($this->files[$tariff])) {
		return false;
	}

	// read file
	$handle = fopen($this->files[$tariff], 'r');
	$pdf = fread($handle, filesize($this->files[$tariff]));
	fclose($handle);

	// replace data
	foreach($this->fields[$tariff] as $key => $len) {
		$needle = str_pad($key, $len, '0');
		$target = str_pad(substr($data[$key], 0, $len), $len);
		$pdf = str_replace($needle, $target, $pdf);
	}

	// save file
	$outputFile = 'output/'.time().'.pdf';
	$writeHandle = fopen($outputFile, 'w+');
	fwrite($writeHandle, $pdf);
	fclose($writeHandle);

	return $outputFile;
}

public function getShowJustPhone ()
{
	return $this->showJustPhone;
}

public function setShowJustPhone ($var)
{
	$this->showJustPhone = $var;
}


// -----------------------------------------------------------------------------

} // end class

