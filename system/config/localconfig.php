<?php

### INSTALL SCRIPT START ###
$GLOBALS['TL_CONFIG']['licenseAccepted'] = true;
$GLOBALS['TL_CONFIG']['installPassword'] = '$2y$10$SjMw/8FL/jPr8GsZRis8bOvlGSvDdQUg0dZvnvg5o3OtPg2J3yYAy';
$GLOBALS['TL_CONFIG']['exampleWebsite'] = 1552063940;
$GLOBALS['TL_CONFIG']['pct_themedesigner_hidden'] = true;
$GLOBALS['TL_CONFIG']['adminEmail'] = 'info@zahnzusatzversicherung-experten.de';
$GLOBALS['TL_CONFIG']['minifyMarkup'] = true;
$GLOBALS['TL_CONFIG']['gzipScripts'] = true;
$GLOBALS['TL_CONFIG']['gdMaxImgWidth'] = 12000;
$GLOBALS['TL_CONFIG']['gdMaxImgHeight'] = 12000;
$GLOBALS['TL_CONFIG']['maxFileSize'] = 19048000;
$GLOBALS['TL_CONFIG']['imageWidth'] = 2000;
$GLOBALS['TL_CONFIG']['imageHeight'] = 2000;
$GLOBALS['TL_CONFIG']['cron_limit'] = 1000;
$GLOBALS['TL_CONFIG']['fontawesome'] = 'local';
$GLOBALS['TL_CONFIG']['fontaweseomeSource'] = 'local';
$GLOBALS['TL_CONFIG']['pct_themedesigner_off'] = false;
$GLOBALS['TL_CONFIG']['enableGlossar'] = false;
$GLOBALS['TL_CONFIG']['uploadTypes'] = 'jpg,jpeg,gif,png,ico,svg,svgz,odt,ods,odp,odg,ott,ots,otp,otg,pdf,csv,doc,docx,dot,dotx,xls,xlsx,xlt,xltx,ppt,pptx,pot,potx,mp3,mp4,m4a,m4v,webm,ogg,ogv,wma,wmv,ram,rm,mov,fla,flv,swf,ttf,ttc,otf,eot,woff,woff2,css,scss,less,js,html,htm,txt,zip,rar,7z,cto';
$GLOBALS['TL_CONFIG']['acceptTeasersAsContent'] = true;
$GLOBALS['TL_CONFIG']['illegalChars'] = '")(=?.,;~:\'\\>\\&lt;+\\/\\&lt;';
$GLOBALS['TL_CONFIG']['installCount'] = 0;
$GLOBALS['TL_CONFIG']['dateFormat'] = 'd.m.Y';
$GLOBALS['TL_CONFIG']['datimFormat'] = 'd.m.Y H:i';
$GLOBALS['TL_CONFIG']['pct_themedesigner_nofonts'] = true;
$GLOBALS['TL_CONFIG']['glossarPurgable'] = true;
$GLOBALS['TL_CONFIG']['disableGlossarCache'] = true;
$GLOBALS['TL_CONFIG']['strictSearch'] = 1;
$GLOBALS['TL_CONFIG']['glossarMaxWidth'] = 200;
$GLOBALS['TL_CONFIG']['glossarMaxHeight'] = 200;
$GLOBALS['TL_CONFIG']['allowedTags'] = '<iframe><a><abbr><acronym><address><area><article><aside><audio><b><bdi><bdo><big><blockquote><br><base><button><canvas><caption><cite><code><col><colgroup><data><datalist><dataset><dd><del><dfn><div><dl><dt><em><fieldset><figcaption><figure><footer><form><h1><h2><h3><h4><h5><h6><header><hgroup><hr><i><img><input><ins><kbd><keygen><label><legend><li><link><map><mark><menu><nav><object><ol><optgroup><option><output><p><param><picture><pre><q><s><samp><section><select><small><source><span><script><strong><style><sub><sup><table><tbody><td><textarea><tfoot><th><thead><time><tr><tt><u><ul><var><video><wbr>';
$GLOBALS['TL_CONFIG']['folderUrl'] = false;
$GLOBALS['TL_CONFIG']['useAutoItem'] = true;
$GLOBALS['TL_CONFIG']['seoSerpCache'] = false;
$GLOBALS['TL_CONFIG']['AutoBackupCount'] = 30;
$GLOBALS['TL_CONFIG']['disableRefererCheck'] = false;
$GLOBALS['TL_CONFIG']['lazyPlaceholder'] = 'transparent';
$GLOBALS['TL_CONFIG']['defaultChmod'] = 'a:10:{i:0;s:2:"u1";i:1;s:2:"u2";i:2;s:2:"u3";i:3;s:2:"u4";i:4;s:2:"u5";i:5;s:2:"u6";i:6;s:2:"g4";i:7;s:2:"g5";i:8;s:2:"g6";i:9;s:2:"w4";}';
### INSTALL SCRIPT STOP ###
