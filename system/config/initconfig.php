<?php

// only show ThemeDesigner, DemoInstaller and/or ThemeInstaller for backend user by id

if(TL_MODE == 'BE')
{
    $arrUsers = array(); // user-id
    $objUser = \BackendUser::getInstance();

    if( !in_array($objUser->id, $arrUsers) )
    {
        // hide ThemeDesigner
        unset($GLOBALS['BE_MOD']['design']['pct_themedesigner']);
        // hide DemoInstaller
        unset($GLOBALS['BE_MOD']['design']['pct_demoinstaller']);
        // hide ThemeInstaller
        unset($GLOBALS['BE_MOD']['system']['pct_theme_installer']);

        unset($GLOBALS['BE_MOD']['content']['calendar']);

        unset($GLOBALS['BE_MOD']['content']['faq']);

        unset($GLOBALS['BE_MOD']['content']['newsletter']);

        unset($GLOBALS['BE_MOD']['content']['comments']);

        unset($GLOBALS['BE_MOD']['system']['glossar_log']);

        unset($GLOBALS['BE_MOD']['system']['glossar_status']);
    }
}
