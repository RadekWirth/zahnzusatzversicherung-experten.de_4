<?php

/*
 * @copyright  trilobit GmbH
 * @author     trilobit GmbH <https://github.com/trilobit-gmbh>
 * @license    LGPL-3.0-or-later
 * @link       http://github.com/trilobit-gmbh/contao-calculator-bundle
 */

namespace Trilobit\HeaderfootercodeBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Configures the trilobit header- and footer code bundle.
 *
 * @author trilobit GmbH <https://github.com/trilobitgmbh>
 */
class TrilobitHeaderfootercodeBundle extends Bundle
{
}
