<?php

namespace Sioweb\GlossarBundle\ContaoManager;

use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;

/**
 * Plugin for the Contao Manager.
 *
 * @author Sascha Weidner <https://www.sioweb.de>
 */

class Plugin implements BundlePluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create('Sioweb\GlossarBundle\SiowebGlossarBundle')
                ->setLoadAfter(['Contao\CoreBundle\ContaoCoreBundle', 'Contao\CalendarBundle\ContaoCalendarBundle'])
                ->setReplace(['siowebglossar']),
        ];
    }
}
