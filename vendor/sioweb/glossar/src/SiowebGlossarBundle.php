<?php

namespace Sioweb\GlossarBundle;
use Symfony\Component\HttpKernel\Bundle\Bundle;


/**
 * Configures the Contao Glossar bundle.
 *
 * @author Sascha Weidner <https://www.sioweb.de>
 */
class SiowebGlossarBundle extends Bundle
{
}