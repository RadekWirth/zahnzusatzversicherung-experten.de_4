/* =============================================================================
 * Set viewport meta tag
 * ========================================================================== */
jQuery(document).ready(function()
{
	if( jQuery('body').hasClass('mobile') )
	{
		jQuery('head').append('<meta name="viewport" content="width=device-width,initial-scale=1.0">');
	}
});


/* =============================================================================
 * IOS workaround for wrong position bug under IOS11,12
 * ========================================================================== */
jQuery(document).ready(function()
{
	if( jQuery('body').hasClass('ios') )
	{
		// current chrome, safari 11, safari 12
		if( jQuery('body').hasClass('ch70') || jQuery('body').hasClass('sf11') || jQuery('body').hasClass('sf12')  )
		{
			jQuery("html, body").animate({scrollTop: 0});
		}
	}
});


/* =============================================================================
 * Universal optin loader
 * ========================================================================== */
function Eclipse_optin(strElementType)
{
	if(strElementType == undefined || strElementType == '')
	{
		return;
	}
	// user settings not applied yet
	if(localStorage.getItem('user_privacy_settings') == undefined || localStorage.getItem('user_privacy_settings') == '' || localStorage.getItem('user_privacy_settings') <= 0)
	{
		return
	}

	// find all scripts having a data-src attribute
	var targets = jQuery(strElementType+'[data-src]');

	if(targets.length > 0)
	{
		jQuery.each(targets,function(i,e)
		{
			var privacy = jQuery(e).data('privacy');
			if(privacy == undefined)
			{
				privacy = 0;
			}

			var attr = 'src';
			if(strElementType == 'link')
			{
				attr = 'href';
			}
			else if(strElementType == 'object')
			{
				attr = 'data';
			}

			if(localStorage.getItem('user_privacy_settings') >= privacy)
			{
				jQuery(e).attr(attr,jQuery(e).data('src') );
			}
		});
	}
}

jQuery(document).ready(function()
{
	Eclipse_optin('script');
	Eclipse_optin('link');
	Eclipse_optin('iframe');
	Eclipse_optin('object');
	Eclipse_optin('img');
});

// listen to Eclipse.user_privacy Event
jQuery(document).on('Eclipse.user_privacy',function(event,params)
{
	Eclipse_optin('script');
	Eclipse_optin('link');
	Eclipse_optin('iframe');
	Eclipse_optin('object');
	Eclipse_optin('img');
});


/* =============================================================================
 * Eclipse_setPrivacy(intLevel)
 * @param integer	Level of privacy
 * ========================================================================== */
function Eclipse_setPrivacy(intLevel)
{
	// set local storage
	localStorage.setItem('user_privacy_settings',intLevel);
	// set cookie for php
	jQuery.cookie('user_privacy_settings',intLevel,{expires:30,path:'/'});
	// fire event
	jQuery(document).trigger('Eclipse.user_privacy',{'level':intLevel});
}


/* =============================================================================
 * Eclipse_clearPrivacy(blnReload)
 * @param boolean	Reload page true/false
 * ========================================================================== */
function Eclipse_clearPrivacy(blnReload)
{
	if(blnReload == undefined || blnReload == null)
	{
		blnReload = true;
	}

	// clear local storage
	localStorage.removeItem('user_privacy_settings');
	// empty cookie
	jQuery.removeCookie('user_privacy_settings',{path:'/'});
	// fire event
	jQuery(document).trigger('Eclipse.clear_privacy_settings',{});

	// reload page
	if(blnReload)
	{
		location.reload();
	}
}


/* =============================================================================
 * mainmenu menuheader (deactivate link)
 * ========================================================================== */
jQuery(document).ready(function(){
	jQuery('.mainmenu .menuheader').removeAttr("href");
});

/* =============================================================================
 * mainmenu avoid_click
 * ========================================================================== */
jQuery(document).ready(function(){
	jQuery('.mainmenu a.avoid_click, .smartmenu-content a.avoid_click, #mobnav a.avoid_click').attr("href","javascript:void(0);");
	jQuery('.mainmenu a.avoid-click, .smartmenu-content a.avoid-click, #mobnav a.avoid-click').attr("href","javascript:void(0);");
});

/* =============================================================================
 * ce_parallax scripts/parallax/
 * ========================================================================== */
/*if(!jQuery('body').hasClass('ios')){
	jQuery(window).stellar({
		horizontalScrolling: false,
		responsive:true,
	})
}*/

/* =============================================================================
 * ce_table responsive
 * ========================================================================== */
function respTables() {
	jQuery('.ce_table').each(function() {
	 	var tableWidth = jQuery(this).find('table').width();
	 	var ce_tableWidth = jQuery(this).width();
	 	if (tableWidth > ce_tableWidth) {
		 	jQuery(this).addClass('overflow');
	 	} else {
		 	jQuery(this).removeClass('overflow');
	 	}
	});
};

jQuery(document).ready(function(){
	respTables();
});

jQuery(window).on("resize", function(){
	respTables();
});

/* =============================================================================
 * css3 animation (css/animate.css)
 * ========================================================================== */
var el = jQuery("body");
var animationClasses = ["fadeIn", "zoomIn", "fadeInDown","fadeInDownBig","fadeInLeft","fadeInLeftBig","fadeInRight","fadeInRightBig","fadeInUp","fadeInUpBig","rotateIn","zoomIn","slideInDown","slideInLeft","slideInRight","slideInUp","bounceIn","bounceInDown","bounceInLeft","bounceInRight","bounceInUp"];
jQuery.each(animationClasses, function(key, value){
	el.find("." + value).each(function(){
		jQuery(this).removeClass(value).attr("data-animate", value);

	});
});

jQuery(document).ready(function() {
	var animate_start = function(element) {
		element.find('.animate').not('.nowaypoint').each(function() {
			var item = jQuery(this);
		    var animation = item.data("animate");
		    if(jQuery('body').hasClass('ios') || jQuery('body').hasClass('android')) {
		    	item.removeClass('animate');
		    	return true;

		    } else {

	            item.waypoint(function(direction) {
	    			item.removeClass('animate').addClass('animated '+animation).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
	    				item.removeClass(animation+' animated');
	    			});
	            },{offset: '90%',triggerOnce: true});
	        }
		});
	};

	setTimeout(function() {
		jQuery('.mod_article').each(function() {
	        animate_start( jQuery(this) );
	    });
	},500);

});

/* =============================================================================
 * ce_youtube_background (scripts/jquery.mb.YTPlayer/)
 * ========================================================================== */
jQuery(document).ready(function() {
	if(!jQuery('body').hasClass('mobile')) {
		jQuery(".player").mb_YTPlayer();
	}
});

/* =============================================================================
 * scrollToTop
 * ========================================================================== */
jQuery(document).ready(function(){
	jQuery('a.totop').click(function(e){
    	e.preventDefault();
    	jQuery("html, body").animate({scrollTop: jQuery('#contentwrapper').offset().top - 100}, 500, 'linear');
	});
});

/* =============================================================================
 * sticky header
 * ========================================================================== */
jQuery(document).ready(function()
{
	var header = jQuery('#header');
	if(header)
	{
		var childs = header.clone();

		var inner = '';
		childs.wrap(function()
		{
		  	inner += jQuery( this ).html();
		});
		inner += '';

		var stickyheader = document.createElement('div');
		jQuery(stickyheader).attr('id','stickyheader').addClass('stickyheader').html(inner).insertAfter('.body_bottom');
		jQuery('#stickyheader .inside').wrap('<div class="header cloned"></div>');
		jQuery(stickyheader).find('a').removeAttr('tabindex');
	}
});

function headerFixed() {
	var topHeight = jQuery("#top-wrapper").outerHeight();
	if(topHeight == jQuery(window).innerHeight())
	{
		topHeight = jQuery(window).innerHeight() / 3;
	}

	if (jQuery(this).scrollTop() > topHeight) {
		jQuery("body").addClass("fixed-header");

	} else {
		jQuery("body").removeClass("fixed-header");
    }
};

jQuery(document).ready(function(){
	headerFixed();
});

jQuery(window).scroll(function() {
	headerFixed();
});

/* =============================================================================
 * generate smartmenu on onepage_page
 * ========================================================================== */
jQuery(document).ready(function() {

	/* delete menu of smartmenu */
	var smartmenu = jQuery('body.onepage_page .body_bottom .smartmenu-content .smartmenu-table');
	smartmenu.empty();

	/* clone mainmenu to smartmenu */
	var mobSmartmenu = jQuery("body.onepage_page .header.original nav.mainmenu ul.level_1").clone().removeClass("mainmenu");
	smartmenu.append(mobSmartmenu);
});

/* =============================================================================
 * smartmenu
 * ========================================================================== */
jQuery(document).ready(function(){

	jQuery('.smartmenu-trigger').click(function(e){
		jQuery('.smartmenu-content').addClass('open');
		jQuery('.smartmenu').addClass('open');
		jQuery('body').addClass('no_scroll');
	});

	jQuery('.smartmenu-content .smartmenu-close').click(function(e){
		jQuery('.smartmenu-content').removeClass('open');
		jQuery('.smartmenu').removeClass('open');
		jQuery('body').removeClass('no_scroll');
	});

	jQuery('.smartmenu-content a:not(.submenu)').click(function(e){
		// is not new window
		if(jQuery(e.target).attr('target') != '_blank')
		{
			jQuery('.smartmenu-content').removeClass('open');
			jQuery('body').removeClass('no_scroll');
		}
	});

	// open trail content on page load
	jQuery('.smartmenu-content').find('.trail').addClass('open');
	jQuery('.smartmenu-content').find('.trail > ul').show();
});

jQuery(document).ready(function(){

	jQuery('.smartmenu-content .subitems_trigger').on('click', function(){
		var element = jQuery(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp();
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown();
			element.siblings('li').children('ul').slideUp();
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp();
		}
	});

});

/* =============================================================================
 * mmenu // jquery.mmenu.min.all.js
 * ========================================================================== */
jQuery(document).ready(function() {

	var mobnav = jQuery("body.onepage_page .header nav.mainmenu").clone().attr("id","mobnav").removeClass("mainmenu");
	jQuery('.body_bottom').append(mobnav);

	mobnav = jQuery("#mobnav");

	// inject additional layout section [mmenu_top]
	var content_mmenu_top = '';
	if(jQuery('#mmenu_top'))
	{
		content_mmenu_top = jQuery('#mmenu_top').html();
		mobnav.addClass('has_section mmenu_top');
		jQuery('body > #mmenu_top').remove();
	}

	// inject additional layout section [mmenu_bottom]
	var content_mmenu_bottom = '';
	if(jQuery('#mmenu_bottom'))
	{
		content_mmenu_bottom = jQuery('#mmenu_bottom').html();
		mobnav.addClass('has_section mmenu_bottom');
		jQuery('body > #mmenu_bottom').remove();
	}

	// init mmenu
	mobnav.mmenu(
	{
		"navbars":
		[
			{"position": "top", "content": [content_mmenu_top]},
			{"position": "bottom","content": [content_mmenu_bottom]}
		],
		setSelected: {parent: true, current: true},
		onClick: {close: false, preventDefault: false}
	});

	// open mmenu
	var api = mobnav.data('mmenu');
	jQuery('#nav-open-btn, #stickyheader #nav-open-btn').click(function(e)
	{
		e.preventDefault();
		api.open();
	});

	// is onepage
	if(mobnav.hasClass('onepagenav')){
		jQuery("#mobnav a:not(.backlink)").bind('click',function(e)
		{
	   		if(jQuery('body').hasClass('is_regular_page'))
	   		{
		   		window.location = jQuery(this).attr('href');
		   		return true;
	   		}
	   		else
	   		{
	   			api.close();
	   		}
	 	});
	}

	// use internal forward pages as placeholders
	mobnav.find('li.forward').click(function(e)
	{
		// check if button has a nested level
		var next = jQuery(this).find('.mm-next');
		if (next == undefined || next == null)
		{
			return true;
		}
		// prevent regular click event
		e.preventDefault();
		// open panel
		api.openPanel( jQuery( next.data('target') ) );
	});


});

/* =============================================================================
 * megamenu
 * ========================================================================== */
jQuery(document).ready(function(){
	jQuery('.mainmenu li.megamenu .level_2').wrap('<div class="megamenu-wrapper"></div>');
	jQuery('li.megamenu.remove-link a.a-level_2').removeAttr("href");
});

function megamenuWidth() {
	var elWidth = jQuery(".header .inside").width();
	jQuery(".header .mainmenu ul .megamenu-wrapper").css('width', elWidth);
};

jQuery(window).on("resize", function(){
	megamenuWidth();
});

jQuery(document).ready(function(){
	megamenuWidth();
});

/* =============================================================================
 * doubleTapToGo scripts/doubletaptogo/
 * ========================================================================== */
jQuery(document).ready(function(){
	if(jQuery('body').hasClass('android') || jQuery('body').hasClass('win') || jQuery('body').hasClass('ios')){
		jQuery('nav.mainmenu li.submenu:not(.level_2 .megamenu)').doubleTapToGo();
		jQuery('.smartmenu li.submenu').doubleTapToGo();
		jQuery('.top_metanavi li.submenu').doubleTapToGo();
		jQuery('.header_metanavi li.submenu').doubleTapToGo();
		jQuery('.mod_langswitcher li').doubleTapToGo();

		jQuery.each(jQuery('.mod_langswitcher li a'),function(i,elem)
		{
			jQuery(elem).click(function(e) { e.preventDefault(); window.location.href = jQuery(this).attr('href'); });
		});
	}
});

/* =============================================================================
 * Add no-scroll class to body when lightbox opens
 * ========================================================================== */
jQuery(document).bind('cbox_open',function() {
	jQuery('body').addClass('no_scroll');
});

jQuery(document).bind('cbox_closed',function() {
	jQuery('body').removeClass('no_scroll');
});

/* =============================================================================
 * remove class mainmenu in offcanvas menu
 * ========================================================================== */
jQuery(document).ready(function(){
	jQuery('.mm-menu').removeClass('mainmenu');
});

/* =============================================================================
 * offcanvas-top
 * ========================================================================== */
jQuery(document).ready(function(){
	jQuery('.offcanvas-trigger').click(function(){
		jQuery('#offcanvas-top').toggleClass('offcanvas-top-open');
		jQuery('.offcanvas-trigger').toggleClass('offcanvas-top-open');
	});
});


/* =============================================================================
 * one-page-nav
 * ========================================================================== */

/**
 * Add an item counter class to the body on one_page sites
 */
jQuery(document).ready(function()
{
	if(jQuery('body').hasClass('onepage_page'))
	{
		// count articles in #slider
		jQuery('body').addClass( 'onepage_items_' + jQuery('#slider .inside > .mod_article').length );
	}
});

/**
 * Initialize smooth scrolling to one page anchor links
 */
jQuery(document).ready(function() {

	if(jQuery('body').hasClass('is_regular_page'))
	{
		jQuery('.smartmenu-content a').addClass('backlink');
		jQuery('.onepage_page a').addClass('backlink');
	}

	var stickyheader = jQuery(".stickyheader");
	var header = jQuery(".header.original");
	var lastActive = null;
	var links = jQuery(".onepage_page .mainmenu a:not(.backlink), .onepage_page .onepagenav a:not(.backlink), .page_navigation a:not(.backlink), .smartmenu-content a:not(.backlink)");
	var horizontalScroll = jQuery('body').hasClass('horizontal_scrolling');
	// not on mobile devices because content is 100%
	if(horizontalScroll && jQuery('body').hasClass('mobile'))
	{
		horizontalScroll = false;
	}

	var duration = 300;
	var timeout = 200;

	// jump to hashtag anchor in url
	var strHash = window.location.hash.toString();
	if(strHash.length > 0)
	{
		var target = jQuery(strHash);
		jQuery(strHash+' a').addClass('active');

		lastActive = jQuery(".onepagenav a:not(.backlink)[href*=\\"+strHash+"]");
		lastActive.addClass('active');

		var offsetX = 0;
		//var posX = target.offset().left - offsetX;

		var offsetY = stickyheader.height();
		//var posY = target.offset().top - offsetY;

		jQuery("html, body").animate({scrollTop: posY, scrollLeft:posX},
		{
			start		: function()
			{
				jQuery.cookie('onepage_animate',1);
			},
			complete	: function()
			{
				setTimeout(function()
				{
					jQuery.cookie('onepage_animate',0);
				}, timeout);
			}
		}, 0);
	}

	// set the last anchor to active on page load and scroll to it
	if(jQuery.cookie('onepage_active') !== null && jQuery.cookie('onepage_active') != undefined && strHash.length < 1)
	{
		jQuery.each(links, function(index, elem) {
			var hash = this.href.split("#");
			if(!hash[1])
			{
				return;
			}
			var anchor = hash[1].toString();
			if(anchor == jQuery.cookie('onepage_active'))
			{
				jQuery(elem).addClass('active');
				lastActive = jQuery(elem);
			}
		});

		// find target in page
		var target = jQuery("#"+jQuery.cookie('onepage_active'));
		if(target.length > 0)
		{
			var offsetX = 0;
			var posX = target.offset().left - offsetX;

			var offsetY = stickyheader.height();
			var posY = target.offset().top - offsetY;

			jQuery("html,body").stop().animate({scrollTop: posY, scrollLeft: posX}, {
				duration	: 0,
				start		: function()
				{
					// on start: flag that onepage started its animation
					jQuery.cookie('onepage_animate',1);
				},
				complete	: function()
				{
					setTimeout(function()
					{
						jQuery.cookie('onepage_animate',0);
						jQuery.cookie('onepage_position',null);
					}, timeout);
				}
			});
		}
	}

	// click event listener
	links.click(function(event)
	{
		var hash = this.href.split("#");
		if(!hash[1])
		{
			return true;
		}
		var anchor = hash[1].toString();
		var target = jQuery("#"+anchor);
		if(target.length < 1)
		{
			return false;
		}

		// store the current active anchor as cookie for further use
		jQuery.cookie('onepage_active',anchor);

		var offsetX = 0;
		var posX = target.offset().left - offsetX;

		var offsetY = stickyheader.height();
		var posY = target.offset().top - offsetY;

		jQuery("html,body").stop().animate({scrollTop: posY, scrollLeft: posX},
		{
			duration	: duration,
			start		: function()
			{
				jQuery.cookie('onepage_animate',1);
			},
			step		: function()
			{
				// on start: flag that onepage started its animation
				jQuery.cookie('onepage_animate',1);
			},
			complete	: function()
			{
				// on complete: remove the flag
				setTimeout(function()
				{
					jQuery.cookie('onepage_animate',0);
				}, timeout);

				// store last position
				jQuery.cookie('onepage_position',{'x':posX,'y':posY,'anchor':anchor});
			}
		});

		// toggle active class
		if(lastActive != jQuery(this))
		{
			links.removeClass('active');

			jQuery(".onepagenav a[href*="+anchor+"]").addClass('active');
			jQuery(".mainmenu a[href*="+anchor+"]").addClass('active');

			jQuery(this).addClass('active');
		}

		event.preventDefault();
		return false;
	});
});


/**
 * Set navi active on scroll
 */
jQuery(document).ready(function() {
	var links = jQuery(".onepage_page .mainmenu a:not(.backlink), .onepage_page .onepagenav a:not(.backlink), .page_navigation a");
	var stickyheader = jQuery(".stickyheader");
	var header = jQuery(".header.original");
	var lastActive = jQuery(".onepage_page .mainmenu a.active, .onepage_page .onepagenav a.active");
	var horizontalScroll = jQuery('body').hasClass('horizontal_scrolling');
	// not on mobile devices because content is 100%
	if(horizontalScroll && jQuery('body').hasClass('mobile'))
	{
		horizontalScroll = false;
	}

	var lastScrollX = 0;
	var lastScrollY = 0;

	jQuery(window).scroll(function()
	{
		// escape when animation is running
		if(jQuery.cookie('onepage_animate') > 0)
		{
			return;
		}

		// remove active class from all links
		links.removeClass('active');

		var scrollX = jQuery(window).scrollLeft();
		var scrollY = jQuery(window).scrollTop();
		var offsetX = 10;
		var offsetY = stickyheader.height();

		jQuery.each(links, function(index, elem)
		{
			var hash = elem.href.split("#");
			if(!hash[1])
			{
				return;
			}
			var anchor = hash[1].toString();
			var target = jQuery("#"+anchor);
			if(target.length < 1)
			{
				return;
			}

			// vertical scrolling
			var posY = target.offset().top - offsetY;
			var sizeY = posY + target.height();
			if (posY <= scrollY && sizeY >= scrollY && !horizontalScroll)
			{
	        	jQuery(elem).addClass("active");
	        }

	        // horizontal scolling
	        var posX = target.offset().left - offsetX;
			var sizeX = posX + target.width();
			if (posX <= scrollX && sizeX >= scrollX && horizontalScroll)
			{
	        	jQuery(elem).addClass("active");
	        }
	    });
	});

});

/* =============================================================================
 * imagebox workaround if content height > fix height
 * ========================================================================== */
function imageboxHeight() {
	jQuery(".ce_text_imagebox").each(function() {
		var fixHeight = jQuery(this).height();
		var contentHeight = jQuery(this).find(".inside").outerHeight();
		if (contentHeight > fixHeight) {
			jQuery(this).addClass("oversize");
		} else {
			jQuery(this).removeClass("oversize");
		}
	});
};

jQuery(document).ready(function(){
	imageboxHeight();
});

jQuery(window).on("resize", function(){
	imageboxHeight();
});

/* =============================================================================
 * ce_text_image_bar workaround if content height > fix height
 * ========================================================================== */
function imagebarHeight() {
	jQuery(".ce_text_image_bar").each(function() {
		var fixHeight = jQuery(this).height();
		var contentHeight = jQuery(this).find(".text-table").outerHeight();
		if (contentHeight > fixHeight) {
			jQuery(this).addClass("oversize");
		} else {
			jQuery(this).removeClass("oversize");
		}
	});
};

jQuery(document).ready(function(){
	imagebarHeight();
});

jQuery(window).on("resize", function(){
	imagebarHeight();
});

/* =============================================================================
 * search top trigger
 * ========================================================================== */
jQuery(document).ready(function(){
	jQuery(".ce_search_label").click(function(){
    	jQuery(".body_bottom .mod_search").addClass("show-search");
	});
	jQuery(".body_bottom .close-window").click(function(){
    	jQuery(".body_bottom .mod_search").removeClass("show-search");
	});
});

/* =============================================================================
 * scroll to anchors
 * ========================================================================== */
jQuery(document).ready(function()
{
    jQuery('#main a[href*=\\#], #left a[href*=\\#], #right a[href*=\\#], #slider a[href*=\\#], #footer a[href*=\\#], #bottom a[href*=\\#]').click(function(e)
	{
		// return with default behavior
		if( jQuery(this).hasClass('external-anchor') || jQuery(this).hasClass('not-anchor') )
		{
			return true;
		}

    	var target = jQuery('#'+jQuery(this).attr("href").split('#')[1]);
		if(target == undefined || target == null)
        {
            return true;
        }
        else if(target.length < 1)
        {
	        return true;
        }

        e.preventDefault();

        var stickheaderHeight = 0;
		if(jQuery('#stickyheader'))
		{
			stickheaderHeight = jQuery('#stickyheader').height();
        }

        jQuery("html, body").animate({scrollTop: target.offset().top - stickheaderHeight}, 500);
        return false;
    });
});
