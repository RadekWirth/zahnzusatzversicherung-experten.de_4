jQuery(document).ready(function($) {
	
	if($(window).width() < 768)
	{
		// change functionality for smaller screens
		$('.tooltip').tooltipster({
			animation: 'fade',
			delay: 200,
			theme: 'tooltipster-light',
			trigger: 'click',
			contentCloning: true
		});
	} else {
		// change functionality for larger screens
		$('.tooltip').tooltipster({
			animation: 'fade',
			delay: 200,
			theme: 'tooltipster-light',
			trigger: 'hover',
			contentCloning: true
		});
	}


    // Fix Ausgezeichnet.org
    var auorg = $('#auorg-bg').first().html();
    $('div#auorg-bg').html(auorg);

    // Toogle Menu - Accordeon
    $('span.content-expert>.content').hide();
    $('span.content-expert>.headline').append('<i class="fa fa-plus-square fa-15-em" aria-hidden="true"></i>');

    $('span.content-expert>.headline').click(function(){
	$(this).parent().find('.content').toggle(150);
	
	if ($(this).find('i').hasClass('fa-plus-square')) {
		$(this).find('i').removeClass('fa-plus-square').addClass('fa-minus-square');
	}
	else if ($(this).find('i').hasClass('fa-minus-square')) {
		$(this).find('i').removeClass('fa-minus-square').addClass('fa-plus-square');
	}
    });
    // Toogle Menu - Accordeon Ende

    
    
    
     mobileMenuFix();
     mobileEntriesFix();
});


function mobileMenuFix() {
	var nav = jQuery('nav');

	if(nav)
	{
		//console.log('nav found');
		nav.find('li.submenu').each(function() {
			var datatarget = jQuery(this).find('a').attr('data-target');
			jQuery(this).find('a').attr('href', datatarget);
		});
	}
}

function mobileEntriesFix() {
	var nav = jQuery('nav');

	if(nav)
	{
		var title = ''; //no-megamenu-title
		//console.log('nav found');
		nav.find('li.submenu a.submenu.no-megamenu-title').each(function() {
			var href  = jQuery(this).attr("href");
			var html = jQuery("div.mm-panel"+href+" ul").html();
			jQuery(this).parent().remove();
			jQuery("div.mm-panel"+href).remove();
			var search = href.substr(1,3)+(href.substr(-1)-1);
			jQuery("div.mm-panel#"+search).find("ul").append(html);
			
		});
	}
}
