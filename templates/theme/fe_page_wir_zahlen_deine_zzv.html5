<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>"<?php if ($this->isRTL): ?> dir="rtl"<?php endif; ?>>
<head>
    <?php $this->block('head'); ?>
    <?php $this->block('meta'); ?>
    <meta name="format-detection" content="telephone=no">
    <meta charset="<?php echo $this->charset; ?>">
    <meta name="robots" content="<?php echo $this->robots; ?>">
    <meta name="description" content="<?php echo $this->description; ?>">
    <meta name="keywords" content="<?php echo $this->keywords; ?>">
    <meta name="generator" content="Contao Open Source CMS">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <?php $this->endblock(); ?>
    <title><?php echo $this->pageTitle; ?></title>
    <base href="<?php echo $this->base; ?>">
    <link rel="shortcut icon" href="files/cto_layout/img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="files/cto_layout/img/apple-touch-icon.png">
    <?php echo $this->framework; ?>
    <?php echo $this->mooScripts; ?>
    <?php echo $this->stylesheets; ?>
    <link rel="stylesheet" type="text/css" media="all" href="<?php
    $objCombiner = new Combiner();
    //$objCombiner->add('files/cto_layout/scripts/swiper/swiper.min.css');
    $objCombiner->add('files/cto_layout/fonts/fontawesome-free-6.4.0-web/css/all.css');
    $objCombiner->add('files/cto_layout/scripts/mmenu/jquery.mmenu.all.css');
    //$objCombiner->add('files/cto_layout/css/animate.css');
    $objCombiner->add('files/cto_layout/scripts/jquery.mb.YTPlayer/css/YTPlayer.css');
    $objCombiner->add('files/cto_layout/scripts/isotope/isotope_styles.css');
    $objCombiner->add('files/cto_layout/css/framework.css');
    $objCombiner->add('files/cto_layout/css/customelements.css');
    //$objCombiner->add('files/cto_layout/css/customcatalog.css');
    $objCombiner->add('files/cto_layout/css/styles.css');
    $objCombiner->add('files/cto_layout/scripts/tooltipster/tooltipster.bundle.min.css');
    $objCombiner->add('files/cto_layout/themedesigner/css/layout_eclipse_industrytech_untitled-28.css');
    $objCombiner->add('files/cto_layout/css/customize.css');
    echo $objCombiner->getCombinedFile();
    ?>">
    <!-- <link id="layout_css" rel="stylesheet" title="theme_css_session" type="text/css" href="<?php echo $this->pct_layout_css; ?>"> -->
    <!-- <link rel="stylesheet" type="text/css" href="files/cto_layout/css/customize.css"> -->
    <!-- <link rel="stylesheet" type="text/css" href="files/cto_layout/css/print.css" media="print"> -->
    <?php echo $this->head; ?>
    <?php $this->endblock(); ?>

    <!-- Google Tag Manager -->
    <script>
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-N2VN4NS');
    </script>
    <!-- End Google Tag Manager -->
</head>
<body class="{{ua::class}}<?php if ($this->class) echo ' ' . trim(preg_replace('/fa(?:-[-\w]+|\b)/', '', $this->class)); ?>"<?php if ($this->onload): ?> onload="<?php echo $this->onload; ?>"<?php endif; ?>
      itemscope itemtype="http://schema.org/WebPage">

<?php if (!empty($this->sections['body_top'])): ?>
    <div class="body_top"><?php echo $this->sections['body_top']; ?></div><?php endif; ?>

<!--[if lt IE 9]><p id="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your
    browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to
    better experience this site.</p><![endif]-->
<div>

    <?php $this->block('body'); ?>

    <div id="contentwrapper">
        <div id="fix-wrapper">
            <div id="top-wrapper">
                <?php if (!empty($this->sections['top'])): ?>
                    <?php $this->block('top'); ?>
                    <div id="top">
                        <div class="inside">
                            <?php echo $this->sections['top']; ?>
                            <hr class="clear">
                        </div>
                        <div class="offcanvas-trigger"><span class="line1"></span><span class="line2"></span></div>
                    </div>
                    <?php $this->endblock(); ?>
                <?php endif; ?>

                <?php $this->block('header'); ?>
                <?php if ($this->header): ?>
                    <header id="header" class="header original" itemscope itemtype="http://schema.org/WPHeader">
                        <div class="inside">
                            <?php echo $this->header; ?>
                            <div class="clear"></div>
                        </div>
                    </header>
                    <div class="clear"></div>
                <?php endif; ?>
                <?php $this->endblock(); ?>
            </div>
        </div>

        <?php if (!empty($this->sections['slider'])): ?>
            <?php $this->block('slider'); ?>
            <div id="slider">
                <div class="inside">
                    <?php echo $this->sections['slider']; ?>
                </div>
            </div>
            <?php $this->endblock(); ?>
        <?php endif; ?>

        <?php if (!empty($this->sections['breadcrumb'])): ?>
            <?php $this->block('breadcrumb'); ?>
            <?php echo $this->sections['breadcrumb']; ?>
            <?php $this->endblock(); ?>
        <?php endif; ?>

        <?php $this->block('container'); ?>
        <div id="wrapper">
            <div id="container">

                <?php $this->block('main'); ?>
                <section id="main" itemscope itemtype="http://schema.org/WebPageElement" itemprop="mainContentOfPage">
                    <div class="inside">
                        <?php echo $this->main; ?>
                    </div>
                </section>
                <?php $this->endblock(); ?>

                <?php $this->block('left'); ?>
                <?php if ($this->left): ?>
                    <aside id="left">
                        <div class="inside">
                            <?php echo $this->left; ?>
                        </div>
                    </aside>
                <?php endif; ?>
                <?php $this->endblock(); ?>

                <?php $this->block('right'); ?>
                <?php if ($this->right): ?>
                    <aside id="right">
                        <div class="inside">
                            <?php echo $this->right; ?>
                        </div>
                    </aside>
                <?php endif; ?>
                <?php $this->endblock(); ?>

            </div>
        </div>
        <?php $this->endblock(); ?>

        <?php $this->block('footer'); ?>
        <?php if ($this->footer): ?>
            <footer id="footer" itemscope itemtype="http://schema.org/WPFooter">
                <div class="inside">
                    <?php echo $this->footer; ?>
                    <a href="{{env::request}}#contentwrapper" class="totop"></a>
                </div>
            </footer>
        <?php endif; ?>
        <?php $this->endblock(); ?>

        <?php if (!empty($this->sections['bottom'])): ?>
            <?php $this->block('bottom'); ?>
            <div id="bottom">
                <div class="inside">
                    <?php echo $this->sections['bottom']; ?>
                </div>
            </div>
            <?php $this->endblock(); ?>
        <?php endif; ?>

    </div>
</div>

<div class="body_bottom"><?php echo $this->sections['body_bottom']; ?></div>
<?php if (!empty($this->sections['body'])): ?><?php echo $this->sections['body']; ?><?php endif; ?>
<?php if (!empty($this->sections['mmenu_top'])): ?>
    <div id="mmenu_top"><?php echo $this->sections['mmenu_top']; ?></div><?php endif; ?>
<?php if (!empty($this->sections['mmenu_bottom'])): ?>
    <div id="mmenu_bottom"><?php echo $this->sections['mmenu_bottom']; ?></div><?php endif; ?>

<script>jQuery.noConflict();</script>
<?php echo $this->mootools; ?>
<script src="<?php
$objCombiner = new Combiner();
$objCombiner->add('files/cto_layout/scripts/mmenu/jquery.mmenu.min.all.js');
//$objCombiner->add('files/cto_layout/scripts/waypoints/waypoints.min.js');
$objCombiner->add('files/cto_layout/scripts/jquery.mb.YTPlayer/inc/jquery.mb.YTPlayer.js');
//$objCombiner->add('files/cto_layout/scripts/typed/js/typed.js');
//$objCombiner->add('files/cto_layout/scripts/countup/countUp.min.js');
//$objCombiner->add('files/cto_layout/scripts/parallax/jquery.stellar.min.js');
$objCombiner->add('files/cto_layout/scripts/doubletaptogo/doubletaptogo.js');
//$objCombiner->add('files/cto_layout/scripts/easypiecharts/easypiechart.min.js');
$objCombiner->add('files/cto_layout/scripts/cookie/jquery.cookie.js');
//$objCombiner->add('files/cto_layout/scripts/swiper/swiper.jquery.min.js');
$objCombiner->add('files/cto_layout/scripts/tooltipster/tooltipster.bundle.min.js');
//$objCombiner->add('files/cto_layout/scripts/chart/chart.min.js');
$objCombiner->add('files/cto_layout/scripts/scripts.js');
echo $objCombiner->getCombinedFile();
?>"></script>
<script src="files/cto_layout/scripts/customize.js"></script>

<?php $this->endblock(); ?>

</body>
</html>